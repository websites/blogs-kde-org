---
title: Neal Gompa
---

Neal Gompa is a developer for and contributor to multiple Linux distributions.
Most notably, he leads the development of Fedora KDE, which provides KDE Plasma
for Fedora Linux, CentOS Stream, and Red Hat Enterprise Linux. He is a big believer
in "upstream first", which has led him all over the open source world.
