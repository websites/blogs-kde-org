---
title:   "Introducing Carewolf"
date:    2004-12-01
authors:
  - carewolf
slug:    introducing-carewolf
---
Damn, now even I have blog. 

Like I used to renounce mobile phones (until 2001), but have come to accept them as a usefull form of communications, I might learn to embrace blogging as well.

For this blog entry I will introduce myself, what I do and what I have planned. I think it will help make sense of later much shorter entries.

My name is Allan Sandfeld, I often use the nick Carewolf which is an "avatar" sitting next to my monitor. I am a computer science student at Copenhagen University department of DIKU, and have been engaged in KDE since 2002, when I found it much more fun than the linux-kernel. 
In "real life" I am writing various project at the university trying to get ideas for a master thesis, which is the only really missing piece of my study. 

My active work for KDE is in two areas:<br>
<b>KDE Multimedia</b><br>
<i>aRts</i>: I monitor all bugs and I am one of the only active bug fixers. I hate the code though, and am only doing it for the good of all of KDE.
<i>aKode</i>: I am the author. It currently works as a decoder-plugin for aRts and have fixed most of commom problems in aRts. (a small secret: if you use the akode-plugin you don't need run artd in real-time mode)
It is also an independent library though, and I have direct backends for it for juk, amarok and kdemm, which I have not yet decided the fate of. The original plan of encoding is more less stranded on more interesting projects.
<i>TagLib</i>: I've added FLAC, MPC and general APE-tags to it. Thank Scott though, he is the one keeping the quality high, even in parts I've writen.
<i>KDEMM</i>: I was one of the original backers, but I have been slightly disillusioned about the project lately. I am not leaving it though.

<b>KDE Libraries</b><br>
<i>MIME-types</i> I've more or less maintained them for a period, have updated and improved the magic detection, and have more magic comming for KDE 3.4. If you think we should detect some wierd format, just send me an email or assign the wishlist to me at bugs.kde.org.
<i>KHTML</i> I decided to get engaged in KHTML back in September, and have since implemented several useless features and fixed obscure bugs. More importantly together with Germain Garand, KHTML has got a feel of really fast development. It is very satisfying work.


For the immediate future, I've got several wierd things in the drawer:

For aKode I have parts of a FFMPEG-decoder which will make WMA and RealAudio radio-streaming work in KDE. Pulling specific parts out of FFMPEG is just really hard, and I don't really like supporting properitary formats. Also I have the amarok backend I am trying to perfect and commit, not sure about the juk part.

For KHTML I have a few Safari-merges: FlexBoxes, but I've postponed them since they are more useless than most useless crap. Shadows, easy to port but unfortunatly _our_ Qt cannot paint shadows. Soft hyphens, but Safari only ignores them, they don't break words.

More KHTML stuff: The white-space: pre property, original a Safari merge, but again _our_ Qt handles \n differently, which means I have to implement a lot of string cleaning before letting Qt render any strings. CSS3 selectors, they became a candidate recommendation in 2001; I implement them when trying to take my mind off other projects.

I guess that is all. Next entry will be shorter and more ajour :)
