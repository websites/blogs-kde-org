---
title:   "Use-case of multiple text-shadows"
date:    2006-06-02
authors:
  - carewolf
slug:    use-case-multiple-text-shadows
---
As some of you know: When I implemented text-shadow for KHTML back in 2004, I added the ability to draw multiple shadows, because it was just a minor addition in my implementation, but one that meant we followed the entire spec. unlike Safari.

Ever since I've been trying to find a practical use cases for it though, and let's just say: It's not easy. Multiple shadows are just not all that usefull; but I think I've finally found one. 

The basic premise is that text-shadows can be used to draw outline by keeping the shadow offset (0,0) from the text and add a small blur like 2px. Well, with multiple shadows I can draw an outlined text and its shadow, and the best thing is that it regress poorly and will be unreadable in all other browsers (invisible in Firefox and MSIE, and blurry in Safari)!!! Finally a secret kode for Konqui lovers.

It's done like this:
<code>
   color: white;  
   text-shadow: 3px 2px 3px black, 0px 0px 2px black;"
</code>
For a live example see <a href="http://www.carewolf.com/secret-shadow.html">here</a> (why doesn't kdedevelopers.org support style attribute?)

Ok, so multiple shadows is not all that usefull. Fortunately it was only a one liner addition, so if it gets removed from CSS3 Text I won't cry.
<!--break-->