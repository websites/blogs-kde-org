---
title:   "Safari and KHTML again"
date:    2005-04-30
authors:
  - carewolf
slug:    safari-and-khtml-again
---
I just wish to weigh in on debacle to clear up some mistakes. First of all I would like to say I agree with Zack. The annoying part is not that Apple don't cooperate as much as they could. They are actually helpfull in answering questions and _tries_ at least to separate OS X specific features in the code (allthough they fail miserably at it). No, our problem are users who think Apple does more and underestimate the effort it takes for us to implement patches from WebCore. We are doing this for free and for fun, all we really want is appreciation for our effort.

In December 2004 I "ported" the CSS text-shadow property from WebCore. I put ported in quotes because the only thing ported was the code to parse the property. In the rendering Apple had created an extension to KWQ (their Qt) that used an OS X call to draw the shadow. This meant that to "port" it I had to write the shadow drawing myself. Not that I mind, it was pretty fun work. I was just saddened to see afterwards that many people assumed I had just merged a WebCore patch. The advantage of the reimplementation though is that we can apply any number of shadows like the standard requires, while Safari can only apply the one OS X supports.

The situation was even more extreme with the white-space patch. The goal was to make "white-space: pre" work like it did in WebCore. But the actual patch I merged from WebCore was exactly _one_ line of code. On top of that I had to merge the right 3 or 4 other changes that it broke, write several hundred lines of code myself and spend several weeks hunting corner case regressions. So the port of white-space: pre was one line of code from WebCore and several hundred written by myself and many weeks work. Yet it is treated as something I "ported" from WebCore. No wonder Zack is bitter if he has experienced the same. Of course the advantage again is that by redeveloping I could do it better than Apple, and thus KHTML supports both white-space: pre-wrap and pre-line from CSS 2.1 that Safari does not.

For those interested in the Acid2 patches I can say that I have merged three of them now and just need to fix a few new regressions. For small simple patches like these it takes 1 or 2 hours to merge them. It is still somewhat easier than to redevelop which would take at least a few hours more. Don't expect all of the patches. First of all the Acid2 test is complete crap. It doesn't really test any important features and the only thing it caught we couldn't do was min/max on positioned elements, the rest of the test are just obscure corner cases of error fall-back. Second of all some the patches by Dave are written in ObjC and is a joke to publish as KHTML patches. And lastly I believe correctly parsing SGML comments should be considered a bug.

