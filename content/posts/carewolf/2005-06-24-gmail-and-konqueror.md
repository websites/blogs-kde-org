---
title:   "GMail and Konqueror"
date:    2005-06-24
authors:
  - carewolf
slug:    gmail-and-konqueror
---
As some bugs.kde.org votes might have noticed I've fixed GMail (standard view) again in Konqueror. Although it is an odd sort of fix, because I really just improved our Mozilla emulation so we change one more bit from our usual KHTML/MSIE behavior to that of Mozilla when spoofing as a Mozilla based browser. This means that GMail only works when you use UserAgent spoofing and set it to Firefox.

So why doesn't it work when you spoof as Safari or use "?nocheckbrowser"?. 

Well to be honest, while GMail is a very nice application; from a Standards and a Web-Developer point of view it stinks. I've seen worse javascript on the web, but not of this magnitude. Not only is it auto-obfuscated in order to make it as hard as possible to figure out what it does, it implements two different paths. One that use proprietary MSIE extensions and one that use specific Mozilla extensions, the last has been superficially hacked to work on Safari.

There is a single specific line of code for Konqueror that detects it and then treats it as Safari. In the bug I fixed above Mozilla has implemented the DOM2 standard for MouseEvent::button() while we have implemented the MSIE non-standard. Unfortunately GMail now depends on us using the MSIE behaviour when they detect us and thus we cannot change to the standard without creating even more bugs in GMail (do you see the problem?).

But on the other hand there is a specific Safari work-around later that doesn't work in Konqueror because we have fixed a bug, while the Firefox version of the same code works fine, meaning we can get a working but slightly misrendered version of GMail by pretending to be Firefox (and often just misrendered because they send us invalid css with -moz- extensions).

All in all I am proud of having gotten this far, but in the end there is no way from our side keep GMail working in Konqueror. It is only a job that GMail can do, because they don't really care about safe implementations or standard behavior, and they don't care about making debuggable code.