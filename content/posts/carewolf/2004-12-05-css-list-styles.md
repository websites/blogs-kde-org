---
title:   "CSS List Styles"
date:    2004-12-05
authors:
  - carewolf
slug:    css-list-styles
---
I was working on render_list.cpp the other day and decided to complete our set of CSS2 list-styles. I just wonder if anyone anywhere has ever used the armenian and georgian styles, or even the various japanese "alphabetic" types? 
The really crappy part of all these styles is that the CSS2 standard doesn't say how they should be handled or rendered. They just list the 3 first entries or so, if they even do that.

I eventually found the algoritms for the CSS2 styles in the CSS3 draft recommendation! The CSS3 recommendation ofcourse have serious problems of its own, to give you an idea of rediculous they are getting, try these new alphabetic list-styles-types: lower- and upper-norwegian, somali, ethiopic-abegede-ti-er, ethiopic-halehame-ti-et or amheric-abagede. They seem to want a list style for every alphabet in the world, and are even failing badly at that, and just have list styles for all the obscure ones. I don't even think norwegian ones are not even correct, because W is not considered a native letter. 

In the end I decided to just implemented the CSS3 numeric styles for arabic, persian and urdu numbers. They can be as -khtml-arabic-indic, -khtml-persian and -khtml-urdu. They seem a lot more usefull than even most of the CSS2 styles.