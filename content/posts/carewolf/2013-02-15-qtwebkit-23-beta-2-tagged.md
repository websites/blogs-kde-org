---
title:   "QtWebKit 2.3 beta 2 tagged."
date:    2013-02-15
authors:
  - carewolf
slug:    qtwebkit-23-beta-2-tagged
---
Just a quick update to tell you beta 2 of QtWebKit 2.3 has been tagged. I am planning to tag the release before the end of the month. 

I would like to already now thank for all the encouragement and backup I have received on the project, Qupzilla and rekonq developers have been very helpfully and are already recommending to use QtWebKit 2.3 beta together with their applications, and a handfull of linux distros have been packaging it. The only thing missing is more feedback from Mac or Windows developers. On Linux this beta could be considered release quality, but on Mac and Win, there are still some issues I hope can get ironed out soon.

Fixed in beta 2:
 * HTML5 notifications.
 * Crashes related to history (you may lose the history on upgrade though, since the format has been upgraded).
 * Lots of corner case crashes.
 * Performance of large box shadows (as used on pastebin and paste.kde).
 * Fixed evaluation of 'forms.elements.length' on sites with a form element called 'length'.

Download:
  <a href="https://gitorious.org/webkit/qtwebkit-23/archive-tarball/qtwebkit-2.3-beta2b">qtwebkit-2.3-beta2b</a>

Edit: Updated link to trimmed tarball.