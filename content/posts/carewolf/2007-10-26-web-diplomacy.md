---
title:   "Web Diplomacy"
date:    2007-10-26
authors:
  - carewolf
slug:    web-diplomacy
---
So the cold war between KHTML and WebKit heated up a little again, this time the enemy isn't Apple, but former KHTML developers, and KDE personas. I will spare you the links, since it is all too embarrassing.

As someone with a leg in both camps, I will try and explain what is the real issues are and suggest a road map for peace.
<!--break-->
Since summer 2006 there have been active attempts to remerge KHTML and WebKit, or simply switching to WebKit. It started because Apple had started a process of opening up WebKit and creating a real open source community. In the beginning the effort was driven by George Staikos who started the Unity project. We discussed several solutions and requirements for KDE, and even wrote a list of demands for Apple. The list ended up a little too bureaucratic, and was rejected by Apple, but George kept going and tried to create a workable solution for both KDE and Apple, but at some point he gave up. During 2007 the Qt-version of WebKit keeps improving as Trolltech turned up their involvement, and at aKademy 2007 in June, Lars Knoll announced Trolltechs plans of including WebKitQt in Qt 4.4. We had a few meetings, discussing all kinds of solutions, but reaching no conclusion. After aKademy the discussions stopped, and no progress is made, until suddenly Troy published some of the discussion. This created a problem, since it was now "official" that KHTML will switch to WebKit, but we still had no idea how to solve the practical issues. Several months later a few KHTML developers responds with the FAQ that started the latest debacle.

<i>So what are the practical issues, why haven't we agreed on what to do?</i>

In the perfect world of helpfull magic fairies, we would dump KHTML and port all improvements from KHTML over into WebKit, but as all the trouble surrounding KDE 4.0 has shown so perfectly, the development model of "dumping stuff, replacing it with a good idea and let the magic fairies do the rest" has serious real world issues.

We would like several things:

<ul>
<li>
Binary compatibility: Practically impossible, but if we fix the KDE applications to just use the kpart interface we should be safe.
<li>
Feature compatibility: Hard, requires porting many improvements, and Apple are reluctant in accepting patches.
<li>
No bug regressions: Practically impossible. There are too many changes on both sides, and going through 5 years worth of patches is not anyones idea of fun. On the other hand, bug-for-bug compatibility with Safari might be worth more than having few bugs.
<li>
No speed regression: Hard, requires porting many improvements, and the WebKit has a very different understanding.
</ul>

These are just the KHTML work, then there is the surrounding work:
<ul>
<li>
Making WebKitQt work in a KPart (partly done), integrating it with KDE image viewers, integrating it with KDE settings, integrating it with KDE plugins, integrating it with KDE applications.
<li>
Syncing releases or branching. If we have to commit a patch to WebKit, wait for WebKit to release with the next version of OS X, and wait for Trolltech to release that with the next version of Qt, we are in for some very long and unsatisfying turn-around times. 
</ul>

<i>What happens if we don't solve these issues?</i>  
Remember the key phrase in aseigo and zrusin's blogs? "Let the market decide". 

Well KHTML is actually not in a bad shape, we have excellent standards support, unbeatable performance and pretty good support of most MSIE- and Firefox-extensions used on the web. We only miss a few features like contentEditable and native-SVG, but the sites that use these are still very limited. Most of all, the few sites that fail in Konqueror fails because they don't recognize us. 

Unfortunately this is enough for several KDE distributions to default to Firefox instead of Konqueror. With a 3rd party WebKit KPart available, these distros will likely replace Firefox with Konqueror+WebKit, but what will the rest do? I don't know, but the risk of losing a well-integrated web-component and much work is too big.

<i>What do we do?</i>
Well, right now the answer is simple. We are busting our ass to make KDE 4.0 the best KDE release ever, and since KHTML is all we've got, we are making it the best we can.


<i>What do we do after KDE 4.0?</i>
We have two options: Keep improving KHTML, or try and make WebKitQt good enough to replace it.

Improving KHTML is actually not that hard. WebKit has both SVG and contentEditable, and we are so experienced in both trees, that merging is a fun one week project. Still we are going lack hype.

At least trying to work with Apple is certainly a good idea. I've been submitting patches to Apple since November 2006. The first patch was finally merged in just a few weeks ago. Most patches was rejected over white-space. I thought it was a bit silly to send a 200-line patch back to me just to force me to change one line from "Element *node;" to "Element* node;", so for a long time I let the patches sit with Apple to see if they actually wanted contributors or just free employees. However after aKademy, I gave in and started resubmitted my patches closely following their KDE-like, except anal coding-style. Things are moving, but at a pace no one can be satisfied with.

Trying to work with Trolltech is another good option. Trolltech is running a Git branch of webkit that they sync and submit from regularly. Git has the distinct advantage of making it easy to merge individual patches between different branches. Where a branch in subversion would likely end up as a fork, a Git branch is more likely to just stay a branch. Trolltech is trying something completely new with Qt 4.4, it will contain both Phonon and WebKit, both projects developed outside of Trolltech. Phonon is primarily developed in kdelibs, and WebKit at Apple. Finding a solution where Trolltech could provide a branch of webkit that KDE could use and sync with our release, might make sense.

