---
title:   "First Release Candidate of QtWebKit 2.3"
date:    2013-03-05
authors:
  - carewolf
slug:    first-release-candidate-qtwebkit-23
---
I tagged the first release candidate of <a href="http://blogs.kde.org/2012/11/14/introducing-qtwebkit-23">QtWebKit 2.3</a> yesterday. You can find on <a href="https://gitorious.org/webkit/qtwebkit-23/trees/qtwebkit-2.3-rc1">gitorious</a>, where you also find the <a href="https://gitorious.org/webkit/qtwebkit-23/archive-tarball/qtwebkit-2.3-rc1">tar-ball</a>.

Not much have changed since beta2, crashes fixes, build fixes on non-Linux and a bug that caused browsers to open links in a new window when set to open in new tab.

For more on what QtWebKit 2.3 is, see my original blog-post about it: <a href="http://blogs.kde.org/2012/11/14/introducing-qtwebkit-23">Introducing QtWebKit 2.3</a>

This RC1, more or less matches QtWebKit from the upcoming Qt 5.0.2, except for Qt 5 specific features.

If you have more questions or feedback, catch me or other QtWebKit developers on freenode IRC #qtwebkit.