---
title:   "Safari and KHTML"
date:    2005-05-13
authors:
  - carewolf
slug:    safari-and-khtml
---
Notice how there isn't a vs in the title? :)

Hyatt and Maciej joined us on IRC yesterday, and we had some really good discussions. I might as well also admit that Maciejs comment was true (but out of context). Please notice that that implies we are discussing solutions and a common future. The idea of a common source tree is pretty much abandoned as we have very different goals and requirements, but we are discussing improved cooperation. With Apple just having released Tiger and us preparing for KDE4 we have a unique opportunity for bringing our source trees closer again.

Since Apple is being a nice guy for the time being, I will let them announce how things will improve once we have a solution, but please, no more "vs." stories for the time being, we are working on solving it.
<!--break-->