---
title:   "KDE, Science and Yule"
date:    2004-12-12
authors:
  - carewolf
slug:    kde-science-and-yule
---
Since the last blog I've added a few more goodies to KHTML most interestingly text-shadow, but also :target, :enabled and :disabled pseudo-classes. The most interesting development in KHTML though is Germain's work on incremental repaints (speeding up CSS animated webpages). Hopefully he can fix the last couple of regressions and commit it soon.

In aKode i've commited the jack-output sink, and an auto sink that auto-probes for Jack, ALSA and OSS. I am very disappointed in Jack though. It works like crap in default mode and requires a kernel-patch to get realtime priority. I expect now to add a polypaudio sink, and commit the JuK aKode-backend, so people can use it (and I can hummilate GStreamer by having a stable ALSA sink).

Unfortunatly all the work on KDE has come on a hard price for my study, last week I only managed to write a page or two on my project, and I even got an additional one. Now I have one project on partial-evalution of combinator languages, and one on divide & conquer of planar graphs. I really need to put down KDE development down once in a while and get some science done.

It seems though that when I am not coding, my time goes with the traditional danish yule celebrations. Yule used to a pagan/viking celebration with drinking, eating and whoring, which we in Denmark have carried over to the christmas celebrations, with a continuous series of parties throughout December. I usually have the two first parts of the tradition pretty nailed down, but I am improving on the later part, and tonight I have a date; so no more KDE or science today.