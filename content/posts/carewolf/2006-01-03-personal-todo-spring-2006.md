---
title:   "Personal TODO for spring 2006"
date:    2006-01-03
authors:
  - carewolf
slug:    personal-todo-spring-2006
---
I haven't had much time this fall to do much actual KDE development. So the number of tasks is starting to accumulate. I might have time to do one or two the next couple of months so I would like to give you the opportunity to give feed-back on what you feel is most important.

* Finish paged-media
There is a number of small left-overs from my Google SoC project. This includes implementation of "@page", and the ability to customize margin-headers and footer (and moving them out of the page-box and into the actual margin).

* Finish smooth-scrolling hack
I wrote a small hack to implement smooth scrolling in KHTML just before the release of KDE 3.4. It never really reached a releaseworthy state, but has somehow still ended up in both SuSE's and Mandriva's official packages (!). To finish it, I need a dialog with some good well-balanced options to choose from. Also I need to improve the hack to auto-disable itself when the computer can't scroll the page fast enough; some pages are just slower to scroll than others, so it is not a simple question of computer power.

* Implement CSS opacity
While technically a CSS3 feature, it is one implemented by Firefox, Opera and Safari, and even MSIE has a similar feature (filter: alpha). In other words Konqueror is the only browser that can't do it. 
I have most of the code to do it (I tried to merge it, back in the KDE 3.3 days), but Qt3 wasn't really capable of doing it in any kind of simple or elegant maneour. It seems this short-comming have finally been fixed in Qt, by allowing QPainters on QImages. Only problem is that KDE4 is really hard to even get to compile in this state, and once compiled I have no idea if all the crashes are my fault or just the state of affairs.

* Fork FFMPEG into aKode
For aKode 2.0 I wrote an experimental FFMPEG plugin for aKode. The real problem with it though is that it requires FFMPEG from CVS, and futhermore that FFMPEG is not really trying to stay either ABI nor API compatible. So like all other projects using FFMPEG plugins, it should really take a fork of FFMPEG and remove everything I don't need and ship it with aKode.

* Write xinelib backend for KDEMM
One of the goals for KDE4 is the adoption of flexible backends. With GStreamer sucking and NMM really being alpha software, what we really need is a good stable backend for simple audio and video playback. aKode can to begin with easily handle the audio playback, but something should be done about video. One good candidate is xinelib, but I need to look much into it to know what kind of problems there could be in that.

* Fix KIO
Kinda of a vague statement, but something should be done before KDE4. As a minimum we need to allow seeking in kioslaves, and also the ability to add new custom "move"-actions would come in handy (besides copy/move/link).