---
title:   "Development version 1.1 of Guidedog is available"
date:    2008-08-23
authors:
  - simon edwards
slug:    development-version-11-guidedog-available
---
Just a small announcement. Development version 1.1 of my little network routing configuration utility is up on <a href="http://www.simonzone.com/software/guidedog/">my website</a> for your testing pleasure. There is no new functionality. I've just ported it from KDE 3 and C++ to KDE4 and Python, saving it from ravages of bit-rot. It's a neat little utility and it would be a shame to let it get lost on the migration to KDE 4. It is also in Python now which should make the code a lot more accessible for contributors. If you've written a few shell scripts in the past, then your skills a probably high enough to hack on Guidedog and fix any bugs which show up.
<p>
My attention is spread across too many projects these days which slows down fixes and applying patches. So I've now put the guidedog source in KDE's subversion repository in the playground/network section. (<a href="http://websvn.kde.org/trunk/playground/network/guidedog/">/trunk/playground/network/guidedog/</a>) This will hopefully take me out of the critical path for fixes and patches.
<p>
I've also got a mostly done port of Guarddog to KDE 4 and Python here too on my computer which I want to move into KDE's SVN one of these days.
<!--break-->
