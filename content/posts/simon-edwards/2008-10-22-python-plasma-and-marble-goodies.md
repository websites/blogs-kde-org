---
title:   "Python, Plasma and Marble goodies"
date:    2008-10-22
authors:
  - simon edwards
slug:    python-plasma-and-marble-goodies
---
I landed the Python script engine for Plasma in KDE trunk about a week ago and already and the keen and excitable among us have been franticly trying to get it all set up and installed. rgreening said it best on IRC "I've been wanting this sooooooooooooooooooooooooo bad". Now that's what I call an endorsement. ;-) So if you are running KDE trunk out of subversion and you want to have a go at the Python support then you can have a look at this wiki page which I hastily wrote which describes what needs to be installed and in which order:

<a href="http://techbase.kde.org/Getting_Started/Build/KDE4/Python_Support">http://techbase.kde.org/Getting_Started/Build/KDE4/Python_Support</a>

It was hastily done, so you if you have more detail or information then please add it to the page.

About the Python support in Plasma itself. Right now there is a test/demo Python plasmoid (Pythoid!) in svn. You can guess which common time-keeping instrument this plasmoid displays. There is also a data engine which implements a simple time service. Runners are currently not in there but shouldn't be too hard to do (volunteers?). Also there are Python bindings for all of the plasma API. (Note that the bindings are not automatically updated from the C++ headers. They can sometimes lag a bit behind the C++ libraries they wrap.) The code itself mostly works but needs to be cleaned up a bit, documented and most of used and tested.

Now on to Marble. I was able to get an initial version of Python bindings for the Marble widget and related classes done and committed to subversion in time for the soft freeze. This means that we will support bindings for the marble widget in KDE 4.2. I'm pretty excited about this and the possibilities it can open up to people with crazy geo-data ideas who want a fast and effective way of implementing them. This also dove-tails nicely with my recent involvement with OpenStreetMap. OSM is a great way of combining cycling or a commute with geeky map making.
<!--break-->
