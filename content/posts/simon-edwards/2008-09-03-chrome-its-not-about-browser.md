---
title:   "Chrome: It's not about the browser"
date:    2008-09-03
authors:
  - simon edwards
slug:    chrome-its-not-about-browser
---
(Warning, rant ahead)

There has been a lot of excitement on the web about Google's new browser Chrome. So much excitement that it has been spilling over into the free  desktop blog world. Excitement is good in general, but I think many people are missing the point of Chrome and what Google is trying to achieve here. Chrome is not about building a better browser or winning the browser wars. It's about building a better platform for running web applications. It's about winning the internet operating system war. It's about determining what the "operating system" for running internet applications will look like in the future. It's about platforms, APIs and VMs, not web pages.

This is Google's way of trying to lift the base level of the web application platform out of the year 2001 and into the modern era. What's so special about the year 2001 you ask? That's the year when Internet Explorer 6 came out. The browser which still represents 36% of the page hits in the year 2008 according to the thecounter.com. IE6 also represents the last passable effort by Microsoft to improve their browser as a platform for developing applications which run inside it. For those who are too young to remember, once Microsoft won the first browser war just after the turn of the century and crushed Netscape, they threw the brakes on the development of their browser completely, and from what I can tell, cryogenically froze the IE development team for the next 5 years. Don't believe me, go check the <a href="http://en.wikipedia.org/wiki/Internet_Explorer">IE release dates</a> yourself, google the dates too if you don't trust Wikipedia. Microsoft woke up to the fact that the browser was an emerging platform that threatened the their windows platform. (I'm not sure if woke up is the right word, Marc Andreesen of Netscape Communications pretty much blurted out the whole plan when he commented in 1997 that the web browser would reduce the operating system to an "unimportant collection of slightly buggy device drivers".) From a web developer point of view nothing interesting has happened in IE since at least 2001. 

Chrome is Google's way of lifting the base platform that web developers can reasonably target for their applications and expect users to have installed. Google need this in order to be able to create the next generation of their applications like Google Docs, Google Maps, Gmail etc etc. Right now they are held hostage by Microsoft. What is important is not so much that people run Chrome, but that the performance improvements and new APIs make it to the browser users themselves, whether that be through Chrome or through improvements to other browsers like Firefox and Safari etc. A web browser is just a useful way, or a Trojan horse if you like, for getting the platform out to the masses. That's the plan.

Some more comments I want to make, or "Signs that you've missed the point":
<ul>
<li>"You complain that simple pages will use more memory." -- So what, we've got plenty of memory to run as many simple pages as we want, it is the big pages (read: applications) which are the problem.
</li>
<li>"You complain that Chrome doesn't have better RSS support or a built in RSS reader." -- The idea is this. You go online and find an online RSS reader. You then go to the little 'page icon' menu and select "make shortcut" and place an icon on your desktop. You then click on the icon and Chrome opens up without the unneeded browser address bar and controls. That's now your RSS reader.
</li>
<li>"You don't like the idea of the 'web application' mode where the browser controls and address bar hidden." -- Web applications are only web pages in technical sense, not a conceptual sense. You don't need those old browser controls any more than you need a hex memory viewer when using a desktop application. The URL is an implementation detail, get over it.
</li>
</ul>

So take your time, and think about these points, and save yourself from missing the point and <a href="http://www.osnews.com/story/20244/Google_Chrome_Considered_Harmful">posting rubbish</a> about Chrome on your otherwise excellent blog or website. Thanks for listening.

(Ok, the web developer hat is coming off and the KDE hacking hat is going back on. Time to get back to hacking on Python support in Plasma. \o/ yay )
<!--break-->
