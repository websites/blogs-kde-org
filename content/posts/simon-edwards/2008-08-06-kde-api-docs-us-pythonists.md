---
title:   "KDE API docs for us Pythonists"
date:    2008-08-06
authors:
  - simon edwards
slug:    kde-api-docs-us-pythonists
---
After the kdebindings meeting about a month ago in Berlin, I had a 8-ish hour long trip back on the train from Berlin to Nijmegen. Deutsche Bahn's trains are rather civilised and have power on board for all your laptop charging needs (provided you can get close enough to the seats with the tables *and* the power outlets). Anyway, after getting some preliminary Python coding working inside KDE 4's systemsettings (thanks go to rdale for his help), I had a go at trying to fix up the PyKDE class documentation to more closely match the C++ KDE API docs. About 5 weeks of hack time later I now have something which is ready enough for the public. The formatting is much more in line with the C++ docs and the pages are laid out and cross linked much better than the previous class reference for PyKDE. It is still not perfect (code fragments are not translated to Python), but it should be perfectly usable 98% of the time. <a href="http://api.kde.org/pykde-4.1-api/">Give it a try.</a>

This is part of my effort to update the PyKDE docs a bit move them over to the <a href="http://techbase.kde.org/Development/Languages/Python">KDE techbase</a> where it is much easier for everyone to help keep them updated and to add more information.

Oh, and I can't forget the obligatory:

<img src="http://www.simonzone.com/software/akademy2008_and_waffles.png" />

<!--break-->