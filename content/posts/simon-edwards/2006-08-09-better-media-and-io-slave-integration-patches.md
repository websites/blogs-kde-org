---
title:   "Better media and IO-slave integration (+patches)"
date:    2006-08-09
authors:
  - simon edwards
slug:    better-media-and-io-slave-integration-patches
---
In this fairly long article I discuss my attempt to simplify file and device management in KDE, while avoiding some of the draw backs of the current media:/ io-slave.

<b>The Challenge</b>

About a year ago I expressed concern about all of the extra io-slaves that have appeared in KDE. The intention of the new slaves was very important (e.g. better file and media management) but the implementation could be better, IMO. The problem as I saw it is that wholesale replacing the unix file system heirachy with a new but incompatible heirachy (e.g. system:/) comes at a price which is too high. The user gets a usable heirachy for dealing with files, but this heirachy doesn't work in non-KDE applications. Give a system:/ URL to apache or gedit for example, and they will give you an error message.

<!--break-->

It could be argued that the file heirachy is an implementation detail. But unlike implementation details like tracks and sectors on disk, the file heirachy is also the language that programs *and* users use when talking about the location of data stored on disk. It is not a detail that can be easily concealed.

This "problem" has been hanging around in the back of my brain ever since. In the last week or so the urge became too great and I had to see what really could be done about it. It is easy to guess and talk about what should be done to improve usability in an aspect of KDE, but guess-work is not the same as having a prototype in front of you that you can try out. Time to warm up that compiler.

Put simply, the goal of this investigation is to try to answer the question:

"What can we do <i>today</i> to make file management more usable in KDE while at the same time preserving integration with other non-KDE applications?"


<b>The Results</b>

First what everyone wants to know first, the results, and then late a technical explaination.

<img src="http://www.simonzone.com/software/kio_file_integration.png" class="planetkde" >

This is in a nutshell what I was able to come up with after a few evenings working, configuring and patching my "test" installation. It is our old friend konqueror operating as a file manager showing my hard disk. Things to notice:
<ol>
<li>Only the /home and /media directories are shown in the file system root. These are the only two directories in the root that a typical user really needs to deal with. The other standard directories are hidden.</li>

<li>The URL in the location bar is the standard unix path and most importantly, the paths here will be understood by any program on the system.</li>

<li>My home directory automatically has a house icon, and the home directories of any other users on the system are hidden.</li>

<li>Hard disks and removable media also have the correct icons just like in the standard media kio-slave. The icons have the same functionality as the icons in media:/. The context menu for these icons show the expected "Safely Remove", "Unmount" etc options.</li>

<li>The name of the sidebar folder is the name of this computer and some of the of the other redundant sidebar folders have been removed.</li>
</ol>

This setup integrates the functionality of media:/ into konqueror's view of the file system, only shows the directories that the user is interested while still maintaining compatibility with existing software.

Everything that is hidden here can be shown using the "Show Hidden Files" menu item in Konqueror by the way.


<b>What I did (technical)</b>

There are quite a few things going on to get this result.

<ol>
<li>Added support to the file:/ kio-slave for ".hidden" files. This makes it possible to hide arbitrary files in konqueror even though they don't start with a period (".") character. A ".hidden" is a file literally called .hidden, which contains a list of file and directory names which should not be shown in the GUI. This is the technique that Mac OS X uses to hide the unix directories in the root. Incidentally, the GNOME file manager Nautilus has supported this for quite some time too. oh, I of course put a .hidden file in my root directory. ;)</li>

<li>Integrated most of the functionality of the media io-slave into the standard file io-slave. What this means is that the file io-slave now passes on the extra meta-data about mounted devices on to Konqueror (or the Open file dialog for example). Using this meta-data Konqueror then knows when to show an icon of the hard disk instead of a blue folder. The context menus also depend on this meta-data in order to offer the right menu items like "Safely Remove" or "Unmount".</li>

<li>Added extra functionality to Konqueror to hide other people's home directories under /home, and to show a picture of a house for my home directory ($HOME).</li>

<li>Fixed KDE bug 101636, now konqueror shows the correct icon in the sidebar all of the time. A small cosmetic fix. It bugged me, ok.</li>

<li>Configuration changes in Konqueror. I removed the bookmarks sidebar and Home folder (Konqueror already has a Bookmarks menu), and changed the icon for the Root Folder to something more pleasant, like a computer. I also changed the label to match the computer's. (In this case "dappertest").</li>
</ol>

<b>Conclusion</b>


I'm rather happy with the result and I'll probably set this up on my "real" machine. It makes it possible to vastly simpilify file management in Konqueror. Using only one hierarchy you can easily get to your home directory, other hard drives, removable media and the standand unix filesystem itself if you want to.

Complexity and an overabundance of things and buttons and options is a common criticism of KDE. And I feel that this can help combat the problem.


<b>TODO</b>

<ul>
<li>The media notifier popup still opens things under system:/ instead of using the real mount point.</li>

<li>It would be nice if I have the history sidebar only show up in Konqueror's  web browser profile.</li>
</ul>

Here are the patches that I'm using:

kdelibs (3.5.4) - http://www.simonzone.com/software/kdelibs_file_cc.diff
kdelibs (3.5.4) - http://www.simonzone.com/software/kdelibs_file_h.diff
kdebase (3.5.4) - http://www.simonzone.com/software/kdebase_mounthelper.diff
