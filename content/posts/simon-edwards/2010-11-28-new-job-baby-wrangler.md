---
title:   "New Job: Baby Wrangler"
date:    2010-11-28
authors:
  - simon edwards
slug:    new-job-baby-wrangler
---
This news is perhaps a little bit late, but I can assure you I've been very busy in the meantime. The first day of November Debbie and I were able to welcome our first child to the world, Toby Edwards. We are all very happy. It was a pretty tough labour and result was a health boy who has since been busy growing and stacking on weight. The parenting learning curve is quite steep at the start but I think we're finally getting the hang of managing the baby and sleeping enough.
<p>
Thanks to all who suggested first and seconds names starting with Ks and Ds. Maybe next time eh.
<p>
<img src="http://www.simonzone.com/software/DSCN3280.JPG" />
<!--break-->
