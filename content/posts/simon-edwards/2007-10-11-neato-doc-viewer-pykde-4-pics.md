---
title:   "Neato doc viewer for PyKDE 4 [Pics!]"
date:    2007-10-11
authors:
  - simon edwards
slug:    neato-doc-viewer-pykde-4-pics
---
Jim Bublitz has been industriously working on getting the Python bindings for KDE 4 into shape. Part of that work is documentation of course and for that Jim has put together a very handy documentation viewer which combines reference docs with code samples and example code all in one easy to navigate package. One of the classic documentation problems for GUIs which are as customisable as Qt/KDE, is that everyone can, and often does, have their own visual style configured for their desktop. This of course means that any screenshots accompanying documentation simply don't match what is in front of the user most of the time. Having real widgets displayed and operational in the reference docs themselves solves this problem for Python developers at least. I think it's real neat.

Some screenshots below:
<!--break-->
<img src="http://www.simonzone.com/software/pykdedocs_1.png" />

<img src="http://www.simonzone.com/software/pykdedocs_3.png" />

<img src="http://www.simonzone.com/software/pykdedocs_2.png" />
