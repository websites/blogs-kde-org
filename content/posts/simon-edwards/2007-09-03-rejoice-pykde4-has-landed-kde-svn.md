---
title:   "Rejoice, for PyKDE4 has landed in KDE SVN"
date:    2007-09-03
authors:
  - simon edwards
slug:    rejoice-pykde4-has-landed-kde-svn
---
Python language bindings for KDE's libraries, PyKDE4, has landed in KDE's subversion repository. Jim Bublitz has been working behind the behind the scenes on PyKDE4 for quite some time, and now PyKDE4 is stable enough to enter its new home in subversion. The last of big sweeping changes to the code, like licensing notices and module layout for example, have been done and PyKDE4 is in good shape for those who want to get in there, port their applications or create new ones and help shake any bugs out. Now that KDE's libraries are mostly settled, changes and improvements to the bindings will be incremental in nature and not too disruptive for Python developers.

Almost all classes in kdelibs are covered, except Phonon which is waiting on imporved namespace support in SIP before we can add support. Supporting tools like a Qt designer compiler that works with KDE classes and uses i18n() are also in the works, along with extra support for things like installation, handling i18n messages etc, and not to mention documentation and example code.

PyKDE4 is in KDE's subversion repository in /trunk/KDE/kdebindings/python/pykde4/ . Be sure to read the INSTALL and README files for more information. Thanks go to Jim Bublitz who has done the real heavy lifting here, and also to Phil Thompson who developes SIP and PyQt4 which PyKDE4 is built on top of.
<!--break-->