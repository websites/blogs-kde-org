---
title:   "Usability, hierarchies and IO-slaves"
date:    2005-08-15
authors:
  - simon edwards
slug:    usability-hierarchies-and-io-slaves
---
<p>There is a really good series of articles on hierarchies and usability at the SAP Design Guild site which ever developer should read:</p>
<p>
<a href="http://www.sapdesignguild.org/community/design/design.asp">http://www.sapdesignguild.org/community/design/design.asp</a>
</p>
<p>(look in the left side menu for the hierarchy articles)</p>
<p>
For those who don't have time to read the articles I'll just break it down to a couple simple points below.
</p>
<!--break-->
<ol>
<li>PEOPLE DON'T "GET" HIERARCHIES. Us computer people live and breathe hierarchies. Wonderful data structure / organising method, can't live without it. Meanwhile back in the real world, real people do not share this love affair. Most people have trouble with hierarchies. They don't understand the structure, they get lost in them, the categories seem arbitrary. Sure people understand the hierarchical structure of their company for example, but they rarely feel comfortable with the abstract concept of a hierarchy as a way of organising data. In day to day life most people don't create or deal with hierarchies.
</li>
<li>People don't understand other people's way of organising things. This is discussed in the first hierarchy article under "Categories' Arbitrariness". Simply put, there is always more than one way to organise information into a hierarchy and people will often not understand the system being used. The never ending Kcontrol reorganisation discussions on the kde usability list are good testament to this simple truth.
</li>
</ol>
<p>
Now, getting to my point. KDE has been growing a lot of extra IO-slaves lately, system:, media:, homes:, settings:, and there are ideas floating around for more along the lines of movies:, music: and documents:. I can't help but get the feeling that by doing this we would just be supplementing one complex hierarchy (filesystem) that people have trouble with, with lots of extra smaller hierarchies that people can go have trouble with. I don't see the gain. I fear that this is exactly the wrong direction. Fixing poor organisation by adding even more poor organisation.
</p>
<p>
Not to mention the other problems that IO-slaves have. Firstly, they are hidden to the user. The user just doesn't know they are there. Secondly, the relationship between something in media: and the unix filesystem is a complete mystery. Also, people don't "get" URIs. They're for geeks.
</p>
<p>
I think that the only real structural solution is what Apple OS X and <a href="http://www.gobolinux.org/">GoboLinux</a> have done. Drop the unix filesystem hierarchy and think up a completely new one based on the user's needs.
</p>
<p>
Failing that, all I ask is that people keep <a href="http://en.wikipedia.org/wiki/KISS_Principle">KISS</a> in mind.
</p>