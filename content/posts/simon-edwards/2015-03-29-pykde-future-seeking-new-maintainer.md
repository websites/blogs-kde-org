---
title:   "PyKDE Future: Seeking a New Maintainer"
date:    2015-03-29
authors:
  - simon edwards
slug:    pykde-future-seeking-new-maintainer
---
<p>For anyone who has been paying any attention of PyKDE5 over the last year or so, it is no secret that development and maintenance has been at a standstill. I've been very busy with a family and small children, and that eats time like you wouldn't believe. (Unit number 2 is almost 6 months now, healthy and happy I can report.) But another important factor is that my interests have shifted towards web related technologies over the last few years.</p>

<p>So, it is time to put the call out for a new maintainer or maintainers. I would also like to put in a bit of an apology to anyone who has depended on PyKDE5 and has had to put up this wishy-washy state of limbo for the project. And I would also like the thank people for the occassional drive-by commit which has helped keep the existing functionality mostly working.</p>

<p>Anyone interested should contact me. I'm willing to help and advise anyone who wants to learn the ropes, and to facilitate a transition to new management. If you are interested and curious but don't want to stick your hand up immediately and commit yourself, then still email me and I can forward on some information to you which may help explain how PyKDE5 works and is updated. It should give you an idea of the task at hand.</p>
