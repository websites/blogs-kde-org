---
title:   "Hard disk failure? My unscheduled upgrade to Dapper"
date:    2006-04-06
authors:
  - simon edwards
slug:    hard-disk-failure-my-unscheduled-upgrade-dapper
---
This was my Wednesday. 5 in the afternoon GF phones me.

GF: "Your computer isn't working? It is not turning on. It is acting weird. What's wrong?"

ME: "How the **** should I know."

I was already not having a good day and my ESP wasn't working well enough to diagnose a machine 10km away and won't even boot up correctly.

GF: "Can you fix it?"

Meaning, can I fix it remotely from work.

ME: "No, I can't fix it from here."

GF: "But I need the internet!"

Like most geeky people, my machine is always on and apart from being a "workstation" and development machine for me, it is also a router, file server, firewall etc for any other machines on the LAN. Including GF's silver toaster-like shuttle PC.

ME: "I'll fix it when I get home".

So I go home. My machine is showing the Kubuntu Breezy installer. Also, like a lot of geeky people the only time I reboot is for installing operating systems or kernels. The boot sequence is naturally CDROM then harddisk. I was installing some packages from the Breezy disk the other day, so the CDROM was in the drive. I don't think too much about what I saw.

I reboot the machine.

Grub fails to kick in and complains about an error 17, whatever /that/ means. After scratching around on my desk looking for something, anything(!) to boot from ("Hey! a Kororra Xgl live CD"; didn't work with my graphics card dammit!) Remembering that Mandriva has pretty good rescue mode on their installation CD I boot it up and get to a console.

BRAIN: "My partition table doesn't look right at all. I don't use ext3, and where are the rest of my partitions?!?"

It was looking like a long night and we needed some food so I hopped on my bike and went to the 'Appie' (read: supermarket). On the fairly short trip I had a bad feeling to the effect of: "Wasn't there a vulnerability in OpenSSH recently? /Did/ I actually patch/upgrade?". I walk in and see that the whole refrigerated section is empty. I hear that there has been a power failure for most of the day. "Ah ha! That explains the reboot. Mental note to self, maybe a UPS is a good investment."

I get back, try a rescue ISO, run gpart, and poke around to see if there was anyway of saving the situation before resigning to a reinstall. Now, notice how I haven't said anything about all the freaking out and throwing chairs around the room and that is because I didn't need to. I was feeling only slightly annoyed and it wasn't because I've spent years up in the mountains working on my Zen ninja powers. ==> daily automatic backups.

So I grab a Kubuntu Dapper flight CD (flight 6 won't boot for me BTW) and start the reinstall. The installer starts up.

GF: "It was acting all weird and asking for a language and stuff"

She says while looking at my screen.

GF: "So I tried to get it working; pressing a few things."

ME: "You did ****ing what??? Did you see anything about partitions or disks??"

GF: "I don't know, maybe."

Ok, this post is getting a bit long and you can see where this was going.

A few lessons:

1. "Boot from CDROM" + installation media + reboot + GF, doesn't mix well.

2. There are no backups like daily backups. Get yourself a second HD and put it in your machine. I've been using a little program called faubackup to do backups to another file system. faubackup uses hardlinks to do incremental backups which look like full backups on disk and can easily be examined and restored from using standard tools. There are a few other backup programs that work like this. The peace of mind this buys is incredible.

Dapper is working quite nicely here. I was planning to upgrade from breezy on sunday anyway. I only lost some media and video from the internet and time.

It could have been much worse.
