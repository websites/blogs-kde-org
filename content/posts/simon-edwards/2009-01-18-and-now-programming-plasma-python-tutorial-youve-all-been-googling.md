---
title:   "And now the Programming Plasma with Python tutorial you've all been googling for..."
date:    2009-01-18
authors:
  - simon edwards
slug:    and-now-programming-plasma-python-tutorial-youve-all-been-googling
---
In a great demonstration that not only do great minds think alike, they can also subconsciously syncronise to attack the same problem, Luca Beltrame and I started work on our own tutorials about writing Plasma applets using Python at exactly the same time and day this weekend. We've coordinated ourselves and now there are 3 new tutorials about programming Plasma applets with Python up on techbase. The first tutorial by myself is an introduction to the whole work cycle of creating an applet. Luca continues in the second tutorial with how to use Plasma widgets in an applet. And today I've written another tutorial about how to use DataEngines in an applet.

All three are available from the <a href="http://techbase.kde.org/Development/Tutorials/Plasma">Plasma tutorials page on KDE techbase</a>.

As always KDE isn't a spectator sport, so you are welcome to add to the tutorials on techbase. Enjoy.
<!--break-->
