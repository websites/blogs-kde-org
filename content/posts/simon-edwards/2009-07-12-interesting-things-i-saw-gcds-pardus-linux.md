---
title:   "Interesting things I saw at GCDS: Pardus Linux"
date:    2009-07-12
authors:
  - simon edwards
slug:    interesting-things-i-saw-gcds-pardus-linux
---
This year at GCDS I had the pleasure of meeting Gökmen and Gökçen (pronounced “Gerkman” and “Gerkchan” with hard Gees not Jays). They are part of a relatively small team of around 15 developers who are sponsored by the Turkish government work on a Turkish Linux distribution called <a href="http://pardus.org.tr/eng/index.html">Pardus</a>. It is a KDE focused distribution which has been around since the end of 2005. What makes this distribution so interesting is the system tools and configuration tools which they've developed based heavily on Python, PyQt and PyKDE.

Here is a run down on the custom tools that I'm aware of and my impression thereof. I'm getting most of this information from the document “<a href="http://pardus.org.tr/eng/projects/comar/PythonInPardus.html">Python in Pardus</a>” [1], my discussions with G & G, and what I've seen from playing a bit with the current RC2 inside Virtual Box.

YALI is their “Yet Another Linux Installer” and is Python and PyQt based. Using it this afternoon it appeared quite polished and no more complicated than the typical Kubuntu install. The Pardus artwork leans toward “cartoon”-ish and less towards photo realistic like the standard KDE 4 artwork. The only blemishes I saw during the install were limited to a couple of English grammar problems and awkward sentences.

System boot and initialisation is done using a Python based system called Mudur which replaces the mass of shell scripts which make up the typical Linux boot system. This, I'm told, provides a simple and understandable boot system. They claim to be able to boot to KDM in around 15-20 seconds, although I can't really say if this is true since I've only used it in a virtual environment.

Instead of RPM or Debian dpkg, Pardus has its own package manager called PiSi written in, yes, Python. It appears to be quite complete and includes a lot of functionality in one place which is often spread amongst separate programs on other packaging systems. The package scripts for building the packages are just little Python scripts. The packages can also contain Python scripts for performing updates (e.g. converting old settings into a new format etc). Also package updates are done by transmitting only the deltas between package versions. After I installed the RC2, I updated two packages and it downloaded 34 Kb. Slick.  PiSi also includes a GUI based on PyKDE. It looks quite good and suitable for you're average user, and not entirely unlike KPackageKit.

The system configuration such as services/daemons, network connections, users and groups etc is managed by Çomar. From what I understand, it is a daemon (written in C) which sits on Dbus and centrally manages the system configuration, spawning Python processes to handle the actual work of changing and updating the configuration. There is a Python API which applications use to change the configuration and communicate with Çomar. Çomar can also send notification messages out to interested applications when the configuration changes. Gökmen demonstrated how changes to the wireless configuration were immediately reflected in their wireless plasmoid. There also a whole host of GUI configuration modules for KDE's “System Settings” written using PyKDE and Çomar.

Also, after I logged in on my Virtual Box install for the first time, I spotted a settings migration wizard and a desktop personalisation wizard for doing basic customisations such as double or single click mouse, type of menu in the K-menu etc.

As a Python fan and the main developer/maintainer of PyKDE, it certainly gives me that warm fuzzy feeling inside to see Python, PyQt and PyKDE put to such great use. It is also very impressive to see how such a small team of developers can put together such an impressive distribution. It is a great demonstration of why it is important to choose the right tool for the job. As one of the developers said to me, Pardus would not exist without PyQt and PyKDE.

The task for the future is to see how we, KDE and Pardus, can better work together to share code and make sure that more things can go up stream into KDE. The next major release of Pardus is due in about two weeks, is KDE 4.2 based and is definitely worth checking out. Keep up the good work Pardus!

[1] This document appears to be a couple years old.
<!--break-->
