---
title:   "New life in Simon speech recognition"
date:    2017-03-13
authors:
  - fux
slug:    new-life-simon-speech-recognition
---
As <a href="https://blogs.fsfe.org/mario">my blog as FSFE Fellow No. 1</a> is temporarily not aggregated on planet.kde.org and <a href="http://www.zeitverschenken.ch/index.php/blog">my private blog about woodwork (German only)</a> currently only tells about a wooden staircase (but soon again about wooden jewelry) I'm building I found a new place for my KDE (non-Randa) related stuff: KDE Blogs. Thanks to the KDE Sysadmin team for the quick setup!

Since the beginning of this year there is some new activity about and around the Simon speech recogition. We had several weekly IRC meeting (Logs: <a href="https://notes.kde.org/p/simon-meeting-201701">W02</a>, <a href="https://notes.kde.org/p/simon-meeting-201702">W03</a>, <a href="https://notes.kde.org/p/simon-meeting-201703">W04</a>, <a href="https://notes.kde.org/p/simon-meeting-201704">W05</a>, <a href="https://notes.kde.org/p/simon-meeting-201705">W06</a>, <a href="https://notes.kde.org/p/simon-meeting-201706">W07</a> and <a href="https://notes.kde.org/p/simon-meeting-201707">W08</a>) and there is a <a href="https://phabricator.kde.org/project/view/213/">workboard</a> with tasks. Our plan for the near future is it to release a last Kdelibs4 and Qt4 based version of Simon. Afterwards we focus on the KDE Frameworks 5 and Qt5 port and then we might have time and work power to look at new feature development like e.g. <a href="">Lera</a> or the integration of the Kaldi speech recognition framework. But there is parallel work as well like creating Scenarios, working on speech (acustic) and language models and document all this.

So to reach this first goal of a last kdelibs4/Qt4 based version of Simon (the last stable release of Simon happened back in 2013 and there are some commits waiting to be released) we need your help. Would you like to work on documentation checking, compiling first Alpha versions of the new release or just writing about Simon or showcasing it in videos then please get in contact with is via <a href="https://mail.kde.org/mailman/listinfo/kde-speech">email</a>, IRC (#kde-accessibility on freenode.net) or the <a href="https://forum.kde.org/viewforum.php?f=216">KDE Forums</a>

And if you'd like to start right away you'll find us tomorrow (Tuesday, 14th of March) at 10pm (CEST) in #kde-accessibility on freenode.net. Looking forward to meeting you!

PS: Something different and how times change: Just bought a dishwasher and got a printed copy of the GNU GPL ;-).