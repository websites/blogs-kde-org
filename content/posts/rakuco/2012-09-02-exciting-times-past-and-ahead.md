---
title:   "Exciting times, past and ahead"
date:    2012-09-02
authors:
  - rakuco
slug:    exciting-times-past-and-ahead
---
<b>The past part</b>
So many interesting things have happened since my last blog post that the list below is probably missing a lot of stuff.

<i>Switzerland and Germany</i>
Thanks to the KDE e.V., I took part in the Platform 11 Randa sprint last year (better late than never to talk about that!). It was my first trip abroad and my first flight ever. I was pleased to finally meet other fellow KDE hackers in person: Mario Fux and his family are awesome people, and so are David Faure, Alexander Neundorf, my fellow KDE-FreeBSD friend Alberto Villa, Sebastian Kügler, Aaron Seigo, Frederik Gladhorn and everyone else who was there for all those sprints which were happening at the same time.

I have shamefully not contributed much to the Frameworks efforts since then, but doing so is definitely part of my plans.

Also thanks to the KDE e.V., I was also able to fly to Berlin a few months after the Platform 11 sprint to attend Desktop Summit. Berlin is an awesome city, and meeting even more people from both GNOME and KDE was really nice. Kudos to everyone involved in the event's organization, which I found pretty much flawless. Interestingly enough, I think I ended up hanging out more with GNOME folks than with my KDE peers, which we can consider a successful case of cross-desktop collaboration :-)

<i>kdeutils</i>
Last year I also became the kdeutils module maintainer; now that Okteta has moved from kdeutils to kdesdk, Friedrich Kossebau decided to pass the torch to me.

Since then we have moved from SVN to git (thanks to the helping hands of Nicolás Alvarez, Ian Monroe, Torgny Nyblom, Eike Hein and everyone else in the #kde-git IRC channel), Evan Teran has showed up again and is occasionally working on KCalc, Rolf Eike Beer has been rocking KGpg as usual and I've been maintaining Ark as time allows.

<i>FreeBSD</i>
Alberto Villa approached me last year with the usual punishment for people who contributed to FreeBSD, and I became a FreeBSD ports committer. Being part of another community, with its own traditions, rules and characteristics is a good experience in terms of both community management and interaction. We have been doing a fairly good job at packaging Qt and KDE on FreeBSD, helping our users and contributing many fixes upstream.

And yes, it is possible to use FreeBSD as a desktop system (I am even run it on my laptop)!

<i>WebKit</i>
Speaking of communities and commit hats, last year I also became a WebKit committer. I'm not very actively involved in the Qt port, though: most of my work there so far has been on the EFL (Enlightenment) port. Working on a browser engine has been a fun exercise, and interacting with a community in which most of its members are paid to work on the project is certainly different from what I was used to.
</i>

<b>The future part</b>

This year has been even more hectic than 2011. At the end of March I left my friends at ProFUSION, stopped working for a while and took some time to relax. After very long 5.5 years, two weeks ago I finally got my Computer Science diploma. I did not like university very much, and am quite pleased that it is finally over.

And last but definitely not least, I have bought a <b>one-way ticket</b> and will embark on a 19h-long journey to Helsinki next Tuesday. I am going to be part of <b>Intel's Open Source Technology Center</b>, and start working in Intel's Espoo office this Thursday. I feel very excited about that: I have wanted to move to Europe for a long time, my job involves working on free software and learning Finnish is definitely going to be challenging! In the process, my girlfriend has had to become my wife, and will join me in Finland with our dog and cat at the end of the year, switching from 30ºC in Brazil to some very negative temperatures in Finland.

Let's hope the trend continues and next year's post has as many exciting stuff as this one :-)