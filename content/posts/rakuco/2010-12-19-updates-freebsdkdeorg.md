---
title:   "Updates to freebsd.kde.org"
date:    2010-12-19
authors:
  - rakuco
slug:    updates-freebsdkdeorg
---
A few days ago, <a href="http://people.freebsd.org/~avilla">Alberto Villa</a> committed some changes to the <a href="http://freebsd.kde.org/">KDE on FreeBSD</a> website: it is now using the Chihuaha framework, which means it looks a lot more like the rest of the kde.org websites. As noted in the commit message, many thanks to the <a href="http://windows.kde.org/">KDE on Windows</a> webmasters, since Alberto originally got the code from their website.

As for myself, I've recently started playing with the KDE conversion to git. The kdeutils conversion rules are shaping up nicely, thanks to the #kde-git folks who've been kind enough to always answer my questions. To get my feet wet, I decided to try converting the KDE on FreeBSD repository from SVN to git, since its history is much smaller and simpler than kdeutils'.

So after spending a few minutes writing the conversion rules and waiting for some feedback from the other KDE on FreeBSD guys, I filed a sysadmin bug report to start the conversion.

Thanks to <a href="http://www.omat.nl">toma</a> and <a href="http://neverendingo.blogspot.com/">neverendingo</a>, things went pretty smoothly, and freebsd.kde.org became the first website to have migrated from <a href="http://websvn.kde.org/trunk/www/sites/">/trunk/www/sites</a>, and we're now on <a href="https://projects.kde.org/projects/websites/freebsd-kde-org">/websites/freebsd-kde-org</a>. :)
<!--break-->
