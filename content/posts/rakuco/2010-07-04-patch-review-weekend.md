---
title:   "Patch review weekend"
date:    2010-07-04
authors:
  - rakuco
slug:    patch-review-weekend
---
Besides maintaining Ark, I also spend some of my KDE time reviewing and applying patches for other projects as well. This weekend I had some free time, which was dedicated to applying some Kopete patches and pushing some KDE-FreeBSD patches upstream.

So let's start with Kopete. My initial intention when subscribing to the <a href="https://mail.kde.org/mailman/listinfo/kopete-devel">kopete-devel</a> mailing list was to work on new features and get in touch with the development community in general. Even though in the end I haven't written code for Kopete myself and have replaced it with <a href="http://www.bitlbee.org">Bitlbee</a>, <a href="http://www.irssi.org">irssi</a> and <a href="http://www.gnu.org/software/screen/">GNU Screen</a> on a remote server, I still comment on the list, help moderate it and review patches whenever I can.

Actually, "reviewing" may be too strong a word here -- since I've never digged too deeply through the code base, I usually just give an outsider's look at the patches, watching for const iterators in foreach() loops, giving hints about what patches should be applied after trunk is unfrozen and other things like that. Since there aren't many active Kopete developers, even these small actions end up helping the development.

Yesterday I went through some <a href="http://reviewboard.kde.org/groups/kopete/">open review requests for Kopete</a>, checking which ones could finally be committed now that trunk is once again <a href="http://techbase.kde.org/Schedules/KDE4/4.5_Release_Schedule#June_23rd:_Tag_.2B_release_RC_1">open for feature commits</a>, asking some request authors to close their reviews as Submitted or simply pinging people about pending reviews. Ralf Haferkamp's <a href="http://reviewboard.kde.org/r/4274/">patch</a>, for example, implements wishlist item <a href="https://bugs.kde.org/97998">97998</a>; Manuel Luitz's <a href="http://reviewboard.kde.org/r/4039/">one</a> connects and disconnects an account when the IdentityStatusWidget is double-clicked. Finally, Igor Poboiko's <a href="http://reviewboard.kde.org/r/4357/">big patch</a> brings the translator plugin back to a working state after who-knows how much time -- I've never used it myself, but it apparently fixes <a href="https://bugs.kde.org/164870">no</a> <a href="https://bugs.kde.org/167503">less</a> <a href="https://bugs.kde.org/168026">than</a> <a href="https://bugs.kde.org/213133">6</a> <a href="https://bugs.kde.org/230686">bug</a> <a href="https://bugs.kde.org/234225">reports</a>! The patch does add some new strings, though, so it will only make it to KDE SC 4.6.

On the FreeBSD front, <a href="http://people.freebsd.org/~avilla">Alberto Villa</a> asked me a few days ago to take a look at some patches to kdeadmin's KNetworkConf and KDM that were lying around FreeBSD's ports tree. I asked him to forward the KDM patch to ossi (who has already given him some suggestions of improvements), but the KNetworkConf ones were fairly simple -- even though the application is quite old and mostly unmaintained, it's good to keep things such as correct FreeBSD release detection and routing information loading upstream.

P.S.: No, I'm not at Akademy :(
<!--break-->
