---
title:   "Around the world"
date:    2011-04-21
authors:
  - rakuco
slug:    around-world
---
After asking the KDE e.V. for funding, I can now finally say I'm going to the Platform 11 sprint in June and to Desktop Summit in August.

I would like to thank the e.V. (hi there Claudia!) and everyone involved in organizing both events for making these trips possible. Besides being my first international trips, these are going to be my first plane trips ever too :) I plan on taking a few hats with me to Randa and Berlin, in hopes that they can provide valuable contributions: my KDE developer hat (kdelibs and kdeutils), my KDE on FreeBSD hat (there are even going to be two of us there) and also my buildsystem hat.

Last but not least, the obligatory "I'm going to" images, made possible by KolourPaint -- as you can see, my work as an artist is heavily inspired by the master <a href="http://blogs.fsfe.org/padams/">Paul Adams</a>:

<img src="http://www.students.ic.unicamp.br/~ra072201/igtr.png">

<img src="http://www.students.ic.unicamp.br/~ra072201/igtds.png">

Update (2011-04-22): Credit where credit is due, the original awesome "I'm going to" artwork was created by Paul Adams :)
<!--break-->
