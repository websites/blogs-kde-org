---
title:   "In which the author introduces himself"
date:    2009-11-13
authors:
  - rakuco
slug:    which-author-introduces-himself
---
Hi there. In order to keep the tradition, an introduction in this first post. I'm Raphael Kubo da Costa, a 20-year-old Computer Science student at <a href="http://www.unicamp.br">State University of Campinas</a>, Brazil, and also part-time employee at <a href="http://profusion.mobi">ProFUSION Embedded Systems</a> (a very nice place for any free software developer, I'd say).

My first contact with KDE as a developer was in 2006 when I sent two <a href="http://websvn.kde.org/?view=revision&revision=566100">small</a> <a href="http://websvn.kde.org/?view=revision&revision=566733">patches</a> for Konversation. Curiously and frustratingly enough, when I started university in 2007 I found myself with much less time to dedicate to my own free software interests than before -- even though I did to go to the International Free Software Forum that year and happened to be one of those <a href="http://aseigo.blogspot.com/2007/04/know-score.html">"new and interested developers"</a> to whom aseigo showed off the soon-to-be KDE4.0 desktop on the exhibition floor. ;)

Since the beginning of this year I've been contributing to KDE in a few different fronts: I do some occasional bug triaging and commits to kdelibs, but most of my KDE time is spent maintaining Ark, our old and beloved file archiver, with <a href="http://metelliuscode.wordpress.com">Harald</a> and helping the KDE-FreeBSD team (I'm not a full-time FreeBSD user, although I'm writing this post directly from FreeBSD 8.0-RC3). So expect me to write more about these activities in the future.

Now let's try to make this post a little less "hello planet-ish" and talk about Ark, KDE's file archiver which lives in kdeutils. I believe it's one of the longest-living programs in KDE (the first commit shown by svn log is <a href="http://websvn.kde.org/?view=revision&revision=12097">revision 12097</a>, which was actually a CVS commit and dates back to the end of 1998). It has survived our four major releases and has had quite a few different maintainers (curiously enough again, most of them were Brazilian).

So what's the state of Ark today? As I write this post, Ark's bug count in Bugzilla is at 72, which I consider a small number to be honest. However, we have been mostly in bug fixing mode for at least a few months, and some bugs need pretty big changes in code to be properly fixed. Being a university student, working (even when it involves using Qt) and also having a life means I don't have that much time for big changes at the moment, and neither does Harald, who's also losing his hair to graduate from university.

I don't want to end this post in a negative mood, though. So my dear reader, you have been watching the KDE "scene" for some time, you would like to contribute and you don't know where to start. Well, Ark needs you! Despite its age, Ark has a small code base, and both Harald and I are (presumably) nice guys and we are more than willing to help you. And you, my dear reader who already contributes to KDE, how about doing something completely different? :)
<!--break-->
