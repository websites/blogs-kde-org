---
title:   "Calligra Words style selection combo"
date:    2012-01-18
authors:
  - pstirnweiss
slug:    calligra-words-style-selection-combo
---
Since Monday, a feature I have been working on for quite a while has been merged into Calligra master. This is the style selection combo box.
Until Monday, the style selection widget was a home brewed widget based on QFrame. It wasn't optimal and that is a British understatement. Now this widget is still home brewed, but based on a QComboBox.

As this is my first blog here, a bit of introduction is probably due. My name is Pierre Stirnweiss, I am french and currently lives in Munich. I have been using Linux for longer than I care to admit and have been coding for about 4-5 years now, starting on KOffice. I am now a fellow member of the Calligra team.

Now to the point:

<img src="http://s18.postimage.org/s11b9zl95/snapshot1.png">

On this first screen shot you can see two of these combo used (on the right hand side in the "Text Editing" docker). What you can see is that there is a paragraph style selected (Standard) and that no special character style is applied at the current cursor position.

<img src="http://s18.postimage.org/94r1cetzt/snapshot4.png">

On this screen shot you see the drop down menu of the combo. The items in the list provide a preview of each style. Here you can spot a small button on the currently hovered item. This button allows to call the full blown Style Manager where you'll be able to change the style options. Also of interest, the character style preview is shown "on top" of the currently selected paragraph style. This means that a character style defining only the bold property, will be previewed with the other properties (like font, italic, ...) of the current paragraph style.

If the style name cannot fit in the preview (because the font is too big for example), it will be scaled down. The real font point size is shown:

<img src="http://s18.postimage.org/5jv5t6pg9/snapshot5.png">

<img src="http://s18.postimage.org/6d7rlsvh5/snapshot2.png">

On this screenshot, the cursor is supposed to be in the "some bold words" part. You'll notice a green + sign in the character style combo. This has two purposes. The first is to notify that the style under the cursor has some modified properties. In that particular case the bold property is different from the defined one in the style. The second purpose is to allow a quick creation of a new style. Pressing the + button will bring you:

<img src="http://s18.postimage.org/oriaps7rt/snapshot3.png">

Here you can enter the name of the new style. Validating the name will create a style based on the text properties under the cursor.

Now that the functionality has been roughly presented, I must also give credit where credit is due. This combo implementation is to a very large extend inspired by the KComboBox, KLineEdit classes. The + button functionality derives directly from these classes.
As for the drop down menu and the style manager button, this is implemented using a custom delegate.

That's it for now. Please try out the next beta and let us know what you think.