---
title:   "KOffice Sprint 2k9"
date:    2009-06-06
authors:
  - jaroslaw staniek
slug:    koffice-sprint-2k9
---
Just pictures this time. For text see very detailed <a href="http://hannascott.blogspot.com/2009/06/koffice-meeting-090606.html">(b)log about the meeting by Hanna.</a>

<a href="http://wiki.koffice.org/images/5/5f/KOffice_Sprint_2009_group_photo.jpg"><img src="http://wiki.koffice.org/images/5/5f/KOffice_Sprint_2009_group_photo.jpg" width="472"><br/>(KoGroupPhoto)</a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


<a href="http://wiki.koffice.org/images/8/8f/KOffice_Sprint_2009_N810_KDE.jpg"><img src="http://wiki.koffice.org/images/8/8f/KOffice_Sprint_2009_N810_KDE.jpg" width="472"><br/>(KDE4 everywhere)</a>
<!--break-->
