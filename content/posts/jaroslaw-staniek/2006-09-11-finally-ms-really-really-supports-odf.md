---
title:   "Finally: MS really, really supports ODF"
date:    2006-09-11
authors:
  - jaroslaw staniek
slug:    finally-ms-really-really-supports-odf
---
We've been waiting for this step. Read on to discover how clever it is.

<!--break-->

Short version: MS now encourages you to make all your data ODF-compatible. Period.

Long version, after the /. <a href="http://it.slashdot.org/article.pl?sid=06/09/11/1342224">article</a>:

<pre>
"If you have compression activated on any folder, 
 then the compressed data is at risk from corruption.
 New files that are close to a multiple of 4K in size 
 will have their last 4,000 bytes or so overwritten 
 with 0xDF."</pre>

Noticed that? <b>0xDF</b>. 

(thanks to <a href="http://slashdot.org/~Rob+Kaper">Rob Kaper</a> for finding this out)

Q: Now, what all the MS haters will do? And, more important:

<pre>
 "When you have a monopoly
  What're your customers going to do?"</pre>

A:

<pre>
 "The guy at the keyboard of a Windows Vista box, 
  using Microsoft Office at work, and Windows Media 
  Player at home is not the customer, he is <b>_the product_</b>.
  The customers are Dell, AOL, media licensing conglomerates, and so on.</pre>

(thanks to <a href=" http://slashdot.org/~Tackhead">Tackhead</a> for the reminder)

OK, now seriously - this is of course awesome news for any vendor of backup solutions, but I hope users will get a new working patch quickly and no important data is lost.
