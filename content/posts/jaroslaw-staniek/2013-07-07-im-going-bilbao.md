---
title:   "I'm going to Bilbao"
date:    2013-07-07
authors:
  - jaroslaw staniek
slug:    im-going-bilbao
---
I'm going to Bilbao... well not quite to the whole Akademy but <a href="http://qtfortizen.blogspot.com/2013/07/meet-us-at-qt-contributors-summit-2013.html">mainly to the QtCS</a>. Looking at my calendar I conclude even getting these two days is a success. That said I'll try to show up @ Akademy too and say hello to you friends and chat a bit :)