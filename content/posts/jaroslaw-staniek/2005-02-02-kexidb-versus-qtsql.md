---
title:   "KexiDB versus QtSQL"
date:    2005-02-02
authors:
  - jaroslaw staniek
slug:    kexidb-versus-qtsql
---
KexiDB is a database API that abstracts over the specifics of individual database backends. 
Following article is just to allow you to make a choice...
<!--break-->
<a href="http://www.kexi-project.org/wiki/wikiview/index.php?KexiDB%20versus%20QtSQL">http://www.kexi-project.org/wiki/wikiview/index.php?KexiDB versus   QtSQL</a>

<img src="http://www.kexi-project.org/pics/0.1beta5/datetime_and_autonumber.png">
<br>
(KexiDB)
<br>
vs
<br>
<img src="http://doc.trolltech.com/4.0/noforeignkeys.png"><br>
(QtSQL 4)
