---
title:   "The fun with .mdb format"
date:    2005-01-23
authors:
  - jaroslaw staniek
slug:    fun-mdb-format
---
We can see <a href="http://dba.openoffice.org/drivers/mdb/index.html">an attempt</a>  to reuse <a href="http://mdbtools.sourceforge.net/">mdbtoos</a> by OpenOffice.org v. 2.0. 
<!--break-->
Thanks to Martin Ellis' contribution we have recently more than nothing done (also by reusing mdbtools, not commited yet) in this department within Kexi too. 

I've started to publish our <a href="http://www.kexi-project.org/wiki/wikiview/index.php?MDBDriver">thoughts and discussions</a> about gothas in this part of the development. There is also related log on <a href="http://www.kexi-project.org/wiki/wikiview/index.php?KexiDataMigrationAndSharing">Cross-DB-Engine Queries</a> (don't lament if it's not very informative for now..).