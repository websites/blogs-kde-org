---
title:   "Kexi Report Designer Jobs"
date:    2014-09-11
authors:
  - jaroslaw staniek
slug:    kexi-report-designer-jobs
---
It's not a secret that reasonable explained and mentored junior jobs attract new great contributors.

Based on a popular request, here's massive (growing) list of 10 new Junior Jobs for Kexi. Now they are mostly related to Kexi Report Designer, which receives a lot of love recently as an <b>unique</b> Free Software solution of this kind.

<a href="https://community.kde.org/Kexi/Junior_Jobs/Small_report_improvements">https://community.kde.org/Kexi/Junior_Jobs/Small_report_improvements</a>

Be sure to ask for details or request different kind of junior jobs... there are basically thousands of them :)
Enjoy!

<img src="https://farm3.staticflickr.com/2589/4084212198_c1882527a9_z_d.jpg"/><br/>
<small><a href="https://www.flickr.com/photos/jepoirrier/4084212198/">SQL car by jepoirrier, CC BY-SA 2.0</a></small>