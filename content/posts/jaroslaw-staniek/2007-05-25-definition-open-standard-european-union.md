---
title:   "Definition of \"Open Standard\" in the European Union"
date:    2007-05-25
authors:
  - jaroslaw staniek
slug:    definition-open-standard-european-union
---
<p>
<a href="http://en.wikipedia.org/wiki/Adam_Gierek">Adam Gierek</a>'s report on Innovation (INI/2006/2274: <i>"Putting knowledge into practice: A broad-based innovation strategy for Europe"</i>) has been adopted in <a href="http://action.ffii.org/Gierek_Report">plenary this Thursday 24 May with amendments</a>.
</p>

<p>
The report is probably the largest document related to innovation since launching the <a href="http://europa.eu/scadplus/glossary/lisbon_strategy_en.htm">"Lisbon Strategy"</a> in 2002. One of the most interesting points for those who follow battle over standards and patents is a definition of open standard:
</p>
<p>
"<i>[.. ] recalls the definition of open standards adopted by the Commission pursuant to that (i) the standard is adopted and will be maintained by a not-for-profit organisation, and its ongoing development occurs on the basis of an open decision-making procedure available to all interested parties; (ii) the standard has been published and the standard specification document is available either freely or at a nominal charge; (iii) the intellectual property - i.e. patents possibly present - of (parts of) the standard is made irrevocably available on a royalty-free basis</i>"
</p>
<p>
Details: <a href="http://action.ffii.org/Gierek_Report/ITRE_Tabled_Amendments#head-e819836a7a721d76a8d555c711d5d104320ae52f">action.ffii.org</a>.
</p>

<p>
By the way, the majority of the <a href="http://en.wikipedia.org/wiki/European_People%27s_Party">European People's Party (EPP)</a> voted for an amendment to promote software patents in standards. When you vote in your country, please remember this (<a href="http://en.wikipedia.org/wiki/European_People%27s_Party#Associate_members">list of members/observers</a>).
</p>

<p>As a side note, I think since <a href="http://en.wikipedia.org/wiki/Ecma_International">ECMA</a> is an international, private (membership-based), not at all a non-for-profit, then its "standards" will not be considered as open in EU -- especially that it is not uncommon to believe that you can buy standarization services for your proprietary format what can even lead to <a href="http://www.groklaw.net/article.php?story=20070312083134403">putting it as fast track for ISO</a> (MSOOXML case). Moreover if we talk about MSOOXML, it does not fulfill at least the following part of the definition: "development occurs on the basis of an open decision-making procedure".</p>

<p>
(thanks to Wladyslaw Majewski for this news)
</p>
