---
title:   "Qt for Android, 2nd try"
date:    2010-02-16
authors:
  - jaroslaw staniek
slug:    qt-android-2nd-try
---
Remember the <a href="http://blogs.kde.org/node/4070">last call</a>? After less than 5 months we can see <a href="http://www.newlc.com/node/23522">apparent success</a>, and a lot more than a proof of concept.

<a href="http://blip.tv/play/AYG_2CkC"><img src="http://lh4.ggpht.com/_FBlhhjjfQMQ/S3sMCh4GUQI/AAAAAAAAHWs/F1Q8VOPsHxY/s512/qt-android1.png"/></a>
<a href="http://blip.tv/play/AYG_2CkC">Click to enjoy the show</a>
<!--break-->
Apaprently, <a href="http://labs.trolltech.com/blogs/category/labs/lighthouse/">Qt Lighthouse</a> was used for porting the QtGUI module.

From the <a href="http://code.google.com/p/android-lighthouse/wiki/Compile">Author</a>:
<b><i>"You don't have to write your own jni<=>java connections, all the magic is done from java by com.nokia.qt package, take a look to examples/android/QtTest/src/com/nokia/qt"</i></b>

I can only say, Kudos to BogDan! And of course I still cannot believe how flexible Qt architecture is. I bet we can expect more ports this year.

Dig for more details... and leave what you discovered in the comments below.