---
title:   "Kexi: GSoC, JJ, Porting"
date:    2014-09-04
authors:
  - jaroslaw staniek
slug:    kexi-gsoc-jj-porting
---
There were quite a few Kexi releases since my last blog entry. I tell you, the focus in this work was on improving stability. As an effect, reportedly, there can be a whole day of work without stability issues. Not bad..

Kexi is in fact a family of 5 or more apps integrated into one environment. Given the scope and ambitions it can be easily stated that we miss 20 to 30 engineers! Encouraging and educating new ones is a neverending activity. Out of the possible approaches these are well known: Google Summer of Code, Junior Jobs, non-coding contributors.

<b>GSoC</b>

Google Summer of Code brought Harshita Mistry to the Kexi project this year. It's now possible to import <a href="https://community.kde.org/Kexi/Junior_Jobs/Add_support_for_importing_tables_from_LibreOffice_Base">LibreOffice Base</a> proprietary ODB files. Kexi is currently the only non-Java solution providing exit scenario from the Java Based HyperSQL database engine that LibreOffice Base employs. Currently table schema and data can be imported to Kexi. Much more can be done, if you're interested in joining, let me know.

But there's one more Google Summer of Code that I mentor. Amarvir Singh's <a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2014/xenochrist/5639274879778816">application</a> was accepted and I actually co-mentored him with Inge Wallin (thanks for convincing me!). Parley, a program that helps you memorize things, was obvious candidate for adding a database layer. The idea is to use an ordinary, usually file-based, database instead of a custom storage. Parley gets performant data storage solution for free and by the way, the file could be also opened by Kexi for modification.

<b>Junior Jobs</b>

Junior Jobs are sometimes available within the Season of KDE but are also present as a whole year offer. 
<a href="https://www.linkedin.com/pub/wojciech-kosowicz/6a/978/978">Wojciech Kosowicz</a> joined just before this summer and already contributed a number of useful improvements! Can a Junior be productive with a 160k-lines-of-code project in a week? Sure he can.

<b>Great non-programming contributors</b>

In addition, many great contributors offer their time and energy for testing and suggesting improvements. Among them, <a href="http://www.grahamstown.co.za/listing/fables_bookshop_ee">bookstore owner and hacker</a> Ian Balchin joined the team. He <a href="https://www.linkedin.com/pub/ian-balchin/3b/383/b8">moved</a> to Linux for his book database needs and now uses Kexi 2.8 paired with a database server.

<b>Porting to Qt 5, KF 5, and evolution of the UX</b>

Finally, together with Calligra Kexi sails to the new ports of Qt 5 and KF5. This is rather very early stage for Kexi, Krita is much more ahead in this process. At technical level the code and app will be more compact and more accessible for final touches. That would hopefully lead to truly native experience on various platforms. I expect noticeable refresh of the visuals in many ways. For example, layouts, menus, as developed in the <a href="https://techbase.kde.org/Projects/Usability/HIG">VDG's HIGs</a>. Moreover, the great <a href="http://wheeldesign.blogspot.com/2014/08/monday-report-old-style-in-new-form.html">C++ based style</a> will help in the transition.

For me this bit is a sign my personal change from developer who likes design and art to an amateur designer/creator who also program. You might remember <a href="https://blogs.kde.org/2013/03/10/fruits-css2013">this entry</a> posted 18 months ago, probably before the new KDE visual/UX philosophy emerged. Now I see the building blocks fit.

Kexi 2.x already mixes of web user experience (just start Kexi to see its very own assistant, and removal of many K/QDialogs) and advantages of unmatched performance and offline capabilities. This happens without compromises like constriants of the traditional QStyle. Often I am thinking about application's appearance in the terms of its very own signature, program's DNA. I'll try to prepare new mockups.

And of course... find a guy wearing an old Kexi T-shirt, because:

<a href="http://akademy.kde.org/2014"><img src="https://community.kde.org/images.community/2/22/Banner400.going.png"></a>