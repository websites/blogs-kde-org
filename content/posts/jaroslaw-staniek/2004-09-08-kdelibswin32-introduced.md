---
title:   "KDElibs/win32 Introduced"
date:    2004-09-08
authors:
  - jaroslaw staniek
slug:    kdelibswin32-introduced
---
Yes, first part of my <a href="http://iidea.pl/~js/qkw/">QKW</a> patches are merged with first few (most hardcore/lowlevel) KDElibs directories. Beside Kexi, that's what I am playing with since march 2003. Everyone is invited to contribute. 

<a href="http://kde.ground.cz/tiki-index.php?page=KDElibs+for+win32">More information</a>

