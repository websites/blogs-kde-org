---
title:   "Usable TechBase layout"
date:    2007-12-20
authors:
  - jaroslaw staniek
slug:    usable-techbase-layout
---
Too many KDE folks refuse to use http://techbase.kde.org/ because it breaks blocks of code.

You can however make it usable. A hint just for you:
<!--break-->

<tt>
[jstaniek] btw, I've improved http://techbase.kde.org/Projects/PIM a bit, and have 'windows port' column
<i>[jstaniek] does not use oxygen wiki style which actually causes usability problems with <code> blocks</i>
[marc_kdab]	oh, you can switch the style?
[marc_kdab]	how?
[marc_kdab]	that could actually make the wiki readable :)
<i>[marc_kdab] hates fixed-width formats in websites</i>
[jstaniek]	marc_kdab: ah, so I should blog about this; too many folks complain... and I talked to danimo @ akademy but... well...
[jstaniek]	(same for websvn, btw)
[marc_kdab]	yes, and the apidox
[jstaniek]	marc_kdab: <b>login -> my preferences (on the topbar) -> skin tab -> monobook</b>
[marc_kdab]	oh, sigh, login...
[jstaniek]	marc_kdab: y, you can set permanent login
<i>[jstaniek] hacked his wiki page with readable <a href="http://en.wikipedia.org/wiki/Breadcrumb_(navigation)">breadcrumbs</a>, eg. "News->Kexi Releases-> Kexi 2007.1" and forced reasonable width (http://kexi.pl/en/News/Kexi_Releases/Kexi_2007.1)</i>
[marc_kdab]	ahhh :)
[marc_kdab]	mediawiki look
<i>[marc_kdab] files a request for mediawiki to remove styling :)</i>
[jstaniek]	the oxygen style is great but also breaks mediawiki skin rules a bit - e.g. we have even no kde logo defined for the web site... :)
</tt>
