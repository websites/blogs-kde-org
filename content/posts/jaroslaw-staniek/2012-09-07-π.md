---
title:   "π"
date:    2012-09-07
authors:
  - jaroslaw staniek
slug:    π
---
Evening conversation with my 7 y.o.:

<i>me: Michael, tell me what's the next number after -1.</i>

<i>0</i>

<i>me: OK...</i>

<i>what's the next number after π?</i>

<i>4</i>

Close enough as he answered after ~1 second, never even hearing about π ... unless he's secretly reading Wikipedia articles instead of playing games...

What's your interesting moment of this sort?

PS: Adam recommended me <a href="http://themagpi.com">themagpi</a> if I finally decide to show some programming to Michael (well, we started even before, with tools like <a href="http://repl.it">repl.it</a>).

<br>
<img src="http://farm3.staticflickr.com/2633/3966729594_980a3ece7a.jpg"><br/>
(CC by <a href="http://www.flickr.com/photos/gabrielsaldana/3966729594/">gabrielsaldana</a>)