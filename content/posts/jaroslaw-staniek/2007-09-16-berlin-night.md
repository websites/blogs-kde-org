---
title:   "Berlin by night..."
date:    2007-09-16
authors:
  - jaroslaw staniek
slug:    berlin-night
---
<a href="http://kexi-project.org/pics/2.0/alpha3/kword/kword2win.png"><img src="http://kexi-project.org/pics/2.0/alpha3/kword/kword2win_sm.png"/></a><br/>
(click to enlarge)</br>

Yes, this is working copy of KWord 2.0 alpha running on KDE 4 under MS Windows, compiled during KDE-On-Windows meeting in Berlin. I must say local Trolltech's office is great environment for developing.
<!--break-->

<img src="http://kexi-project.org/pics/2.0/alpha3/kword/kword_task.png"/>

<img src="http://kexi-project.org/pics/2.0/alpha3/kword/koffice2.0a-458.jpg"/>

<img src="http://kexi-project.org/pics/akademy/2007/berlin_win/berlin_win_meeting_2k7_sm.jpg"/>