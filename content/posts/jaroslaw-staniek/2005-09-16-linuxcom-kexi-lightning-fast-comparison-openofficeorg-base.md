---
title:   "Linux.com: Kexi is lightning fast in comparison to OpenOffice.org Base"
date:    2005-09-16
authors:
  - jaroslaw staniek
slug:    linuxcom-kexi-lightning-fast-comparison-openofficeorg-base
---
Read more in this <a href="http://applications.linux.com/article.pl?sid=05/09/08/1411250&tid=73">article</a>. 
It's a comparison with Kexi 0.9 from may 29, also read <a href="http://applications.linux.com/comments.pl?sid=35712&cid=85611">some my explanation</a>.

<!--break-->