---
title:   "New 'Cool' Developments"
date:    2011-08-28
authors:
  - jaroslaw staniek
slug:    new-cool-developments
---
"World must be crazy" say fellow hackers when realized that one day I left <a href="http://www.rd.samsung.pl/index.php?page=research&id=2">Samsung's Linux Mobile Lab</a> to work on Smart Refrigerators. 

But well, it's still in the same company, the same city. Yet this does not mean I am stopping to dig in Linux stuff for living: we're talking about Linux fridges.

However, there is something even less expected: <b>these are full four-doors Qt fridges</b>. I dare to say, except for cars or airplanes with infotainment modules, for me these cooling monsters are one of the biggest 'Qt devices' available on the consumer market.

Different Form Factor pushed to the extreme:<br><img src="http://kexi-project.org/pics/ads/qt-everywhere.png"/>

Read on for some hot details.
<!--break-->
So I have opportunity to work on the third generation of the fridge. The first was WiFi-enabled SRT746AWTN, which at software level is even <a href="http://qt.nokia.com/qt-in-use/story/customer/samsung-srt746awtn-refrigerator">marketed by Nokia</a> as a customer story. 

<a href="http://qt.nokia.com/qt-in-use/story/customer/samsung-srt746awtn-refrigerator"><img src ="http://qt.nokia.com/images/qt-in-use/devices/Samsung_Refridgerator.png/image_preview"><br>SRT746AWTN, the very first generation of the Qt Fridge</a>

Then this year appeared (already quite popular) RF4289x, <a href="http://www.engadget.com/2011/01/12/samsung-wifi-enabled-rf4289-fridge-cools-eats-and-tweets-we-go/">announced</a> at <a href="http://www.engadget.com/2011/04/18/samsungs-rf4289-wifi-smart-fridge-gets-a-dumb-3-499-price/">CES</a> (<a href="http://www.youtube.com/watch?v=R3e5aBh7JUI">video</a>).

Technically, the fridge itself is an intelligent black box for us, developers. What developers do really deal with is the a Linux embedded device with 8" LCD Screen touch screen capable to control the box and handle quite a few apps. So far following custom Qt apps are included: <a href="http://www.samsung.com/us/topic/apps-on-your-fridge">Memo, Recipes, Picasa Photos, Weather viewer, Organizer, News, Internet Radio, Twitter</a>.

<a href="http://www.blogcdn.com/www.engadget.com/media/2011/01/samsung-rf4289-1engadget-1294838262.jpg"><img src="http://www.blogcdn.com/www.engadget.com/media/2011/01/samsung-rf4289-1engadget-1294838262.jpg" width="400" height="266"></a>

<i>"Why do I need another device with apps in my home when we all own smartphones?"</i> you would ask. Let's note that the device would be the only one in your home that is always-on, and maintenance-free (no geeks assistance needed). As such, its Calendar/Organizer app should be useful for sharing appointments in a traditional way but geeky style. And with screen of a tablet size rather than smartphone.

<a href="http://www.blogcdn.com/www.engadget.com/media/2011/01/samsung-rf4289-img0934engadget.jpg"><img src="http://www.blogcdn.com/www.engadget.com/media/2011/01/samsung-rf4289-img0934engadget.jpg" width="400" height="266"></a>

What extra uses would you like to see for this high-end device? How about a full SDK and App Store? Let's hear about it in the comments.