---
title:   "KOffice Logos & Icons"
date:    2008-04-20
authors:
  - jaroslaw staniek
slug:    koffice-logos-icons
---
NLnet provided new KOffice logos (and it sponsors ODF development). Kudos for that!
To help readers of the <a href="http://dot.kde.org/1208521131/">announcement</a>, let's note that <b>application logo is not always equal to its icon</b>.
<!--break-->
For example it is not the case in KOffice 2. Look how logos of 4 koffice apps would look on the taskbar or window titlebar (16x16 size): <img src="http://wiki.koffice.org/images/thumb/5/54/Koffice-logo-kword.png/16px-Koffice-logo-kword.png"/> <img src="http://wiki.koffice.org/images/thumb/b/b9/Koffice-logo-kspread.png/16px-Koffice-logo-kspread.png"/> <img src="http://wiki.koffice.org/images/thumb/e/e8/Koffice-logo-kpresenter.png/16px-Koffice-logo-kpresenter.png"/> <img src="http://wiki.koffice.org/images/thumb/8/8b/Koffice-logo-kexi.png/12px-Koffice-logo-kexi.png"/>
Try to find out what's what in ~1 second...

Edit [21 apr]: (I kept the text-under-icons because 1. In real world logos are not editable - once accepted - often you cannot alter their colors, remove elements, change their padding area.., 2. E.g. Kexi logo without the app name looks like "close" button "(x)". So, here are real KOffice icons for comparison: (16x16) <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/16x16/apps/kword.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/16x16/apps/kexi.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/16x16/apps/krita.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/16x16/apps/kchart.png"/>, (22x22) <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/22x22/apps/kword.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/22x22/apps/kexi.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/22x22/apps/krita.png"/> <img src="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/runtime/pics/oxygen/22x22/apps/kchart.png"/>

I've published a page presenting the excellent logos and Oxygen icons for KOffice applications as well as typical Oxygen document icons, side-by-side (click to visit the page):

<a href="http://wiki.koffice.org/index.php?title=Icons/KOffice_apps_logos_and_icons"><img src="http://wiki.koffice.org/images/9/94/Koffice_logos_and_icons_sm.png"/></a>