---
title:   "Kudos to Marcus and the aKademy team!"
date:    2006-10-03
authors:
  - jaroslaw staniek
slug:    kudos-marcus-and-akademy-team
---
Someone on the <a href="http://dot.kde.org/1142439906/">dot</a> asked about Marcus Furlong -- "I didn't know him, where're his code contributions?". Well. Sometimes I think writing code is so much "safer" activity: you can delay your release, you can keep stuff uncommited or not working, and find at least dozens reasons to justify that. Try to do the same with recent Marcus' task! No way. You need to work in real time. People will not wait. 

<a href="http://kexi-project.org/pics/akademy/2006/akademy.jpg" target="_blank"><img src="http://kexi-project.org/pics/akademy/2006/akademy_sm.jpg" align="center" border="0" alt="aKademy 2006"><br>(click to enlarge)</a>

<!--break-->

So finally, great job Marcus, thanks to you and your aKademy team! 
Virtually all of us are happy with this year aKademy. No important questions were unanswered. Any yet, I learned your're really nice man.

In my "department", I tried to cover various database related topics and yet not to scare people, suggesting them it's about data models, and not really ORACLE administrating or things like that. In OpenDocument day, I mentioned <a href="http://kexi-project.org/download/kde4/akademy2006/odf_db/odf_db.odp">db-related issues</a> for extending ODF. Rob Weir proposed <a href="http://opendocument.xml.org/files/ODDK.pdf">OpenDocument Developers Kit</a>, where I found things similar to these covered in my TODO file as well. "20.Software which packs/unpacks a document into relational database form" was one of my favourites.

On the other hand, topics for <a href="http://kexi-project.org/download/kde4/akademy2006/kdelibs_win32/kdelibswin32.odp" BoF ">"KDE apps/libs development on Windows"</a> BoF (<a href="http://kexi-project.org/download/kde4/akademy2006/kdelibs_win32/transcript.txt">transcript</a>) was diversed, for instance thanks to Alan Horkan's input. Holger Schroeder was there too!

Additional small <a href="http://kexi-project.org/download/kde4/akademy2006/kexidb/kexidb.odp">"Database components for KDE applications developers"</a> BoF was organized to fulfill a few requests, and now you at least know there are things letting you to avoid working from scratch if you want implement database access.

<a href="http://kexi-project.org/pics/akademy/2006/kexi.jpg" target="_blank"><img src="http://kexi-project.org/pics/akademy/2006/kexi_sm.jpg" align="center" border="0" alt="Kexi advertisement on Grafton street"><br>(click to enlarge)</a>

Fizz devoted his time, met me at the very first bit of the hacking maraton and presented KDE-wide ODBC data source configuration tool, sooner or later to be available as a KCM module for KDE 4. This is going to be the first time where such tool is in par with its <a href="http://support.adobe.com/devsup/devsup.nsf/b2557cfb56bbb8788825691500525bb2/e3a0d615767fe76388256ed3005a41c3/$FILE/ODBC%20Data%20Source%20Administrator.png">Windows</a> or <a href="http://www.openlinksw.com/info/docs/uda42/images/lite/osxlite12.gif">Mac</a> counterparts. And finally we found a conclusion how to make it nicely play with <a href="http://www.kexi-project.org/docs/svn-api/html/namespaceKexiDB.html">KexiDB</a> interfaces, to make it usable for average user as well!

<a href="http://kexi-project.org/pics/akademy/2006/koffice_shop.jpg" target="_blank"><img src="http://kexi-project.org/pics/akademy/2006/koffice_shop_sm.jpg" align="center" border="0" alt="KOffice advertisement at Grafton street"><br>As you can see Dublin is full of KDE ads ;)</a>

<a href="http://dot.kde.org/1152490640/">Sebastian Sauer</a> (of <a href="http://www.koffice.org/developer/apidocs/libs-kross/namespaceKross.html">Kross</a> scripting framework fame, on the photo above) proved to have wonderful sense of humor. If you took a look over Sebastian's shoulder, you could notice he's actually developing <a href="http://kde-files.org/index.php?xcontentmode=617">scrips</a> for OpenDocument data integration in minutes, not days. And these solutions just depend on small KOffice libraries - e.g. you don't need to run any app to export your data to a spreadsheet file. This can became soon a KDE-wide feature.

<a href="http://kexi-project.org/pics/akademy/2006/assistant.jpg" target="_blank"><img src="http://kexi-project.org/pics/akademy/2006/assistant_sm.jpg" border="0"><br>Some design notes for KAssistant</a>

The last, private talk with Tobias König was interesting for me -- I hope we can get some results relevant for development of the <a href="http://pim.kde.org/akonadi/">Akonadi</a> storage backend.

The final contest is: find three KDE logos on the above photos.
