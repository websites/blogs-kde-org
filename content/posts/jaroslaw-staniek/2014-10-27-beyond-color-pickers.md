---
title:   "Beyond the color pickers"
date:    2014-10-27
authors:
  - jaroslaw staniek
slug:    beyond-color-pickers
---
Andy, Thanks so much for picking up the topic of <a href="http://anditosan.com/blog/2014/10/26/color-pickers/">color pickers</a> (pun intended).

I'd like to add to the topic by discussing cases when accuracy is not as important at results. In this case there's a problem of <b>abundance of choice</b>. You don't have time to quickly and <b>correctly</b> pick one from the 16777216 colors. Plus alpha value? Come on! Whenever you're confronted with a photoshop/gimp-like color picker you either choose random color, or single color that's globally predefined in a palette. I also understand that very few 'casual' users maintain own palettes for consistency.

What's worse, for subsequent choices of colors, the same difficulties repeat with various level or annoyance. I know advanced users sometimes add currently picked the color to a <i>Custom Colors</i> table of the color picker, but that's all. 
(Andy covered the color swatches topic, that's a clear improvement). The table as it's present in KDE apps and other apps, is global per your OS account, so context-less, as if it was designed in hurry. It comes from old Windows software, early 90's. The context is not even at application level, and obviously not related to the project you're working on. (BTW, this is why I think the story of KDE Activities would better start at (smart) apps level not at desktop level)

I'd like to mention there are other approaches. I mean especially <b>themes</b>, explained on my blog 3 years ago:

<a href="https://blogs.kde.org/2011/12/14/fruits-css2-shared-themes">https://blogs.kde.org/2011/12/14/fruits-css2-shared-themes</a>

<img src="http://kexi-project.org/pics/blog/docthemes/theme-colors-mso.png"/>

So the themes <b>can</b> address the problem of abundance of choice. In one go you can pick a color set, and application can use it to apply defaults to your document title, heading, maybe some background. Possibilities depend on what the content is for given application.

Second thing is <b>live previews</b>, something that benefits from the power of themes. Without them you pick a color without seeing the context. Picking your selection in a 10 pixels box is not the same as observing it temporarily applied to the destination area (filling an object's backgound, setting text color, etc.). So very the same blog mentions <i>Document themes selector picking MS Office 2010 as an example</i>:

<img src="http://kexi-project.org/pics/blog/docthemes/theme-doc-mso.png"/>

You try the selection by hovering your mouse pointer and you immediately see the live preview. As engineers and they say <b>it's hard to implement</b>. That shouldn't stop us. I believe such concept can play well with <a href="https://techbase.kde.org/Projects/Usability/HIG/Layout/CommandPatterns">side context panels</a> we have already documented in the HIG.

All that, naturally depends or application, but the usefulness is beyond of the color picking. One can imagine color theme for widget style can pick the colors (and paragraph settings, font settings, by the way) from the <b>theme</b>.

For all this to work, we need developers that are willing to apply the concept to their apps. And an organized effort to avoid dozens of app-defined, incompatible theme notations/definitions floating around. Ideally, the place of favourite theme(s) is the KDE system settings. For me that's not just a proposal without a follow up, <a href="http://calligra.org/kexi">Kexi</a> will be applying it at one point.

Comments? Please share them on the <a href="https://forum.kde.org/viewtopic.php?f=285&t=123435">VDG forum</a>.