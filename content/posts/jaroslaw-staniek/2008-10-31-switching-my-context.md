---
title:   "Switching my context"
date:    2008-10-31
authors:
  - jaroslaw staniek
slug:    switching-my-context
---
The confirmed rumour at least on the IRC has it that I am in process of switching my job after 5 years at OpenOffice Polska / <a href="http://openoffice.com.pl/en/">OpenOffice Software</a>. Much of that really interesting time was devoted to <a href="http://kexi-project.org/">Kexi</a> within KOffice and quite a bit to various aspects of KDE on Windows. The switch has been planned about one year ago so is largely gradual. These KDE-related tasks have been already my spare time activities since then. My will is to invest a lot there.
<!--break-->
There are a couple of options I have now in Warsaw. Among them, there are the following types:
<ul>
<li>developer in a services company aimed at software design/development (entirely non-Qt, quite a bit of C++, a lot of Java)</li>
<li>a work for one of the main Nokia's rivals, as developer (either Symbian or Android API, so again: custom C++ or Java, and most probably entirely non-Qt)</li>
<li>freelancing, largely remote work on various projects, where in many cases I can use or propose C++/Qt or even KDE technologies; sometimes an overhead or delays are present there</li>
</ul>

And finally, a Qt/C++ developer in a company offering very popular general-purpose app based on the technology, but it is not a formal proposal so far.

Being bound to Warsaw, I say - too bad for me that Nokia's "unofficial" software shop is located in Wroclaw and not in Warsaw. Well, it's also rather interesting perspective to try different technologies after spending (now exactly) 10 years with C++/Qt since 1.41.