---
title:   "Kexi 1.1.3, and the road to KDE 4, KOffice 2"
date:    2007-06-08
authors:
  - jaroslaw staniek
slug:    kexi-113-and-road-kde-4-koffice-2
---
The long awaited <a href="http://www.koffice.org/announcements/announce-1.6.3.php">1.1.3</a> release is available. 

<a href="http://kexi-project.org/wiki/wikiview/index.php?Kexi1.1.3"><img src="http://kexi-project.org/pics/1.1.3/kexi_1.1.3-320.jpg"/></a>

These were long months when I shared my time between 1.1.x development, 2.x development (careful porting) and <a href="http://kexi-project.org/pics/1.1.3/kexi2007box.jpg">Windows version</a>.

<!--break-->

Aside from many fixes, Kexi 1.1.3 even brings <a href="http://www.koffice.org/announcements/changelog-1.6.3.php#newfeatures">new features, requested by users</a>:

<ul>
<li><i>Easy data search</i> - a new "Find" window enables easy data search for your tables, queries and forms. There are various options available like setting search direction or limiting it to a given column.</li>
<li><i>Images on printouts and preview</i> - images can be now printed and displayed on the print preview.</li>
<li><i>Users can create database templates</i>, accessible within the Startup dialog window.</li>
</ul>

Mandatory <a href="http://kexi-project.org/screenshots.html#1.1.3">1.1.3 screenshots</a> and one <a href="http://kexi-project.org/screencasts.html#1.1.3">screencast</a>.

Another news is that Kexi 2.0 is now <a href="http://kexi-project.org/wiki/wikiview/index.php?Porting%20to%20Qt4">largely ported to KDE 4</a>.
