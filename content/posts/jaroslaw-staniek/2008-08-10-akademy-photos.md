---
title:   "Akademy photos"
date:    2008-08-10
authors:
  - jaroslaw staniek
slug:    akademy-photos
---
Have some pictures? Share with us at <a href="http://techbase.kde.org/Events/Akademy/2008/Photos">http://techbase.kde.org/Events/Akademy/2008/Photos</a>...

<img src="http://kexi-project.org/pics/akademy/2008/belgium/akademy2k8_brewery.jpg"/>
<a href="http://akademy.kde.org/events/social_event.php">The social event in "Het Anker" brewery, Mechelen</a>

<img src="http://kexi-project.org/pics/akademy/2008/belgium/akademy2k8_mascot.jpg"/>
The brewery mascot or so...