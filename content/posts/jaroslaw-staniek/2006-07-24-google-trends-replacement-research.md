---
title:   "Is Google Trends a replacement for research?"
date:    2006-07-24
authors:
  - jaroslaw staniek
slug:    google-trends-replacement-research
---
Definitely not. I don't know whether Steven's <a href="http://www.linux-watch.com/news/NS5056220977.html">article</a> should be read as ironic, but it is filled with false or semi-true conclusions. Among them I found one especially funny:

<!--break-->

<i>Russia, however, is the country that has the most interest in Linux. It's followed by India -- out-sourcing anyone? -- and the Czech Republic. [..]</i>

Steven, KDE is a Czech word "where" or so, you know? :)
A few folks even remember this is mentioned in a KDE's Tip of the Day...

That said, we may be proud of "KDE" being mentioned in the national <a href="http://en.wikipedia.org/wiki/Kde_domov_m%C5%AFj">anthem</a> of the Czech Republic:
<pre>
    <b>Kde</b> domov můj, <b>kde</b> domov můj?
    Voda hu