---
title:   "Easter Egg"
date:    2006-04-01
authors:
  - jaroslaw staniek
slug:    easter-egg
---
There's hidden easter egg in recent Kexi SVN version. 

<!--break-->

Just create a new table design and name it "sudoku" and you'll get a nice game for free...

<img src="http://kexi-project.org/pics/easter_eggs/kexi_sudoku.png">

Thanks to ksudoku author for the code ;)
