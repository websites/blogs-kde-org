---
title:   "Q-Fridges - we're hiring!"
date:    2012-01-25
authors:
  - jaroslaw staniek
slug:    q-fridges-were-hiring
---
There are job offers floating sporadically on planetkde so I guess this one would fit too especially that there are many related technologies involved.

You may remember my story about <a href="http://blogs.kde.org/node/4474">some crazy Qt device</a>. Now there is apparent expansion both vertically and to other types of <a href="http://www.youtube.com/embed/ZAhiHY5KtXk">high-end devices</a>, so:

<center><a href="http://blog.kde.org.pl/archiwum/267/Q-Lodowki"><img src="http://blog.kde.org.pl/js/qt-fridge-jobs.png"/></a></center>

This offer is for permanent jobs in Warsaw office, rather for Polish speakers. More on my <a href="http://blog.kde.org.pl/archiwum/267/Q-Lodowki">Polish blog</a>.
<!--break-->
