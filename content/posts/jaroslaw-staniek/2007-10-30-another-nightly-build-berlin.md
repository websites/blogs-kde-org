---
title:   "Another nightly build from Berlin"
date:    2007-10-30
authors:
  - jaroslaw staniek
slug:    another-nightly-build-berlin
---
How's possible that most of my nightly builds I mention here, come from Berlin so far? Tasty food ensured by <a href="http://trolltech.com/">Trolltech</a> or <a href="http://kdab.net">KDAB</a>? <a href="http://de.wikipedia.org/wiki/Weizenbier">Weißbier</a>?

<img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/koffice2_approach_sm.png"><br/>(gfx by <a href="http://mkbart.blogspot.com/">mkbart</a>, based on <a href="http://www.oxygen-icons.org/">Oxygen</a> icons)

In Berlin KDAB headquarters we have found all facilities needed for <a href="http://dot.kde.org/1193705373/">hacking</a>. Awesome!

Anyway, here come another two KOffice apps running on Windows. Read on for tasty screenshots >>>
<!--break-->

<img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kpresenter_kchart_taskbar_win.png">

When Thorsten Zachmann, already half-Pole as his wife comes from my country,  asked for KPresenter on Windows, I couldn't oppose, especially that the government gave us <a href="http://en.wikipedia.org/wiki/Image:Daylightsavings.jpg">extra</a> hour on Saturday night :)

<a href="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kpresenter_startup_win.png"><img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kpresenter_startup_win_sm.png"></a><br/>
KPresenter's startup on Windows (click to enlarge)</br>

<a href="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kpresenter_win.png"><img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kpresenter_win_sm.png"></a><br/>
KPresenter on Windows with various shapes (click to enlarge)</br>

It turned out that Johannes Simon, hacking into KChart, among other guys, is relatively young and apparently - tireless and able to ask and listen, what I value in the long term. 

<a href="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_win.png"><img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_win_sm.png"></a><br/>
KChart with full <a href="http://en.wikipedia.org/wiki/Anti-aliasing">antialiasing</a>, also accepting 3rd-party shapes like freehand vector lines (click to enlarge)</br>

<a href="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_about_win.png"><img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_about_win_sm.png"></a><br/>
(click to enlarge)</br>

But not only that encouraged me to present KChart on Windows, so more people can discover this software based on excellent KDAB's <a href="http://kdab.net/?page=products&sub=kdchart">contribution</a>. I decided to give a hint what KOffice integration for Kexi means in my opinion.

<a href="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_kexi.png"><img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/kchart_kexi_sm.png" align="center" ></a><br/>
KChart-based plugin for Kexi displaying information from live data sources; work in progress (click to enlarge)</br>

<img src="http://kexi-project.org/pics/2.0/alpha4/kchart_kpresenter/upyo_sm.jpg" hspace="6" vspace="4" align="right" alt="A wall next to the KDAB's office" title="A wall next to the KDAB's office"/><br/>
<br><br>For novices, I think I should repeat this: (unlike in OpenOffice.org), porting means for us making the _single_ source code portable and behaving more natively across Linux/Unix/MacOSX/Windows/who-knows-what-else. 

There is practically _no_: (a) #ifdefs, (b) custom file of Windows code, (c) any Windows resource files, (d) Windows-style Makefiles or msvc projects. The three former features are the reality thanks to <a href="http://trolltech.com/products/qt">Qt</a> and <a href="http://techbase.kde.org/Development">KDE</a> libraries, and the last thanks to flexible <a href="http://cmake.org">CMake</a> build system.
