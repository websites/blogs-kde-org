---
title:   "KexiDB, ODBC and Drivers in General"
date:    2004-02-05
authors:
  - jaroslaw staniek
slug:    kexidb-odbc-and-drivers-general
---
Anyone who is interested in database systems integration may know ODBC can be usable.

Currently, development of KexiDB integration with ODBC functionality is started. So, I've updated <a href="http://www.kexi-project.org/wiki/wikiview">Kexi Developers Wiki Pages</a> with a <a href="http://www.kexi-project.org/wiki/wikiview/index.php?ODBCDriver">small research on ODBC</a> in <a href="http://www.kexi-project.org/wiki/wikiview/index.php?KexiDBDrivers">KexiDB Drivers</a> context.

Another thing I've contributed, as it's the good time for this already, are <a href="http://www.kexi-project.org/wiki/wikiview/index.php?KexiDBDrivers_General">General Issues For KexiDB Driver Developers</a>.


Hope this could help in the development and result in a discussion on the topic.


