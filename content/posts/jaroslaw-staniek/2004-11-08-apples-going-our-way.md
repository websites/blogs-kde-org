---
title:   "Apple's going our way"
date:    2004-11-08
authors:
  - jaroslaw staniek
slug:    apples-going-our-way
---
..with its <a href="http://www.apple.com/macosx/tiger/spotlighttech.html">Spotlight&reg;</a>.

See 'SQLite Support' and 'Core Data Framework' here: http://www.appleinsider.com/article.php?id=593. That's not entire KexiDB but always looks like interesting competition.