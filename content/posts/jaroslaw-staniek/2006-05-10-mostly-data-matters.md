---
title:   "Mostly the data matters!"
date:    2006-05-10
authors:
  - jaroslaw staniek
slug:    mostly-data-matters
---
Some would even say "only the apps and data matter". Why? The next iteration of the desktop itself may be pretty, but it is just...desktop (yeah, more feature-rich than competition).

<!--break-->

Saying this I'd like to ask you (if you're able to do this) to vote for <a href="http://code.google.com/soc/">Google Summer of Code 2006</a> proposals related primarily to <a href="http://en.wikipedia.org/wiki/Vertical_application">vertical applications</a>. To do so, can use <a href="http://code.google.com/soc/kde/app.html"> the GSoC interface</a> (register <a href="http://code.google.com/soc/mentor_step1.html">here</a>, then go to Summer of Code > Mentor Home > Application List).

<img src="http://img61.imageshack.us/img61/8056/p8bm.jpg" align="right" alt="Photo by Jakub Kubica">You may be not interested in databases, but what are these applications for, is the interoperability, KDE's maturity & completeness, and finally, KDE for the enterprise and small businesses. 
I believe the latter have enough of task bar of K Menu variants. If you have plain fun with something what they call "bells and whistles", no problem, it's up to you. But if you ask the users, the lack of higher-level integration can pull them off of KDE back to our competitors.

While I apperciate the work put into many of the (about 200) proposals for GSoc 2006, I'd like to ask for supporting KOffice features, many will likely to find their way to KDE4 core technologies. In addition to 
<ul>
<li>"Ariya Hidayat: Microsoft Excel export filter for KOffice", 
<li>"Gábor Lehel: Writing a widget (View/Delegate) for KOffice 2.0, for displaying layers, pages, and slides, based on Qt4's Model/View architecture, to be used throughout KOffice", 
<li>"Thomas Schaap: Encryption of documents in KOffice", 
<li>"Robert Knight: Improve performance when handling large documents in KSpread" 
<li>(and a few more)
</ul>
...there are also high level KDE data/database handling features I decided to help with by mentoring. These currently used in Kexi and soon in KOffice 2.0, then in KDE 4, for many data-oriented applications (as easily as KIO):

<ul>
<li>"Kexi - Paradox & HSQL Access" by Joseph Wenninger of Kate fame, Kexi co-founder. Joseph writes:
<ul>
<li><i>"a data import for Paradox database files based on the GPL licensed pxlib and an external helper application (to avoid license issues) or perhaps an entirely new implementation based on the paradox file specification available through many sources on the net </i>
<li><i>a data import for OpenOffice.org Base HSQL schemas and table data and</i>
<li><i>a backend driver that enables kexi to store its data in a HSQL database instead of an sqlite db directly.</i>
<i>Which means it stores schemas and data in the HSQL database, but still uses the kexi internal format for forms, queries, ... so they will still not work in OOo Base, but it will help further development into that direction."</i>
</ul>
<li>"DBase Migration Plugin for Kexi" by Jonathon Manning; who writes: 
<i>"Kexi is excellent, but other database formats are in use, in particular DBase. To this end, the Kexi developers have requested a plugin that provides import support for DBase databases.
Once the migration plugin for DBase databases is complete, I would then code a simple read-only backend plugin to enable users to more directly work with their databases."</i>
<li>"Kexi Web Forms" by Jacek Migdal (http://jacek.migdal.pl/gsoc/); this is the well defined topic Jacek worked with me during last days. We had even two small Kexi meetings in Poland devoted to this rather large task.  The plan is to also reuse and extend the KDE Personal Web Server (KPF), to make usage of the Web Forms as easy as possible!
</ul>

As I work on Kexi full-time, I believe there are good chances that the projects will be implemented successfully, and your be votes will not be lost. Again, you may indifferent to "databases" topic, but you cannot be indifferent to "data" topic. And the above applications are largely about data.

