---
title:   "KMediaWiki ;)"
date:    2007-05-15
authors:
  - jaroslaw staniek
slug:    kmediawiki
---
Thanks to help from danimo, I've grabbed <a href="http://techbase.kde.org/">techbase</a>'s nice <a href="http://mediawiki.org/">MediaWiki</a> Oxygen web skin and turned it into a regular KDE web/news site skin in Poland (the result: <a href="http://kde.org.pl">kde.org.pl</a>, <a href="http://dot.kde.org/1179085340/">dot article</a>). What means that unlike on techbase, which is aimed at more collective editing, kde.org.pl has left hand menu as well wiki tools are accessible and visible only for logged-in users. So it is more like kde.org.

<!--break-->

The editorial staff is not big so I used extensions like <a href="http://www.mediawiki.org/wiki/Extension:DynamicPageList">DynamicPageList</a> so they can save time by inserting automatically generated list of pages of given type/subtype (either sorted alphabetically or by creation date), e.g. see <a href="http://kde.org.pl/Nowo%C5%9Bci/KDE_4/Droga_do_KDE_4">Droga do KDE 4 (i.e. Road to KDE 4)</a>.

There are also 
<ul>
<li>fixes for IE (danimo, ping me for these before I really forget what to do)
<li>my own <a href="http://en.wikipedia.org/wiki/Breadcrumb_(navigation)">breadcrumb navigation</a> style (greatly reducing clutter in the title, IMHO also _well_ suitable for techbase)
<li><a href="http://jimbojw.com/wiki/index.php?title=WikiArticleFeeds_Extension">rss/atom feeds system</a> that you can configure as easily as is wiki editing (a bit changed to show the same feed on every page)
<li>unlike in techbase, by default images are not clickable for not-loggedin users, to avoid confusion (users no longer end up on http://techbase.kde.org/Image:Action_launch.svg page when what they really wanted is to visit http://techbase.kde.org/Getting_Started); having available &lt;a href..&gt; links in the wiki also helps to create explicit links to external places when [[ ]] does not work well
<li><a href="http://www.huddletogether.com/projects/lightbox/">Lightbox JS</a> code plugged in an insanely simple and effective way -- every thumbnail you insert in the wiki markup will automatically be plugged to lightbox; e.g. see thumbnails on this <a href="http://kde.org.pl/Nowo%C5%9Bci/KDE_4/Droga_do_KDE_4/Dolphin_i_Konqueror">page</a>
</ul>

<img src="http://kexi-project.org/pics/other/kde.org.pl.png" align="right"/>The final word is a note: if you want to run similar Oxygen-compatible KDE web site for your country, either by replacing the older one or starting a new one, let me know you want the templates and some mentoring. Most languages are supported. But remember you still need a few folks for editorial staff -- things won't happen automatically (translation, local articles, administration).
