---
title:   "Kexi 3 object icons"
date:    2015-11-10
authors:
  - jaroslaw staniek
slug:    kexi-3-object-icons
---
<b>Intro or what are Kexi objects?</b>

Kexi objects are the top-level entities of type Table, Query, Form, Report, Script or Macro. They are compared to perhaps separate document types except of course they are not physical files (in Kexi nothing touches physical files except the database that handles the actual storage but this is rather an implementation detail).

The latter two objects aren't in use officially: Script is experimental and subject to rework; Macros (MS Access style -- <a href="http://kexi-project.org/wiki/wikiview/index.php@Macros.html">old dev docs</a>) are only planned.

If you're new to the RAD world (or don't care), for info on what these types of objects are google for <a href="http://allenbrowne.com/casu-01.html">MS Access objects</a>. I found out that many of the original concepts and naming make sense with the exception of Modules -- Kexi tends to use Scripts term instead. I mean, make sense in a way that there's no other at least equivalent metaphor for given object.

<img src="http://kexi-project.org/pics/3.0/ms-access-icons-orig.png">

<br/>
&nbsp;
<p>
<b>The Art</b>

Weeks ago I decided to try to move Kexi 3 away from the oxygen style for the object icons. I am even considering this a work donation because it cost me quite a bit and it's not actual code/design development. Or call it that I give something back for the gorgeous Breeze project.

<img src="http://kexi-project.org/pics/3.0/kexi-3.0-new-object-icons_icons_only.png">

The new icons look as follows in Kexi.

<img src="http://kexi-project.org/pics/3.0/kexi-3.0-new-object-icons.png">

All of them in the Project Navigator:

<img src="http://kexi-project.org/pics/3.0/kexi-3.0-new-object-icons2.png">

Table, Query, Form, Report, Script, Macro zoomed in:

<img src="http://kexi-project.org/pics/3.0/kexi-3.0-new-icons-zoom.png">

Consider this a mature beta. Everything is made in SVG with 3 detail-variants (16, 22, 32 px). This is the same approach that I followed for the flat Breeze-friendly <a href="http://kexi-project.org/pics/2.9/kexi-2.9-breeze-icons.png">database icons</a> that came to the Kexi 2.9's Welcome screen. The icons can't be sharper!

For reference, these are the oxygen also ones authored by me (years ago):

<img src="http://kexi-project.org/pics/3.0/kexi-3.0-new-object-icons-old.png">

<br/>
&nbsp;
<p>
<b>Colors and Concepts</b>
Each type has own colors --  I think there shall be limits where we go with the flat aesthetics. We can follow this rule a bit more: when presenting various form-related symbols, blue color can be used. Since there are just 5 (well, actually 4) colors, users wouldn't be lost. Color-coding can work.

Wrapping up -- visually there's a lot of similarities to how the object icons were presented first around circa 1990 in MS Access 2.x :) These are just good (logical) concepts present on the market until now with many business/database-related apps following.
