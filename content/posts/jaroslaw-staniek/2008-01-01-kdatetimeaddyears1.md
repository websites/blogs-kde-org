---
title:   "KDateTime::addYears(1)"
date:    2008-01-01
authors:
  - jaroslaw staniek
slug:    kdatetimeaddyears1
---
This xmas I was given a watch. It's pretty geeky one LCD <a href="http://gizmodo.com/gadgets/gadgets/lexon-e8-watch-reinvented-time-191290.php">Lexon E8</a>, but for me still easier to accept than real binary counters. 
<!--break-->
<img src="http://kexi-project.org/pics/blog/watch/1.jpg"/>

If we're at watches, <a href="http://www.piggz.co.uk/">Adam Pigg</a> apparently got something like this <a href="http://cgi.ebay.com/PIMP-Style-LED-watch-type-Dual-black-blue-binary-BCD_W0QQitemZ140186313216QQihZ004QQcategoryZ31387QQtcZphotoQQcmdZViewItem">hardcore LED BCD watch</a>. In terms of usability I prefer Lexon's LCD to (otherwise cool) LED display, because in the latter case you need to use your second hand to press a button to turn the power-hungry diodes on for a few seconds to read the time.

<a href="http://kexi-project.org/pics/blog/watch/2.jpg"><img src="http://kexi-project.org/pics/blog/watch/2_.jpg"/></a>