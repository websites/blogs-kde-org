---
title:   "Slashing startup time, again"
date:    2006-07-26
authors:
  - jaroslaw staniek
slug:    slashing-startup-time-again
---
In a thread filled with complaints about problems with (sw)suspend in Linux I found a link to <a href="http://cryopid.berlios.de/">CryoPID - A Process Freezer for Linux</a>.

If you know this, forgive me. Let's look at slashing KDE apps startup time. Again. I am referring to topics covered in this <a href="blogs.kde.org/node/1538">thread</a>. 

What's (relatively) new?
<!--break-->
From the project page:

<i>"CryoPID allows you to capture the state of a running process in Linux and save it to a file. This file can then be used to resume the process later on, either after a reboot or even on another machine."</i>

Example improvement for KMail startup time could be obvious (but may be a nonsense for this app after moving to Akonadi's storage backend, but who knows). CryoPID authors mention a case for other mail program:

<i>"mutt - You have a huge inbox with 30000 emails that you don't want to have to close mutt and reopen to check your email. You also don't want to leave it running in the interests of memory consumption. Ensure you sync your mutt before saving it to a file, and the just resume it later. Et voila."</i>

Other use case could be to implement saving sessions for Konqueror (with all tabs and contents) as it's available for Mozilla with the Tab Extension. Using the CryoPID there is even no need to re-render the web pages, although these can be outdated after the resume...

RFC...
