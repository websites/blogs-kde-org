---
title:   "CSV, eventually"
date:    2005-05-18
authors:
  - jaroslaw staniek
slug:    csv-eventually
---
Last month I've "stolen" code from KSpread's CSV import dialog, and now it's tweaked quite much:
<ul>
<li> support for setting primary key (including autodetection) </li>
<li> support for 'first row contains column names' flag (including autodetection)</li>
<li>column types are autodetected</li>
</ul>

<!--break-->
<br><br>
Import is performed to a new table, within a single transaction, and (what's usual for Kexi) in a database-engine independent way, thanks to <a href="http://www.kexi-project.org/docs/cvs-api/html/namespaceKexiDB.html">KexiDB</a> layer.
<br><br>
<img src="http://iidea.pl/~js/kexi/shots/1.0/csv_import2.png">
<br><br>
As an extension of above, support for pasting clipboard contents will be added.
<br><br>

Next step for me will be stop conding and help my friends with design/discussions, to be sure everybody in the Team knows the plan for following two months or so.

