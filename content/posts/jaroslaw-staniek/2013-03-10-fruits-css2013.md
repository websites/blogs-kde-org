---
title:   "Fruits of CSS2013"
date:    2013-03-10
authors:
  - jaroslaw staniek
slug:    fruits-css2013
---
Putting unexpected visions of space tourists aside, now for something completely different. This was a busy weekend with Calligra Suite Sprint 2013 which despite of different timezone fully dominated Essen and Bangalore. More about that <a href="http://sujithh.info/2013/03/calligra-sprint-2013-day-1-bangalore/">here</a>, <a href="http://frinring.wordpress.com/2013/03/08/calligra-spring-2013-sprint-started">here</a>, <a href="http://lukast.mediablog.sk/log/?p=444">here</a>, <a href="http://sujithh.info/2013/03/calligra-sprint-2013-day-2-bangalore/">here</a>, <a href="http://lukast.mediablog.sk/log/?p=458">here</a> and <a href="http://sujithh.info/2013/03/calligra-sprint-2013-day-3-bangalore">here</a>. You can find the full agenda on <a href="http://community.kde.org/Calligra/Meetings/Spring_2013_Sprint/Agenda">the KDE Community Wiki</a>.

Lords of the ring like me were able to join at least remotely through various media. So here's a list of my limited activities.

<br><br><b>A Mockup for Calligra Words Look & Feel Rework</b>

<a href="http://blogs.kde.org/2011/04/06/kde-5-menu">The Modern GUI and Startup screen</a> is already established in Kexi, people <a href="http://www.calligra.org/news/first-beta-version-of-the-calligra-suite/attachment/kexi-2-4-startup-new-project/">know it and use saving many mouse clicks</a>. I took some time to present a follow up, mockup of how Calligra Words word processor would look. The topic was iterated before at the <a href="http://community.kde.org/Calligra/Usability_and_UX/Common/Web_Browser-like_UX_for_Main_Window">previous sprint</a>.

Calligra Words before:<br>
<a href="http://kexi-project.org/pics/calligra/mockups/original_style.png"><img src="http://kexi-project.org/pics/calligra/mockups/original_style_sm.jpg"></a>
<!--break-->

Calligra Words after:<br>

<a href="http://kexi-project.org/pics/calligra/mockups/web_style.png"><img src="http://kexi-project.org/pics/calligra/mockups/web_style_sm.jpg"></a>

You can read about <a href="http://community.kde.org/Calligra/Usability_and_UX/Common/Web_Browser-like_UX_for_Main_Window/Modern_GUI_of_Calligra_Words">the story behind the look and inspirations that can influence what I'll try to do</a>. Obviously thanks to the Qt Quick taking over the overall GUI concepts, our users can expect the apps can be run will pleasure. Yes, it's time for the desktop.

<br><br><b>Calligra Mail Merge</b>

The Kexi virtual BoF turned out to be an IRC meeting between me and Smit Patel, a Calligra dev who is successfully collaborating with me already on the <a href="http://community.kde.org/Kexi/Integration/Bibliographic_Database">Bibliographic Database</a> project for Calligra Words. It takes advantage of rather awesome desktop database capabilities that Kexi offers to other apps.

This time we covered fully fledged <a href="http://en.wikipedia.org/wiki/Mail_merge">Mail merge</a> for Calligra, an initiative that was planned long ago. It should be as easy as possible (no tinkering with database/data source as in LibreOffice) and visible in Words with some optional visibility in Kexi for premium integration. Like the Bibliographic Database, Mail Merge reuses certain features of Kexi so that provokes my attention. More on <a href="http://community.kde.org/Kexi/Integration/Mail_Merge">the Mail Merge Design wiki page</a>.

<br>
<b>Finally a small announcement about interesting opportunity: Karbon, Plan and Braindump apps from Calligra are looking for new Maintainers. That's rare opportunity for you!</b>

<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.calligra.org/"><img src="http://www.calligra.org/wp-content/themes/calligra/images/calligra-logo-200.png"></a>