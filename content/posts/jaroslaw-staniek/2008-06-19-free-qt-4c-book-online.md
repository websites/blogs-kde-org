---
title:   "Free Qt 4/C++ book online"
date:    2008-06-19
authors:
  - jaroslaw staniek
slug:    free-qt-4c-book-online
---
Full text of valuable book <a href="http://cartan.cas.suffolk.edu/moin/OopDocbookWiki">"An Introduction to Design Patterns in C++ with Qt 4"</a> by Alan and Paul Ezust (Suffolk University) is now available online at <a href="http://cartan.cas.suffolk.edu/oopdocbook/opensource/">http://cartan.cas.suffolk.edu/oopdocbook/opensource/</a> under <a href="http://www.opencontent.org/openpub/">OPL</a> license. 

<a href="http://cartan.cas.suffolk.edu/oopdocbook/opensource/"><img src="https://blogs.kde.org/files/images//qt_patterns_sm.png"></a>

The authors also provided <a href="http://oop.mcs.suffolk.edu/html/">slides</a> suitable for courses and exercises.

<b>C++ taught "the Qt Way"!</b>

