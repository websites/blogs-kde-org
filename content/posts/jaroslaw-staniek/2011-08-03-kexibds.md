---
title:   "Kexi@BDS"
date:    2011-08-03
authors:
  - jaroslaw staniek
slug:    kexibds
---
<a href="https://www.desktopsummit.org/"><img src="https://www.desktopsummit.org/sites/www.desktopsummit.org/files/DS2011banner.png"></a>

In addition to usual chatting about whether and how is Kexi utilised under KDE I would like to hear about others, what in the context of the Desktop Summit means GNOME users.
<!--break-->
This year you can meet one more Kexi developer and actually new one: Shreya Pandit (of <a href="http://blogs.kde.org/node/4448">Kexi Web Widget</a> fame) will attend the Summit. She plans to give a lightning talk explaining how she joined the Kexi project and why it is interesting hacking experience.

We'll also welcome anyone to join us on Wednesday 09.00..10.00 <a href="http://wiki.desktopsummit.org/Workshops_%26_BoFs/Schedule#Wednesday_10.08.2011">Ad-hoc session</a> in Room 1.404/1 to hear more about Kexi as application, as a project, and to discuss future milestones.

<img src="http://farm1.static.flickr.com/25/61844076_84f137add9.jpg"><br><a href="http://www.flickr.com/photos/good_day/61844076/">by-nc-nd</a>