---
title:   "KDE laptops in \"Polish Walmart\""
date:    2008-06-05
authors:
  - jaroslaw staniek
slug:    kde-laptops-polish-walmart
---
Just got a (<a href="http://www.biedronka.pl/img/promocja/3NextWeek/2008.06.16/s13_b.gif">link</a>) to offer of the "Polish Walmart" called Biedronka (Ladybird) - cheap (~&euro;290) laptop with Kubuntu. It has been customized for Polish users, e.g. contains <a href="http://www.kadu.net">Kadu</a>, instant messenger supporting locally popular proprietary protocol.
