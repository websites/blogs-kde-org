---
title:   "Fruits of CSS2: Kexi for KDE Contributors"
date:    2011-11-20
authors:
  - jaroslaw staniek
slug:    fruits-css2-kexi-kde-contributors
---
Over the years, most if not all my presentations on Kexi were aimed at users or power users. When preparing for the Calligra Sprint I thought it would make sense to approach the Calligra contributors with a small update on what are goals and ideas embedded in this specific office application. I hope you, the KDE contributors, would benefit a bit too.

<a href="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-kexi-intro-for-devs.pdf"><img src="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-kexi-intro-for-devs.png"></a><br/><a href="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-kexi-intro-for-devs.pdf">Click to open (PDF)</a>

We, at the project believe the software foundations that Kexi is built on are also increasingly more interesting for Qt-only developers or those that expect minimal dependency on KDE libraries. This becomes possible while modularization advances, in particular while the <a href="http://community.kde.org/Predicate">Predicate</a> database creation/connectivity library matures.

Is there anything left unanswered? Please leave your questions in the comments section below.

PS: Next time on eating our own dog food in Calligra.

<center><a href="http://www.calligra.org"><img src="http://kexi-project.org/pics/2.4/ads/calligra-2.4-beta-500.png"></a></center>
