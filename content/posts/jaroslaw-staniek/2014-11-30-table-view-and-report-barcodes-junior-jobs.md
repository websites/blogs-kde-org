---
title:   "Table View and Report Barcodes junior jobs"
date:    2014-11-30
authors:
  - jaroslaw staniek
slug:    table-view-and-report-barcodes-junior-jobs
---
Kexi has improved quite a bit since the last time, especially in <a href="https://www.calligra.org/kexi/attachment/kexi-2-2-report-design/">Reports</a>. We're close to supplementary 2.8.7 release within Calligra, then 2.9 will follow and Qt5/KF5-based 3.0 with a shiny mask.

<b>We're about to reach the point where it's very very hard to find comparable Free Software reporting tool in terms of usability.</b> If you don't know why reporting software may be useful, try to generate 50 or 500 pages PDF out of structured data with just mouse clicks and no programming. Get the report file generated in 2 seconds or less. Or print the report directly without creating intermediate PDF or ODF or HTML files that will pollute your computer... It's data that matters.
<!--break-->
All this is only possible because smart people join the effort. Today two new Junior Jobs appeared for software engineers: for Table View and Report Barcodes. Don't be shy :)

1. <a href="https://community.kde.org/Kexi/Junior_Jobs/Port_&quot;Interleaved_2_of_5&quot;_barcodes_support_from_OpenRPT">Port "Interleaved 2 of 5" barcodes support from OpenRPT</a>

2. <a href="https://community.kde.org/Kexi/Junior_Jobs/Add_&quot;Open_as_Query&quot;_action_to_Table_local_menu">Add "Open as Query" action to Table local menu</a>

PS: Reminder, there are still many <a href="https://community.kde.org/Kexi/Junior_Jobs/Small_report_improvements">smaller jobs</a> for Kexi Reports. More Junior Jobs can be spotted <a href="https://community.kde.org/Kexi/Junior_Jobs/">here</a>.

PS2: And did you know the Reports contains a reusable reporting library that would be public once <a href="https://community.kde.org/Kexi/Porting_to_Qt%26KF_5">ported to Qt 5</a>?

