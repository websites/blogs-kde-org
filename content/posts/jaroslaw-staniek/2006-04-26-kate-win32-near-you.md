---
title:   "Kate for Win32 is Near You"
date:    2006-04-26
authors:
  - jaroslaw staniek
slug:    kate-win32-near-you
---
Spring suddenly arrived also here in Poland. Not bad pretext to finally replace rather nice CRT with relatively awesome LCD. Too bad that win2k has no subpixel hinting.

In software department, this week I got a message from Ralf Habacker who's persistently polishing kdelibs<b>4</b>/win32:

<pre>Just like to inform you that the kdelibs/katetest application 
could be started. Several areas are working now, but some need fixing:

- dcopserver isn't able to handle more then one connection - kdeinit/klauncher
  has to be ported 
- kioslaves should be able to load 
- I saw on linux that the katetest file open dialog does not work also, so there 
  may be other problems in kdelibs
</pre>

<a href="http://img274.imageshack.us/my.php?image=katewin326ws.jpg"><img src="http://img247.imageshack.us/img247/7269/katewin32sm7wo.jpg" border="0"></a>

(click to enlarge)


