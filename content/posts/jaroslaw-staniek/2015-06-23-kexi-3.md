---
title:   "Kexi 3!"
date:    2015-06-23
authors:
  - jaroslaw staniek
slug:    kexi-3
---
I am happy to say that Pre-Alpha edition of ‪Kexi‬ 3.0 runs nicely already after like 3 weeks of porting! Especially its tabular view work out of the box for me after fixing the last compilation error with zero fixes needed in the functionality.

<a href="http://kexi-project.org/pics/3.0/kexi-3.0-pre-alpha.png"><img src="http://kexi-project.org/pics/3.0/kexi-3.0-pre-alpha_sm.png"></a>

Porting for the porting itself would not be an interesting goal. As you see on the picture Kexi 3.0 uses KDE Frameworks 5. With Qt 5.4 it's More compact, faster, closer to mobile devices. And a 1st class citizen called Windows version has to come to reach the wide audience.

Under the mask Kexi uses a new business apps-oriented <b><a href="https://community.kde.org/KDb">KDb framework</a></b> (kde:kdb - try it, LGPL), formely called Predicate and KexiDB. KDb is a database connectivity and creation framework, consisted of a general-purpose C++ Qt library and set of plugins delivering support for various database vendors. More about it in following posts maybe with code snippets that would explain why would you want to do the same in a few mouse clicks in a visual designer and 200 lines of code instead of inventing own data abstractions in a plain source code.

A big fraction of the porting effort went into KDb, KReport and KProperty.

<b><a href="https://community.kde.org/KReport">KReport</a></b> (kde:kreport repository, LGPL) is a framework for the creation and generation of reports in multiple formats. Think of a SAP Crystal Reports or MS Access reports but Qt friendly and extensible and fully FOSS. So far it was a gem hidden in Kexi code, long ago forked from the OpenRPT.

<b><a href="https://community.kde.org/KProperty">KProperty</a></b> (kde:kproperty repository, LGPL) is a property editing framework with editor widget similar to what is known from Qt Designer. It has some candies over competitors of the same sort. I found out that developers often use a traditional list box with Add/Delete/Edit buttons instead of an inline property editor widget. Of course they do if this so far was the fastest path to get things done.

The biggest FOSS user of KDb is Kexi at the moment and KProperty and KReport are already used by Kexi and Calligra Plan. All three repos are  very KF5-like so you'd feel at home. None of the code of the libs and Kexi actually uses kdelibs4support. Note: KReport's API will cleaned up after Calligra 3.0 release but the lib is so high-level one that porting is a relatively quick task. If you're interested in using any of the libs at this pre-release stage, we at Calligra are truly interested in knowing your needs and opinions.

The source code of Kexi 3 itself (LGPL) sits in a kexi-frameworks{N}-staniek branch of the kde:calligra repository where so far N=6, and will increase soon as I rebase the mainline. Common building shared for entire Calligra 3 is explaned at <a href="https://community.kde.org/Calligra/Building/3">https://community.kde.org/Calligra/Building/3</a> (work in progress). To configure, make sure required dependencies are met and type 'cmake -DPRODUCTSET=kexi -DCMAKE_INSTALL_PREFIX=~/dev/inst5 -DCMAKE_BUILD_TYPE=Debug path/to/source'. PRODUCTSET=kexi picks Kexi only; if you want more of Calligra, adjust it to your needs.
