---
title:   "Database-aware slider widget"
date:    2006-09-11
authors:
  - jaroslaw staniek
slug:    database-aware-slider-widget
---
Today something more lightweight than usually.

New database form widget has arrived: slider has been developed for Kexi, as a tutorial for 
the polish edition of <a href="http://www.sdjournal.org/pl/">Software Developer's Journal</a>. The widget is usable when data is restricted to a given interval, and in the tutoral it is provided witin a custom widget factory (i.e. an extension, plugin) to show how to implement such thing and let Kexi find it.

<!--break-->

&rarr; <a href="http://kexi-project.org/pics/1.1/slider/">Screenshots of a database providing temperature and humidity records</a>.

The article also contains second tutorial: "How to implement a command-line app for exporting table or query data to Fixed-Width Text format in 'a few' lines of code". No single backend-dependent line of code is included, so you can export from SQLite, MySQL and PostgreSQL so far.

The source code will be probably included in Kexi 2.0. I plan to "open" the tutorial and translate it to english as soon as the publisher allows that.
