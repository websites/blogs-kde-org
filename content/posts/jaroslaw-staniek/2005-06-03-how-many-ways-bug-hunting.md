---
title:   "How many ways for bug hunting?"
date:    2005-06-03
authors:
  - jaroslaw staniek
slug:    how-many-ways-bug-hunting
---
I couldn't complain about excessive number of bug reports for Kexi. Yes, writing 43252352nd text editor, drawing app or a frontend for media player can be more cool, especially if somebody's coding session is just <i>for fun</i> after boring day at {work|school}.

It's not that there're missing bugs/TODO lists in the Kexi project. We've even tend to use our internal bugs.kexi-project.org engine to avoid submitting too hermetical/internal bugs/wishes to bugs.kde.org. No surprise, most of TODOs are coming to live during development, and this became so frequent that somebody can consider there're enough tasks for a few busy years. It's enough to take a look at the dirty but famous <a href="http://webcvs.kde.org/koffice/kexi/doc/dev/CHANGELOG-Kexi-js?view=markup">CHANGELOG-Kexi-js</a> beast :).

Anyway, this time I realized a second, underestimated way for founding a bug: doing so while updating user docs. Nothing new, but I needed to mention this, again. Most of you are probably aware of the fact, but it's easy to be so sure there're (almost) no extremely trivial, unknown bugs when releasing a stable version. In my case, while writing about rather trivial database form-related tasks, I tried to resize application's main window (to take a screen shot) and resize-related bug popped up immediately. (I am using most of my apps in a maximized state :) ). So note that if your app allows for many different states and configuration options, it will probably have such "features".

Another typical bug had been found when I tried to load my old example database, the one I am referring to in the user docs. I tend to update it for every major release. Kexi database form's behaviour was weird here because upper case letters existed in table field names of my database schema, what required a quick fix in schema-data-loading code. Funny, because any recently created database schema wouldn't cause problem like that. So, the conclusion: it's pretty good to keep data created in ancient times (hmm, unless we have really so many testers who runs multiple software versions).
