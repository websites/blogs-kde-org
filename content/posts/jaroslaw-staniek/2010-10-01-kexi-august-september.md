---
title:   "Kexi in August & September"
date:    2010-10-01
authors:
  - jaroslaw staniek
slug:    kexi-august-september
---
Kexi moves towards the new 2.3 release within the KOffice family.

<ul>
<li><a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kexi&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist&emailtype1=substring&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&known_name=Kexi+wishes&query_based_on=Kexi+wishes&field0-0-0=noop&type0-0-0=noop&value0-0-0=">The list of current wishes for Kexi</a>. Add yours! There is no easier way to contribute!</li>
<li><a href="http://kde-files.org/content/show.php?content=129683">Kexi Scripting Example for JavaScript - reading external data</a>.</li>
<li>2.3 Beta 1 released, featuring movable tabs, completely new project navigator compliant with the KDE 4 style, improved look of the CSV Export dialog, reintroduction of Widgets Tree pane of the Forms Designer, and more. List of changes changes available on <a href="http://www.koffice.org/news/koffice-2-3-beta-1-changelog/#Kexi">koffice.org</a>.</li>
<li><a href="http://blogs.kde.org/node/4327">Notes on further uncluttering style in Kexi</a>. Small but pleasant improvements.</li>
<li><a href="http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.3/Feature_Plan#Kexi">All planned features and improvements delivered</a>. Ready for <a href="http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.3/Release_Plan">2.3 release</a>.</li>
</ul>

<br/>
<br/>

<img src="http://kexi-project.org/pics/2.3/0/kexi-2.3-widgets-tree_sm.png"/>
<!--break-->
