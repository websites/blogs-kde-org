---
title:   "Calligra Suite"
date:    2010-12-08
authors:
  - jaroslaw staniek
slug:    calligra-suite
---
<a href="http://calligra-suite.org">Calligra Suite</a> project is up!

The roots of the name are in <a href="http://en.wikipedia.org/wiki/Calligraphy">Calligraphy</a>. Announcement can be read at the <a href="http://dot.kde.org/2010/12/06/kde-announces-calligra-suite">dot.kde.org</a>. 

Here's how I see this. We're widening the scope, definitely we are *not* starting from scratch. Changes like moving to the git version control system from Subversion would be needed anyway, so there are no that much overhead related to the move as observers would think. And finally you have direct access to roughly the same group of fellow developers as before.
<!--break-->
What we want to improve even more is communication. Internally we've been enjoying high degree of transparency in KOffice and this is the case now too in Calligra. Perhaps it's not that always visible to everyone and this is OK -- after all developers, users, testers, journalists have so ofter different skills and point of view. We know that.

Many questions may appear after the yesterday's announcement. As a small step into make things more clear, just today we have started to fill the FAQ page:

<a href="http://community.kde.org/Calligra/FAQ">http://community.kde.org/Calligra/FAQ</a>

Feel free to put new questions there (signing in is preferred) or send them to calligra-devel@kde.org or ask on the <a href="http://forum.kde.org/viewforum.php?f=203">dedicated forum</a>.

But first for much more information you can visit <a href="http://blog.cberger.net/2010/12/07/calligra-past-present-future-a-few-answers/">Cyrille's blog</a> or <a href="http://aseigo.blogspot.com/2010/12/rose-by-any-other-name.html">Aaron's blog</a>.

<img src="http://farm3.static.flickr.com/2375/2106209643_cde7aa55bb.jpg"/><br/><small>(<a href="http://creativecommons.org/licenses/by-nc/2.0/">by-nc</a>) <a href="http://www.flickr.com/photos/akabilk/2106209643">akabilk</a></small>