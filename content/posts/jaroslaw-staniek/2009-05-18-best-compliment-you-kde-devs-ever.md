---
title:   "The best compliment for you, KDE devs, ever"
date:    2009-05-18
authors:
  - jaroslaw staniek
slug:    best-compliment-you-kde-devs-ever
---
The best compliment for you, KDE devs, ever:

"Daddy you have a new computer!"

-- my ~4 years old son Michał, yesterday while sitting with me at a (4 years old) ASUS notebook rebooted from Vista to Linux+KDE 4.2.3...
