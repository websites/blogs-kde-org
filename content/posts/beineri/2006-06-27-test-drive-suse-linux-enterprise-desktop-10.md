---
title:   "Test Drive SUSE Linux Enterprise Desktop 10"
date:    2006-06-27
authors:
  - beineri
slug:    test-drive-suse-linux-enterprise-desktop-10
---
If you're interested how the KDE desktop of SUSE Linux Enterprise Desktop 10 looks like, Novell offers now a <a href="http://www.novell.com/linux/prerelease.html">pre-release for free download</a>. It contains all the functionality of the regular release, but is not the final product. Or have just a look into the <a href="http://www.novell.com/documentation/beta/sled10/pdfdoc/kdequick/kdequick.pdf">SLED 10 KDE Quick Start</a> or <a href="http://www.novell.com/documentation/beta/sled10/pdfdoc/userguide_kde/userguide_kde.pdf">SLED 10 KDE User Guide</a> (both PDF).<!--break-->