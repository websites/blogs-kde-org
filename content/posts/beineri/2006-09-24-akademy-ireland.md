---
title:   "aKademy / Ireland"
date:    2006-09-24
authors:
  - beineri
slug:    akademy-ireland
---
Some random thoughts, if you want coverage of the talks happening here: <a href="http://planetkde.org">Planet KDE</a> has many impressions. I have never seen so many people being interested in golf. <a href="http://www.rydercup.com/2006/">The Ryder Cup</a> is visible everywhere in the town, on many advertizing spaces, on all TVs in the pubs. It looks btw like Europe will win against USA (again). The weather, as cold and moistly as weather forecast predicted. I guess the real challenge in Golf is to play with rain and hurricane left-overs noticeable. How often a week do Irish people have an original Irish breakfast? And how does their cholesterol level look like? In general, the food seems to be expensive here. Never experienced a town were in all nights (ok, only two until now) at around 2am horse carriages are passing on the streets. How to you recognize a good pub? Obviously if there are more people standing on the street (drinking and not smoking) than would ever fit into the pub. The architecture of the living houses and business streets follow all the same imo boring style. Pedestrian lights seem to exist at traffic lights only with a slight chance. They get green (after yellow) in all directions at the same time if at all. The residents seem to ignore them most time anyway. Really risky if you follow this habit and are used to look for traffic to the left...
<!--break-->