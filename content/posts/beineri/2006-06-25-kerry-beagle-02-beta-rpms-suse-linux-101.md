---
title:   "Kerry Beagle 0.2 Beta; RPMs for SUSE Linux 10.1"
date:    2006-06-25
authors:
  - beineri
slug:    kerry-beagle-02-beta-rpms-suse-linux-101
---
I released this week the current KDE SVN version as <a href="http://kde-apps.org/content/show.php?content=36832">Kerry 0.2 Beta</a> because not much progress will happen the next two weeks: coding part of a KDE improvement project together with the usability experts of my SUSE team started and a long-planned week of vacation (for recreation, while others will heavily hack in Norway :-) ).

RPMs for SUSE Linux 10.1 and <a href="http://en.opensuse.org/Factory">Factory</a> are available in <a href="http://software.opensuse.org/download/home:/Beineri/">my home project</a> on the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a>. This version requires a newer version of Beagle than SUSE Linux 10.1 and at this time even Factory contains so I put also Beagle 0.2.7 RPMs there. You can also find current versions of <a href="http://www.kde-apps.org/content/show.php?content=28437">kio-beagle</a> and <a href="http://www.kde-apps.org/content/show.php?content=35781">kBeagleBar</a> in the repository.
<!--break-->