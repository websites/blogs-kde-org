---
title:   "openSUSE Accepted to Summer of Code"
date:    2008-03-17
authors:
  - beineri
slug:    opensuse-accepted-summer-code
---
The participating mentoring organizations for the <a href="http://code.google.com/soc/2008/">Google Summer of Code 2008</a> have been just announced and the <a href="http://code.google.com/soc/2008/suse/about.html">openSUSE project is among</a>. If you're a qualifying student and want to earn some money by contributing to an Open Source project, please start discussing with us your (eg Build Service, KDE or YaST related) idea. We will also propose some ideas during the week and put them online <a href="http://en.opensuse.org/Summer_of_Code_2008">in the openSUSE wiki</a>. Note the student application deadline on March 31th!

The <a href="http://code.google.com/soc/2008/kde/about.html">KDE project</a> has of course also <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas">some distro-agnostic ideas</a>. :-)
<!--break-->