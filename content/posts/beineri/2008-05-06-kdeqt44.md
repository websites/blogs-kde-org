---
title:   "KDE:Qt44++"
date:    2008-05-06
authors:
  - beineri
slug:    kdeqt44
---
From the <a href="http://wire.dattitu.de/archives/2008/04/29/KDE-4.1-Alpha1-Live.html">cross-blogging</a> department, <a href="http://labs.trolltech.com/blogs/2008/05/06/qt-440-fully-released/">Qt 4.4 has been released</a> and is entering Factory for <a href="http://en.opensuse.org/OpenSUSE_11.0">openSUSE 11.0</a> with packages for older openSUSE releases being available in <a href="http://download.opensuse.org/repositories/KDE:/Qt44/">KDE:Qt44</a> (this will move sooner or later to KDE:Qt). Dirk Müller thinks that's boring news as "everyone has packages of it" and I should rather mention that he has created packages from Qt 4.5 development snapshot within the <a href="http://download.opensuse.org/repositories/KDE:/Qt45/">KDE:Qt45</a> Build Service repository.<!--break-->