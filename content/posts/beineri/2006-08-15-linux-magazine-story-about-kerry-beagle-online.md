---
title:   "Linux Magazine Story About Kerry Beagle Online"
date:    2006-08-15
authors:
  - beineri
slug:    linux-magazine-story-about-kerry-beagle-online
---
<a href="http://www.linux-magazine.com/">Linux Magazine</a> informed me that they put parts of <a href="http://www.linux-magazine.com/issue/70">their September 2006 issue</a> online including a PDF copy of the "<a href="http://www.linux-magazine.com/issue/70/KTools_Beagle_Helpers.pdf">KTools: Beagle Helpers - Kerry and KBeaglebar</a>" story. It's about the 0.1 version of Kerry though. The Kerry development paused a bit after the 0.2 Beta but I plan to continue it again after <a href="http://conference2006.kde.org/conference/talks/33.php">Kickoff</a> (to be included in openSUSE 10.2 Alpha 4) is now being close to be a good looking, usable and sophisticated start menu - also thanks to previous work done for KBeagleBar and Kerry. <!--break-->