---
title:   "Chemnitzer Linux-Tage 2008 Summary"
date:    2008-03-05
authors:
  - beineri
slug:    chemnitzer-linux-tage-2008-summary
---
The last week-end <a href="http://www.czessi.de/de/gallery/linux-messen/chemnitzer-linux-tage-2008/cimg0404.jpg.html">me</a> and Martin manned the openSUSE booth at <a href="http://chemnitzer.linux-tage.de/2008/live/">Chemnitzer Linux-Tage</a>:

[image:3315 size=original hspace=50]

Executive summary: the organizers counted 2400 visitors, Martin gave an "openSUSE project" talk to 100-120 people, we distributed 400 Promo-DVDs, several openSUSE caps and some Novell pinguins. More photos <a href="http://home.kde.org/~binner/clt2008/">from me</a> and <a href="http://chemnitzer.linux-tage.de/2008/info/bilder.html">from others</a> are available.
<!--break-->