---
title:   "Some openSUSE 10.3 Misconceptions"
date:    2007-10-08
authors:
  - beineri
slug:    some-opensuse-103-misconceptions
---
There are some misconceptions floating around about openSUSE 10.3. Unfortunately uninformed people are still allowed to blog ;-) so let me pick up some I read:

"<b>No Live-CD! Every hobby distro has one. Why can't a huge company like Novell do one?"</b>

Obviously someone didn't <a href="http://news.opensuse.org/?p=386">follow the development</a> and also didn't read <a href="http://news.opensuse.org/?p=400">the release announcement</a>. To quote from there: "Live CDs will be released in the next couple of weeks." <a href="https://bugzilla.novell.com/show_bug.cgi?id=299663">A bug with CD-drives</a> led to the decision to not release it last Thursday, currently I would bet on later this week. The KDE/GNOME Live-CDs contain btw the same packages as the one CD install media.

"<b>Other distro installer require X clicks, for YaST you need XX clicks</b>"

The YaST of the install media empowers you as usual to control every aspect of your installation - if you want. If you have no special needs clicking "Next" at every step will also lead you quickly to the goal. But openSUSE 10.3 will also premiere a Live-Installer on above mentioned Live-CDs which allows to install a system with the most common setting with much less steps (read X clicks) like known from other popular Live-CD distros.

"<b>With the one CD media the install downloads over 600MB. Didn't they manage to put it all on CD?</b>"

The one CD install media contain a complete functional desktop, either KDE or GNOME, which can be installed offline. If you compare you will actually notice that we managed to fit more applications on it than most other CD distros which often miss bigger stuff like OpenOffice.org, Firefox, Gimp or games. openSUSE 10.3 introduces the new concept of registering online repositories before the installation starts. On the screen where you choose whether to install or upgrade, there is <a href="http://news.opensuse.org/wp-content/uploads/2007/09/more-repos1.png">a checkbox "Add online repositories before installation"</a> which is enabled by default. If you want a quick offline installation, or an 'unbloated' installation, disable this option. Keeping it enabled will give you the default installation like you would get from the release DVD for one desktop, including eg translations and more games. And yes, we didn't manage to fit the DVD content on a single CD. :-)

"<b>openSUSE is bloated</b>"

This couldn't be further away from truth. openSUSE 10.3 has actually the most lean footprint of all recent releases. All patterns have been reworked and packages more splitted, eg you can install a very small base system or basic X window. The desktop CD installations are coercively optimized for size. You can call a full DVD or CD+online repos installation bloated but then you opted for the wide range of applications option.

<!--break-->