---
title:   "openSUSE News Goes Live"
date:    2007-07-19
authors:
  - beineri
slug:    opensuse-news-goes-live
---
Today one of my hack week projects <a href="http://news.opensuse.org/?p=63">went online</a>: <a href="http://news.opensuse.org/">openSUSE News</a>. Actually I have been working on it together with Robert Lihm already before and finished it only after. :-)

The openSUSE project missed a news portal for a long time. An announce mailing list isn't really the same, also because people cannot comment directly. And we plan to have much more content about <a href="http://lists.opensuse.org/opensuse-project/2007-07/msg00024.html">openSUSE and its people</a> on the openSUSE News site than on the announce mailing list.

Also news on it: <a href="http://news.opensuse.org/?p=36">the openSUSE distribution has a new project manager</a> who many of you will know: Stephan Kulow. Congratulations I guess - it seems he is too busy in his new position to blog about it. ;-)
<!--break-->