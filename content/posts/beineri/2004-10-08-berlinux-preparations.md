---
title:   "Berlinux Preparations"
date:    2004-10-08
authors:
  - beineri
slug:    berlinux-preparations
---
KDE will be present at <a href="http://www.berlinux.de/">Berlinux 2004</a> in two weeks and I'm busy with several preparations for that. There will be <a href="http://www.berlinux.de/vortrag2.htm">several mostly German talks</a>, including some prominent names likes Georg Greve and Jon 'Maddog' Hall, and I should be a bit prepared for my talk about KDE being qualified as enterprise desktop: Kurt provided me with an account for his <a href="http://dot.kde.org/1094109094/">NX</a> demo server. I made a small deal with SUSE to be able to show the <a href="http://www.suse.com/us/private/products/suse_linux/preview/">newest version</a> which includes among other things <a href="http://kde.openoffice.org/">OpenOffice.org 1.1.3 with KDE file dialog</a> integration. Now I have to bug Zack, who finally got his Mozilla CVS account, to create instructions how to build <a href="http://dot.kde.org/1094924433/">Mozilla/Qt and the Gecko kpart</a>. :-)

There will be also a small KDE booth with two computers. Up to now only Ellen and Ossi volunteered to help, and likely there will be at least one day where only one of them can attend full-time. So if any KDE contributor or user wants to help for some hours on either Friday or Saturday (s)he is more than welcome, please <a href="mailto:binner@kde.org">contact me</a>!

For the booth we have the <a href="http://developer.kde.org/~binner/aKademy2004/day6-8/dscf0104.jpg">nice KDE poster</a> printed by HP (didn't succeed to lose it yet ;-) ) and hundreds of oldish KDE flyers. And of course <a href="http://www.kde.org/areas/people/konqi.html">Konqui Konqueror</a> will appear both in person and within <a href="http://www.kde.org/stuff/clipart.php">movie</a>. This year we will have only <a href="http://www.kde.org/stuff/">new KDE pins</a> to sell (including a donation share for KDE e.V.) but I know that there will a merchant present with more KDE related stuff (mouse pads, stuffed toy, etc).
<!--break-->