---
title:   "Factory + Dashboard + OBS + KIWI = ???"
date:    2007-03-26
authors:
  - beineri
slug:    factory-dashboard-obs-kiwi
---
A short story about using the right tools: <a href="http://en.opensuse.org/Factory">Factory</a> is the development distribution of the openSUSE project, currently at version 10.3 Alpha 2+. The SUSE-powered <a href="http://developer.kde.org/~dirk/dashboard/">Dashboard</a> tells you what is compiling - or not. If it's a good day the <a href="http://software.opensuse.org/download/KDE:/KDE4/">KDE 4 packages</a> in the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> can be updated. And finally <a href="http://en.opensuse.org/Build_Service/KIWI">KIWI</a> allows you to create operating system images, eg. Live-DVDs.

And what do you get if you combine these together? A <a href="http://home.kde.org/~binner/kde4-live-dvd/">KDE SVN Live-DVD</a>. :-)
<!--break-->