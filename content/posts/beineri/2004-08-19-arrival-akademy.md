---
title:   "Arrival at aKademy"
date:    2004-08-19
authors:
  - beineri
slug:    arrival-akademy
---
Today Ossi and I arrived together at <a href="http://conference2004.kde.org/">aKademy</a> in Ludwigsburg. The organization room seems to be the busiest place at the moment, full with working people, sleepy North Americans, loaned hardware (refrigerators, laptops, network and sound equipment) and delivered merchandizing stuff. I also went around a bit to take <a href="http://developer.kde.org/~binner/aKademy2004/day0/">photos</a> of the outside, "our" cinema and the yard where the social events will take place.
<!--break-->'