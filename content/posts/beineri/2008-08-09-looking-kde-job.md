---
title:   "Looking for a KDE Job?"
date:    2008-08-09
authors:
  - beineri
slug:    looking-kde-job
---
As just mentioned in the <a href="http://akademy.kde.org/conference/presentation/35.php">openSUSE talk at Akademy</a>, Novell/SUSE is hiring for its KDE team! :-) The team is working on KDE in past and future openSUSE and SUSE Linux Enterprise releases. You can find the job description at <a href="http://novell.com/company/careers/">http://novell.com/company/careers/</a> under "Software Engineer Senior" (ID#1158). Please feel invited to inquire <a href="http://blogs.kde.org/node/3562">us at Akademy</a> if you want know more about benefits, how it's working for SUSE and any other question you might have about it.
<!--break-->
