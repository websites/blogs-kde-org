---
title:   "New KDE Four Live-CDs"
date:    2009-03-08
authors:
  - beineri
slug:    new-kde-four-live-cds
---
<p>New <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CDs with KDE 4.2.1, KOffice 2.0 Beta 7 and <a href="http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.2.1.list">much more</a> are up.</p>

<p>They were built within openSUSE Build Service's <a href="https://build.opensuse.org/project/show?project=KDE%3AMedias">KDE:Medias project</a> in which also automatically Live-CDs with <a href="ftp://ftp.kde.org/pub/kde/unstable/">KDE trunk snapshot</a> packages (currently 4.2.65) from <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/openSUSE_11.1/">KDE:KDE4:UNSTABLE</a> repositories are built. The ISOs appear (without prior testing by anyone) <a href="http://download.opensuse.org/repositories/KDE:/Medias/images/iso/">here</a>.
<!--break-->
