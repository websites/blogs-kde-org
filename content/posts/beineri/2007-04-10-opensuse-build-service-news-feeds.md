---
title:   "openSUSE Build Service News Feeds"
date:    2007-04-10
authors:
  - beineri
slug:    opensuse-build-service-news-feeds
---
Even less known than that there are RepoView pages created in every OBS repository's repodata/ sub-directory seems to be that there is also a latest-feeds.xml file which you can subscribe to with your feed reader. For example, for KDE:Community and openSUSE 10.2, this is <a href="http://software.opensuse.org/download/KDE:/Community/openSUSE_10.2/repodata/">the RepoView</a> page and this <a href="http://software.opensuse.org/download/repositories/KDE:/Backports/openSUSE_10.2/repodata/latest-feed.xml">the latest changes feed</a> it links to.<!--break-->