---
title:   "The KDE 4[ 1.].0.0 Release"
date:    2008-01-12
authors:
  - beineri
slug:    kde-4-100-release
---
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0 was released</a> yesterday and of course openSUSE <a href="http://en.opensuse.org/KDE4">complimented with packages</a> for openSUSE 10.2, 10.3 & Factory and the <a href="http://home.kde.org/~binner/kde-four-live/">version 1.0 of KDE Four Live</a> CD. For reactions see <a href="http://news.opensuse.org/2008/01/11/kde-40-released-with-opensuse-packages-and-live-cd/">openSUSE News</a>, <a href="http://digg.com/linux_unix/KDE4_final_has_been_released_Grab_a_Live_CD_here_to_test_it">Digg</a> or look at <a href="http://www.thecodingstudio.com/opensource/linux/screenshots/index.php?linux_distribution_sm=KDE%204.0">screenshots</a> - many readers seem to understand the nature of this "1.0.0 release of KDE4". :-)

If you run openSUSE 10.3 then you're only one click away from installing/upgrading to a common KDE 4.0 desktop setup:

<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Extra-Apps/openSUSE_10.3/KDE4-DEFAULT.ymp"><img hspace=50 src="http://files.opensuse.org/opensuse/en/d/dd/Kde4-ymp.png" /></a>

The KDE 4.0.0 tagging last week didn't make us stop improving our packages: we added 4.0 branch diffs of this week (so you get eg distinct resize and rotate handles for Plasmoids and Aaron's beloved "Zoom Out" button), and started to add our usual distro bits including an in our opinion better Kickoff presentation (more openSUSE 10.3 like look and/or what its upstream maintainer objected to):

[image:3202 size=original hspace=50]

Our current packages will be part of openSUSE 11.0 Alpha 1 next week. Of course there is lots and lots more stuff to integrate and port, everything that distinguishes a "distribution with KDE packages" from a "good KDE distribution", and to bring back old functionality (eg Kickoff/10.3 features) until the <a href="http://en.opensuse.org/Roadmap">openSUSE 11.0 release in June</a>.

As long as the KDE 4.1 schedule is still under discussion we can of course also only discuss and not decide (btw next <a href="http://en.opensuse.org/KDE/Meetings/">KDE IRC meeting</a> on 30th January) but likely we can tell you latest in the "KDE and openSUSE" (or alike) <a href="http://fosdem.org/2008/schedule/devroom/opensuse">talk at FOSDEM</a> more about our KDE 4.x/openSUSE 11.x migration strategy.

As for the build service, we will restructure <a href="http://en.opensuse.org/KDE/Repositories">our KDE repositories</a> and track both the KDE 4.0 branch and trunk with regularly updated packages. Regularly means (bi)weekly but for sure not daily as some users rumored our packages to be after every minor package fix/revision rebuild. :-)

PS: The talk which Bille gave yesterday to 60+ SuSE colleagues was recorded and will be afaik published soon.
<!--break-->