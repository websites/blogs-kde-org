---
title:   "openSUSE 11.0: Qt Package Manager Improvements"
date:    2008-05-11
authors:
  - beineri
slug:    opensuse-110-qt-package-manager-improvements
---
Just want to point out four improvements of the YaST Qt package selector in the upcoming openSUSE 11.0 that were missing too long, much requested (at least by me) and now added :-) :

[image:3458 height=300 hspace=50] [image:3459 height=300 hspace=25]

The first screenshot shows the new special package groups "Suggested packages" and "Recommended packages" to list packages which enhance your installed packages. Also the strange "zzz All" package group of previous releases is renamed to "All packages" and visible without endless scrolling.

On the second screenshot you can see the new "@System" meta repository to list all installed packages only. And note the new secondary filter "Unmaintained packages" to detect which packages are not contained in your activated repositories (also a nice way to detect which old packages were wrongly not obsoleted by a distro upgrade).

Update: Let me add a fifth one. As you can see on the screenshots we have a yast2-theme-openSUSE-Oxygen package with Oxygen style like icons everywhere thanks to <a href="http://mschlander.wordpress.com">Martin Schlander</a>.

<a href="http://en.opensuse.org/openSUSE_11.0"><img hspace=20 src="http://counter.opensuse.org/11.0/small" /></a> 
<!--break-->