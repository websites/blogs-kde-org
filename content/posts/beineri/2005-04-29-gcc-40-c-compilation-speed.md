---
title:   "gcc 4.0 C++ Compilation Speed"
date:    2005-04-29
authors:
  - beineri
slug:    gcc-40-c-compilation-speed
---
KDE sources now blacklist gcc 4.0.0 because it miscompiles KDE but that shouldn't be
a reason to do no compilation benchmarks, or? :-) My test machine was an Athlon XP
2600+ with 512MB and SUSE 9.3's gcc 3.3.5. The other gcc versions were pure gcc.
<br><br>
My first test was to compile Qt 3.3.4:
<pre>
             -O0     -O2
gcc 3.3.5   23m40   31m38
gcc 3.4.3   22m47   28m45
gcc 4.0.0   13m16   19m23
</pre>

Next I compiled kdelibs/HEAD with --enable-final and unsermake -j 2:
<pre>
             -O0     -O2
gcc 3.3.5   14m44   27m28
gcc 3.4.3   14m49   27m03
gcc 4.0.0    9m54   23m30
</pre>

Finally the usual development case, same as above without --enable-final:
<pre>
             -O0
gcc 3.3.5   32m56
gcc 3.4.3   32m49
gcc 4.0.0   15m15
</pre>

I really like gcc 4.0's -O0 compilation speed improvement. :-) After the Subversion
switch (latest rumor this weekend) I will certainly start to use gcc 4.0.1 or a
recent snapshot. Let us hope that gcc 4.1 keeps the optimization promises for
generated code the same way.
<br><br>
Emerging question: When will we see "unsertool" replacing the slow-going libtool?
<!--break-->
