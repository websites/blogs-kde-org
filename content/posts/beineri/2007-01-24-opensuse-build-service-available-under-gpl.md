---
title:   "openSUSE Build Service available under GPL"
date:    2007-01-24
authors:
  - beineri
slug:    opensuse-build-service-available-under-gpl
---
Joining the list of tools being used internally at Novell/SUSE being available as Open Source (like <a href="http://swamp.sourceforge.net/">SWAMP</a> or <a href="http://www.mozilla.org/projects/testopia/">Testopia</a>) now also the source code for the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> is <a href="http://lists.opensuse.org/opensuse-announce/2007-01/msg00002.html">available under GPL from today</a> on. So if you have some spare computers in your cellar you can set up your own build service farm (to build your own secret or illegal stuff).
<!--break-->