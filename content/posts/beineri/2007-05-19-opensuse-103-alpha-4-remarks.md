---
title:   "openSUSE 10.3 Alpha 4 Remarks"
date:    2007-05-19
authors:
  - beineri
slug:    opensuse-103-alpha-4-remarks
---
This week <a href="http://lists.opensuse.org/opensuse-announce/2007-05/msg00003.html">openSUSE 10.3 Alpha 4 has been released</a>. <a href="http://blogs.kde.org/node/2777">As previously announced</a> it doesn't contain ZMD anymore. From my point of view the inclusion of first KDE4 packages and the installation of four KDE4 games in the default KDE selection is more exciting. :-) Another interesting application you may want to install and try is Dolphin/KDE4. With Alpha 5 we should have all KDE4 modules in Factory. Until then has the <a href="http://software.opensuse.org/download/KDE:/KDE4/">KDE:KDE4 build service project</a> all modules available (and in atm two weeks newer version than included in Alpha 4).

Also interesting are the first-time inclusion of two community developments: <a href="http://en.opensuse.org/Instlux">Instlux</a> allows you to start the installation of openSUSE under MS Windows and the <a href="http://en.opensuse.org/Meta_Packages">YaST Meta Package Handler</a> allows one-click installations of packages (bundles) and subscription to repositories. There is a <a href="http://benjiweber.co.uk/installdemo/">Software Install Demo</a> page available and Benjiman's <a href="http://benjiweber.co.uk:8080/webpin/index-test.jsp?distro=SUSE_Factory">Software Search</a> can also provide "Install Now" information.

Unfortunately mainly due to the switch to TeX Live the download distribution version occupies now 6 CDs (still one DVD via Torrent) and there are no promises yet that certain selections require only the first X CDs for installation. At the opposite size end, the one KDE-Installation CD project (KDE3 only) missed the deadline of Alpha 4, but we will have it finally with <a href="http://en.opensuse.org/Roadmap/10.3">openSUSE 10.3 Alpha 5</a>. :-)
<!--break-->