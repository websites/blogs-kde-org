---
title:   "KOffice 1.5 Beta 2 for SUSE Linux"
date:    2006-03-11
authors:
  - beineri
slug:    koffice-15-beta-2-suse-linux
---
<a href="http://dot.kde.org/1142077964/">KOffice 1.5 Beta 2</a> has been released together with <a href="http://download.kde.org/download.php?url=unstable/koffice-1.5-beta2">binaries for SUSE Linux</a> 9.3, 10.0 and <a href="http://en.opensuse.org/Factory_Distribution">Factory</a>. Please help to test it to make the release rocking stable and install the provided debuginfo rpms for best crash reports if you can afford to download them.
<!--break-->