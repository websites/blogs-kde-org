---
title:   "The Return of kdedevelopers.org"
date:    2007-07-12
authors:
  - beineri
slug:    return-kdedevelopersorg
---
The <a href="http://blogs.kde.org/node/2854">missed</a> <a href="https://blogs.kde.org/">kdedevelopers.org</a> site <a href="http://blogs.kde.org/node/2854">is back</a>! Thanks go to Ian Geiser for founding and hosting it until recently. Starting this week it's hosted on a KDE e.V. server and administered by the KDE sysadmins. All old content except the theme has been transferred. The old (new account and password reminder notifications) and most known bugs of the new setup are fixed meanwhile. :-)

Some settings of the previous Drupal have been tweaked: the unused story feature is now completely disabled to avoid confusion between blogs and stories. You still have to be verified as KDE contributor after account creation for your own blog (and you still have to mail clee to get added on Planet KDE). Some new categories have been added, please tell if you think one is missing. Also the home page shows different content, a well visible and working search and post access counters are among the improved configuration.
<!--break-->