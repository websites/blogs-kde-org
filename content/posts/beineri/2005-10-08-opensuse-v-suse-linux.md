---
title:   "\"openSUSE\" v \"SUSE Linux\""
date:    2005-10-08
authors:
  - beineri
slug:    opensuse-v-suse-linux
---
<p><a href="http://www.novell.com/products/suselinux/">SUSE Linux 10.0</a> is available for purchase and <a href="http://lists.opensuse.org/archive/opensuse-announce/2005-Oct/0003.html">download in different flavors</a> and some people are still spreading wrong information (initially started by some journalists). To make it short: "openSUSE" is only the name for the development project. "SUSE Linux" is the name of the distribution, also for the Open Source Software edition. Everything other is wrong,  independent from if you read it on IRC, a supposed creditable news site - or even in the <a href="http://www.opensuse.org/">openSUSE Wiki</a>. As the nature of a Wiki is that everyone can change it, a user could wrongly rename all references to "SUSE Linux OSS" to "openSUSE". But once discovered the correct information was restored.</p>

<p>Footnote: Nice to the <a href="http://distrowatch.com/stats.php?section=popularity">increasing mindshare</a> and <a href="http://www.tuxmachines.org/node/2945">appreciation</a> for openSUSE's baby. Let's make 10.1 with KDE 3.5 even better!</p>
<!--break-->