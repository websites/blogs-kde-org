---
title:   "4 at 10th"
date:    2005-06-09
authors:
  - beineri
slug:    4-10th
---
Let us all together ensure that KDE 4.0 will be released at latest before the 10th anniversary of the KDE project (14th October 2006): this will put us into a good position against that legacy OS' major release "late next year".<br>
But do not let it us name "KDE X". :-)
<!--break-->