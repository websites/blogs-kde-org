---
title:   "SUSE Linux 10.1 Release"
date:    2006-05-13
authors:
  - beineri
slug:    suse-linux-101-release
---
SUSE Linux 10.1 has been <a href="http://lists.opensuse.org/archive/opensuse-announce/2006-May/0003.html">finally released</a> including <a href="http://en.opensuse.org/Xgl">Xgl</a> preview, <a href="http://www.gnome.org/projects/NetworkManager/">NetworkManager</a>, <a href="http://en.opensuse.org/Apparmor">AppArmor 2.0</a> and <a href="http://en.opensuse.org/Xen">XEN 3</a>. Learn more about it reading the <a href="http://en.opensuse.org/Product_Highlights">Product Highlights</a> and studying the first incoming <a href="http://en.opensuse.org/In_the_Press">reviews and screenshots galeries</a>.

This release forces you to choose your desktop. Guess what I think is the better desktop and more polished one: KDE 3.5 including all the neat Novell projects like <a href="http://en.opensuse.org/Projects/KNetworkManager_Screenshots">KNetworkManager</a>, <a href="http://www.kde-apps.org/content/show.php?content=29295">KPowersave</a>, <a href="http://en.opensuse.org/Kerry">Kerry Beagle</a>, <a href="http://kde.openoffice.org/">OpenOffice.org/KDE</a> and of course YaST system configuration which together make it the best available KDE desktop.

Meanwhile the <a href="http://en.opensuse.org/">openSUSE project</a> server and <a href="http://en.opensuse.org/Mirrors_Released_Version">the download mirrors</a> recovered from the first wave of interest. For an unchanged desktop installation only the first three CDs are now required for English and German (choose network installation for less traffic). Next week the mirrors will be challenged again with install and live DVDs and the supplementary repository (carrying KDE 3.5.2, KOffice 1.5 etc) for SUSE Linux 10.1 has also to be synced out.
<!--break-->