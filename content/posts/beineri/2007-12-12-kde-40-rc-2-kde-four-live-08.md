---
title:   "KDE 4.0 RC 2 / KDE Four Live 0.8"
date:    2007-12-12
authors:
  - beineri
slug:    kde-40-rc-2-kde-four-live-08
---
<a href="http://dot.kde.org/1197405276/">KDE 4.0 RC 2</a> has been released so it's time for a new version of <a href="http://home.kde.org/~binner/kde-four-live/">the  most comprehensive KDE4 Live-CD</a>:

[image:3140 size=original hspace=50]
