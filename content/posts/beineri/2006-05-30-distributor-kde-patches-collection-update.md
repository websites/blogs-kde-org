---
title:   "Distributor KDE Patches Collection Update"
date:    2006-05-30
authors:
  - beineri
slug:    distributor-kde-patches-collection-update
---
After a long time I have updated my <a href="http://developer.kde.org/~binner/distributor-patches/">Distributor KDE Patches Collection</a> during the last days. It now contains the latest release or release candidate tweaks and fixes of the most actively patching distributions like SUSE Linux, Kubuntu, Mandriva and ArkLinux. It should be of interest for maintainers of applications in KDE modules, packagers and users which are curious how much distributions patch the KDE releases.
<!--break-->