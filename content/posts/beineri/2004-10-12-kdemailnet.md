---
title:   "KDEMail.net"
date:    2004-10-12
authors:
  - beineri
slug:    kdemailnet
---
Do you know <a href="http://kdemail.net/">KDEMail.net</a>? It provides free email aliases in the form "firstname.lastname@kdemail.net" for KDE contributors and developers of KDE software, not necessarily in the main distribution, who are interested in a stable contact address to put as contact point into their work.
<!--break-->