---
title:   "KDE:Playground; KOffice 1.6 Alpha"
date:    2006-08-01
authors:
  - beineri
slug:    kdeplayground-koffice-16-alpha
---
We have founded yet another KDE project over at the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a>. It's called <a href="http://repos.opensuse.org/KDE:/Playground/">KDE:Playground</a> (<a href="http://repos.opensuse.org/KDE:/Playground/SUSE_Linux_10.1/repodata/">SUSE Linux 10.1 repo view</a>). Its purpose? To allow the SUSE developers to test development versions like the current Beta releases of Amarok, Basket, Digikam and Gwenview. You can even find today's <a href="http://dot.kde.org/1154424570/">KOffice 1.6 Alpha</a> in there (until now only 10.1/i586 rpms built, 10.0 rpms [without Krita] will follow).<!--break-->