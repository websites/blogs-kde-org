---
title:   "The KDE Team and Friends are Leaving SUSE..."
date:    2006-09-21
authors:
  - beineri
slug:    kde-team-and-friends-are-leaving-suse
---
...and are on the way to Dublin's <a href="http://akademy2006.kde.org">aKademy 2006</a> to "shape the future of the free desktop". :-) My last count resulted in 9 Novell/SUSE employees attending, 4 talks of and 2 BoFs with them. Another not so nice "nine": likely rain on all 9 days which I will be in Dublin. :-(
<!--break-->