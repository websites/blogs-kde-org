---
title:   "Akademy upcoming"
date:    2008-07-15
authors:
  - beineri
slug:    akademy-upcoming
---
<img src="http://akademy.kde.org/images/akademy2008_logo.png" align="right" hspace="2" vspace="2" alt="Akademy logo"/><a href="http://akademy.kde.org/">Akademy</a>, the big yearly KDE conference, is only three and a half weeks away. Novell sponsors at Gold level this year and will by last count be represented by 8 people (me, Dirk Müller, Will Stephenson, Lubos Lunak, Vincent Untz, Danny Kukawka, Klaas Freitag and Cornelius Schumacher) and involved in three talks during the conference part. I am looking forward to show/distribute openSUSE 11.0 to everyone and spread how lot of fun it is to work on/for [open]SUSE :-)...<!--break-->