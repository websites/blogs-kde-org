---
title:   "XBlast for SUSE Linux"
date:    2006-06-29
authors:
  - beineri
slug:    xblast-suse-linux
---
If I should name the reason which most extended my university visit I might mention XBlast. ;-) What is XBlast? <a href="http://xblast.sourceforge.net/">XBlast</a> is a X11 multiplayer game inspired by the classic game <a href="http://en.wikipedia.org/wiki/Bomberman">Bomberman</a> (known in Europe as Dynablaster). If you don't know it (are you under twenty?), it's a strategic, maze-based computer game with your avatars running around either alone or in teams and pushing soon exploding bombs at each other. It has no 3D or superb graphics - its sole attraction, like so many vintage games, is the fun it makes playing it with your friends.

Me and my fellow students played it usually daily and quite often up to an hour or beyond. First the original version running on a fast host and opening its displays on work stations, later the client/server version which over the years was improved by different developers with more features, new special bombs, new player shapes and of course tons and tons of levels. It takes quite some time to learn all the over 1000 levels inside out to have an advantage over other players.

Within SUSE department there don't seem to exist XBlast players. What a pity. However in remembrance to good old times (and to also may animate some of my colleagues) I have put XBlast into <a href="http://software.opensuse.org/download/games">the 'games' project</a> on the <a href="http://www.opensuse.org/Build_Service">openSUSE build service</a>. SUSE Linux until 2003 even contained XBlast 2.6.1 (the one server version), but then it was dropped for a silly reason. Now again there are RPMs available for SUSE Linux 9.3 to 10.1. Play it, spread the word, the world needs more XBlast players! :-)
<!--break-->