---
title:   "The Importance of KDE 3.5"
date:    2005-11-30
authors:
  - beineri
slug:    importance-kde-35
---
KDE 3.5 has been released (<a href="http://www.eweek.com/article2/0,1895,1894172,00.asp">press quote</a>: "<i>The newest version of KDE will have users happy that vendors have recently decided to keep supporting it</i>"), the really last feature release in the until now three and a half years successful KDE 3 series.  KDE 3.5.x will be the versions most users will use for likely more than a year (time until KDE 4 release + time until distributions pick it up + time until users upgrade). And KDE 3.5 will also be the version on the next year upcoming business desktop products which currently still ship KDE 3.2. 

So please don't jump now full-time into KDE 4 development despite as much fun it is being able to break or rewrite everything. Please help to make KDE 3.5 series the most stable and bug-free KDE ever. After, let us say KDE 3.5.2 bugfix release, there will be still several months left to fully concentrate on KDE 4. And if you want to hack on new features based on stable kdelibs3, KOffice is always looking for helping hands and aiming for a great release early next year.

PS: Congratulations to Firefox for gaining the ability to reorder tabs - a feature I enjoy in Konqueror <a href="http://lists.kde.org/?l=kde-commits&m=105396522304737&w=2">for two years</a> now. ;-)
<!--break-->