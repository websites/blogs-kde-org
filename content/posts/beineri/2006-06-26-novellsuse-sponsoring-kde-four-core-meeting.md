---
title:   "Novell/SUSE Sponsoring KDE Four Core Meeting"
date:    2006-06-26
authors:
  - beineri
slug:    novellsuse-sponsoring-kde-four-core-meeting
---
There is now a <a href="http://dot.kde.org/1151271635/">public announcement</a> for the hack fest <a href="http://blogs.kde.org/node/2136">hinted at in my last blog</a>: KDE Four Core aims at paving the way for KDE 4 development for others and is sponsored by Novell/SUSE together with Trolltech. Rather short-term scheduled it was first planned to happen in Nuremberg, nearby of it or then in Prague already during in June but that did clash a bit too much with the soccermania happening in Germany at the moment. So now it happens in Norway with 24 core hackers who could make it in the first July week.

Btw, <a href="http://dot.kde.org/1150362632/">another recent dot story</a>: Novell Poland contributed documentation locations to KDE.
<!--break-->