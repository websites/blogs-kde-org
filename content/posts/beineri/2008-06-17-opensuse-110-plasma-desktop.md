---
title:   "openSUSE 11.0: The Plasma Desktop"
date:    2008-06-17
authors:
  - beineri
slug:    opensuse-110-plasma-desktop
---
[image:3515 align=right hspace=2 vspace=2 width=341 height=256]

<a href="http://en.opensuse.org/OpenSUSE_11.0">openSUSE 11.0</a> will be finally released on Thursday! :-) The <a href="http://news.opensuse.org/2008/06/17/sneak-peeks-at-opensuse-110-kde-with-stephan-binner/">Sneak Peeks story about KDE</a> has just been published and I want to follow up with a list how our Plasma desktop differs from the stock KDE 4.0 version.

Most of the new Plasma features developed upstream until the Plasma sprint in April have been backported. Additionally we added our own new features and patches (all on KDE SVN server), which we tried to get upstream until review board went down and the sprint kicked off incompatible plasma API changes. A few of those changes already found their way into other distributions like an early version of our branding patch into Kubuntu or the moving of plasmoids on panels into Fedora. 

So here is a list (excluding bugfixes) of changed things I still remember:

<ul>
<li>Useless/non-working things have been removed: desktop rubber band, krunner option dialog, "Zoom Out" button in desktop toolbox, ...</li>
<li>Aya plasma theme is used by default, this required color tint support in Plasma.</li>
<li>The Plasma theme can be changed via GUI in the "Configure Desktop" dialog.</li>
<li>Desktop icon plasmoids don't show a big shadow by default, upstream mode is optional.</li>
<li>Dragging files onto Plasma desktop copies to ~/Desktop, removing icon plasmoids removes from ~/Desktop.</li>
<li>Trashcan, Show Desktop, Calculator and RSS Now plasmoids are shipped.</li>
<li>KRunner gives KDE4 applications higher ranking than parallel installed KDE3 applications.</li>
<li>Plasmoids can be moved on panels</li>
<li>It's possible to remove plasmoids from panel which could not be instantiated.</li>
<li>It's possible to add additional panels and remove existing ones.</li>
<li><a href="http://blogs.kde.org/node/3501">Hidden option</a> to hide the desktop toolbox.</li>
<li>"Lock Widgets" and "Show Dashboard" options added to desktop toolbox.</li>
<li>The dashboard view shows a title and quits when clicking on empty background.</li>
<li>added global shortcut to open start menu</li>
<li>Kickoff has the nicer look including branding from KDE 4.1 development</li>
<li>Kickoff popup is freely resizable</li>
<li>Kickoff can point out newly installed applications</li>
<li>Kickoff context menu lets you easily switch between Kickoff and classic KDE menu style and call the menu editor</li>
<li>Kickoff has "Save Session" option within Leave tab</li>
<li>Kickoff shows only selected action in logout confirmation dialog, with countdown</li>
<li>Digital clock plasmoid: can show seconds, tooltip added</li>
<li>Device notifier plasmoid allows to safely remove mounted devices</li>
<li>Task manager plasmoid: new option to show only tasks from current screen</li>
<li>Pager plasmoid: window icon support added</li>
</ul>

<a href="http://en.opensuse.org/openSUSE_11.0"><img hspace=20 src="http://counter.opensuse.org/11.0/small" /></a> 
<!--break-->