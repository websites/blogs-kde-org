---
title:   "Which Distro Will Be First to Ship KDE4?"
date:    2007-12-08
authors:
  - beineri
slug:    which-distro-will-be-first-ship-kde4
---
This amusing question seems to pretty bother some users in forums/story comment sections and fanboys of distros are fast with answering their distro will for sure be it. :-)

It's amusing for several reasons: Any question about KDE4 <a href="http://blogs.kde.org/node/2600">is faulty by concept</a>. Next, how can a distribution be "first" or significant earlier than all other when all get the source code at the same time, which is not earlier than when the release happens (or rather about a week before when the supposed release state got tagged)? And what do they mean with "ship"? One not serious answer I read was "Debian testing". Let us assume distro release including it is meant. I don't think that any distribution will adopt their regular release schedule to the KDE 4.0.0 release (also caused by ever moving release date). And if a distribution would do, would it do its users or the KDE project a favor?

My prediction is that on the release day of KDE 4.0.0 we will see a whole bunch of distributions providing packages either in their development tree or in separate repositories for older distro releases which should satisfy very early adopters of KDE4 now asking this question. And that we will see several distro releases in first half of 2008 containing a later KDE 4.0.x bugfix release for early adopters of KDE4.
<!--break-->