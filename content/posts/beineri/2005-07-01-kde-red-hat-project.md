---
title:   "The KDE for Red Hat Project"
date:    2005-07-01
authors:
  - beineri
slug:    kde-red-hat-project
---
Fedora is being called a community effort by RedHat. Despite that and demand it doesn't create decent KDE packages. Their KDE still defaults to the ugly and buggy "BlueCurve" style of the past "BlueCurve" age which tried to give GNOME and KDE applications a similiar look. Nowaday's joke is that KDE's default style "Plastik" looks more similar to Fedora GNOME's "Clearlook" style than "BlueCurve".

But there exists a true community effort to create KDE packages for RedHat products which many people seem to be still not aware of: The <a href="http://kde-redhat.sourceforge.net/">KDE for Red Hat Linux</a> team under the leadership of brave Rex Dieter regularly creates KDE packages of newest KDE releases. And that not only for Fedora but also Red Hat 9, Red Hat Enterprise 3 and Red Hat Enterprise 4 distributions. Hooray! :-)
<!--break-->