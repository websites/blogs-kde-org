---
title:   "Klax KDE 3.5 RC 1 Live-CD"
date:    2005-11-13
authors:
  - beineri
slug:    klax-kde-35-rc-1-live-cd
---
To complete the <a href="http://dot.kde.org/1131747786/">KDE 3.5 RC 1 release</a> efforts there is now also a <a href="http://ktown.kde.org/~binner/klax/devel.html">Klax Live-CD with KDE 3.5 RC 1</a> available. Now let me see if someone left something from this weekend over for me.<!--break-->