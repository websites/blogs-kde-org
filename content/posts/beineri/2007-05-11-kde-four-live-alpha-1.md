---
title:   "KDE Four Live Alpha 1"
date:    2007-05-11
authors:
  - beineri
slug:    kde-four-live-alpha-1
---
<a href="http://dot.kde.org/1178891375/">KDE 4.0 Alpha 1</a> has been released, code-named "Knut" (btw my reasoning was: "small but growing and being forgotten soon"). And of course it's accompanied by a new <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> version and <a href="http://en.opensuse.org/KDE4">packages for openSUSE</a>. Just keep in mind that it's the first Alpha release which is totally unusable and not representive for KDE 4[.0]. :-)<!--break-->