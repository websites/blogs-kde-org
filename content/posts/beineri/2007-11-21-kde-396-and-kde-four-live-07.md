---
title:   "KDE 3.96 and KDE Four Live 0.7"
date:    2007-11-21
authors:
  - beineri
slug:    kde-396-and-kde-four-live-07
---
The so called <a href="http://dot.kde.org/1195581531/">"Release Candidate" of KDE 4.0</a> has been released, with <a href="http://en.opensuse.org/KDE4">packages for openSUSE</a> available. The <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD release 0.7 contains them. I looks like whatever will be released or presented at the event which was scheduled by marketing/sponsor to happen in January will be only used by very early adopters. Hopefully openSUSE 11.0 will be able to ship some KDE 4.1.x release or some very high KDE 4.0.x release (which saw some light features freeze lift).<!--break-->