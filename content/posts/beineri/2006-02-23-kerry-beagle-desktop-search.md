---
title:   "Kerry Beagle Desktop Search"
date:    2006-02-23
authors:
  - beineri
slug:    kerry-beagle-desktop-search
---
[image:1819 align=right] The KDE desktop of SUSE Linux 10.1 (and the future enterprise products built on it) will contain a KDE frontend for <a href="http://beaglewiki.org/">Beagle</a> called Kerry. For this Beagle has been splitted into non-GUI and GUI parts, some backends are now in sub-packages (Evolution, Firefox) and the libbeagle API has been improved in parts. Besides generic file indexing Beagle already contains backends written by <a href="http://dbera.blogspot.com/">Debajyoti Bera</a> and others for KMail, Kopete and Konqueror's web history cache.

[image:1818 align=left hspace=5]Kerry resembles the previous clean Best frontend but with better KDE integration (autostart, global shortcuts, DCOP call for searching etc.) and some improvements like help texts, sorting by relevance / name / modification date and searching the term in the clipboard) compared to it. If you have one of the <a href="http://www.opensuse.org">SUSE Linux 10.1 Betas</a> (Beta 5 is due tomorrow) installed you can click on its system tray icon right now or invoke it with one of the shortcuts F12 or Alt-Space.

Debajyoti's <a href="http://www.kde-apps.org/content/show.php?content=28437">kio-beagle</a> gets also installed, that means you can enter "beagle:&lt;searchterm&gt;" within all KDE file dialogs (including the ones of <a href="http://kde.openoffice.org/">OpenOffice.org/KDE</a>) and of course file managers like Konqueror (hint: bookmark your regular searches).

Some Beagle wishes for the future: <a href="http://bugzilla.gnome.org/show_bug.cgi?id=331135">working Akregator/KDE 3.5  backend</a>, <a href="http://bugzilla.gnome.org/show_bug.cgi?id=331609">a Konversation backend</a> and long term a more complete backend for <a href="http://www.kontact.org">Kontact</a> (addressbook, calendar) or <a href="http://pim.kde.org/akonadi/">Akonadi</a>.


PS: This weekend's <a href="http://www.fosdem.org/2006">FOSDEM 2006</a> is a good place to learn more about KDE, openSUSE and Beagle - and <a href="http://www.advogato.org/person/Marcus/diary.html?start=108">to ask for Marcus</a>.
<!--break-->