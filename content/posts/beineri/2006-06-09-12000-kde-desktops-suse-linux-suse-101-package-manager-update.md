---
title:   "12000 KDE desktops on SUSE Linux; SUSE 10.1 Package Manager Update"
date:    2006-06-09
authors:
  - beineri
slug:    12000-kde-desktops-suse-linux-suse-101-package-manager-update
---
Two good news today: ZDnet reports about a German tax authority <a href="http://news.zdnet.co.uk/0,39020330,39274196,00.htm">migrating 12000 desktops to KDE on SUSE Linux</a> from Solaris. And a first <a href="http://lists.opensuse.org/archive/opensuse-announce/2006-Jun/0002.html">SUSE Linux 10.1 package management update</a> (should be installed with YaST Online Update) was released which fixes several annoying bugs (performance improvements are planned for later).<!--break-->