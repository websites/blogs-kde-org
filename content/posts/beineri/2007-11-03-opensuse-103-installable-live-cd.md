---
title:   "openSUSE 10.3 Installable Live-CD"
date:    2007-11-03
authors:
  - beineri
slug:    opensuse-103-installable-live-cd
---
The initial comments about openSUSE 10.3 have been ranging from "best openSUSE release ever" to "worse than 10.2". Strange, or? So why not form your own opinion? It's easy with the <a href="http://news.opensuse.org/?p=489">openSUSE 10.3 Live-CDs released</a> (<a href="http://download.opensuse.org/distribution/10.3/iso/cd/openSUSE-10.3-GM-KDE-Live-i386.iso">KDE ISO)</a>. The installable Live-CDs address also <a href="http://blogs.kde.org/node/3019">the common misconceptions</a> mentioned earlier.

The installation numbers have doubled compared to 10.2 and a constant majority of the openSUSE users choose the KDE desktop/version over GNOME. Still most reviewers seem to try the GNOME version only and then make a judgement for the whole distribution. The new YaST-GTK+ frontend & package selector and the GNOME updater applet not being mature don't help with their verdict for sure. Please see this as another reason to try above linked KDE version and compare with other KDE distros. :-)
<!--break-->