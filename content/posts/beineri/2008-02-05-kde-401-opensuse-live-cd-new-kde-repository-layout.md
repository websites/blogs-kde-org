---
title:   "KDE 4.0.1, openSUSE Live CD, New KDE Repository Layout"
date:    2008-02-05
authors:
  - beineri
slug:    kde-401-opensuse-live-cd-new-kde-repository-layout
---
The <a href="http://news.opensuse.org/2008/02/05/kde-quickies-kde-401-opensuse-live-cd-new-kde-repository-layout/">openSUSE News story</a> has the same title and I don't want to repeat everything here, just add some bits:

The new <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live CD</a> will hopefully work also on some computers whose broken BIOS had problems finding the boot code in certain sectors before.

Our <a href="http://en.opensuse.org/KDE/KDE4">KDE 4.0.1 packages</a> have meanwhile quite some patches to improve Kickoff, for creating a nice familiar desktop setup and several Plasma features backported from trunk (eg <a href="http://vizzzion.org/images/blog/resizable-panel.jpg">taskbar features and panel size</a>/location configuration):

<p>><img src="http://news.opensuse.org/wp-content/uploads/2008/02/kde4live.png" hspace="50" alt="KDE-Four-Live 1.0" /></a></p>


<p>The obligatory link for openSUSE 10.3 users to install/upgrade KDE 4.0.1 and register the new KDE4 build service repositories:</p>

<p><a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Extra-Apps/openSUSE_10.3/KDE4-DEFAULT.ymp"><img hspace="50" src="http://files.opensuse.org/opensuse/en/d/dd/Kde4-ymp.png" /></a></p>

Afterwards please unregister the KDE:KDE4 repository, it will be removed soon.
<!--break-->