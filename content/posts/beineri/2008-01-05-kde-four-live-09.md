---
title:   "KDE Four Live 0.9"
date:    2008-01-05
authors:
  - beineri
slug:    kde-four-live-09
---
This new version of <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> shows the changes done during the holiday season until new year. Compared to the final 4.0 release it misses some days of bugfixing and the final Oxygen artwork (new sounds and wallpapers, icon updates). In preparation for the release this CD contains only applications/modules which will be part of the KDE 4.0 release (so eg no KDEPIM, KOffice, Amarok, Quanta and extra plasmoids).