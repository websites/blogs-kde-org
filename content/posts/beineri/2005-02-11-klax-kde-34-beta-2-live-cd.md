---
title:   "\"Klax\" KDE 3.4 Beta 2 Live-CD"
date:    2005-02-11
authors:
  - beineri
slug:    klax-kde-34-beta-2-live-cd
---
I have long time not blogged but now that <a href="http://www.kde.org/announcements/announce-3.4beta2.php">KDE 3.4 Beta 2</a> code-named "Keinstein" (blame me for the name) was released this week and with it the feature and i18n freezes in place this might change. :-) Also this week <a href="http://www.slackware.org/announce/10.1.php">Slackware 10.1</a> became available. A small hobby of me is to test distributions (see <a href="http://ktown.kde.org/~binner/#distributions">history</a>). My first Linux distribution was Slackware 2.0 as book appendix, which I quickly had to replace with Walnut Creek Slackware 3.0 CDROMs because I needed ELF support. Since then I didn't use Slackware anymore, so it was time to revisit.

Playing with Slackware reminded me of my previous <a href="http://slax.linux-live.org/">Slax</a> testing, especially the size of its ISO image. So I got the idea to produce a KDE 3.4 Beta 2 Live-CD based on it (always something new, my last remastered Live-CD was Knoppix based). Unfortunately I couldn't complete it within 2,5 days until the KDE 3.4 Beta 2 release because this time no Slackware binary packages were contributed before the announcement. Also it showed that Slackware is missing many dependencies of KDE as packages which I had to build additionally myself. But today I brought it to an end and am now happy to announce the birthday of <a href="http://ktown.kde.org/~binner/klax/">Klax</a>. The birth weight is 373 MB and it's looking proper. :-)

I know that it works on two of my computers. It would be nice to have some feedback before making more buzz about it.
<!--break-->