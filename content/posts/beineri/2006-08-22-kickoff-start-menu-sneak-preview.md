---
title:   "Kickoff Start Menu - Sneak Preview"
date:    2006-08-22
authors:
  - beineri
slug:    kickoff-start-menu-sneak-preview
---
<p>As previously blogged, <a href="http://opensuse.org/">openSUSE 10.2</a> will have a redesigned KDE start menu created by the KDE and usability team at SUSE, after doing usability testing with other start menus. We now have a working prototype, code-named 'Kickoff' (started during world soccer championship, obviously), which is currently being tested with real users in the SUSE usability lab. At <a href="http://conference2006.kde.org/">aKademy 2006</a> in Dublin, Coolo will give a <a href="http://conference2006.kde.org/conference/talks/33.php">Start Menu Research talk</a> about what we learned during this project. Also, we are preparing a web-page to document our research. Click on the screenshot for the whole Kickoff sneak preview experience:

<a href="http://home.kde.org/~binner/kickoff/sneak_preview.html"><img src="http://home.kde.org/~binner/kickoff/sneak_preview.png" align="middle"></a>
<!--break-->