---
title:   "KIllustrator to Prevent Adobe Monopoly"
date:    2005-04-19
authors:
  - beineri
slug:    killustrator-prevent-adobe-monopoly
---
So Adobe is about to buy Macromedia for 3.4 billion US dollars. Because of the possible antitrust investigation of the market for illustration tools company executives <a href="http://www.heise.de/newsticker/meldung/58709">like the Adobe CEO</a> and <a href="http://www.pcworld.idg.com.au/index.php/id;1197843252;fp;2;fpid;1">the Adobe Chief Financial Officer</a> were eager to point out the strong competition with explicit mention of KIllustrator.

Somehow they missed that Adobe urged a renaming of <a href="http://www.koffice.org/killustrator/">KIllustrator</a> (to <a href="http://www.koffice.org/kontour/">Kontour</a>) some years ago and that the development of this software has been discontinued (in favor of <a href="http://www.koffice.org/karbon/">Karbon 14</a>). But I guess that wouldn't have fit into their argumentation.
<!--break--> 