---
title:   "openSUSE 11.1: Plasma Desktop Toolbox"
date:    2008-11-02
authors:
  - beineri
slug:    opensuse-111-plasma-desktop-toolbox
---
Discussions about the usefulness of the Plasma desktop toolbox arise regularly. Usually it focus on the "Zoom Out"/Activities feature which as also Plasma developers admit is not as far implemented and nicely integrated as of KDE 4.1 as everyone wants it to be. If one removes (maybe even irreversible) the "Zoom Out" button, nothing is left in the desktop toolbox which is not also available in the panel/desktop context menus. So why not make it optional completely? For openSUSE 11.0 we offered that as <a href="http://blogs.kde.org/node/3501">a hidden option</a>.

For openSUSE 11.1 we decided to ship with the desktop toolbox disabled by default. This takes effect with Beta 4 (it's not announced/released yet because of the week-end but ISOs are propagating to mirrors). Technically this is done by adding a desktop containment implementation without toolbox (as possible since Plasma/KDE 4.0) and backporting the desktop containment swicher from KDE 4.2. As result users who want to experiment with Plasma activities can easily switch the desktop toolbox on again:

<img hspace=20 src="https://blogs.kde.org/files/images/containment.png">

Also please take note of the third desktop activity type option. :-)

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/small" /></a>
<!--break-->
<!--more-->