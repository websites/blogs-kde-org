---
title:   "The Tale of Fixed Release Schedules"
date:    2006-03-16
authors:
  - beineri
slug:    tale-fixed-release-schedules
---
Once upon a time there were some dwarfs who wandered around the earth and told everyone who liked to hear that they would produce a desktop including the distribution of their handcraft every sixth months. And some people believed them and the dwarfs' business grew a bit. But the dwarfs wanted to grow further and so they continued to praise themselves as the high lords of fixed release schedules - and it worked some times somehow. People who had an own project or business to place on the desktop started to trust the dwarfs' telling. The dwarfs were happy and built bigger and bigger furniture...

But on one dark day the dwarfs <a href="https://lists.ubuntu.com/archives/ubuntu-art/2006-March/000734.html">couldn't deliver anymore</a> in time, their business outgrew them. Sad for the people who were dumb to trust the promises and made themselves dependent on a delivery in time. Let us welcome the dwarfs in <a href="http://lists.opensuse.org/archive/opensuse-announce/2006-Mar/0001.html">the</a> <a href="http://www.osnews.com/story.php?news_id=13035">reality</a> and next time you hear them talking remember The Tale of Fixed Release Schedules.<!--break-->