---
title:   "openSUSE 11.0 Announced"
date:    2008-06-19
authors:
  - beineri
slug:    opensuse-110-announced
---
<a href="http://news.opensuse.org/2008/06/19/announcing-opensuse-110-gm/">Release Announcement</a>

<a href="http://software.opensuse.org/"><img src="http://gk2.sk/countdown/leaderboard.png" border="0" /></a>

Hint: If you get your ISOs within the next 24h you will enjoy full Akamai speed.