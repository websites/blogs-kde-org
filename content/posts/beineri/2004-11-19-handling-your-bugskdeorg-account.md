---
title:   "Handling your bugs.kde.org Account"
date:    2004-11-19
authors:
  - beineri
slug:    handling-your-bugskdeorg-account
---
Your email address is your user id on <a href="http://bugs.kde.org/">bugs.kde.org</a>. You <a href="http://bugs.kde.org/createaccount.cgi">create an account</a> by having you mailed a random password. You can of course <a href="http://bugs.kde.org/userprefs.cgi">change your password</a> later. If your email address changes, please also <a href="http://bugs.kde.org/userprefs.cgi">change your bugs.kde.org id</a> so that you will continue to receive comments and questions by developers and other users (if you have not changed the <a href="http://bugs.kde.org/userprefs.cgi?tab=email">email settings</a>). You have created more than one bugzilla account over the years? Then you can <a href="mailto:sysadmin@office.kde.org">mail the sysadmins</a> and ask to have your accounts manually merged.
<!--break-->