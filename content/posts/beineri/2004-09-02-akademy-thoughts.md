---
title:   "aKademy Thoughts"
date:    2004-09-02
authors:
  - beineri
slug:    akademy-thoughts
---
So aKademy is over for most people, only the aKademy press staff continues to publish stories based on content collected and interviews conducted in Ludwidgsburg and it will take some weeks to close all financial transactions. I only read positive comments until now so don't let me ruin the "perfect" impression by listing what didn't work or other than planned. :-)

Personally disappointing was my own talk which was bugged by technical problems: First the external VGA output didn't want to activate (this used to work with the very same notebook and a previous version of the distribution/graphic driver). Then it didn't want to mount a USB stick (later I discovered I had to reboot to make my USB mouse work again) to where I wanted to copy my presentation over to another notebook. Then the network didn't want to work at first go causing an over 10min delayed start of my talk with a visitor's notebook.

Unfortunately the loaned notebook had only KDE 3.2 installed so I couldn't show some new features of KDE 3.3's kdialog and it also hadn't the KDE sources so no possibility to show at least the kdialog test scripts. To make the bad impression complete, the KDE installation was plagued by the famous "every KDE application start takes 30 sec because DNS/network is not available" bug. Btw, the cause for this bug was discovered at aKademy and fixed for KDE 3.3.1 just few days before.

What a bad start into the day you may think, but you're wrong! It was already bad earlier. I did the night shift this day and there was a group of drunken developers doing all kinds of strange stuff and keeping me busy opening the door for them, closing it behind them several times and watching them in general. And btw you don't gain my respect, more the opposite, when showing me your blank arse. In short this was a very short night with only 45min sleep on the ground.

Talking about respect, it's sad to see who has newly (compared to N7y) started to smoke. And you were not looking good at all. Can't intelligent people find a better "solution" against stress?

What can I add to the positive comments? It was nice to meet and talk to new and already known people which could quickly grow to a small BoF, eg I talked to Frank about kde-apps.org/kde-look.org and several nearby sitting developers started to participate in the discussion. I liked presentations giving ambitious plans or showing new working features like the integration of Kontact and Kopete with Novell Groupwise (why did I have to think of lying monkeys here?).

I have now uploaded <a href="http://developer.kde.org/~binner/aKademy2004/">all the photos</a> which I have taken. Dunno when I will find the time and motivation to comment, sort and process them. Unfortunately there doesn't exist a rather complete group picture with image map comments laid underneath this time so it will be difficult to name everyone correctly. On the other hand I took much fewer pictures than last year in Nove Hrady, partly because I don't think that the pictures are good. I'm now dreaming of a new camera with external flash and way shorter exposure time.
<!--break-->