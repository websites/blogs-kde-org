---
title:   "Trolltech Doing Everything Right?"
date:    2005-05-23
authors:
  - beineri
slug:    trolltech-doing-everything-right
---
Trolltech seems to be hard working to smooth out every imaginable point of criticism and to be the best Free Software team player: 
<ul>
<li>It ensures with the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a> that Qt will be always available for Free Software developers.</a>
<li>Upcoming Qt 4 gets polished with daily snapshots based on <a href="http://lists.trolltech.com/qt4-preview-feedback/">everyone's feedback</a>.</li>
<li>Qt 4 will be also <a href="http://www.trolltech.com/newsroom/announcements/00000192.html">available under GPL</a> for Windows as already for Linux and MacOS.</li>
<li>Trolltech <a href="http://people.kde.org/david.html">sponsors developers</a> to work on KDE and also <a href="http://blogs.kde.org/node/view/1086">hires from the KDE camp</a>.</li>
<li>Trolltech invests now into improving the Unix infrastructure <a href="http://blogs.kde.org/node/view/933">like the X server</a>.</li>
<li>Likely with Trolltech influence additional to normal Qt documentation also the Qt book is <a href="http://dot.kde.org/1098950311/">available for free</a>.</li>
<li>It continues to <a href="http://www.kde.org/support/thanks.php">support KDE</a> and meetings like the <a href="http://accessibility.kde.org/forum/">Unix Accessiblity Forum</a> or upcoming <a href="http://kdevelop.org/mediawiki/index.php/Linux_Desktop_Development_and_KDevelop_Developers_Conference_2005">KDevelop conference.</li>
<li>Trolltech arranges and sponsors <a href="http://dot.kde.org/1076341428/">Qt development contests</a> with nice prizes.</li>
<li>Trolltech developers participiate and help with the <a href="http://lists.kde.org/?l=kde-commits&w=2&r=1&s=kde4&q=b">initial port of KDE to Qt 4</a> including making changes to Qt 4.</li>
<li>Thanks to a <a href="http://www.trolltech.com/developer/affiliations.html">new investment round announced today</a>, Canopy and SCO got kicked out from Trolltech ownership.</li>
</ul>

And no, I won't argue about the LGPL as business plan for a company having a library as main product.
<!--break-->