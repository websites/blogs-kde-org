---
title:   "Nat Friedman about Desktop Developers"
date:    2005-05-27
authors:
  - beineri
slug:    nat-friedman-about-desktop-developers
---
Just read an interview with Nat Friedman, carrying a 'Novell Vice President' title, in German c't magazine 12/05 (to be published on Monday). For your information, c't is a reputable computer technology magazine and not something like LUG Radio. In there he says about KDE and GNOME: "<i>The only people who emphasize the differences  are the developers, and I do not mean the ISVs (Independent Software Vendors), but the people who do not take showers. *laugh*</i>"

Maybe he will have the chance at GUADEC in Stuttgart to see desktop developers take a shower, but then maybe not because he lives by oneself in a first class hotel room. Anyway, Novell really should choose their executives better.
<!--break-->