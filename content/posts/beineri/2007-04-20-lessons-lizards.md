---
title:   "Lessons for Lizards"
date:    2007-04-20
authors:
  - beineri
slug:    lessons-lizards
---
<a href="http://developer.novell.com/wiki/index.php/Lessons_for_Lizards">Lessons for Lizards</a> is a community cookbook-style book project for openSUSE which started a few months ago (<a href="http://files.opensuse.org/opensuse/en/4/41/LfL_FOSDEM2007_presentation.pdf">FOSDEM presentation</a>). Most of <a href="http://forgeftp.novell.com/lfl/.html/index.html">the current articles</a> deal with system adminstration, but there are also already two about KDE: "<a href="http://forgeftp.novell.com/lfl/.html/cha.kde.kiosk.html">KDE Configuration for Administrators</a>" and "<a href="http://forgeftp.novell.com/lfl/.html/kde-customization.html">Customizing KDE</a>". Keep it growing! :-)<!--break-->