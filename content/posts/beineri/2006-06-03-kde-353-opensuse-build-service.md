---
title:   "KDE 3.5.3 in openSUSE Build Service"
date:    2006-06-03
authors:
  - beineri
slug:    kde-353-opensuse-build-service
---
This week saw the <a href="http://dot.kde.org/1149112734/">release of KDE 3.5.3</a> (bugfixes and carefully chosen new features) and also the <a href="http://en.opensuse.org">openSUSE</a> <a href="http://en.opensuse.org/Build_Service">Build Service</a>, although <a href="http://en.opensuse.org/Build_Service/Roadmap">still in Alpha phase</a>, <a href="http://lists.opensuse.org/archive/opensuse-announce/2006-May/0008.html">becoming productive</a> as it now creates and offers the KDE and KDE applications backports among other stuff.

The supplementary/KDE repository <a href="ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/README">does not exist anymore</a>. KDE and applications backports are now splitted into <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/">KDE:KDE3</a> and <a href="http://software.opensuse.org/download/repositories/KDE:/Backports/">KDE:Backports</a>  projects (there is also a <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/">KDE:KDE4 project</a> which atm doesn't offer more than Qt and kdelibs packages) from <a href="http://software.opensuse.org">software.opensuse.org</a>. This allows you to update an application without having to update your KDE as applications are now built against the KDE version from your distribution. Please read the <a href="http://en.opensuse.org/Build_Service/User">Build Service for Users</a> page how to add the new repositories to your package manager.

Please keep in mind that these KDE packages are, as supplementary/KDE before, experimental, untested and unsupported stuff and hurting your health by keeping you longer in front of the computer. :-)
<!--break-->