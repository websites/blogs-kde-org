---
title:   "Start Menu Blogging"
date:    2007-02-20
authors:
  - beineri
slug:    start-menu-blogging
---
Two days ago <a href="http://tirania.org/blog/archive/2007/Feb-18.html">Miguel de Icaza blogged about the revised Main Menu</a> of the GNOME desktop in the upcoming SUSE Linux Enterprise 10 Desktop Service Pack 1 and how it and the <a href="http://en.opensuse.org/Kickoff">KDE Kickoff start menu</a> inspired each other. In the update section he adds some very familiar sounding anecdotes: during the usability study done in Germany with amongst others Microsoft Vista Beta 2 we also found that most users don't recognize a "Search" input box at the bottom. And that users have problems to find its shutdown button which really turns off their machine (iirc a video with this was shown at Akademy). So same findings and wondering if Microsoft has done their homework with the Vista start menu.

Today <a href="http://www.barisione.org/blog.html/p=74">Marco Barisione followed up with a response</a> what he liked about the new Main Menu and what not, adding mockups and wishes how to improve it: among them "no buttons behaving as tabs", the dislike of an external application browser and wanting to have search-results displayed in-place: this sound almost as he wants Main Menu to become more like what Kickoff as of openSUSE 10.2 delivers. :-)
<!--break-->