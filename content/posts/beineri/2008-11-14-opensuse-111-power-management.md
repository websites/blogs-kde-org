---
title:   "openSUSE 11.1: Power Management"
date:    2008-11-14
authors:
  - beineri
slug:    opensuse-111-power-management
---
Another bit for the upcoming openSUSE 11.1 KDE4 desktop is power management: KPowersave has not been ported to KDE4. Powerdevil, which will be part of KDE 4.2, comes to our rescue. Together with a backport of the battery plasmoid popup from KDE 4.2 SVN as in yesterday's released <a href="http://news.opensuse.org/2008/11/13/development-release-opensuse-111-beta-5-now-available/">openSUSE 11.1 Beta 5</a> we get a good power management solution. The backport only uses QWidgets so that we are not forced to backport all the latest Plasma 4.2 widgets etc.:

<img hspace=20 src="https://blogs.kde.org/files/images/OS11.1-battery.png">

Btw, a big thanks to the <a href="http://i18n.opensuse.org/">openSUSE Localization</a> teams who make it possible that we can add or backport all these small bits. :-)

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/small" /></a>
<!--break-->
