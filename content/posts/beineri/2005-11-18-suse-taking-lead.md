---
title:   "SUSE Taking the Lead"
date:    2005-11-18
authors:
  - beineri
slug:    suse-taking-lead
---
The development snapshot <a href="http://lists.opensuse.org/archive/opensuse-announce/2005-Nov/0002.html">SUSE Linux 10.1 Alpha 3</a> has been released yesterday and of course it <a href="http://www.opensuse.org/Factory-News">contains the latest release candidates</a> of KDE 3.5, Firefox 1.5, Thunderbird 1.5 and X.org 6.9. If you are a developer you may be interested to learn that SUSE is the first distribution which has switched its development to and is based on the <a href="http://gcc.gnu.org/gcc-4.1/changes.html">upcoming gcc 4.1</a> compiler and this Alpha offers nice ways (upgrade, http/ftp installation, ISOs) to play with it.<!--break-->