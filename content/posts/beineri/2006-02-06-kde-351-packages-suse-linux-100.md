---
title:   "KDE 3.5.1 Packages for SUSE Linux 10.0"
date:    2006-02-06
authors:
  - beineri
slug:    kde-351-packages-suse-linux-100
---
<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.1/SuSE/">KDE 3.5.1 packages for SUSE Linux 10.0</a> are now available which include fixes for the worst KDE 3.5.1 regressions. They will also appear at <a href="http://www.novell.com/linux/download/linuks/index.html">the usual YaST source</a>. These packages are unsupported and mostly untested: kdelibs3-devel has a known wrong dependency which can be safely ignored and will be removed in the next revision. Packages for older distributions may follow once the new dependency resolution of the build server for older distributions problems are solved out .
<!--break-->