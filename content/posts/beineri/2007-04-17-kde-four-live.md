---
title:   "KDE Four Live"
date:    2007-04-17
authors:
  - beineri
slug:    kde-four-live
---
I found a nice name for my <a href="http://en.opensuse.org/Build_Service/KIWI">openSUSE-based</a> KDE 4 Live CDs/DVDs: &quot;<a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a>&quot; - which has also spoken a nice second meaning. :-) It has been three weeks since <a href="http://blogs.kde.org/node/2743">the initial announcement</a>, but still <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule#May_1.2C_2007:_Alpha_Release_.2B_kdelibs_soft_API_Freeze">two weeks to go</a> until KDE 4.0 Alpha 1, so I updated the DVD to another random SVN snapshot. The refinement and up-to-date keeping of the KDE4 packages in the openSUSE build service continues. A new <a href="http://en.opensuse.org/KDE4">KDE4 wiki page</a> describes our goals, the packages and  their usage.
<!--break-->