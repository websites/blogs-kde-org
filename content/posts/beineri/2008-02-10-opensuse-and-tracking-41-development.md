---
title:   "openSUSE and Tracking 4.1 Development"
date:    2008-02-10
authors:
  - beineri
slug:    opensuse-and-tracking-41-development
---
First two mentions from an exciting openSUSE week: the new openSUSE evan..uhm <a href="http://news.opensuse.org/2008/02/04/welcome-zonker/">community manager has been finally disclosed</a>, it's <a href="http://zonker.opensuse.org/">Joe "Zonker" Brockmeier</a>. And <a href="http://news.opensuse.org/2008/02/08/announcing-opensuse-110-alpha-2/">openSUSE 11.0 Alpha 2 has been released</a> with which we put KDE 4.0 to the test as default desktop. To keep up with openSUSE happenings occasionally I recommend <a href="http://news.opensuse.org/category/weekly-news/">openSUSE Weekly News</a> btw...

As previously said openSUSE will, besides providing packages for the KDE 4.0 branch, continue to track the KDE trunk/4.1 development with packages for openSUSE 10.3 and Factory. This happens in the  new <a href="http://en.opensuse.org/KDE/Repositories#KDE_4_Development">KDE:KDE4:UNSTABLE:*</a> Build Service repositories. The packages don't have all of our distro-specific patches though. Don't mix these repositories with the *:STABLE:* editions! A new <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD version including our current KDE 4.0.61 snapshot and KOffice 1.9.95.3 packages has just been uploaded. This CD should now also work with KVM (<a href="http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.0.1-1.0.1b.iso.xdelta">4.0.1 CD xdelta patch for KVM</a>). :-)
<!--break-->