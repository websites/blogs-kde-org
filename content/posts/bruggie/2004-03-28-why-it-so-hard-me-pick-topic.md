---
title:   "Why is it so hard for me to pick a topic ?"
date:    2004-03-28
authors:
  - bruggie
slug:    why-it-so-hard-me-pick-topic
---
Wow, it has been more then 5 months since i posted here... This really requires an update :)
<!--break-->
On a personal level there were some changes. I quit my job at Beaphar and now work for Intel at SAP in Germany so i moved from the Netherlands to Germany (reading all the blog entries from the other people it seems that moving is a hip thing to do :). Dont have my own place yet (staying at wheels' place, and yes i am the dutch guy singing along with that Lyle Lovett song) but i'm working on it so i'll be moving soon again. The work at Intel is Linux related, i'm very pleased with that and i owe wheels a lot of gratitude for paving the way for getting me in.

About Kompare, i'm working on it :) but it takes time. Everytime i start i see issues in the design that i am not happy with and start fixing them first which leads to new bugs that have to be fixed first :). 

Jeff made a nice patch for Kompare that will derive the middle widget with all the squigly lines of a QSplitter so you can now drag it left and right.

I almost finished a huge patch that removes all QPtrLists and replaces them with QValueLists. This was a pain in the butt. Also in this patch is a 25 percent speed improvement on parsing. KCachegrind now tells me that 75% of the time spend in opening and parsing a diff file is now in QTextStream::readLine() (for the conversion to unicode) so there is not much i can do anymore. I'll try to keep speeding it up by regularly using calltree and KCachegrind and searching for bottlenecks.

This is it, you're now up to date again.

<b>Update</b>
After committing this patch i found a huge time guzzler that i introduced when converting to QValueList: QValueList::operator[]( size_t ); it was good for a hell of a lot of my runtime :} After changing it to use a QValueVector, the runtime cost of that function was brought back to 0.19% :) The calltree instruction dumps decreased from 104 to 57 dumps for doing the exact same diff and selection in kompare.