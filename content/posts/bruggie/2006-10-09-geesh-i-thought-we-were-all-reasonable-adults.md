---
title:   "Geesh, I thought we were all reasonable adults..."
date:    2006-10-09
authors:
  - bruggie
slug:    geesh-i-thought-we-were-all-reasonable-adults
---
.. but this scripting thread that keeps going and going on kde-core-devel is pathetic. Bah! Currently I am no longer reading the thread and simply deleting every email with scripting in the subject. He who codes decides and as soon as an application is working properly in any scripting language (therefore the bindings are working fine) it should be shipped inside a module. Period. If it creates an extra dependency then so be it. I assume cmake will be made smart enough to make sure that when the bindings are not there the application will not be built and cmake will not bail out with a "requires these and these bindings" error. If you feel that you have to have the same app in C++ then code it yourself. Again he who codes decides. You need it in C++ and you make it better than the scripting version, fine we ship both versions as long as they are properly maintained. That goes for any software we put inside the modules like it has always been done.

So I dont get the fuzz about this, just let people do whatever they want, you can't steer them unless you pay them and since no one of us is really paid to work on their app there is no way in hell to force your thoughts on anyone. He will either join some other project that does let him code in his or her own preferred language or he will not code at all. Either way we lose so please stop this thread once and for all.

Edit: made a booboo that i hope is fixed now.
