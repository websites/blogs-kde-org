---
title:   "Inline differences"
date:    2003-11-15
authors:
  - bruggie
slug:    inline-differences
---
Yes i have implemented it in Kompare and committed it to CVS but unfortunately it is not activated atm. It is not open for discussion before people start begging me about activating it because it is not in the release plan and it is way too late for new features. 

If you want to toy with it change the -DINLINE_DIFFERENCES=0 in the Makefile.am files in the libdiff2/, komparepart/ and komparenavigationpart/ directories (no not folders :)) to -DINLINE_DIFFERENCES=1 and recompile Kompare. If you have problems feel free to open bugreports in bugs.kde.org. Have fun with it !