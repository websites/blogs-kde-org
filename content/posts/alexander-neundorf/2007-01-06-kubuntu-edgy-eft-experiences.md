---
title:   "kubuntu edgy eft experiences"
date:    2007-01-06
authors:
  - alexander neundorf
slug:    kubuntu-edgy-eft-experiences
---
So finally I decided to do a completely fresh install on my notebook, a Dell C640. If you are looking for a notebook which is good supported by Linux and FreeBSD, I can really recommend it, everything works out-of-the-box. also under FreeBSD, also the external VGA connector, useful when giving talks etc.

Ok, so what to install ?
It's intended to be easy to administer, so Slackware is out, although it is still my favourite distro for my desktop.
So, SUSE or kubuntu ? Both are good, but since I thought I should learn a bit more about Debian, I decided for kubuntu, downloaded the 6.10 Edgy Eft iso, burned it with k3b and rebooted the notebook.
The Edgy Eft CD booted without problems, then before starting the installation I set up the network manually (I don't have DHCP here, just simple static addresses) and then started the installation. 
Not much to say, everything went smoothely without any issues and like 15 minutes later my new kubuntu booted :-)

Then I started adding packages: kdevelop, cmake, koffice, mc, joe, xfte and some more.

Until now I found three issues:
By default no additional package repository is enabled, so you have only a small selection of packages. This wouldn't be a problem, it it would be possible to enable the other repositories using the default package manager from the start menu. But this one provides only very limited features. I had to find out about adept_manager and start it from the command line "sudo adept_manager", then I was able to activate the other repos and add all the stuff I wanted.
How should a newbie ever find this ?
Or did I miss something obvious ?

Second issue: I plugged in a digital camera, the dialog popped up, asking me whether I wanted to open it in digikam or konqueror. I selected konqueror, it was mounted and displayed in konqy. Everything fine :-)
I tried a second time, selected "Upload to digikam" this time, digikam started, but complained "Can't read contents of /sda1". Obviously digikam was started with the URL media:/sda1, but doesn't seem to handle it correctly, but just uses the path from the URL, i.e "/sda1". This directory obviously doesn't exist, so it can't work.
Are there any known workarounds ?

Last issue: How can I put photos on my iPod under Linux ?
I can upload songs with the ipod-ioslave without problems, for images I found gpixpod. But it seems gpixpod doesn't like me.
If I point it to a wrong directory, it tells me things like "you have an ipod-none, this is not supported and will never be. Do you want to exit now ?" Hmm.
Ok, when pointing it to the mounted iPod it starts and seems to work. Except, when I want to add photos, the file dialog doesn't list any jpg files, just empty directories, so I can't actually select any photos to upload :-/
I guess it's some missing dependency, some python lib I guess. Any hints which one ?

Beside that, Edgy Eft makes a really good impression. Installation took all in all maybe 30 minutes, plus 1 or two hours installing the complete set of applications and transferring files from my old installation.
kubuntu now also has nice GUI utilities for package installation, user management and more, hibernating the notebook simply worked, and upstart also seems to work. So, very impressive ! :-)

Two things I like better in SUSE: they dropped arts, and KDE lives in /opt/kde3.
I'm really looking forward to kubuntu 7.04 :-)

Bye
Alex
