---
title:   "Looking for a student job in an Open Source project in Germany ?"
date:    2012-01-20
authors:
  - alexander neundorf
slug:    looking-student-job-open-source-project-germany
---
You are a student, you would like to contribute to an Open Source project, and get paid for it ?

Then I may have something for you.
At my job at Fraunhofer in Kaiserslautern we are using the <a href="http://www.cdash.org">CDash</a> testing server for testing the software we develop - various different High Performance applications for Linux clusters.

We'd like to extend the features offered by CDash, especially with regard to performance monitoring (so we can check our software actually stays high-performance). There are more things we'd like to have in CDash, and if you are interested, you can probably also come up with ideas how to make CDash rock even more. :-)
Of course everything you develop for CDash will be upstreamed (as long as the maintainer agrees, I'm in good contact with him).

So, if this sounds interesting to you, if you are good with LAMP, then get in touch with me: neundorf AT kde.org.

Alex

P.S. there are also regular software development jobs available (C++, Linux, Qt, distributed systems), but they are not in an Open Source project. If you are interested anyway, feel free to contact me.
