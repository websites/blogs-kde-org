---
title:   "text based presentation software ?"
date:    2008-02-16
authors:
  - alexander neundorf
slug:    text-based-presentation-software
---
I just decided for myself I should try to use a text based presentation software, since GUI-based presentation apps require you to think about what goes on which page from the beginning, usually you see only the current page, you have to think about the layout (fontsize, decorations etc.), you have to use the mouse, etc.
<!--break-->
I remember that I heard several times of text or markup language based presentation tools, where you basically type some text in your editor, run the tool and then you get some presentation. I just can't remember any names. Preferably it shouldn't require latex.

What can you suggest ?
I'm looking forward to your suggestion. :-)

Alex
