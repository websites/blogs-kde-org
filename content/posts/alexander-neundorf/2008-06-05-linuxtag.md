---
title:   "LinuxTag"
date:    2008-06-05
authors:
  - alexander neundorf
slug:    linuxtag
---
This year I was again at LinuxTag, after I missed the one last year. LinuxTag in Karlsruhe was always very nice, the one two years ago in Wiesbaden somehow didn't feel that good, but this time in Berlin it was really great again.
<!--break-->
It was a nice event. I arrived late Thursday, just so I could manage to get to the Social Event before midnight. From there we headed to the hotel and next morning the fun started :-)
There were a lot of new people I never met before: Patrick Spendrin, aka Saro Engels (according to him a name from Brave New World), one of our KDE-on-Windows hackers, Claudia Rauch, our KDE e.V. secretary/manager/employee/... (what's the correct job position ?), Alexandra Leisse, who did a great job at organizing LinuxTag for KDE, Roland Wolters aka Liquidat, and we noticed that we both had lived the previous 5 years in Jena without knowing that, Eckhart, Luca, Lydia , Simon, and of course all the usual suspects :-)

We had KDE running on Linux, Windows and Mac there, I was quite impressed to actually see that working :-)
Friday evening we were invited by Trolltech to a dinner in an Italian restaurant, which was nice, and directly located below a S-Bahn (suburban train), so had that train running all few minutes over our heads. 
Saturday LinuxTag ended. I took the chance and talked with some Gnome/gtk guys, and in the end we were talking about CMake again. They are also not really happy with autotools, it would be great if they would join us with using CMake. Beside all the (mainly) KDE devs (but also e.g. VLC devs), here and there we were also talking about version control systems. git seems to be quite popular, Patrick would prefer hg, me too. Also the Gnomes are thinking about VCSs, but won't switch in the near future. Their switch to svn is not that long ago yet.
In the evening we met at a Ubuntu party with BBQ at a canal, which was very nice too. 
So now I'm back and have finally catched up with email, so I have time to blog again :-)

Looking forward to Akademy to meet you again :-)
Alex

