---
title:   "Please update to CMake 2.4.5 or newer"
date:    2007-03-11
authors:
  - alexander neundorf
slug:    please-update-cmake-245-or-newer
---
do you still have CMake 2.4.3 installed on your KDE4 development  system ? Then it's time to upgrade to CMake 2.4.5 or newer. Because starting Monday in a week, March 19th, at least CMake 2.4.5 or newer will be required for building KDE4.
The last required upgrade (which was to 2.4.3) happened on Sept. 12th last year, which is now exactly 6 months ago. Before this we lived with CMake 2.4.1 for 4 1/2 months. I think that's not too bad and CMake 2.4.5 will again last for several months.

So what will CMake 2.4.5 bring us compared to 2.4.3 ?
<ul>
<li>better support for building the tests (the targets will always be there but not always part of the "all" target)
<li>INSTALL(DIRECTORY ... ) as it is used by the oxygen icons
<li>faster dependency scanning
<li>IF( file1 IS_NEWER_THAN file2 ) which will enable us to do automoc at buildtime (and not at cmake/configure time as it always was)
<li>various minor improvements and bug fixes here and there 
</ul>
So, please update your CMake to version 2.4.5 or 2.4.6 until next Monday, March 19th if you haven't done so yet.

Bye
Alex