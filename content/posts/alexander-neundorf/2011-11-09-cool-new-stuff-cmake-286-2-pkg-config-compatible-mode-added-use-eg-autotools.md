---
title:   "Cool new stuff in CMake 2.8.6 (2): pkg-config compatible mode added for use e.g. with autotools"
date:    2011-11-09
authors:
  - alexander neundorf
slug:    cool-new-stuff-cmake-286-2-pkg-config-compatible-mode-added-use-eg-autotools
---
After introducing the automoc feature in <a href="http://blogs.kde.org/node/4495">my last blog</a>, here comes the next part of this series. More will follow.

<b><i>The new --find-package mode of CMake</i></b>

Typically, in projects which are built using autotools or handwritten Makefiles, the tool pkg-config is used to find whether and where some library, used by the software, is installed on the current system, and prints the respective command line options for the compiler to stdout.

Since CMake 2.8.6, also CMake can be used additionally to or instead of pkg-config in such projects to find installed libraries.

With version 2.8.6 CMake features the new command line flag --find-package. When called in this mode, CMake produces results compatible to pkg-config, and can thus be used in a similar way.

E.g. to get the compiler command line arguments for compiling an object file, it can be called like this:
<pre>
   $ cmake --find-package -DNAME=LibXml2 -DLANGUAGE=C -DCOMPILER_ID=GNU -DMODE=COMPILE
   -I/usr/include/libxml2
   $
</pre>
To get the flags needed for linking, do
<pre>
   $ cmake --find-package -DNAME=LibXml2 -DLANGUAGE=C -DCOMPILER_ID=GNU -DMODE=LINK
   -rdynamic -lxml2
   $
</pre>
As result, the flags are printed to stdout, as you can see.

The required parameters are
<ul>
<li> NAME - this is the name of the package, the same way as it is used in
 find_package() calls when using CMake
<li> LANGUAGE : which language is used, possible options are C, CXX, Fortran and ASM
<li> MODE : either COMPILE, LINK or EXIST
<li> COMPILER_ID: this identifies which compiler you are using, so CMake can print
 the options as necessary for this toolchain. When using the GNU compiler, use "GNU"
  here. Many other compilers are compatible to the command line syntax of gcc. Other
  options are "Intel", "Clang", "MSVC" etc.
</ul>
So, you can insert calls like the above in your hand-written Makefiles.
For using CMake in autotools-based projects, you can use cmake.m4, which is now also installed by CMake.
This is used similar to the pkg-config m4-macro, just that it uses CMake internally instead of pkg-config. So your configure.in could look something like this:
<pre>
   ...
   PKG_CHECK_MODULES(XFT, xft >= 2.1.0, have_xft=true, have_xft=false)
    if test $have_xft = "true"; then
        AC_MSG_RESULT(Result: CFLAGS: $XFT_CFLAGS LIBS: $XFT_LIBS)
    fi

   CMAKE_FIND_PACKAGE(LibXml2, C, GNU)
   AC_MSG_RESULT(Result: CFLAGS: $LibXml2_CFLAGS LIBS: $LibXml2_LIBS)
   ...
</pre>

This will define the variables LibXml2_CFLAGS and LibXml2_LIBS, which can then be used in the Makefile.in/Makefiles.



<b><i>What does that mean for developers of CMake-based libraries ?</i></b>

You don't have to install pkg-config pc-files anymore, just install a Config.cmake file for CMake, and both CMake-based and also autotools-based or any other projects can make use of your library without problems.
Documentation how this is done can be found here:
<ul>
<li> <a href="http://www.cmake.org/Wiki/CMake/Tutorials#CMake_Packages">http://www.cmake.org/Wiki/CMake/Tutorials#CMake_Packages</a>
<li><a href="https://projects.kde.org/projects/kde/kdeexamples/repository/revisions/master/show/buildsystem">https://projects.kde.org/projects/kde/kdeexamples/repository/revisions/master/show/buildsystem</a>
</ul>

What does that mean for developers working on e.g. autotools-based projects, and using a project built with CMake ?

Take a look at the cmake_find_package() m4-macro installed since CMake 2.8.6 in share/aclocal/cmake.m4, it contains documentation, and will help you using that library.
Thanks go to Matthias Kretz of Phonon fame, now working <a href="http://compeng.uni-frankfurt.de/index.php?id=mkretz">on HPC stuff</a>, who wrote the cmake.m4 from scratch (which was necessary since it had to be BSD-licensed in order to be included in CMake).

<b><i>Internals</b></i>

Internally, CMake basically executes a find_package() with the given name, turns the results into the command line options for the compiler and prints them to stdout.
This means it works basically for all packages for which a FindFoo.cmake file exists or which install a FooConfig.cmake file.
There is one issue though: FindFoo.cmake files, which execute try_compile() or try_run() commands internally, are not supported, since this would required setting up and testing the compiler toolchain completely.
It works best for libraries which install a FooConfig.cmake file, since in these cases nothing has to be detected, all the information is already there.

All this stuff is still very new, and has not yet seen wide real world testing.
So, if you use it and find issues, or have suggestions how to improve it, please let me know.

Alex
