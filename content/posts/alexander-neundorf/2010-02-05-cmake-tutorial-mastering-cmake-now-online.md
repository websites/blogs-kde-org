---
title:   "CMake tutorial from \"Mastering CMake\" now online"
date:    2010-02-05
authors:
  - alexander neundorf
slug:    cmake-tutorial-mastering-cmake-now-online
---
Hi,

Bill from Kitware just announced that the CMake tutorial from the "Mastering CMake" book is now also <a href="http://www.kitware.com/blog/home/post/7">available online</a>.
If you're interested, have a look.

(Btw. <a href="http://www.kitware.com/blog/">their new blog</a> also contains other interesting reads, e.g. about <a href="http://www.kitware.com/blog/home/post/6">open science</a> etc.)

Alex
