---
title:   "CMake news"
date:    2007-06-12
authors:
  - alexander neundorf
slug:    cmake-news
---
so since 6 weeks I'm now working at Kitware, Clifton Park, NY, USA.
Since it's the first time that I'm in America this means a <b>lot</b> of new impressions for me. Friendly people, suburbs, beautiful nature, huge cars, baseball everywhere and much more.
Are there actually any other KDE developers here in the Capital Region ?
<p>
So what can I say about CMake (since this is of interest for KDE) ?
<!--break-->
<p>
It was definitely a good decision to switch to CMake. It is maintained and developed by a team of highly skilled software engineers, mainly Bill and Brad. It's maintained carefully and new features are designed with flexibility, backward compatibility and "usabilty" (if you can say so for a build system) in mind. CMake comes with a big suite of unit tests, consisting of over 80 separate tests, each one testing several features of CMake, which gives several hundred tested features with each full test run. These tests run nightly on a big variety of platforms, and on some platforms continuous builds are running. Without this continuous testing it would be impossible to ensure the quality of CMake. We should have this too, at least for kdelibs on let's say Linux, FreeBSD, OSX and Windows.
<p>
I'm currently setting up a <a href="http://www.cmake.org/Testing/Sites/galibier/kdelibs4-alpha1-CMake-HEAD/20070611-1858-Experimental/BuildWarning.html">Nightly build of kdelibs</a> (from alpha1) with CMake from cvs head. While this doesn't test kdelibs, it ensures that there go no changes into CMake which might break the compilation of KDE. Which is good both for KDE and for CMake.
<p>
What new stuff will we get in CMake ?<br>
Well, CMake 2.4.7 will be released soon, it will bring mainly bugfixes. Current development work goes into what will become CMake 2.6. Among others this will bring cross compiling capabilities to CMake, which will make cross compiling with CMake <b>way</b> easier than e.g. with autotools. Autotools don't really know that they are cross compiling and must be tweaked into doing so, either by setting a bunch of environment variables or by "hacks" like Scratchbox (which works quite good, but nevertheless I think it's a hack) or the variety of other cross compiling environments (OpenEmbedded, T2), each with its own approach.
While this is not directly related to KDE, it's of course a good thing if our buildsystem makes cross compiling easier, especially since there are recently efforts to make kdelibs usable with the QWS windowing system, which is the first step in porting kdelibs to portable devices.
<p>
CMake is used at Kitware for all their software products, so there is time, man power and also some money behind CMake development, which ensures that CMake will be there and well maintained for the next years. It also means that CMake will get support for all the new stuff as it comes out, like 64 bit systems (ok, not so new anymore), OSX on Intel, universal binaries, Vista (yes, now that KDE4 is portable to Windows that's also interesting for us) etc. Otherwise Kitwares customers wouldn't be happy...
<p>
So, ignoring all the features of CMake itself, just the development behind it makes it a very good choice for KDE :-)  ...and of course also for other projects.
<p>
Alex

