---
title:   "Wanna have regular regression builds ? Continous builds ? Continous unit testing ?"
date:    2006-01-27
authors:
  - alexander neundorf
slug:    wanna-have-regular-regression-builds-continous-builds-continous-unit-testing
---
Well, if you want that, you can have it today for trunk/KDE/kdelibs/.
The cmake ( http://www.cmake.org ) developers have been so nice to setup a dashboard for KDE: 
http://public.kitware.com/dashboard.php?name=kde
If you build kdelibs/ using cmake, simply enter "make Experimental" and the results will end up there. As you can see, I just succeeded building kdelibs/ on FreeBSD :-)

Setting up continous builds (recompiling on commits) is also possible, if required with email notifications if there are problems detected. Unit tests can be run. And this on all platforms, Linux, *BSD, Mac OS X, Windows (cygwin, mingw, MSVC). 

What you have to do for this ?
Get trunk/KDE/kdelibs/, get cmake from cmake cvs or download the special cmake KDE release created yesterday by Bill Hoffman: http://mail.kde.org/pipermail/kde-buildsystem/2006-January/000539.html
and read the short introduction:
http://mail.kde.org/pipermail/kde-buildsystem/2006-January/000517.html

Finally run cmake, make or make Experimental, and see how kdelibs/ is built and the results appear on the dashboard :-)

Have fun
Alex


