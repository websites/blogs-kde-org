---
title:   "where to buy a 14\" non-glossy notebook without Windows in Germany ???"
date:    2008-04-02
authors:
  - alexander neundorf
slug:    where-buy-14-non-glossy-notebook-without-windows-germany
---
Ok, so because the old notebook is starting to fall apart (it is probably around 5 years old or so) I need a new one.
So, I want to buy a new notebook, nothing too fancy: 14 inch, non-glossy, Intel Core Duo, Intel Graphics, Intel WLAN, 1GB RAM, price at most 800 Euro.
I guess it's obvious to everybody reading this that I'd like to have fully Intel chipset because of the free drivers. This will actually be my first non-AMD CPU I buy new since 1994 or so.

Now to the tough part: the last MS product I bought was Win95, now I'm running 100% Linux since around 1997 on all my private computers, so does my girl friend. Oh, that's not true, from time to time I also install FreeBSD somewhere, but that's not the point. Add to that that I spend basically all of my spare time working on free software, so that people can create, share and access their data, information and communication freely without having to pay a company to let them do that.

Now, how much do I think I want to buy MS Windows today and add one more sample to the number of sold MS systems, even if it just comes bundled with a notebook and probably doesn't add much more than 20 to 50 Euro to the price ?
Correct, I will not do that.
I mean, even if I would get it for free, I want my next notebook vendor to know that there are customers who want to run Linux.

Now comes the problem: does anybody in Germany sell such a notebook ?

Lenovo Thinkpads come with Windows and I didn't find an option where I could get one without it. 

Dell Germany has a 15 inch notebook which it ships with Linux, so this comes already quite close. It is not 14 inch, it starts at a quite low price but also with less powerful hardware and if I upgrade it to the level of of the entry Windows notebook it is more expensive than this one. Choosing a different notebook and asking for Ubuntu instead of Windows is not possible (yes, I called the order hotline and asked several times).

Dell USA actually has what I'd like to have, 14" non-glossy fully Intel with colors to choose (an Inspiron). But they don't ship to Germany :-/

So, can anybody give me some hints where to get such a notebook ?
(Asus EeePC is not powerful enough)

Alex
