---
title:   "Breezy and its \"crippled\" konqueror"
date:    2005-11-10
authors:
  - alexander neundorf
slug:    breezy-and-its-crippled-konqueror
---
Last week I updated my notebook from Ubuntu Hoary to Breezy. I was very pleasantly suprised when I noticed that they decided to make the "Simple Browser" profile of konqueror the default profile.
I added it one and a half year ago to konqueror in cvs and now finally it seems people start using it :-)

For those who want the "full" konqueror back:
Search for konqueror.rc (should be in $KDEDIR/share/apps/konqueror/). This is the default profile. In the subdirectory profiles/& you will find the other profiles. These profiles are simple xml files, so you can edit them manually if you like. You can also execute "konqueror --profiles" to let konqy list all known profiles for you. To start konqueror with one of these profiles, enter "konqueror --profile your_preferred_profile". You can also use the special kicker button to start konqy in a special profile. If you have konqueror preloading enabled, this might play some tricks on you. Disable it and set the number of preloaded konquerors to 0, then it will work as expected.

Alex
