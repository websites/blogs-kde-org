---
title:   "Back from Paris, nothing about my niece, and some words about konqueror"
date:    2005-08-09
authors:
  - alexander neundorf
slug:    back-paris-nothing-about-my-niece-and-some-words-about-konqueror
---
Hi all,

so now I'm back from Paris, now already almost for two weeks, where I visited the finale of the Tour de France this year.
It was a great experience :-)
<!--break-->
Ulle had a great day during the time trial and managed to get on the third place overall. Even if Rasmussen wouldn't have had the problems he had, we wouldn't have been able to defend his third place. This weekend is <a href="http://www.teag-hainleite.de"> TEAG Hainleite</a> where I'll see them all again :-)

Is Linux for geeks only and too hard to use for "normal" people ?
Last week the niece of my girlfriend visited us for some days. Among others she played some games on the computer and surfed a bit in the web. She also managed to create her first web page ever (she didn't know what html is before this). Now, well, I don't have any Windows boxes here. IOW she had no problems using Linux/KDE, she liked the KDE games and with Kooka, Quanta and Gimp she was able to create <a href="http://www.neundorf.net/sarah/sarah.html">a simple web page</a>. So for somebody new to computers Linux/KDE is not especially hard to use I'd say (actually she didn't care whether the box runs Linux or Windows or whatever).<br>

What's happening on the KDE side of things ?
Not much currently. I don't have much time left over from work and my holidays start in two weeks. This is at the same time as Akademy so that I won't be able to attend Akademy.<br>
I'm still trying to compile kdelibs using <a href="http://www.cmake.org">cmake</a>. It's progressing, but slowly. The problem is that I don't know much about our current build system, and so everytime the cmake build stops I have to find out what our current build system does differently in this case. Once I have figured this out implementing it in cmake is usually not the big issue. But if e.g. there was a missing define in config.h, this means that then config.h changed and so almost *everything* will be rebuilt. This takes time. Additionally there have been major changes to the makefile generator of cmake, which had the result that cmake cvs was some days a little bit buggy. Now that the next stable cmake release is approaching the situation is getting a lot better :-)

And finally some words about konqueror: it is right that currently konqueror is neither the perfect web browser nor the perfect file manager. I really like konqueror, I don't want to miss the ability to have several tabs open, some with local directories, others with man pages, some web pages, pdf files. This really rules. But it is also true that the default konqueror profiles are quite full of icons, toolbars menus and other stuff which can confuse users (including me).
Do we have to split konqueror into three separate applications to improve the situation ? I don't think so.
Konqueror supports profiles. Each profile can have its own xmlgui file attached. This way it is possible that the "file management" profile can have a different user interface than the web browsing profile. Some time ago I created the "Simple Browser" profile for konqy, which quite radically removes stuff from the default layout. I use it since then and don't miss anything. But some things are not (yet) possible with konqy and profiles. So what can be done to improve the situation ?
Some points:
<ul>
<li>we should provide three konqueror profiles by default:
<ol>
<li> Swiss army knife - full featured as we know it today
<li> Webbrowser - tailored for web browsing, maybe based on "Simple Browser", with a simple layout similar to Safari or Firefox
<li> File manager - a simple file manager, maybe inspired by Rox Filer or Mac Finder
</ol>
For the Webbrowser and the Filemanager profile we need to radically simplify the user interface. This will inevitably mean that some things some people consider essential won't be there: maybe the print icon in the toolbar, the cut/copy/paste icons or others. These actions will still be  available either through the menu or shortcuts or the context menu. We don't have to put everything into the toolbars. For the users who want everything in the toolbars, there's still the "Swiss army knife" profile.<br>
<li>AFAIK currently all profiles share the same home URL. This must also be made a per-profile setting.
<li>The profiles should be hidden a bit from the user. Creating and switching profiles is IMO a developer/geek/power user feature. IMO we can simply remove the "Settings -> Load View Profile" item from the menu. The different profiles should be started either via the "Konqueror profiles" kicker button or dedicated konqueror buttons/links. I also don't think it is necessary that konqueror can switch profiles on the fly. It just complicates the code and leads to bugs.
<li>Using xlmgui and KParts it is possible that parts add their own actions to konquerors toolbars and menu. It is currently not possible that the xmlgui file of a part hides/shows actions belonging to konqueror. If this was possible, e.g. the khtml part could hide the "Copy/Cut/Paste" actions, and the directory (i.e. file managements) parts could show them again. 
<li>Maybe it should be possible to limit the range of ioslaves which can be used with certain profiles. So that e.g. file:/ is disabled in simple browser mode (although I wouldn't like this) or that http is disabled in file manager mode (which I wouldn't like either).
<li>Now that konqueror supports tabs, do we actually still need the splitting of the views anymore ? 
<li>General polishing: konqueror exposes a bit too much of it internals to the user. E.g. the concept of linked and locked views in konqueror is hard to grasp. It stems from KDE 2 times when we were proud to show we are able to do. It should be hidden from the user. Maybe a profile editor (where you can set up the views, link/lock them, set the home url, edit xmlgui etc.) would be a good idea.
<li>Konsole integration isn't really good. Same as above, we were proud that we are able to embed a konsole part in konqueror.
Konqueror comes with a midnight commander-like profile. Does it work ? Kind of. Do the views follow the embedded konsole ? No. Does the konsole follow the views ? If you have the right link/lock combination, yes. Is it easy to jump between the views, the url line and the konsole ? No. Tab in the konsole doesn't change focus, but acts as a tab in konsole. Then it has a shortcut to open a terminal in the current directory. And it has a shortcut to execute a command from the shell. IOW three ways to have something executed from a shell. I'd suggest to throw all three of them away. 
Instead add a shortcut which opens an embedded konsole in its own tab. The same shortcut should change back to the previously active view. This would make navigating between the view and konsole easier and faster and additionally provide more room for konsole.<br>
<li>There was even a Preview profile for konqueror. The sidebar with the directory tree, then a file view, and on the right side a locked view which displayed the file selected in the file view. This failed completely if you changed the directory from within the file view. Then the locked view and the file view both displayed the new directory.<br> A nice working preview profile would be nice to have. This can't be done simply with linking and locking views. AFAIK there is already a patch to implement something like this.
<li>Last but not least: konqy needs some dedicated developers. Currently the main work is going into khtml/web browsing, and not much into the file management part of konqueror.
</ul>
So, these are some issues which come to my mind and there are probably more things like this in konqueror. But IMO they don't require a rewrite or a split, just a good amount of polishing. With this in KDE 4 konqueror will be able to show its full potential as a powerful <b>and</b> userfriendly everything-browser :-)

Ok, enough for now.
Alex
