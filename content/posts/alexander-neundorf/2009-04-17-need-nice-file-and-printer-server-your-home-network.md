---
title:   "Need a nice file- and printer server for your home network ?"
date:    2009-04-17
authors:
  - alexander neundorf
slug:    need-nice-file-and-printer-server-your-home-network
---
Ok, this blog is not really KDE related (well, it makes the network installation of a KDE developer more convenient, so...), but anyway here we go.
Main purpose is to get the compatibility information out there, so others can find it.

So, I recently purchased a QNAP TS 109 Pro. 
This is a small NAS, i.e. basically it is a small Linux server. It has a 500 MHz ARM processor from Marvel, 128 MB RAM and its main purpose is to be used as a NAS. That's what I'm doing. For that, it supports among others NFS, Samba and also a web interface. I'm using it here over NFS from my different Linux machines.
Additionally it can also be used as a printer server over Samba.
This is also nicely working, both from SUSE 11.1 and Slackware 12.1. Finally we can now print wireless :-)

Some nice points of the device:
<ul>
<li> it's small, roughly the size of an external 5.25" drive
<li> it's very silent, no fans at all !
<li> in idle mode it consumes only 6W
<li> the power-on switch is also an power-off switch (AFAIK e.g. for the NetGear 2150 it is only a power-on switch)
</ul>

I'm using it with hardware which is not on the official compatibility list at QNAP, but it seems to work flawlessly.
So, the harddisk is a Samsung HD322HJ 320 GB Spinpoint F1. No problems until now, also very silent.
The printer is a HP DeskJet 5151, connected via USB to the QNAP and exported via Samba. 
An additional note: the HP5151 is AFAIK the same as the HP DeskJet 5150, just in different colors.


I just did a quick benchmark, reading 261 MB of data and piping it into /dev/null took 28.3 s, gives 9.2 MB/s.
I guess this is not too far away from the maximum one can get over a 100 MBit ethernet cable. And it's more than you can get over WLAN anyway.

So, all in all, a nice, silent and small device :-)

The TS 109 II has more RAM, but I think I don't need that.
The "Pro" versions of the TS 109 and TS 109 II support NFS (that's what I'm using), the versions without "Pro" AFAIK don't. 

Alex

P.S. one bad point: for setting it up they ship a Windows-only software, so I had to do this on somebodys else PC. AFAIK there is also a Linux-only way to set it up initially, but I didn't feel like doing this since it seemed slightly complicated.
