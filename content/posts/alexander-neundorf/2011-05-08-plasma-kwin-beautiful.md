---
title:   "Plasma + KWin = beautiful ! "
date:    2011-05-08
authors:
  - alexander neundorf
slug:    plasma-kwin-beautiful
---
Until last week I was still running good old Slackware 12.1 with KDE 3.5.something.

I had skipped Slackware 13.0, because it had still no support for virtual screens in X/xrandr, and the KDE 4.4 in Slackware 13.1 crashed a few times while playing around with it. So I stayed with 12.1. For building current KDE, it doesn't matter what desktop you're running, so that was fine for me.

Now last week the new Slackware 13.37 was released and I gave it a try. Virtual screens are back in xrandr (now called "panning"), good.
And it comes with KDE 4.5.5.

To make it short: it simply looks beautiful and it did not crash once since then (except the kwin+Intel+mesa crash Martin blogged about) !
Effects work flawlessly and smooth, the icon layout on the panel which I had problems with before works, I can use "#" again in krunner to open manpages, it's all there and looking great !
(nitpick: it would be nice if the krunner plugin selection dialog would also have a "Help" tab for each plugin, at least I don't know how to find out what all these krunner plugins are doing and how to use them).

I'm missing a little bit kpager as a standalone application, since the plasma widget cannot stay above all other windows, and I can't change the position of a window when dragging it to another desktop.

But I even kind of like bubblemon to display the load of my 4 CPU cores, until now I still had xosview running, but the bubbles are kind of cool :-)

The folder view for plasma is great, so much better than old kdesktop. I have now a few of them on the desktop, showing different directories, some of them filtered, so I see only the pdfs.

Kate has improved, alone that I can have multiple "Find in files" tabs open at once is great.

KMail -  still works, but the grouping has slowed things down a lot, before I had all threads always open, now I have to keep only the ones with new stuff open, so it is fast again.

Looking back, at the KDE Core meeting 2006 in Norway Aaron was talking about "organic" software and interfaces, and I thought "oh, big words, but show me code".
And the code did not appear for a quite long time (I think it was merged into trunk late 2006, and beginning 2007 was the planned release date), so plasma was not yet really ready when KDE 4.0 was released.
And now we are finally there, Plasma (and not only) is working: looking great, stable, fast, and, indeed "organic" or "elegant".
So, the vision has come true, and we are now at one level with other modern UI interfaces. So it was definitely worth it ! :-)
KDE 3 looks so old now...

Alex
