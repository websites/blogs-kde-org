---
title:   "KDE 4.2 release party in Frankfurt"
date:    2009-01-13
authors:
  - alexander neundorf
slug:    kde-42-release-party-frankfurt
---
Hi,

great events are coming closer: the release of KDE 4.2 is due in two weeks :-)

To celebrate this, Claudia is organizing a (small) release party Friday, January 30th in Frankfurt/Main, details here:
<a href="http://wiki.kde.org/tiki-index.php?page=KDE%204.2%20Release%20Party#_Germany_Frankfurt">
http://wiki.kde.org/tiki-index.php?page=KDE%204.2%20Release%20Party#_Germany_Frankfurt
</a>
So if you are from Frankfurt/Main or close to it, come and celebrate KDE 4.2 with us. If you want to come, please send an email to Claudia (rauch AT kde.org) (for now we have a table for 12 people reserved...)

I will come from Kaiserslautern, so if you are from K-Town or surroundings, I still have some places free in my car. In this case just send me an email to neundorf AT kde.org.

See you there :-)
Alex
