---
title:   "New maintainer wanted: network browsing"
date:    2008-01-04
authors:
  - alexander neundorf
slug:    new-maintainer-wanted-network-browsing
---
Around 2000 (back in KDE2 times) I wrote the so called LAN information server lisa (http://lisa-home.sf.net), which can you show the <a href="http://lisa-home.sourceforge.net/img/lan1.jpg">neighbours in your network</a>.
<!--break-->
This works by regularly sending ping packets on the network. In order not to flood the network too much, if there are several of these daemons running in one network, they collaborate and reuse their results.

Originally the idea was to get a "network neighbourhood" without having to rely on samba, domain controllers etc. So here you get a list of every host which responds to ping. Once lisa has this list, KDE comes into play. There is an ioslave for it, lan:/, which on the first level displays the hosts, and if you enter a host, it can check some ports (smb, nfs, ftp, http, ssh) to see if these services are available on that host.
All that is implemented and should work. The only hard thing is the configuration, which has to be done somewhat manually.

Now, the problem is, since a few years I don't really have a need for that anymore, and so did not really work on it.

So: if that sounds interesting for you, how about becoming the new maintainer ? The code has been moved to <a href="http://websvn.kde.org/tags/unmaintained/4/lanbrowsing/">kdesvn/unmaintained</a>
 yesterday.

What needs to be done ?
At first it must be made sure that it works properly within KDE 4. In the last years we have seen the development of Bonjour (Rendezvous, Zeroconf, what's the official name) for service discovery. This is quite similar to the purpose of the lan browsing stuff. Maybe lisa is just not needed anymore ? But maybe a better idea would be to announce the existence of a lisa server over zeroconf, so that other hosts can connect to that lisa daemon (which should work already right now using broadcasts, i.e. without zeroconf, but this is not much tested, so I don't know how reliable it works). Configuration should be made more automatic.

So, if you are interested, send me a mail (neundorf (AT) kde (DOT) org), of course I'm available to get you running, answer questions, etc.

Looking forward to you :-)
Alex
