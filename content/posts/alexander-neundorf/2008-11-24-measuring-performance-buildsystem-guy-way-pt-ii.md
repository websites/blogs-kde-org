---
title:   "Measuring performance the buildsystem-guy way..., Pt. II"
date:    2008-11-24
authors:
  - alexander neundorf
slug:    measuring-performance-buildsystem-guy-way-pt-ii
---
Back in April I did some very rough <a href="http://blogs.kde.org/node/3444">performance measurements</a> of my new notebook vs. my desktop machine: 

Back then I got the following numbers for a complete build of CMake:
<!--break-->
Desktop, AMD Athlon XP2000+: 3:59 min
Notebook, Intel Core 2 Duo: 1:15 min

The conclusion was that it's time to upgrade my development machine. I did that last week.
So, the new machine is a Intel Q6600 Core 2 Quad. How long will this new CPU take to build CMake, in all its quadcore glory ?

Here's the number for make -j4: 0:33 min

Yes, that's 33 s compared to 359 second on my "old" machine.
That's more than 10 times as fast !

Now that's a real big improvement ! :-)


Before that Intel all CPUs I bought (except notebooks) were AMD. Why did I go for Intel now ?
Two reasons:
<li> it compiles faster: http://www.tomshardware.com/charts/desktop-cpu-charts-q3-2008/Linux-Kernel-Compilation,841.html
<li> there are mainboards which have Intel graphics onboard, i.e. somewhat unproblematic graphic drivers (that's the more significant reason)

Beside that, I would have probably gone for AMD again. 

Now there is also a new shiny Slackware 12.1 running on the box. Installation went smoothly, actually I didn't have to know anything, everything was configured fully automatically. Almost too easy. But of course in good old Slackware-style still completely in text mode :-)
And also Slackware 12.1 still uses Lilo (i.e. no Grub). And this time Lilo even comes with a very stylish Slackware-themed graphical boot menu :-)

Btw. the notebook will probably get a new SUSE 11.1 as soon as it is released. I heard just from too many problems with kUbuntu 8.10. Both kUbuntu 8.10 and SUSE 11.1 are AFAIK the first distros which will come with a kernel version with improved support for the Intel 3945 wlan. It's actually not really nice to have to upgrade the distro in order to get a newer version of a device driver.

Alex

