---
title:   "[OT] How to get Webex working on SUSE Linux 12.2 64bit"
date:    2013-02-05
authors:
  - alexander neundorf
slug:    ot-how-get-webex-working-suse-linux-122-64bit
---
Sorry, for posting this on blogs.kde.org, but I think it might help a few people out there having the same problem.

So, if you want to get webex working on a 64bit Linux system, basically all you have to do is make sure that all necessary 32bit libraries are present. 64bit firefox with 64bit java are ok.
When you join a webex session, a directory ~/.webex/ is created. On my system there is a subdir 1324/ in it, and in this directory there are a bunch of 32bit ELF plugins, named *.so.
Run ldd on all of them, and check which shared libs are not found, and install them (the 32bit versions). Once I had done that, desktop and application sharing worked for me.
It seems you can ignore the missing libjawt.so, everything seems to work fine without it.

If somebody knows how to make the Whiteboard feature or video work, let me know. :-)

Hope that helps.

Alex
