---
title:   "KDEs printing support rocks :-)"
date:    2006-04-25
authors:
  - alexander neundorf
slug:    kdes-printing-support-rocks
---
So I fiddled around with hardware last week, accepted that my board really doesn't like DDR400 RAM and got my first SATA disk working, main problem were BIOS issues. Well, finally I have enough disk space for the complete KDE/trunk/ again :-)

Today I noticed a side effect. I tried to print on my HP 5151 from kate, but the printer didn't do anything. It had a red sign on its icon. Maybe this is intended as some kind of warning sign that there's something wrong.
What the hell could be wrong ?
"Maybe I can simply adjust it in the KDE control center, printer management" I thought.

So I opened the control center, entered the printer management tab and changed to admin mode.
I noticed that my old printer which I don't have anymore since 2 years or so, was still installed. So I selected "Remove" and it was gone. Then I changed to the HP printer, and saw the status "Stopped". Hmm, why is it stopped ? How to get it going again ?
Well, right clicking on the printer icon opened a menu which contained among others an entry "Start". So I clicked start, closed the control center and tried again to print in Kate.
And, voila, the printer printed :-)

Did anybody say printing support under Linux is poor ?

Alex
