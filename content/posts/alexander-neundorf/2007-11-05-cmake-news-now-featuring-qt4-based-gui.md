---
title:   "CMake news: now featuring a Qt4 based GUI"
date:    2007-11-05
authors:
  - alexander neundorf
slug:    cmake-news-now-featuring-qt4-based-gui
---
Hi,

CMake 2.6 will rock. Latest news: Clinton Stimpson has started to work on a Qt4 based GUI for CMake,<!--break--> so there will be a modern GUI to CMake also on UNIX, here's a screenshot from CMake cvs today: [image:3080].

The first commit is only a few days ago and it is already working nicely. Now that there is a GUI written with the best toolkit in the world ;-), there is a lot of room for cool features and improvements (ok, as cool as a feature for something as boring as a buildsystem can be... ;-) ).
I guess we need a Plasmoid for it, to make it more sexy ;-)

Anyway, if you feel like hacking on this, join on the cmake list :-)

Now to something completely different: all my three distros I currently use are working absolutely flawlessly, Slackware 12 is as great as only Slackware can be :-), to get WLAN working on kUbuntu 6.10 all I had to do was to install wireless tools and to plug the card in (really, I just plugged it in and it just worked), and kUbuntu 7.10 on my new work machine also just works, even with OpenGL support enabled automatically (Intel graphics :-) ).

That's it for now
Alex
