---
title:   "A new book on CMake"
date:    2018-07-10
authors:
  - alexander neundorf
slug:    new-book-cmake
---
Hi,

there is a new book on CMake (there are not many):
<a href="https://crascit.com/professional-cmake/">Professional CMake - A practical guide</a>.

I haven't read it yet, so I cannot say more about it, but I guess it should be useful.
