---
title:   "LinuxTag 2005"
date:    2005-07-01
authors:
  - alexander neundorf
slug:    linuxtag-2005
---
I'm a bit late, but I also want to share some of my impressions from LinuxTag last week. 
As always it was fun. It's always nice to meet with other OSS developers, be it gnomes, X devs, the mplayer guys and of course, our KDE fellows :-)<br>
A big thanks goes to the people who organized our KDE booth, so especially to Joseph Spillner this year. We had four nice boxes there, two running SUSE, and two running kUbuntu, both very nice although quite different distributions.
SUSE is good for exhibitions since a lot of the visitors are using it and so if they have problems it's easier to help them if you have the same system at hand. The kUbunto installations OTOH were more up to date. On all machines there was also amarok and kaffeine installed, kpdf and kdevelop had to be installed afterwards.<br>
There were a lot of questions related to KDE PIM: KMail, Kontact, so ideally there should always be at least one KDEPIM person around. As usual the top question was "what's new ?"
I think at the booth there should always be one box running KDE HEAD, just to be able to demonstrate new cool stuff. And of course there were questions about KDE 4. I tried to do my best, talked about the split of the Qt lib, the lower memory requirements and the fewer symbols to resolve, multimedia, arts, gstreamer and NMM and usability.<br>
<br>
The announcement of the cooperation between Wikipedia and KDE was quite impressive to me. Is this now really the beginning of a new era ? Wikipedia contains really a lot of information, available freely on the Internet. All this content is created by small contributions of individuals. Which company would be able to create such a big pool of knowlegde itself ? <br>
NMM, the network multimedia middleware seems also to be taking the lead in well, network-enabled multimedia stuff. Stream video and audio seemlessly between different devices, different operating systems, it stays synchronously, the are no gaps, really impressive.<br>
And all these things can now be used by free software applications. This opens up completely new opportunities, maybe superior to todays commercial software. Has it ever been possible for individuals to create things made up from such powerful components ? ... standing on the shoulders of giants...<br>
<br>
Last but not least a list of people/projects who were <b>not</b> at LinuxTag:
<ul>
<li> SUSE/Novell, the biggest distro in germany, why didn't they come ?
<li>Trolltech - ok, Matthias Ettrich and Jasmin were there, but why didn't Trolltech have a booth ? (I also missed them at Embedded World... )
<li>MAS, the Media Application Server by X.org - the last years there was always Silvio from Shiman Inc. there, but now he doesn't work there anymore. Is MAS still alive ?
<li>Fresco/Berlin - once designated as the successor to XFree, this is now the second year that they don't have a booth anymore. Is the project still alive ?
<li>Scribus - ok, Klaus Schmidinger was there as a visitor
</ul>

And some booths who <b>were</b> at LinuxTag:
<ul>
<li>Microsoft - what are they doing there ? (I think they should consider porting MS Office to Linux, I guess they could sell a lot of copies)
<li>WorldForge - an open source network enabled role playing game/gaming engine, since years they are there, hopefully they'll will be able to create something playable in the next few years. They reached already very much, but without users (gamers) the project might be endangered in the long run.
</ul>

So, I guess that's more than enough for a blog...
Tomorrow the Tour de France starts, so there'll be other priorities :-)
<br>

Alex
