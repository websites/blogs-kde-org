---
title:   "SUSE 11.2: Very nice, but networkmanager still doesn't like me "
date:    2010-01-05
authors:
  - alexander neundorf
slug:    suse-112-very-nice-networkmanager-still-doesnt-me
---
Now finally yesterday I installed OpenSUSE 11.2 on my notebook (<a href="http://blogs.kde.org/node/3400">this one</a>).

Installation went very smooth, and it seems all the hardware components were recognized automatically, 3D graphics, even WLAN.

Only issue, it still seems modern networking (aka networkmanager) doesn't like me. Or I am too stupid.

So once again I tried to use networkmanager, from a KDE4 workspace (correct term ? I didn't look up...)

So, what happened.
When I installed SUSE, it asked me for the configuration of the network hardware, including WLAN. I entered essid, encryption etc.
Wired network was working, that's good.

Then I looked at the networkmanager applet (correct term ?) in the panel.

There I saw the eth0 connection as active. So far good. This was correct. I tried to disable it. So I clicked on that icon. It activated the already active connection again. Hmm. I didn't find a way to disable it.
I went to the "Configure connections" dialog. 
No wired connection there, although it was clearly working and also displayed in the menu.
Also no wireless connection, although I entered all the data during installation.

So I tried to get a wireless connection first.
This is now my biggest issue with network manager. 
I entered all the connection data, and the connection appeared on the list were I could chose connections.
I clicked on it.
It did nothing. I mean nothing. No "Success" message, no "Sorry, something went wrong" message, just nothing. And I think I really tried all things in the UI.

The eth0 connection also said "system connection", so I thought maybe that's way I can't disable it. So I went to yast, which told me "networking is managed by network manager, you can't change the settings here".
Hmm. I tried a few times, until I skipped that message, switched in yast to traditional style networking, and disabled the "start on boot" option for eth0, then switched to network manager again.
Still no luck, I was not able to disable eth0 and to enable wlan0.

So I gave up on network manager once more. 
Something must be very wrong here. Apparently there must be people for which it is working, for me it never did anything :-/


So, back to yast, I switched to traditional networking (ifup/ifdown) and "Manual" activation of the interfaces (since I didn't find a way to specify that eth0 should be active if plugged in, but if not plugged in, it should try wlan, and disable wlan again if eth0 is plugged in).
This didn't go too smooth.
Yast disabled the networking due to this, ok, and then tried to download two additionally required packages from the online SUSE repository.
Obviuosly this failed, and it told that this is quite bad. Hmm, so what.
I also checked the checkbox "make device available to kinternet". It didn't install kinternet automatically.

So, obvious next step, I manually enabled eth (ifup), went to yast again, installed the failed packages and also manually selected kinternet, and voila, basically I got it working. 
It would be even nicer if in this case kinternet would be started automatically for all users when logging in to KDE, but that's not a big thing.

So now I have eth0 and wlan0 working nicely, and can enable/disable them via kinternet. 

The big good news about this: SUSE automatically chose a working kernel module for my wlan card. That's the first time with this notebook, SUSE 11.1 chose a non-working one, and so did the factory-installed kubuntu. Now this time it was just working :-)

Anything else I could complain about... hmm... not much.
I'm not sure why cmake is listed in the "KDE development" section and not in the general "C/C++ development" section, I think it should be there.
Also, if cmake is installed and Qt4 is installed, I think cmake-gui should be also automatically installed, i.e. it should at least be part of the "KDE development" preselection.


Beside all that, together with some FreeBSD, Solaris, kdeutils and Kitware guys we are in the process of setting up Nightly builds for several KDE modules using <a href="http://www.cdash.org">CDash</a>, look for the the kde-related projects at http://my.cdash.org, hosted by <a href="http://www.kitware.com">Kitware</a>.
It's almost in a state that it can be announced, what's still not done is proper documentation how to set Nightly builds up, and we're still fiddling with more fine-grained email notifications. But this will come :-)

Happy new year
Alex

P.S. this year I won't be at FOSDEM, but most probably Akademy :-)




