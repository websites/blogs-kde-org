---
title:   "How to change the X11 DPI under kUbuntu"
date:    2008-04-30
authors:
  - alexander neundorf
slug:    how-change-x11-dpi-under-kubuntu
---
I don't know why, but often I have the impression that KDE fonts are quite big, and this can be changed by modifying the X11 dpi settings.

Let's head over to good old Slackware. How is it done here ?
<!--break-->
Enter /usr/bin/startx, which starts since ages with 
"# This is just a sample implementation of a slightly less
# primitive interface than xinit.
...
# Site administrators are STRONGLY urged to write nicer versions.
"

At the end there is the line:
xinit $client $clientargs -- $server $display $serverargs

If I append e.g "-dpi 75" to this line (or set it correctly in one of the args), X will start with this dpi setting.

Now let's try the same with (k)Ubuntu 7.10. /usr/bin/startx looks just the same, including the comments that the site administrator should do something. What happens if we append -dpi 75 here ?
Nothing.
Why ? Because, as I found out, xinit reads here /etc/X11/xinit/xserverrc, which looks like this:
<pre>
exec /usr/bin/X11/X -dpi 100 -nolisten tcp
</pre>
So the dpi is already set here. So let's change it here too. Now, voila, if I run startx, X starts with 75 dpi :-)

Ok, next step. Now I login via kdm. Again I'm back ti 98 dpi. So it seems here it works in some other way.
How to find out ?
<pre>
$ cd /etc
$ grep -Ri dpi *
</pre>
which yields /etc/kde3/kdm/kdmrc with this line:
<pre>
ServerArgsLocal = -nolisten tcp
</pre>
So here X starts without special setting and defaults to 98 dpi. So let's append -dpi 75 here too and restart kdm.

Now, kdm starts with 75 dpi (and small fonts), so I continue and login to KDE. What do I see ? Big fonts, i.e. it still looks like 98 dpi. What does kinfocenter (or xdpyinfo) say ? Both report 75 dpi.
Now this seems weird. X reports 75 dpi but fonts look like 98 dpi ? Hmm.

Let's try again, maybe there is something more:
<pre>
$ cd /etc
$ grep -Ri dpi *
</pre>
Indeed, there is also a /etc/X11/Xsession.d/40guidance-displayconfig-restore, which talks about DPI. And it calls /usr/bin/displayconfig-restore. Now let's see what this does. It is a pyhon script, and it has a function FixXorgDPI. This function sets the dpi according to some calculations. Disabling this function (e.g. by inserting return right at the beginning) helps, finally kdm and KDE and X start with 75 dpi ! :-)

Conclusion ?

It may have been meant good, but please, don't implement hacks which circumvent the normal system tools. If I set all possible parameters to the dpi value I'd like to have, I don't want some clever custom script to completely ignore what I said explicitely.

Alex

P.S. I guess the same is true for NetworkManager, which sits in the background and fiddles around with network interfaces, making it impossible to manually debug problems...



