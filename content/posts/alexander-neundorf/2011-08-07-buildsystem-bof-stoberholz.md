---
title:   "Buildsystem BoF at St.Oberholz"
date:    2011-08-07
authors:
  - alexander neundorf
slug:    buildsystem-bof-stoberholz
---
Hi,

the KDE buildsystem BoF/meeting/whatever you want to call it/ will be Monday, at 8:00 PM (i.e. in the evening) in the restaurant/cafe St.Oberholz:
<a href="http://www.sanktoberholz.de/?page_id=13">http://www.sanktoberholz.de/?page_id=13</a>

According to Claudia they are used to geeks sitting around and discussing :-)

The address is 
Sankt Oberholz
Rosenthaler Straße 72a
10119 Berlin

Places are reserved for us on my name (neundorf).

St. Oberholz should be 15 to 30 minutes walking from here.
I think I'll leave here from the university building at 6:30PM. Then there  should be enough time to have dinner before 8:00PM.

So either you'll just be there at 8:00PM, or we walk there together. I'll be here at the registration at 6:30PM.

Alex
