---
title:   "Ruby everywhere"
date:    2006-09-21
authors:
  - alexander neundorf
slug:    ruby-everywhere
---
Recently it seems to me that Ruby is now really taking off, everywhere you hear news about it.
You know, there is Ruby on Rails, which is really the hype nowadays.
And there is an effort to get a Ruby implementation for .NET: http://plas.fit.qut.edu.au/Ruby.NET/ .
There has already been a project to implement Ruby on the Java Virtual Machine, and now the two main developers have been hired by Sun to work fulltime on JRuby :
http://headius.blogspot.com/2006/09/jruby-steps-into-sun.html :-)
...since there are some processors which support Java execution in hardware (e.g. http://www.arm.com/products/esd/jazelle_home.html, http://www.jopdesign.com/) , does this mean these processors will be able to execute JRuby in hardware ?
Ruby for embedded !
And as if this wouldn't be enough, there are also people working on running Ruby on Parrot, the Perl6 runtime:  http://www.parrotcode.org/news/2006/Parrot-0.4.6.html .

So it really seems Ruby is everywhere today :-)

Alex
... who will leave in 36 hours for akademy :-)
