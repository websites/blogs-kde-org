---
title:   "kubuntu 8.10 and KDE 4.1 rock !"
date:    2008-10-08
authors:
  - alexander neundorf
slug:    kubuntu-810-and-kde-41-rock
---
Just a short note: yesterday I booted my laptop with a brandnew kubuntu 8.10 beta CD.

How to put it, this rocked so much !!!

KDE 4 really is beautiful !

The graphics effects were running smoothly, it seems integrated Intel graphic was indeed a good choice :-)
And also now finally the Intel 3945 WLAN just worked, using the iwl3945 driver :-)
With kubuntu 7.10 this was still really much more fiddling around than I expected, to get it finally somewhat working with the ipw3945 driver.
But 8.10 - it just worked :-)

Alex
