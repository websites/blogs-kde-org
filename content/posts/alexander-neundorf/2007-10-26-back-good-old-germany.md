---
title:   "Back in good old Germany"
date:    2007-10-26
authors:
  - alexander neundorf
slug:    back-good-old-germany
---
Maybe some of you have already noticed, I'm back in good old Germany. Since end of April I was in Clifton Park, New York, working at <a href="http://www.kitware.com">Kitware</a>. This was a really great time. <!--break-->
I learned a lot about the US and visited a lot of beautiful and interesting places, beside the usual also locations which are a bit less known (in Germany at least) but not less beautiful: the <a href="http://visitadirondacks.com/"> Adirondacks</a>, a great place for hiking and relaxing and the peninsula <a href="http://www.capecodchamber.org/">Cape Cod</a> just south of Boston (really beautiful city !).

Basically everybody we met in the US was very nice and welcoming, and somehow more relaxed than the average German guy. Working at Kitware was a really good experience, a relaxed atmosphere and great people to work with ! (Hi Bill, Brad, Berk, Dave, Utkarsh and all the others :-) )

I had the chance to work on <a href="http://www.paraview.org">ParaView</a> (which now also runs on supercomputers) and (of course) on <a href="http://www.cmake.org">CMake</a>.
How to put it, CMake 2.6 will feature a lot of cool new features, almost everything what CMake 2.4 didn't support is now available: building frameworks on OS X, cross compiling to arbitrary target platforms with or without OS, improved documentation, project generators for Eclipse and CodeBlocks (both still need more testing !) and an improved CPack, which can now also create RPM and Deb packages !

Unfortunately the time went by too fast, but now a new challenge is already waiting for me, I will start working as a research assistant at the Technical University of Kaiserslautern in the department for <a href="http://rts.eit.uni-kl.de/"> Embedded and Realtime Systems</a>.

KDE related stuff ?

Exciting times ! The KDE Development Platform is basically frozen, which is a good thing for me, because it means as long as there are no bugs there will be no big changes to the CMake stuff for KDE. Some things will need to be addressed for KDE 4.1, probably several Windows- and maybe OS X related issues will get attention. Right now still CMake 2.4.5 is required to build KDE, and this will also stay that way for all KDE 4.0.x releases (of course CMake 2.4.6 and 2.4.7 work too). I noticed the lively debate about KHTML and WebKit. It's a bit unfortunate that WebKit doesn't have "KHTML" in its name, I guess that would make things easier and more obvious that it's based on KHTML. I guess an HTML component with  developers and support from several companies (Apple, Nokia and more) will have less bugs and be more powerful and standards compliant in the long term than an HTML component developed "only" by KDE developers (it's simply less man power). We'll see how this works out.

Alex
