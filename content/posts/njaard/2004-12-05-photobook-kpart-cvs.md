---
title:   "Photobook KPart in CVS"
date:    2004-12-05
authors:
  - njaard
slug:    photobook-kpart-cvs
---
I just committed my "Photo Book" part to cvs, it will be in KDE 3.4 unless I'm flamed.

It's neat I think, and it probably fulfills a lot of wishes too.  What the hell is going on here? I'm not supposed to be committing new features!  Really now, KParts is an amazing (and complex) API.  The kind of stuff you can do with it with just one-liners is incredible... the problem is finding the right one-liners.

 <img src="http://ktown.kde.org/~charles/photobook.jpg" />

In other news, my flatmate is DoSing our internet connection by downloading very big files.  He should be done soon, I hope.  Meanwhile, I'm trying to find a way to limit the speed of his transfers with those LinkSys 802.11g access points/routers (the ones that run Linux!).. short of installing my own Linux on it.

Also note my new "hackergotchi" on kdedevelopers.org!