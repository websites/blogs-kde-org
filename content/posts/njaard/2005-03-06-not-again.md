---
title:   "Not this again"
date:    2005-03-06
authors:
  - njaard
slug:    not-again
---
<a href="http://hokev.brinkster.net/quiz/default.asp?quiz=Better+Humor&amp;page=1">Apparently</a>, I am a DYO--Dark Dry Offbeat. This makes me a Neurotic.

I believe it.  The complete analysis was head-on, in my opinion, but that might just be the result of <a href="http://en.wikipedia.org/wiki/Confirmation_bias">Confirmation bias</a> (the tendency to see accuracy in astrology, for example).

Oh, and take a look at <a href="http://www.derkarl.org/pics/westward.jpg">this</a> -- it's really pretty.
<!--break-->