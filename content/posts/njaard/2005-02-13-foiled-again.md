---
title:   "Foiled again"
date:    2005-02-13
authors:
  - njaard
slug:    foiled-again
---
I spent practically all of today working on Noatun make-it-snow. My fun was cut short when I (re)encountered a rather serious akode bug. It's some sort of race condition related to creating a playobject, and then, uhm, trying to get it to play.

This means that I can't commit what I have. Also, my installed noatun is broken, which means that when I exit the working one, I won't be able to listen to music. If I can't listen to music, I can't code, and if I can't code, I can't fix noatun, which means I won't be able to listen to music.

See the problem?

I have a table bug to fix in khtml, but it will involve lots of new code, and unfortunately I really rather get my noatun changes in cvs so that Stefan can work on it as well, again. The patch is more than 4500 lines!

In other news, I got bored last night and completely redid my <a href="http://www.derkarl.org">web site</a>. In the process, I found a PHP bug related to XML parsing. Every page is now XHTML 1.0 Strict compliant, and on all the "proper" pages, there's no presentational markup! It's also now more of a "personal" page.

I went a bit nuts with the RSS there. The good news is that the RSS feed I provide for KDE development is improved and works fine in akregator. You're all free to use it; it's useful for providing progress of your software's development on its web site.

On Tuesday I'll be seeing Tristania and Nightwish in Birmingham, which I do look forward to! I must say, lots of Sirenia fans don't seem to like the new Tristania album (named Ashes), but I think it's very nice (however different from what they've done in the past). I feel it's a very unique album in itself, but I find that all the songs are kind of similar to each other.

Why is it that every time there's a band I want to see, they're always the supporting talent? Tells me something about my tastes.

Oh, and Waldo Bastian is a <a href="http://lists.kde.org/?l=kde-core-devel&amp;m=110831597025113&amp;w=2">hero</a>.

<!--break-->