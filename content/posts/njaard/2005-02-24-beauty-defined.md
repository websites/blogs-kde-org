---
title:   "Beauty defined"
date:    2005-02-24
authors:
  - njaard
slug:    beauty-defined
---
How was I living without snow all this time?

This stuff is amazing!  The essence of beauty.

<a href="http://www.derkarl.org/~charles/pics/snow.jpg"><img src="http://www.derkarl.org/~charles/pics/snow-thumb.jpg" /></a>  <a href="http://www.derkarl.org/~charles/pics/snow2.jpg"><img src="http://www.derkarl.org/~charles/pics/snow2-thumb.jpg" /></a>

Truly the best things in life are all around us, why do so many dislike such a lovely gift from nature?

(To put this in context, we've been recieving snow here since Monday, and I've been enjoying every moment of it)

<!--break-->