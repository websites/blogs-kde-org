---
title:   "They should send their excess vowels to eastern europe"
date:    2005-01-12
authors:
  - njaard
slug:    they-should-send-their-excess-vowels-eastern-europe
---
I returned from The Netherlands yesterday. Fun was had.

First, I got myself an insane <a href="http://ktown.kde.org/~charles/cut.jpg">haircut</a>.

Then, I saw <a href="http://www.sirenia.no">Sirenia</a> (and also Tiamat, Pain, and Theatre of Tragedy).  I sure do hope they come to England soon; it's difficult having to fly to the continent just to fulfill my <a href="http://ktown.kde.org/~charles/o13ticket.jpg">obsession</a>. Yes, I was in the front row.

It was also very nice of the Dutch to name a <a href="http://ktown.kde.org/~charles/spanjaardsbrug.jpg">bridge</a> after me.

Note to all: the people in customs are very suspicious when you're carrying 5kg of boxes of hagelslag from the Netherlands.

As I did last week, I'll give you all something to read and think about: <a href="http://www.midwinter.com/lurk/making/warprayer.html">The War Prayer</a> by Mark Twain. Maksim Orlovich introduced this one to me and it's an exceptional work. Furthermore, I should point out that Mark Twain is one of the greatest masters of the English Language, and this story demonstrates that finely.
<!--break-->