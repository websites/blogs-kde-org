---
title:   "Wintersend"
date:    2005-03-17
authors:
  - njaard
slug:    wintersend
---
12°C today, and this marks the beginning of the long downward spiral into summer.

English summers are awful, the raindrops form out of the air all around you.  The drops hit you and you wake up hours later dazed and with concussion.

Californian summers are of course much nicer, but you still get waves of blistering heat sometimes. I love the winter. I like to wear long coats. I love the winter, not the grey skies, but the wind and the snow (at least when I'm in Britain). The sunlight shining off the rain-soaked pavement. Wintertime.

It means that I'm one step closer to returning home.  Once this summer begins, I've just a year left of university.

Oh, by the way, my computer "arkimedes" is back and in working order; time to readapt to its US-keymap.

Happy new KDE 3.4 release!
<!--break-->