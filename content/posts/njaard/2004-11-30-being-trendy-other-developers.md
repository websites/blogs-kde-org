---
title:   "Being trendy like the other developers"
date:    2004-11-30
authors:
  - njaard
slug:    being-trendy-other-developers
---
This is my first online journal ever. Some of you call them "blogs", but I don't, because the word is too silly sounding.

I actually did some coding today: <a href="http://ktown.kde.org/~charles/tooeasy.jpg">Image Galleries</a> for Konqueror! (Also note the photographs of fellow developers in compromising positions). It's just far too easy to do stuff like this.  KParts really are that amazing, and the really cool part is how we get stuff like network transparency for free!  Due to my incredibly short attention span, we'll see how far I develop this before losing interest.

At the university today, a friend and I found a computer with a broken CD-ROM drive, so we set ourselves upon the task of installing Debian on the machine without even so as much as a floppy disk.  Conveniently the network card supports network boot which means that we get to install stuff like tftpd and such.  It boots up fine, but the default debian ramdisk image lacks a lot of stuff.  I think we'll end up making our own ramdisk, but I think it'd be much more cool to have it properly boot up over ethernet without even touching the HD.  Speaking of this, if you're at a British academic institution, just visit http://mirror.ac.uk and download something, I easily get 4MiB/s, and those servers are clear across the country.

Debian's installer (or at least the version I have) is incredibly broken.  The default kernel (Linux 2.4) apparently does not support hard-disks at all, fortunately the boot loader on the CD lets you pick Linux 2.6.  Even then though, it doesn't let you select reiserfs as your root filesystem. My main complaint with ext2/ext3 is that if there's disk corruption, fsck may block startup and require someone with the root password to intervene.  Reiserfs just fixes the disk and goes on its merry way.  And now that all the bugs have been worked out, it's easily my favourite filesystem.  Just for comparison, try deleting your kdelibs CVS checkout on reiserfs, and again on ext2; the speed difference is insane.