---
title:   "Presentation"
date:    2005-05-13
authors:
  - njaard
slug:    presentation
---
Next week, on Wednesday the 18th of May, I will be overseeing a KDE Flamewar (and talking about how to tweak KDE) for the <a href="http://www.nottingham.lug.org.uk/">Nottingham Linux Users Group</a>.

Come one, come all!
<!--break-->
