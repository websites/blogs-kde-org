---
title:   "KDEBluetooth 1.0 beta2 Released!"
date:    2006-10-16
authors:
  - rockman
slug:    kdebluetooth-10-beta2-released
---
After a long waiting, KDEBluetooth 1.0 beta2 got released!
Release hilights include compilation fixes, and now works with lastest versions of bluez and openobex
Including also some stability fixes, and header exporting to allow other application to use kdebluetooth libraries. KMobileTools 0.5 will use them.
<a href="http://sourceforge.net/project/shownotes.php?group_id=89888&release_id=455943" target="_BLANK">Release notes</a>
<a href="http://sourceforge.net/project/showfiles.php?group_id=89888&package_id=147546&release_id=455943" target="_BLANK">Download files</a>