---
title:   "Junior Job for KMobileTools"
date:    2007-06-18
authors:
  - rockman
slug:    junior-job-kmobiletools
---
As i'm currently <u>very busy</u> with university in these days, and also i've to code both kde3 and kde4 branches, i was wondering if someone is willing to contribute with a not-too-complex application to be rewritten.
The application is kserialdeviceemulator in kmobiletools/tests (kde3).. i tried to use it to solve a (damn) bug with PDU encoding, and i noticed that the emulator itself is more and more buggy than kmobiletools :P
So that's what i had in mind:
<ul>
<li>Rewrite it in Qt4 (but not KDE4, i don't think it's necessary, and it'll also allow us to release it as standalone)</li>
<li>Use Model/View for managing data.</li>
<li>Use QSettings as default file load/save, and using kmobiletools log only to IMPORT data. This way we could import a log, save to an ini file, and fix the ini itself instead of trying to fix the log decoder (which is supposed to fail anyway, since it's not a simple task).</li></ul>

If anyone wants to do this i'll be happy to give him more details, as well as sample log files.
Note: you NEED a linux kernel, since the emulator itself uses the USB Gadget Serial kernel module to emulate a serial port.
Of course if you know about a better solution to emulate a standard AT-Commands modem, you're welcome ;)