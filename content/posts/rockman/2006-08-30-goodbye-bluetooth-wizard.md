---
title:   "Goodbye bluetooth wizard!"
date:    2006-08-30
authors:
  - rockman
slug:    goodbye-bluetooth-wizard
---
Well, i finally had enough time and ideas to add direct support for bluetooth in kmobiletools.
It was more tricky than i though anyway..
First i had to update kdebluetooth to make it export some classes, and add a new one, RfcommSocketDevice, which is similar to the existant RfcommSocket, but more usabile on threading.
Then.. well, i had only to learn their API, and to find a new good UI for my "New Mobile Phone Wizard". I simply added another page dedicated to bluetooth, which can scan over devices, find names, and services. Later i'll also add an automatic "good service discovery" which will find the best service for KMobileTools.
New stuff then.. adding a mobile phone is even easier, since you don't need to run ANOTHER wizard for bluetooth binding, and the rfcommsocket seems a bit faster than the rfcomm binded serial device.
Bad news: to use it you have to upgrade kdebluetooth to trunk, of course :P
Good news: you don't have to recompile the WHOLE kdebluetooth.. just configure it, then enter in "libkbluetooth" and type "make install". That's all!