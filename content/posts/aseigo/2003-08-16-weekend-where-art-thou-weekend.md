---
title:   "weekend, where for art thou weekend"
date:    2003-08-16
authors:
  - aseigo
slug:    weekend-where-art-thou-weekend
---
this was the week that wouldn't end. plus 30 degree weather, tons of smoke from forest fires, work work work work ... but, bliss! the weekend is upon me!
<!--break-->
i did get some KDE stuff done this week, mostly a bunch of kicker applet hacking... but most personally satisfying is how ksnapshot now looks and works. it has the general cleanliness that it had in early KDE2, but with several more features and a more standard and understandable interface. it's been an unusually long road with that app.

i'll be working on TOM this weekend, and publishing a tutorial for non-coders on how to add WhatsThis support to KDE applications. give some of the kde-usability subscribers something to do ;-) but I really want to see how much of TOM I can get _finished_. the design is pretty well complete in my head and on scraps of paper, i just need to code the damn thing =)

in other news... it looks like the nonsense bug bit a few people in the KDE project this weekl. there was a CVS commit fight as someone decided to revert a commit by Waldo in a knee-jerk reaction to his CVS log comment rather than actually taking the time to read the code. i respect the reverter's vigilance towards issues that he cares about, but c'mon: at least get it right when you accuse someone as acting as their company's shill by fighting a legal issue in CVS. what's really ironic is that according to the cvs list, he hadn't made a CVS commit that involved any substantial code this year until now. so far this year, his commits had been limited to changing information in the about data in his apps, yet he feels empowered to police KDE's CVS against the evil forces of Gingivitis and other SuSE agents!

oh, and then there was the widely cross-posted email requsting that people at N7Y do all their work in a CVS branch out of respect for those who won't be there. yeah, that's a great idea: let's waste time at KDE's most productive meeting for absolutely no benefit. "But think about the children^Wpeople who can't make it!" whatever... i'm one of those people who can't make it, and i still think the idea to do everything in a branch is stupid

oh well... maybe i'm just cranky because of the heat.