---
title:   "Today vs Not Today"
date:    2003-12-09
authors:
  - aseigo
slug:    today-vs-not-today
---
Every time I read a bit of news regarding the Linux desktop, I find myself asking myself: are they reporting on something that has happened or that will happen in the future? I fear that many of our colleagues in the Linux desktop struggle are trying to sell a tomorrow that doesn't exist yet because they feel that they have, so far, lost. This has the potential to blow up in so many horrible ways: people will start expecting tomorrow's abilities today; people will wait until tomorrow rather than act today; competitors will see where we are headed before we get there and be able to act with initiative; those promising may not deliver or even be around in their current form to deliver; what we want to accomplish today may be scrapped in favour of a better idea tomorrow. KDE is very good at avoiding all this. Let's make sure we keep it that way. =)<!--break-->