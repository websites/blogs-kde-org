---
title:   "Developing in a community"
date:    2003-07-23
authors:
  - aseigo
slug:    developing-community
---
so the other night i got sucked into joining #hci on openprojects.net. it was all good and fun for a while with some rather interesting discussion going on. and then someone brought up the many issues surrounding the delivery of binary packages on Free Software OSes and all went to hell in a hand basket.

there were two people in the channel who considered themselves experts in the topic, and this resulted in an exchange that would keep any budding social psychologist stocked with enough material for the coming winter.

i won't detail the whole conversation, but suffice it to say that it ended up somewhere south of Nowhere. i'm just glad i don't have to work with those people on a daily basis. or maybe they are very different in person. anyways, i'm not here to bitch about it; it really made me think about interaction between people involved in open community-driven collaberative environments.

here's some things that occured to me while pondering it all last night:

1) My experience does not invalidate your experience. Be open to the concept that different experiences naturally lead to different (and perhaps equally valid) needs and desires.

2) If I consider myself to be knowledgeable about something, that doesn't mean I've thought of everything. In fact, I may have missed some of the most obvious things specifcally <i>because</i> of my familiarity with the topic.

3)  I should try to listen to <i>and</i> hear what you have to say. Letting my preconceptions colour your words only limits me.

4) When you say you disagree, you aren't insulting me. When I insult you, I'm not furthing our exchange of ideas.

5) It isn't my job in life to convince you of things. Should I decide to try and do so anyways, I should do it with open and honest dialogue.

6) The world can progress with or without me, and that's OK.