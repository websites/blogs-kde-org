---
title:   "User Linux *sigh*"
date:    2003-12-19
authors:
  - aseigo
slug:    user-linux-sigh
---
Bruce took his reply to the KDE proposal off his website and replaced it with four short paragraphs that are much more to-the-point and not nearly as dubious as his original piece. The web is vastly impermanent, which can be good but is often unfortunate as we lose our history and context easily. in related news JDub has said written up in his blog (http://www.gnome.org/~jdub/blog/) how dissapointed he is in some of the KDE people for the trail of snipes that occured on the User Linux discuss list in the last few days; i agree with his sentiments but wonder why he felt the need to whine about it in his blog. (hey, how self-referential! look at me, i'm Kevin Smith.) if it wasn't for the fact that we're doing a lot of cool stuff (e.g. code and technical specs) on the kde-debian list, i'd be seriously in the dumps right now. oh well... <!--break-->