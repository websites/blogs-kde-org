---
title:   "feels like forever"
date:    2003-08-11
authors:
  - aseigo
slug:    feels-forever
---
wow, feels like forever since i posted a blog. but i'm behind in most of my correspondence: too much is happening.
<!--break-->
on the KDE side of life i finished up some change to the address picker in libkdepim... it seems to make a lot more sense now and be a lot more polished. Zack made a great start on it, but it needed that last 10%.. maybe it only needs that last 9% now ;-)

please try it out and bitch at me via email... speaking of which, i finally got an @kde.org email! huzzah for me! aseigo at kde dot org ... and speaking of which, my name is spelled with an 'ei', not an 'ie' and sounds kind of like "psycho" (disturbingly enough). i'm sure that clears up an issue that's been burning in the minds of KDE developers everywhere. or not. ;-)

i also did a bit of a fix-up to the damned clock applet dialog. now the clock type drop down actually reflects and controls the clock type! amazing! it did teach me that kautoconfig is sorely lacking, for instance: no KConfig or KAutoConfig accessors in KAuto*, when default clicked, settings modified should be called which updates default / ok / apply buttons, poor handling of combobox items. oh well, it's only in kdecore and kdeui, right?

i also briefly changed the default from digital to plain, and promptly got an earful on IRC for doing so. explaining why i did so wasn't enough, i eventually gave up and reverted, while making some of the digital clock defaults less annoying (e.g. no pinstripe background). it can really be frustrating sometimes dealing with other developers whome i like and respect when it comes to usability changes. now, i'm not always right, i'll be the first to admit that. but it gets a bit tiring after the Nth fix to deal with the (N*M)th discussion about why it's such a damned stupid idea. 

perhaps i should start including full justifications for all usability changes in my CVS logs. i'm not being fascetious, i'm serious. yes, they'd be a bit long, but then at least anyone could see WHY the changes were made. hrm... good idea, Aaron.

random thought: blogs are like talking to yourself. i guess diaries are too, but i've never kept one of those so i wouldn't really know. i've written lots of reflective stuff on a regular and timely basis, but nothing i'd call a diary. 

well, on to other things... hopefully work won't be a mad rush this week; at least the folk fests are over, as fun as they are. 

oh, and my football team is at the top of the table right now! huzzah for excercise!