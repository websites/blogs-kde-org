---
title:   "from the purgatory of kicker into the hell of kmail"
date:    2003-09-03
authors:
  - aseigo
slug:    purgatory-kicker-hell-kmail
---
i've been doing waaaay too much screwing around in kicker lately. there's a few more things that need doing, like alphabetizing the Add/Remove menus and possible splitting up the Applets menu now that we ship with a couple dozen applets, but it's generally in not too bad of shape at this point. i even snuck in a change to the default look of kicker. one less icon. one more background. the custom colour option for butons also looks pretty snazzy IMHO and should be friendly to low-resource machines that still want a panel that isn't completely boring.

of course, as my kicker hacking approaches an end for 3.2, it means i get to start in on my kmail TODOs. hooray for me. custom configure dialogs for the different types of folders using actual, honest-to-goddess OOD! that'll probably come as a shock to the kmail code base ;-) but so be it. i've also started in on a redesign of kmail configuration dialogs. BOY is that a lot of work. i've got drafts of 3 of the pages done, and that took me most of a night. i still have several to go and then it's off to merging the code with it. huzzah. =(

in the good news side of life Klak Kalass implemented an idea i sketched out just after 3.1 for KMail's header configs. it was on my TODO as well, but i guess it isn't any more.. check it out at: http://www.kalass.de/kmail/index.html 

it's a must-have for 3.2 IMHO... needs a bit of clean up here and there, but that's being discussed on the kmail-devel list. where "discussed" means "i've sent an email in reply" ;-)

i'm going to finish this blog entry by saying: DCOP rocks. now when you launch kicker's config from a child panel, the config jumps to that panel when it opens. it does this by communicting back and forth with kicker via DCOP (both direct send()s and DCOP signals). a dozen lines of code and an even shorter .kidl file were all that was necessary. wow.