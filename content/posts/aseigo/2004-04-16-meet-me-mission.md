---
title:   "meet me at the mission"
date:    2004-04-16
authors:
  - aseigo
slug:    meet-me-mission
---
so here's the plan... i'm going to go to sleep right now (it 2:30am...) and get up tomorrow, er, today (at 7:00am) and head out to LinuxFest Northwest. if it all works out, i'll have a bunch of fun and inspire some people w/regards to KDE... i'll keep a running diary of the events here whenever i can get 'net access along the way (i'm driving there and back.. ~12 hours on the road each way.. road trips ROCK! =) this way i can keep my friends and interested folk up to date with my KDE weekend adventure, in case anyone cares. well, i know i do. =P

after i get back next week, i'll be announcing the location of my new personal website, which will house my blog, various patches, screenies, writings, links, .plans, photos and more. expect my first entry there to be a bit of an explanation of what transpired recently regarding my blogging here and my personal take on it. hopefully that will satiate the curious, and we can all move on.

so meet at the mission at midnight, we'll divvy up there.<!--break-->