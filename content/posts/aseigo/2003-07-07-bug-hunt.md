---
title:   "Bug Hunt"
date:    2003-07-07
authors:
  - aseigo
slug:    bug-hunt
---
Bug hunting ... people usually appreciate it when their bugs have been solved and are closed on bugs.kde.org, but they really get excited over new features. New features often require a good amount of work, especially to get them right. But the effort to "wow factor" ratio is far higher on new features than bug hunting.

Here's an example: I closed a bug today where the user had tried to create a custom file association for README files. They wanted it to match *README*, but this didn't work. *README worked. README* worked. But not *README*. 

First, I spent a few minutes confirming the bug. Unfortunately the quality and accuracy of bug reports varies significantly. Not all bug reporters are created equal. Add to that that some bugs are specific to certain KDE versions, OSes, build tools, user behaviour pecularities, etc... and the step of confirmation isn't one you can skip over lightly. Unless you don't mind chasing ghosts in the code base, of course. 

In this case the bug was real and accurately reported. The answer was 2 simple lines of code (plus a couple lines of prettifying white space) to kstringhandler.cpp.

But to come to that answer I first looked into libkonq to see how KFileIVI's figured out their icon. They got it from KFileItems. Which got it from figuring out their mimetype by asking KMime. KMime asked classes hither and yon over kio in a rather optimized (aka hard to follow ;) bit of code. I finally tracked it down to a small snippet in KServiceTypeFactory, which called KStringHandler::matchFileName. Now, if I had known that this code path existed as it does, twisting and weaving in a fine example of code reuse through kdebase and kdelibs, it would've taken me a minute or so to fix, compile and test.

But no. I've never spelunked that code path before. Those that do know it obviously hadn't checked that bug. So I happily spent a good amount of time opening file after file and reading dozens of lines of code to find the true culprit hidden away innocently in kdelibs/kdecore/kstringhandler.cpp. All just to make *README* work as a file association file pattern.

Now, I'm not complaining. I actually find the process somewhat fun. Like solving a riddle or a logic problem. But it reminded me how unobviously strenuous hunting bugs can be. And this time I didn't even have to open a debugger....