---
title:   "folk fest"
date:    2003-07-29
authors:
  - aseigo
slug:    folk-fest
---
this isn't a rant. it's just off-topic. so there. =) if you're not into music, esp of the folk fest variety, save yourself the boredom and don't click the Read More link.
<!--break-->
the Calgary Folk Fest this year was, without a doubt, AMAZING. the musical line up was crazy-good. Ani DiFranco kicked ass and even hung out at the fest the day after she headlined to take in some other acts. she milled around with the general crowd and people just let her be "one of the people" rather than mob her like a rock star. but Ani wasn't the only bright star of the show. there were so many artists there what with the 6 side stages in addition to the main stage: http://www.calgaryfolkfest.com/html/schedules.html

grooving to the Lee Boys had to be one of the highlights for me. their music is honest and infectious. i'm not really into spiritual music, but as far as well crafted, highly energetic music goes they are a top notch act.

Mahlah and her friend / business partner Heather had a vegan foods and organic teas booth. i worked the booth for most of the weekend with them, doing cash and occasional food prep.

the energy in the crowd was so positive and uplifting. people were happy to talk, uptempo, progressive, generally thoughtful, etc... doing the cash 'n orders thing was a great way to interact with lots of them (~25% bought something from the booth apparently?)... 

there were some not-so-great-times, like when someone stole one of the volunteer's backpacks from the volunteer tent during after-show clean up on Saturday... or when people stood up and crowded the mainstage during Ani's act, to the visual detriment of everyone behind them... but even then, most everyone just rolled with it with the understanding that in every crowd there'll be problems.

but, it's Monday, back to the grindstone. at least the music is still ringing loud in my ears. =)