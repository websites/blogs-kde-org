---
title:   "a year of ... life"
date:    2004-03-26
authors:
  - aseigo
slug:    year-life
---
so.. this is the year that will never end ... in both good and bad ways... i've got my 3 hours of speaking time about KDE staring me in the face in the middle of April @ Bellingham, Washington's Linuxfest Northwest, a HUGE backlog of KDE hacking and a million things going on. i'm watching what's happening @Brainshare with anticipation. my step-Father died, a company of mine is in the final stages of selling it's first "franchise" (licensee, really, but... yeah, those are just legal differences ;) which is something that just cropped up, i'm busy sorting out life with women and other beings, i'm excited about what's happening w/Ian's company re:Kolab (DUDE: we need to talk on the phone again about what's up with the marketing materials!), i'm wrapping up a sideline contract devel job, and i'm trying to figure out how to put together a meaningful action plan for people who watch documentaries like The Corporation and have their eyes openned for the first time to the ills of our culture's metasystems. in all, i'm having a slow month. =P<!--break-->