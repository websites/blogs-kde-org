---
title:   "Touchdown, Bellingham"
date:    2004-04-17
authors:
  - aseigo
slug:    touchdown-bellingham
---
after 17 hours on the road i'm finally here. we only got lost once (losing ~40 minutes =/ ), which isn't too bad really. even the border crossing was uneventful. the drive was gorgeous: mountains, trees, beautiful little towns. i love the Pacific Northwest.

it's 10:20am local time and i have to be in my presentation room in half an hour. i forgot the digital camera at the hotel, but that's only 5 minutes away from the festival so i think i'll go grab it at lunch.

there are a LOT of machines running KDE here, more than i expected really (this being the USA and all =). there are a number of non-KDE machines, but from my initial tour of the booth area i'd say that there are definitely more machines running KDE than any other environment.

i've got the pre-presentation jitters, which is really just my apprehension with having to dole out the amount of energy it takes. i love public speaking, though. we'll see how it goes.

as long as KPresenter does its job, i'll do mine. =)