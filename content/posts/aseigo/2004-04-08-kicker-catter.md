---
title:   "kicker the catter"
date:    2004-04-08
authors:
  - aseigo
slug:    kicker-catter
---
getting back to development is nice. very nice. kicker is in shreds on one of my machines. the non-KDE app button dialog has a nice, spiffy new (standard) look to it. and i'm a few steps closer getting away from the "main" panel being a hard-coded fact-o-life. loading applets via appletproxies will also likely be hitting the bit-bin, after discussion with John Firebaugh and others. its a nice idea, but causes it's own problems while not providing a complete solution anyways (e.g. kicker still crashes due to the bad applets. look at all the BRs on it!). i must also say that smooth zooming icons look so much nicer than icons that jump from small to big (assuming you've got the right icon sizes around in the first place!).

kjots is also in tatters as i rework the data classes to be independant from the UI and ready for a nicer file format. kscd needs some work after the last few rounds of patch management on it. nx stuff is asking for my eyes. kiosktool has seen some lovin', and hopefully Waldo will be kind on my patches. almost feeling like the good ol' days again.

Vancouver beat Calgary in game one of their divisional playoff series. it's heating up to be a pretty painful series for all involved, methinks. i don't have a TV in the house anymore (by choice), which means i'll just have to catch it at the pub. aw, shucks!