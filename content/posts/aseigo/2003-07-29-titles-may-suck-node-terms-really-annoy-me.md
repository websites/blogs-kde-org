---
title:   "titles may suck, but node terms really annoy me"
date:    2003-07-29
authors:
  - aseigo
slug:    titles-may-suck-node-terms-really-annoy-me
---
coming up with titles for blog entries isn't fun/easy, but having to pick node terms is really bloody annoying. especially since it's this long listbox that pushes the body waaaay down the page. couldn't it at least be after the body where i could ignore it? and have "KDE General" (or what-have-you) selected by default?

ok, enough metablogging, here's what i'm up to today.
<!--break-->
first, gallium's annoyed that i changed the kwin deco kcm without the holy blessing of the kwin maintainers. not sure what to make of that really. i suppose it was the insinuation that i didn't test the changes due to a BR that popped up soon afterwards which isn't as far as i can tell related to my changes at all that rubbed me raw the most. that or perhaps redoing practically every kcm window deco plugin setting so it didn't suck donkey nuts (or at least so many of them at once). or perhaps the fact that he wants to reinstate the third (disjointed) tab to add a deco preview, which is nonesense since there's plenty of room. or perhaps the fact that he posted the tripe in a public mailing list that i'm sub'd to and CC'd me anyways. i dunno.. feh.  maybe he was having a pissy day too. or perhaps i'm just having a double pissy day. who knows.

i'm also slogging through some more kjots patches. fixing printing in it. simplifying some stuff. it's starting to come together. i'm happy about that. cool to have a couple others working on it as well. not as lonely.

sometimes working on the smaller apps in KDE's CVS feels like coding in the middle of a quiet forest far from the city centre.

also committed some improvements to servicemenus: per-directory servicemenus, and separators! whee...

am also going to commit a small change to Nadeem's recent patch to show the systray frame on hover so it only does so when the applet handles are faded out... hopefully he won't club me upside the head for doing so, but in our discussions via email that's what i though the idea was. sven will thank me, at least. =)

ok, enough blogging. well.... maybe....