---
title:   "Why I hate being a sysadmin..."
date:    2004-06-28
authors:
  - geiseri
slug:    why-i-hate-being-sysadmin
---
Well, after basicly 3 weeks of hounding from chris i got the server upgraded.  To my utter shock and surprise drupal radically changed their plug-ins and themes.  This blew about 9 hours porting as many of them to the new api as i could.  I still don't have the members page working because they hid most of the information in random places in the database...  maybe ill get that finished this week here.  i was happy to get debain installed though.  Now at least I can get software updates for the server again, also it freed up about 2gigs of space.  SuSE may be easier to use, but what do those 2.5 gigs of rpms do on the server?  Oh well so is life i guess....  N-joy the new server, i'm going to get some sleep :)