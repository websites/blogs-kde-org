---
title:   "Plotting my revenge..."
date:    2008-01-01
authors:
  - geiseri
slug:    plotting-my-revenge
---
Okay so I can say 2007 sucked the big one, but 2008 is looking good so far.

2006-2007 had been a pretty rough time for me.  In 2006 I set aside my consulting company and my wife left me.  It was a little too much drama and change for me.  2007 was pretty much set dealing with the crap that followed all the changes of 2006.  I would like to apologize to everyone for letting KDE Developers fall by the wayside at that time.  Special thanks for <a href="https://blogs.kde.org/user/57">jriddell</a> and <a href="https://blogs.kde.org/user/457">beineri</a> for picking up the slack there.

So now here we are 2008 and I'm ready to get back into OSS development again.  I regret missing the difficult part of KDE 4, but alas, I am sure there is more than enough work to be done yet on the project. I am curious to see how QtScript is going to impact KDE, and well dbus so far has been an utter disappointment from my POV.  I think if one doesn't exist, I will need to port the dcop command line tool to dbus since the dbus command line tool is well disappointing.  

I have been working on my own project <a href="http://trac.geiseri.com/wiki/FloMain">Flo</a> during my pause.  Sad when coding is your lifeline, but its been a place I can experiment with Qt 4.x technology.  Its a mind mapping tool, but its largely incomplete.  

Other than that I have been increasingly obsessed with concepts behind the wiki and how to build that more into a desktop.  I have also because of my curious employment become very interested in ways to make KDE more thinclient friendly.  Making a remote desktop is easy... now lets get remote filesystems, usb, and audio :)

So what will 2008 bring?  Who knows, but it cant be any worse than the last two years!  First thing is first though.  I need to do a svn checkout and build asap :)


