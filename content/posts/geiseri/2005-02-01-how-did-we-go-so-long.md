---
title:   "How did we go so long?"
date:    2005-02-01
authors:
  - geiseri
slug:    how-did-we-go-so-long
---
I remember back in the KDE 2 betas trying to find an editor that would handle the KDE desktop files.  They where simple ini files, but I could never remember all the stupid little details of how the damn things worked.  So invariably I would find one, copy it, and dork with it until it did something similar to what I wanted.

Now, almost 5 years later I have written the damn tool. 
[image:833]
I basicly took the fd.o <a href="http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-0.9.4.html"> spec </a> that I can never find on the first try and made an app to edit those files.  The current version supports the main "Desktop Entry" group and the various actions associated with the entry.  I am toying with support for other groups, but I feel that might overlap too much with the KConfig editor, and that app should just be made to edit single files.

So either way its <a href="http://kde.geiseri.com/kdesktopeditor/kdesktopeditor-1.0.tar.bz2">here</a>, and ready for some abuse...  let me know what you think.