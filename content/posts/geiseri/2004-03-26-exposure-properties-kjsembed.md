---
title:   "Exposure of properties in KJSEmbed..."
date:    2004-03-26
authors:
  - geiseri
slug:    exposure-properties-kjsembed
---
Well, I finally got around to implementing my pet pieve with KJSEmbed.  When you imported  a UI File, you would just get the top level widget.

Now after my modifications, it will add the child widgets as JavaScript properties of the parent.  This is nice because now you can just call a UI file as:
<tt>
var Form1 = Factory.loadui("Form1.ui");
var text = Form1.TextGroupBox.NameLineEdit.text;
</tt>
instead of
<tt>
var Form1 = Factory.loadui("Form1.ui");
var TextGroupBox = Form1.child("TextGroupBox");
var NameLineEdit = Form1.child("NameLineEdit");
var text = NameLineEdit.text;
</tt>
Much clearer no?  

