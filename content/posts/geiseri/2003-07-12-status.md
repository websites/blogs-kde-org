---
title:   "The status"
date:    2003-07-12
authors:
  - geiseri
slug:    status
---
Okay, the members page is in a usable state, but I think im going to have to rewrite the module because it wont use any of the current filters or i18n stuff.

Image galleries are up and running.  Each image can be a maximum of 1meg with a max usage of 5megs each user or 50 images.  Im in the process of getting apache to also not allow images here to be linked elsewhere.  This is to save bandwidth.  You can link images in your own posts with no problems.  The process to do this is to upload your image, view the image, copy the image location url on the server, and include it in a img tag.

Im still in the process of adding terms for nodes.  If you feel a term is missing please feel free to let me know about it.  The goal is to hammer out a few policy bugs and style issues before going live on the 21st.  Ideally we should have a slow trickle of developers until then when we will announce this site on the dot.

