---
title:   "Save the dinosaurs!"
date:    2005-04-27
authors:
  - geiseri
slug:    save-dinosaurs
---
So because of events outside of my own wilful control I have been mandated to render every computer in the house booting or recycle it.  Now past the fact that there are about 20 some computers in the basement, and that some of them have operating systems I wrote long ago in school I have some keepers that are in dire need of help. So if anyone has some help/hints/boot disks please send them my way.  Basicy the systems on the rocks are:

DEC Rainbow - Needs boot disks.  The old DOS disk is corrupt and I cannot find a new one.  The drive itself may be bad, but I guess I can boot off an audio tape.  So if anyone has a copy of CPM/80 or DOS 2.0 on tape that would rock.

BeBox - This one is strange.  Its a prototype (i was told #150).  Either way all of the chips are hand labled and jumpers are everywhere.  Its missing RAM and the original BeOS disks.  It seems the ones for my Power Macintosh do not work.

Motorola Net600 - It needs WindowsNT for the PowerPC.  Cant find it and the company that wrote the firmware disavows all knowledge of it.

IBM Spruce systems - I have two of these, but I never got them to boot correctly with any kernel.  These may go, but damn they are 400Mhz CPUs and would make nice compiler nodes.

AlphaStation - yeah this is a funny one... I lost the keys :P   It boots but its days are numbered as the hard disk is making noise.

Philips WebTV based off of the Acorn - I got one of these back in school cause it seemed cool, the next year Philips killed the project and the system is a paper weight.  In theory it will run RiscOS or Debian for the ARM,but it needs a network card.  In theory these exist somewhere in the world, maybe you have one.

Apple eMate - Still works, but the G and H key are trashed.  It has some issues with rebooting randomly but I am not sure why.  If you have a keyboard or a spare one that you want to sell for parts, ill take it.  This buggers got extra RAM and a 1MB card, its sexy for trains and planes, but without a G and H key its difficult to use.

Decstation 3000 - It has ultrix on it, but it wont boot up all the way.  Its suppose to run VMS native I think but I have no clue.  I was told it could also run NetBSD (but then again what cant).  This one is also the only firmware revision that cannot boot off of the nic without MOP.

If anyone has any of this crap in their lab/garage/vw van I will be willing to barter for free or for cheep. 