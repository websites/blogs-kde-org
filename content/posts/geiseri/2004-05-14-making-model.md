---
title:   "The making of a Model"
date:    2004-05-14
authors:
  - geiseri
slug:    making-model
---
Well I have been busy this week bothering Zack and fellow employees here.  For the last 5 months I have been fighting with the concept of Java's Storable interface, and MFC's CDocument object.  Both seem to fall very short of my goals...

Both attempts seem to force the developer to write a serelizer that locks the poor object into some customer data format.  One approach to fixing this would be to have a generic serializer.  The problem here is that still its invasive, and all you do is make the document format somewhat more portable.  Its a start though.  So I went ahead and wrote a serializer interface.  This interface allows the programmer to write things out as a series of properties, this helps with the issue of the data format getting borked when ever a property is added/removed or even reordered.

A loose outline of the class is like below:
<tt>
class SerializerBase{
public:
    ...
    virtual void startProperty( const QString &name ) = 0;
    virtual void endProperty( const QString &name ) = 0;
    virtual void writePropertyType( const QString &type ) = 0;
    virtual void writeProperty( const QString &textData ) = 0;
    virtual void writeProperty( const QString &textData ) = 0;
};
</tt>

This is still under heavy work... and I am not sure how to handle the <tt>writeProperty(...)</tt> functions.  The goal here is to provide an interface that one can place either a textstream or datastream representation of the data on in to the serializer.  This way we can have binary, XML or even SQL based serilaizers in the background.

The next part I am working on is the deserializer portion of the code.  I guess users like to load documents once they have been saved....

Ill post more on that later.

The grand vision here is that I will be able to provide a base <tt>StorableObject</tt> object that will do the grunt work of loading and saving properties, and will provide a nice interface for the developer to base their "Controller" logic off of.  This way developers do not need to waste their time writing xml/binary/text/"God knows what" parsers and just concentrait on building their desired objects, along with their control logic.  IMHO there is no reason in the year 2004 that we are still writing parsers.  Even crude attempts like DOM and SQL only give us parsed data, we still need to walk it and construct our model to apply our control logic too... My dream is that the <tt>StorableObject</tt> will provide that interface for developers to build objects that can load and save themselves to arbitrary formats.  This object is still under heavy development, but I have made some headway into making the properties transparently map into a property map.  From the developer's point of view these will look and act as normal data members, but from the object's point of view they are StorableProperties in a property map.  This allows me to walk the map and manipulate the map genericly.  

Well see... gotta load these buggers back from disk first ;)