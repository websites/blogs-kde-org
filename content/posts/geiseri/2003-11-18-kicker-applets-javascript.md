---
title:   "kicker applets with javascript"
date:    2003-11-18
authors:
  - geiseri
slug:    kicker-applets-javascript
---
Yeah, im insane... but you can.
<!--break-->
Check out cvs:[kdenonbeta/applets/kjsapplet] from cvs and build/install it.  Next comes the fun part.

There are 3 files, an .la file, desktop file and a js file.  The desktop file is the same as they are for normal kicker applets.  The la file is a bit more interesting, this is here to fool KDE into loading the javascript properly.  Ideally we can make this go away in KDE 4 or 3.3 at the earliest.  The la file must match the library name in the desktop file and must be unique on the system.  This will get installed into the $KDEDIR/lib directory with the other.la files.  The last step is to write your ECMA Script :)

This is the fun part.  There are a few functions that make the kicker applet useful so you may wish to implement them:
function widthForHeight(height)
function heightForWidth(width)
function about()
function help()
function configure()

Everything else in the main body of the script will be executed on init.  So you can put  your fun stuff there.

Here is a minimal example that can show off the system:
<code>
#!/usr/bin/env kjscmd

var lab = new QLabel(mainView);
lab.setText("This is a test");
mainView.show();

function about()
{
        lab.setText("about");
}

function widthForHeight(height)
{
        return height * 2;
}

function heightForWidth(width)
{
        return width * 2;
}

</code>

Once this script is installed to $KDEDIR/share/kicker/applets/ecmascripts you should be able to install it into the pannel where you can see this [image:239,middle] .

Now why is this cool?
Well because this is the spring board to make other applications use ECMA Script based plugins.  Nice fast script engine,that has built in KDE/Qt support.  Now  that is cool!
