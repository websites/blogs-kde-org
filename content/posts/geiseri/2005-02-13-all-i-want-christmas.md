---
title:   "all i want for christmas..."
date:    2005-02-13
authors:
  - geiseri
slug:    all-i-want-christmas
---
...is a media player for my TV :P

Basicly I can't seem to find what I am looking for.  All I want is a small set top box that I can plugin, or use wifi to play mp3/oggs via my entertainment system's stereo system, and a way to put my large collection of avi,mpegs, and vcds on my nice TV screen.

MythTV looks like its neat, but I'm not in the mood to put together a computer... and the threat of a athlon 1800xp humming away by my TV is terrifying.  Doesn't anyone sell a media player with a simple GUI that can connect to my network shares?  The best I could do was PC based media centers... really I just want a nice small set top box.  Ideally that costs less than $200...

Any ideas from developer land out there?