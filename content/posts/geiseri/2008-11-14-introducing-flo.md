---
title:   "Introducing Flo"
date:    2008-11-14
authors:
  - geiseri
slug:    introducing-flo
---
Well I have been mumbling for a few weeks now about this "Flo" project and so far I have gotten a ton of encouraging feedback.  For those of you who don't know what Flo is, here is a gentle introduction.

<!--break-->
Flo is a multi-platform mind mapping program.  I have been interested in mind mapping for some years and have written an application that can help express mindmaps and translate them into various document formats. Mind mapping for those of you who don't know about it is a way of brainstorming.  One of the main ideas of Flo is to take these brainstorms and create outlines and various documents.  Mind maps can be used for everything from project planning to designing software test cases.  Pretty much anything that can be expressed in an outline can translate into a mind map quite painlessly.  The main advantage of a mind map is that it is easier to reorder and reorganize ideas in a visual fashion before applying them to something more rigid like an outline.  

Flo on Ubuntu Linux
<img src="http://trac.geiseri.com/wiki/download/flo-linux.png" alt="Flo Linux"/>
Flo on Windows XP
<img src="http://trac.geiseri.com/wiki/download/flo-win32.png" alt="Flo Windows"/>
Flo on MacOS X
<img src="http://trac.geiseri.com/wiki/download/flo-macos.png" alt="Flo MacOS X"/>
So here is the theory of operation.  I have organized ideas into three types.  The main thesis of the mind map, ideas that support the main thesis and general supporting ideas.  Most of the time there is only one main thesis, but nothing prohibits having multiple ones.  The main purpose for these classifications of ideas is for the document export of outlines.  Each idea has an index, this is mainly autolayout and for export of the mind map to other formats, but can be helpful for organization of the mind map.  To add a new idea one can select a parent idea and press the "+" button.  To remove an idea select it and press the "-" button.  These actions are also available via a right click context menu when you select a shape.  If you right click on the canvas you can pan across the view and if you use your mouse wheel on the canvas you can change the zoom level.  If you wish to reorganize the map's ideas you can change the parents of child ideas very easily.  First select the new parent, then click the "Reparent" button on the UI. Lastly drag from the new parent to the child idea you wish to reparent.  When you release the mouse it will reparent the idea for you.  At this point you can right click on the child and edit its properties to change its index in relation to its new sibling ideas.  Once you have created and organized the ideas in your brainstorming session you can export to an outline format to quickly generate your final document.

So now on to the features!  Flo has quite a few small features that I have added as I needed them for work.  One big problem with me working on it for two years now is that a lot of little undocumented features have slipped in that I have used once or twice and then forgotten.  These features need to be debugged a little bit, but in my opinion they are useful still.  Here is a taste of what I have so far.

<ul>
<li> Adding custom text notes to each idea.
<li> Ability to add custom pixmap emblems as well as a set of built in vector emblems.
<li> Document wide meta information to help organize the document better.
<li> Ability to change the parents of ideas to reorganize the map quickly.
<li> Cut, Copy and Paste between instances of Flo.
<li> Undo/Redo support.
<li> Panning support via the mouse.
<li> Zoom support via the UI, mouse and keyboard.
<li> Font embedding to provide common fonts across platforms.
<li> Spellchecking for all text in the document.
<li> Automatic layout of the document as well as alignment options.
<li> Auto save as well as automatic creation of backup files.
<li> Recent documents support.
<li> Ability to change font, color and shape on multiple ideas at a time.
<li> Support for multiple languages in the spell checker (TODO)
<li> Export Features
<ul>
<li> Docbook 4.2 export support that supports graphics.
<li> ASCII text outline support.
<li> Wiki outline support for Wikimedia, Trac, Creole 1.0 and Parsewiki
<li> SVG, PDF and pixmap.
<li> Rudimentary PDF export of beamer slides. 
<li> ODF outline support with images (Pending Qt 4.5)
</ul>
<li> Import Features
<ul>
<li> Freemind import with shapes, fonts and colors.
<li> ASCII text outline import (TODO)
<li> Creole 1.0 wiki import (TODO)
</ul>
</ul>

So as you can see its got a lot of little features that make it usable for every day use at this point.  I am finishing up the documentation of the whole application and it should be available in the next few weeks.

Now most importantly how to get Flo!  If you are using a Debian based system you can just add the following to your /etc/apt/sources.list file:  deb http://apt.geiseri.com/flo/apt/ $YOURDIST contrib Where $YOURDIST is one of the follwing: sid, etch, lenny, feisty, gutsy, hardy, or intrepid. I have amd64 and i386 builds. PowerPC builds are pending on me getting a new hard disc for my system. Jaunty will be available as it is updated. I have a basic spec file present for SuSE and Red Hat based systems, but I have not been able to get automated builds for those dists done.  If someone wishes to provide me builds I will more than gladly post them.  If you are stuck on windows you can also download an installer <a href="http://trac.geiseri.com/wiki/download/FloSetup.zip">here</a>.  Mac users are out of luck until I can fix a problem that causes Flo to crash when you move multiple objects.  I think this is manifested as a performance problem on Win32 and Linux, but its fatal on MacOS X.  The main webpage is at <a href="http://trac.geiseri.com/wiki/FloMain">http://trac.geiseri.com/wiki/FloMain</a>.

Lastly, I consider this a Beta release.  I use Flo every day and have stopped adding new features.  I will finish the remaining todos and finish the documentation and hope to release the final version on the 1st of December.  Until then please everyone take it for a spin and crash it any way you can.  I want to release something solid here. Please give it a whirl and have fun!
