---
title:   "Building a QTextDocument the hard way..."
date:    2008-01-27
authors:
  - geiseri
slug:    building-qtextdocument-hard-way
---
So now I am trying to put the output of my wiki parser into a QTextDocument.  I am struggling with the QTextCursor though.  Things are not acting at all as I would think they do.  I think its because I am constantly using insertHtml() everywhere still...

So I'm calling out to any QTextDocument gurus for a bit of help.  I am sure its because I have not been working with the QTextDocument an entire 3 hours now, but I for the life of me cannot get a horizontal rule inserted into my document without using <tt>insertHtml("&lt;hr&gt;");</tt>  This is acceptable, but for some reason my document then has a horizontal rule after every paragraph in my document.  If i replace "<tt>&lt;hr&gt;</tt>" with "---" i see the correct output.  The next problem I seem to be stuck with is how do I append a block, edit said block and then go past the end of it so that I can create a new block with none of the prior formatting?  

These things seem pretty trivial, so I am not worried.  I will more than likely figure them out in the next few days.  I just wish there was more documentation and sample code on how to build up a QTextDocument block by block.

Other than that things are moving along pretty well.  I even am as far as round tripping basic text and headings from Creole 1.0 wiki format to a QTextDocument, and then back to the wiki format.