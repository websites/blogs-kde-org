---
title:   "this should not be so hard."
date:    2005-02-16
authors:
  - geiseri
slug:    should-not-be-so-hard
---
Okay, so you are a happy customer with their shiny new Qt application.  According to Trolltech now your application is portable.  Then reality sets in and you find you are not much better off than before. Now most developers don't mind doing a win32 build and a OSX build, as well as a linux build.  After all they are different operating systems...

But why do I need (number of distributions out there) * ( number of revisions of their distribution with different gccs) * (my application i want to port)?  Well its two fold.  The first I guess we cant blame, because they are still "trying" to get it right.  This is GCC, they break the ABI every time, with, good reasons, but you might think that they would have possibly built a tool that can "fixup" old applications?  No biggie though, I can get around this by building Qt and my application with the oldest gcc the application supports.  This way I only have to screw the user with duplicate copies of Qt and support C++ libraries. Developers are spared.  But then we have the distributions.  I understand the need to brand your packages so users are forced to use your distribution with your packages.  This is the nature of the beast.  RH and Novell have different ideas of what is a proper configuration, so we cannot always rely on the same version of supporting libraries underneath.  So what's the solution if I want to ship a linux application 

1) GNU approach - Open source it and let the poor users compile it or hope packagers pick it up.  This might not work if companies don't want to go OSS yet, or their application is so niche nobody picks it up.

2) Many machines approach - Set up a nice cluster of machines, or a machine with chroot installs of EVERY distribution and version that you wish to target.  Hope like hell that you don't miss one, and that you got enough so you don't miss a prospective client.  This can also get expensive fast, and pity the poor sysadmin on this abomination.

3) Pick one and screw the rest approach - Pick your favorite distribution and say its the only supported version.  Hope you application is cooler than Jesus or you have the largest distribution in your target market, cause its going to alenate all those users you leave out.  This also sucks because it promotes software monoculture on Linux.  

4) Throw in the kitchen sink approach - Either staticly link everything, or build shared versions that insall to their own directory, then set LD_LIBRARY_PATHs and hope for the best.  This can screw you out of other services on the system though that rely on linkage to the main system libraries.

Personally I think this is a problem that needs to be solved.  There has to be some way to "reorder" symbols at install time to make things link up correctly.  If source compatibility has been kept I think its a tool problem that binary compatibility cannot be preserved, or at least restored...  Is this really that impossible?