---
title:   "now thats krafty..."
date:    2004-07-22
authors:
  - geiseri
slug:    now-thats-krafty
---
so sometime last week I got my Sony Ericsson T610 phone.  After the fear that I would be stuck using the default themes on the phone I went out in search of the file format or some information on them.  Well shockingly what I discovered was quite cool...


It seems that the file format is in fact a tar file full of GIF/JPEGS and an XML file.  So I got out Kate and ark and had a party.  The result of this was a <a href="http://www.geiseri.com/T610/kde.thm">KDE theme</a> for the T6xx phones. So I told zack about this and well he obviously thought this was very cool and soon there after Krafy was born.  While its not very advanced we can actually load and save valid theme files, as well as manipulate the properties.  The task remaining... and one that seems to have sucked up all our efforts is painting the previews. so anyone with a T6xx interested in playing along? ;)