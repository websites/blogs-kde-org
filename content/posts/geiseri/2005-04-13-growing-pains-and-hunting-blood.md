---
title:   "Growing pains and hunting for blood"
date:    2005-04-13
authors:
  - geiseri
slug:    growing-pains-and-hunting-blood
---
Man it seems like months since I have taken the time to blog, but this quarter just flew by. Since Zack joined the party at Trolltech we have been on the hunt for more developers.  Now Zack in Norway is cool, he's doing cool stuff and in the end its all going to help me make better products for my clients. 
<!--break-->
Annoyingly though it seems that it seems that OSS developers (my preferred fare) are hard to find in the Philly area.  I was fortunate enough that Matt Rogers of Kopete fame was willing to come up to help us out for a few weeks so that was cool.  It's also cool that Qt is gaining the traction in over here that our biggest problem is needing more developers :)  Now imho what annoys me is that we are small, and still pretty young, so I suppose that scares away developers.  Either way its interesting.  Its cool though that we have been able to promote open source tools and have also done GPL based projects for clients.  Now for the next step, to employee a few OSS developers and rock the world.

One of the visions we had at the beginning was to still have time to hack on OSS software.  Too often companies become black holes for OSS developers to fall into.  We hope to avoid this, partly by earmarking time and money so that devs can still work on their OSS projects.  Now its still a dream, but its growing more real as KDE takes a foothold in the business market.  

Either way I suppose these are good problems to have and its a sign that OSS is taking off in the US.  So now to look for people who want to party at my place? (Yes that was a thinly veiled attempt at a job posting :P)