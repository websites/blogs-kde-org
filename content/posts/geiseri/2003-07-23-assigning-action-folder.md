---
title:   "Assigning an action to a folder"
date:    2003-07-23
authors:
  - geiseri
slug:    assigning-action-folder
---
So I'm trying to figure out how to add a menu to a single folder, kinda like the trashcan when it hits me.  We have this .directory file in there.  I wonder how hard it would be to add a service menu to that folder via the .directory file?  We already use that file for icons...  

I wonder how hard that would be since the .directory file is already parsed to get the icon.  What if we parsed it to get a set of service menus too.  Im thinking of a folder on my desktop that contains a menu entry to rsync the folder with my remote file store. 
example:
...
[Desktop Action sync]
Exec=rsync ... %d %u
Name=Sync to Server...
Icon=sync

[Desktop Entry]
Actions=sync
BgImage=funky.png
Icon=desktop
Type=Directory

[IconPosition::kweather page.desktop]
X=5
Y=404
....

hrm... yet another thing on my todo list :)