---
title:   "Givin some Kiosktool lovin' and how you can give some too"
date:    2009-03-02
authors:
  - geiseri
slug:    givin-some-kiosktool-lovin-and-how-you-can-give-some-too
---
So I was notified that commits to KDE are HOT, and I am in serious need of some hotness.  What could be more hot than working on Kiosk Tool?  Well maybe KHTML, but I don't need love that badly.  So back to kiosk tool.  Back in KDE 3 Waldo Bastian made up this nice little tool that could give administrators a GUI interface around the kiosk functionality in KDE 3.  Well 5 years have passed and the tool was in need of some love.  First step was to get the last of the KDE/Qt 3 compatibility code removed.  That took most of the day yesterday.  Today was spent trying to add a few features to make it easier for 3rd party application developers to add their application's settings to the Kiosktool UI.  I broke up the old monolithic XML file into modular ini files that can be installed into the kiosktool application data directory.  This allows any application developer to create a custom set of options for administrators to lock down their application.

Now for those of you who have no clue what kiosk is, its some hooks in the KDE configuration system as well as the GUI system that allow features to be turned on, turned off, or even locked to specific values.  It can also control what menu options are visible, what applications are available to the user or even if they get a shell.  Its pretty powerful stuff, but its difficult to administer unless you have an intimate knowledge of every KDE application you wish to maintain.  This is the big thing kiosktool provides.  Administrators can navigate a list of available application and desktop settings that can be controlled.  

If you are an application developer that would like to get in on this action its pretty easy.  The only thing you really need to know is your application's settings and the .kiosk file format.  Every component goes in its own file, and it must be located in $().  Each file has a mandatory Group section that contains meta-data properties about the component.  An example is shown below:
<code>
[Group]
Icon=package_settings
Name=General
Description=Generic restrictions for a KDE session
</code>
The Icon property can be any standard icon name understood by KDE.  The Name is what is displayed to the user in the component chooser and the Description is used for headings.  These can be translated like any normal desktop file. The next section is the "Action-" section.  This is the specific feature that you wish to allow the administrator to enable, disable or modify at a global scope.  An example is shown below:
<code>
[Action-1]
Type=action restriction
Key=action/kwin_rmb
Name=Disable Window Manager context menu (Alt-F3)
Description=The Window Manager context menu is normally shown when Alt-F3 is pressed or when the menu button on the window frame is pressed
</code>
In this case we have a Type of "action restriction" , this means that the action can be disabled if the administrator decides.  Key is the name of the action according to KDE.  These actions can be gotten from dbus using something like this: "qdbus org.kde.myapp | grep actions  | cut -d '/' -f 4,5".    Again Name and Description are provided to the administrator so that they know what they are enabling or disabling.  Another type of action is the ability to filter modules from being loadable.  This can be done as shown below:
<code>
[Action-1]
Type=module
Key=style.desktop
Name=Lock down style settings
Description=When the style settings are locked down the user can no longer change them.
</code>
Here the type is "module" and the Key is now the name of the plugin's desktop file that administrator would wish to filter from the user's view.  Next there is the ability to set default values for application settings.  This is shown in the below example:
<code>
[Action-5]
Type=config
Key=widgetStyle
Group=General
Name=Default KDE Widget Style
Description=Set the system wide widget style to the following style.
DefaultValue=motif
</code>
Here the Type is now "config" and the Key is the name of the setting in the configuration file.  There are two other keys that are used for config types.  Group is the configuration file's group that the key is a child of.  DefaultValue is the value that will be supplied to the configuration file.  DefaultValue is optional, but it gives the administrator something to go by.  There are a few other types but these are the big ones to get you started.  

If you have any questions drop on by #kde-devel on freenode and find geiseri. I hope to refine these settings in the next few months and release Kiosk tool with KDE 4.3.  As new features develop I will blog about them here.  My goal is to expose the KDE kiosk functionality to administrators and users so that they can see how awesome it is.

