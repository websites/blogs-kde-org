---
title:   "Moving on..."
date:    2006-08-04
authors:
  - geiseri
slug:    moving
---
Some of you may have heard already that I am moving on from SourceXtreme, Inc. Due to the stress of being involved as a partner in a small company I decided to set it aside for a while so I could concentrate on my mental health and KDE development again.

This last year has been very hard on me personally, and honestly the stress has pretty much wrecked me.  Personally my Grandfather has been ill with Alzheimer's, I have been working 14 hour days, and have not really had a good chance to hack on KDE projects.  Throw in a few other family dramas, being 800 miles from said family, and a wife who wants you home more often.  It becomes clear that I need to sit back and rethink my lifestyle.  Oh and to top it off, during the hottest heatwave in the last 10 years my AC went out (just a little salt in the wounds) :)  I guess in a way the only bright side of this is that there is now enough of a team here that things will be able to continue without me.

So yeah stay tuned.  Ideally I will have my life back together soon and things will be on the up and up.  Just no more consulting for a while...  and no I am not going to work for Trolltech, I do not wish to move any further from said family who is providing my steady diet of drama. :P
