---
title:   "Just a thought..."
date:    2006-01-05
authors:
  - geiseri
slug:    just-thought
---
There has been some talk here at the office about making custom stylized buttons in Qt 4.  Since the Qt 4.1 painting is pretty friggen cool I played around with a tool that can generate stylized buttons. 

Basicly what the bugger does is takes in a set of bitmaps and allows the artist to create a metafile that can be converted into a Qt Designer widget plugin, as well as code for use in a C++ application.  As developers go, they would just see it as a QPushButton.  The artist would see a bitmap.

[image:1725]

The goal here is to make a tool that artists can work with to build customized buttons without having to mess with code, or even designer.  Also since the developer can use the generated widget plugin in their UI files without having to care about integrating artwork, or maintain the code.  Now this is just a simple attempt. I would be shocked if in its current incarnation it was even usable by anyone but me. Its just a pushbutton, and wont solve world hunger.  I'm mostly interested in if artists care.  Could this be the tool that brings all those screen mockups from kde-look to reality?  I figure I will attack one GUI element at a time until I have something that can take an artists composition and turn it into code.