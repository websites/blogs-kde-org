---
title:   "Why bother?"
date:    2003-12-11
authors:
  - geiseri
slug:    why-bother
---
So I was talking to my wife last night who does rubber stamping as her hobby.  Unlike me who hacks for hours on KDE she toys around with rubber stamps, various inks and things found from garage sales to make art.

So we where talking about our various organization, and she was complaining about how much work it was to organize meetings for their stamping group. I made the mistake of asking "Well why bother if its such a big deal?"  

This lead to a very interesting discussion and led me to think later. Why do I bother? I bother because I care about it.  I bother because I feel that there is some level in pride that the desktop I'm using has been influenced, and aided by me (no matter how small).  I bother because if I didn't I cannot say someone else would bother to fix the problems I see.  I bother because I believe in the project as a whole. 

Now that is pretty powerful stuff.  KDE grows because there is an amount of pride behind it.  Its funny, my wife takes pride in her stamping, I would argue that a handmade card means infinitely more than some 50 cent Hallmark card made in batches of 1 million.  Same with KDE.  I think that pride will make KDE in the end a better platform than a million payed slaves can push out.  Pride in your work is what fixes bugs, pride is what promotes the quality.  Not saying we are perfect, but we strive for it!