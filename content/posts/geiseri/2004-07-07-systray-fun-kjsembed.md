---
title:   "Systray fun with KJSembed..."
date:    2004-07-07
authors:
  - geiseri
slug:    systray-fun-kjsembed
---
Someone a few days ago on IRC asked me about creating kde:KSystemTray apps with KJSEmbed.  So I dug into it and found out how cool it really is...
<!--break-->
It seems there are two parts to building scripts that use the system tray.  The first part is the tray itself.  This is pretty easy to create you just new it, set the icon, and show it
Example:
<code>var tray = new KSystemTray( this );
tray.setPixmap( StdIcons.SmallIcon( "news_subscribe" ) );
tray.show();
application.exec();
</code>

Nothing really spectacular, and for 99% of the apps this might do.  Well the next step is to actually have the system tray show/hide a main window.  This can be done by instead of parenting off the toplevel widget, to create another widget and parent the tray off of that.
Example:
<code>var widget = new KLed( this );
var tray = new KSystemTray( widget );
tray.setPixmap( StdIcons.SmallIcon( "news_subscribe" ) );
tray.show();
application.exec();
</code>

This will cause the kde:KLed to show and hide when the tray icon is clicked.  So now I was thinking "How do I add my own menu items"

Well same way as in C++.  We can get the kde:KPopupMenu pointer, and add our items as we please.  NOTE: This is only available in head, since the qt:QPopupMenu stuff wasn't working earlier.

This code will look like this:
<code>var tray = new KSystemTray( this );
tray.setPixmap( StdIcons.SmallIcon( "news_subscribe" ) );
var menu = tray.contextMenu();
menu.insertItem( "test" );
tray.show();
application.exec();
</code>

This will insert a menu item called test in the kde:KSystemTray popup menu.  You can also connect signals and slots to this kde:KPopupMenu pointer and have a good ole time with it.

Hope its been educational for you, it was for me :)