---
title:   "close your eyes and sleep..."
date:    2005-09-01
authors:
  - geiseri
slug:    close-your-eyes-and-sleep
---
well finally my wife's grandfather has passed away.  we went out last week to visit him, so at least we got some closure there and a chance to say good bye.  this is quite difficult as i am here and she is home alone. its also not like this is for the worst, his health has been worse every year, and he has been in constant pain for years. so hopefully he found peace, and so will the rest of the family. i guess i cannot think of anything more to talk about right now, i just wanted to let everyone know that its all worked it's self out now.