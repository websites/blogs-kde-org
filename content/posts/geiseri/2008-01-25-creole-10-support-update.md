---
title:   "Creole 1.0 Support Update"
date:    2008-01-25
authors:
  - geiseri
slug:    creole-10-support-update
---
Finally I have had some more hack time and this morning I got the last of Creole tables supported.  There is still a problem with how I am doing the delimiters of the table pipes when it contains a wiki link.  I should be able to fix this this weekend.  There are also problems with nested lists, but this is something that I hope to fix this weekend also.

I did take a look at spirit over the last two weeks.  Its very cool, but I am not sure I am interested enough in parsers to port the parser.  Ideally someone who is "in to" parsers more will help me with that one.  Otherwise the current parser will work, even though its a horrible hack.

Now that the parser is done enough I can start working on the QTextDocument observer.  This will allow me to have a QTextDocument that can read wiki markup.  Once thats done the last step will be to have something that can dump a QTextDocument back to wiki markup.  I am assuming that will be trivial, so I am not too worried about that.

Its great to be banging the keyboard for fun again!