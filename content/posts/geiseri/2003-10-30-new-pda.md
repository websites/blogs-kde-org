---
title:   "new pda"
date:    2003-10-30
authors:
  - geiseri
slug:    new-pda
---
okay so my old visor prism is starting to show its age... its big its bulky and after owning two newtons before that, its cumbersome to use... so im shopping.

the new palms are getting better at their input, but its still a 9 out of 10 on the most inane ways to put data into your pda...  but damn, they are fast, and far cheeper than the linux and wince offerings...

but there is the sync factor, kpilot FINALLY with the awesome efforts of its developers is comming into shape.  usb support is mature enough on ia32 that i only get a kernel pannic once and a while with it, and well i know the tricks...

linux pdas scare me... will they exist in two years?  ive yet to see a sub $300 linux pda, and yet to see one with reasonable apps... no im not playing the opie game, this is a tool... if i wanted a toy id get that new gameboy advance.

wince seems to be the only ones that have some nice features, wifi, irda, tons of apps... but the sync support on linux seems spotty at best... hell even under win2k my vandem clio is a pain in the ass to backup... that and win32 sucks on a 320x128 screen...  

has anyone out there found a PDA that was sub $300 and did the job?  i mean all i want is a calender/addressbook/memo (with a reasonable input method) and the ability to sync it with my linux desktop... is that too much to ask?

oh well... maby ill get the new palm T3, and port Qtopia to it, i mean its got more beef than the zarus now.. and i think might be cheeper.