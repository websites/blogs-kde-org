---
title:   "More with MyPoint"
date:    2008-04-15
authors:
  - geiseri
slug:    more-mypoint
---
So after a few emails of feedback on MyPoint i have made a few changes and updates to the tool. 

The big change is that I changed the wiki formatting to trac's flavor of markup.  While I don't like it's flavor as much as say media wiki trac is what I use at work and play.  The major gain of this that it is easier to collaborate on content with coworkers.  Since MyPoint's focus is to make it easier for developers to make presentations with developers tools I thought this was a wise choice.

The other big change was that I added more formatting options and text alignment options to the templates.  This gives a great deal of flexability to the text effects in a presentation.  Fonts can also be changed via the templates to give more flexability there too.  I am still not happy with the template format, but I have not found something better yet.  The big goal is that it should be simple, human readable and human editable.  This is something that XML just cant offer easily without a good XML editor.  This was my big gripe with Magic Point.

The last big change was with respects to the templates is that they can have default backgrounds now.  This makes the presentation markup less verbose and lets the author concentrate on the content vs the markup.  

For more information about MyPoint check out the webpage at: http://trac.geiseri.com/wiki/MyPointMain.  I have a sample presentation there as well as more information about the template format.  Personally I think its a cool little tool and have used it for a few presentations already.