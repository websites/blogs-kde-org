---
title:   "what kind of nethack monster are you?"
date:    2006-01-26
authors:
  - geiseri
slug:    what-kind-nethack-monster-are-you
---
Proof that finding useful stuff on the internet is much harder than it should be.  This was found as a side link with a google search for "stroke matching"... 
<tt>|%.....
|./<B>U</B>...
+.....[</tt>
If I were a NetHack monster, I would be an <B>umber hulk</B>. I know where I'm going and will work through anything to get there, even if my methods sometimes appear confusing.</div><A HREF="http://kevan.org/nethack">Which NetHack Monster Are You?</a></div>

<!--break-->