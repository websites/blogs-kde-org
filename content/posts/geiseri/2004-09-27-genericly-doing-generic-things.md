---
title:   "Genericly doing generic things..."
date:    2004-09-27
authors:
  - geiseri
slug:    genericly-doing-generic-things
---
So about 3 months ago I took a survey of some of the projects I have been using meta-programming techniques with and decided on two basic things:  a) Meta-programming can make tedious error prone development faster, easier and more reliable. b) Maintaining meta-programming tools is not fun.
<br>
I have 2 KDE projects Krafty and makekdewidgets, that both extensively simplify building a component via a processed template.  They are awesome tools, and makekdewidets is so flexable it will even work with Qt only widgets.  The problem is maintaining these tools is a liability.  Maintaining code within code is nightmarishly complex, and involves quite a bit of madness with copying and pasting in and out of test programs.  Worse, it seems that over time the code just flat out gets harder to read and organize.  The other issue is the matter of input files.  Believe it or not XML is not always the best solution, and same goes for flat csv files, or INI files.  I also have yet to meet anyone who recreationally programs in lex and yacc so custom parsers are even more painful.
<br>
Cornelius Schumacher has done some interesting stuff wrt to KConfigXT and meta-programming, as has Rich Moore in using XSLT to generate bindings for KJSEmbed.  I quickly grew tired of XSLT though, its constant brain damages, and completely bizzare constructs made the templates as hard to maintain as just embedding code in C++.  Also its reliance on using a xml file for input killed its usefulness for things like makekdewidgets or krafty.
<br>
So with that in mind I have been trying to explore a tool that can solve my big problems:
<ol>
<li> Must have a flexible front end to adapt for different file formats.
<li> Template must be clear, and have clean separate of the template control logic and the code itself.</ol>
So far progress is slow.  I have a property tree, that my engine can use to extract properties to apply to the template.  I am not satisfied with the language that is growing out of this either, its basicly extensions to kmacroexpander to handle loops.  
Example:<code>%{start x in foo}
   %{foo(x)}
%{end}
</code>
Its not offensive yet, but its getting there.  It sorely reminds me of the MSVC templates language, but I am at a loss as to how to do it in a cleaner way.  The other goal that I have yet to archive is also that the template files should be able to be compiled by gcc (for C/C++ files), to make them easier to test.  
<br>
So anyway... its closer... its solving a few of my problems... its getting progressively less wrong,but I'm not happy yet.