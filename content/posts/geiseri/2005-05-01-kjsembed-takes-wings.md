---
title:   "KJSEmbed takes wings..."
date:    2005-05-01
authors:
  - geiseri
slug:    kjsembed-takes-wings
---
So over the last few weeks I have started work on the new KJSEmbed that will be in KDE 4.  In a change of heart that is an entirely different blog all together I have decided that I will push to have KJSEmbed in the core of KDE 4.

My goals are to make KJSEmbed able to be natively handled via KParts and bindings available automaticly inside of appliactions.  I hope to allow for KJSEmbed applets in Kicker, KDesktop and elements of the Konqueror file browser.  More will come on this later as I have a few more examples, and when KDE starts the Qt4 migration.

Currently I have simplified the bindings system and made them much smaller and faster.  There are basicly 3 stages of bindings.  1) Value bindings - Basicly anything that can be handled by a QVariant.  2) Object bindings - These are pointers that cannot be supported by QVariants.  3) QObject bindings - These are just special Object bindings that dynamicly expose properties and slots.  Unlike the current version KJSEmbed will have the ability to add any type of binding at runtime has been added.  This will make extending KJSEmbed much easier so we can ship a much lighter core.  The goal is that KDElibs could contain a very simple core KJSEmbed and the extensions could lie in the KDE bindings.  This would allow for a base functionality and scripting layer to be added to every application in KDE.  This could bring some very powerful tools for applications like KWord and KDevelop, as well as quanta.  Using KJSEmbed as a lightweight layer we could also do some very remarkable things with applications like KControl and smaller KDE applications.  KJSEmbed gives us a very fast, portable and flexable way to build smaller KDE applications.  One powerful thing about KJSEmbed is that it basicly is "glue" that holds C++ objects together. This allows us to do the dirty work of component development in C++ and very easily use KJS to hold these different components together.  The future gets even more exciting if we have KParts support for loading KJSEmbed components, as then they can transparently mix with our other C++ applications as first class citizens.  

I already have hacks for KDE 3.x that have kicker applets, kdesktop applets (aka superkaramba like) and konqi sidebar plugins that use KJSEmbed.  These are horrible hacks, and require some stupid BIC changes so they wont see the light of day until KDE 4.  Needless to say I have some exciting dreams for KDE 4. :)