---
title:   "QMake as a KDE Platform"
date:    2004-10-01
authors:
  - geiseri
slug:    qmake-kde-platform
---
One of the largest problems we have in KDevelop is that people use our templates.  The problem with templates is they are obsolete the second you install them on your project.  You don't get bug fixes from new KDevelop releases, you don't get updates when autotools or something changes.  So they do over time become a serious liability.

One of the things I have discovered over the years of consulting is that simple solutions are easy to maintain, and easy to fix when they break.  Given that bit of insight, I really feel basing our KDE templates off of KDE's build system is not really a nice solution.  It's complex, and ever changing, so its hard to change your local projects when the build system updates.

Enter QMake. QMake has an advantage that it makes simple things simple to do.  In the last 7 years of development only 2 times have I ever needed to add a configure check for an external library to my software project.  Even then with the arrival of tools like pkg-config these checks might not be really that important.

So my proposal is pretty simple.  Lets facilitate via templates and QMake project include files the use of a simple tool.  By no means abandon the KDE build system.  We address many other more complex needs in the KDE build system, that QMake is completely inadequate in its current state to solve.  But to be fair to our 3rd party developers, lets give them a simple option, so they can develop for KDE as easily as possible.

So what do people think?