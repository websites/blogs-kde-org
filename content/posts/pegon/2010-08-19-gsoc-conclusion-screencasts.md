---
title:   "GSoC: Conclusion - Screencasts"
date:    2010-08-19
authors:
  - pegon
slug:    gsoc-conclusion-screencasts
---
Though this entry will be the last one about my GSoC, it certainly won't be the last one about my work on Krita or even on its new Transformation Tool. It was a great summer, I had a lot of fun working on this project. It was all the more stimulating so as I could follow the progress of the other GSoC students working for Krita. Everyone did such an incredible job, it gave me the envy and the energy to complete my project and make it as it is.

I'll be brief about what I did the past two weeks, as I prepared 3 small screencasts which will show you what the new tool looks like and how to use it.
Two weeks ago, I had just implemented warp functionality, so I still had some work to do on the UI.
First I changed the way to switch between Free Transform and Warp mode, so that the Tool Options Widget changes depending on the mode.
In warp mode, the user can either use default control points placed like a grid whose density can be changed, or place the control points by himself/herself.
In the paper I used as reference for the maths, 3 deformation functions were described, and I had only implemented one (affine). Thus I then added similitude and as-rigid-as-possible deformation functions.

Here is a picture I produced with the tool, which illustrates the difference between those functions. From left to right : original image (sorry if it's not very nice, I made it myself), warped using affine function, similitude function, and rigid-as-possible function.

<img src=http://lh6.ggpht.com/_KZOijM4jJGU/TGxTdA6dJTI/AAAAAAAAADQ/3xF1ep1AIw8/warp_functions.jpg></img>

Last week I essentially cleaned the code, fixed some bugs/weird behaviours, and changed some icons.

To close this entry, I wanted to show the results of my hard work, so here are some screencasts of the tool in action :
<ul>
<li>Using <a href=http://www.youtube.com/watch?v=n0iy9vJ5H14>Free Transform mode</a>.
<li>Using <a href=http://www.youtube.com/watch?v=ytJZ9R1e6iI>Warp mode</a>.
<li>And a last one using Warp mode to show the difference between <a href=http://www.youtube.com/watch?v=kWuwBwRW5OA>warp functions</a>.
</ul>