---
title:   "Having some fun with old maps"
date:    2010-09-10
authors:
  - pegon
slug:    having-some-fun-old-maps
---
While relaxing after the hard-working period that was the GSoC, I came across a comment left by Torsten Rahn of the marble team. He wondered whether it would be possible to adapt image warping functionality so that it can be used to morph some old maps.

Apparently, they would like to add historical maps to Marble. Unfortunately, these maps are most of the time not "plate carrée projections" of the globe, which is a requirement for Marble. Hence they use xmorph to convert those maps "by hand" into plate carrée projections.

Of course, it would be too long and too hard for me to do something like xmorph. Though, after taking a look at some of these old maps and searching on wikipedia, I became pretty sure these were some kind of stereographic projections (actually two stereographic projections : each one representing a half of the globe).
So, I did the maths and wrote a simple command-line program to convert automatically stereographic projections to plate carrée projections.

I was quite convinced the result would be bad, but actually it turned out quite well :). My program takes a .ppm square image of a semi stereographic projection (rightly centered, etc..) and converts it to a .ppm square image of a semi plate carree projection (the plate carree projection of the corresponding half of the globe).

I tried it with a map that had already been converted by the marble team, in order to see if I obtained the right result.
Here is the original map :
<a href="http://lh5.ggpht.com/_KZOijM4jJGU/TIqSYGdewUI/AAAAAAAAAI8/FYB4G4GblQY/World_Map_1689_orig.jpg"><img src="http://lh5.ggpht.com/_KZOijM4jJGU/TIqULdMZDnI/AAAAAAAAAJM/4T0KRd6hSxI/World_Map_1689_orig_thumb.png" width="400" height="345" /></a>

And the expected result :
<a href="http://lh3.ggpht.com/_KZOijM4jJGU/TIqPCVOHrEI/AAAAAAAAAI0/QzOOFYwYqtM/World_Map_1689.jpg"><img src="http://lh6.ggpht.com/_KZOijM4jJGU/TIqULrBB0VI/AAAAAAAAAJQ/3S8cbbYxG04/World_Map_1689_thumb.png" width="500" height="250" /></a>

Here is what I gave to my program (the stereographic projection of one half of the globe, cut from the original map, and scale a bit to make it a perfect circle) :
<a href="http://lh3.ggpht.com/_KZOijM4jJGU/TIqU19MV0mI/AAAAAAAAAJU/ehKlRILwnU0/input.png"><img src="http://lh4.ggpht.com/_KZOijM4jJGU/TIqU4FOBhZI/AAAAAAAAAJY/5ARYApscj80/input_thumb.png" width="400" height="400" /></a>

And here is what I obtained (the operation took about 2 seconds) :
<a href="http://lh4.ggpht.com/_KZOijM4jJGU/TIqWJ5qiFwI/AAAAAAAAAJg/BJq4_Zffyv4/output.png"><img src="http://lh4.ggpht.com/_KZOijM4jJGU/TIqWLgCToII/AAAAAAAAAJk/7lEOr69yJY0/output_thumb.png" width="400" height="400" /></a>

Torsten Rahn told me the result looks very similar to what they obtain with xmorph :). They also had distortions appearing on the edges, which needed to be fixed by hand.

For fun, I wanted to try it with a map that hadn't been converted yet :
<a href="http://lh6.ggpht.com/_KZOijM4jJGU/TIqWkyXcrDI/AAAAAAAAAJs/CQRASGLnWrw/Nova_totius_Terrarum_Orbis_geographica_ac_hydrographica_tabula_(Hendrik_Hondius)_balanced_orig.jpg"><img src="http://lh3.ggpht.com/_KZOijM4jJGU/TIqaQQ0HqwI/AAAAAAAAAKI/NMeu-UZ3KqI/Nova_totius_Terrarum_Orbis_geographica_ac_hydrographica_tabula_(Hendrik_Hondius)_balanced_orig_thumb.png" width="500" height="351" /></a>

Here is what I obtained quickly (just the time to cut the two halves of the globe, give them to my program, and merge them properly) :
<a href="http://lh4.ggpht.com/_KZOijM4jJGU/TIqXijBP2xI/AAAAAAAAAJ4/SreMfhX3ARk/Nova_totius_Terrarum_Orbis_geographica_ac_hydrographica_tabula_(Hendrik_Hondius)_balanced.png"><img src="http://lh6.ggpht.com/_KZOijM4jJGU/TIqXlR6C49I/AAAAAAAAAJ8/NnuWWZrcnsM/Nova_totius_Terrarum_Orbis_geographica_ac_hydrographica_tabula_(Hendrik_Hondius)_balanced_thumb.png" width="500" height="250" /></a>

For those who are interested, here are the <a href="http://dl.free.fr/rBEbEXyal">sources</a>. It's written in pure C99, and under LGPL.

Now, back to Krita. I was a bit lazy the past few weeks, since I merely fixed 2 or 3 bugs related the transform worker. And now that I am back to school, I will have even less time to work on Krita. Anyway, even if it is at a lower pace, I will still find time to contribute, and maybe even blog ;).