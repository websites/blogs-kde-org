---
title: Cursor Size Problems In Wayland, Explained
authors:
  - genericity
date: "2024-10-09T09:36:02Z"
discourse: jinliu
categories:
  - KWin
  - Plasma
---

I've been fixing cursor problems on and off in the last few months. Here's a recap of what I've done, explanation of some cursor size problems you might encounter, and how new developments like [Wayland cursor shape protocol](https://wayland.app/protocols/cursor-shape-v1) and [SVG cursors](https://blog.vladzahorodnii.com/2024/10/06/svg-cursors-everything-that-you-need-to-know-about-them/) might improve the situation.

(_I'm by no means an expert on cursors, X11 or Wayland, so please correct me if I'm wrong._)

## Why don't we have cursors in the same size anymore?

My involvement with cursors started back in the end of 2023, when KDE Plasma 6.0 was about to be released. A major change in 6.0 was enabling Wayland by default. And if you enabled global scaling in Wayland, especially with a fractional scale like 2.5x, cursor sizes would be a mess across various apps (the upper row: Breeze cursors in Plasma 6.0 Beta 1, Wayland, 2.5x global scale, the lower row: Same cursors in Plasma 6.0):

![Breeze cursors in Plasma 6.0 Beta 1, Wayland, 2.5x global scale](cursor-sizes.png)

So I dug into the code of my favorite terminal emulator, [Kitty](https://sw.kovidgoyal.net/kitty/), which at the time drew the cursor in a slightly smaller size than it should be (similar to vscode in the above image). I gained some understanding of the problem, and eventually fixed it. Let me explain.

### How to draw cursors in the same size in different apps?

In X11, there used to be [a standard set of cursors](https://tronche.com/gui/x/xlib/appendix/b/), but nowadays most apps use [the XCursor lib](https://www.x.org/releases/X11R7.6/doc/man/man3/Xcursor.3.xhtml) to load a (user-specified) cursor theme and draw the cursor themselves. So in order to have cursors in the same theme and size across apps, we need to make sure that:

1. Apps get the same cursor theme and size from the system.
2. Apps draw the cursor in the same way.

The transition to Wayland created difficulties in both points:

#### 1. Get the same cursor theme and size from the system

It used to be simple in X11: we have `Xcursor.size` and `Xcursor.theme` in `xrdb`, also `XCURSOR_SIZE` and `XCURSOR_THEME` in environment variables. Setting them to the same value would make sure that all apps get the same cursor theme and size.

But Wayland apps don't use `xrdb`, and they interpret `XCURSOR_SIZE` differently: in X11, the size is in physical pixels, but in Wayland it's in logical pixels. E.g., if you have a cursor size 24 and global scale 2x, then in X11, `XCURSOR_SIZE` should be 48, but in Wayland it should be 24.

The Wayland way is necessary. Imagine you have two monitors with different DPI, e.g. they are both 24" but monitor A is 1920x1080, while monitor B is 3840x2160. You set scale=1 for A and scale=2 for B, so UI elements would be the same size on both monitors. Then you would also want the cursor to be of the same size on both monitors, which requires it to have 2x more physical pixels on B than on A, but it would be the same logical pixels.

So Plasma 6.0 no longer sets the two environment variables, because `XCURSOR_SIZE` can't be simultaneously correct for both X11 and Wayland apps. But without them and `xrdb`, Wayland apps no longer have a standard way to get the cursor theme and size. Instead, different frameworks / toolkits have their own ways. In Plasma, KDE / Qt apps get them from the Qt platform integration plugin provided by Plasma, GTK4 apps from `~/.config/gtk-4.0/settings.ini` (also set by Plasma), Flatpak GTK apps from the GTK-specific configs in [XDG Settings Portal](https://flatpak.github.io/xdg-desktop-portal/docs/doc-org.freedesktop.portal.Settings.html).

The last one is particularly weird, as you need to install `xdg-desktop-portal-gtk` in order fix Flatpak apps in Plasma, which surprised many. It might seem like a hack, but it's not. Plasma [officially recommends](https://community.kde.org/Distributions/Packaging_Recommendations) installing `xdg-desktop-portal-gtk`, and this was suggested by GNOME developers.

But what for 3rd-party Wayland apps besides GTK and Qt? The best hope is to read settings in either the GTK or the Qt way, piggy-backing the two major toolkits, assuming that the DE would at least take care of the two.

(IMHO either Wayland or the XDG Settings Portal should provide a standard way for apps to get the cursor theme and size.)

That was part of the problem in Kitty. It used to read settings from the GTK portal, but only under a GNOME session. I changed it to always try to read from the portal, even if under Plasma. But that's not the end of the story...

#### 2. Draw the cursor in the same way

It's practically a non-issue in X11, as the user usually sets a size that the cursor theme provides, and the app just draws the cursor images as-is. But if you do set a cursor size not available in the theme (you can't do that in the cursor theme settings UI, but you can manually set `XCURSOR_SIZE`), you'll open a can of worms: various toolkits / apps deal with it differently:

1. Some just use the closest size available (Electron and Kitty at the time), so it can be a bit smaller.
2. Some use the `XCursor` default size 24, so it's a lot smaller.
3. Some scale the cursor to the desired size, and the scaling algorithm might be different, resulting in pixelated or blurry cursors; Also they might scale from either the default size or the closest size available, resulting in very blurry (GTK) or slightly blurry (Qt) cursors.

The situation becomes worse with Wayland, as the user now specifies the size in logical pixels, then apps need to multiply it by the global scale to get the size in physical pixels, and try to load a cursor in that size. (If the app load the cursor in the logical size, then either the app or the compositor needs to scale it, resulting in a blurry / pixelated cursor.) With fractional scaling, it's even more likely that the required physical size is not available in the theme (which typically has only 2~5 sizes), and you see the result in the picture above.

## One way to fix it (and why I didn't do)

It can be fixed by moving the "when we can't load cursors in the size we need, load a different size and scale it" logic from apps / toolkits to the XCursor lib. When the app requests cursors in a size, instead of returning the closest size available, the lib could scale the image to the requested size. So apps would always get the cursor in the size they ask for, and their own different scaling algorithms won't get a chance to run.

Either the default behavior can be changed, or it can be hidden behind a new option. But I didn't do that, because I felt at the time that it would be difficult to either convince XCursor lib maintainers to make a (potentially breaking) change to the default behavior, or to go around convincing all apps / toolkits to use a new option.

## My fix (or shall we say workaround)

Then it came to me that although I can't fix all these toolkits / apps, they seem to all work the same way if the required physical size is available in the theme - then they just draw the cursor as-is. So I [added a lot of sizes to the Breeze theme](https://invent.kde.org/plasma/breeze/-/merge_requests/380). It only has size 24, 36 and 48 at the time, but I added physical sizes corresponding to a logical size 24 and all global scales that Plasma allows, from 0.5x to 3x, So it's 12, 18, 24 ... all the way to 72.

It was easy. The source code of the Breeze theme is SVG (so are most other themes). Then a build script renders it into images using Inkscape, and packages them to XCursor format. The script has a list of the sizes it renders in, so I added a lot more.

And it worked! If you choose Breeze and size 24, then (as in the bottom row in the picture above) various apps draw the cursor in the same size at any global scale available in Plasma.

But this method has its limitations:

1. We can't do that to 3rd-party themes, as we don't have their source SVG.
2. It only works if you choose the default size 24. If you choose a different size, e.g. 36, and a global scale 3x, then the physical size `36x3=108` is not available in the theme, and you see the mess again. But we can't add sizes infinitely, as [explained in Vlad's blog](https://blog.vladzahorodnii.com/2024/10/06/svg-cursors-everything-that-you-need-to-know-about-them/), the XCursor format stores cursor images uncompressed, so the binary size grows very fast when adding larger sizes.

Both limitations can be lifted with SVG cursors. But before getting to that, let's talk about the "right" way to fix the cursor size problem:

## The "right" fix: Wayland cursor shape protocol

The simple and reliable way to get consistent cursors across apps is to _not let apps draw the cursor at all_. Instead, they only specify the name of the cursor shape, and the compositor draws the cursor for them. This is how [Wayland cursor shape protocol](https://wayland.app/protocols/cursor-shape-v1) works. Apps no longer need to care about the cursor theme and size (well, they might still need the size, if they want to draw custom cursors in the same size as standard shapes), and since the compositor is the only program drawing the cursor, it's guaranteed to be consistent for all apps using the protocol.

(It's quite interesting that we seem to went a full circle back to the original server-defined cursor font way in X11.)

Support for this protocol leaves a lot to improve, though. [Not all compositors support it.](https://wayland.app/protocols/cursor-shape-v1#compositor-support) On the client side, both Qt and Electron have the support, but GTK doesn't.

There are [merge requests](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6212) for GTK and Mutter, but GNOME devs request [some modifications](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/issues/185) in the Wayland protocol before merging them, and the request seems to be stuck for some months. I hope the recent Wayland "things" could move it out of this seemingly deadlock.

Anyway, with this protocol, only the compositor has to be modified to support a new way to draw cursors. This makes it much easier to change how cursors work. So we come to:

## SVG cursors

Immediately after the fix in Breeze, I [proposed this idea](https://invent.kde.org/plasma/kwin/-/issues/187) of shipping the source SVG files of the Breeze cursor theme to the end user, and re-generate the XCursor files whenever the user changes the cursor size or global scale. This way, the theme will always be able to provide the exact size requested by apps. (Similar to the "modify XCursor lib" idea, but in a different way.) It would remove the limitation 2 above (and also limitation 1 if 3rd-party themes ship their source SVGs too).

With [SVG cursors support in KWin and Breeze](https://blog.vladzahorodnii.com/2024/10/06/svg-cursors-everything-that-you-need-to-know-about-them/), I plan to implement this idea. It would also allow the user to set arbitrary cursor size, instead of limited to a predefined list.

## Problems you might still encounter today

### Huge cursors in GTK4 apps

It's a new problem in GTK 4.16. If you use the Breeze cursor theme and a large global scale like 2x or 3x, you get huge cursors:

![Huge cursors in GTK4 apps](gtk4.png)

It has not limited to Plasma. Using Breeze in GNOME would result in the same problem. To explain it, let me first introduce the concept of "nominal size" and "image size" in XCursor.

Here is GNOME's default cursor theme, Adwaita:

![Adwaita cursors](adwaita.png)

"Nominal size" is the "cursor size" we are talking about above. It makes the list of sizes you choose from in the cursor theme settings UI. It's also the size you set in `XCURSOR_SIZE`. "Image size" is the actual size of the cursor image. "Hot spot" is the point in the image where the cursor is pointing at.

Things are a bit different in the Plasma default cursor theme, Breeze:

![Breeze cursors](breeze.png)

Unlike Adwaita, the image size is larger than the nominal size. That, combined with a global scale, triggers the bug in GTK4. [Explanation of the bug.](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7722)

XCursor allows the image size to be different from the nominal size. I don't know why it was designed this way, but my guess is so you can crop the empty part of the image. This both reduces file size, and reduces flicking when the cursor changes (with software cursors under X11). But the image size can also be larger than the nominal size, and Breeze (and a lot of other themes) uses this feature.

You can see in the above images that the "arrow" of nominal size 24 in Breeze is actually similar in size to the same nominal size in Adwaita. But the "badge" in Breeze is further apart, so it can't fit into a 24x24 image. That's why Breeze is built this way. In a sense, "nominal size" is similar to how "font size" works, where it resembles the "main part" of a character in the font, but some characters can have "extra parts" that go through the ceiling or floor.

This problem is already fixed [in the main branch of GTK 4](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7760), but it's not backported to 4.16 yet, probably because the fix uses a Wayland feature that Mutter doesn't support yet. So at the moment, your only option is to use a different cursor theme whose "nominal size" and "image size" are equal.

### Smaller cursors in GTK3 apps (most notably, Firefox)

The cursor code in GTK3 is different from GTK4, with its own limitations. You might find the cursor to be smaller than in other apps, and if you run the app in a terminal, you might see warnings like:

```
cursor image size (64x64) not an integer multiple of scale (3)
```

GTK3 doesn't support fractional scales in cursors. So if you have cursor size 24 and global scale 2.5x or 3x, it will use a scale 3x and try to load a cursor with a nominal size `24x3=72`. And it requires the image size to be an integer multiple of the scale. So if your theme doesn't have a size 72, or it does but the image size is not multiple of 3, GTK3 fallbacks to a smaller unscaled cursor.

## End words

OK, this is a long post. Hope I can bring you more cursor goodies in Plasma 6.3 and beyond.
