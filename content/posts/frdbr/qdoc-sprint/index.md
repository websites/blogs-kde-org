---
title: API documentation porting sprint
date: 2024-11-13
authors:
  - frdbr
categories:
  - KDE Goals
  - Documentation
  - c++
SPDX-License-Identifier: CC-BY-SA-4.0
---

![](sprint-port-card.png)

It was once said over the [grapevine](https://mail.kde.org/pipermail/kde-devel/2024-July/002849.html) that: "Our C++ API documentation has some issues, our QML API documentation has a lot of issues."

And it was true, but that is to change soon! As you might know, there is an ongoing effort to [port our documentation](https://invent.kde.org/teams/documentation/sprints/-/boards) from [Doxygen](https://www.doxygen.nl/) to [QDoc](https://doc.qt.io/qt-6/qdoc-index.html), and you can help with that. 

This is a [task](https://invent.kde.org/teams/goals/streamlined-application-development-experience/-/issues/10) that has been adopted by the [Streamlined Application Development Experience](https://phabricator.kde.org/T17396) championed by [Nicolas Fella](https://nicolasfella.de/) and [Nate Graham](https://pointieststick.com/) as part of the [KDE Goals](https://kde.org/goals/) initiative.

We would like to invite you to join our porting sprint effort to finish this task. On **November 14th at 1PM UTC**, we'll be hanging out in the [Matrix room](https://matrix.to/#/#kde-streamlined-app-dev:kde.org) working on this. Hope to see you there. 

**Some prerequisites:**

- Ability to use a terminal
- Extra disk space (30GB minimum)
- Some familiarity with APIs

[Check out the instructions](https://invent.kde.org/teams/goals/streamlined-application-development-experience/-/issues/10#note_1052720) prepared by [Thiago Sueto](https://rabbitictranslator.com/) on how to get started porting a project to QDoc.
