---
title: KDE Goals - Our Cumulative Culture
date: 2024-08-26
authors:
  - frdbr
categories:
  - KDE Goals
  - CI/CD
  - Accessibility
  - KDE Eco
SPDX-License-Identifier: CC-BY-SA-4.0
---

Every two years, the KDE community selects three goals that serve as focal points for the entire community's efforts in the coming years. This cyclical process of goal-setting and community-wide focus is a great example of KDE's [*Cumulative Culture*](https://openevo.eva.mpg.de/teachingbase/cumulative-culture/) in action.

This concept, typically observed in human societies, refers to the ability to build upon previous knowledge and innovations to create increasingly complex and effective solutions. In KDE's case, each cycle of goals represents a new layer of accumulated wisdom, i.e. new features and more stability.

## The First Cycle (2018-2020)

The first cycle of goals laid the groundwork with its focus on community growth, privacy, and usability.

* [Streamlined Onboarding](https://phabricator.kde.org/T7116): Focused on attracting and retaining new contributors by making the onboarding process smoother and more engaging.
* [Privacy Software](https://phabricator.kde.org/T7050): Prioritized user privacy and security, ensuring KDE software respects user data and complies with security standards.
* [Usability & Productivity](https://phabricator.kde.org/T6831): Aimed to enhance the usability and productivity of KDE software, making it powerful yet easy to use.

## The Second Cycle (2020-2022)

The second cycle tackled more complex challenges. Goals like Wayland implementation improvements (which layed the foundation for the Plasma 6 release), improving the app ecosystem, and ensuring consistency in design and functionality.

* [Wayland](https://phabricator.kde.org/T11081): This task aimed at stabilizing Wayland support accross KDE apps.
* [All About the Apps](https://phabricator.kde.org/T11117): Improved KDE's app infrastructure, enabling more efficient app delivery and better support services.
* [Improve Consistency across the Board](https://phabricator.kde.org/T11093): Ensured uniformity in design and functionality across KDE software, improving usability and reducing redundancy.

## The Third Cycle (2022-2024)

The third cycle, which is currently coming to an end, was about progress and adaptation. A focus to include environmental responsibility, operational efficiency, and inclusive design.

* [Sustainable Software](https://phabricator.kde.org/T15676): Focused on making KDE software more energy-efficient and environmentally friendly by implementing practices that reduce resource consumption and ensure long-term sustainability.
* [Automate and Systematize Internal Processes](https://phabricator.kde.org/T15627): Aimed to streamline KDE’s internal workflows by automating repetitive tasks, adding code tests across projects and creating a Quality Assurance team to name a few.
* [KDE For All](https://phabricator.kde.org/T15611): Seeked to make KDE software accessible and inclusive for all users.

## A New Cycle A Comin' (2024-2026)

Now, as we enter the fourth cycle of the KDE Goals, we see the full power of this cumulative process. Each goal, whether fully achieved or not, contributes to the collective knowledge and capability of the KDE community. Ideas and partial solutions from past cycles become a solid foundation of knowledge and experience that support future efforts. 

The commmunity is currently voting on the following proposals for the next KDE Goals cycle that will guide our efforts and shape our focus for the coming years:

- [Enhancing control and automation: integrate KDE Plasma (and apps) with Smart Home Ecosystems](https://phabricator.kde.org/T17435)
- [Freedom through Better Data and Workflow Organization and Management](https://phabricator.kde.org/T17458)
- [KDE Needs You! 🫵 - Formalise and boost KDE's processes for recruiting active contributors](https://phabricator.kde.org/T17439)
- [KDE-based Text Snippet Expansion](https://phabricator.kde.org/T17406)
- [Sandbox all the things!](https://phabricator.kde.org/T17370)
- [Plasma - A Beacon for Open Design](https://phabricator.kde.org/T17408)
- [Refining and Enriching KDE: Empowering Users with Convenient and Intuitive Features](https://phabricator.kde.org/T17372)
- [Streamlined Application Development Experience](https://phabricator.kde.org/T17396)
- [Unify the Plasma experience](https://phabricator.kde.org/T17391)
- [We care about your Input](https://phabricator.kde.org/T17433)

## KDE Goals at Akademy 

The three most voted goals will be [announced](https://conf.kde.org/event/6/contributions/194/) at [Akademy](https://akademy.kde.org/2024/), where there will also be a [wrap-up talk](https://conf.kde.org/event/6/contributions/200/) about the achievements of the current goals. Also, there will be [Birds-of-a-feather (BoF)](https://community.kde.org/Akademy/2024/Monday) sessions with the new goal champions.

*Join the [Matrix room](https://go.kde.org/matrix/#/#goals:kde.org) and keep an eye on the [website](https://kde.org/goals/) for the latest KDE Goals updates.*
