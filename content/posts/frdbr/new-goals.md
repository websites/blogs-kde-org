---
title: KDE Goals - A New Cycle Begins
date: 2024-09-07
authors:
  - frdbr
categories:
  - KDE Goals
SPDX-License-Identifier: CC-BY-SA-4.0
---

<div class="text-center">
    <img src="https://kde.org/content/goals/KGoals-logo.png" alt="KDE Goals logo" style="width:65%;height:65%;margin-bottom:70px"/>
</div>

The KDE community has charted its course for the coming years, focusing on three interconnected paths that converge on a single point: community. These paths aim to improve user experience, support developers, and foster community growth.

## [Streamlined Application Development Experience](https://phabricator.kde.org/T17396)

This goal focuses on improving the application development process. By making it easier for developers to create applications, KDE hopes to attract more contributors and deliver better software for both first-party and third-party applications. A notable task within this goal is enhancing the experience of building KDE apps with languages beyond C++, such as Rust or Python.

**Champions: Nicolas Fella and Nate Graham**


## [We care about your Input](https://phabricator.kde.org/T17433)

KDE has a diverse users base with unique input needs: artists using complex monitor and drawing tablet setups; gamers with controllers, fancy mice, and handhelds; users requiring accessibility features or using a language optimally types with complex input methods; students with laptops, 2-in-1s, and tablets — and more! While KDE has made significant progress in supporting these diverse sources of input over the years, there are still gaps to be addressed. This goal aims to close those gaps and deliver a truly seamless input experience for everyone.

**Champions: Gernot Schiller, Jakob Petsovits and Joshua Goins**


## [KDE Needs You! 🫵](https://phabricator.kde.org/T17439) 

KDE’s growth depends on new contributors, but a lack of fresh involvement in key projects like Plasma, Kdenlive, Krita, GCompris, and others is a concern. This goal focuses on formalizing and enhancing recruitment processes, not just for individuals but also for institutions. Ensuring that bringing in new talent becomes a continuous and community-wide priority, vital for KDE's long-term sustainability.


**Champions: Aniqa Khokhar, Johnny Jazeix and Paul Brown**


## Join us!

Your voice, your code, and your ideas are what will shape the KDE of tomorrow — whether you're a user, developer, or contributor. Let’s go on this journey together and make these goals a reality! 

Join the [Matrix room](https://go.kde.org/matrix/#/#goals:kde.org) and keep an eye on the [website](https://kde.org/goals/) for the latest KDE Goals updates.