---
title: Amarok 3.2.2 released
date: "2025-02-15T11:20:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of Amarok 3.2.2, the second bugfix release for [Amarok 3.2 "Punkadiddle"!](https://apps.kde.org/amarok/)

3.2.2 features some minor bugfixes, and improvements for building Amarok on non-UNIX systems and without X11 support. Additionally, a 16-year-old feature request has been fulfilled.
Concluding years of Qt5 porting and polishing work, Amarok 3.2.2 is likely to be the last version with Qt5/KF5 support, and
it should provide a nice and stable music player experience for users on various systems and distributions.
The development in git, on the other hand, will soon switch the default configuration to Qt6/KF6, and focus for the next 3.3 series will be to ensure that everything functions nicely with the new Qt version.


#### Changes since 3.2.1  
##### FEATURES:  
   * Try to preserve collection browser order when adding tracks to playlist ([BR 180404](https://bugs.kde.org/180404))  

##### CHANGES:  
   * Allow building without X11 support  
   * Various build fixes for non-UNIX systems  

##### BUGFIXES:  
   * Fix DAAP collection connections, browsing and playing ([BR 498654](https://bugs.kde.org/498654))  
   * Fix first line of lyrics.ovh lyrics missing ([BR 493882](https://bugs.kde.org/493882))  

### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to get updated to 3.2.2 soon, as well as
the flatpak available on [flathub](https://flathub.org/apps/org.kde.amarok).

### Packager section

You can find the tarball package on
[download.kde.org](https://download.kde.org/stable/amarok/3.2.2/amarok-3.2.2.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
