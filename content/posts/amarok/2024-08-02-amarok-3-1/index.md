---
title: Amarok 3.1 "Tricks of the Light" released!
date: "2024-08-02T21:00:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of [Amarok 3.1 "Tricks of the Light"!](https://apps.kde.org/amarok/)

![Amarok 3.1 player window with a customized layout, featuring the new Similar Artists applet](https://cdn.kde.org/screenshots/amarok/amarok31.png)

Coming three months after 3.0.0 and two months after the first bugfix release 3.0.1, the main development focus in 3.1 has been getting Qt6 / KDE Frameworks 6
based version closer. We are not quite there yet, but not that far away anymore. And there are some quite nice new features too! Amarok 3.1.0 brings in a refreshed Last.fm integration,
which uses more up-to-date account connection mechanisms, and is better at informing users of any Last.fm errors. Similar Artists context applet does a comeback,
and there's naturally also a nice bunch of smaller features and bug fixes; this time the oldest fulfilled feature request was filed just under 15 years ago.

#### Changes since 3.0.1
##### FEATURES:  
* Last.fm plugin updated to use token-based authentication method and to notify user of session key errors ([BR 414826](https://bugs.kde.org/414826), [BR 327547](https://bugs.kde.org/327547))  
* Reintroducing Last.fm Similar Artists context applet - a new Amarok 3 version  
* Remember the previous destination provider when saving playlist ([BR 216528](https://bugs.kde.org/216528))  

##### CHANGES:  
* Amarok now depends on KDE Frameworks 5.89.  
* Cleanup of unused code and various changes that improve Qt6 compatibility but shouldn't affect functionality.  (n.b. one won't be able to compile a Qt6 Amarok with 3.1 yet, but perhaps with the eventual 3.2) 
* Remove old derelict openDesktop.org integrations from about dialog. This also removes the dependency to Attica framework.  
* Disable gapless playback if ReplayGain is active and the following track is not from same album, to avoid volume spikes due to delay in the applying of ReplayGain ([BR 299461](https://bugs.kde.org/299461))  

##### BUGFIXES:  
* Small UI and compilation fixes  
* Fix saving and restoring playlist queue on quit / restart ([BR 338741](https://bugs.kde.org/338741))  
* Fix system tray icon menu reordering  
* Fix erroneous apparent zero track progresses on track changes, which caused playcount updates and scrobbles to get skipped ([BR 337849](https://bugs.kde.org/337849), [BR 406701](https://bugs.kde.org/406701))  
* Fix 'save playlist' button in playlist controls  
* Sort playlist sorting breadcrumb menu by localized names ([BR 277146](https://bugs.kde.org/277146))  
* Miscellaneous fixes to saving and loading various playlist file formats, resulting also in improved compatibility with other software (including [BR 435779](https://bugs.kde.org/435779), [BR 333745](https://bugs.kde.org/333745))  
* Don't show false reordering visual hints on a sorted playlist ([BR 254821](https://bugs.kde.org/254821))  
* Fix multiple instances of web services appearing in Internet menu after saving plugin config.  
* Show podcast provider actions for non-empty podcast categories, too ([BR 371192](https://bugs.kde.org/371192))  
* Fix threading-related crashes in CoverManager ([BR 490147](https://bugs.kde.org/490147))  


In comparison to changes between 2.9 to 3.0, the git repository statistics between 3.0 and 3.1 are somewhat short:  

Tuomas Nurmi: 164 commits, +11910, -29957  
l10n daemon script: 97 commits, +207075, -237380  
Pino Toscano: 13 commits, +1, -11152  



However, this is an excellent spot to send a huge thank you out to everyone who has been on board outside the git history;
translators, packagers, bug reporters, writers, commenters, and of course: users - music fans all around the world!
Happy listening, everyone! You rok!



### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to update to 3.1.0 soon.
A flatpak is currently available on [flathub-beta](https://discourse.flathub.org/t/how-to-use-flathub-beta/2111).

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/amarok/3.1.0/amarok-3.1.0.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
