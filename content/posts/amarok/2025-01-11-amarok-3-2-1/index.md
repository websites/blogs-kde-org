---
title: Amarok 3.2.1 released
date: "2025-01-11T19:00:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of Amarok 3.2.1, the first bugfix release for [Amarok 3.2 "Punkadiddle"!](https://apps.kde.org/amarok/)

3.2.1 features fixes for some small UI bugs, improvements for file transfers to MTP devices, and some compilation fixes for different combinations of Qt6 versions and compilers, enabling easier testing of Qt6 builds.
Additionally, it is now theoretically possible to enable last.fm and gpodder.net support in a Qt6 build. However, this requires Qt6 support in liblastfm and libmygpo-qt; a functionality that
is not yet included in the most recent released versions of the libraries.


#### Changes since 3.2.0  
##### CHANGES:  
   * Support gpodder and lastfm on Qt6 builds  
   * Limit maximum current track font size more when context view is narrow  

##### BUGFIXES:  
   * Fix displaying settings button for Internet services  
   * Enable Wikipedia context applet on Qt6 builds  
   * Don't crash when copying multiple files to MTP device ([BR 467616](https://bugs.kde.org/467616))  
   * Avoid unnecessarily flooding MTP devices with storage capacity queries  
   * Compilation fixes for various compiler + Qt6 version combinations  

### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to get updated to 3.2.1 soon, as well as
the flatpak available on [flathub](https://flathub.org/apps/org.kde.amarok).

### Packager section

You can find the tarball package on
[download.kde.org](https://download.kde.org/stable/amarok/3.2.1/amarok-3.2.1.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
