---
title: Beta for Amarok 3.1 available
date: "2024-07-23T21:30:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---


Dear fans of music & open source music players,  
in preparation of the upcoming Amarok 3.1 release, a beta release (3.0.81) has been prepared.

As is observable from the [ChangeLog](https://invent.kde.org/multimedia/amarok/-/blob/d1bc3f421b2b5f0843595fdf8712add7daf0b264/ChangeLog),
in addition to various bugfixes, there will be some, but not that many, new features included in the upcoming version.
However, there has been a lot of Qt6 compatibility preparation work done under the hood, so version number 3.1 reflects the amount of changed code better than 3.0.2 would.
3.1.0 is likely to be released in early August, and all help catching any regressions during this period is highly appreciated. (*n.b. one won't be able to compile a Qt6 Amarok with 3.1 yet, but perhaps with the eventual 3.2*)


The source tarball is available on
[download.kde.org](https://download.kde.org/unstable/amarok/3.0.81/amarok-3.0.81.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
There doesn't appear to be many binary packages of the beta available, at least at the moment, but the various nightly git builds provided by various splendid packagers
are also based on corresponding source code, so using them and reporting findings is also a valid way to participate in the beta test effort.

Happy listening!
