---
title: Amarok 3.1.1 released
date: "2024-09-29T09:40:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of Amarok 3.1.1, the first bugfix release for [Amarok 3.1 "Tricks of the Light"!](https://apps.kde.org/amarok/)

3.1.1 features a number of small improvements and bug fixes, including miscellaneous fixes for toolbars and the return of tag dialog autocompletions, a functionality that initially got lost during the Qt5/KF5 port.
However, most of the work has again happened under the hood to improve the codebase's Qt6/KF6 compatibility.
For the 3.2 version coming up later this year, the KDE frameworks dependency will be raised to 5.108. This should allow replacing the remaining deprecated KF5 functionalities; one of the final barriers preventing Qt6/KF6 based builds from succeeding.


#### Changes since 3.1.0
##### CHANGES:
   * Most of the context view QML items ported from QtControls 1 to QtControls 2
   * Default to no fadeout on pause and stop ([BR 491603](https://bugs.kde.org/491603))

##### BUGFIXES:
   * Actually show the file browser panel toolbar
   * Fix track editor autocompletions ([BR 491520](https://bugs.kde.org/491520))
   * Ensure home icon is shown in browser breadcrumb widgets ([BR 491354](https://bugs.kde.org/491354))


### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to get updated to 3.1.1 soon, as well as
the flatpak available on [flathub](https://flathub.org/apps/org.kde.amarok).

### Packager section

You can find the tarball package on
[download.kde.org](https://download.kde.org/stable/amarok/3.1.1/amarok-3.1.1.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
