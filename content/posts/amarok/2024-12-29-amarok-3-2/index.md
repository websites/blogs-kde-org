---
title: Amarok 3.2 "Punkadiddle" released!
date: "2024-12-29T19:30:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of [Amarok 3.2 "Punkadiddle"!](https://apps.kde.org/amarok/)

2024 was the year that finally introduced a Qt5/KF5 based Amarok 3 release in April, and it was followed by a number of 3.x bugfix and feature releases.
Now, to conclude 2024, it is time for Amarok 3.2.0. The most interesting change is probably the ability to build the same codebase on both Qt5 and Qt6.
Qt5/KF5 is still the recommended, tested configuration, for now. Qt6 version should be usable, but in addition to any unknown issues, there are a number
of known issues documented in README.

Additionally, 3.2.0 includes e.g. collection filtering and Ampache related features and fixes, oldest resolved feature request being from 2013.
Multiple long-standing crash bug reports have been closed lately, and probable fixes for various issues observed in crash report data have been introduced.
Amarok 3.2.0 should thus feature slightly improved stability.
Some 3.2.x bugfix releases are to be expected in 2025, before the focus shifts to preparations for an "Amarok 4".


#### Changes since 3.1.1
##### CHANGES:  
   * Building an experimental Qt6/KF6 Amarok version is now possible  
   * Allow filtering collection by lack of tag / empty tag ([BR 325317](https://bugs.kde.org/325317))  
##### CHANGES:  
   * Amarok now depends on KDE Frameworks 5.108  
   * Show current track context applet by default  
##### BUGFIXES:  
   * Probably fix occasional crashes when filtering collection ([BR 492406](https://bugs.kde.org/492406))  
   * Probably fix occasional crashes when clearing CompondProgressBars  
   * Fix context view applets on Qt6/KF6  
   * Fix Ampache login on server version 5.0.0 and later ([BR 496581](https://bugs.kde.org/496581))  
   * Fix crash if Ampache login is redirected ([BR 396590](https://bugs.kde.org/396590))  


The git repository statistics between 3.1.0 and 3.2.0 are as follows:  

l10n daemon script: 68 commits, +41987, -35275  
Tuomas Nurmi: 46 commits, +723, -4854  
Raresh Rus: 3 commits, +41, -53  
Ian Abbott: 1 commit, +17, -3  
Heiko Becker: 1 commit, +0, -3  

### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to get updated to 3.2.0 soon, as well as
the flatpak available on [flathub](https://flathub.org/apps/org.kde.amarok).

### Packager section

You can find the tarball package on
[download.kde.org](https://download.kde.org/stable/amarok/3.2.0/amarok-3.2.0.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
