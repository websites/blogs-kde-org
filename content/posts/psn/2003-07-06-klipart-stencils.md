---
title:   "Klipart Stencils"
date:    2003-07-06
authors:
  - psn
slug:    klipart-stencils
---
Ok Kristof Borrey&#039;s Klipart Stencils are in cvs now.<br/>
I decided to rearrange them a bit though, this is how they are arranged in cvs:<br/>
<br/>
<ul>
<li>
Geographic
<ul>
<li>Flags</li>
<li>Maps</li>
</ul>
</li>
<li>
Hardware - This stencilset have been broken up in to two and put into it&#039;s own category
<ul>
<li>Computer - All computer related thing went in here</li>
<li>Miscellaneous - The rest went here :)</li>
</ul>
</li>
<li>
Miscellaneous - Here I put the ones I could not come up with a good category for...
<ul>
<li>Arrows</li>
<li>Buildings</li>
<li>People</li>
<li>Transport</li>
</ul>
</li>
</ul>
<br/>
If you do not use cvs and still want these stencils you can find them here:<br/>
<a href=\\\"http://www.kde-look.org/content/show.php?content=6907\\\">
http://www.kde-look.org/content/show.php?content=6907</a>