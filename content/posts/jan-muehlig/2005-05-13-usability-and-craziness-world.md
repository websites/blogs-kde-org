---
title:   "/., usability and the craziness of the world"
date:    2005-05-13
authors:
  - jan muehlig
slug:    usability-and-craziness-world
---
Tx to a <a href="http://developers.slashdot.org/article.pl?sid=05/05/12/2150231&tid=121&tid=189&tid=156&tid=8" target="_blank">slashdot article</a> about KDE and OpenUsability, I am really confused, perhaps sad. It was the discussion afterwards. I did not know that the world is devided into Gnome and KDE. But if I read (some of) the comments, I get the impression. True, the idea for the OpenUsability project came out of discussions during Nove Hrady two years ago, and many KDE projects do like it and use it, and the OU project learned a lot about potential workflows and how to ease cooperation from it. But, and let me quote: "Now getting usability expert on board to solve these issues sure is the right way and if KDE 3.4 is anything to judge from, there are great things to come for KDE." Hello? Again, anyone who produces free software can and should make use of OpenUsability.<!--break-->