---
title:   "Where did all these projects come from?"
date:    2004-04-12
authors:
  - mattr
slug:    where-did-all-these-projects-come
---
I just looked at the mound of stuff I have in my local CVS tree, in addition to the bits and pieces that I have scattered around the KDE CVS tree and I started thinking, "Where did all these projects come from?" I've got quite several going on. They are:
<ul>
<li>Kopete - bug fixing. Seems like i'm the only active developer at the moment (even though I'm really not)
<li>KChangelogMaker - little app in kdenonbeta to edit changelogs. I need to finish it so i can release a 0.1 version
<li>Kexi's ODBC Framework - Need time at work to work on it since that's where the servers are
<li>KDEQA - App similiar to the Query Analyzer app found in the MS SQL Server product. I hate using isql
<li>YMSG Protocol Documentation - Vaporware as of yet. I haven't even created the CVS module for it. Need to decide on the format
<li>libyahoo2++ - C++ port of the yahoo backend that kopete uses. I told the libyahoo2 project leader that i'd take it over. As of yet, haven't done anything with it.
<li>Secret Project - i can't tell people what it is yet, because I don't want to be bombarded with mail about it (like I expect to be)

So, yup, I'm working on alot right now. Maybe I'll get some of it finished, and if you want to help, drop me a line.

