---
title:   "reporting that there's really not much to report"
date:    2004-04-22
authors:
  - mattr
slug:    reporting-theres-really-not-much-report
---
hmmm, not much to report really (watch, I'll end up writing a huge blog ;-) ). I've lost interest in hacking on anything network related at the moment, so I haven't been working on Kopete much lately, although I did start rewriting some OSCAR code to remove the old Kit api that's been polluting it and generally causing bugs with the contact list handling. :-( I might complete that at the end of this week if I get around to it (and feel like it). <br><br>
I dropped from the top spot in the bugs closed list earlier this week, which is good, because i don't feel like fixing any bugs right now anyways. The bug reporters have been kinda sassy and seem like they think they know it all lately, at least with the Kopete reports. However, I suppose that's what you should expect when you're hacking on an IM application everybody expects to be perfect, and if it's not, you get bitched at like you're a damn moron. I'd like to see them do it. I should start a page for Kopete like Don Sanders did for Kontact. Even better would be finding somebody to sponsor my continued development on Kopete and KDE in general. :-) I'll keep dreaming, but for now, I'll go back to hacking on the secret project.