---
title:   "running more than one distro on the same machine"
date:    2004-05-20
authors:
  - mattr
slug:    running-more-one-distro-same-machine
---
I've been thinking for quite some time on how to install multiple distributions onto a single hard disk. At the moment, my sole reason for doing this would be to troubleshoot bugs on the various distributions since some of them seem to be distribution specific, and I'm sure I'll find other uses later. <!--break-->I'm pretty sure I've got a decent idea for booting things in my head, it should be pretty easy, and sharing a boot partition should be a piece of cake.  I have a few ideas for how I can share certain things from the home directory as well. Perhaps when I feel like being without a system for a few days, I'll give it a whirl.  Here's the distributions I was thinking of installing:
<ul>
<li>Fedora
<li>Mandrake
<li>SuSE
<li>Gentoo
<li>Debian
</ul>

I'm thinking about adding Conectiva and PLD to the list, but I'm not sure yet. With 110 GB of space, I'm sure I'll have room. :-)  It'll be interesting to see how the various distributions' installers will handle the partition scheme that I'll have to set up. Hopefully there's not a limit on the number of logical partitions one can make in an extended partition. IIRC, the total number of available partitions is 26 which I hope I won't hit. I'd better remember to make backups first. ;-)<br><br>
Since I don't really have the motivation to do any active coding at the moment, I may start tonight or tomorrow night depending how whether or not I get the partition scheme and downloading done in time. <br><br>
If anybody wants a HOWTO, let me know, so I can make sure to write neatly as I take notes so I can read them later. ;-)

