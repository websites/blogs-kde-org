---
title:   "The translators are going to hate me"
date:    2003-07-27
authors:
  - mattr
slug:    translators-are-going-hate-me
---
I'm trying to get kopete's .pot file updated since it's three days old. I was under the impression that they were generated automagically by one of the kde servers, but I was wrong. :( I'm trying to get it done today since today is the start of kopete's string freeze before 0.7 comes out around August 4th.  I don't think I'm going to get my fix for the yahoo account online with invalid data bug in for Kopete 0.7 because of the string freeze, although I might be able to, since it doesn't add any strings, it just moves them somewhere, so I might do that if I can get rid of the stupid select bug ( bug:56028 ).  Maybe after 0.7 (and when I have time), I'll port libyahoo over to QT/KDE so I can use things like KExtendedSocket for automatic proxy stuff, and maybe even get to use KIO for file transfers, and then I also won't have to worry about C with it's stupid callbacks. I think the functors in the IRC protocol are enough to keep my head spinning.
<!--break-->
In other news, my algorithms project is 45% done now. 5% of that is code, the remaining 45% is testing and writing the report. That shouldn't be too hard really, since I've had a couple of Tech Writing classes and there's going to be lots of tables and graphs and charts anyways. 

I also have a laptop now. It's a PIII-500 with 256MB of RAM. It's installing Gentoo right now (thank you distcc) and I'm sure it will be nice to have.