---
title:   "No More Classes!"
date:    2003-08-08
authors:
  - mattr
slug:    no-more-classes
---
YAY! One last exam (at 6pm today) and then I'm done with stupid summer classes!! After that exam, the weekend starts, along with my two week break from school before fall classes start. Fortunately for me, I'm only taking 12 hours in the fall, and so I should have plenty of time to hack then too. I get to hack at work sometimes too, although It's mostly when I don't have anything else to do, so wouldn't say that I get paid to hack on KDE. But all in all, it just looks like I'll have more time for KDE hacking (which is always a good thing). :D
<!--break-->

In case you're wondering, I'm only taking 12 hours in the fall, which includes Linear Algebra, a Technical Writing class (don't remember which one), Programming Languages (I hope we go over some cool ones, like Ruby), and a Directed Study (Unix Sys. Admin.). I'm actually looking forward to it, since it's my last semester and then I graduate. 

Anyways, with my two week break from school, I hope to get some of the following on my TODO list done:

port libyahoo to QT/KDE stuff since right now it's just C.
port AIM and ICQ in Kopete to use KExtendedSocket instead of QSocket.
Hack on KTagit, my MP3 Tag and Rename program since I've let it languish a little bit with all the classes and stuff.

Those are the main things, I'm also going to do a few other little things, but not anything worth mentioning here.