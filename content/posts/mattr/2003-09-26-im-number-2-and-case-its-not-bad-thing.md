---
title:   "I'm number 2! (and in this case, it's not a bad thing)"
date:    2003-09-26
authors:
  - mattr
slug:    im-number-2-and-case-its-not-bad-thing
---
Wow, it's amazing what a little venting can do for your motivation!. I <a href="http://blogs.kde.org/node/view/199">ranted</a> about how certain people were pissing me off and that I just didn't have motivation to work on Kopete a whole lot anymore. Well, after ranting, I feel much better! Being in your last semester of uni and taking only 9 hours of class gives you lots of free time to work on free software. :)  (and I prefer to use it doing something other than playing video games, although Halo has become rather addicting)
<!--break--><br><br>
I currently sit at #2 on the "Top 15 people who resolved the most reports in the last 7 days" list. While I'm not really trying to gloat, there's something uniquely satisfying about being #2 on that list. I don't think I'll ever catch up to coolo, unless he slows down,  so I think I won't reach for the lofty goal of 150+ bugs closed yet. Maybe when I'm old and gray and KDE 40.5 is out. ;)
<br><br>
Anyways, I'm curious to see if there's anybody who can knock me off the number two spot in the top 15 bug closers rankings. Unless I have a slow weekend (it's starting to look likely that I will, I have a lot of schoolwork to do this weekend), it's not gonna happen. ;) 
<br><br>
 So, is there anybody who wants to wager on which application coolo is going to start fixing bugs for next? ;) 