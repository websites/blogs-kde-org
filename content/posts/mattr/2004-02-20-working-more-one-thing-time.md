---
title:   "Working on more than one thing at a time"
date:    2004-02-20
authors:
  - mattr
slug:    working-more-one-thing-time
---
Recently, I've decided to find something else in the KDE project that interests me, familiarize myself with its inner workings, and start hacking on it.  I've come up with a couple of things, mostly KIO, and KOffice (KWord, KSpread, and Kexi to be exact). The problem that I've run into is one of time management. Now that I have a full time job, I don't exactly have the amount of time that I can devote to hacking KDE that I used to when i was still in university. I'm curious to know how people manage their time with working on more than one piece of KDE at a time. 
<!--break-->
So, i'm interested to hear how other people manage their time when working one more than one thing in the KDE project. Oh, and if you know of a company (or person) that would be willing to employ or sponsor a KDE hacker, point them in my direction. :)