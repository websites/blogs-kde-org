---
title:   "GSOC: Implemention of GeoNames support and ExtendedData tag handler to Marble"
date:    2010-06-19
authors:
  - hjain
slug:    gsoc-implemention-geonames-support-and-extendeddata-tag-handler-marble
---
Hello viewers,

I am writing this blog to share the progress of work I have done till now as Gsoc 2010 participant. The official date of writing code was 24th May,2010. In the meantime, I was unable to resist myself to develop marble. So I decided to change the data source of city placemarks from <a href="http://www.world-gazetteer.com/">World Gazetteer</a> to <a href="http://www.geonames.org/">GeoNames</a>. GeoNames has great collection of over 8,000,000 geographical names. Beyond names of places in various languages, data provided by GeoNames include latitude, longitude, elevation, population, administrative subdivision and postal codes. Marble now shows all cities with a population greater than 15000. This has solved various bugs in Marble such as <a href="https://bugs.kde.org/show_bug.cgi?id=232450">mirrored coordinates bug</a>. This displays the correct name for many Indian cities like Delhi, Mysore, Hyderabad, Meerut. Screenshots of Marble using World Gazetteer and GeoNames data source are:

<p><a href="http://img33.imageshack.us/img33/6905/marbleworldgazzatteer.png"><img border="0" src="http://img33.imageshack.us/img33/6905/marbleworldgazzatteer.png" title="World-Gazetteer" width="40%" height="40%" /></a>    <a href="http://img529.imageshack.us/img529/6730/marblegeonames.png"><img border="0" src="http://img529.imageshack.us/img529/6730/marblegeonames.png" title="GeoNames" width="40%" height="40%" /></a></p>

The text file containing data of city placemarks is converted into KML file. The cache files are generated from the KML files by Marble which are shipped in the package and installed. However that there was a bug in Marble which prevented regeneration of these cache file. I fixed this bug to develop the cache from KML files. This fix will be back-ported to older version of Marble in future.

After the start of official period of writing code, I implemented the <a href="http://code.google.com/apis/kml/documentation/kmlreference.html#extendeddata">ExtendedData</a> tag handler. &lt;ExtendedData&gt; element offers following three techniques for adding custom data to a KML Feature.

<code>
<ExtendedData>                       
  <Data name="string">
    <displayName>...</displayName>    <!-- string -->
    <value>...</value>                <!-- string -->
  </Data>
  <SchemaData schemaUrl="anyURI">
    <SimpleData name=""> ... </SimpleData>   <!-- string -->
  </SchemaData>
  <namespace_prefix:other>...</namespace_prefix:other>
</ExtendedData>
</code>
<br/>

 After having discussion with <a href="https://blogs.kde.org/user/6843">Bastian Holst</a> and <a href="https://blogs.kde.org/user/551">Torsten Rahn</a>, we decided to use basic technique of adding untyped data/value pairs using the &lt;Data&gt; element because of its simplicity and ease of implementation. &lt;ExtendedData&gt; element will store all the marble specific information like timezone, gmt, dst, bookmark, geonameid etc. GeoDataExtendedData class stores the data object of &lt;ExtendedData&gt; element in QHash. In the forthcoming days, I will display time zone information of all city placemarks using &lt;ExtendedData&gt; element. Its structure will be :-

<code>
    <Placemark>
        <name>Andorra la Vella</name>
        <ExtendedData>
            <Data name="timezone">
                <value>Europe/Andorra</value>
            </Data>
            <Data name="gmt">
                <value>1.0</value>
            </Data>
            <Data name="dst">
                <value>2.0</value>
            </Data>
        </ExtendedData>
    </Placemark>
</code>
<br/>

That's all for now. Thanks for reading it... :)