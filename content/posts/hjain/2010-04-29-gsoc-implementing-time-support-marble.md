---
title:   "GSOC: Implementing Time Support to Marble "
date:    2010-04-29
authors:
  - hjain
slug:    gsoc-implementing-time-support-marble
---
Hello blogging world.. This is my first blog entry. I am having my final examination from day after tomorrow, so wish we best of luck ;)

About me:
I am Harshit Jain, a undergraduate student from Institute of Technology, Banaras Hindu University (IT-BHU), India. Currently, I am in 6th semester in Computer Science and Engineering. My hometown is Bhilwara, India.


Goal of my GSOC project:
This summer I will work on implementing time support feature in Marble. This project will strengthen the framework and allow user to browse through time to view the information relevant to him.

About project:
The aim of this project is to make Marble more interesting for user by showing information about past and future time. Some of such interests can be viewing historical maps, viewing maps of evolution of the Earth over million years, viewing satellite maps of the Earth in different months, viewing set of continental drift maps, having weather report of particular place at specific time. Time support will also assist the animation by displaying rapid sequences of images. These images will have some time tag associated with it. One such feature requiring animation can be displaying various recent earthquakes. Time support will give functionality to online services to get the data for selective time period. Implementation of KML time tags will enhance KML support of Marble. Marble already supports GPS devices for navigation. Using TimeStamp tag of KML, user will be able to track his route with time stamp for each placemark. The time zone of any placemark along with its GMT (Greenwich Mean Time) and DST (Daylight Saving Time) will be shown in 'Marble Info Center' dialog box. Thus, time support will enable Marble to display information with great education value to users.

My project consists of following eight tasks:
* Implementation of the KML time tags
* Displaying time zone information
* Implementing central 'internal' clock
* Porting starry sky and sun shading feature over to the new class design
* Implementing GUI 'Time Slider'
* Showing monthly maps in 'Satellite View' theme
* Using the new time support in Marbles Online Service plugins
* Adding Palaeogeographic maps

That's all for now. Hope you enjoyed reading it. :)