---
title:   "Compiling all of Qt5, KF5, Plasma5, kdepim5, apps..."
date:    2016-05-16
authors:
  - dfaure
slug:    compiling-all-qt5-kf5-plasma5-kdepim5-apps
---
I see a very high value in compiling my own Qt, and on top of it all the KDE-made software that I use. This makes it very easy to fix bugs and add improvements to the software I use on a day to day basis. Nowadays I think many developers use distro packages for Qt or KF5 or even the whole environment except for the one app they're working on, but in my opinion this leads to "silo" thinking, with workarounds in upper layers rather than fixing issues at the right place.

So, here's a working and easy recipe for compiling all the Qt-based software you use.

In theory all you need is <b>kdesrc-build</b> and a minimal kdesrc-buildrc that sets some vars (including "branch-group kf5-qt5") and then includes extragear/utils/kdesrc-build/kf5-qt5-build-include (see <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source">wiki</a> for more info).

One issue though is <b>dependencies</b>. I'm on OpenSuSE Tumbleweed, not gentoo, so the goal is not to compile everything including aspell by hand. So I use *-devel distro packages for everything that doesn't depend on Qt. OpenSuSE users can install all the devel packages necessary for this by downloading <a href="http://www.davidfaure.fr/kde/packages-devel">this file</a> and then doing zypper in `cat packages-devel` (I'll mention this in <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Install_the_dependencies">the wiki</a>).

However for any package that depends on Qt, it becomes messy to use distro devel packages, because it's mixing the distro Qt and my self-compiled Qt. So the best solution is to compile yourself any dependency that uses Qt.
And here's <u>the point of this post</u>, I just committed to kdesrc-build a file you can include for this:
<code>include extragear/utils/kdesrc-build/custom-qt5-libs-build-include</code>

With the devel packages above installed and this kdesrc-build setup, kdesrc-build builds <b>294 modules</b>, and it all succeeds except for artikulate which requires qt5gstreamer (there's an option to turn that off but I can't set that in custom-qt5-libs-build-include, I sent a mail to Andreas Cord-Landwehr about making this more automated), and right now calligra+kexi have a build error but that's unrelated to this setup.

Then you can boot into this version of plasma5 (e.g. with a ~/.xsession file).  The only thing you need to take care of is <b>screen unlocking</b>. I think kscreenlocker still needs suid root (which breaks installing kscreenlocker later on, we still need to fix cmake about that), but more importantly, you need to set the right PAM module for kscreenlocker. On OpenSuSE this means the following lines in your kdesrc-buildrc (yes it's still called "KDE4", no typo there) :
<pre>options kscreenlocker
  cmake-options -DKDE4_COMMON_PAM_SERVICE=xdm
end options
</pre>

If you use other Qt-based software, you can add it to your kdesrc-buildrc, even if it doesn't come from git.kde.org, using the "module" syntax with a "repository" being a git URL. For instance I add the gammaray debugging tool to the compilation :
<pre>module gammaray
  repository git@github.com:KDAB/GammaRay.git
  branch master
end module
</pre>

The <b>end result</b>: a successful kdesrc-build of 302 out of 306 modules, (the 4 failures being artikulate, calligra, kexi and kscreenlocker, i.e. all investigated and worked on), that makes me very happy. Let's keep it working. And I'm finally using a self-compiled plasma5 environment with kdepim5, on my main laptop. Bugs be warned, here I come!