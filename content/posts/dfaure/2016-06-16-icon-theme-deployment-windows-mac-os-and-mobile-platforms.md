---
title:   "Icon theme deployment on Windows, Mac OS and mobile platforms"
date:    2016-06-16
authors:
  - dfaure
slug:    icon-theme-deployment-windows-mac-os-and-mobile-platforms
---
During the Randa meeting I implemented some magic in KIconThemes to generalize a design initially made by Christoph Cullmann for Kate.

On platforms other than Linux/BSD, icon themes are not available as part of the system, and installing 6235 files takes forever. The deployment strategy for applications on those operations systems is now the following:

<ul>
<li>breeze-icons (and in the future other icon themes, probably), when configured with -DBINARY_ICONS_RESOURCE=ON, installs .rcc files (binary resources, loadable by Qt).</li>
<li>The installation process copies one of these under the name "icontheme.rcc", in a directory found by <a href="http://doc.qt.io/qt-5/qstandardpaths.html#StandardLocation-enum">QStandardPaths::AppDataLocation</a>. For instance on Windows, icontheme.rcc is usually installed in APPROOT\data\, while on Mac OS it is installed in the Resources directory inside the application bundle.</li>
<li>As long as the application links to KIconThemes (even if it doesn't use any of its API), the icontheme.rcc file will be found on startup, loaded, and set as the default icon theme.</li>
</ul>

Therefore, no code modification is needed in KDE applications, to make them find an icon theme from a rcc file, it's just a matter of packaging the desired theme into the right location. And the users can even swap that with another theme rcc file easily if they want to.

The official long-term version of this explanation is available in <a href="https://api.kde.org/frameworks/kiconthemes/html/index.html">the KIconThemes API documentation</a>.

<div class="separator" style="clear: both; text-align: center;">
<a href="https://www.kde.org/fundraisers/randameetings2016/"><img border="0" height="55" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="400" /><br />
Help us keep going, at this and many other sprints!</a></div>