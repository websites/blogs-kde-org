---
title:   "Compiling *all* of KDE SC from svn/git"
date:    2013-06-18
authors:
  - dfaure
slug:    compiling-all-kde-sc-svngit
---
On my desktop machine, I have always tried to compile "everything". At a time it was easy: about 18 SVN modules listed in kdesvn-buildrc, trunk for everything, done.
Nowadays there are many more git modules - I currently compile 234 modules, with the help of kdesrc-buildrc (I don't have to list all 234 modules, it expands them for me). Still it's a bit difficult to ensure I really have everything...

Of course I don't use every KDE application, but that's not the point. I want users and distros to be able to actually build the stuff... so it should build for developers, for a start. And in a unified fashion (no special hacks to make things build, it just raises the barrier of entry).

Anyhow, what I want to complain about, is that it's actually impossible to build everything, at this point.
Too many dependencies on very-recent libs, or on libs that aren't packaged by distros (unless I'm missing something).

Here's my list of unsolved issues right now, on an OpenSUSE 12.3 system:
<ul>
<li>kolena needs tesseract library, not shipped by opensuse</li>
<li>libnm-qt (and therefore networkmanagement) needs NetworkManager 0.9.8.0, opensuse 12.3 only has 0.9.6.4</li>
<li>libkface (and therefore digikam) needs OpenCV >= 2.4.4 (Opensuse has 2.4.1)</li>
<li>amarok needs gmock >= 1.4, not shipped by opensuse</li>
<li>kdenlive needs MLT >= 0.9.0, opensuse has 0.8.8</li>
<li>apper needs packagekit-qt2 >= 0.8.8, opensuse has 0.7.4</li>
<li>kamoso needs FindQtGStreamer.cmake or QtGStreamerConfig.cmake, none of which is provided by libQtGStreamer-0_10-0 (no devel package)</li>
<li>kolor-manager needs FindOyranos.cmake or OyranosConfig.cmake, none of which is provided by liboyranos-devel</li>
<li>libqapt needs aptpkg, not shipped by opensuse</li>
<li>trojita uses qmake (kdesrc-build doesn't seem to support that)</li>
<li>qoauth tries to install into /usr, which isn't even where my Qt is</li>

<li>calligra defines its own iterators, which breaks -DQT_STRICT_ITERATORS</li>
<li>bodega-webapp-client - nothing to build</li>
<li>unittests in nepomuk-core hang, every time.</li>
<li>The kdesvn developer still didn't apply my patch from April (fixing compilation with strict iterators), I'm worried that it's not maintained anymore.</li>
<li>playground never builds, and yet there's KDE apps that depend on playground stuff! (hello bluedevil). Either people (and not just me) should ensure that playground builds, or dependencies of KDE SC software should be elsewhere).</li>
</ul>

I am not happy about things that require too recent libs. I'm using a rather recent distribution, it's 3 months old, and yet there's a lot of stuff I can't compile. NetworkManager in particular is really annoying - no way to build it, with 0.9.6.4. There could at least be a branch that builds with the NetworkManager that people are currently using!

But, to end on a positive note, that's 234-19=215 modules that build just fine :-)

