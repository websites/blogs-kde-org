---
title:   "No print preview available, due to \"libpoppler-qt4.so: undefined reference to qBadAlloc\""
date:    2010-03-31
authors:
  - dfaure
slug:    no-print-preview-available-due-libpoppler-qt4so-undefined-reference-qbadalloc
---
If print preview doesn't work (no viewer found for PDF files), then most likely okularpart (from kdegraphics) isn't installed. If this is because your kdegraphics can't find poppler (the test for HAVE_POPPLER_0_12_1 fails) even though you have it installed, look into CMakeFiles/CMakeError.log. If you see "undefined reference to qBadAlloc" while linking, then the problem is that /usr/lib/libpoppler-qt4.so was compiled against a Qt with exceptions enabled, and your own Qt (e.g. from kde-qt git) was compiled with -no-exceptions. The solution: recompile Qt with exceptions, or download and recompile libpoppler against your own Qt.
Putting this up here so that people googling for the error can find it :)