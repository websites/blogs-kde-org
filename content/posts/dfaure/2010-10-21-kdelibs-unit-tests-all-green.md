---
title:   "kdelibs unit tests: all green!"
date:    2010-10-21
authors:
  - dfaure
slug:    kdelibs-unit-tests-all-green
---
For the first time since end of May, 2010, the kdelibs unittests are all green again, in trunk:

100% tests passed, 0 tests failed out of 171
<!--break-->
Thanks to everyone involved in the last round of fixing (Aaron Seigo, Kévin Ottens, Bernhard Beschow, Sébastian Trueg and a few others)!

What does this mean? Well, it means two things:

1) if someone breaks something, I'll be able to find out very quickly and to shout at him/her :)
(well, or at myself, a case which happens often too...)

The point is that it's much more useful when we can associate regressions with a very small number of commits (ideally just one), because then the bugfixing is much faster, than when having to dig again in a 6-months-old change. This is where unittests are very powerful, compared to waiting for user bug reports after the next release, a year later...

2) if you still have a failing kdelibs unit-test on your machine, then it would be very helpful if you could investigate a bit and if you can't find out how to fix it, send me details about the failure. (Not just "footest fails"!). Ideally they should all pass on everyone's machine, not just on mine, so that you can all double-check your changes in kdelibs before committing, without having to wait for my whip...
Remember that kdelibs must be installed before doing "make test", and that you must be in a KDE session to run them, preferrably using the same version of kdelibs (well, yesterday's version is probably fine too, no need to logout).

Oh, and the most important piece of information: pass -DKDE4_BUILD_TESTS=TRUE to cmake! Always. As if your life depended on it!

You're going to say "but relinking the tests every time I change one line in a library is really slow", to which I answer: "mf kio".
"mf" stands for "make fast", and my mf script reads:
#!/bin/zsh
cd /kde/src /kde/build
make -j4 $*/fast && make install/fast

In other words: switch to build dir, make $target/fast, make install/fast. After a change in a kio file, this would recompile libkio, and install it, without relinking all the tests, like "make ; make install" would normally do.

And once you have a kdelibs compiled with tests enabled, you can even automate the running of the test so that you don't have to remember typing "make test". In your kdesrc-buildrc, add "run-tests true" in the kdelibs module. Magic!