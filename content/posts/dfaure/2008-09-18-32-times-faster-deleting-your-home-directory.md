---
title:   "32 times faster deleting your home directory"
date:    2008-09-18
authors:
  - dfaure
slug:    32-times-faster-deleting-your-home-directory
---
Yes, KDE-4.2 will be 32 times faster deleting a large number of files compared to KDE-4.1!

I was looking at the highest-voted bugs last Monday (yes, voting _is_ useful in bugzilla, some people weren't sure about that), and bug 43356 (260 votes) was about konqueror taking too much time deleting a large number of files (in kde-3.0, but still true in 4.1).

I first made the KIO job itself 25 times faster for local files, but then it turned out that the KIO job was anyway "only" taking 35 seconds, out of the 8 minutes that konqueror-4.1 took for deleting 5000 times. The other 7 minutes and a half were spent updating the itemview, removing rows one row at a time. Ooops.

After making KDirLister emit a new itemsDeleted() signal with all items, and KDirModel processing this in a more clever way, the view is now told about the 5000 removed rows in one call, and deleting 5000 files with konqueror-trunk (or dolphin, same code) now takes only 15 seconds. That's 32 times faster.

Conclusions: Bugzilla rocks (and users report useful issues, we as developers never test enough of these large-datasets cases). Qt model/view rocks (there was no way to optimize this that much in Qt3). But, there are too many bug reports for a lifetime of bugfixing, unfortunately.