---
title:   "Unlocking wallet during startup"
date:    2016-06-03
authors:
  - dfaure
slug:    unlocking-wallet-during-startup
---
While setting up plasma5 I found a solution for something that had been bothering me forever. Basically, while session management is restoring all windows, the wallet isn't open yet, so if the wallet is needed to get online (wifi password), all the apps being restored (in my case, about 20 konqueror windows) have no networking yet and just show error pages.
I had to be there during startup and type fast to unlock the wallet before the apps needed it.

One of the users of the wallet is ksshaskpass (which uses kwallet to give the ssh passphase to ssh-agent). By calling ssh-add in the  Autostart folder, it was just one more of the kwallet queries in the queue, waiting for the user while everything is being restored.

While setting up plasma5 I did it slightly differently: I put the file that calls ssh-add into <b>~/.config/plasma-workspace/env/</b>.
Because this is sourced by startkde, it actually <b>blocks session startup</b>, waiting for me to type the wallet password, and happens much before session management restore kicks in. This way, I can make sure to unlock the wallet first (which is useful for all wallet users afterwards, not just ksshaskpass). Very nice.
Too bad QtWebkit 5.6 crashes so much that konqueror is unusable... this idea comes 10 years too late :-)

This makes me wonder how distros set up ksshaskpass (more precisely where is the call to ssh-add). It also makes me wonder if we should have a more direct way of unlocking the wallet at startup (e.g. even for people without a ssh key). E.g. a mode where kwallet-query just opens the wallet and nothing else.