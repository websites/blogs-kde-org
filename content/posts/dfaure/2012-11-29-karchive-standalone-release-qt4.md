---
title:   "KArchive standalone release for Qt4"
date:    2012-11-29
authors:
  - dfaure
slug:    karchive-standalone-release-qt4
---
Due to popular request during my KDE Frameworks 5 presentation, in other words "where can I get KArchive today?", which is also a long-standing request from many developers of Qt applications, I experimented with making a Qt4-based release of the KArchive framework.

The Qt5-based karchive will be a lot thinner, thanks to much of the necessary functionality going into Qt5 (mimetypes, standard paths, safe saving...). For now, the Qt4 package has to come with all that stuff built in.

Here it is: http://www.davidfaure.fr/2012/karchive-qt4-1.0.0.zip

It features KCompressionDevice, a QIODevice that can compress/uncompress data on the fly, in GZIP or BZIP2 or XZ/LZMA format.

It also features support for "archive" formats: ZIP, TAR, AR, 7Zip, through a common API, KArchive.
