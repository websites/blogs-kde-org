---
title:   "svn.kde.org down temporarily "
date:    2008-10-10
authors:
  - dfaure
slug:    svnkdeorg-down-temporarily
---
Another few hours until it comes back, apparently.

You can read more details about it at
http://news.opensuse.org/2008/10/10/power-outage-in-area-where-most-opensuse-servers-are-located
