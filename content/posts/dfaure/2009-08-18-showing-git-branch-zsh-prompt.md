---
title:   "Showing the git branch in the zsh prompt"
date:    2009-08-18
authors:
  - dfaure
slug:    showing-git-branch-zsh-prompt
---
One big risk with git is forgetting which branch you're working in. Several people solve that by putting the git branch in their shell prompt. But often they do that by calling `git branch` and parsing its output, which is quite slow, especially in big repositories (we strace'd it, and it really opens and reads a lot of files).

At GCDS, together with Aurélien Gateau et Sébastien Renard, we came up with
<a href="http://web.davidfaure.fr/scripts/git_branch_in_zsh_prompt">this light-weight implementation (http://www.davidfaure.fr/scripts/git_branch_in_zsh_prompt)</a>.

Requires `findup`, which you can get from <a href="http://www.davidfaure.fr/scripts/findup">the same directory</a>
