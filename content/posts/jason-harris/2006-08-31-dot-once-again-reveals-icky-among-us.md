---
title:   "The Dot once again reveals the icky among us"
date:    2006-08-31
authors:
  - jason harris
slug:    dot-once-again-reveals-icky-among-us
---
Why is it that whenever a Dot story features one of our female contributors, people feel the need to embarass themselves (and the rest of us) with pathetic and demeaning comments?  Greg Meyer called it, it's really creepy, anti-social behavior.  

A few days ago, I was going to blog here about my recent engagement, with pictures...but you know, I thought about previous instances of this happening, and I didn't particularly want to share the photos with the members of our community who make such comments.  And that makes me sad:  :(

Anway, things are moving slowly in KStars-land.  Haven't been too motivated lately, and I haven't had much time anyway.  We should be getting earth satellites sometime soon.  I also was messing around a bit with QGraphicsView, but I am not sure if it will be worthwhile to extend that system to handle spherical projections.  I don't think it would be faster than the system we already have in place.  I should take another look at openGL at some point though.
