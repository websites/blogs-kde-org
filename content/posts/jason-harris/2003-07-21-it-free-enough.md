---
title:   "is it Free enough?"
date:    2003-07-21
authors:
  - jason harris
slug:    it-free-enough
---
We recently had a bit of <a href="http://lists.kde.org/?t=105637866600007&r=1&w=2">drama</a> on the kde-edu-devel mailing list.  It seems that Debian decided that KStars was not quite Free enough, because some of the images we use are used with permission, and the catalog files are not unambiguously in the public domain.  As a result, they were either going to remove KStars altogether, or move it to non-free.

Now, let's be clear.  There's absolutely no problem with KStars itself using these data.  Everything that isn't public domain is either "free for non-commercial use" or "used with permission".  The problem comes when you consider the hypothetical situation that some third party may want to redistribute data obtained from KStars (necessarily still under the GPL), and that the data's copyright holder would not approve of this third party's usage, and furthermore that they would hold either Debian or us responsible for the infringement (and not the hypothetical third-party infringer), despite the fact that we clearly state the copyright status of every bit of information used by the program.

The discussion centered on our use of the SAO star catalog.  The NASA website from which I originally downloaded the catalog explicitly states that all of its catalogs are in the public domain, but this was not good enough for the Debian guys because the NASA site is only a distributor of the catalog; they do not hold the copyright.  I tried to contact various parties to clarify the copyright status of the SAO catalog (and other catalogs), but got mostly dead ends.  Seems that scientists don't worry about these issues nearly as much as FOSS developers!  In the end, I <a href="http://sourceforge.net/mailarchive/message.php?msg_id=5377327">got a bit frustrated at the absurdity of the whole situation</a>.

I never was able to find a credible source to tell me exactly what the copyright status of the SAO catalog is.  However, I did finally get a straight answer about the Hipparcos star catalog.  It is definitely in the public domain, so KStars will be switching to that catalog in the near future.

The other fallout from this is that we have lost our beautiful moon images.  They were licensed with explicit permission, which I decided was really too restrictive, so I grabbed the only public-domain alternative I could find, which are sadly quite a bit worse.  Hence my <a href="http://edu.kde.org/kstars">call</a> for Free moon images.