---
title:   "OS X: usability Xanadu?"
date:    2004-05-17
authors:
  - jason harris
slug:    os-x-usability-xanadu
---
Ok, so I got a new PowerBook recently.  I was really excited about OS X from the little I'd seen of it looking over people's shoulders.  Finally, I would taste humankind's highest achievement in usability and user-friendliness!
<br>
<br>

After using it for about a month, here is my four-word review of OS X: "it sure is shiny!"
<br>
<br>

The things it does well, it does extremely well.  expose is really cool.  I like the slick animated desktop fluff.  I *really* like the brainless network detection and activation, including the automatic appearance of my co-workers' music collections in iTunes :), and the automatic detection of the network's printers.  Safari is nice (but it does seem like Konqueror's beautiful-but-vacuous cousin).
<br>
<br>

But, call me a fanboy, call me a zealot, call me a hopeless troglodyte.  I find KDE 3.2 much more usable than OS X.  I don't understand why the prevailing internet opinion seems to be "KDE (and Gnome) are Ok, but be serious: next to OS X, they are hacked-up kludge-jobs."  Well, I disagree.  I am frustrated so much more often on OS X than on KDE.
<br>
<br>

Yes, this is at least partly because I have much more experience with KDE.  Yes, it is at least partly because as a KDE app developer and community member, I am biased.  But consider:
<br>
<br>


<ul>
<li> Where is the text editor for OS X?  There's some app called "TextEdit", but as far as I can tell, it should be called "SimpleViewer, and oh yeah, you may be able to modify the file contents as well".  I mean, I tried opening an HTML file in it, and it showed me (poorly) *rendered* html!  How is this helpful in a text editor?  I was able to view the file normally by opening the Preferences window and checking the "ignore rich text commands in HTML files", but even in this case, the tags weren't highlighted.  Features-wise, it's equivalent to pico, AFAICT.  I felt completely hogtied using it.

<li> No pager by default (got one at wsmanager.sf.net).  No themeing by default (paid $20 for "ShapeShifter" -- worth every dime to escape from brushed-metal hell).  iTunes: no ogg support by default (got a third-party quicktime plugin).  i18n: 15 languages to our 40+.

<li> The spatial finder.  Ok, it's not as spatial as it used to be (thank God!), but the vestiges of it are still annoying to me.  I cannot understand why people like the spatial metaphor for a file manager.  KFM blows finder completely out of the water.  ioslaves, view-splitting, preview tooltips, servicemenus, it just rocks.
</ul>


Anyway, that's just my thoughts on some things that KDE does way better than OS X, IMHO.  Don't get me wrong, I really like my laptop, and I'm glad I got it.  I am sure that I'll get used to a lot of these things, but I doubt it will ever feel like "home" the way KDE does.  I did grab the native port of KDE apps to OS X by the Bens (kde.opendarwin.org); this looks really promising, but it's not quite there yet.  Several apps freeze up completely on startup, or crash during use.  And for some reason, my version doesn't have SSL, so I can't use fish:/.  I will eventually compile it from source and submit bugs (and-- dare I say?-- patches).  Even the OS X version of venerable emacs has problems.  I can't figure out how to paste text from the OS X "clipboard" into my emacs window (it's still better than "TextEdit" though).
<br>
<br>

On a totally unrelated note, I just finished adding extended-date support to KStars!  The date is no longer limited to QDate's narrow range of years (1752-8000).  Actually, the bare functionality has been in for a while, but I just fully integrated it all this weekend.  I have about 8 other things I want to get in before the feature freeze.  Seems like 3.3 totally snuck up on me; I had no idea it was happening this summer!  I thought our progress was looking pretty sparse, but then I actually listed it all at the 3.3 feature plan, and it looks pretty good.
<br>
<br>

Oh yeah, I registered at QtForum.org to subit KStars to their contest a few weeks ago, but have heard nothing from them.  Has anyone successfully entered that contest yet?
<br>
<br>

I've been monitoring the huge thread on core-devel regarding the list of localities in the "Country/Region" KCM...lots of sound and fury.  Funny how simple things can be so complicated.  Reminded me of a BR we got a while ago, because we named to the country of Lhasa as "Tibet", not "China".  Fair enough, that's a bug because there is no country named "Tibet".  Fixed.  The whole "FYROM" vs. "Macedonia" thing is much more complicated (as The Dude would say, "lotta ins, lotta outs"), but I think the resolution that's emerging is a good one.  We can't let users with axen to grind politicize KDE.
<br>
<br>

Oh well.  It's sick.  I'm late.  Wait, scratch that.  Reverse it.  
Anyway, LMCBoy out.
<br>
<br>
<b>[Edit]</b>: either I'm an idiot, or it's really late (possibly both).  Why do I need to insert two br-tags to get a paragraph break?  empty lines worked for my previous entries, IIRC...