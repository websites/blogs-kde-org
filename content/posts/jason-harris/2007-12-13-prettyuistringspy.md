---
title:   "prettyuistrings.py"
date:    2007-12-13
authors:
  - jason harris
slug:    prettyuistringspy
---
I posted this to k-c-d a couple days ago, but it generated zero response, so I thought I'd post it to the ol' blog in case any non k-c-d types might be interested.

Qt designer is, shall we say, quite verbose when it comes to encoding rich text.  For example, if you put a couple of words in <b>bold</b> in a text field, this is what you get in your UI file:
<code>
<html><head><meta name="qrichtext" 
content="1" /></head><body style=" white-space: 
pre-wrap; font-family:Sans Serif; font-weight:400; font-style:normal; 
text-decoration:none;"><p style=" margin-top:12px; 
margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; 
text-indent:0px;">You may now download optional data files to enhance 
KStars, such as Messier object images, or a more complete NGC/IC catalog. 
Press the <span style=" font-weight:600;">Download Extra 
Data</span> button to proceed. </p><p style=" margin-top:12px; 
margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; 
text-indent:0px;">You can also use this tool later, by selecting 
<span style=" font-weight:600;">Download data</span> from the 
<span style=" font-weight:600;">File</span> 
menu.</p></body></html> 
</code>
<p>
My goodness.  Needless to say, that's not exactly easy for the translators to deal with, as Luciano Montanaro recently pointed out to me.  As far as either of us knows, the remedy for this is currently to manually edit the ui files by hand.  But I decided to try to automate it a bit, and came up with a python script that will detect all HTML blocks in your UI files and clean them up.  For example, the above block looks like this after my script gets done with it:
<code>
<html><head></head><body><p>You may now download optional data files to 
enhance KStars, such as Messier object images, or a more complete NGC/IC 
catalog. Press the <span style=" font-weight:600;">Download Extra Data</span> 
button to proceed. </p><p>You can also use this tool later, by selecting 
<span style=" font-weight:600;">Download data</span> from the <span style=" 
font-weight:600;">File</span> menu.</p></body></html> 
</code>
<p>
Better, no?

My idea was to try to get this added to kdesdk, so it can be run on UI files along with fixuifiles (or maybe the functionality in my script can simply be merged into fixuifiles).  As written, it might be too fragile for use without human eyeballs checking the result...but maybe it's a good start.

Here is the script:
<pre>
#!/usr/bin/env python
#
# prettyuistrings: If you use rich text in Qt designer, the
# html generated is difficult to read and filled with unneeded cruft.
# This script will simplify the html in all of your ui files.
#
# Just run "./prettyuifiles.py" in your topmost source directory, and it
# will recursively find all your *.ui files, and modify those which
# contain HTML.
#
# Warning: This code has only been lightly tested ("works for me"), and
# may not be bulletproof.
#
# Author: Jason Harris &lt;kstars@30doradus.org&gt;

import os, string, fnmatch

def main():
    for root, dirs, files in os.walk('.'):
        files = fnmatch.filter( files, "*.ui" )
        for f in files:
            fname = os.path.join( root, f )

            f = open( fname )
            sfile = f.read()
            f.close()
        
            #Skip UI files that don't contain HTML
            if sfile.find( "html&gt;" ) &lt;0:
                print fname+": Ok"
                continue
        
            #isolate the text inside each &lt;string&gt;&lt;/string&gt; tag
            #replace the text with a "pretty" version
            istring = sfile.find( "&lt;string&gt;" )
            iend = sfile.find( "&lt;/string&gt;", istring )
            while istring &gt;=0:
                
                sfile = sfile[:istring+8] + pretty( sfile[istring+8:iend] ) + sfile[iend:]
        
                istring = sfile.find( "&lt;string&gt;", istring+1 )
                iend = sfile.find( "&lt;/string&gt;", istring )
        

            f = open( fname, 'w' )
            f.write( sfile )
            f.close()

            print fname+": Made pretty"


                
def pretty( s ):
    s = s.replace( "&amp;lt;", "&lt;" )
    s = s.replace( "&amp;quot;", "\"" )

    if s.find( "&lt;html&gt;" ) &lt; 0:
        return s

    r = "&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;"

    #Find each &lt;p&gt; tag
    ip = s.find( "&lt;p" )
    istart = s.find( "&gt;", ip ) + 1
    iend = s.find( "&lt;/p&gt;", ip )
    while ip &gt;= 0:
        pstring = s[istart:iend]

        r = r + "&lt;p&gt;" + pstring + "&lt;/p&gt;"

        ip = s.find( "&lt;p", ip + 1 )
        istart = s.find( "&gt;", ip ) + 1
        iend = s.find( "&lt;/p&gt;", ip )

    r = r + "&lt;/body&gt;&lt;/html&gt;"

    return r

main()
</pre>
