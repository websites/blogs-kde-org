---
title:   "We need contributors"
date:    2008-06-27
authors:
  - jason harris
slug:    we-need-contributors
---
Have to say, I totally agree with the sentiments <a href="http://troy-at-kde.livejournal.com/17753.html">expressed</a> by Troy.  KDE, like many other open-source projects, doesn't really need users at all, whether they are poisonous or not.  What we need are <b>contributors</b>: that's the life-blood of our community, what keeps KDE growing and evolving.  To the extent that users can and do become contributors, I will grant that we need a userbase as a pool of potential future contributors.  But I am simply baffled by any argument that we "need" to have a large number of people that never do more than use KDE.  Why do we need them?

I don't really understand Rafael's <a href="http://www.ereslibre.es/?p=107">argument</a>, because all of the people he describes in his post are highly valuable <b>contributors</b> to the KDE project.  Of course we need such contributors; we all agree on that.  When I say we don't need users, I am talking <i>by definition</i> about people who do not contribute to KDE: they do not report bugs, they do not write documentation, they do not translate, they do not promote...they simply use our software.

Now, I have nothing against users, although this post probably sounds pretty harsh.  Other than the vocal poisonous minority gnashing their teeth on the dot and elsewhere, they do us no harm.  I'm happy to see lots of people using our software, and I hope they find it useful and enjoyable.  But in terms of what we *need* for the continued vitality of the project?  

We need <b>contributors</b>, not users.  We need contributors like Microsoft and Apple need customers.


