---
title:   "greetings from Chile"
date:    2003-08-08
authors:
  - jason harris
slug:    greetings-chile
---
I am on the last night of a 4-night observing run at the <a href="http://www.lco.cl/lco/magellan/">Magellan 6.5-m telescope</a>, and KStars has been my lovely-and-talented observing assistant for the whole run.

It's great to be on a mountain again; prior to this, I hadn't been observing at all in 3 years.  The weather's been great, the telescope is incredible, and the winter nights are long down here, so I'm getting lots of good data.

On the long flight back tomorrow (if I don't sleep the whole way), I hope to finish a new feature for KStars I've been working on:  command-line image-dump mode!  By starting kstars with command line arguments (kstars --dump --width=800 --height=600 --filename="foo.png"), it will generate an image based on the settings in your kstarsrc without opening a GUI at all.  

This feature is to answer many requests we've had for running kstars dynamically as the desktop background.  You can just run a cron job that will generate an image the size of your desktop, and set the background through DCOP.  I've actually heard that kcontrol is capable of setting up a program to periodically generate the background image, but I haven't ever seen such a thing.  Anyone know more about that?

Of course, running kstars this way every N minutes might not be very efficient, because it has to initialize itself first, which will hog the CPU for maybe 5-10 seconds.  The alternative is to have a mode where kstars lies loaded-but-dormant in the system tray, ready to serve up images on a moment's notice.  The trade-off is that the idling kstars is using ~50 MB of your precious RAM.  Anyway, where can I learn about adding a program to the system tray?

Ok, now for my obligatory request:  how about a KDE-Edu "Project" category?!  I'm tired of filing under boring old "KDE General".  What do you say, Ian?  There's at least 4 of us Edu-ers signed up here...