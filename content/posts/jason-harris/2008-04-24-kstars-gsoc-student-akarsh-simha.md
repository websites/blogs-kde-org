---
title:   "KStars GSoC student: Akarsh Simha!"
date:    2008-04-24
authors:
  - jason harris
slug:    kstars-gsoc-student-akarsh-simha
---
I'd like to introduce Akarsh Simha, who was awarded a GSoC this year to work on KStars.  His project is entitled "Optimising loading and painting of stars in KStars", and when it is successfully completed, KStars will finally have the ability to display millions of stars, without adversely affecting the programs interactive responsiveness.

Akarsh is relatively new to the KDE community, but he is already an active and valued contributor to KStars.  I first heard from him in an email, subject line: "Contributing to KStars".  In it he explained that he is a physics student in Bangalore who was inspired by KStars to become a "hardcore amateur astronomer", and that he is eager to contribute to KStars development.  Well, an email like that is like music to a KDE developer's ears!  Since then, Akarsh has found several bugs and has started submitting code patches.  This culminated in his recent contribution of a new Conjunction Predictor tool, which I committed just in time for the 4.1 feature freeze!

So please welcome Akarsh to our community!

