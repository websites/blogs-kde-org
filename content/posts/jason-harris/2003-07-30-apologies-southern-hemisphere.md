---
title:   "Apologies to the southern hemisphere"
date:    2003-07-30
authors:
  - jason harris
slug:    apologies-southern-hemisphere
---
I recently noticed that handling of "Daylight Savings Time" (a.k.a. "Summer Time") did not work for any southern-hemisphere locations in KStars.  I fixed it in HEAD and 3_1_BRANCH yesterday, a bit too late for 3.1.3, unfortunately (it will be in version M_PI if there is one ;) )

Anyway, the problem was a badly-nested if-block in the function TimeZoneRule::isDSTActive(QDateTime), which determines if the given date should be using DST.  The offending code was:
<pre>
  if ( StartMonth &lt; RevertMonth ) 
    if ( month &lt; StartMonth || month &gt; RevertMonth ) return false;
  else 
    if ( month &lt; StartMonth &amp;&amp; month &gt; RevertMonth ) return false;
</pre>

which I changed to:
<pre>
  if ( StartMonth &lt; RevertMonth ) {
    if ( month &lt; StartMonth || month &gt; RevertMonth ) return false;
  } else {
    if ( month &lt; StartMonth &amp;&amp; month &gt; RevertMonth ) return false;
  }
</pre>

Whoops.  Any programmer worth their salt would not have made such a mistake; here, take the salt back, I don't deserve it!  

Anyway, moral of the story?  Qt/KDE is so easy to use, that even mediocre programmers like me can still be successful at application programming, but for Pete's sake, keep me away from kdelibs!  ;)
<p>
(BTW, the reason the above code broke southern hemisphere locations is that for them, DST starts in months like October (10) and ends the following year in months like April (4))