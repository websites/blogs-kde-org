---
title:   "Seeking help for bug #65429"
date:    2003-10-27
authors:
  - jason harris
slug:    seeking-help-bug-65429
---
The skinny:  two people <a href="http://bugs.kde.org/show_bug.cgi?id=65429">have reported</a> that KStars will not compile for them; both are using Redhat 9 and gcc 3.2.2.  The error message is:

<tt>lx200classic.cpp:29: structure `eqNum' with uninitialized const members</tt>

eqNum is a struct with const members, which *are* initialized in lx200generic.cpp.  In lx200classic.cpp, we reference it with 'extern INumberVectorProperty eqNum;'.  Obviously none of us on the KStars team have had a problem compiling the code, and Stephan Binner commented at BKO that it works for him as well.  So it seems to be a weird compiler-specific issue.

Beta1 is almost upon us; I'd hate to have even a rare compiler issue in there.  First off, does anyone see immediately a better way to handle this struct?  Secondly, if you have recently compiled kdeedu from HEAD, can you post your results, or email them to me (kstars AT 30doradus DOT org)?  We are especially interested if you are seeing this error, OR if you do *not* get an error using RH9 and gcc 3.2.2.  Even "works here" messages are good, though :)

Thanks!