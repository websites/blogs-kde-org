---
title:   "On the cusp of a phase transition"
date:    2008-09-19
authors:
  - jason harris
slug:    cusp-phase-transition
---
This is a big day for me: after 15 years, my last day as an astronomer.
<!--break-->
After three years of trying, I wasn't able to get a faculty job, so this year I started applying for non-academic positions.  Those applications were basically all dead ends, which has been pretty frustrating.

Then, just a few weeks ago, I got an email that an ex-astronomer now working in biotech was looking for someone with an astronomer's sensibilities for imaging data analysis.  So, I emailed him.  Two days later, we had a phone interview; two days after that I flew out for an on-site interview; two days after that they offered me the job.  It's all happened head-spinningly fast, but my wife and I are very excited about this new chapter in our lives, and especially about getting to live in the SF bay area!

So, here I sit among the boxes of stuff in my house waiting to be loaded onto a truck.  Moving is so interesting.  I find that my brain is unable to grok the concept that I'm really moving until a few days after I've arrived.  In fact, I prefer to drive to my new location, because being on the road provides a great sense of "betweenness" to ease the transition.  But, with the baby and the cat, a road trip to CA is not very practical, so we're going by air tomorrow.

Not sure what this means for KStars in the long run.  In the short run, it's meant almost total stagnation from me over the past few weeks.  Hopefully, once we get settled in to our new life, I'll be able to get back into some development.  Until then, I leave you in the capable hands of my fellow developers!