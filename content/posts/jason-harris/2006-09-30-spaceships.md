---
title:   "Spaceships!"
date:    2006-09-30
authors:
  - jason harris
slug:    spaceships
---
Something like 9 years ago, when I was a young grad student, I created a web page that was, at the time, the <a href="http://web.archive.org/web/19980124183538/www.ucolick.org/~jharris/starships.html">internet's only repository of spaceship images</a> (thanks wayback machine!).  I enjoyed putting it together, but inevitably, I eventually let it stagnate.
<p>
A funny thing happened, though.  Even after years of neglecting this little page, I still got a steady stream of positive feedback.  Total strangers were <i>thanking</i> me for putting together the page.  After I moved to Baltimore and got my <a href="http://www.30doradus.org">own domain</a>, I reconstructed the page and named it the Spaceship Images Archive, but didn't really expand or improve it much.  Still the comments and page hits kept coming.<!--break-->  
<p>
A few years ago, I finally <a href="http://www.30doradus.org/spaceships/index_old.html">brought the SIA into the new millenium</a> with things like CSS, but still the actual content remained vintage 1997 bits.  And there it sat...for almost a decade.
<p>
And to this day, my apache logs show a near-<b>constant</b> stream of people checking out my spaceship images.  I can literally watch people browsing in real time with "tail -f /var/log/apache/...".  My site is the first listed when you <a href="http://www.google.com/search?q=spaceship+images&ie=UTF-8&oe=UTF-8">google for 'spaceship images'</a>.  
<p>
I always felt kind of bad about neglecting the SIA, since it's apparently so popular.  So I've finally done something about it.  Today I <a href="http://spaceships.30doradus.org">relaunched the SIA again</a>.  This time it's based on <a href="http://gallery.menalto.com/">Gallery 2</a>, which makes browsing the images much nicer, and allows people attach comments to images.  Best of all, I finally expanded the collection by adding screengrabs from several DVDs that I own.  These are much higher quality than most of the existing collection.
<p>
Anyway, I kind of like this story of my little website, whose popularity I've never quite understood, but am constantly gratified by.  Thank you, fellow sci-fi geeks everywhere...the SIA is for you!
