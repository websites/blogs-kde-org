---
title:   "My First KDE event"
date:    2008-01-20
authors:
  - jason harris
slug:    my-first-kde-event
---
Attending the KDE4 Release Event has been insanely great.  It's so good to finally meet people I've only known via email and IRC.  We have an incredible community, and now I feel much closer to it than I did before.

Some random bits:
<!--break-->

<ul>
<li> Aaron is a very gifted speaker.  And Adriaan looks exactly like Alton Brown.  (BTW, Adriaan, here's a vid of <a href="http://video.google.com/videoplay?docid=5912487412723519389">your alter ego</a>).

<li> I should rename KStars to Demo.  I heard from many people that they always show KStars when they want to show off KDE to someone :).  That's so gratifying.

<li> I gave a "KStars Demo How-to" breakout session today, which went pretty well.  I'd like to make short tutorial screencasts showing how to do specific demos in KStars, to assist people who want to show it at meetings, or for teachers to show it to their class.

<li> The Google people helping us run the meeting are really helpful.  Thanks Google!  

<li> I had to miss the Amarok2 BoF to take the Google tour...Sorry Leo.

<li> Benjamin Reed's wife Cynthia is the sister of a good friend with whom I went to grad school at UC Santa Cruz.  

<li> I got some good ideas for rebuilding the KStars community, which seems to be pretty well disintegrated these days (at least in terms of new contributors and people taking interest on the mailing list or <a href="http://kstars.30doradus.org">message boards</a>).  Hopefully, we'll benefit from the KDE4 release and things like my screencast tutorials.  I have to say, I've been pretty discouraged about KStars these past several months, due to having very little time for it, and feeling really isolated.  This meeting has been *so* good for me in that regard.

<li> I've gotten some great ideas from people on new features: simulated meteors, marking the Galactic center, allowing Galactic coordinates view.

<li> One guy was really persistently asking me why I don't "just" merge KStars with Celestia.  I couldn't convince him that the amount of effort required wouldn't be worth the alleged benefits of having a single program that does both the night sky and solar system exploration.  In the end I had to resort to "I just don't want to do it, if you want to do it, go ahead."  I wish I didn't have to reach that point, but there was simply no convincing the guy that it was not worth my (very limited) time.

<li> Anyway, I'm happy to have finally attended a KDE event!  This is a great community; I'm taking a suitcase full of inspiration home with me which should last until the next event :)  Thanks everyone!

</ul>

