---
title:   "My nethack monster"
date:    2006-01-26
authors:
  - jason harris
slug:    my-nethack-monster
---
If I was a nethack monster, I would be:
<pre>
......|
..%d+.|
......|
</pre>

a little dog:  Loyal.  Friendly.  Housebroken. <br>
Nice doggie.
<p>
<a href="http://kevan.org/nethack">Which NetHack Monster Are You?</a>
