---
title:   "Custom Catalogs in KStars"
date:    2005-06-04
authors:
  - jason harris
slug:    custom-catalogs-kstars
---
For a long time, KStars had a non-functional "Add Catalog" button in its configuration window.  Now, it finally works:

<img class="showonplanet" alt="addcatalog screen" src="http://www.30doradus.org/kde/addcat.png">

<!--break-->

You can specify what data fields are present in the catalog, and in what order.  The pair of listboxes constitute the first use of drag-n-drop in KStars. :)  Custom catalogs are easy to add to the program; hopefully we'll see some catalogs on GHNS and kde-files.org.

This was the third major feature I worked on since 3.4 (the other two being the new Details Dialog and the Observing List tool).  I want to polish these up a bit, but after that I am basically ready to start thinking about the KDE4 port.  It's not going to be trivial.

One complication is that others on the KStars team want more time to work on their in-progress features before we start a divergent kde4 branch.  We had planned on just ignoring the kde4 branch (soon to be trunk) until we were ready.  However, other devs are already making changes to kstars in kde4, so delaying the port in order to keep the kde4 and kde3.5 branches in sync may be futile.

The technical challenges of the port seem pretty daunting, though I've heard it's not that big a deal.  KStars is riddled with QPtrLists, so that's a bit troubling.  Also, our ExtDateTime classes present a special challenge; we basically rewrote the QDateTime classes, allowing them to extend beyond the bounds of the Gregorian calendar.  In Qt4, the Gregorian date limits are still in place, so we'll have to rewrite the classes again.  Or maybe we can try to derive ExtDateTime from QDateTime to reduce the amount of code.  Not sure about that.
