---
title:   "At last!  Focus follows mind"
date:    2006-03-14
authors:
  - jason harris
slug:    last-focus-follows-mind
---
Ever since <a href="http://people.kde.nl/ettrich.html">Matthias Ettrich</a> joked about it in the early history of the KDE project, <b>focus-follows-mind</b> 
<a href="http://webcvs.kde.org/kdebase/kicker/taskmanager/taskmanager.cpp?rev=1.64&view=markup">has</a> <a href="http://dot.kde.org/1057763789/1057798976/1057810737/1057824240/">been</a> a <a href="http://people.kde.nl/neundorf.html">recurring</a> <a href="http://people.kde.nl/kevin.html">meme</a> <a href="http://dot.kde.org/1009182852/1009324525/">in</a> the <a href="http://www.osnews.com/story.php?news_id=2997&page=5">KDE</a> <a href="http://dot.kde.org/973661320/973671842/973674214/">lexicon</a>.  Well, it looks like it's <a href="http://wiredblogs.tripod.com/gadgets/index.blog?entry_id=1435406">finally coming</a>!

Do you think the kernel will ship "thinking cap" drivers by the time we release KDE-4.0? :)
<!--break-->