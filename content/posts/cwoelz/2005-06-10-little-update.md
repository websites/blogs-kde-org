---
title:   "A little update"
date:    2005-06-10
authors:
  - cwoelz
slug:    little-update
---
I haven't blogged for a while...

I had the pleasure of having Andras Mantia with me here in Brazil. I showed Rio de Janeiro to him, and Buzios, a very nice beach place, before he went to a free software conference in Porto Alegre. I bugged him about making Quanta better for DocBook editing, and he fixed some entity quirks, but other than that, it was leisure time.

I have been working a bit on KOrganizer "quality" recently. The "general status" of the app is not bad at all: the docs are outdated, but not terribly so, the whatsthis is almost complete, (also thanks to the work of Antonio Salazar, the Kontact docs maintainer), there is work in the usability front by the openusability guys. What is really missing is more documentation about using the different resources, as there is currently none. KOrganizer is a very capable application, but I am not sure people know how to use its power, or how to configure the resources.

While working on that, I noticed that it is hard to configure Kontact for groupware use by hand, (without the use of the wizard), as groupware config options are spread betwen KOrg main configuration, (KOrg and KAddressBook) groupware resource configuration, KMail main configuration, and KAddressBook main configuration: five places to touch! To make it worse, there is not a lot of info on the web on how to configure all this stuff. The solution is trust wizards or create a centralized place for Kontact groupware configuration.

<!--break-->

