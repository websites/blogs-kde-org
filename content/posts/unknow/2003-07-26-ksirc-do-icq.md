---
title:   "kSirc to do ICQ?"
date:    2003-07-26
authors:
  - unknow
slug:    ksirc-do-icq
---
Hmm, I'm not sure how twisted this is, but I want to do something other than hack irc for a few days.  At work everyone is on ICQ, so I run sim to talk to them.  But I've never liked the way IM apps run, what was wrong with IRC?!?

Since ksirc really just executes a perl script why can't I just make it execute a different perl script?  Say one that talks ICQ?  The only thing the ICQ client has to do is accept 3 different commands:

<UL>
<LI>/msg <nick> <message>
<LI>/lag
<LI>/join <nick/channel>
</UL>

For ICQ we can ignore the last 2 and make them noops.  Now the ICQ client has to output data in a usefull way:

<UL>
<LI>~nick~<nick> message
<LI>*I* server message
<LI>*** generic message
<LI>*E* error message
</UL>

Where each *?* message can be prifixed with ~nick~ if it relates to a specific person.

This isn't much at all really.  Would be nice if all commands too /command format, like /help, etc but we can just modifiy the input parser, maybe?

Every chat session with a user is just an irc query window.  Would seem to work...

Now, since ksirc uses perl a perl ICQ client without tons of ascii colour codes is what I want.  I found vicq, anyone know any better ones?  Clean code, easy to hack and modify.  Suggestions?

Pretty twisted huh?