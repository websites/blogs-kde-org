---
title:   "Kopete Jabber Plugin - Do you use it?"
date:    2003-07-21
authors:
  - unknow
slug:    kopete-jabber-plugin-do-you-use-it
---
Jabber seems to be growing again lately. Yet, Kopete seems to have no real userbase in Jabberland. I always wonder why I don't get any real feedback. There are hardly any bug reports, hardly any wishlist entries. Compared to the other plugins like ICQ or MSN at least, although that might be an unfair comparison.

Would you use the Kopete Jabber plugin? Do you know about Jabber at all? If you are using the Jabber plugin, are you missing any features?<!--break-->

There are things like multi-user chat that I don't use at all, yet they are on my todo list. It would be cool to have multi-user chat support, Kopete can rudimentarily handle groupchat, but since there's no feedback (and thus seemingly no demand), it's hard to motivate yourself to finish it properly.

There are many more things like presence signing, standards-compliant encryption and others that I maybe don't even know about, it's cool to add more stuff that makes Kopete interact better with other clients, but then again, if nobody seems to use it, it sometimes feels like a wasted effort.