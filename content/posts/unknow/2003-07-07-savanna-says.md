---
title:   "Savanna Says:"
date:    2003-07-07
authors:
  - unknow
slug:    savanna-says
---
Nothing.

Savanna isn't saying anything right now. She's just marking her space in the cyber arena of the KDE Blogosphere.

Maybe her silence will last, or maybe not. Only time will tell.