---
title:   "Jabber Blog Bot Finished"
date:    2003-08-07
authors:
  - unknow
slug:    jabber-blog-bot-finished
---
I created a Jabber blog bot today which you can use to post diary entries to this blog via your favorite Jabber client.

It's a PHP script and it actually seems to work, this message here has been posted using the bot. I'll try to leave it running overnight and if it behaves, I'll look for a server that will be able to run the script 24/7 so everybody can use it.<!--break-->