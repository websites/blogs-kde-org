---
title:   "Freedows and me and the story of my life in the past couple of years"
date:    2004-10-05
authors:
  - unknow
slug:    freedows-and-me-and-story-my-life-past-couple-years
---
I've got a new job (actually I've had this job for about 4-5 months). In telling where I work now, I started to realise why I've been away from <a href="http://www.kde.org">KDE</a> development in the past two years.

I used to work for a greatly underrated distribution called <a href="http://www.conectiva.com">Conectiva</a>. I had the time of my life there (although I've also met hell sometimes). Anyways, in late 2002, Conectiva signed a deal with the Brazilian Army for the development of some software. I was the lead developer and gladly welcomed the opportunity to get to know the Military Sector (something like the Pentagon is for the US) in Brasilia, our capital. I had done something for the navy and learned a lot there, so the army seemed to be cool. Silly me.

As it turned out, the commercial deal was a little obscure and ambiguous, making a project that should last 3-4 months take the whole of 2003. As lead developer, I got really envolved in the project and visited Brasilia so many times that I actually can't stand it anymore. I now <b>hate</b> Brasilia :-)

Anyways, this took the rest of time I still might have in 2002. Well, by late Decembre 2003, the project was technically over and all that remained was a fierce battle between Conectiva's managenment and the Army representatives regarding some details of cost and stuff. As I was virtually free from the project, I was given my first vacation since time_t was 0. Two weeks after I got back to work, Conectiva layed off 70% of its employees. According to some of the partners, they had to get back to being a small company to survive. I was layed off. Me and a couple of friends from the dev team that happened to be in the same bad situation: we were not actively working for any important customer at that moment. Bad timing... humpf.

Two weeks into my 30-days notice, I got a rang from former Conectiva president and founder, Sandro Henrique. We talked and all of a sudden I had a new job.

The interesting part is that at the time I had only a rough idea about what I was going to work on. I had to sign a NDA because I was going to be working on a secret project for the largest bank in Brazil. Part of the project was to become a new linux distribution called <a href="http://www.freedows.com">Freedows</a>. Freedows, as coolo eloquently put it once, sounds like dog food, but is in fact a linux distribution with a specific goal: to be as much like Microsoft Windows as possible. Yes, I work for a Windows look-alike. Not particularly proud of it, but it sure pays well and I can use the money. Of course, Freedows is about <b>more</b> than just <b>looking</b> like Windows. It's about behaviour.

And I must mention that even though all the development team considers that KDE is better and easier to work with, the former project leader decided that we should use - oh my! I'm about to say it - <a href="http://www.gnome.org">GNOME</a>. Yep, that's it. I've said it. I work for a GNOME-based distro.

But in fact I have fun, because I do not get to work on the GUI most of the time and I do a lot of coding. I usually works on backends and infrastructure coding, so I have a lot of fun. Better than the mix of coding/managing job I had at Conectiva (no hard feelings, I still use Conectiva - even at work!)

The cool part of all this is that working with GNOME gave me more motivation to actually work on KDE again. In the past couple of weeks I have been playing with code, testing the new stuff that got done in the past two years. I'm even bringing <a href="http://knode.sf.net">KNode</a> into the 21st Century. It's fun to be sort of back :-)