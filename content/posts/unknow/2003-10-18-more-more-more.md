---
title:   "more... More... MORE!!!"
date:    2003-10-18
authors:
  - unknow
slug:    more-more-more
---
I know, that I should study geography / history these days (and will, too), but instead of doing that I am _again_ working on kmameleon... This week there has been some drastic changes in the (core) of the kmameleon proggy, so basically this is what is new:

 * bug fixes left from the 0.5 release
 * Frontend color change (with all the tools and the debug output included)
 * Relayed out 4 configuration plugins (basically it was a design flaw in this so it stetched much more than it should)
 * New configuration module: It's for the either for more advanced users, or if kmameleon is missing a switch you can use this... Still have one tiny thing to add, and will be complete :)

It's getting very crispy there in the KDE CVS, and it's currently tagged with version 0.9, the next major release will be 1.0 (but rather 1.0.1, as I am not fond fo that round numbers...)

Timeline? At least 2 more months, but if someone wishes to contribute, it would be much faster (about 1 month) :)

Oh -- And there is a very interesting shell script (my first, have to brag) in the nonbeta/kmameleon/main/layouts/convert.sh , that will convert all the .ui files in the directory into .h/.cc pair

'nuff for now...