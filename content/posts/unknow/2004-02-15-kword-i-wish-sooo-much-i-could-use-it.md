---
title:   "Kword - I wish sooo much i could use it"
date:    2004-02-15
authors:
  - unknow
slug:    kword-i-wish-sooo-much-i-could-use-it
---
Once in a while, i need to write a formatted text document for printing, for example a letter, a job application or, like to day, a table for checking the boats in my local sailing club.
<br><br>Every time, I optimistically launch KWord, because it is fast and lightweight, and have the features I need -- <b><i>would it just work</i></b>...
<br><br>The table I had to write today a little bit complex, it has some rows with 3 columns, some with 2 and some with one. So I created a table with 3 columns and borders, and started to join the columns in the rows where I needed it, and here the problem starts.
<br><br>All the rows with joined columns looses the right border, and there seems to be NO way to get that back, which alone makes the document useless, I can't provide a nice looking document this way.
<br><br>After adding all the rows I wanted, saving screws up the formatting COMPLETELY. Why does kwork reformat the document in the event of saving?? Autosave will cause kword to reformat my document at what feels like random times while working, and I could find *no* way of turning autosave of, setting the interval to the maximal 60 mins also seemed to do nothing.
<br><br>So, I can change a few things and then save, after that i have to
<ol>
<li>Close KWord, because of the reformatting
<li>Kill any left processes
<li>Reopen the document
</ol>
<p>My next experiment will be to create a new document and use a table with only one column for a start -- if it is my lucky day, maybe splitting the rows will leave me with visible borders...
<br><br>In a positive spirit: Is there <i>anything</i> I can do to help with KWord development? Please let me know, I believe that would just the key parts of koffice work, we would have <i>such</i> a nice desktop.