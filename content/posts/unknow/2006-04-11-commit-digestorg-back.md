---
title:   "commit-digest.org is back!"
date:    2006-04-11
authors:
  - unknow
slug:    commit-digestorg-back
---
The KDE Commit-Digest, written by Derek Kite for many, many issues is back... only this time, i'm producing it :)

So, here is the first issue of my new digest: <a href="http://commit-digest.org/issues/2006-04-09/">http://commit-digest.org/issues/2006-04-09/</a>

It is from last Sunday (yes, it is 2 days late, but what is 2 days after 6 months? ;)) On the positive side, that means you only have 5 days to wait to the next issue - which will be better, I promise :) It might even have

If you go to <a href="http://commit-digest.org/archive">http://commit-digest.org/archive</a>, you can find the issues written by Derek - the archives are currently incomplete, but should be finished by this Sunday.

The people I need to thank are: Pino, for listening to the result of my poor PHP and HTML skills, Paleo for hosting my website projects and of course, Derek, for firstly bringing us the digest for so many weeks, and for answering my questions about the digest production process.

Things learnt so far: that my time predictions are too optimistic, and that perl is unreadable ;)

Anyway, please leave any comments/suggestions in the comments section on the <a href="http://dot.kde.org/1144788552/">dot story</a> (keeping in mind that this first issue is not an example of the completeness of the next issues!).

<!--break-->