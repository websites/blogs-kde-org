---
title:   "Kopete 0.7 released"
date:    2003-08-06
authors:
  - unknow
slug:    kopete-07-released
---
Woohoo the site is back! Good time to announce Kopete 0.7 is out of the door, even though that's probably old news for most. Except for a few upgrade issues and an ICQ password problem, the majority of things seems to be stable. However, not a very long time has passed yet and I am putting this in relation to the previous releases. :)<!--break-->