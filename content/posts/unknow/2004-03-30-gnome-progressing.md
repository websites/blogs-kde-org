---
title:   "gnome is progressing!"
date:    2004-03-30
authors:
  - unknow
slug:    gnome-progressing
---
wow, i'm amazed, i just read
   http://wlinux.ods.org/GNOME-2.6/
the gnome guys are doing some awesome work!

i'm really happy that they are actually catching
up with the state we were in 2 years back! 

its awesome to see that all that hard hard work is 
paying off, what with there awful coding environments, 
dreadful implementation languages, horrendous numbers 
of core packages, and ultimate levels of code duplication 
between core and non core components! just awe-inspiring!

its amazing that they are actually now getting all the 
money and support that is required to work beyond that and 
almost approach the functionality that kde had over two years 
ago, in some cases even three years! to bridge a three year
gap in just one release, amazing!

a big chorus of clapping for the gnome guys, congratz!

admittedly the "warn on modified form feature" in epiphany
is pretty nifty though, i've had problems with closing
unposted blog entries windows in the past :)

Alex