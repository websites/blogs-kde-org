---
title:   "Progress"
date:    2003-07-20
authors:
  - unknow
slug:    progress
---
So, a quick primer for people who aren't familiar with me.

I wrote and maintain the dotNET widget style. A lot of people have given me a bunch of different compliments on it, so I've gotten kind of a big head. dotNET is about to be renamed, although I'm not sure as to what it's going to be renamed to - my own preference at this point is heavily leaning towards calling it 'Bytecode' but we'll see if anybody comes up with something more clever or appropriate.

Despite what you might hear from kszwed/gallium (or whatever his current nickname is), or SadEagle, or fred1 - dotNET is actually pretty popular. There are still plenty of optimizations to be had, but that's ok. It just means that the style will get faster. Scarily enough, even in its current and past forms, people have forked it to start their own style projects. Well, ok, it's not really _that_ bad, but I find it kind of amusing. (To date, the forked versions that I know about include the GONXstyle for KDE2, the Reno desk stuff also for KDE2, the dotNET-R style for KDE3, the Slicker style for KDE3, and also the new style in kdenonbeta/clean, being done by cullman, one of the Kate guys.)

I'm also working on Kiwi, a music jukebox aiming to be just as easy to use as iTunes and more well-integrated into KDE. So far, Kiwi has come a loooong way from where it used to be, and I expect it to keep getting better. We now offer public read-only access to the Subversion repository that we use, so people can pull up-to-the-minute sources if they want to destroy their hard drives. MWAHAHAHA!

I'll see about putting up a few screenshots about Kiwi here. Of course, they'll be using dotNET as the widget style. :)

-clee