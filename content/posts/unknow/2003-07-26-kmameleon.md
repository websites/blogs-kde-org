---
title:   "KMameleon"
date:    2003-07-26
authors:
  - unknow
slug:    kmameleon
---
OK... So I have managed to install KDE on qt-copy these days... now going back to development...

I am figuring, that xmame has some really weird and funky options, like -norotate isn't exactly the same as rotation switch disabled, and -nothrottle isn't the same as throttle switch disabled... Weird... Ok... So I have deactivated the -[no]command switches and now it should work better... Specially in the advanced mode...

So what is next? I am looking at those x11/xgl/SDL options, and I think it would be very neat idea, if I create modules for them, too, and still fix some strange mod_info behaviours...

Meanwhile, I seek for someone to draw a nice chameleon icon for the app :)