---
title:   "Reverse video code?"
date:    2003-07-23
authors:
  - unknow
slug:    reverse-video-code
---
Finally fixed ksirc's handling of effects characters.  For bold, underline and reverse it used to use ctrl-b, ctrl-u, ctrl-r.  This followed the standard text irc clients.  

It causes problems though, so I changed it to *<b>bold</b>* _underline_ and #reverse#.  Problem though, I can't find anyone else who does reverse video. Does #reverse# make sense?  Does it match with anything else out there.

Anyone know anything else that uses similar codes for reverse video? Anyone know what I'm talking about?