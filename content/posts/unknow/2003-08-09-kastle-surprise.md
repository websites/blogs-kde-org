---
title:   "Kastle Surprise"
date:    2003-08-09
authors:
  - unknow
slug:    kastle-surprise
---
I wanted to go to Kastle. I wanted to go really badly. Well, unfortunately it happens during the only week my girlfriend and I could have gone on vacation this year. Now she's busy with her diploma thesis and I could go to N7Y, but registration is closed. ARGH! I hope it will be possible to get some last-minute planning done.<!--break-->