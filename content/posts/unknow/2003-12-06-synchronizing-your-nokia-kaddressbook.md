---
title:   "Synchronizing your Nokia with KAddressbook"
date:    2003-12-06
authors:
  - unknow
slug:    synchronizing-your-nokia-kaddressbook
---
It's been a long long while since I came up with a blog entry here, mostly since I didn't do anything interesting for the public lately. KDE is still in a freeze, so I have been fixing a few bugs occasionally, nothing more.

However, due to the integration of Kopete into KAddressbook, I wanted to start to use KAddressbook seriously. This also meant to be able to synch it with my Nokia 6310i, preferably via Bluetooth. I went to the store, bought a Bluetooth USB adapter and found out that nobody seemed to have attempted synching a Nokia via Bluetooth before.

After quite a while of trial and error, testing CVS snapshots and even thinking about writing my own protocol software, I got a working setup. I've written a small article documenting my steps, you can read it <a href="http://www.tillgerken.de/item/214">on my personal page</a>.<!--break-->