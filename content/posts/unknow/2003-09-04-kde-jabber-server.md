---
title:   "KDE Jabber server"
date:    2003-09-04
authors:
  - unknow
slug:    kde-jabber-server
---
Long time no posts from me, I have been buried in exams work - but that is now over!

A while ago I found a chatlog posted on the web where a few persons obviously close to the <a href="/www.jabber.org">Jabber</a> project (or even <a href="/www.jabber.com">Jabber Inc.</a>) were discussing ways of promoting Jabber. One of their thoughts was how about setting up a server for the KDE folks?

Regardless if it's Jabber sponsored or not, what do you think about a KDE Jabber server?<!--break-->

One of the cool things would be to map the email addresses everybody now has at kde.org to Jabber IDs, so that everyone could also be reached via IM. I understand that many people will already have other IM IDs, some will not even show interest in Instant Messaging. However, Jabber is really cool (IMHO) and since a lot of clients now also support Jabber, it would be great to have an open protocol for an open project!

Any thoughts?