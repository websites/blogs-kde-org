---
title:   "What is your rating?"
date:    2005-10-04
authors:
  - unknow
slug:    what-your-rating
---
I have now extended a certain myscreen feature kde-wide... buzz ratings!
Check the charts at http://myscreen.org/buzz

As this is the first calculation, naturally the "change" indicator is flat - however, I will probably run the calculation once a day.

Now, to stop everyone asking me how they are calculated, here is what I have written on the FAQ page:

"...a number of sources, including commit activity, mentions on webpages, and short-term news and discussion. Each source is weighted based on overall importance and influence on "buzz", then added together to produce the buzz rating."

If I have left you or your application out, it wasn't really on purpose - if you want to be added to the charts, find me on IRC.

More to come soon.
<!--break-->