---
title:   "KStringHandler::tagURLs"
date:    2003-07-20
authors:
  - unknow
slug:    kstringhandlertagurls
---
Trying to fix http://bugs.kde.org/show_bug.cgi?id=60522 isn't a problem in kSirc.  It's a generic problem in KStringHandler::tagURLs.

URL entries that are of the form http://url/~ or end in any none-word or none-number entry gets poorly highlighted.

Looks like the best fix is to port kdepim/libkdenetwork/linklocator is tagURLs.  Better thoughts?