---
title:   "Holidays and KOffice"
date:    2005-12-20
authors:
  - rwlbuis
slug:    holidays-and-koffice
---
 Well, holidays have started, and it is about time since the last months of work
were very busy. Now I am taking a small break from ksvg2 hacking to work again
on KOffice and Karbon, fixing bugs and small annoying problems. I think I'll
wait with large redesigns/ui changes until after the next release though.

 One thing I noticed when fixing a problem with eps/Adobe illustrator import
is just how nice developing on these filters is. I mean, when done right, every
change in the code gives a different, hopefully improved visual result, so in
this sense filter development is easy. Also when you use an app like gs you
can check what the eps file really should look like. So if you are looking
for a nice, small project to work on, please contact me!
<!--break-->