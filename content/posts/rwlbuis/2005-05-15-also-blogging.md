---
title:   "Also blogging"
date:    2005-05-15
authors:
  - rwlbuis
slug:    also-blogging
---
Well, I decide to give blogging a go ;-)
There are so many interesting things going on right now (qt4, kdom stuff)
that this may be the best time to start. Hopefully this way we ksvg devs can also improve
the external communication, since we unfortunately did not give many updates in
a while, while lots of coding did take place. Hopefully we can start by updating svg.kde.org
a bit :)