---
title:   "Macbook Pro arrived"
date:    2006-04-10
authors:
  - rwlbuis
slug:    macbook-pro-arrived
---
On friday, after a lot of anticipation, my Macbook Pro finally arrived :)
It was a reward for contributing to webkit(http://webkit.opendarwin.org/blog/?p=44).

It is the first Apple computer I own, using one or two for work is not the same:)
After playing around with it, it is clear that it is a very nice machine. A lot of
other laptops look clunky and big next to it, especially my old (windows) laptops. One
of the things I like most right now is the media centre and being able to use the remote,
that is obviously something really nice to demonstrate :)

Unfortunately there seem some issues with the Macbooks, some small, some really bad. My
machine behaves fine so far. So once again, a big thank you to Apple! I am starting to
contribute back by fixing four (svg) bugs over the weekend, well isn't that what open
source is about? :)
<!--break-->
Cheers,

Rob.