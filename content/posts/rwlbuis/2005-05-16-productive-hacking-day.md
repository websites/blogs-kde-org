---
title:   "A productive hacking day"
date:    2005-05-16
authors:
  - rwlbuis
slug:    productive-hacking-day
---
I am not always the optimistic kind, but "today was a good day".
A lot of code was added to ksvg2 and kdom that I can feel content with.
It is never a good feeling to leave code lying there that you know is
hacky. So yes, lots of refactoring of views, parts, zoom and pan etc. Then
also some fixes for clipping, which seems always tricky.

And I should not forget, psn is adding nice bug fixes for karbon, cheers!