---
title:   "Refactorings for KDevelop"
date:    2015-05-24
authors:
  - maciej
slug:    refactorings-kdevelop
---
I am going to implement some refactoring tools for KDevelop (<a href="http://www.google-melange.com/gsoc/proposal/public/google/gsoc2015/maciej_poleski/5629499534213120">GSoC 2015</a>). After reading libTooling documentation (quite nice), some guides and reviewing some KDevelop/KDevPlatform/kdev-clang source code my ideas become more sharp. That's some of my thoughts.

In general refactoring tools will work on whole project (or even set of projects). That's because changes in one source files may (and often will) affect other files as well. In fact automatic refactoring becomes important especially in these cases. We may even want to think about refactoring in terms of changing (abstract) structure of code in some projects rather than "change identifier in this place to &lt;your favourite name of variable&gt;".

We need precise information about project we are working on: all files with full compile commands. Compile commands contain necessary informations like version of language used in file, defines, includes and other. These informations affect semantics of source code.

It is very easy to build CompileDatabase using compile_commands.json file. In case of CMake based project this will be very easy task. In case of other projects there is a <a href="https://github.com/rizsotto/Bear">Bear</a>... I will go back to this later. Existing libTooling implementation can use informations from compile_commands.json, but I can provide my own plugin for this task (CompilationDatabasePlugin). This could be a place for use of Bear and then delegate to JSONCompilationDatabase. In any case this task seems be connected with IBuildSystemManager (which would have to provide information about localization of build directory - looks quite easy :) ). A bit more difficult is keeping this database in sync with changes of project structure.

libTooling works on VFS (virtual file system). This VFS can be provided by user (me, writting tools based on Clang). My idea is to use things like IDocument/Document/DocumentModel from kdevplatform to provide custom VFS for Clang. This should be non intrusive for KDevelop (avoid changes in KDevelop internal file access API), avoid modification of underlying files without KDevelop knowledge, races between KDevelop cache and state of files on real file system and get along with other functionalities like Undo.

Result of libTooling work will most likely be in form of Replacements - translate them to DocumentChangeSet (but full implementation of VFS should be enough to "just work", there is even a tool named clang-apply-replacements which is not going to be used here).

That's about internals, but what about UI? I see one place in LanguageSupport (ClangSupport in kdev-clang) which can be used to extend context menu. I also used this earlier in other project. It would be nice to populate this menu only with applicable refactorings and not kill UI responsiveness...

I hope this summarizes task of integration tooling with KDevelop. Appropriate changes should be created soon and shipped into kdev-clang to enable actual implementation of refactoring tools. It is going to be interesting summer...