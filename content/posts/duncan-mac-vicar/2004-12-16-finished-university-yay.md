---
title:   "Finished University! yay!"
date:    2004-12-16
authors:
  - duncan mac-vicar
slug:    finished-university-yay
---
<p>After 7 years of hard work I finally finished University. No more classes. I should now work hard to give my MSc. thesis a final polish so I can present it in the following months, and then I'll able to graduate both as a Civil Industrial Engineer in Computer Science and as a MSc. in Engineering at the same time. I am very happy with this.</p>
<p>These days without the stress of the final weeks have been fun. Celebrating, parties, Pubs, Karaoke. Even Kopete Latex plugin got some love thanks to the free time. Closed all bugs and implemented every wishlist except one.</p>
<p>Now time to go bed, as my thesis deserves some love tomorrow too...</p>
<!--break-->

