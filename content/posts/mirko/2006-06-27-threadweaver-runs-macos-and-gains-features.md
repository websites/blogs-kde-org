---
title:   "ThreadWeaver runs on MacOs, and gains features"
date:    2006-06-27
authors:
  - mirko
slug:    threadweaver-runs-macos-and-gains-features
---
Only recently, thanks to KDAB, I got my hands on one of the brandnew MacBooks running MacOs X on the Intel dual-core CPUs. One of the main reasons to get it was to measure the effect the two cores have on ThreadWeaver's tests and examples. And what do you know - besides a little makefile fix and some adaptions to different dynamic library path setups, all of it runs fine - and amazingly fast. Exact measurements will follow lateron, but so far the results are very promising. 

No bug was discovered by the rather trivial port. Bugs that could have surfaced would be issues that result from real concurrency as opposed to pure CPU time-slicing. None of that happened, and all unit tests passed right out of the box. Overall, starting to code on MacOs was a quite smooth experience. Qt installs and runs as expected, and all I need to code (emacs, make and a compiler :-) )is available under the hood. I wonder what took me so long to try it, maybe all that practical reasoning? The next step will be to install Linux on the same hardware. Let's see how that works out. 

For some more exact performance measurements, I am thinking about creating a small benchmark suite within ThreadWeaver. But before that, queue policies will be implemented that allow special restrictions on sets of jobs. For example, it will be possible to state that, from a specific set of jobs, only 2 can be executed at the same time. That should be an interesting feature, for example, for disk I/O optimizations. 

Jobs now can return whether their execution was successful or not, and depending jobs will only be executed when the jobs they depend on finish successfully. Job collections dequeue smoothly, even in a recursive scenario (collections containing sequences et cetera). Suggestions welcome.