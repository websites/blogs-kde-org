---
title:   "icecream in trunk now supports Mac Os X compile servers"
date:    2007-05-18
authors:
  - mirko
slug:    icecream-trunk-now-supports-mac-os-x-compile-servers
---
KDE is moving from X11 only to be a good citizen on Windows and Mac Os X. For those who like to code on Mac Os X, here's a bit of good news for you: icecream in KDE trunk now supports Mac Os X machines as compile servers. This means that compile jobs can be distributed between Mac Os X machines with the same Xcode version. And if somebody goes the extra mile to make a Linux-Mac cross compiler, even between Linux and Mac Os X nodes. 
<!--break-->

icecream and a couple of fast compile nodes feels like an absolute requirement to code on Linux. Working without it is comparable to eating rotten toasts or something. You can imagine our pain when we wanted to start some development on Mac Os X, and watched the build process go on for hours and hours. We quickly calculated how many hours a port would take, and realized it is well worth spending the time to make icecream work on Mac Os X. You may know that Mac Os X uses GCC to compile. And it is based (by some distance) on BSD. So a port seemed possible, after all.

The port was done by Frank Osterfeld and me. Stephan Kulow and Dirk Mueller of SuSE merged the changes into trunk. 
It is in fact an interesting experience to work out Linuxisms and find ways of doing things that work on different Unix platforms the same way. It should be possible to make the same source code work in the Cygwin environment. Finally, your brother's high-end gaming PC could work on something useful. Or Miss Wife's laptop in the kitchen. 

Since we only have access to Intel Mac's, we are unable to test it and make it work on older PowerPC Macs. If somebody has one of those sitting around collecting dust, we are happy to take it in. Also, we tested in with the latest Xcode release (Xcode is the package that delivers gcc and make and stuff on Mac Os), we have no clue whether icecream will work with older versions. 

The scheduler should be able to coordinate compile jobs across platforms. This means one scheduler should be enough, even if you want to use Linux and Mac Os X compile nodes in the same icecream network. We tried compiling Qt, and KDE modules, and so far it seems to work just fine. Let us know if you discover bugs. And have fun with it...

