---
title:   "CMake is a great tool"
date:    2007-03-21
authors:
  - mirko
slug:    cmake-great-tool
---
At last year's Akademy, I had the chance to ask Bill Hoffman, one of the key figures behind CMake, a couple of questions about it. One of them was the availability of the full CMake documentation, which seemed to only in print. It turned out that this is not the case: CMake is well-documented and easy to learn. Read on for more.
<!--break-->

Bill said that the documentation in the CMake wiki is nearly as complete as the book, and in parts is even more detailed or accurate.

To give me a chance to verify this, he gave me a copy of the book (thanks, Bill. I still knock myself over the head for not having you sign it). So on some long airplane ride, I took the book and read it. When I started to compare my notes from the first CMake-based projects I worked on, it became apparent that the book offers the same information that I also had access to -- it is more structured, and more comfy to consume leaning back with a cup of coffee, but the same basic content. 

Today, I consider myself rather fluent in CMake lingo. The book remains on my desk as a reference, but to find a certain CMake variable I rarely use it. I browse the CMake wiki.

Why is this important? Just to avoid a wrong impression. Some trolls under bridges (not people at a certain company we partnered with) have started to grumble "What is wrong with autotools?" Or "Them new shiny tools you are using ain't looking like my trusty old sledgehammer!" See, we like progress. We do not use CVS anymore, because there are better solutions now. And switching to CMake was one of the moves that increased KDE's productivity, and allowed certain developments like the cross-platform desktop that would have been a major PITA, if even possible, with auto- or other tools. Lots of respect and appreciation to Alexander Neundorf for his kind, focused way of convincing us. And thanks to the CMake team for a great tool.