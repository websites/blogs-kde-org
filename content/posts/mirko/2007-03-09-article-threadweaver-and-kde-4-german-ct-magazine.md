---
title:   "Article on ThreadWeaver and KDE 4 in German c't Magazine"
date:    2007-03-09
authors:
  - mirko
slug:    article-threadweaver-and-kde-4-german-ct-magazine
---
This weeks c't magazine furnishes an article on ThreadWeaver programming as part of their long-going series on articles on concurrent programming. An interesting feature of the article is that is describes ThreadWeaver as a component of the upcoming KDE 4 platform. 

<!--break-->

As such, it displays 3 screenshots of the same example program, running on the three major platforms. This seems to be the most compelling feature that the KDE community promised to the outside world at the moment. It looks like the upcoming Akademy will bring a number of talks about the non-Unix KDE platform. It looks to me like this development is worth all effort and necessary focus, to bridge the gap for potential users of KDE as the desktop, not as the platform of Kontact on Mac. 
To prepare the article, a few fixes needed to be applied to make everything work on Windows. c't has tested ThreadWeaver on an experimental 4 core system with hyperthreading, and it scales very nicely with the CPU power. Loading 56 4MBit images files took a little over 2 seconds. I guess that is fast :-) 
