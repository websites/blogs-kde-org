---
title:   "openSUSE 11.0: Package installation 743% faster for default patterns"
date:    2008-04-09
authors:
  - coolo
slug:    opensuse-110-package-installation-743-faster-default-patterns
---
We implemented some very interesting features for openSUSE 11.0 to make the
installation easier and faster:

<ul>
 <li>giving it a green face</li>
 <li>making the configuration automatic</li>
 <li>switching from bzip to lzma for rpm payload</li>
 <li>put images of default patterns on the DVDs</li>
 <li>move online update to the desktop applets</li>
 <li>improved package management speed</li>
</ul>

Yesterday I installed 11.0 Alpha3+ (which is a snapshot of Factory burned
on DVD) and it definitely felt fast and easy. So today I decided to give
10.3 another try to measure where exactly we improved or if it's just placebo.

I let the #yast channel know my ongoing times (here only a short excerpt):
10:22  * coolo starts with a 10.3-i386
10:23 &lt; coolo&gt; ok, let's start.
10:29 &lt; coolo&gt; 6 minutes for first stage configuration
10:52 &lt; coolo&gt; finishing basic installation
11:06 &lt; coolo&gt; I'm in the 10.3 KDE online and registered
11:06 &lt; coolo&gt; 43 minutes all in all
11:09 &lt; coolo&gt; ok, on to 11.0
11:16 &lt; coolo&gt; starting from DVD
11:21 &lt; coolo&gt; first stage config done
11:25 &lt; coolo&gt; images deployed
11:33 &lt; coolo&gt; finishing basic installation
11:40 &lt; coolo&gt; I'm online in KDE - 24 minutes

I did not do online updates in either - that would have been incomparable, the
registration is missing from 11.0 though, this will only be done in time for 
beta1. But even if you add another 2 minutes, an improvement from 43 to 26
minutes would be a very good deal to install a full blown system with 2.6 GB.

Sorry that I did not create graphical representations of the individual stages,
that will come later - and yes, the title of this blog was fixed before I did
the actual testing.
<!--break-->