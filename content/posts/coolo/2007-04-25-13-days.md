---
title:   "13 days"
date:    2007-04-25
authors:
  - coolo
slug:    13-days
---
Just to keep you informed: the KDE sysadmin team is working on migrating our svn server to subversion 1.4. If you look for an howto on this you will find "dump and load" - aka svnadmin dump old | svnadmin load new. Nothing fancy. So I started this on my workstation (having had it pimped in april I thought it's good enough) and waited... 13 days. Now I need another day to load the revisions of that 13 days into the repository.

But soon we will have svn 1.4 on our servers - and most likely you won't notice.
<!--break-->