---
title:   "So little hands"
date:    2007-08-20
authors:
  - coolo
slug:    so-little-hands
---
<p>This is my first evening I have on my own, so I thought I let the world know:</p>

<p>On the 16th of August 2007 our son Felix made world++. It was a busy day - after I complained on the 15th about deadlines it came a bit out of the sudden. Just as we agreed with the midwife that the 17th would be the better date anyway, he protested so heavily that he made it at 16 minutes to midnight (I don't want to bore you with medical details).<br>
Meanwhile I figured the 2^4th of the 2^3th month of 2..7 has its charm too - but only to boring people :)</p>

<p>But right to the interesting parts, the obligatory proof that we have the cutest baby on earth:</p>
<!--break-->

<img src="http://ktown.kde.org/~coolo/felix1.jpg">
<img src="http://ktown.kde.org/~coolo/felix2.jpg">