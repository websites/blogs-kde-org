---
title:   "I told you it's flying"
date:    2005-12-02
authors:
  - coolo
slug:    i-told-you-its-flying
---
<p>Seems I scared some people with my 15 fonts story. So no more worrying, 
a SUSE Linux 10.1 Alpha3 (pretty standard system) can be pretty quick:</p>

<p>
That is with 280 fonts - and as you can see my bash is up after 4 seconds.
Of course this works only if your kdm will preload your data in the background,
which it already does in 10.0 though.</p>

<img src="http://ktown.kde.org/~coolo/kdechart.png">
<!--break-->
