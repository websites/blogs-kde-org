---
title:   "I had enough"
date:    2004-09-03
authors:
  - coolo
slug:    i-had-enough
---
I promised I would improve my blog rate, so here I start.<br>
The day started interesting: Got a mail from Martin Pool asking why we forked icecream and it took me a while to give a good enough answer. In short: We didn't fork, but wrote something similiar from scratch using distcc code. Will still be interesting how this story continues though.<br>
Still the thing that turns around in my head the most right now is this: I leave for two weeks greece next week and got already in this dangerous "doesn't really matter if I start with it now" mood. I still would like to make the icecream scheduler use even less blocking file operations and have the client send local jobs notifications async, so updating monitors doesn't block Simon's configure runs. The notification of monitors is right now blocking too (one thing that made the aKademy cluster behave so much different than the one at SUSE: the number of monitors), but Michael is not yet sure if he wants non-blocking TCP or UDP (I guess, if we go UDP, we need some way to reset the state of monitors).<br>
For those interested in KDE 3.3 release schedule: I return at 27th to full work, so I target that week for tagging/packaging and ~5.10. for KDE 3.3.1 (I now that's roughly 7 weeks since KDE 3.3.0, but I hope you agree that I deserved some kind of vacation)</p>
<p>KDE 3.4 schedule will appear then too. My current plan goes like this:<br>
<ul>
 <li>Feature list freeze in end of november</li>
 <li>Alpha release in december, feature freeze early January</li>
 <li>Beta releases in end of January and end of February</li>
 <li>Final in end of March</li>
</ul>

How this works together with KDE 4, we have to see. I expected much more pressure for a quick start of KDE4 development, but didn't feel that - the Qt4 delay did good in keeping our developers focused :)
<!--break-->