---
title:   "Deprecated functions"
date:    2006-09-13
authors:
  - coolo
slug:    deprecated-functions
---
From time to time I do a fresh trunk compile to entertain myself on the logs (as I have no time to do real development at the moment).

So this is the top list of warnings about deprecated functions:

<pre>
    168  'insertItem' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:170)
    156  '__base_ctor ' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qwidget.h:191)
    146  '__comp_ctor ' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qgridlayout.h:53)
    123  '__comp_ctor ' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qboxlayout.h:113)
    123  ‘bitBlt’ is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/painting/qpaintdevice.h:126)
     86  'setItemChecked' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:259)
     75  '__comp_ctor ' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qboxlayout.h:133)
     74  ‘insertItem’ is deprecated (declared at build/qt-copy/include/Qt/../../../../qt-copy/src/gui/widgets/qcombobox.h:257)
     69  ‘__comp_ctor ’ is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qboxlayout.h:113)
     65  'insertItem' is deprecated (declared at build/qt-copy/include/Qt/../../../../qt-copy/src/gui/widgets/qcombobox.h:257)
     64  'addMultiCellWidget' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qgridlayout.h:123)
     59  ‘__comp_ctor ’ is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qgridlayout.h:53)
     57  'latin1' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/tools/qstring.h:428)
     54  'setPixmap' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qabstractbutton.h:132)
     54  ‘setCurrentItem’ is deprecated (declared at build/qt-copy/include/Qt/../../../../qt-copy/src/gui/widgets/qcombobox.h:237)
     54  'insertItem' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:157)
     54  '__comp_ctor ' is deprecated (declared at inst/include/kaction.h:276)
     52  'find' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/tools/qstring.h:450)
     51  'insertSeparator' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:191)
     51  'insertItem' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:173)
     40  'repaint' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qwidget.h:726)
     40  ‘__base_ctor ’ is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/kernel/qwidget.h:191)
     37  'setItemEnabled' is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qmenu.h:250)
     37  'data' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/tools/qmap.h:270)
     37  '__comp_ctor ' is deprecated (declared at inst/include/kactivelabel.h:42)
     36  'lower' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/tools/qstring.h:477)
     35  'find' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/tools/qstring.h:448)
     32  'currentItem' is deprecated (declared at build/qt-copy/include/Qt/../../../../qt-copy/src/gui/widgets/qcombobox.h:236)
     32  '__comp_ctor ' is deprecated (declared at inst/include/ktoggleaction.h:125)
     31  'fromPathOrUrl' is deprecated (declared at inst/include/kurl.h:859)
     31  deprecated conversion from string constant to 'char*''
     31  '__comp_ctor ' is deprecated (declared at inst/include/kaction.h:331)
     28  'start' is deprecated (declared at build/qt-copy/include/QtCore/../../../../qt-copy/src/corelib/kernel/qtimer.h:67)
     28  ‘setPixmap’ is deprecated (declared at build/qt-copy/include/QtGui/../../../../qt-copy/src/gui/widgets/qabstractbutton.h:132)
</pre>

So if you're interested, you can make any of these warnings an error for yourself and recompile to port the warnings away. But make sure know what you're doing.
<!--break-->