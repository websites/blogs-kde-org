---
title:   "Stopped escalators"
date:    2006-09-14
authors:
  - coolo
slug:    stopped-escalators
---
Why is it so complicated to walk on stopped escalators? Is it because it feels like walking back in time? Or are the riffle telling the feed not to move? Or is just because you're loosing your rythm? I don't know - fact is: most people I watch avoid stopped escalators if there is a normal staircase next to it.
