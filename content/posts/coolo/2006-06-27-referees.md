---
title:   "Referees"
date:    2006-06-27
authors:
  - coolo
slug:    referees
---
What's up with all these teams being unable to score and then claiming the referee was unfair? To remind everyone: this game is not about standing still and wait for the referee to signal penalty shoot out. <br>
Having said that: the russian referee for Nuremberg wasn't really up to the unfairness of the match and it's sad that the rules do not allow the referee to watch on slow motions on TV - I'm sure we would have seen a red card in 8. minute and the match would have been another.<br>
Beside that: I'm really looking forward for the match on friday, this city is not the same if good teams play - and Germany is one of them this time. You can feel the proud and see it in all faces and on everyone's shirt.<!--break-->