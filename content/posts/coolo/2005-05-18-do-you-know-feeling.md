---
title:   "Do you know the feeling?"
date:    2005-05-18
authors:
  - coolo
slug:    do-you-know-feeling
---
Do you know the feeling when your job just isn't fun because you have to come to work every day and hack at features you don't like or don't need or both? <b>I DON'T!</b><br><br>

And you know why? Because I work for <a href="http://www.novell.com/linux/suse/">SUSE</a>. And the best: you can have that <a href="http://www.novell.com/offices/emea/jobs/germany/index.html">too</a>!<br><br>

The last weeks were pretty interesting too.
<ul>
<li>I managed to convince Ossi that it's about <a href="http://mail.kde.org/pipermail/kde-cvs-announce/2005/000006.html">enough</a></li> 
<li>I then had to do some major subversion repository scripting (and love the flexibility svnadmin gives me - but even more the machines I can use for it ;):<br><!--break-->
  <a href="http://ktown.kde.org/~coolo/icemon.swf"><img width="80%" src="http://ktown.kde.org/~coolo/icemon.jpeg"></img></a>
</li>
<li>I'm still toying with build system ideas - now that I get people to hack unsermake and even Ossi and David learned python the sky is the limit ;)</li>
<li>I started the kde@qt4 port and to my great relief not less than 26 developers jumped on it to help (of course I hope like everyone else that kdelibs is special and that the other modules are easier to port)</li>
<li>David and me went on trying to harmonize the translation module for KDE, koffice and extragear apps</li>
</ul>

And what's nice too is Stephan agreed that KDE 3.4.1 deserves a better than being released in between, so he took over responsibility for it, while I concentrate on the technical aspects of the SVN move related to releases (the i18n->l10n move is part of that)


