---
title:   "Could be just me, but..."
date:    2005-09-03
authors:
  - coolo
slug:    could-be-just-me
---
OK, this is my first and only blog entry during the Malaga conference. And so I have to report about what really bothers me about this city. 

Yesterday we decided to take a random trip through the city and then we found this nice little place with a huge obelisk on it. There was a cocktail bar on it and many many young people. So we ordered some drinks (and I saw for the very first time in my life a cocktail presented in a coconut - for just 6 Euro the guy opened a coconut, put lots of alcohol in it and put cream on top of it, amazing. I admit though that it didn't taste just as well, but I'm not _that_ much of a cocos fan, but then again it wasn't my drink) and just sat there. And now the trouble starts:

For every guy on that place there were about 3 girls (at least 2 of them good looking). I mean what does this city do to its guys? Are they are just fed up with good looking women and prefer staying home playing Quake? There must be something like this in this culture. 

Of course the rest of the week I spent most time with much more guys than girls (but just as Zogje said, the women we have around are really given our community a lot both on a technical and social level) - but these guys were all imported!!! And now that I see all these disappear I'm kind of afraid where they're heading to. Don't you agree that the whole plot reminds a lot on the rape of the Sabine women just with different sexes?

I'm really afraid, so I better try to get savely to the airport today and put myself in the arms of the most wonderful woman on earth tonight.
<!--break-->