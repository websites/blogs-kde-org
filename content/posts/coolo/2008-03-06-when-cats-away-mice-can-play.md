---
title:   "When the cat's away, the mice can play"
date:    2008-03-06
authors:
  - coolo
slug:    when-cats-away-mice-can-play
---
It may work earlier for other couples, but it didn't work out earlier for us. So yesterday was officially the first evening mom was out alone - that is without being called home at 7pm because coolo ran out of ideas how to calm the baby.

Breastfeeding is really a great way to feed babies (and taking that Felix almost tripled his birth weight in just 6 months proves my point), but it also has one downside: My wife was irreplaceable when it came to food for quite some time and she both loved and hated it. Now she's out of direct duty as he's getting random vegetables for lunch and millet gruel for dinner, she thought we retry the experiment. 

So I had an interesting evening, of course this ultraclever boy figured pretty quickly that something is different this evening. So he protested pretty clearly when I tried to feed him and did not want to sleep at all. I guess many parents know that: too tired to eat, too hungry to sleep.

So I put on our beloved "Ergo Baby Carrier" and went up and down till he finally was more tired than hungry and slept. Then I read the backlog of the openSUSE project IRC meeting and turned on the TV to watch some soccer. But as sleeping hungry wasn't the long term plan of my son, I only watched the first half. Then the bottle? carrying? diper? fun? bottle? sleep? carrying? bottle? fun? - cycle started, which almost frustrated me as much as calling my wife. Then we entered the living room and he spotted the soccer - and was calm out of the sudden. Something of it fascinates him, so he forgets every other problem he's having. This only works with soccer - and for a short time if our birds are having a fight.

So I was very pleased the fabulous Neuer kept the race open till well after 10pm. During that time, Felix was nipping from the bottle and it was enough so he felt back to sleep early enough so I could only guess the result of the penalty shootout while singing him to sleep in the sleeping room. But I did not expect anything else from the Neuer we saw last night (and even though the guy basically destroys the dream of my brother-in-law to ever become goal keeper auf Schalke).

Needless to say Maren was very happy to see us alive as she came home. And I'm very happy she does not want to go out tonight, so if time permits I may join the yast team tonight. 

<img src="http://ktown.kde.org/~coolo/Felix_dick.jpg">