---
title:   "Paradise News"
date:    2004-08-02
authors:
  - till
slug:    paradise-news
---
Aaaah, summer. :) The last few days I've been spending a lot of time on the balcony in the sun, hacking away or just developing my tan. Around this time of year it's really, really nice to live in <a href=http://www.laboe.de/WebCam/bilder/now.jpg>paradise</a>. I have hardly any commitments, apart from keeping my wife happy and the sun screen layer constant, thanks to some projects which have fallen through, so I can take it easy, get my bike repaired, work out some and look forward to aKademy. Oh, and get last minute bug fixes in, of course. If it weren't for that dreadfull surgery my wife has to undergo soon, life could hardly be any better.