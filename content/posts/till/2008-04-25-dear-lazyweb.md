---
title:   "Dear Lazyweb,"
date:    2008-04-25
authors:
  - till
slug:    dear-lazyweb
---
speaking of logos, and while I'm in 1:N communication mode and have your kind attention, large and lovely N that you are: we're in need of a vector version of the current Kontact logo, for purposes of blowing it up indecently in size for use in a poster or two (for Linuxtag). If you, dear k \in N, happen to be in possession of such, or happen to know j \not\in N, but the creator of said artwork, or happen to be j \in N, said creator, yourself, please get in contact with me at your earliest convenience for the overall furtherance of the ascent of the K (assuming |K| > |N| without loss of generality).<!--break-->