---
title:   "Whack them buggers!"
date:    2004-07-27
authors:
  - till
slug:    whack-them-buggers
---
So the Kontact bug squashing day was a big success, I think, with a lot of bugs closed over pretty much all components of Kontact. Quite a few people showed up and especially KMail seems to have a acquired a bit of a bug squad, which is just awesome. Michael Jahn, Tom Albers, Ismail Donmez and others have been doing an amazing job and KMail now stands -65 for bugs and -73 for wishes for the last 14 days, which must be the best fortnight in the history of KMail bugs. Keep up the excellent work, guys, it is very much appreciated. If we're not carefull, Kontact 1.0 might actually rock. :)<!--break-->