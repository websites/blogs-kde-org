---
title:   "Beyond aKademy"
date:    2004-08-30
authors:
  - till
slug:    beyond-akademy
---
Back from Ludwigsburg and things have settled down some at last. It was very nice to meet the rest of the PIM team again and of course all the other KDE people. I had a great time there and I'm glad I spent comparatively little of it hacking and quite a lot of it talking to people, having beer and meals with them and just generally hanging out. The downside of that is that I got none of the things done I intended to do there. :) Sadly I couldn't be there for either the social event on Saturday or the talk I was supposed to do with Ingo on KMail on Sunday, but I'm sure he did allright on his own.
<br>
I'm pretty happy with the HP nx5000 I bought there at reduced price as part of HP's generous sponsoring of the event, it actually suspends to disk nicely and the keyboard is just great. I'm spoiled from my old Compaq's 1400x1050 display, but I've already mostly gotten used to being back to a smaller resolution. It's very quiet and light, which is a rather nice change from the bulky Armada E500. Oh, and the headphone out doesn't frizzle and hum, which makes listening to music on the plane/train much more pleasant. /me pets HP.
<br>
Cornelius wrote a few days ago:
<small>"In addition to that I’m also happy about the progress kdepim made as a project and as a team as well. We had a kdepim meeting yesterday and it was amazing how easy it has become to find common ground and constructively work together into one direction. I really enjoy being part of this team."</small>
In short: So do I. What a team. And when I look outside of kdepim I find there are even more amazing people doing amazing work. And I'm happy and proud to be part of that larger team as well. Makes you feel all Olympic and fuzzy inside. Am I hearing the neighbor's kids hum the Free Software Song down there? 
<!--break-->