---
title:   "Conclusive proof - Allen Winter actually exists!"
date:    2009-03-05
authors:
  - till
slug:    conclusive-proof-allen-winter-actually-exists
---
Since I was in the US anyway, I thought I'd fly to North Carolina and verify something that has had the KDEPIM community wondering for years. Does Allen Winter, in fact, exist, and look like the picture, since no one has ever met him in person. Well, after a 2 hour plane ride from Chicago and another 2 hours in a car, I can attest that he does, in fact, exist, and is as nice in person as he is online. Photographic proof below. Your reporter will write more as events develop, now it's lunch time. :)

<img src=http://kdab.net/~till/TillandAllen.jpg>