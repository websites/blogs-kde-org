---
title:   "That's what happens when your mom actually uses it. ;)"
date:    2006-01-21
authors:
  - till
slug:    thats-what-happens-when-your-mom-actually-uses-it
---
I've got a bit of an unusual reason to blog today. My mom recently upgraded her machine to SuSE 10, being an avid KDE user, and now the driver for her pesky builtin USB wireless LAN card is not working anymore, which means she can't go online anymore, which sucks. Sound also seems to have broken in the upgrade, and the X-server only wants to start every 4th or so time. Sometimes it just freezes up completely, which I guess means some driver is borked. Unfortunately I'm on the other side of the country and can't log in remotely to fix things, what with the network being broken. So I'm wondering if there's anyone in the Saarbruecken, Germany area who either knows someone local to hire to come and fix it, or maybe even feels like helping out themselves, in exchange for my eternal gratitude and one of my dad's meals (he's an excellent cook, my old man). So, if you think you can help or know someone who could, please drop me an email at adam@kde.org. Thanks a lot.

In other news, my talk at OSWC II in Malaga in February was accepted, so with Andras Mantia, Antonio Larrosa, Joseph Spillner, Pedro Jurado Maqueda and myself we'll have quite a strong presence there. I'll be doing a non-technical talk there, which is a first for me, on diversity in Free Software projects and my recent experiences with KDE India. Looking forward to that one. Speaking of KDE India, I'm really proud of Pradeepto, Ananth and the gang, it's great to see the mailing list grow to more than 100 subscribers, the enthusiasm they have and all the cool things happening. Maybe this model of established community members "adopting" a regional organization to help get them off the ground might work for other parts of the world as well. So who wants to do KDE China? :)
<!--break-->