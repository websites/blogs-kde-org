---
title:   "testing, 1, 0, 1, 0, test, test"
date:    2004-07-09
authors:
  - till
slug:    testing-1-0-1-0-test-test
---
Hm. So clee tells me the world wants to read my thoughts on KDE development. The thing is, I usually don't bother with thoughts of my own, I mostly just ask Zack, or David. :)<br>Ok, I have one thought: It seems the progress handling infrastructure and dialogs David and I did for KMail are being adopted by some of the other pim apps, namely KOrganizer and KAddressbook, which is nice, because it means we can now show cross application progress info in Kontact, for example. If you are in the mail part and KOrganizer starts downloading calender data, you'll be able to see the progres of that as if it was an operation performed by KMail. Spiffy. Zack is using it in KConfigEditor as well now, apparently, so once 3.3 is out the door I'll likely add a few missing features and make it a bit more generic so it can go into kdelibs.<br>KMail seems in pretty good shape for the release already, stable and all, so hopefully we'll be able to incorporate some of the usability input from the fine folks at <a href=http://www.openusability.org>OpenUsability.org</a> and fix some of the remaining annoyances before the release as well.<br>clee, is that enough thoughts for now?