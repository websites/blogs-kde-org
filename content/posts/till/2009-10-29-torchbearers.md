---
title:   "Torchbearers"
date:    2009-10-29
authors:
  - till
slug:    torchbearers
---
With all the excitement and energy surrounding Akonadi and the ongoing porting of our main applications to it at the moment (over 100 commits to KDEPIM yesterday alone!), it's easy to get the impression that we've collectively abandoned our stable versions and the many users relying on them today. Not so. While Volker Krause and his team at KDAB (currently Kevin Ottens, Frank Osterfeld, Sebastian Sauer, Leo Franchi, Stephen Kelly and Laurent Montel, with various others pitching in occasionally, like Marc, Guillermo and Romain) are ripping through KDEPIM trunk, Allen Winter and Thomas McGuire (again aided by Marc and others) are faithfully watching over the stable branches. They are making sure that all relevant bugfixes found by the Akonadi port make it back into the 3.x and 4.x stable branches and are doing many bugfixes and features in those branches themselves, every week, which are then merged into trunk. This results in a steady stream of improvements into both the 3.x and 4.x series, all of which make it to our users (i.e. you out there, probably) via the Linux distributions and via the KDE Windows and Mac packages regularly. This is mostly unglamorous and sometimes boring work which they carry out with great professionalism and personal commitment, both during their KDAB work time and well beyond, in their personal time. They hardly ever get any recognition for what they do, so this is an attempt to remedy that a bit. Rock on, boys!
<!--break-->
