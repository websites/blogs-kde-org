---
title:   "fd.o disast^Wdiscussion"
date:    2004-01-30
authors:
  - daniels
slug:    fdo-disastwdiscussion
---
The "*nods in agreement*" comment at the bottom of <a href="http://blogs.kde.org/node/view/326">this post</a> is required reading. For all of you. Go Eike.