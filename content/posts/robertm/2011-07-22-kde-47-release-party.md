---
title:   "KDE 4.7 Release Party"
date:    2011-07-22
authors:
  - robertm
slug:    kde-47-release-party
---
When I first got involved in KDE, I was very disappointed that none of the release parties were ever close enough for me to visit.  There weren't very many in North America to begin with and most of those were either on the west coast or the midwest.  It is hard to really feel plugged-in to a community when your only interaction with everyone is on IRC and e-mail.  

I'm sure that there are other KDE developers on the east coast, but I've never had the chance to meet them.  So -- now that I own my own home, I've decided to do something about it. I've talked my wife into letting me throw a release party for 4.7 this Wednesday.  If you are a KDE developer (or user) on the east coast, it would sure be great to have you!

All the details are on the web:

http://community.kde.org/Promo/Events/Release_Parties/4.7



