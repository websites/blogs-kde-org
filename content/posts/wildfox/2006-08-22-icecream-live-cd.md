---
title:   "Icecream Live CD"
date:    2006-08-22
authors:
  - wildfox
slug:    icecream-live-cd
---
After having worked the whole week with an icecream farm at Rob's place,
I couldn't cope with the fact that my other pcs (two older linux machines, one windows macine)
are mostly idle... So I started the creation of a "OpenSuSE 10.1 Icecream Slave Live-CD"
(nice name eh ;-)

Beineri told me about Torsten Duwe's excellent live cd/dvd creation scripts. All I had to
do is setting up a new "icecream.config" file, a new package selection (text only minimal)
and run the scripts. 20 minutes later the 181 MB iso could be burnt.

Machine boots, automatically sets up networking. Login as root (no password), rcicecream start.
That's it :-) If the scripts are of any use, leave a comment and I'll upload them to my ktown account.

Niko<!--break-->