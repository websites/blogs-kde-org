---
title:   "Unity fun"
date:    2006-08-18
authors:
  - wildfox
slug:    unity-fun
---
Hello again,

it's been a long time since I blogged last time. University really demands me, nowadays.
All exams are done since ~ 25 days, and well that means, hacking hacking hacking.

I'm at Rob Buis's place since last Monday and it has been a very productive & relaxing
week. We're hacking on Unity/WebKit (as you may have guessed), combined with a nice
ICECream setup (<a href="http://ktown.kde.org/~wildfox/ktown/icecream.png">Hacking fun looks like this!</a>).

Hot news are probably that we've started merging back the Qt platform stuff from Unity into
WebKit. I've created the bugs 10463, 10464, 10465, 10466, 10467, 10470 to keep track of the
needed changes in WebKit, seperated in several categories:
* WTF (Web Template Framework) changes
* KCanvas Qt device
* Qt platform hooks
* Qt platform code
* CMake build files
* General build fixes for Linux

The platform hooks, the cmake build files & wtf changes & kcanvas qt device & general build fixes are
already landed in WebKit SVN. The others are waiting for review by Darin/Maciej. Hopefully we'll get them in soon (tm).

The impatient may already try WebKit on Linux: (5 Steps Guide)

#1 Get latest WebKit from SVN (beware this is a lot of stuff!)
cd /your/directory
svn co svn://anonsvn.opensource.apple.com/svn/webkit/trunk

#2 Prepare the WebKit build
mv trunk WebKit
mkdir WebKit-Build

#3 Get patches from my website
cd WebKit/

wget http://ktown.kde.org/~wildfox/ktown/WebKit-PatchFlood/platform-qt-unresolved-hacks.diff
patch -p0 < platform-qt-unresolved-hacks.diff

cd WebCore/platform/
wget http://ktown.kde.org/~wildfox/ktown/WebKit-PatchFlood/platform-qt.diff
patch -p0 < platform-qt.diff

#4 Build WebKit
cd ../../../WebKit-build/
cmake -DCMAKE_BUILD_TYPE=debugfull ../WebKit
make

#5 Test it!
cd WebCore/platform/qt/test
./testunity http://www.kde.org

Only conformant sites work atm, as quirks mode isn't enabled (mimetype detection missing).
Have fun!

You can check the status of following bug: (once it is in you don't need the patches anymore,
except the platform-qt-unresolved-hacks.diff of course :-)

<a href="http://bugzilla.opendarwin.org/show_bug.cgi?id=10467">Bug 10467 - platform-qt.diff</a>

Have fun, Niko
<!--break-->