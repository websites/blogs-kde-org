---
title:   "The MacBook Saga"
date:    2006-04-10
authors:
  - wildfox
slug:    macbook-saga
---
The MacBook Pro Saga finally come to a good end :-)
Rob & me patiently waited for the mbp (MacBook Pro),
and last friday it really arrived. I was very happy because
after just 5 minutes it was up & running, and as a Apple-newbie
I was just amazed by the osx coolness / smoothness and the
way the laptop had to be setup: open box & plug in power &
start laptop & choose language -> done. Just great.

So after playing with it for about two hours, I left the house,
returned after another hour, went to my mbp -- THE SHOCK.
The keyboard & trackpad just stopped working. Using an USB
mouse & keyboard I could control the system again. I decided
to move my mbp physically around my house, and downstairs
it suddenly worked again. Weird, weird. Back upstairs in my room
it stopped working, so I got the feeling my mbp is broken.

So I decided to contact Darin Adler to see how we can "fix" my problem.
He answered quickly stating I should just contact a local mac dealer,
as I'm within the 90 days of free support. On Saturday I found a mac
dealer in Cologne (~ 30km away from my home town) called 'Gravis'.

On the phone they told me to bring the mbp & the commercial invoice.
Well - as I got it for free - that was a bit problematic :) I showed them
my "Gift letter" & the mail package order - the guy who dealed with my
request was pretty surprised about that, asked his boss and then we had
a nice talk about WebKit & SVG & OpenSource & KDE. Pretty nice to see
people already like/use our SVG stuff in WebKit :-)

After 15 mins all was done, and I was told that they'll hurry up to fix
my mbp. Today it was already done and it REALLY was a 'loose connection'
between the logic board and the keyboard/trackpad. I was really surprised
how fast they handled my repair request. Thanks again Gravis team!

Now the MacBook Saga ended, and I'm very very very happy with it :-)
My first laptop ever, even for free - is there any better easter present
for a young obsessive geek? I don't think so :-)

Gettin' Linux to run in parallel is the next task though, as you may have guessed....
Anyway, thanks go out to Apple again for the nice laptop!

All the patient readers, who are still with me: university starts again
in ~9 hours, so I wish you & me a good night...