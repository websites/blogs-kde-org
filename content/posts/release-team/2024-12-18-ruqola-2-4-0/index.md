---
title: Ruqola 2.4.0
categories:
 - Release
authors:
  - release-team
date: 2024-12-18
---

Ruqola 2.4.0 is a feature and bugfix release of the Rocket.chat messenger app.

Some of the new features in this release of Ruqola include:
- Allow to clean up room history when room was not opened for a long time.
- Add restore button in administrator server settings dialog.
- Improve changing password (show validation info).
- Improve register new account (Add reason support).
- Implement mute/unmute user.
- Add color to the text in the account tab.
- Allow to show private installed applications.


Some bug fixing:
- Fix editing message.
- Show permissions in Rocket.Chat Marketplace.
- Fix reconnect server.
- Fix single application on Windows/MacOs
- Fix select created room/discussion/teams
- Fix filter discussion in administrator mode
- Fix message video support
- Fix highlight text in quoted message
- Fix open discussion channel
- Allow to show application market settings

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.4.0.tar.xz  
**SHA256:** `f532e421ae731dfc2e88b78ab61de01e0e367a31a4fe34497664a66fc737225c`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
