---
title: Plasma Wayland Protocols 1.16.0
categories:
 - Release
authors:
  - release-team
date: 2025-01-09
---

Plasma Wayland Protocols 1.16.0 is now available for packaging.  It is needed for the forthcoming Plasma 6.3.

**URL:** [https://download.kde.org/stable/plasma-wayland-protocols/](https://download.kde.org/stable/plasma-wayland-protocols/) <br />
**SHA256:** `da3fbbe3fa5603f9dc9aabe948a6fc8c3b451edd1958138628e96c83649c1f16`
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` [Jonathan Riddell <jr@jriddell.org>](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/jriddell@key1.asc?ref_type=heads)

Full changelog:

- external-brightness: Allow the client to specify observed brightness
- output management: add a failure reason event
- output device,-management: add a dimming multiplier
- output device/management: add power/performance vs. color accuracy preference
