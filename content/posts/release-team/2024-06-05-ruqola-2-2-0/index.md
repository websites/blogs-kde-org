---
title: Ruqola 2.2.0
categories:
 - Release
authors:
  - release-team
date: 2024-06-05
---

Ruqola 2.2.0 is a feature and bugfix release of the Rocket.chat app.

Improvements:
- Allow to increase/decrease font (CTRL++/CTRL+-)
- Add channel list style (Condensed/Medium/Extended)
- Add forward message
- Improve mentions support. 
- Add support for deep linking Deep Linking.
- Implement block actions.
- Implement personal token authentication. Bug 481400 
- Add Plasma Activities Support 
- Add Report User Support 
- Implement Channel Sound Notification. 
- Implement New Room Sound Notification.
- Implement Sorted/Unsorted markdown list.

Some bug fixing:
- Fix dark mode support.
- Fix jitsi support.Fix translate message in direct channel.
- Don't show @here/@all as user.
- Reduce memory footprint.
- Use RESTAPI for logging.
- Allow to send multi files.
- Fix preview url.

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.2.0.tar.xz  
**SHA256:** `4091126316ab0cd2d4a131facd3cd8fc8c659f348103b852db8b6d1fd4f164e2`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Esk-Riddell <jr@jriddell.org>  
https://jriddell.org/esk-riddell.gpg  
