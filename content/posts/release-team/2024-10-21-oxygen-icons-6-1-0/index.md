---
title: Oxygen Icons 6.1.0
categories:
 - Release
authors:
  - release-team
date: 2024-10-21
---

Oxygen Icons 6.1.0 is a feature release of the Oxygen Icon set.

It features new SVG symbolic icons for use in Plasma Applets.

It also features improved icon theme compliance, fixed visability and added mimetype links.

![](oxygen-6.1.0.png)

**URL:** https://download.kde.org/stable/oxygen-icons/  
**Source:** oxygen-icons-6.1.0.tar.xz  
**SHA256:** `16ca971079c5067c4507cabf1b619dc87dd6b326fd5c2dd9f5d43810f2174d68`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
