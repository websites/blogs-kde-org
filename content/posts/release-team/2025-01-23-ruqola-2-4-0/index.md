---
title: Ruqola 2.4.1
categories:
 - Release
authors:
  - release-team
date: 2025-01-23
---

Ruqola 2.4.1 is a feature and bugfix release of the Rocket.chat messenger app.

Ruqola 2.4.1                                                                                                                
------------                                                                                                                
- Fix typing support (new API)
- Exclude string starting with /* or // as Rocket chat command (avoid error)
- Don't copy text when preview is hidden
- Fix Market apps support
- Fix search user (Allow to use '@') when inviting users in Room
- Inform when we don't have database history for a specific Room
- Fix clicking in url in text in preview url
- Don't allow to create two tokens with same name
- Don't show clear button in lineedit when it's readonly

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.4.1.tar.xz  
**SHA256:** `e5adb0806e12b4ce44b55434256139656546db9f5b8d78ccafae07db0ce70570`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
