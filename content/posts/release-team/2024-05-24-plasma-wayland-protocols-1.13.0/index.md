---
title: Plasma Wayland Protocols 1.13.0
categories:
 - Release
authors:
  - release-team
date: 2024-05-24
---

Plasma Wayland Protocls 1.13.0 is now available for packaging.

This adds features needed for the Plasma 6.1 beta.

**URL:** [https://download.kde.org/stable/plasma-wayland-protocols/](https://download.kde.org/stable/plasma-wayland-protocols/) <br />
**SHA256:** `dd477e352f5ff6e6ac686286c4b22b19bf5a4921b85ee5a7da02bb7aa115d57e`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` [Jonathan Esk-Riddell <jr@jriddell.org>](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/jriddell@key1.asc?ref_type=heads)

Full changelog:

- plasma-window-management: add a stacking order object
- output device, output management: add brightness setting
- outputdevice,outputconfiguration: add a way to use the EDID-provided color profile
- Enforce passing tests
- output device, management: change the descriptions for sdr gamut wideness
