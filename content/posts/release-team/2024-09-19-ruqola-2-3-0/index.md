---
title: Ruqola 2.3.0
categories:
 - Release
authors:
  - release-team
date: 2024-09-19
---

Ruqola 2.3.0 is a feature and bugfix release of the Rocket.chat app.

New features:
- Implement Rocket.Chat Marketplace.
- Allow to clean room history.
- Allow to check new version.
- Implement moderation (administrator mode).
- Add welcome page.
- Implement pending users info (administrator mode).
- Use cmark-rc (https://github.com/dfaure/cmark-rc) for markdown support.
- Delete oldest files from some cache directories (file-upload and media) so it doesn't grow forever.

Fixed bugs:
- Clean market application model after 30 minutes (reduce memory footprint).
- Fix show discussion name in completion.
- Fix duplicated messages in search message dialog.
- Add delegate in search rooms in team dialog.

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.3.0.tar.xz  
**SHA256:** `051186793b7edc4fb2151c80ceab3bcfd65acb27d38305568fda54553660fdd0`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
