---
title: Massif Visualizer 0.8.0 released
categories:
 - Release
authors:
  - release-team
date: 2024-05-08
---

Massif Visualizer 0.8.0 has been just released! The main focus of this release is the port to Qt6 and KDE Frameworks 6 as well as general code modernisation, but of course some bugs have been squashed too. The full changelog can be found below.

About Massif Visualizer:

Massif Visualizer is a tool that - who'd guess that - visualizes massif data. You run your application in Valgrind with --tool=massif and then open the generated file in the visualizer. Gzip or Bzip2 compressed massif files can also be opened transparently.

You can learn more at [https://apps.kde.org/massif-visualizer/](https://apps.kde.org/massif-visualizer/)

**URL:** [https://download.kde.org/stable/massif-visualizer/0.8.0/src/](https://download.kde.org/stable/massif-visualizer/0.8.0/src/) <br />
**SHA256:** `5fad3f0e0d9fbb6bc8cfb744cb4e2c99f231d57ee0dd66dd594d36c2cc588a80`  
**Signed by:** `D81C 0CB3 8EB7 25EF 6691  C385 BB46 3350 D6EF 31EF` Heiko Becker <heiko.becker@kde.org><br/>
[https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/heikobecker@key1.asc](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/heikobecker@key1.asc)

Full changelog:
* appdata: Add upcoming 0.8.0 release
* Unbreak KDE CI config: require &quot;@stable-kf6&quot; branch of kgraphviewer
* KDE CI: require tests to pass (for platforms where they currently do)
* KDE CI config: require &quot;@same&quot; branch of kgraphviewer
* appstream: use new id without hyphen
* Port away from QScopedPointer
* ParseWorker: fix switched error title &amp; text for empty data file
* Overhaul action &amp; title texts to follow KDE HIG, add more UI marker contexts
* Config dialog: align &quot;Shorten Templates&quot; checkbox with form fields
* Config dialog: avoid full-width comboboxes, use system style
* Update outdated Kate editor setting replace-trailing-space-save
* Use more function-pointer-based Qt signal/slot connections
* Remove some unused includes
* Use KStandardAction (member-)function-pointer-based overloads
* Use ECM-requirement-derived default KDE_COMPILERSETTINGS_LEVEL
* Drop any margins around document &amp; tool views
* Use QList directly instead of Qt6-times alias QVector
* Switch from target_link_libraries&#39; legacy LINK_PRIVATE to PRIVATE
* Use more target-centric cmake code
* CMake: remove unneeded explicit addition of current dirs to include dirs
* Fix build with older cmake: -D not expected with COMPILE_DEFINITIONS
* Update homepage to apps.kde.org
* Use commits.kde.org/kgraphviewer as source location for KGraphViewerPart
* Set version to 0.8.0
* Use CMake&#39;s project(VERSION)
* Drop support for Qt5/KF5
* Support Qt6/KF6 builds
* Rely on CMake&#39;s autorcc (enabled by KDECMakeSettings) to add Qt resources
* Use ECMDeprecationSettings
* Use KF6-proof KConfigGroup::group() overload
* Port away from deprecated KPluginFactory::create() overload
* Port away from deprecated KPluginLoader::factory()
* Port away from deprecated KFilterDev
* Port away from deprecated QPrinter::pageRect()
* Port away from deprecated QDesktopWidget
* Port away from deprecated QString::split overload
* Port away from deprecated QPixmap::grabWidget
* Port away from deprecated QTreeView::sortByColumn overload
* Port away from ModelTest copy to QAbstractItemModelTester
* Adapt iterator type to match actual type returned from QHash method
* Add explicit QRegExp includes
* Port away from deprecated QAlgorithmsPrivate::qReverse
* Port away from deprecated qSort
* Bump min required CMake/Qt/ECM/KF to 3.16/5.15.2/5.100/5.100
* appstream: use https for screenshot links
* appstream: use desktop-application type, add developer &amp; launchable tags
* Appdata: Add developer name
* [CI/CD] Add flatpak job
* [CI] Don&#39;t depend on kgraphviewer on Windows
* Port to new CI template syntax
* snapcraft: initial import snapcraft files.
* Deploy ui.rc files as Qt resource
* Move Flatpak CI to GitLab
* Add explicit moc includes to sources for moc-covered headers
* Use non-deprecated KDEInstallDirs variables
* Install translations
* Port away from deprecated KMessageBox::sorry
* Remove arcconfig
* Remove unused include
* Use imported target for KGraphViewerPart
* debug
* Add Gitlab CI
* Remove unused XmlPatterns
* Add some missing linkages
* Use KDE_INSTALL_MIMEDIR instead of custom XDG_MIME_INSTALL_DIR
* appdata.xml: Minor fixes for submission to Flathub
* Fix minor issues found by EBN
* fix xml
* update screenshot
* Set StartupWMClass in desktop file
* ui.rc files: use &lt;gui&gt; instead of deprecated &lt;kpartgui&gt;
* Do not duplicate work done by KAboutData::setupCommandLine()
* Use nullptr
* Use override
* Fix window icon for non-desktopfile WM to own icon &quot;massif-visualizer&quot;
* Properly support BUILD_TESTING
* Remove explicit enable_testing(), duplicated from KDECMakeSettings
* Bump min cmake version to 3.0
* Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH
* Fix minor EBN issues

