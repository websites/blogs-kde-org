---
title: Plasma Wayland Protocols 1.15.0
categories:
 - Release
authors:
  - release-team
date: 2024-12-07
---

Plasma Wayland Protocols 1.15.0 is now available for packaging.  It is needed for the forthcoming KDE Frameworks.

**URL:** [https://download.kde.org/stable/plasma-wayland-protocols/](https://download.kde.org/stable/plasma-wayland-protocols/) <br />
**SHA256:** `e5aedfe7c0b2443aa67882b4792d08814570e00dd82f719a35c922a0993f621e`
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` [Jonathan Riddell <jr@jriddell.org>](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/jriddell@key1.asc?ref_type=heads)

Full changelog:

- Add a request to create a virtual output stream with description
- Add alpine CI
- Add modifier information to keystate
- gitignore: use same as KWin
- Add a destructor to appmenu manager
- Add protocol tests
- Add CI for static builds on Linux
