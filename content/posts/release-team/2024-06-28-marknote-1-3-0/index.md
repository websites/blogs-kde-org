---
title: Marknote 1.3
date: "2024-06-28T9:00:00Z"
categories:
 - Release
authors:
  - mbruchert
SPDX-License-Identifier: CC-BY-SA-4.0
---

It's been almost two months since the release of Marknote 1.2. Marknote is a
rich text editor and note management tool using Markdown. Since the release a
lot has changed and many new features have been added thanks to the work of all
contributors!

## User Interface

On small phone screens, you want as much space as possible to write and read
your notes. Marknote now lets you hide the editor toolbar on mobile, so you can
focus on the important stuff while having the formatting options just a tap
away.

<video class="img-fluid" muted="true" loop="true" autoplay>
  <source src="mobile.mp4" type="video/mp4">
</video>

On desktop, you can now adjust the width of the note list and editor by
dragging the separator, just like in other desktop applications.

<video class="img-fluid" muted="true" loop="true" autoplay>
  <source src="resize.mp4" type="video/mp4">
</video>

You can now undo and redo changes using buttons in the UI, rather than relying
on keyboard shortcuts, which is especially helpful if you're using a virtual
keyboard that may not have a control key.

The application menu has been reorganized and simplified to make it less
cluttered.

Carl's work on responsive images has finally landed in Qt and now also in
Marknote. This means that images no longer get cropped on small screens.

Volker implemented clickable links! You can now ctrl-click links to open them.

## Switching to Marknote

To make it easier to switch from your current notes application to Marknote,
Marknote now offers several import options. for now you can import your notes
from KNotes and maildir.

## Preferences

The settings now longer open in an overlay dialog, and are now shown in
Kirigami Addons' new ConfigurationView.

Thanks to Garry Wang, you can now change the color theme directly from
Marknote's preferences.

![New setting dialog](settings.png)

## Bug fixes

- The heading of the note list no longer overlaps the buttons if it gets too
  long, now it is hidden.
- The list of notes now overlaps the header longer when scrolling.
- Opening a note on a touch device no longer opens the options menu every time.
- Thanks to Carl, file creation on Windows has been fixed so you can use the
  sketch feature there, too.

## Get It

Marknote is available on [Flathub](https://flathub.org/apps/org.kde.marknote).

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/marknote/marknote-1.3.0.tar.xz.mirrorlist)
and it has been signed with my [Carl's GPG key](https://carlschwan.eu/gpg-02325448204e452a/).
