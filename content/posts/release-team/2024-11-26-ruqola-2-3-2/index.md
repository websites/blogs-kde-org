---
title: Ruqola 2.3.2
categories:
 - Release
authors:
  - release-team
date: 2024-11-26
---

Ruqola 2.3.2 is a feature and bugfix release of the Rocket.chat app.

It includes many fixes for RocketChat 7.0.

New features:
  - Fix administrator refresh user list
  - Fix menu when we select video conference message
  - Fix RocketChat 7.0 server support
  - Fix create video message
  - Fix update cache when we change video/attachment description
  - Fix export message job
  - Fix show userOffline when we have a group
  - Fix enable/disable ok button when search room in team dialog
  - Fix crash when we remove room in team dialog
  - Fix update channel selection when we reconnect server

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.3.2.tar.xz  
**SHA256:** `57c8ff6fdeb4aba286425a1bc915db98ff786544a3ada9dec39056ca4b587837`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
