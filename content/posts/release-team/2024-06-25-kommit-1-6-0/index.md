---
title: Kommit 1.6.0
categories:
 - Release
authors:
  - release-team
date: 2024-06-25
---

Kommit 1.6.0 is a feature and bugfix release of our Git repo app which now builds with Q 5 or 6.

Improvements:
- build without kdbusaddons on windows
- Add flatpak support
- Fix show date (using QLocale for it)
- Fix mem leak
- Reactivate open file in external apps in qt6
- Add zoom support in Report Chart Widget
- Replace a QTableWidget by QTreeWidget in report page
- Fix crash when we didn't open git repository
- Fix load style/icon on windows (KF >= 6.3)
- Implement a gravatar cache
- Fix i18n

**URL:** https://download.kde.org/stable/kommit  
**Source:** kommit-1.6.0.tar.xz  
**SHA256:** `4091126316ab0cd2d4a131facd3cd8fc8c659f348103b852db8b6d1fd4f164e2`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Esk-Riddell <jr@jriddell.org>  
https://jriddell.org/esk-riddell.gpg  
