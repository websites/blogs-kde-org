---
title: Ruqola 2.3.1
categories:
 - Release
authors:
  - release-team
date: 2024-10-29
---

Ruqola 2.3.1 is a feature and bugfix release of the Rocket.chat app.

New features:
- Use "view-conversation-balloon-symbolic" icon when we have private conversation with multi users                                                                                                                                                          
- Add version in market application information                                                                                                                                                                                                             
- Fix reset password                                                                                                                                                                                                                                        
- Fix mouse position when QT_SCREEN_SCALE_FACTORS != 1                                                                                                                                                                                                      
- Add missing icons                                                                                                                                                                                                                                         
- Fix create topic when creating teams                                                                                                                                                                                                                      
- Fix discussion count information                                                                                                                                                                                                                          
- Remove @ or # when we search user/channel                                                                                                                                                                                                                 
- Fix edit message logic

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.3.1.tar.xz  
**SHA256:** `99356ec689473cd5bfaca7f8db79ed5978efa8b3427577ba7b35c1b3714d5fcb`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Riddell <jr@jriddell.org>  
https://jriddell.org/jriddell.pgp  
