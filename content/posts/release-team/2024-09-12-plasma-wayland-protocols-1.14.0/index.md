---
title: Plasma Wayland Protocols 1.14.0
categories:
 - Release
authors:
  - release-team
date: 2024-09-12
---

Plasma Wayland Protocols 1.14.0 is now available for packaging.

This adds features needed for the Plasma 6.2 beta.

**URL:** [https://download.kde.org/stable/plasma-wayland-protocols/](https://download.kde.org/stable/plasma-wayland-protocols/) <br />
**SHA256:** `1a4385ecfc79f7589f07381cab11c3ff51f6e2fa4b73b78600d6ad096394bf81`
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` [Jonathan Riddell <jr@jriddell.org>](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/jriddell@key1.asc?ref_type=heads)

Full changelog:

- add a protocol for externally controlled display brightness
- output device: add support for brightness in SDR mode
- plasma-window: add client geometry + bump to v18
- Add warnings discouraging third party clients using internal desktop environment protocols
