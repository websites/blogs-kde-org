---
title: KUnifiedPush 1.0.0 is out!
date: 2024-10-19
categories:
 - Release
authors:
  - release-team
  - carlschwan
discourse: carl
---

KUnifiedPush provides push notifications for KDE applications. Push
notifications are a mechanism to support applications that occasionally need to
receive some kind of information from their server-side part, and where
receiving in a timely manner matters. Chat applications or weather and
emergency alerts would be examples for that. More technical details about
KUnifiedPush are available on [Volker's introduction post about KUnifiedPush](https://www.volkerkrause.eu/2022/11/12/kde-unifiedpush-push-notifications.html).

KUnifiedPush provides three possible provider backends for push notifications:

- [Ntfy](https://ntfy.sh/)
- [Nextpush](https://codeberg.org/NextPush/uppush)
- [Gotify](https://gotify.net/)

The default provider of KUnifiedPush is Ntfy with
[unifiedpush.kde.org](https://unifiedpush.kde.org/) but you can change it to
your own server in the System Settings.

![Screenshot of the settings showing the default settings](settings.png)

Currently both [NeoChat](htpps://apps.kde.org/neochat) and
[Tokodon](https://apps.kde.org/tokodon) integrates with KUnifiedPush as both
Matrix and Mastodon support UnifiedPush. There is also ongoing work for
[weather and emergency alerts](https://invent.kde.org/vkrause/kpublicalerts).

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/kunifiedpush/kunifiedpush-1.0.0.tar.xz.mirrorlist)
and it has been signed with [Carl Schwan's GPG key](https://carlschwan.eu/gpg-02325448204e452a/).
