---
title: KTextAddons 1.5.4
categories:
 - Release
authors:
  - release-team
date: 2024-03-27
---

KTextAddons 1.5.4 is a bugfix release of our text display and handling library

It fixes two notable bugs and updates translations

- Fix bug 484328: Kmail config dialog takes about 40 seconds to show up
- Use QListView::clicked (in emoji popup menu)

**URL:** https://download.kde.org/stable/ktextaddons/  
**Source:** ktextaddons-1.5.4.tar.xz  
**SHA256:** `64b80602e84b25e9164620af3f6341fa865b85e826ab8f5e02061ae24a277b20`  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Esk-Riddell <jr@jriddell.org>
https://jriddell.org/esk-riddell.gpg
