---
title: KGraphViewer 2.5.0 released
categories:
 - Release
authors:
  - release-team
date: 2024-05-08
---

KGraphViewer 2.5.0 has been just released! The main focus of this release is the port to Qt6 and KDE Frameworks 6 as well as general code modernisation, but of course some bugs have been squashed too. The full changelog can be found below.

About KGraphViewer:

KGraphViewer is a Graphviz DOT graph file viewer, aimed to replace the other outdated Graphviz tools. Graphs are commonly used in scientific domains and particularly in computer science.

You can learn more at [https://apps.kde.org/kgraphviewer/](https://apps.kde.org/kgraphviewer/)

**URL:** [https://download.kde.org/stable/kgraphviewer/2.5.0/](https://download.kde.org/stable/kgraphviewer/2.5.0/) <br />
**SHA256:** `872bee63fb4df6f7fb2b4eaf02ff825cba3ca953ac02509a287fe5cd0f1e2b69`  
**Signed by:** `D81C 0CB3 8EB7 25EF 6691  C385 BB46 3350 D6EF 31EF` Heiko Becker <heiko.becker@kde.org><br/>
[https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/heikobecker@key1.asc](https://invent.kde.org/sysadmin/release-keyring/-/raw/master/keys/heikobecker@key1.asc)

Full changelog:
* appstream: Add upcoming 2.5.0 release
* Brush up menu &amp; action terms a bit
* Add icons to more actions &amp; submenus
* Update homepage URL in README
* Remove some outdated/unused files
* Avoid double look-ups in maps, use iterator returned from find method
* Add widget parent to QMenu instances
* DotGraphView: create popup menu only on demand
* Share also zoom actions between DotGraphView &amp; KGraphViewerPart
* Use enum QColor constructor instead of string based one
* Use KStandardAction convenience creation methods, parent all to collecitions
* Drop file_open_recent from ui.rc files, given KStandardAction toolbar magic
* Use more member-function-pointer-based Qt signal/slot connects
* Port away from auto-casting from ascii strings
* Fix missing closing tags in D-Bus API xml files
* Use QList directly instead of Qt6-times alias QVector
* Make manual build &amp; install fully optional
* Update links to graphviz website
* Fix handling file cmdl arguments with relative path
* Fix bad defaults for fonts, also for colors, shapes &amp; style
* KGraphViewerPart CMake config file: drop KGraphViewerPart_INCLUDE_DIRS
* Bump version &amp; SO version for first Qt6-based release
* Drop support for Qt5/KF5
* Clean up includes &amp; forward declares
* Do not use Qt modules includes
* Deploy custom pixmaps as Qt resource
* Printing page settings: remove custom broken window icon
* Printing page settings: replace &quot;lock ratio&quot; button with checkbox
* KGraphViewer KPart metadata: use normal app display name as name
* Drop libkgraphviewer appstream file, no other libraries provide some
* Set target properties right after declaring the target
* Remove unused version header includes
* Drop code for no longer supported KF versions
* Fix another wrong min Qt version variable name usage
* Use ECMDeprecationSettings
* Port away from deprecated QMouseEvent::globalPos()
* KGraphEditor: fix bad port to QMessageBox::question
* Use Q_EMIT instead of emit
* Switch to ECM required-default KDE_COMPILERSETTINGS_LEVEL
* Remove unneeded ; after Q_DECLARE_PRIVATE() &amp; Q_DECLARE_PUBLIC()
* Use more nullptr
* Fix wrong min Qt version variable name usage
* Add Qt6/KF6 build support
* Remove unneeded QApp::setOrganizationDomain, dupl. KAboutData::setApp...Data
* appdate: use desktop-application type, add developer &amp; launchable tags
* Update homepage to apps.kde.org
* Port away from deprecated QDesktopWidget
* Port away from deprecated QPrinter::setOrientation()
* Port away from deprecated QPrinter::numCopies()
* Port away from deprecated operator+(Qt::Modifier, Qt::Key)
* Port away from deprecated QWheelEvent::delta()/orientation()
* Port away from deprecated signal QButtonGroup::buttonClicked(int)
* Port away from deprecated I18N_NOOP2
* Port away from deprecated KXmlGui RESTORE() macro
* Bump min required Qt/KF to 5.15.2/5.100.0
* Port away from deprecated QLayout::setMargin()
* Add missing includes for Qt6 build
* Remove unused include
* Drop usage of outdated no-effect QGraphicsView::DontClipPainter
* Port away from deprecated QStyle::PM_DefaultLayoutSpacing
* change QFontMetrics.width with horizontalAdvance
* replace QRegExp by QRegularExpression
* Use for instead of foreach
* Replace deprecated endl with Qt variant
* remove -qt5 suffix
* change path in gitlab-ci
* snapcraft: initial import snapcraft files.
* kgrapheditor: deploy ui.rc file as Qt resource
* Remove Designer&#39;s &quot;.&quot; normaloff file data from icon properties in .ui files
* Add explicit moc includes to sources for moc-covered headers
* doc: use a non-deprecated entity for Frameworks
* Add releases
* Add Open Age Content Rating
* Remove warning about unknown DOT fonts
* Remove custom action to configure shortcuts
* Init graph members
* Remove unused graphviz/gvc.h includes
* Add KI18n and KDocTools macro to install translations
* Port away from deprecated KMessageBox Yes/No API
* Remove arcconfig
* Add interface library for part include dir
* Handle Qt6 change around enterEvent
* Add missing include
* Remove unused include
* Port away from deprecated KPluginLoader
* Port away from deprecated endl
* Adapt build system to Qt6
* Remove pointless/broken icons
* Enable highdpi pixmaps
* Add git blame ignore file
* Add GitLab CI
* Use ecm_qt_install_logging_categories to generate kdebugsettings file
* Drop code variants for no longer supported Qt/KF versions
* Use more target-centric cmake code
* Update .gitignore
* Use non-deprecated KDEInstallDirs variables
* Port away from deprecated KSelectAction::triggered(QString)
* Documentation updates - Update date and fix version numbers - Fix tagging and sync option&#39;s text
* Port away from deprecated QMatrix
* KAboutData::setupCommandLine() already adds help &amp; version options
* kgraphviewer_part.desktop: remove unused key Categories
* Use .in suffix for header file that is passed to configure_file()
* Port away from deprecated signal QProcess::error
* Use default window flags for QInputDialog::getText
* Port away from deprecated KEditToolBar::newToolbarConfig
* Port to new KPluginMetaData-based KParts API
* Deploy ui.rc files as Qt resources
* Remove broken argument SERVICE_TYPES kpart.desktop from desktop2json call
* Handle Graphviz capitalization changes
* Capitalize Graphviz consistently
* cmake: Simplify and improve FindGraphviz.cmake
* Add KDE ClangFormat on CMake and run the target
* Fix link: ui.html -&gt; menus.html
* Draw empty arrowheads closed
* add genericname for use on kde.org/applications
* Set StartupWMClass in desktop file
* Use more https in links (and update outdated ones)

