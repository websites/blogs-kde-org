---
title:   "Blogging support for Kontact"
date:    2007-07-19
authors:
  - mikearthur
slug:    blogging-support-kontact
---
Hi, I'm a Google Summer Of Code student and I'm working on creating a kresource to add support to add a journal and retrieve them from a blog.

You can monitor my progress on the kresourrce in trunk/KDE/kdepim/kresources/blog/.

The next stage of my project is revamping the journal support in Kontact/Korganizer. This will not be completed by the end of the summer but I plan to keep working on it regardless.

If you have an ideas or input on either of these parts of my project then send me an email at mike@mikearthur.co.uk or talk to me on #kontact.