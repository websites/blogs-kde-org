---
title:   "Who is paid to hack Kde?"
date:    2003-07-21
authors:
  - dkite
slug:    who-paid-hack-kde
---
I am planning on doing a feature which will list all the Kde developers who are paid by someone.

Here is an incomplete list.

Waldo Bastian by Suse
Zack Rusin by Automatix  ( there was someone else too)
David Faure by Trolltech
Andras Mantia by Eric Laffoon
Bo Thorsen by Klaralvdalens Datakonsult AB 

Any others you know of, or would like to add or correct?

Adding to the list:

 Arend van Beelen jr.  by Auton Rijnsburg
Holger Schroeder   Automatix
 Laurent Montel  Mandrake

Cornelius Schumacher , Stephan Kulow, Michael Matz, 
Lubos Lunak, Lukas Tinkl and Everaldo Coelho by Suse.

Derek