---
title:   "How to highlight work in progress?"
date:    2004-03-07
authors:
  - dkite
slug:    how-highlight-work-progress
---
A common complaint I get from developers is that their work isn't showing up in the digest.

I'll explain how the whole thing is done, and then get to the question. I go through the 2000 or so commit emails from kde-cvs list, and select commits that are either bugfixes, new features, optimizations or security fixes. With bugfixes, any that have a bug number are selected. I then run a script that builds the html, statistics and other stuff, edit, add a few things here and there, then publish. This works reasonably well, although there are imo too many trivial bugfixes highlighted.

The format of the digest isn't conducive to highlighting work in progress. For a mature application, most of the commits are either maintenance/janitorial type fixes, or patches that introduce new features. Like KMail getting spam wizards. There was a patch inserting the basic functions, followed by a bunch that finished it up. Most of the time this work is highlighted appropriately. But when someone is working on building an application, they may have 15-20 patches that build the class hierarchy, some ui stuff, etc. This represents hours of important work, but no one or two patches give a decent idea of what is happening. The complaints usually arise from this situation.

So how would be a good way of representing this work? I encourage developers to email me summaries of their work. Any other ideas?

I am currently rewriting the scripts in php, and am close to being ready for some testing. My goal is to first match what we have now, move everything over to the new stuff, then add any neat stuff that comes to mind. The whole thing will still be based on selected commits.

Derek