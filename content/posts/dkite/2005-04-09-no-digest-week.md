---
title:   "No Digest this week"
date:    2005-04-09
authors:
  - dkite
slug:    no-digest-week
---
I'm unable to rsync with the cvs repository, so no digest is possible. It seems everything is in flux until the subversion changeover is done.

Hopefully all will be back to normal next week.