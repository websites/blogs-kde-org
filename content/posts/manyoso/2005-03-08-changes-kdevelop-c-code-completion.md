---
title:   "Changes to kdevelop C++ code completion"
date:    2005-03-08
authors:
  - manyoso
slug:    changes-kdevelop-c-code-completion
---
Matt and I are doing a little retouch of KDevelop's code completion.  First, on Sunday, Matt made a change which prompts KDevelop to complete much more often than in the past.  Before, KDevelop would only complete when it received a textChanged signal from KTextEditor and the current chars were "." or "->" or "::" ...  Now, completions are computed for all chars that are not whitespace or empty.

Second, I just finished another tweak in how KDevelop handles c++ completion.  In the past, KDevelop would not offer completions for methods in class declarations.  I added completion for virtual method declarations.  The completion is now queued with the 'virtual' keyword and offers a completion pop-up of all inherited virtual methods.  When one is selected it inserts the entire declaration.

Overall it works pretty good, but there are a few bugs that present inopportune completions, but hopefully we can get those fixed soon.  Make sure to check out the lastest bit and report any bugs you may find :)
<!--break-->
