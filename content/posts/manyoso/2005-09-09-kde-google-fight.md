---
title:   "KDE Google Fight!"
date:    2005-09-09
authors:
  - manyoso
slug:    kde-google-fight
---
I was playing with <a href="http://googlefight.com/">googlefight</a> today when it occurred to me to run a few KDE names through the gauntlet...  Amused, I decided to use <a href="http://www.google.com/apis/">Google's web API</a> to run the list of <a href="http://www.kde.org/areas/kde-ev/members.php">kde-ev member names</a> and produce the first KDE-Google-Fight-Royal-Rumble :=)

Methodology:

<ol>
<li>Each name is taken verbatim from the kde-ev member list and properly quoted.  ie, can't complain if you have a complicated name or used your initials ;) 
<li>The parameters for the search were: ( start=0, maxResults=10, filter=false, restrict="linux", safeSearch=false, lr="", ie="", oe="" )
<li>Explanations for the parameters can be found <a href="http://www.google.com/apis/reference.html#2_1">here.</a>
<li>The results are estimated and not exact.
</ol>

So, who reigns supreme?  Who's names are splashed all over the KDE web?  The KDE Google Fight World Champion is?

<ol>
<li><b>"Matthias Ettrich"</b>  11800
<li>"Bernhard Rosenkraenzer"    9050
<li>"Aaron Seigo"   2040
<li>"Boudewijn Rempt"   1570
<li>"Roberto Alsina"    1560
<li>"Thiago Macieira"   1400
<li>"Kurt Pfeifle"  1240
<li>"Stephan Kulow" 1000
<li>"Jonathan Riddell"  1000
<li>"David Faure"   1000
</ol>

Read on to see the full list...

<b>UPDATE:</b> If your name isn't on the list you can use something similar to this:
http://www.google.com/search?maxResults=10&filter=false&lr=&ie=&oe=&restrict=linux&safeSearch=false&start=0&q=%22Michael+Pyne%22

Just change 'Michael Pyne' to whatever name you want to check... ;)

<!--break-->

<ol>
<li>"Matthias Ettrich"  11800
<li>"Bernhard Rosenkraenzer"    9050
<li>"Aaron Seigo"   2040
<li>"Boudewijn Rempt"   1570
<li>"Roberto Alsina"    1560
<li>"Thiago Macieira"   1400
<li>"Kurt Pfeifle"  1240
<li>"Stephan Kulow" 1000
<li>"Jonathan Riddell"  1000
<li>"David Faure"   1000
<li>"Torsten Rahn"  1000
<li>"Marc Mutz" 1000
<li>"Zack Rusin"    999
<li>"Helio Chissini de Castro"  994
<li>"Nadeem Hasan"  985
<li>"Michael Matz"  951
<li>"Michael Renner"    933
<li>"Stefan Westerfeld" 867
<li>"Richard Smith" 832
<li>"Lubos Lunak"   802
<li>"Martin Konold" 781
<li>"Chris Howells" 758
<li>"Matthias Welwarsky"    758
<li>"Lars Knoll"    751
<li>"Lauri Watts"   736
<li>"Adrian Schroeter"  719
<li>"Konrad Rosenbaum"  704
<li>"Matthias Kalle Dalheimer"  689
<li>"Jesper Pedersen"   686
<li>"Daniel Molkentin"  684
<li>"Lukas Tinkl"   666
<li>"George Staikos"    629
<li>"Chris Lee" 616
<li>"Eric Laffoon"  610
<li>"Kevin Ottens"  596
<li>"Scott Wheeler" 569
<li>"Waldo Bastian" 564
<li>"Matt Rogers"   561
<li>"Ralf Nolden"   539
<li>"Dirk Mueller"  529
<li>"Richard J. Moore"  514
<li>"Josef Spillner"    492
<li>"Allan Sandfeld"    484
<li>"Oswald Buddenhagen"    451
<li>"Fabrice Mous"  426
<li>"Michael Goffioul"  416
<li>"Alexander Kellett" 403
<li>"Will Stephenson"   373
<li>"Hans Petter Bieker"    351
<li>"Andreas Pour"  346
<li>"Martijn Klingens"  345
<li>"Andras Mantia" 333
<li>"Reinhold Kainhofer"    327
<li>"Harri Porten"  325
<li>"Adam Treat"    318
<li>"Charles Samuels"   313
<li>"Thomas Diehl"  309
<li>"Navindra Umanee"   309
<li>"Holger Freyther"   296
<li>"Eva Brucherseifer" 282
<li>"Matthias Kretz"    266
<li>"Simon Hausmann"    260
<li>"Adriaan de Groot"  258
<li>"Ian Reinhart Geiser"   258
<li>"Stephan Binner"    251
<li>"Michael Brade" 246
<li>"Till Adam" 243
<li>"Carsten Niehaus"   241
<li>"Carsten Pfeiffer"  233
<li>"Cornelius Schumacher"  231
<li>"Rainer Endres" 224
<li>"Harald Fernengel"  224
<li>"Matthias Elter"    221
<li>"Olaf Schmidt"  216
<li>"Joseph Wenninger"  211
<li>"Frerich Raabe" 210
<li>"Thomas Zander" 208
<li>"Jes Hall"  205
<li>"Tobias Koenig" 204
<li>"Gunnar Schmidt"    203
<li>"Cristian Tibirna"  201
<li>"Ken Wimer" 199
<li>"Christoph Cullmann"    195
<li>"Sandy Meier"   194
<li>"Mark Bucciarelli"  190
<li>"Duncan Mac-Vicar"  188
<li>"Albert Astals Cid" 182
<li>"Torben Weis"   165
<li>"John Tapsell"  158
<li>"Matthias Hoelzer-Kluepfel" 158
<li>"Reginald Stadlbauer"   146
<li>"Allen Winter"  142
<li>"Frank Karlitschek" 136
<li>"Carsten Burghardt" 135
<li>"Antonio Larrosa Jimenez"   127
<li>"Inge Wallin"   123
<li>"Mirko Boehm"   121
<li>"Don Sanders"   115
<li>"Christian Esken"   112
<li>"Ingo Kloecker" 110
<li>"Maksim Orlovich"   103
<li>"Friedrich W.H. Kossebau"   96
<li>"Gary Cramblitt"    85
<li>"Joel Dillon"   66
<li>"Sirtaj S. Kang"    48
<li>"Ellen Reitmayr"    44
<li>"Carlos Leonhard Woelz" 42
<li>"Michael Haeckel"   35
<li>"Sven Radej"    34
<li>"Oleg Noskov"   21
<li>"Jens Herden"   20
<li>"Holger Schroeder"  17
<li>"Volker Krause" 15
<li>"Tink Bastian"  10
<li>"Claudia Sorg"  9
<li>"Phillipe Fremy"    7
<li>"Christian Schlaeger"   6
<li>"Matt Douhan"   2
<li>"Ruediger Ettrich"  0
<li>"Fredrik Hoeglund"  0
<li>"Arne Trautmann"    0
</ol>