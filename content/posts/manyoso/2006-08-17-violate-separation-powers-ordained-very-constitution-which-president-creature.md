---
title:   "\"...violate the Separation of Powers ordained by the very Constitution of which this President is a creature.\""
date:    2006-08-17
authors:
  - manyoso
slug:    violate-separation-powers-ordained-very-constitution-which-president-creature
---
The United States just took a gigantic step away from the cliff today.  An unknown federal district court judge just told the President of the United States where he could shove his warrantless wiretapping program.

<a href="http://www.mied.uscourts.gov/_practices/taylor/toc.htm">Federal District Judge Anna Diggs Taylor</a> is my new personal hero.  She has ordered President Bush to <b>immediately</b> end his warrantless wiretapping program as it is in violation of the very Constitution of which his office is a creature.  Here is how she puts it in her <a href="http://www.mied.uscourts.gov/eGov/taylorpdf/06-10204Injunction.pdf">injunction:</a>

<code>       IT IS HEREBY ORDERED that Defendants, its agents, employees, representatives, and
any other persons or entities in active concert or participation with Defendants, are permanently
enjoined from directly or indirectly utilizing the Terrorist Surveillance Program (hereinafter
“TSP