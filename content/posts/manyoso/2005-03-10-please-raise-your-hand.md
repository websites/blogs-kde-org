---
title:   "Please, raise your hand..."
date:    2005-03-10
authors:
  - manyoso
slug:    please-raise-your-hand
---
Please, raise your hand if you'd like:

<ol>
<li>Color syntax highlighting in KDevelop's code completion?</li>
<li>Icons in the entry list corresponding to the entry type ... ctor, dtor, method, member, etc, etc?</li>
<li>Some sorting by entry type so all ctor's come first, dtor's next, etc, etc?</li>
<li>The ability to specify what parts of the entry are "completeable" eg, if I'm completing a method decl I don't necessarily want to key the return type?</li>
</ol>

If so, then the following changes will need to be made to <a href="http://developer.kde.org/documentation/library/cvs-api/interfaces/html/classKTextEditor_1_1CodeCompletionInterface.html">KTE Completion</a> <a href="http://developer.kde.org/documentation/library/cvs-api/interfaces/html/classKTextEditor_1_1CompletionEntry.html">interfaces</a>:

<ol>
<li>KTE::CompletionEntry needs methods for specifying an icon to go with the entry and comparison operator overloads to allow sorting by entry type. It also needs some way of specifying that a prefix/postfix should be inserted upon completion, but not part of the actual keying.
<li>KTE::CodeCompletionInterface needs methods for determining if the current completion box isvisible and hiding methods.
</ol>

The color syntax highlighting could be done in a couple different ways.  KDevelop could do specify the individual entries in richtext, but the interface would have to know that it is doing so and render it accordingly.  I am working on katepart's KateCompletionItem (subclasses QListBoxText currently) to see if I can get it to render richtext.  Another alternative is to have the interface entirely responsible for highlighting.  Presumably katepart could do this, but the highlighting might not be as rich since KDevelop knows more about the C++ syntax than Kate.  

Regardless, I think these things would be pretty cool and it'd be nice to have them before KDE 4, but I don't know.  Perhaps if enough hands are raised... :)
<!--break-->
<br><br>
PS Don't worry, I'm cc'ing the kwrite-devel list too ;)