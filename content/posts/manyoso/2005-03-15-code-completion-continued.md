---
title:   "Code Completion Continued..."
date:    2005-03-15
authors:
  - manyoso
slug:    code-completion-continued
---
A couple of updates on the code completion changes.  In current CVS code completion can:

<ol>
<li> Complete in TRY/CATCH blocks...
<li> Complete on vars declared in if/for/while...
<li> Lots of other fixes...
</ol>

And I have this working although the patch is up for review and not in cvs:
<br><br>
<img class="showonplanet" width="530" height="418" border="0" src="http://web.mit.edu/~treat/Public/color-completion-mini.png" alt="Color Completion in KDevelop" />
<br><br>
There are a few issues, because it relies on richtext and the problem is I have to strip out some chars programmatically which becomes a problem for apps who use katepart and completion entries with legitimate richtext chars...
<!--break-->