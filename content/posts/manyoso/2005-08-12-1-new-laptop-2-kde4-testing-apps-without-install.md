---
title:   "1. New Laptop  2. KDE4 testing of apps without install"
date:    2005-08-12
authors:
  - manyoso
slug:    1-new-laptop-2-kde4-testing-apps-without-install
---
<b>1. Purchasing a new Laptop...</b>

So, this weekend is <a href="http://www.dor.state.ma.us/rul_reg/tir/tir_05_9.htm">Massachusetts tax free holiday</a> and I'm thinking of using the occasion to purchase a new laptop.  

I was wondering if anyone out there in KDE land had experience with the <a href="http://www.linux-tested.com/results/ibm_thinkpad_t43_1871-12u.html">ThinkPad T43 1871</a>?  I can currently get this laptop at discount through a friend for $1,274.00.  Is it a good deal?  Should I get something else?  Anyone have a strong opinion one way or another?

Here are the specs:
 <ul>
<li>Lenovo ThinkPad T43 1871
<li>Intel Pentium M 730 1.6 GHz 533 MHz FSB
<li>RAM  512 MB (installed) - DDR SDRAM - PC2 4200 - 533 MHz
<li>Hard Drive 60 GB - 5400 rpm
<li>DVD recordable 4X MAX. Ultrabay Slim
<li>LAN EN, Fast EN, Gigabit EN, Intel Pro/Wireless 2915ABG
<li>14.1" TFT XGA
<li>Weight  5.14 lbs
<li>Battery  Lithium-ion 6 cell
<li>Average Run Time  4.8 hour(s)
</ul>

<b>2. KDE4 testing of apps without install</b>

In other news, I recently committed a patch to KDE4 that grants the KDE4 developer the ability to test and execute KParts/XMLGUI applications without need for installing first.  The mechanism is quite simple.  The KParts/XMLGUI depends upon KStandardDirs for the locations of the various resources they need to operate.  I committed a simple patch to KStandardDirs that will look for a .krcdirs file in the applications current working directory.  If it finds the file, it will load the resources enumerated within.

It works for KParts and XMLGUI and other types of resources right now, but there remain some problems with how KApplications load the application icon.  See, in most KDE b uild systems those application icon files are 'hi32-app-someapp.png', but the build system takes that and truncates the 'hi32-app-' part of the file name upon install.  The solution is probably to patch KApplication to try and look for a 'hi32-app-' file if it can't find a file that matches KInstance::name() or some such thing.

Anyway, there is some grumbling, but it does work now.  I hope this makes some third party developers and even new KDE developers happy :)
<!--break-->
