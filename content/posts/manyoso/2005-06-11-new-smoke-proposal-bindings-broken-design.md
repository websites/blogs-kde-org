---
title:   "New Smoke proposal?  Bindings broken by design..."
date:    2005-06-11
authors:
  - manyoso
slug:    new-smoke-proposal-bindings-broken-design
---
I've been following <a href="http://jahqueel.blogspot.com/2005/06/smoke-for-kde4.html">Ashley</a> and <a href="http://blogs.kde.org/node/view/1149">Richard's</a> conversations about the new Smoke proposal for KDE4 and I have to say it all reminds me exactly why I have grown to loathe bindings.  First, let me say that I have loads of respect for both Ashley and Richard as they are extremely talented developers.  Both of them helped me enormously with the now discontinued Qt# bindings.  With that said, I think KDE bindings developers and enthusiasts are completely missing the mark for a possible future KDE/Qt binding roadmap.

It seems there is a growing number of KDE'ers with the will to change the KDE binding's situation.  If we continue down the path we're now taking for KDE4 we're going to get the current binding's situation perhaps with more visibility in the community.  Perhaps the kdebindings module can be cleaned up to the point where it builds reliably well.  Perhaps we'll see the adoption of the some of the better bindings (shout out to Ruby, Python and KJSEmbed) in stand alone applications or scripts in Plasma.  If these are your only goals, then please, just ignore this post.  If however, you want more for KDE bindings then read on...

Let's take the new smoke proposal for instance.  Here we already have an enormous library that is about to get even bigger by creating a QObject derived proxy for every non-QObject class in KDE/Qt libraries.  Think about that for a second.  That means every non-polymorphic class in the current libraries will now have a virtual table.  Forget that the hot code paths in KDE/Qt will now have this extra overhead and just think what this will do to the size of this already enormous lib.  Further, we'll have to continue overriding each and every existing virtual function and then on top of that we'll need to make it a slot.

What Ashley, Richard and all other bindings developers are doing is actually quite a remarkable achievement, but it is also the source of the problem.  I believe the entire concept of bindings as we know it is fundamentally broken.  Think about it for a second...  what is a binding, really, after all?

Creating a traditional language binding is nothing short of trying to export an entire language's features by writing a shared library.  IMHO, the mother of all <i>fundamentally</i> broken designs.  Well, actually, there is one that is worse, but you have to read on for that one...

Actually, it is worse than that, because most binding authors see the above as not that challenging enough...  no, no, we have to make it harder by writing a program that automatically writes the shared library exporting an entire language's features. LOL.

And what do we get for this back breaking work?

1. A platform that will never match the speed or performance of the original library <i>by design!</i>
2. A platform that will never have the same feature set as the original library <i>by design!</i>
3. The inability to ever write a shared library that can be consumed by the original language <i>by design!</i>
4. The inability to ever write a shared library that can be consumed by other languages bindings "
5. Awkward and arresting incontinuities between the original language's types and the target language "
6. Awkward and arresting incontinuities between the original language's features and the target language "
7. All of this has to be repeated for every new library that we add and every change we make to the current libraries "
8. Third party library makers that are in some way affiliated with KDE/Qt will have the same issues "

In short, a second class development platform <i>by design!</i>

Now, it seems there is a growing number of developers with the will to change this situation.  If we continue down the path we're now taking for KDE4 we're going to get the above, but perhaps with more visibility in the community.  Perhaps the kdebindings module can be cleaned up to the point where it builds reliably well.  Perhaps we'll see the adoption of the some of the better bindings (shout out to Ruby, Python and KJSEmbed) in stand alone applications or scripts in Plasma.  If that is the goal, then as I said at the top ;), just ignore this post.

The only real way around the problems I've highlighted above is to, as I've been saying to anyone who will listen, build the binding into the compiler.  Actually, it is more like building the binding into the _toolchain_ just like Microsoft has done with the Common Language Runtime.  Although, Microsoft is not the first to come up with this idea, they've done the most thorough job of it.  One way of solving our problems, of course, is by <a href="http://blogs.kde.org/node/view/561">building a C++ compiler</a> for Mono/PNET based upon the <a href="http://www.ecma-international.org/news/ecma-TG5-PR.htmECMA">ECMA spec.</a>  The advantages here would be an already developed CLR implementation for Windows/Linux/MacOSX.  The disadvantages would be the legal uncertainty surrounding this whole path.  I'm not in favor of this, myself.

As an aside, I must say that I find it absolutely hilarious the position that the CLR/Mono folks find themselves in.  They have this new platform that is supposed to be a binding killer, but they <b>still</b> have to write a binding for their old technologies because they can not be naturally consumed by the new platform.  They have to graft their object model that was hacked into the library onto the object model of their new platform.  Thinking about how fundamentally broken that is... and I have to say that this is truly <i>the mother of all fundamentally broken designs.</i> Thus, even though they have this new platform, they also are crippled by the problems with traditional bindings. LOL.

The other option would be to adapt LLVM to support a higher level bytecode presentation.  I've talked about this in numerous locations and I'm sure some are probably tired of hearing me say it again and again.  If you haven't heard about LLVM you can <a href="http://blogs.kde.org/node/view/554">read a little about it here.</a>

To all of you who are tired of hearing me go on and on about this...  It is an enormous undertaking, but I can't help but believe that with the renewed interest among KDE developers towards the bindings, and with very talented hackers such as Ashley, Richard, Alexander, Ian, and the rest that we could get much farther and much faster with this approach than with devoting so much time into what I feel is a withering end.
<!--break-->