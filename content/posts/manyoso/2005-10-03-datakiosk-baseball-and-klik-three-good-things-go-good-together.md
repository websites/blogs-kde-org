---
title:   "Datakiosk, Baseball and Klik!  Three good things that go good together..."
date:    2005-10-03
authors:
  - manyoso
slug:    datakiosk-baseball-and-klik-three-good-things-go-good-together
---
I have been busy working on <a href="http://extragear.kde.org/apps/datakiosk/">Datakiosk</a> and am nearing a new release.  I know many folks are a bit puzzled by this application, so to dispel the wonder I have cooked up a pretty cool demonstration of Datakiosk <a href="https://blogs.kde.org/blog/418">thanks to Klik.</a>  This new release will add some pretty significant new features.

The image below shows the demo in action.  As you can see, I've changed the project bar to reflect Qt4 designer's widget box layout ideas.  This gives a better separation of Tables and Reports and it also allowed me to introduce ... Views.  These are customizable composite forms that can be optionally populated by the edit forms of the table tree.  Searching and scrolling and editing all work as normal from within them.

<a href="http://nx.openusability.org/datakiosk-demo-0.8-beta.cmg">datakiosk-demo-0.8-beta.cmg</a>  (Kurt says we'll have it available as klik://datakiosk-demo in a bit)

<img class="showonplanet" src="/system/files?file=images//datakiosk-0.8-view-small_0.png" width="600" height="421"  alt="Datakiosk 0.8 View" title="Datakiosk 0.8 View" />

Feedback is welcome as I would like to use this demo to get a feel for how people think about this new release.  However, there are a few known issues that mostly spring from the fact that this demo uses an embedded sqlite database.  Because this demo uses Klik, it will not install a database anywhere on your harddrive and is thus, read-only.  It is entirely self-contained within the .cmg file, so don't be surprised if you try to edit a field and click commit to find that you get a 'read-only' error.  Second, the Qt SQL sqlite driver does not return the query size for SELECT statements.  Because of this, the number of rows in the table as reported in the project box will only show how many rows are currently visible unless you scroll.  I'm trying to find a way around this.

Other than that, please let me know if you have any problems or would like to see something I missed.

And in other Datakiosk news, we had a mention in <a href="http://www.linuxformat.co.uk/">Linux Format Magazine recently.</a>  Here is a blurb:

<i>"We've seen a few database front-ends in HotPicks. They're ususally rather grey affairs, geared towards experienced admins.  That's not a big problem per se, but we're always lad to see different approaches, such as the one taken by Datakiosk."</i> [...]

<i>"Datakiosk's power lies in its searching facilities, which range from simple queries via the search bar at the top of the screen to custom SQL commands.  In between sits the Advanced Search facility, where you can select data based on less-thans, not-equals and so forth.  Individual searches can be saved and stored with the overall project for later use, and you can request further information from users mid-search too."</i> [...]

<i>"As well as making searching easy, an approachable front-end has to offer simple data entry.  You'll be pleased to read that Datakiosk's attention to detail in this area is excellent."</i>

Well, thanks for the kind words Mike, but you really should play with this demo, as I think you'll find that it really outshines the last release :=)

<a href="http://nx.openusability.org/datakiosk-demo-0.8-beta.cmg">datakiosk-demo-0.8-beta.cmg</a>

Finally, about the actual database:

<a href="http://www.baseball-databank.org/">It comes from here.</a>  I've modified it a little and converted it to sqlite for use of this example, but it is generally considered to be one of the world's best baseball statistics databases.  

A bit better than Northwind, no?
<!--break-->