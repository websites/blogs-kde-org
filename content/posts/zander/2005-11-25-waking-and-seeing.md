---
title:   "Waking up and seeing..."
date:    2005-11-25
authors:
  - zander
slug:    waking-and-seeing
---
This morning I woke up and heard the slushing sound of the occasional car and bike driving past through the perpetual open window of my bedroom.  Oh, rain again, was my first impression. Sliding open the curtains required just my arm above the covers and I could then see a darkish-gray sky.
Ugh, I'm staying in bed a bit longer.

Half an hour, and some funny chats on IRC later I got up and out of bed.  Only then did I look out the window and noticed how we have a nice layer of snow here in the eastern part of Holland!  And this is not night snow; its still going strong at 10:30 am :)

[image:1646 align=center]
<!--break-->