---
title:   "Flake is snowballing!"
date:    2006-04-29
authors:
  - zander
slug:    flake-snowballing
---
Since a week I've delved into a library the KOffice devs are creating; its called <a href="http://www.valdyas.org/fading/index.cgi/hacking/koffice/flake.html">libFlake</a>. The goal of the library is to have the best of both worlds from KWords frames and KPresenter shapes down to Karbons vector graphics.
To do this we provide a low level object called a Shape and provide hooks for the high level stuff so they can be build on top.
This way we can move shape-manipulation and shape-painting (including painting to PDF) for all KOffice applications into one library with the obvious advantage that any cool component build in a specific application will be really easy to reuse in other KOffice apps.

After a week of working the most important features needed by KWord are already in there;<ul>
<li>Shapes can be given a size in millimeters and they draw in the correct size on any monitor/paper.</li>
<li>Shapes can be grouped so I can add a text area to always be below an image.</li>
<li>These same groups can be used to do interresting stuff like anchored frames, where a frame moves if the parent moves but also when the text is altered and it moves down the anchor.</li>
<li>Each shape can drawn rotated, scaled and skewed without actually changing the content. So you can easily rotate that text, if you want.</li>
<li>Some shape-manipulation tools have been implemented some time ago and they now have undo/redo support.
<li>We have only a move tool but we surely need more (any volunteers?) The goal here is to provide all features from http://www.koffice.org/developer/keyboardmodifiers.php in flake so applications don't have to do it separately!</li></ul>

There are loads of painting optimizations and speedups to create in there, which is one of the things I will be working on in the next weeks :)
<!--break-->