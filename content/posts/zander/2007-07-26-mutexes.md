---
title:   "mutexes"
date:    2007-07-26
authors:
  - zander
slug:    mutexes
---
If you ever did anything with multithreading you'd know mutexes.  They are basically a building block to do any multithreading work in.

In java they are better known as 'synchronized blocks'.  Your basic hot zone can be protected by a combination of <code>myMutex.lock();  /* do stuff here*/  myMutex.unlock();</code>
Which is equivalent to the Java manner of <code>synchronized(myMutex) { /* do stuff here */ }</code>

My biggest problem with the mutex method is that if I add a return before the unlock part, I've got a problem.  The mutex will never be unlocked.  So you will get weird errors that just when that corner case happens which will do such an early return, things will lock up.

So I tend to write the following:<!--break-->
<pre>
  QMutex lock;
  {
      struct Finalizer {
        Finalizer(QMutex &lock) : l(&lock) { l->lock(); }
        ~Finalizer() { l->unlock(); }
         QMutex *l;
      };
      Finalizer finalizer(lock);
      // foo bar
  }</pre>

This means that even if I do a <code>return;</code> call somewhere in that "foo bar" block my lock will nicely be unlocked again.

Now; is there someone that is good with macros and allow me to type the following to expand to the former?
<pre>
  QMutex lock;
  synchronized(lock) {
     // foo bar
  }</pre>

Have fun!