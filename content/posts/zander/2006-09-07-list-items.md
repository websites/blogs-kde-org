---
title:   "List Items"
date:    2006-09-07
authors:
  - zander
slug:    list-items
---
In KOffice 2.0 (based on Qt4/KDE4) we long ago decided to remove the old text engine and base our new one on scribe which is a part of Qt4.

This looks like a simple job; but there are a lot of features an application like KWord needs that are not supplied by Qt. An example of this is numbering paragraphs. That is, Qt supports simple numbered paragraphs, but not nested and certainly not with all the features that the OpenDocument Format specification supplies.

So, I added that 'feature' to KWord and just committed the last part to make it work pretty well. Lots of unit tests to keep it from regressing as I continue working on the text layout.

<img src="http://www.koffice.org/kword/pics/200609-KWord-Lists.png">

Features supported right now;
- restart numbering at any paragraph
- prefix / suffix
- just basic numbering now; I'll work on all the other numbering types, including bullets next.
- displayLevel option; to not show all counters of the list but only the right most ones.
- use a (separate) character-style for the rendering of the list-item.

And features supported that KWord1.5 did not:
- Alignment of lists greater than 10. See how the paragraph text of 3.9 and 3.10 both start at the same point.
- Allow list items without any prefix.  So now you can have a chapter heading without a number.<!--break-->