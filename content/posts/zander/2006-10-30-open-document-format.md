---
title:   "Open Document Format"
date:    2006-10-30
authors:
  - zander
slug:    open-document-format
---
The Open document format is meant as a specification that is open for everyone to see, use and expand upon for now and for a hundred years in the future. That is its main goal and its succeeding in that goal admirably. Especially governments love that principle which breaks the chains of vendor dependency.

A secondary goal is to allow different applications exchange documents without loss. Typically shown by the press as the primary reason, and it it indeed is a good runner up. Its the main reason consumers and companies will choose a suite that has this format. At least for short term advantages. But we are not there yet. It takes time and several versions for all the suites to do things the same and to exchange documents effectively. We are working on that, though!

What KOffice has said in the past is that users should report any problems in transferring documents so we can fix them. While this sounds like a great way to find the problems the programmers missed, its very time consuming since we first need to figure out if the file was saved wrong, or if its opened incorrect. With some misunderstandings over what the spec actually said thrown in every now and then as well. Any typical document will usually use quite a lot of features so it becomes even harder to figure out what is going wrong.

Bring in a testsuite. A long list of small documents each showing a different feature in the spec so application developer can see which feature they are not doing correctly and fix it in a next version.
The testsuite is written independently by the University of Central Florida, sponsored by Intel. Intel additionally sponsored me to do the screenshots for KOffice knowingly that I naturally make sure all bugs there will be known by the application maintainers :)

The testsuite still does not cover the whole spec. That will take more time. It already is very useful and shows the strengths and weaknesses of each suite. And recently the testuite got a new home. I moved it to the http://testsuite.opendocumentfellowship.org/ which is more central in the ODF landscape.

Adding testresults for another application(suite) is not hard, and if you know someone who will work with us on that, please tell them to contact us!

It would be interesting to do a comparison of the number of passed tests now and in a year time :)<!--break-->