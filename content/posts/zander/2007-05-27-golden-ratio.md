---
title:   "Golden Ratio"
date:    2007-05-27
authors:
  - zander
slug:    golden-ratio
---
A couple of days ago I was hanging out on IRC, doing some work on a new Gui widget for KWord.  Now, creating Guis is the most boring and annoying thing I can think of, hence the IRC.  It still beats kpat as that is mouse-only and RSI is not on my wanted features list.

Boudewijn posted a url to a page detailing a feature for another paint application. This feature draws a set of helper lines based on the <a href="http://en.wikipedia.org/wiki/Golden_ratio">golden ratio</a> principle.  If you are not a mathematician, don't fear, the idea is what counts.  The theory details that using a certain proportion (or ratio) to place major components in your drawing, they will almost automatically look good.
I found the idea intriguing, in a way that it would allow me to do something nice and useful instead of slacking off.  And write a blog entry as well, maybe there are people that like writing data-entry widgets that can mail me to help me out ;) )

So, a day later I have a pretty complete new feature that shows the golden ratio in a usable manner on screen, with vanashing lines for those that want to draw 3D and the assorted config options to choose any of the 4 corners as the vanishing point as well as a checkbox to allow the shape to be printed if you want to. (its vector art after all :) )

I'm sure other features can be added, like selecting which colors to use and the line thickness. But I'll leave that to people that want to get started.  A simple and small plugin is a great place to get introduced to mister Flake and its cool associates.

Screeny:
<img src ="http://www.koffice.org/kword/pics/200705-goldenRatio.png"> <!--break-->

