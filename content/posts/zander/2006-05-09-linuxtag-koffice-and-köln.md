---
title:   "LinuxTag + KOffice and Köln"
date:    2006-05-09
authors:
  - zander
slug:    linuxtag-koffice-and-köln
---
Last week I was at LinuxTag in Germany which always is a lot of fun!  Not only in seeing new people, but just as much in making contacts with the various other vendors / projects there. The event has the subtitle "Where .com meets .org" for a reason ;)

I went over to the OpenOffice stand and asked them about their state of implementing the full ODF spec.  Many people don't know that KOffice has placed various features in the spec which were needed for KWord and other suites don't support those yet.  Same effect here, a volunteer openoffice contributer named Uwe (working on the MacOs port) said he was unaware of any missing features, but I'd have to go to the Sun stand and talk with the staroffice people for more in
depth info.
We then got into talks about both office suites and at one point Uwe said that the team would probably be fixing bugs for the next 3 years instead of changing structures to allow for new features like the ones KWord has.  I naturally thought he was just kidding about the timeline but I commented that if that were the truth then KOffice would surely surpass them in that time. I laughed when he answered that he would not mind that, it would give him an excuse to finally dump the OOo codebase!

The next day I went to the Sun stand and asked the staroffice man there about how far the ODF spec is implemented. He was very clear about that, its 100% implemented!  Well, after just a couple of direct questions he came back from that assertion and we ended up looking through some documents I received from KOffice users that were created in KWord but did not load very good in writer.  A productive little meeting where we ended up preparing some example docs for review back at the office by the rest of the staroffice team.

I also met various people from the (K)ubuntu packager team.  They all fully support KOffice, we surely have friends there! The packages I tried are top notch and the LinuxTag DVD caried them so no download needed.  The Kubuntu release currently ships OOo per default (but KOffice can be installed over the net without problems). Reasoning is that currently KOffice can not be used in enough situations.  Which is a point I certainly have to concede is true.  Lets work on that!

After LinuxTag I went over to Thorsten Zaggman who lives a little way away from where LinuxTag was held and we talked about Flake, where it is and how to fix design problems we ran into.  It was productive and I am sure we will end up with a hugely integrated KOffice where all applications use the same way to handle graphic-objects for optimum user satisfaction.   I'll be committing more stuff there this week.

On the way back home, I drove via the Köln (Colone) ring, and since it was about noon, this time I went into the city instead. Its been years since I've been there!  The city still is very nice and vibrant. Walking around brought back good memories :-)
Unfortunately I did not have any addresses/phone numbers of people I know who live there, so I did not get to meet them.  Oh, well, a good excuse to come back sooner this time :)   Its only 1½ hour drive, after all.
<!-- break-->