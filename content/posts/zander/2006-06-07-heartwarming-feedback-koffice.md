---
title:   "Heartwarming feedback & KOffice"
date:    2006-06-07
authors:
  - zander
slug:    heartwarming-feedback-koffice
---
With Blogs we are all publishers, thats the new world and everyone has his or her soapbox.  In the good old days the concept of being published was the most important thing you could get. It meant recognition.
Nowadays, its getting linked and copy-pasted (with or without attributeion).

I know that choosing your subject wisely makes this a lot easies, see just a couple of days ago I wrote a vision paper for <a href="http://dot.kde.org/1149518002/">KOffice 2.0</a>; what we are doing and where we want to go. And, oh, boy :-)

Its a testament to the KOffice popularity and, most of all, its promise for greatness, that we see the vision being translated and linked from major sites. I already spotted <a href="http://www.osnews.com/comment.php?news_id=14836">osnews.com</a>, <a href="http://www.pro-linux.de/news/2006/9791.html">pro-linux.de</a>, <a href="http://golem.de/0606/45728.html">golem.de</a> and <a href="http://barrapunto.com/softlibre/06/06/08/1142202.shtml">barrapunto.com</a> (castilian).

Next to that; did you notice this gem? <a href="http://www.kde-apps.org/content/show.php?content=40489">MSWord toolbars for KWord</a>.  Now; thats a great way to customize KOffice :)

And we just keep on going forward. Last weekend I spent a day hacking at Boudewijns place (its wonderful to see his daughters use KWord and be pretty happy with it!). We spent this time to work out a big part of our strategy, the tools that are used to manipulate the shapes that flake provides.
See; if you want to make things loadable libraries and add more features to an application as services (which basically are plugins) you have to make sure that that plugin ships both a shape as well as its editing interface.
The best example for this is the text-area.  This is shown on screen and printed, so its a shape (inherits from KoShape) but if its just a shape then all you can do with it is move it around, rotate and scale it.   So, what about editing the text?
This is where a tool comes in (inherits from KoTool). The tool works together with the selected text-shape and any keystrokes you make will alter the text.
These two items together, the shape and the tool, then are shipped in one library and have a couple of .desktop files so koffice applications can find them.
Text tools via plugins. Its so simple you feel we're onto something.  Fsking brilliant :-)

Next to text-shapes, formula-shapes, bspline-shapes and kivio-shapes. Any other ideas what could be shapes and tools?<!--break-->

update: added castilian translation.