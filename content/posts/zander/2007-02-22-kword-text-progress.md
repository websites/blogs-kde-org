---
title:   "KWord text progress"
date:    2007-02-22
authors:
  - zander
slug:    kword-text-progress
---
Since my last blog I've been working on various different projects in KOffice. Most are not really screen-shot interresting so I declined to blog about them.  After all, who wants to se that if I type text in a text-frame the frame will grow automatically when the text would not fit anymore.  That's soo boring :)

Slightly less boring are some features that I found really useful in other software, but never managed to implement in the old KWord, but in the KOffice 2 structures its a lot easier to do so.

<b>Example text</b> is what I called the first feature.  Most templates that you open and get an immediate "wow" feeling by are pre-filled with cool looking content. Great images, vector art etc.  One thing that has always been missing is text.  Afterall, adding text to a template kind of makes it a finished document and not a template.  So, what I did was that a new text frame is initially filled with the famous Lorus Ipsum text. Unreadable text that is there not for the content, but for the looks.  As soon as a user tries to edit the text the demo text dissappears and the cursor is placed at the beginning of the shape.  I found that it makes it really easy to play around with KOffice2 features and show off things.

I also added a <b>Text end</b> line. What I observed in many non-technical people using a text-area was that when they typed and revised text there were always some linefeeds at the bottom of the text. And over the document lifetime these linefeeds grew more and more.  The eventual request to someone else as to why that blank page was added while there was no text at all tended to come up.  What I added was a simple, non-printable line at the end of the text.

For people that do DTP-style text-layout there is a new feature to show a <b>more text in frame</b> indicator.  The idea of frames-based layout that KWord uses is really powerful in that you can decide exactly where your text goes. Multi-column text layout, for example. If you read your local paper you might notice that sometimes an article is not finished but the text reached the bottom of the column anyway.  Basically cutting off the text.  What I added is a non-printable marker at the bottom of the last text-frame to indicate there is more text. Its a lifesaver in such cases.

Last; I added a 'docker' for showing and selecting the styles. The old KWord only had a combobox to select the active style, and it did not have any character styles either.  The screenshot below shows you the paragraph-styles page. Its quite simple at this time, and the buttons at the bottom don't do anything yet. They need to get icons to show they will get 'add'/'remove' functionality behind them.

<a href="http://www.koffice.org/kword/pics/200702-kword-text-shape.png"><img src="http://www.koffice.org/kword/pics/200702-kword-text-shape_thumb.png"></a><!--break-->