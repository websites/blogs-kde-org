---
title:   "Installing opensuse 10.1 from the network."
date:    2006-05-16
authors:
  - zander
slug:    installing-opensuse-101-network
---
I had a frustrating hour today,  I wanted to install suse on this spare machine here so I can test the KOffice packages on that, and other distros.
I choose the small image that downloads everything during the install; which is what I always do for installing Debian. (not that I install that more then ones per machine, but still, familiarity gets you points : ).

So, a 36Mb download later I burn it to CD and boot from that.  I follow the instructions on the <a href="http://en.opensuse.org/Released_Version">webpage</a> that tell me I have to enter the http address in the boot screen.  Seems a bit weird that thats not hardcoded, but ok.

This resulted in me getting dumped into the text-yast without any error message and I reboot to try again, maybe I made a typo.  Did that a couple of times, you get my drift.. After some reboots-retries I finally find the logs creen behind alt-F3 and read that the download of this image failed due to an error;  http code: 302.

Hang on; thats not an error! Thats a normal way of the server telling the download software that it wants you to use a different url.  Apparently that 36Mb image does not contain a proper piece of http download software. :(

On my different machine I figure out the redirected URL and try again; success.
The correct URL is; http://ftp.opensuse.org/pub/opensuse/distribution/SL-10.1/inst-source/
Well, its downloading one 70Mb file now before I can start my installation.  Not sure why that didn't go on the CD.  Well, it makes at least me wish I bought the CDs...
