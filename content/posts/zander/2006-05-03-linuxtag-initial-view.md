---
title:   "LinuxTag initial view"
date:    2006-05-03
authors:
  - zander
slug:    linuxtag-initial-view
---
The usual suspects of the KDE crew arrived in Wiesbaden/Germany (near Frankfurt) last night; we did some initial work in setting up the booth and finished up this morning before the crowds arrive. Here is a nice pic of what our booth looks like. Yes this is before any visitors were allowed on the floors; I'm pretty sure it will be more crowded after that ;)

<img src="https://blogs.kde.org/system/files?file=images//linuxTagBooth-small.preview.jpg">

Have fun!
<!--break-->