---
title:   "ReportWriter"
date:    2005-04-14
authors:
  - zander
slug:    reportwriter
---
At the last aKademy the openusability website was demonstrated with the idea that usability experts (mostly people that do this for a living) will describe problems in open source projects with suggestions on how to fix them.
Sounds great!  There was one thing I found less then great; those poor usability experts had to write their report in an XML to make the website understand.

Now; I always think that XML is a great format as long as you have a nice GUI app that will generate it for you so in August I immidiately started sketching a GUI to enter that info. <!--break--> (Aaron; do you still have those?)

The problem with something like this is that you need the freedom of a word processor combined with the structuring of a UI with a small number of textfields.  To make a usable interface you will surely not show a small set of textfields to the user with a button to create more textfields.  What _always_ happens is that the whole text will be pressed into the biggest textarea and the textfields will contain mostly useless info like 'bug' and 'new feature' or other stuff.
People don't work their creativity very well in a structured environment.

So, here is my solution;
I create a document structure to define the usability report.  One line title, a description which allows markup like bold/italic and followed with a list of items to describe how to reproduce the 'bug'.
With that document structure I create a widget that allows the user to edit his text but not break the document structure.  The user can't use bold/italic in the title, and pressing enter in the title will not make it 2 lines.  Stuff like that.

It appears that there were some authors who did something similar before me, but nobody actually published it and the implementation I find (in a company) was pretty horrific and bugridden.  Leaving me with no alternative to start a new text-editor widget that does all this.

I recently finished optimizing it so its usable and really really fast for just about all usages. As always there are some things left; but editing a 10Mb structured text is near instantanious now.

Its been an incredible lot of fun creating a text-entry widget for formatted text which actually has quite a lot of potential uses as well.
Whats left is inserting pictures and numbered lists and allowing the user to see/change the structuring.  For example to change his last bulleted entry in a list to actually be the first line of the solution, since he forgot move the cursor.

Here is a screeny: <img src="http://members.home.nl/zander/reportWriter.png">