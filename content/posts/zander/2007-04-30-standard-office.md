---
title:   "Standard Office"
date:    2007-04-30
authors:
  - zander
slug:    standard-office
---
June 21th 2005 was the day KOffice released version 1.4.  I highlight that release because it was the first release where KOffice switched its native format to the OpenDocument Format.  That would become an official ISO standard in May 2006.
The direct gains may be that there is no conversion step required in loading docs from other application in their native format, but the long term gains are much more substantial.  Being able to work with all the industry leaders on the creation and maintenance of the format (and there are quite a lot in the Technical Committee of ODF) allows us to level the playing field and let office applications compete on features and ease of use instead of on who uses what suite and what your partners have chosen.
This means real competition where the end user is the clear winner with lower prices for better quality software.
It won't surprise you that I believe that KOffice has the upper hand due to its superior design and foundation.

Several months ago Microsoft saw that people noticed that customers demanded the shared and open fileformat as governments started choosing ODF by droves.  Not surprising as a government is legally obligated to prefer an ISO standard over alternative choices. Also not surprising is that Microsoft then came out and stated that the file format that has been driving their software for the last decade could be documented and made into an ISO standard. A huge effort that apparently gives Microsoft a lower cost/profit ratio over just using (and expanding) OpenDocument.  I'll let you speculate on the reasons for that.

So, today we are at a point that the ISO member countries have a vote they have to cast in 5 months whether the Microsoft format (OOXML or Echma 376) indeed becomes an ISO standard.  Naturally I find it useful to take a look at the proposal. Its good to know what is so good about this document that Microsoft things its worth their time and substantial efforts to create.

In short, I am not impressed. The MS fileformat clearly contains the baggage of 10 years of software development and mistakes and corrections on top of corrections.  This is fine for one piece of software, but when this goes to ISO then the concepts change.  An ISO standard has to be used by various pieces of software and that includes creation and parsing of content.  Having exception on top of exception is therefor not something ISO strives for.

It took some convincing (mainly myself) and some rounds of sponsorship, but I have been invited to represent the dutch http://vrijschrift.org in the dutch standards organization http://www.nen.nl in the subcommittee 34. Which is the one that votes on OOXML on behalf of The Netherlands / NEN.

Last Thursday we had a initial meeting where there were 8 companies (6 new) being represented on the committee. Which made the NEN employee smile since its notoriously difficult to get even 1 member.
It was a good introduction to the whole standardization concept and which steps are taken where.  Yeah, its extreme bureaucracy with loads of rules.
Each member country gets one vote, a 'Yes', a 'No' or an 'Abstain'.  Its mandatory to give a reason for a 'No' vote and requested to be in the form of "If you fix XZY, then we'll agree on the proposal anyway".  After the 5 months all p-member countries (some 30 at this time, but more can be added) vote and if 2/3th of all (non abstaining) votes are 'Yes' then its an ISO standard.

Its quite rigor and I'm dissapointed its impossible to fix things in the proposal, bringing it down to a simple yes/no vote.  The reason for this rigorousness is that the proposal was entered on fast track.  Which is meant for a standard that has been fully created in an open manner and has had loads of peer review already.

If you have documentation on Ecma 376, and/or want to coordinate (internationally) efforts, feel free to contact me on my usual email address.<!--break-->