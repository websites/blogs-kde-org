---
title:   "Usability vs. features"
date:    2005-05-01
authors:
  - zander
slug:    usability-vs-features
---
The comic foxtrot is always really nice to read; and todays comic was showing the common problem of more features meaning less usability in a very clear way; I can't help but talk about it (well, after I stopped laughing :)
First here is the comic:  <br>
<img src="http://images.ucomics.com/comics/ft/2005/ft050501.gif">
<br>
The author (correctly) got just about all the big usability rules wrong to make sure this thing display power (its huge!) but be quite useless at the same time; I wrote this down a long time ago in the <a href="http://developer.kde.org/documentation/standards/kde/style/basics/usage.html">UI guidelines</a>.
To be user friendly, software must be: task-suitable, understandable, navigable, conformable to expectations, tolerant of mistakes and feedback-rich. (follow the link above to get a full explenation)

Almost all software, no matter how complex, can be made usable if you follow these simple 6 points.  Naturally you need to know a lot about your users and become an expert in the field so you can choose wording or images, as well as interaction models correctly.  Also some of these can't be fulfilled without user testing since <a href="http://tina-t.blogspot.com/2005/04/mp3-and-testing.html">simple solutions are not always correct solutions</a>.

I'm a strong believer that the best usability innovations come from developers; since they are by nature creative and in open source the feedback loop can become pretty short allowing for fast recovery from mistakes.  Naturally the interpretation of feedback (and in various cases also the generation of feedback) needs expertise most developers don't have.  I'm excited that open source is more and more open to suggestions from usability people so we don't create a UI like sed or awk which are extremely powerful, but lack somewhat in the usability department.