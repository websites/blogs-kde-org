---
title:   "Working with CMake, dependency hell"
date:    2006-08-06
authors:
  - zander
slug:    working-cmake-dependency-hell
---
First of all; I'd like to say that CMake is a nice technology; its cross platform, and saves us from the braindead auto-tools, but most importantly it basically gets the job done.

For me things don't stop there.  I got interrested in user interface design not so much because my boss wanted me to, but because it served my own needs.  See, I want the software I use (and create) to be something that helps me accomplish my goals, not get in my way.
CMake fails in a number of important <a href="http://wiki.kde.org/tiki-index.php?page=cmakeProblems">respects</a>.  I would follow a favorite phrase from Alan Cooper and call cmake "dancing bearware".
Making a bear dance is quite the accomplishment, and most people will be impressed by the feat. Whenever you see a bear dancing, you'd want to give a coin to the owner for this great accomplishment.  Naturally, if you compare the bear to a real dancer there is no doubt in anyones mind who is better.
But everyone will still defend the bear if I make that observation.   They will say: "But, it dances!".

CMake is like that, it just works.  The user-interface-design that was done is close to zero, but at least it does provide the right results for the people that are willing to learn how to use it.

My number one problem (see <a href="http://wiki.kde.org/tiki-index.php?page=cmakeProblems">the wiki page</a>) is that dependency checking is done in an entirely correct, but amazingly annoying way.  In KOffice we have some 120Mb sources and the correct way means that a 5 second job takes cmake about 4 minutes.  So, it works, but it totally ignores the way people use it.  Dancing bearware.

The solution of the cmake authors is an option to totally disable all dependency checking. Which is not a solution at all.  If I own a Ferrari I want to drive any speed between 0 and 200km/h.  Not just 4km/h or 200km/h and nothing in between. Dancing bearware.

For all the programmers that, like me, want to be able to decide for myself what speed I want to drive, and how much dependency checking I want to do for my build targets, I spent this morning to write a program that uses cmake textfiles and uses the no-dependency feature new in 2.4.3, but builds on top of that to do dependency checking based on the directory you are in now.
So, if changes were made in, for example, kdecore, but you want just kdeui to be rebuild without you having to wait for the kdecore recompile. Just cd into the kdeui dir and start the make from there.

I'm sharing this as is, hoping its usefull for more then just me.  No guarantees and all that.
Download: <a href="http://members.home.nl/zander/unsercmake">unsercmake</a>
Have fun! :-)
<!--break-->