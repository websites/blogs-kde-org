---
title:   "conspiracy crackpots"
date:    2007-05-08
authors:
  - zander
slug:    conspiracy-crackpots
---
When I get an email blaming me for doing something wrong, I typically stop and think. Its important to me to be open to feedback from others and 'do better'.
Now; when some mails further in the thread you notice all arguments get ignored, and everyone that speaks up gets accused of conspiring with the others, you know you've got a conspiracy theorist on your hands and you know that whatever you say will have zero effect anyway. (yay for KMails 'ignore thread' feature!)
I was in a couple of such threads recently, and reading this perfectly timed edition of a rather geeky comic lifted my spirit quite a bit :)
Enjoy it for yourself at; http://xkcd.com/c258.html <!--break-->