---
title:   "Flake test application"
date:    2006-07-08
authors:
  - zander
slug:    flake-test-application
---
In KOffice the flake library for shapes is taking a much clearer form now KWord is actually really using them.  Details like shape-configuration widgets have been flashed out.  A tricky think considering its plugins based.
In the last weeks I've also been working on getting a text-shape operable. Its pretty cool to have a couple of text-frames as you know them from KWord, but you can edit the text even while its rotated or skewed.  Much more work has to be done to enable real DTP like features, though.

As KWord is currently the only compilable application in KOffice that loads and shows flake shapes, everyone that wanted to develop on flake itself was using KWord as an expensive shape editor.  Well, at least the pagespreads give you plenty of space ;)

For this reason I got the test-application out of the mothballs and added a plugin-tester to it. As you may know, flake is just a framework for shapes to be build on.  I always compare it to a widget, you can extend a widget and do interrestin stuff there just like you can extend KoShape and do the painting and all shape-specific things in that class.  Apparently its not too hard, and the extensive API docs probably help as well as Isaac made a new shape in a very decent timeframe yesterday.

Here is a screenshot of the test application where you can play with the advancements made in scaling logic by Casper and also browse through the plugin installed.  The plugin tester is a useful tool in that you can see the shape rotate and test common problems.  I think its very useful for plugin writers!

<a href="http://www.koffice.org/kword/pics/200607-flakePluginLoader.png"><img src="http://www.koffice.org/kword/pics/200607-flakePluginLoader_sm.png" border="0"></a>

You should consider trying it out one day, if you have KOffice trunk installed, that is.  I maintain it in a repository format of my <a href="http://darcs.net">favorite tool</a>; you can download the package with a simple<pre>
  darcs get --partial http://people.fruitsalad.org/zander/software/flake</pre>
The only bugger here is that I have no idea how to convince qmake to use the proper directories without me hardcoding the so the testapp.pro has paths that only work here.  Patches welcome (just alter, darcs record and darcs send)

<!--break-->