---
title:   "Its all about the meeting of minds."
date:    2007-02-25
authors:
  - zander
slug:    its-all-about-meeting-minds
---
I went to Fosdem this year, mostly to see people I have not seen for quite some time.  And that part certainly was successful! Seen so many familiar faces.  People I know from Java (Sun) that I met for the first time,  people that I know from free-java that I saw again, and naturally quite a lot of KDE people.  One of them was Annma, whom cancelled a meeting years ago and she moved a couple of times since then. So it was good to finally see her in real life :)

One person I was kind of hoping to see was there as well.  Some 6 years ago we parted and didn't really speak since.  We didn't part on the best of terms.  I found 6 years to be enough, based on the idea that old people always say that their regrets are always about the things they didn't do.  Unfortunately if could not be so, the only reply I got was a: "Thomas, I really don't want to talk to you".  Which saddens me so.
If you read this, please consider that 6 years is a long time, people change. I think I grew up.

Fosdem as a conference is just great. As usual it was packed with great free-software thinkers.  So many people coming from a long way away to talk or just hang out. This year was no different in the really really tightly packed schedule of great talks.
Belgium beers are great for several reasons, there are so many of them to choose from and most of them taste very good. Saturday evening dinner was a little challenging as the menu was in Italian with French explenations, both of which I don't read. When my choice (Lasanga aux Fruit de Mer) of dish arrived, doubts flew out the window, it tasted wonderful!
All in all, I had fun :)
<!--break-->