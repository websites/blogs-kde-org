---
title:   "OOXML at the national level"
date:    2007-06-08
authors:
  - zander
slug:    ooxml-national-level
---
I have been invited to join a subcommittee at the Dutch national institute for standards, NEN. This subcommittee is about document formats, and thus the new OOXML format is being discussed there as that is in its 5 month period for the fast track as requested by Ecma.

I also got a nice box with the paper version of the over 6000 pages specification. Leafing through it gives me a headace every time.  I'd rather preserve the trees anyway (its some 45 cm tall)

At the last meeting we had some fun; there are 3 obviously pro OOXML parties involved, and 3 that are more neutral and are focused on a good standard instead of just having this particular one being Oked without any changes whatsoever.  The fun part is when potshots are being made in the direction of ODF, it was off topic so I don't generally respond, but really the documents written by OpenOffice don't contain 'loads of non standardized tags'. ;)  The content actually contains none in the the loads of documents I have read with vi.

One point was rather interresting, the following question was posted; "What do we loose if this does not become an ISO standard".
Naturally everyone will have his own answer, and that's fine as the idea is to have all stakeholders at one table.  The answer from one of the parties (not MS) was rather amazing, though.  He stated that his business would grow quite a bit if he was able to write to and read out of the files without having to always use MSOffice. So he would be able to create specialized tools.

I naturally fully agree to that point, it is a great enabler to have the spec of a fileformat.  This is, actually, the core reason for open standards.  So, he likes open standards.  Well, so do I :)

The thing is; OOXML is not an open standard.  Easiest proof is that countless objections have been posted and zero changes have been made to the standard so far.  I raised an objection myself at that meeting, the specification is not a full specification it leaves parts unspecified under the guise of backwards compatibility.  There are various tags like "do linespacing like it was done in WordPerfect 5.1". It doesn't actually specify what that is other than saying that you should look at that software and copy its behavior.  Good luck doing that in 10 years!
So this is <a href="http://en.wikipedia.org/wiki/Backwards_compatibility">backwards compatibility</a> by forcing all the implementations of this specification to acknowledge the entire history of word processing and write support for each of those that have come before and did things slightly different.

At this point it warrents making clear that the founding principle of the OpenDocument Technical Committee includes allowing interoperability with the known and older files on the market.  So the goals are similar, but the solution taken is different. Most likely due to the fact that ODF is already implemented by multiple vendors.

I wrote a document to address this point, one I'll present at the next meeting (Monday), maybe people here like to see it; <a href="http://members.home.nl/zander/docs/ooxml-backwardsCompatible.pdf">backwards compatible PDF</a>,   and here is the <a href="http://members.home.nl/zander/docs/ooxml-backwardsCompatible.odt">odt</a> as well.<!--break-->