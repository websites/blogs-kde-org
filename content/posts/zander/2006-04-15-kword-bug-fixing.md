---
title:   "KWord bug 'fixing'"
date:    2006-04-15
authors:
  - zander
slug:    kword-bug-fixing
---
Many years ago I was the maintainer of KWord.  I lost interrest in C++ and things went downhill from there (for KWord, not for me : ).  The module got reassigned to David, who we all know has more code under his wings then is humanly possible to actually maintain.

Now, I got back in KWord and I took a look at KWords buglist; 273 bugs and 212 wishes.  Ugh.  Thats a lot of problems.  So, I went through something like 75% of them closing duplicates and ones that the 1.5 release (or earlier) actually already fixed. I even fixed one bug on the spot :)
Result; I closed 91 bugs and made a lot of small bugs into junior jobs. Yeah! See: <a href="http://bugs.kde.org/weekly-bug-summary.cgi">bug summary for this week</a>

So, if you want to get your name in the new commit-digest, take a look at the <a href="http://www.koffice.org/getinvolved/junior-jobs.php">junior jobs</a> page and help us out!

Now back to actually fixing some bugs :)
<!--break-->