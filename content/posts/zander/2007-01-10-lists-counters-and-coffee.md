---
title:   "Lists, counters and coffee"
date:    2007-01-10
authors:
  - zander
slug:    lists-counters-and-coffee
---
This morning I woke up from the loud sounds of people drilling and otherwise renovating the house.  Ahh, the empty apartment upstairs has gotten new occupants!
It didn't take long before I decided to leave the house for today, the continues sounds of building are horrible for my concentration.

So, I dropped an extra battery in my bag and took my laptop across the street. Right across from my place there is a restaurant that's open all day and serves drinks as well.  Its main target are people that just visited the farm next door with animals specially interresting to kids.  I'd call it an animal farm, but I'll refrain from doing that here. Don't want to explain that in Holland we really don't have farms that herds kids.  This shop has a nice fireplace and all. Pretty cozy :)

I spent most of my day there, enjoying properly made cappuccino, hacking on something I'd been postponing a while now.  Lists.  If you recall, I did a blog about lists some time ago. And indeed the major working had been done. But as Linus once said; the bitch is in the details. Lots of them left to iron out.
I'm pretty proud to say that a day without IRC and a good supply of drinks has delivered a system that can load my very featurefull document completely. Doing all the things that the old KWord could do as well as some new tricks.

This may be a good time to point out the somewhat different approach KWord has to tackle lists.
KWord has two list types; one type is specific for chapter numbering (and requires you to setup a style) and the other is simple lists that work with or without setting up a paragraph-style.
What we do in KWord is we tell a certain paragraph that, yes, its part of a list.  And we tell the paragraph at which level of indentation it is. Example in html syntax;
  &lt;ul&gt;&lt;li&gt;Item1&lt;/li&gt;&lt;ul&gt;&lt;item2&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/ul&gt;
places Item1 at listlevel 1 and item2 at listlevel 2.

After the user marked the list items like this he can go on and tell each paragraph the style. If its a bullet or a number etc.
The rest is automatic.  Have 3 list items in a row? Then they get to be 1, 2 and 3.

For anyone that has ever made lists in OpenOffice or MS Word, this way of working really is an enormous simplification which takes away all surprises. And just works. Do note that ODF allows this kind of listing just fine.

See the (big) image below for an extensive (but not exhaustive) example of whats possible. Including some that are (near) impossible in other software.
<a href="http://www.koffice.org/kword/pics/200701-kwordCounters.png"><img src="http://www.koffice.org/kword/pics/200701-kwordCounters_thumb.png"></a>
<!--break-->