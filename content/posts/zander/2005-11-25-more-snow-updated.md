---
title:   "More snow [updated]"
date:    2005-11-25
authors:
  - zander
slug:    more-snow-updated
---
After a fight with nature and facing certain death in the face I am safely back home in the warmth with a nice cup of tea.
What appeared to be fun and exciting <a href="http://blogs.kde.org/node/1645">this morning</a> turned out to be more then a little too much.  From nice autumn weather to 20 cm of snow in one day. Suffice to say that the only viable mode of transportation tonight is walking.  Bicycles are normally seen everywhere here in Holland, but today you will not get 100 meters without falling flat on your face.  Cars have an advantage with extra wheels, you'd think. Well, not really they don't have any grip either and going to fast means you will end up hitting the sidewalk.

Good that the cars don't go fast today, and I was relatively safe walking to the shopping center because this friend mentioned eating  kebab and I really had a craving. You know how things like that go.
When I walk from home to the shopping center (which is 200 meters from where I live), I go through a corner of the park, past a kiddy-farm. You'd think you entered the forrest there. So I walk in and after 20 meters I see a safety ribbon over the road which I was walking on. It was there to make sure people were not going into the park.  I looked back and saw that this restriction makes sense, there are several really big branches over the paths.  A 10 meter branch that fell from the tree because to much snow was on it, well, not something to get on your head.

I sped along and went past the ribbon, out of the park.  Not a moment to soon!  Behind me a large branch came down in a big cloud of snow! I saw images of myself being entirely too flat go past the minds eye!

I took another route on the way back, no need to take risks, to many good things in life I have not seen yet :)

It won't stop me from making nice pictures of this snow tomorrow, though!

Update: This morning I went out with my camera, here is the result: <a href="http://members.home.nl/zander/firstsnow/">First Snow</a>
<!--break-->