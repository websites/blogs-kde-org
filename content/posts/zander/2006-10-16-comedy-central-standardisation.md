---
title:   "Comedy central at standardisation"
date:    2006-10-16
authors:
  - zander
slug:    comedy-central-standardisation
---
I really had a good time reading <a href="http://www.robweir.com/blog/2006/10/leap-back.html">A leap back</a> from Rob Weir. Its about how Ms let an implementation bug live on for years in the application and now its proposing that that mistake be standardized in the new (as of yet unused) fileformat!

Now, I'm certainly not a Ms basher, but I have to work quite hard to keep from laughing out loud on this one :D<!--break-->