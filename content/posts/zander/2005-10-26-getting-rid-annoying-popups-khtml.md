---
title:   "Getting rid of the annoying popups in khtml"
date:    2005-10-26
authors:
  - zander
slug:    getting-rid-annoying-popups-khtml
---
Since some months the accessibility feature of khtml is a lot more aggressive; in 3.4 only sites that specify shortcuts hijack the control key, so you probably never saw the feature this blog is about.
In 3.5 pressing ctrl, and releasing it again will show a lot of little windows all over your page on the spots a href is located. The little window has the same yellow background color as a tooltip and shows 1 character.  Pressing that character will activate the link.
Sounds great for people that don't want (or can't) to use the mouse, and as a workaround for the entirely unintuitive ordering of using tab to access the links.

The usability people have long ago learned that single key shortcuts are a definite no-no. Anyone remember when many years ago KMail had 'k' for select all?  And 'd' for delete?  Was great when you typed "kde" and you lost all your email :)
We fixed that by using 'ctrl-a' for select all. We now have the usability-guidelines that any default shortcut is with one or more of the Control, Alt, Shift or Meta modifier keys.
The Accessibility people probably have good reasons to ignore this lesson, so don't go staring all angry at them now, but the lesson is still a wise one that I get annoyed by whenever its being ignored.  We have a <a href="http://en.wikipedia.org/wiki/Sticky_keys">'Sticky keys'</a> setting for people that need it, after all.
I see the little popups in khtml way to often, I almost never use them but the little yellow buggers are a huge warning to keep hands of the keyboard or some weird link might be followed (loosing all work, or worse).

If you see this happening in your KDE, I suggest you do the following;
locate your konquerorrc file.  Its in something like $HOME/.kde/share/config/konquerorrc
Add this at the bottom of the file:
<pre>
  [Access Keys]
  Enabled=false
</pre>

And all is good :)
Have fun!
<!--break-->