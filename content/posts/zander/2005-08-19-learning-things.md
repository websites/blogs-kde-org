---
title:   "Learning things"
date:    2005-08-19
authors:
  - zander
slug:    learning-things
---
Last week I read the <a href="http://www.canllaith.org/blog/elevation.html">blog of Jes</a> where she addressed the usage of the word intuition on UIs.  I found this interresting and wanted to add something to her content here.

Intuitiveness of a user interface (UI) is based on the theory of learning; how do people learn and use that learned knowledge in practice.
People go through a couple of phases during the usage of an application (or any UI). At first people read texts and guess meaning from the text, as well as trying the various functions in order to find out what does what; after a while they learn where in the UI to search for a specific function, but they still mostly read the function-names and try more then one button or menu item before hitting the right one.
After long enough usage of the UI people will be an expert of a certain part of that UI, where being an expert means that the user can predict exactly what will happen before actually hitting a button or menu item.

These 3 steps that each user goes through (from beginner to expert) I will call the learning steps and they will be at different states for different parts of the user interface.  A user may know nothing of the config menu but be an expert in the file menu entries. For example.

Each KDE application has a similar file menu, with open, save and quit.  This section is easy to learn for new applications since the user just has to notice its the same as all other KDE applications.  In this case we'd call the menu intuitive. The user is an instant expert in that section of the new application.

I grabbed a book called "Intuition" from the shelve and went to the chapter "Definitions". It describes intuition:
&nbsp;&nbsp;&nbsp;&nbsp;Immediate conviction of the truth, which is not the result of reasoning or analytical thinking.
With this subscript;
&nbsp;&nbsp;&nbsp;&nbsp;Intuition is that you know something, but you think; "where did that come from?"

In relation to UIs an intuitive widget or dialog means that the user is an expert in the usage right away, on first usage, in the sense that (s)he can predict what each action in the dialog will do before actually doing it.
Jes was right that the word is highly overused since this kind of situation is quite rare.

Being un-intuitive means that that same dialog, which the user expects to be an expert in because it looks familiar, but the results are not what he expects.
An example is the kuiviewer application where the file -> quit action does something different from all applications in KDE.  It closes all open windows; not just the selected one.  This is unintuitive because the learned behavior is unpleasantly non applicable in this case causing confusion.

There are various ways to help the user progress smoothly through the 3 learning steps I mentioned above and keep the amount of distinctive sections the user has to learn to a minimum.  These methods and theories are the basis of the usability field.
Next to applying the various theories we (should) do user testing.  No theoretical background can ever be better than real interaction testing and design.  See us at the <a href="http://conference2005.kde.org/sched-marathon.php">aKademy usability sessions</a> on Fri 2-sept where you are taken through things like paper prototyping and you can see a live user test.
<!--break-->