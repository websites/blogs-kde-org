---
title:   "More KWord & KOffice updates"
date:    2007-07-18
authors:
  - zander
slug:    more-kword-koffice-updates
---
Some weeks ago KOffice alpha1 got tagged; but some balls were dropped and it never was uploaded to the ftp site.  The good news is that even more new cool stuff is visible in the KOffice Alpha2 which will probably come out end of August.

One of the cool things in our Flake library are the ease with which you can make vector shapes.  As Jan demonstrated by basically having a string of coordinates as a new template and coming up with a shape we call the Gearhead <a href="http://jaham.wordpress.com/2007/06/28/gearheads-all-around/">Jans blog</a>.  This addition is done in a C++ plugin right now; but we plan to add support so adding this kind of template can be as easy as pointing to a text file in the shape selector.

I rearranged my todo list a bit and did some required features as well as some features that were really scheduled for 2.1. But I just wanted to do them now. Sometimes you have to shake your fist at planning! Even if you are the only one actually overseeing the plan.

I added a feature that I think is pretty cool to have; its called text greeking. <img src="http://www.koffice.org/kword/pics/2007-07-greeking.png" align="right"/>Basically when you paint text and the text ends up being really small (like a couple of pixels per character) you can't really read the text anyway. In this case I just paint a general block in the place of the text which naturally is quite a bit faster.  This means that if you zoom out a lot that all those pages you see are still rendered quite fast.
I intend to use this concept in a page-selector; which can then paint the actual content in real time as its changed in the document without noticeable slowdown.

I have been seeing quite some good commits in KWord ODF loading from both Dipesh and Pierre Ducroquet.  Going through the <a href="http://testsuite.opendocumentfellowship.org>ODF testsuite</a> I notice more and more testcases that KWord is passing. Chakka!

Another redesign I made in KWord has to do with images. We now lazily load images from disk; so you can have a document with tens of thousands of high quality images loading quite fast and taking not a lot of memory. And only when the images is shown the first time will it be loaded from disk for a preview.  You can also set an image to be previewed in a low resolution to speed this up and conserve memory use.  I'll be sure to make KWord print the high-res one when printing, naturally :)

Redesigns are useful when you have to write new code anyway; as I noted  when I was going through our vast database of bugs for KWord I managed to close about 20 long open bugs that basically disappeared due to rewrites and redesigns at the lower level.

<a href="http://www.koffice.org/kword/pics/2007-07-footnote.png"><img align=left src="http://www.koffice.org/kword/pics/2007-07-footnote_tmb.jpg" border="0" alt="footnote"></a>An example of such a thing is a new approach I took to footnotes.  For those that don't know the term; its a often used concept of adding a marker in text, like a '1' and adding some text at the bottom of the page giving extra info.  KWord used to implement the concept using frames, which means that if you have a footnote in a multi-column document you still have a full-page-width footnote at the bottom of your page.  So I took a different approach and I embedded footnotes in the text-flake. Which means that the footnote is rendered at the bottom of the column in KWord.  If you use the text-flake in an application like Krita or karbon you will now also be able to see the footnote. Which people will surely be able to see usages for.  Creative people always do :)

Last item of today; Casper has started to do a rewrite of the ruler; which now looks like this:
<img src="http://members.home.nl/zander/images/2007-07-ruler.png" align="center">
Its still missing guide-lines and some other stuff it had in 1.x, but I believe he is working on that.  Let us know what you think!<!--break-->