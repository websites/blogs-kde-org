---
title:   "KOffice & interaction policies"
date:    2005-12-24
authors:
  - zander
slug:    koffice-interaction-policies
---
I'm a usability guy, so my first concern with any feature or application is consistency, I guess all you guys and galls know that rule of usability, its the most important one.
So, when in one day a couple of questions came up on how to scale a KPresenter object but keep the aspect ratio of the object, and how to disable the grid while scaling, well, I got thinking and I started looking into what all the KOffice applications did.
In short, each one invented its own way of doing things. Which, as my introduction states, is not good usability. Duh!
I kicked off a research and a proposal based on that to the KOffice mailinglist, and a couple of days later, the KOffice team now has a nice set of what I call interaction policies.  All the features that make sense for manipulating objects now have been defined and given a modifier key. So holding shift down while moving or rotating will disable the grid and guides in all KOffice applications.
Consistency is good.

Last week I committed the implementation for KWord so at least ¾th of the policy is working there.  This means a lot of nice new features that make KWord a lot easier to work with.
You can now, for example, scale a series of frames all at the same time and if you hold down alt it will even keep the aspect ratio.
Together with this work I took another long look at how you select and interact with frames and they should in general make you loose a lot less hair.  So if you find the new version of KWord just easier to work with, remember me :)

Several other KOffice applications have been making steps to implement the office-wide policy, but developer time is scarce and every helping hand is appreciated.  So if you are good at reading code and have time in the holiday season, please point your webbrowser to the <a href="http://www.koffice.org/developer/keyboardmodifiers.php">policy</a> and see what you can do! (not for the weak of heart, though. But you don't want to get bored during the holidays!)

KWord has seen loads and loads of bugfixes in the last couple of weeks and before that I have basically been busy refactoring the code to follow more general designs (thing GOF) so new people can read in more easy.  Still loads to do, I'm afraid.  But things certainly are looking good.
Also, about a month ago KWord switched to the OpenDocumentFormat as a native fileformat, for all my files the conversion is perfect already, which makes me feel confident about the 1.5 release in a couple of months.  Be sure to test your docs and report any inconsistencies you find!

I hope you all have a great Christmas party, and be sure to try out that KWord trunk under your KDE 3.5 soon, I think it deserves some attention :)<!--break-->