---
title:   "What I've been reading."
date:    2007-05-06
authors:
  - zander
slug:    what-ive-been-reading
---
A great example of grass roots;  educate the educators about the brave new world and supporing critical thinking about MSWord and .doc 
&nbsp;<a href="http://kairosnews.org/critical-thinking-word-doc">Critical Thinking About Word and .doc</a>

One of the things most of us hate most about flying is that you always get the feeling you get ripped off. Naturally, the airliners are just following the basic economy rule of charging what people are willing to pay.  It seems some people have found a way to do the same with open source software. See;
&nbsp;<a href="http://tech.cybernetnews.com/2007/04/30/how-much-is-free-software-worth-on-ebay/">How much is Free Software Worth on eBay?</a>

Next week a so called developer sprint will take place in Berlin to enhance OpenDocument support in KDE. Starting with KOffice.  The linked article says that kdelibs integration is one of the aims; I understand that due to funding issues that part won't happen. So the title is not as accurate as it should be. 
&nbsp;<a href="http://dot.kde.org/1177773146/">KOffice Developers Meet with KDE Core People for ODF Infrastructure</a><!--break-->