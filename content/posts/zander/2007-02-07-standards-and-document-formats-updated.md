---
title:   "standards and document formats [updated]"
date:    2007-02-07
authors:
  - zander
slug:    standards-and-document-formats-updated
---
Since January 5th there has been a bit of a rush, if not stress to work on standards.
If you may recall, in an earlier blog I posted about Microsofts answer to the <a href="http://blogs.kde.org/node/2492">OpenDocumentFormat</a>. Which got rubber-stamped as ecma 376 late last year.  The ecma seal of approval was not enough for Microsoft. Most probably because it was fighting the ISO approved ODF spec, even if they never said so out loud.
And it makes sense.  The number one request any free office suite gets is that it should be able to read MSOffice docs. Or more accurately, it should be able to read the microsoft invented fileformat.  So, MS has the advantage that people rely on their suite because the information stored in documents can only be read by their software. This means that even if KOffice is better than MSOffice for a company, they would have problems if their old docs were not properly parsed by it.  In other words;  lock-in by fileformat.

This concept is not new to most people.  Its what MS does.  And when ODF started to really <a href="http://blogs.kde.org/node/2568">take off</a> to counter exactly that advantage, MS had to respond. See that's the disadvantage of being someone people rely on because they have to instead of they want to, no customer loyalty.  People walk away in a heartbeat when a competitor can give them what they want on better terms. The response to the open-standard specification of ODF that MS came up with was to create their own.  Well, without the open part. But their PR states that its open anyway.  First sign is that the first name was "Office Open XML". Hoping nobody would catch on.

So, Microsoft submitted it to fast-track ISO approval on January 5th.  This means that ISO has 30 days to object to this process or else it would be (relatively) smooth sailing though the approval process. The deadline for ISO-members to file objections has expired yesterday.  I, and quite some ODF proponents have been working hard to make sure this process would not sneak by everyone and get through unnoticed. The result payed off. There were <strike>19</strike> 20 official submissions.


<a href="http://www.consortiuminfo.org/standardsblog/article.php?story=20070206145620473">Andy Updegrove</a> wrote:
<ul>
Well the results are in, and an unprecedented nineteen countries have responded during the contradictions phase - most or all lodging formal contradictions with Joint Technical Committee 1 (JTC), the ISO/IEC body that is managing the Fast Track process under which OOXML (now Ecma 476) has been submitted.  This may not only be the largest number of countries that have ever submitted contradictions in the ISO/IEC process, but nineteen responses is greater than the total number of national bodies that often bother to vote on a proposed standard at all. </ul>

This just warms my heart. I must say.  Really good to see that people around the globe want a true open fileformat. And that enough people went through MS detox to put their foot down and say so.

This won't be the end, of course.  And its not the only attempt to continue a very profitable monopoly that MS has or will make.
Marbux wrote this on a public mailinglist:
 <ul>But it also takes complete absence of contrary ballots to win win final ISO adoption by any track. It's a consensus process, a negotiation among the nations signatory to the treaty. The fact that so many nations vetoed already would, I think, stir even more nations to veto adoption even if Ecma 376 were resubmitted on another track.
I'm sure that Microsoft's lobbyists realize that. So this might be pretty much the end of the Ecma 376 standardization attempt. That's a surmise, not a prediction. :-)</ul>

If nothing more, this will buy us time. So I'm back to coding on KOffice. Its what I do best, anyway :)
<b>update</b>; today I heard that there actually were 20 objections, not 19. Sorry for the mixup.
<!--break-->