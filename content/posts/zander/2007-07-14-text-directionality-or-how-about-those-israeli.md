---
title:   "Text directionality, or how about those Israeli?"
date:    2007-07-14
authors:
  - zander
slug:    text-directionality-or-how-about-those-israeli
---
The last couple of days [1] I've been working on bi-directional text in KWord.  KOffice as a whole is aimed to be used worldwide, in all sorts of environments and it has to be usable for all sorts of scripts and languages.  I'm proud to tell you that the todo list on getting that done is starting to be really really small.

Here is a point wise list of things that we now do properly.

<ul>
<li>KWord and KOffice supports Unicode 5.0.  This means in rendering the fonts and loading/saving as well as being able to type all known scripts.</li>
<li>BiDi text rendering, and cursor navigation works as expected.</li>
<li> KOffice text now has smart auto detecting for directionality of text while the user it typing it. And it will change the direction properly.</li>
<li> Inheriting of text direction is made possible; you can set the direction on a document so a latin paragraph in hebrew text is not automatically changed since that would look out of place.</li>
<li>The alignment buttons are properly positioned and function as expected for all text, and also when the application is shown right-aligned (--reverse for us europeans).  This was a long standing bug that I fixed.</li>
<li>Smart showing of directionality button.  You can manually alter the direction of a paragraph by clicking a button in the docker. This button will only be shown if the document has mixed directionality already.</li>
<li>List items and brackets are properly reversed on right-to-left layout.  A user having a number and a list-item suffix of ")" will see that being a "foo (3" on a right-to-left paragraph.</li></ul>

Last year I had some Hebrew lessons from a friendly Israeli, the rest of this stuff I got from books. So please do download KWord alpha2 in a couple of weeks and tell me if I missed anything!



1) you may have noticed these commits in svn some time ago since I wrote this blog a week or two back already while kdedevelopers.org was down.<!--break-->