---
title:   "frameworks in Jenkins"
date:    2011-11-16
authors:
  - tnyblom
slug:    frameworks-jenkins
---
kdelibs frameworks branch is now covered in build.kde.org :)

But due to a bug in the build system it requires a special hack to build, ie the cmake call does all the required work but fails with a message about inqt5 not being in the export set. So to make it build in Jenkins the exit code from cmake has to be ignored. Other then that it seems to work ok.