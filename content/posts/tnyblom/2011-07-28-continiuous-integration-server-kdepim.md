---
title:   "Continiuous Integration server for KDE(PIM)"
date:    2011-07-28
authors:
  - tnyblom
slug:    continiuous-integration-server-kdepim
---
I'm experimenting with providing a CI (continuous integration) server for KDEPIM.

For a while I've been running a cronjob that triggers a cmake script. This script then polls the repositories for 24 hours and every time a change was detected a build was performed. The results from this was feed to http://my.cdash.org (kdepim, kdepimlibs and kdepim-runtime).

As I lacked some features I'm now trying Jenkins (http://jenkins-ci.org). This allows build information to be published to one or more IRC channels as well as sending mails. The plan is to enable the mail alerts as soon as I feel that the setup is stable.

For now the server can be found at http://kde-ci.nyblom.org
