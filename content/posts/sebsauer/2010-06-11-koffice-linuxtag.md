---
title:   "KOffice on Linuxtag"
date:    2010-06-11
authors:
  - sebsauer
slug:    koffice-linuxtag
---
My yesterdays presentation about KOffice Version 2 at the Linuxtag was received overhelming good.

The presentation started with me introducing koffice and telling what's my role within that project. The question what KOffice is was probably answered best with the <a href="http://dot.kde.org/sites/dot.kde.org/files/KOffice_Sprint_2009_group_photob.jpg">picture</a> of one of our sprints. Based upon my previous experience with the Linuxtag it was clear that I had to address the technial aspects of KOffice. Our Qt+KDE base, our portability (Windows, OSX, Unix, Haiku, x86 and ARM) and our frameworks (most notable our ODF library, kotext, flake and the textshape as concrete sample).

Then I went on with the interoperability topic. First by providing an introduction about OpenDocument/ODF and the MSOffice filters (2000/2003 binary vs 2007/2010 XML) and then by showing how we solved interoperability between them in KOffice as visualized in the following diagram.

<img src="https://blogs.kde.org/files/images/KOfficeFilterChain.jpg" />

What the diagram shows is the way a MSOffice document takes in KOffice. First the MSOffice filter reads the MSOffice document and translates it into a OpenDocument. Then the OpenDocument is passed on to the KOffice application and then read and finally displayed by the application.

The KOffice applications and libraries are implementing support for OpenDoument and OpenDocument only. That means that any other format, including the MSOffice file formats, is not implemented in the application and libraries themself like the OpenDocument format. That in turn means that other filters either needs to access the applications API direct (not recommed) or they need to output OpenDocument to pass it on to the application (recommed). That, the recommed way, is what the MSOffice import filters are doing. They are reading MSOffice documents and are translating them to OpenDocument to pass the produced OpenDocument on to the application.

This results in a network-effect. Any work done on other filters like those for the MSOffice file format, does also indirectly improve the OpenDocument implementation in KOffice. This is the case cause the filters are producing OpenDocument and are testing our OpenDocument implementation and are then either verifying that things are working as expected or are showing bugs that need to be fixed.

Another positive effect of that design is that we got a clear separation between MSOffice related code and our applications and libraries. The MSOffice related code can focus on transformation between two formats and does not need to provide logic to manipulate documents. The applications and libraries in turn don't need to know anything about other formats or even that there exist other formats. They only care about OpenDocument and OpenDocument only.

Yet there is one more positive thing coming out of this. The MSOffice related code is in itself a rather interesting standalone solution. I am sure that the translation between those both de facto standards is an interesting topic even outside of the KOffice or Linux universes. The code does in fact document the differences between the both ISO standards, a nice read for a raining day.

Later, that is after my presentation, I had a few nice talks with KOffice users, potential new users and potential new developers. Then we hang out a bit at the KDE booth till we left for the social event. Eating, drinking, dancing and after amazing 3 hours sleep in a row I had to catch the train to move on to the KOffice sprint 2010 which takes place this weekend.
