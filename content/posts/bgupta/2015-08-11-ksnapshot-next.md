---
title:   "KSnapshot-Next"
date:    2015-08-11
authors:
  - bgupta
slug:    ksnapshot-next
---
KSnapshot is getting an overhaul.

It's actually a little more complicated than that. I started to work on the KF5 port of KSnapshot (EDIT: no, contrary to what Phoronix claims this port is not my work; I simply wanted to fix anything that needed fixing) sometime in early March this year, before I realised that the codebase, while perfectly in order for being a X11-only screenshot taker for KDE (yes, KSnapshot actually has a complete and fairly decent KF5 port in its frameworks branch on KDE Git), was in need of a major overhaul if we were going to get proper Wayland support in.

To that effect, I started working on a completely new screenshot application, copying in the bits and pieces of code from KSnapshot that I could actually use. It's called KScreenGenie, and has been living in KDE's Git infrastructure for quite a long time. It's currently in KDE Review, and will be moved to KDE Graphics in time for the Applications 15.12 release. Not just that, it will be renamed to KSnapshot, so people upgrading their computers will seamlessly upgrade from KSnapshot to KSnapshot 2.0 a.k.a. KScreenGenie.

But KScreenGenie in its current form and with its current name is actually going to see a public release. The code in the master branch of the git repository is currently in doc and string freeze, and is actually considered stable enough for daily use. A distribution called <a href="http://kaosx.us/">KaOS</a> already ships it, I've been using it as my primary screenshooting tool for months now, and other KDE developers have also spent time using and testing it and fixing minor issues. So barring major blocker bugs that pop up anytime soon, KScreenGenie 2.0.0 will be released (independently of the KDE Applications) on <b>August 15, 2015</b>.

If you're so inclined, here's a little bit of technical information on how KScreenGenie is different from KSnapshot. The biggest internal change is how pictures are actually acquired - instead of using Qt's built in screenshooting APIs, KScreenGenie uses the native API for the platform that it's running on. Currently, only one working platform backend exists, and that's for X11, using xcb. We use libkscreen for properly figuring out screen layout information so that we can take proper multi-monitor screenshots (we don't support Zaphod mode though). On Wayland, because the platform specific bits are so well separated from the platform-independent bits, the application starts up, but does not actually take a screenshot. The reason is that there's no stable API for taking screenshots in Wayland yet (Weston has its own API, and while some of KWin's screenshot effect APIs work, all don't), and thus we don't actually have a working Wayland image grabber yet.

A particularly nice user-facing feature is the ability to take screenshots of transient windows (pop-up menus, for example) along with the parent window. In KSnapshot, if you chose "Window Under Cursor" as your mode and hover over a pop-up menu, only the pop-up menu is captured (technically the pop-up menu is a X11 window). In KScreenGenie, we actually have code to detect if the window is a transient window and if so, we try to figure out who the parent window is and then take a composite shot of both the parent and the transient. Note though, that this currently only works with Gtk3 and Qt5 applications, since only these toolkits set the WM_TRANSIENT_FOR property on their pop-up windows, thus enabling us to figure out who their parent is.

I'm also currently working on a basic image editor integrated into KScreenGenie for editing and annotating screenshots within KScreenGenie itself. The code isn't online yet, since it's nowhere near even half-baked, but hopefully it'll be there by 15.12 or at the very most 16.04.

I'd like to encourage distributions to package KScreenGenie 2.0.0 in their primary repositories, and users to use it, test it and report bugs. Distribution packagers who do include KScreenGenie in their repositories do need to mark the KSnapshot package from Applications 15.12 as replacing, providing and obsoleting the KScreenGenie package though.

Happy clicking!