---
title:   "KDE BBQ"
date:    2005-07-16
authors:
  - fab
slug:    kde-bbq
---
Tommorow we are having a <a href="http://www.kde.nl/agenda/2005-07-17-bolsward-bbq/">BBQ</a> with people of <a href="http://www.kde.nl/">KDE-NL</a>. We did this <a href="http://www.kde.nl/agenda/2004-06-13-nijmegen-bbq/">before</a> and we agreed to have a bbq each year during summer. <a href="http://kde-rinse.blogspot.com/">Rinse</a> is our host this year. As he is a cook, I have high expectations of this year's bbq. Anyway I need to travel (by car) to <a href="http://www.bolsward.nl/">Bolsward</a>, the North of the Netherlands and crossing <a href="http://www.deltawerken.com/The-Afsluitdijk/119.html">The Afluitdijk</a>. I am looking forward seeing my KDE friends again. Also some new faces will be there as well and even some KDE oldies (Hi Otto). <!--break-->