---
title:   "\"We're getting closer to mainstream press\""
date:    2005-09-21
authors:
  - fab
slug:    were-getting-closer-mainstream-press
---
<a href="http://mail.kde.org/pipermail/kde-promo/2005q3/003218.html">said George Staikos</a> on the <a href="https://mail.kde.org/mailman/listinfo/kde-promo">kde-promo mailinglist</a>. So it seems that the August edition of <a href="http://www.newsfactor.com/magazine/">NewsFactor magazine</a> quoted some of our members of KDE community like Chris Schlaeger and <a href="https://blogs.kde.org/blog/91">Thomas Zander</a> in an article titled "Revisiting the GUI Admin Debate". As I am not able to get that magazine here in the Netherlands I asked George for some proof. 

<A href="http://www.xs4all.nl/~leintje/stuff/articlekdequote_c.png"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/articlekdequote_c_small.png" border="0"></A>. 

I noticed you can read the <a href="http://www.newsfactor.com/story.xhtml?story_id=35650">article online</a> as well.<!--break-->