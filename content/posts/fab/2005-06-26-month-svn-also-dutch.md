---
title:   "\"This Month in SVN\"  also in Dutch"
date:    2005-06-26
authors:
  - fab
slug:    month-svn-also-dutch
---
<a href="https://blogs.kde.org/user/view/808">Canllaith</a> recently started a new series on the Dot called <a href="http://dot.kde.org/1119602849/">This Month in SVN</a>. This is indeed a cool idea to give non-technical people some insight on what is happening in SVN. <a href="http://bram85.blogspot.com/">Bram</a> is also <a href="http://dot.kde.org/1119602849/1119603998/">impressed</a> with these series. He suggest that perhaps localization could be done on these series, like we do with <a href="http://www.google.com/u/dot?as_q=Application%20of%20the%20Month&as_epq=&as_oq=dept.&as_eq=&num=100">Application of the Month</a>. 

Bram created a <a href="http://www.kde.nl/future/">new section</a> on the KDE-NL website where <a href="http://www.kde.nl/future/20050624/index.html">translated issues</a> of "This Month in SVN" can be found. 

One last word to Bram. Are you able to support this effort for a longer time? Perhaps find some people to help you. I ask this because KDE-NL already have several translation efforts going and we sometimes have some trouble to keep on supporting those efforts. Perhaps I am too concerned :) 

Anyway Bram, as always, an excellent job and many thanks to Canllaith for these cool new series <!--break-->