---
title:   "More KDE on laptops"
date:    2005-05-05
authors:
  - fab
slug:    more-kde-laptops
---
Perhaps some people remember the <a href="http://dot.kde.org/1094715499/">article</a> about HP selling laptops with a KDE desktop on it. Acer is selling <a href="http://www.acer.co.th/product/travelmate/Aspire3000/index_p.htm">Linux laptops</a> now. Distribution used is <a href="http://www.linpus.com.tw/Products/Desktop/Linpus_92/default.htm">Linpus Linux 9.2</a>. It <a href="http://www.linpus.com.tw/Products/Desktop/Linpus_92/screen_shots.htm">uses</a> the "Friendly KDE desktop environment" as its default desktop  :) <!--break-->