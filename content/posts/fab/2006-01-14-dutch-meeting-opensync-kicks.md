---
title:   "Dutch Meeting OpenSync kicks off .."
date:    2006-01-14
authors:
  - fab
slug:    dutch-meeting-opensync-kicks
---
<A href="http://blogs.kde.org/node/1482">Long time ago</A> that I have blogged! A lot happened in my life. In short: I am pretty happy these days. Perhaps more about that some other day

Now something even more important .

Currently the <A href="http://www.opensync.org/wiki/dmo/">Dutch Meeting OpenSync</A> is taking place. <a href="http://www.opensync.org">OpenSync</a> is a synchronisation framework which is to be used <a href="http://www.apple.com/">on</a> <a href="http://www.microsoft.com/windows/default.mspx">several</a> <a href="http://www.linux.org/">platforms</a> and even more easily within <A href="http://www.gnome.org/">several</A> <A href="http://www.kde.org/">desktop</A> environments we currently enjoy. Let's hope that we can now really say that syncing Just Works &#8482; in KDE.

First of all I would like to thank our sponsors <A href="http://www.nlnet.nl/">NLnet</A> and <A href="http://www.ibm.com/nl/">IBM Netherlands</A> for making this meeting possible. Also I would like to thank Frank Zwart, my partner in crime for organizing this event (he even <a href="http://fzwart.blogspot.com/">started blogging now</a>). 

We have taken some pictures ... and I promise that tommorow I will try to blog a bit earlier.

<!--break-->

<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0054.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0054.jpg" border="0"></A><br>IBM HQ Netherlands<br>Yesterday, Armin, the author of <A href="http://www.opensync.org/">OpenSync</A>, arrived at Amsterdam together with Tobias Koenig (from the <A href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</A> fame). This morning <A href="https://blogs.kde.org/blog/54">Cornelius</A> also arrived and we were at IBM HQ around 9 AM.</p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0055.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0055.jpg" border="0"></A><br>From left to right: Armin, Cornelius and Tobias.<br>Yes .. these KDE developers are well fed, thanks to Ankie from IBM Forum.</p>


<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0058.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0058.jpg" border="0"></A><br>For our sponsors Armin did a presentation about OpenSync which you can find at the OpenSync website.</p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0059.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0059.jpg" border="0"></A><br>I am doing some accounting here.</p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0072.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0072.jpg" border="0"></A><br>And we ordered some more food! Pizza my dear friends.</p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0073.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0073.jpg" border="0"></A><br>Tobias showing off his pizza. Get your kicks here.</p>


<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/img_0078.jpg"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/scaled/img_0078.jpg" border="0"></A><br>Food, fun and love from Holland!</p>