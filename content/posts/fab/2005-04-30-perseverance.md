---
title:   "Perseverance"
date:    2005-04-30
authors:
  - fab
slug:    perseverance
---
<a href="http://pim.kde.org/development/meetings.php">KDEPIM meetings</a> appear to be successful for the progress of KDEPIM/Kontact. So the Dutch local KDE group, <a href="http://www.kde.nl">KDE-NL</a>, decided to organise such a meeting as well. 

For this we needed money. Although KDE-NL has some budget of its own it was clear that we needed some extra money for funding the accomodations and for some people help them pay for the travelling costs. 

To be honest I was quite surprised that (local) companies or organisations were willing to chip in money for such a meeting. I am especially thankful to the kind people of <a href="http://www.nlnet.nl/">stichting NLnet</a> and <a href="http://www.nluug.nl/">NLUUG</a> for supporting this effort. We even have more companies and people who are backing this initiative like <a href="http://www.kdab.net/">KDAB</a>, <a href="http://www.trolltech.com/">Trolltech</a>, the online shop <a hrfe="http://shop.mensys.nl/index.html">Mensys</a>, <a href="http://www.novell.com/netherlands/">Novell Nederland</a>, Patrick Smits and last but not least Jeroen baten with his company <a href="http://www.i2rs.nl/">i2rs</a>. 

So there you have it ... we managed to create some budget. In return we organise a sponsor session where we have 4 talks presented to our sponsors and some (of their) guests. For KDAB and Trolltech it will be a bit difficult I suppose to come over to the Netherlands for a 4 hour sponsor session :) 

The <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/crew_en.php">line up of KDEPIM developers</a> coming to the Netherlands is really impressive. Not yet on the list are the people of <a href="http://www.relevantive.com/index_e.html">relevantive</a> (Jan and Ellen) who are coming as well and will offer their services during the meeting. Also <a href="https://blogs.kde.org/blog/91">Thomas</a> promised to come as well :) (good thing dude). 

How did KDE-NL manage to pull this off? One word: perseverance! 
<!--break-->