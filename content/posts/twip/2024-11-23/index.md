---
title: "This Week in Plasma: Battery Charge Cycles in Info Center"
discourse: ngraham
date: "2024-11-23T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

This week we of course continued the customary bug-fixing, but got some nice new features and UI improvements too!

Let me also remind folks about KDE's [end-of-year fundraiser](https://kde.org/fundraisers/yearend2024). We're 84% of the way to our goal, and it would be *amazing* to get all the way to 100% before December! Then we can focus on those stretch goals from December to January.

Anyway, enough of the sales pitch, back to the free stuff!

And isn't that amazing? Let's zoom out a bit here and remind ourselves just how incredible it is that this software is made available for free, with no contract or license agreement, to everyone. To you, to your school, to community organizations, businesses, governments, even our direct competitors to study and examine (which goes both ways, and helped me fix a bug in GTK this week; read on for details). It's kind of wild, if you think about it. But, here we are, and we want to keep on being a light in a tech world that sometimes seems to be darkening. Help us [keep that light glowing](https://kde.org/fundraisers/yearend2024)!


## Notable New Features

Info Center now shows your battery's cycle count. (Kai Uwe Broulik, 6.3.0. [Link 1](https://invent.kde.org/frameworks/solid/-/merge_requests/192) and [link 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/213))

![](battery-charge-cycles.png)

Added the ability to convert to and from the CFP franc currency in KRunner-powered searches. (someone going by the pseudonym "Mr. Athozus", Frameworks 6.9. [Link](https://invent.kde.org/frameworks/kunitconversion/-/merge_requests/51))


## Notable UI Improvements

Middle-clicking on the Brightness and Color widget no longer does anything when the Night Light hasn't been turned on. (Elias Probst, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=496457))

Improved some sources of visual awkwardness in System Monitor: now the loading screen no longer sometimes has a scrollbar; and clicking something selected in a table view visibly de-selects it. (Akseli Lahtinen, 6.2.4. [Link 1](https://bugs.kde.org/show_bug.cgi?id=494902) and [link 2](https://bugs.kde.org/show_bug.cgi?id=494261))

Improved the way Discover presents external links to be less visually heavy. (Nate Graham, 6.3.0. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/972))

![](discover-app-external-links.png)

Re-did the "Apply Plasma Settings" dialog on System Settings' Login Screen page to look better and more consistent with other dialogs in QML-based software these days. (Oliver Beard, 6.3.0. [Link](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/83))

![](sddm-kcm-apply-plasma-settings-dialog.png)


## Notable Bug Fixes

Fixed a regression in the Power and Battery widget that broke its ability to notice that `power-profiles-daemon` was installed instead of `TLP` after some porting work. (Méven Car, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495918))

Fixed a regression that caused the Disks & Devices widget to not show the correct actions for non-mounted optical discs after some porting work. (Kai Uwe Broulik, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=469706))

Fixed an issue that caused screenshots and screen recordings to look too dim while using HDR mode. (Xaver Hugl, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=496185))

Fixed a case where Plasma could crash when logging in with an external screen connected to a laptop via HDMI. (Marco Martin, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=487684))

Fixed a rare case where Plasma could crash when copying data to the clipboard. (David Edmundson, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4923))

Fixed a bug affecting people using panels in "Fit content" mode that could, under certain circumstances, cause them to be too small until you manually entered Edit Mode once. (Niccolò Venerandi, 6.3.0, [Link](https://bugs.kde.org/show_bug.cgi?id=489086))

KWin now behaves better when you plug in a weird defective TV that asks for an inappropriate resolution. (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/5452))

Discover once again shows update-able "Get New [Stuff]" content on the updates page. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=492455))

XWayland-using apps can no longer crash KWin with ludicrously large icon sizes. (David Redondo, Frameworks 6.9. [Link](https://invent.kde.org/frameworks/kwindowsystem/-/merge_requests/164))

Fixed a bizarre and annoying bug that caused text displayed at fractional scale factors in Plasma and QtQuick-based KDE apps and to look, for lack of a better term, wobbly. Wobbly windows good, wobbly text bad! Text has now been put on the straight and narrow. (David Edmundson, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=479891))

Fixed a strange Qt bug that manifested as Plasma notifications sometimes being vertically squished. (David Edmundson, Qt 6.8.1. [Link](https://bugs.kde.org/show_bug.cgi?id=496201))

GTK 3 apps once again have the correct icon for their spinboxes' "decrease the value" buttons when using the Breeze icon theme or any other icon theme whose `list-remove` icon isn't a minus sign. (Nate Graham, GTK 3.24.44. [Link](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7954))


Other bug information of note:

* 2 Very high priority Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 35 15-minute Plasma bugs (down from 36 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 94 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-11-16&chfieldto=2024-11-23&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

The feature to let you record the screen without re-approval now also works for virtual outputs. Additionally, virtual outputs now have a better name that indicates which app records them. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/329))

Fixed a memory leak caused by having a lot of OverlayFS mounts, e.g. from Docker containers. (Joshua Goins, Frameworks 6.9. [Link](https://invent.kde.org/frameworks/solid/-/merge_requests/190))

## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Filter and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
