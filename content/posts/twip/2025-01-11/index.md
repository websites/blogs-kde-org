---
title: "This Week in Plasma: Final Plasma 6.3 Features"
discourse: ngraham
date: "2025-01-11T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week the focus was on landing final Plasma 6.3 features and UI changes… and land them we did! Now it's time to spend the next month exclusively on bug-fixing and polishing.


## Notable New Features

When notifications arrive while "Do Not Disturb" mode is engaged, exiting that mode now shows a single notification informing you of how many you missed, rather than sending them all in a giant unmanageable flood. (Fushan Wen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=440837))

The desktop context menu for symbolic links now includes a "Show Target" menu item, just like one one in Dolphin. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/312330))

![](show-target-i-have-rounded-the-corners.png)

The System Monitor app and widgets are now capable of collecting GPU statistics on FreeBSD. (Henry Hu, 6.3.0. [Link](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/100))


## Notable UI Improvements

If you didn't like the change in Plasma 6.2 to use symbolic icons in Kickoff's category sidebar, you can now undo it yourself; we changed the implementation to pull icons from the standard data source, so you can set them to whatever you want using the Menu Editor app. (David Redondo, 6.3.0. [Link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2700) and [link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5038))

Reduced the clutter on the Edit Mode toolbar, making its contents more focused and relevant. (Nate Graham, [Link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2695), [link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5027), and [link 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2685))

![](cleaner-edit-mode.png)

The DrKonqi crash reporter/browser app's main windows now remember their size, maximization state, and (on X11), position on screen. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498191))

External web links in Kirigami-based apps such as Discover now show the typical "arrow pointing out of a square" icon to make this more clear. (Carl Schwan and Nate Graham, Frameworks 6.11. [Link](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1693))

![](external-link-icons.png)

Modernized the UI style of the standalone printer-related apps that are not yet integrated directly into the System Settings page. (Thomas Duckworth, 6.3.0. [Link](https://invent.kde.org/plasma/print-manager/-/merge_requests/210))

![](slightly-more-modern-printer-app-ui.png)

All close buttons throughout KDE software are now consistent; we've standardized on the black X. As part of this, we also changed the timeout indicator on Plasma notifications to not be dependent on any particular close button icon appearance, as was the case before. (Nate Graham, Plasma 6.3.0 with Frameworks 6.11. [Link 1](https://invent.kde.org/teams/vdg/issues/-/issues/62), [link 2](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/349), and [link 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4292))

![](new-notification-close-icon-and-timeout-UI.png)

System Settings' Night Light page has moved from the "Colors & Themes" group to the "Display & Monitor" group, which is a more natural and expected place for it. (Kisaragi Hiu, 6.4.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5044))

In Plasma's Networks widget, there's now a "Configure" button for networks that you've used in the past but aren't currently connected to. (Kai Uwe Broulik, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/399))


## Notable Bug Fixes

Plasma no longer crashes when you switch the desktop from "Folder" containment to "Desktop" containment, and then back. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498411))

The session restore "Excluded applications" list you can populate yourself now actually takes effect on Wayland. Also, you now list apps by their desktop file names, which lets the feature work for apps whose executable is ambiguous, such as Flatpak apps. (Harald Sitter, 6.3.0. [Link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5039) and [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2701))

Fixed a bug that could cause full-screen windows being screencasted to freeze under certain circumstances. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495287))

Made laptops more robust against waking up while the lid is closed. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=476492))

Tooltips for favorited apps in Kicker once again appear as expected, and don't disappear immediately on hover. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495262))

Typing text into KRunner that matches a history item but with different capitalization no longer causes the grayed-out auto-completion text to de-sync with the text you already typed. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497603))

Plasma no longer unnecessarily shows you an OSD indicating the default audio device when you return from a different TTY. (Kai Uwe Broulik, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/297))

Time zones shown in the Digital Clock widget's popup are once again sorted by time, rather than randomly. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498379))

Fixed a visual glitch that could manifest as brief graphical corruption when interacting with pages in the clipboard settings dialog in a certain way. (David Edmundson, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494169))

Fixed a bug in the Wayland session restoration feature that could make it inappropriately restore multiple instances of apps. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497980))

In Discover, app pages and pages with lists of apps are no longer inappropriately horizontally scrollable. (Nate Graham and Ismael Asensio, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=477493) and [link 2](https://invent.kde.org/plasma/discover/-/merge_requests/1007))

Fixed an issue in `Kirigami.Icon` that affected multiple Plasma widgets, whereby an icon from the active icon theme would be mistakenly provided instead of a custom image, in cases where that custom image was referenced from an absolute path and happened to have the same filename as a themed icon. (Marco Martin, Frameworks 6.11. [Link](https://bugs.kde.org/show_bug.cgi?id=498288))

Fixed a case where some dialogs in Kirigami-based apps such as System Monitor could have overflowing footer buttons in some languages. (Nate Graham, Frameworks 6.11. [Link](https://bugs.kde.org/show_bug.cgi?id=497795))

Other bug information of note:

* 3 Very high priority Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 36 15-minute Plasma bugs (up from 34 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* At least 138 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-01-03&chfieldto=2025-01-10&chfieldvalue=RESOLVED&list_id=3000437&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Reduced the System Monitor app's background CPU usage down to 1-3% with some clever internal restructuring. (Arjen Hiemstra, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=434877))

Removed a bunch of unnecessary old "sanity checks" on login that were not actually providing any additional sanity, and could even prevent login under certain circumstances! (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5036))

Improved performance on certain GPUs while Night Light is active; previously it could sometimes be quite poor. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=496316))

It's now possible to pre-authorize apps for remote desktop access, so you don't have to wait for them to pop up an interactive permission dialog. Preliminary documentation can be found [here](https://develop.kde.org/docs/administration/portal-permissions). (Harald Sitter, 6.3.0. [Link](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/326))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
