---
title: "This Week in Plasma: Disable-able KWin Rules"
discourse: ngraham
date: "2024-11-30T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

This week there was a flurry of UI polishing work and a nice new feature to go along with the usual background level of bug-fixing. Some of the changes are quite consequential, being minor pain points for years. So hopefully this should be a crowd-pleasing week! If that's the case, consider directing your pleased-ness at [KDE's year-end fundraiser](https://kde.org/fundraisers/yearend2024)! As of the time of writing, we're at 98% of our goal, and it would be amazing to get to 100% by the end of November!


## Notable New Features

It's now possible to temporarily disable KWin window rules rather than fully deleting them. (Ismael Asensio, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=439476))

![](disabled-window-rule.png)


## Notable UI Improvements

Discover no longer shows you a bunch of irrelevant information about Flatpak runtime packages. (Nate Graham, 6.2.4. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/973))

The User Switcher widget now has a more sensible default height, fitting its content better. (Blazer Silving, 6.2.4. [Link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/639))

When you've disabled window thumbnails in the Task Manager widget, it now shows normal tooltips for open windows, rather than nothing at all. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=492530))

![](task-tooltip.png)

Wireless headphones that expose battery information properly (as opposed to headsets, which include a microphone) now get a better icon in the Power and Battery widget and low battery notifications (Kai Uwe Broulik, 6.3.0. [Link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4937) and [link 2](https://invent.kde.org/plasma/powerdevil/-/merge_requests/473))

KWin's Slide Back effect now has a duration that better matches the duration of other effects and animations, and responds more predictably and consistently to non-default global animation speed settings. (Blazer Silving, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6840))

On Ubuntu-based distros, the icon that appears in the System Tray alerting you to a major update available is now symbolic when using the Breeze icon theme, matching other such icons. (Nate Graham, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=399139))

Resizing windows for Qt-Quick-based apps should now look significantly better and smoother. (David Edmundson, Qt 6.9.0. [Link](https://codereview.qt-project.org/c/qt/qtwayland/+/591892))

Qt is now capable of displaying color emojis interspersed with black-and-white text when using the default font settings in Plasma. (Eskil Abrahamsen Blomfeldt, Qt 6.9.0. [Link](https://bugreports.qt.io/browse/QTBUG-85744))


## Notable Bug Fixes

Fixed a case where Plasma could crash when you dismiss a notification about network changes. (Nicolas Fella, 6.2.4. [Link](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/388))

Fixed a case where KWin could crash after running out of file descriptors when using certain non-Intel GPU drivers. (Xaver Hugl, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=496746))

System Settings no longer crashes when you plug in a mouse while viewing the Mouse page. (Nicolas Fella, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=495231))

Fixed a strange issue that would cause notifications to be mis-positioned after the first time you dragged any widgets that were on the desktop. This turned out to have been caused by the Plasma config file having old crusty System Tray widgets in it left over from prior Plasma customizations, which were competing for control over the positions of notifications. Now they're cleaned up properly, which also reduces memory usage, removes a ton of cruft in the config file, and may resolve other mysterious and random-seeming issues with notifications being positioned incorrectly. (Marco Martin, 6.2.5. [Link 1](https://bugs.kde.org/show_bug.cgi?id=472937) and [link 2](https://bugs.kde.org/show_bug.cgi?id=404641))

Fixed a bug that could make panels in "Fit Content" mode sometimes be too small when Plasma loads. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=489086))

Fixed one of the last remaining known bugs relating to desktop icons shifting around: this time due to always-visible panels loading after the desktop and sometimes pushing the icons away. Now that doesn't happen anymore! (Akseli Lahtinen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=458007))

Fixed a regression caused by a change elsewhere in the stack that interacted poorly with some questionable code on our side that caused the highlight effect on the tab bar in the expanded view for the active network in the Networks widget to be invisible. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495948))

Fixed a regression introduced with Frameworks 6.7 that caused many pieces of selected text in Plasma and QtQuick-based apps to become inappropriately de-selected after right-clicking on them. (Akseli Lahtinen, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=496214))

Other bug information of note:

* 3 Very high priority Plasma bugs (up from 2 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 37 15-minute Plasma bugs (up from 35 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 125 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-11-22&chfieldto=2024-11-29&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Made KWin more robust against apps that send faulty HDR metadata so that it is less likely to crash when encountering this condition. (Xaver Hugl, 6.2.5. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6849))

Made Plasma more robust against faulty widgets; now it is more likely to communicate a comprehensible error message rather than crashing. (Nicolas Fella, 6.2.5. [Link](https://invent.kde.org/plasma/libplasma/-/merge_requests/1225))

Made Plasma more robust against malformed .desktop files; now it similarly robust against more types of broken files. (Alexander Lohnau, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495606))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Filter and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
