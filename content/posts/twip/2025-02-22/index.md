---
title: "This Week in Plasma: Refinements All Around"
discourse: ngraham
date: "2025-02-22T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week, we've been rapidly fixing the bugs that people found in Plasma 6.3, as well as some older bugs as well. In addition to that, some smaller UI improvements have started to trickle in! There's some larger work in progress too, but not merged yet. Have a look at what did merge this week:




## Notable UI Improvements

### Plasma 6.3.1

Improved the weather widget's display of search results from the BBC weather service to reduce unhelpful visual noise. (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=500065))

Eliminated the visual difference between how Night Light looks on Wayland compared to on X11. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=500300))


### Plasma 6.4.0

The Digital Clock widget's context menu is now less cluttered with things you're not likely to use. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5139))

![](simpler-digital-clock-widget-context-menu.png)

Rephrased some settings on System Settings' General Behavior page to be clearer about what it is that they actually do. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2808))

![](more-comprehensible-visual-options-on-general-behavior-page.png)

Improved the accessibility of the Widget Explorer sidebar. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2807))




## Notable Bug Fixes

### Plasma 6.3.1

Fixed the issue mentioned last week where KWin built with LTO on GCC 15 could show a black screen on login when using an ICC profile; we found a way to restructure the code that avoids the issue. (Vlad Zahorodnii and Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=499789))

Fixed a case where Plasma could crash when you tried to access the Properties dialog for a file in the Recently or Frequently Used file lists in the Kickoff Application Launcher. (Nicolas Fella, [link](https://bugs.kde.org/show_bug.cgi?id=499845))

Fixed a regression that caused the volume change OSD to fail to appear when adjusting the volume with the integrated volume buttons of a Bluetooth headset. (David Redondo, [link](https://bugs.kde.org/show_bug.cgi?id=500129))

Fixed a regression that caused the <kbd>Meta</kbd>+<kbd>V</kbd> clipboard popup to lose its visual highlights when navigated by keyboard. (Christoph Wolk, [link](https://bugs.kde.org/show_bug.cgi?id=500055))

Fixed an issue in KWin that caused the new "Prefer efficiency" option when using an ICC profile to not actually be very efficient on some hardware, and another one that broke Night Light while using the "Prefer color accuracy" setting. (Xaver Hugl, [link 1](https://bugs.kde.org/show_bug.cgi?id=499987) and [link 2](https://bugs.kde.org/show_bug.cgi?id=500404))

Taking screenshots on Wayland in FreeBSD now works. (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=500261))

Fixed a few bugs in the Color Picker widget, such as the shortcut option not working, and the tooltip not looking correct in certain circumstances. (Christoph Wolk, [link 1](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/676) and [link 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/677))

Fixed a bug with the Task Manager widgets that broke the ability to move the pointer diagonally to a tooltip without dismissing it by accident while using a right-to-left language like Arabic or Hebrew. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2792))

Made several improvements and fixes for keyboard navigation in the Kicker Application Menu widget. (Christoph Wolk, [link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2811), [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2812), and [link 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2813))


### Plasma 6.3.2

Fixed a regression that caused desktop icons selected by dragging a box around them to become inappropriately deselected if the pointer ended right over one of the icons when releasing the mouse button. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=499898))

Fixed a regression that caused the automatic tablet mode feature to accidentally get blocked on certain types of devices, but only when using the feature to re-bind mouse buttons. (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=500025))

Fixed a bug that caused the desktop and panels to go missing when applying a new Global Theme and using the option to replace the existing layout. This also fixed a bug that caused deleted widgets to not be deleted from the `plasma-org.kde.plasma.desktop-appletsrc` config file. (Marco Martin, [link 1](https://bugs.kde.org/show_bug.cgi?id=498175) and [link 2](https://bugs.kde.org/show_bug.cgi?id=404641))

Fixed a set of subtle bugs in the implementation of the new "prefer symbolic icons" behavior of the System Tray that caused it to actually do the opposite, showing you colorful icons instead! (Nate Graham and David Redondo, [link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5227) and [link 2](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/175))

Extremely long weather station names no longer overflow and break the widget popup's layout. (Ismael Asensio, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/680))

The inline file renaming text field on the desktop is now colored correctly when using a mixed light/dark setup, as with Breeze Twilight. (Evgeniy Harchenko, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2829))

Limited the Power Management setting "Change screen brightness" to only take effect for built-in screens on battery-powered systems (e.g. laptops), which avoids certain timing-related brightness bugs for external monitors and makes the settings page less confusing. (Jakob Petsovits, [link](https://bugs.kde.org/show_bug.cgi?id=498771))


### Plasma 6.4.0

Fixed an issue that could cause user switching from KRunner to behave strangely and eventually cause a crash. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=500038))


### Frameworks 6.12

Fixed an older regression that broke the "highlight non-default settings" features for pages in System Settings written using QtWidgets. The fact that this was overlooked for so long goes to show how few are left these days! (David Redondo, [link](https://invent.kde.org/frameworks/kcmutils/-/merge_requests/257))


### Other bug information of note:

* 1 very high priority Plasma bug (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 30 15-minute Plasma bugs (up from 27 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 129 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-02-15&chfieldto=2025-02-21&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)




## Notable in Performance & Technical

### Plasma 6.4.0

Switched KWin's render loop initialization code to use a more precise type of timer that should reduce frame drops. (Apostolos Dimitromanolakis, [link](https://bugs.kde.org/show_bug.cgi?id=500500))


### Frameworks 6.12

When the `kded6` daemon crashes, now it automatically restarts itself in the background. (Bryan Liang, [link](https://invent.kde.org/frameworks/kded/-/merge_requests/57))




## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
