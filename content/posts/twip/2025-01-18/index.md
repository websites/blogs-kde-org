---
title: "This Week in Plasma: Getting Plasma 6.3 in Great Shape"
discourse: ngraham
date: "2025-01-18T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

We're barely a week into the Plasma 6.3 beta period, and Plasma's contributors are already fixing record amounts of bugs! The number of 15-minute bugs has dropped to the low 20s, and there's only one VHI priority bug left. But that's not all; they hammered on a ton more bugs as well, and did quite a bit of UI polishing! Lots of great news this week!

I know I say this about every Plasma release, but 6.3 is gonna be *gooooooood.* Grab a beverage; there's a lot here!


## Notable New Features

There are now several new search providers you can use from KRunner and KRunner-powered search fields, including Docker Hub, Mozilla Developer Network, and Nix Packages. (Aryan Tyagi, Frameworks 6.11. [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1780))

![](new-search-providers.png)


## Notable UI Improvements

The Weather widget now fetches information immediately after the network re-connects following a period of lost connectivity, rather than waiting for the next scheduled refresh interval. (Ismael Asensio, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498009))

KWin is now smarter about choosing a default scale factor for devices with small screens; now it won't choose a scale factor too high to be practical. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/7000))

KWin's automatic scale factor chooser now chooses a scale factor that's rounded to the nearest 5%, no longer to the nearest 25%. (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/7006))

Night Light is now colorimetrically correct when using an ICC profile. (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6974))

If your keyboard has a button to toggle the keyboard backlighting on and off, that button now works on the lock screen. (Yifan Zhu, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=483919))

When a panel de-floats and causes its pinned-open widget popups to also de-float, the popups' de-floatiness animations are now beautifully synced up so everything just looks great. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=481533))

<video class="img-fluid" controls>
  <source src="nice-de-float.mp4" type="video/mp4">
</video>

When right-clicking on a Task Manager icon to show the files and URLs it's opened recently, icons and labels for URLs are now displayed more appropriately. (Nicolas Fella, 6.3.0. [Link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2716) and [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2717))

Switching virtual desktops using <kbd>Meta</kbd>+<kbd>Alt</kbd>+<kbd>scroll</kbd> now goes in the direction you expect when you're using reversed/natural scrolling. (Yifan Zhu, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=483601))

Improved the descriptions of the accounts you can log into in System Settings, so it's a bit clearer what they can do for you. (Nate Graham, Plasma 6.3.0 and kaccounts-providers 25.04.0. [Link 1](https://invent.kde.org/network/kaccounts-providers/-/merge_requests/43), [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2720), and [link 3](https://invent.kde.org/network/kaccounts-integration/-/merge_requests/65))

![](better-online-account-descriptions.png)

In Spectacle — which has moved to Plasma so I'll be mentioning it here — you can now hold down the <kbd>Shift</kbd> key while drawing with the freehand or highlighter tools to constrain them to perfectly straight lines. (Noah Davis, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498327))

Made a number of keyboard navigation and accessibility improvements to Discover and the Kirigami UI components it uses. (Christoph Wolk, Plasma 6.4.0 and Frameworks 6.11. [Link 1](https://invent.kde.org/plasma/discover/-/merge_requests/1004), [link 2](https://invent.kde.org/plasma/discover/-/merge_requests/1003), [link 3](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1698), and [link 4](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1690))

Discover no longer shows the "Plasma Addons" category when not being used in Plasma. (Aleix Pol Gonzalez, 6.4.0. [Link](https://invent.kde.org/plasma/discover/-/issues/34))


## Notable Bug Fixes

It'll soon be once again possible to log into your Google account in System Settings — but unfortunately without Google Drive permission, since we have thus far been unable to demonstrate to Google's satisfaction that our software capable of interfacing with Google Drive is safe. This makes the account somewhat less useful to log into, but at least you can again. (Nate Graham, kaccounts-providers 24.12.2. [Link](https://bugs.kde.org/show_bug.cgi?id=480779))

Fixed a source of KWin crashes when the GPU drivers issue a reset, which they can do under various circumstances. (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6979))

Fixed a source of KWin crashes caused by the kernel sending unexpected data on certain hardware. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497180))

Fixed a case where System Settings' Wallpapers page would crash the app due to stale screen arrangement configurations. (Méven Car, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=489775))

Fixed a case where Plasma could crash while trying to generate window thumbnails or screen recordings. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/kpipewire/-/merge_requests/185))

Fixed a case where Plasma could crash if you disabled the Clipboard widget while its configuration window was still open. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497927))

Fixed a case where Plasma could occasionally crash after you cleared the clipboard history, especially with a very large history size. (Fushan Wen, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5055))

Fixed a random Plasma crash on Wayland. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5066))

Global shortcut keys no longer leak into applications under certain circumstances; this means for example that pressing <kbd>Alt</kbd>+<kbd>Space</kbd> to show and hide KRunner no longer also pauses and plays a video you happen to be watching. (Vlad Zahorodnii, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497245))

Fixed a regression in the Folder View widget which prevented the functioning of inline folder pop-ups, choosing custom or Places panel-based locations, and also the widget's displayed title when showing the contents of the desktop folder. (Nate Graham, 6.3.0. [Link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2731) and [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2729))

Fixed a regression that caused the panel to resize in a slow and laggy way while customizing its maximum length. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=487549))

Fixed a regression in the X11 session that caused auto-hide panels to lose the ability to display the "Alternatives" popup. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498195))

Switching desktop layouts from "Folder" to "Desktop" or vice versa no longer causes Sticky Notes on the desktop to lose their text, and also preserves desktop widgets' positions and sizes. (Marco Martin, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=497384) and [link 2](https://bugs.kde.org/show_bug.cgi?id=419590))

Fixed two bugs that prevented desktop icons from being clickable while they were on a scrollable part of the desktop, or when using right-alignment and top-to-bottom ordering. (Marco Martin, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=484148) and [link 2](https://bugs.kde.org/show_bug.cgi?id=497498))

The "invert screen" accessibility setting now does what it says it will do on Wayland. (Nicolas Fella, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=488967))

It's once again possible to authenticate in password dialogs using the credentials of an admin user other than yourself. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497522))

Fixed a bug that would cause the password dialog's password field to inappropriately become disabled after entering the wrong password when using `systemd-homed`. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=430828))

Worked around some issues with certain monitors and docks being dumb and buggy that could cause remaining monitors to get shut off after only one of them was unplugged or turned off. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=486328))

Remote input permission (e.g. for apps like [Input Leap](https://github.com/input-leap/input-leap)) no longer unexpectedly terminates when the display layout changes. (David Redondo, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494410))

Plasma OSDs no longer sometimes teleport to the top-left corner of the screen, e.g. when switching devices with multiple monitors. (Vlad Zahorodnii, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495194))

Power settings that trigger on a state change (e.g. plugged in -> on battery) once again work as expected when that state change happened while the system was powered off. (Jakob Petsovits, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497362))

The feature to create a desktop widget from something in the System Monitor app now works more reliably, by always putting the new widget on the primary screen's desktop rather than on an unpredictable desktop that might even be invisible due to its screen not being connected at that moment! (David Redondo, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=496768))

Improved the reliability of showing the right icon in the Task Manager for XWayland-using apps with broken or missing metadata. (Xaver Hugl and Nicolas Fella, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498533))

Apps that don't display correct metadata for the titles of their System Tray icons like Discord will now fall back to showing their tooltip text if that's set, so the tray icon doesn't end up with no label at all. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498153))

The Digital Clock widget's tooltip now shows seconds updating in real-time, as was always intended. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497296))

The Calendar widget once again looks correct when using a non-default panel thickness and/or icon theme. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498288))

Fixed a source of [Fitts' Law](https://en.wikipedia.org/wiki/Fitts%27s_law#Implications_for_UI_design) breakage for 24px thick attached full-width horizontal panels — yes, only with that exact combination of settings! (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=483808))

The highlight effect in the Kate Sessions widget now works properly. (Paul Worral, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=485477))

Other bug information of note:

* 1 Very high priority Plasma bug (down from 3 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 23 15-minute Plasma bugs (down from 36 last week!). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 136 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-01-11&chfieldto=2025-01-17&chfieldvalue=RESOLVED&list_id=3000437&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

The list of recent emojis stored by the Emoji Selector app is now considered to be "state" and stored in the state config file, rather than the settings config file — which is helpful for people who version-control their config files. (Nicolas Fella, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2699))

Documented in KDE's Human Interface Guidelines [how and when OSDs should be used](https://develop.kde.org/hig/status_changes/#on-screen-displays) in Plasma. (Nate Graham, [Link](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/553))

Made KWin more robust against screens with horribly broken built-in color profiles. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498616))

Fixed a variety of minor functional and display bugs relating to apps that export their shortcuts using the global shortcuts portal. (Tuxinal Tuxinal, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=474294), [link 2](https://bugs.kde.org/show_bug.cgi?id=474294), and [link 3](https://bugs.kde.org/show_bug.cgi?id=483640))

Ported KRunner's Converter runner away from its nested event loop, which has been a source of crashes in the past. (Fushan Wen, 6.4.0. [Link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/631))

`kscreen-doctor` has gained the ability to report screens' DPMS states. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495499))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
