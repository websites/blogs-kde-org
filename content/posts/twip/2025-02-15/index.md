---
title: "This Week in Plasma: Post-Release Polishing"
discourse: ngraham
date: "2025-02-15T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

[Plasma 6.3 is out!](https://kde.org/announcements/plasma/6/6.3.0) So far the response has been very good, but of course a few issues were found once it was in the wild.

Maybe the worst issue is [something that KWin devs have actually tracked down to a bug in the GCC compiler](https://bugs.kde.org/show_bug.cgi?id=499789), of all things! It only manifests with the kind of release build configurations that many distros use, and also only with GCC 15 and an ICC profile set up. We've informed distros how to work around it until the root cause is understood and GCC gets patched, or KWin devs are able to guard against it internally.

Unfortunately this is a sign that we did not in fact get enough beta testers, since the issue should have been obvious to people in affected environments. Another sign is that most of the regressions are hardware-related. We've got them fixed now, but we need people to be testing the betas with their personal hardware setups! There's simply no way for a small pool of KDE developers to test all of these hardware setups themselves.

Anyway, with those caveats aside, it looks like it's been a pretty smooth release! Building on it, there have been a number of positive changes to the Media Player widget, Weather Report Widget, Info Center Energy page, and touchscreen support.


## Notable new Features

### Plasma 6.4.0

The Media Player widget now features a playback rate selector when the source media makes this feature available using its MPRIS implementation. (Kai Uwe Broulik, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5082))

![](media-player-speed-selector.png)


## Notable UI Improvements

### Plasma 6.3.1

Improved the presentation of search results for the new DWD weather provider in the Weather Report widget. (Ismael Asensio, [link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5193) and [link 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/670))

The BBC Weather provider has recently improved the quality of their forecast data, so we've changed the weather widget to no longer hide search results from it if there are results from other providers as well. (Ismael Asensio, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/669))

The updates list in Discover is now sorted case-insensitively. (Aleix Pol Gonzalez, [link](https://bugs.kde.org/show_bug.cgi?id=499638))

Welcome Center now remembers its window size (and on X11, position) across launches, like most of our other QML app windows these days. (Tracey Clark, [link](https://bugs.kde.org/show_bug.cgi?id=499653))


### Plasma 6.4.0

Improved the graph view on Info Center's Energy page: Now it's in a card, like in System Monitor, and has more normal and visually pleasing margins. (Ismael Asensio, [link 1](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/230) and [link 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/227))

![](energy-page-with-fancy-card-graph.png)

Spectacle has gained support for pinch-zooming in its screenshot viewer window, which can be especially useful when annotating using a touchscreen. (Noah Davis, [link](https://invent.kde.org/graphics/spectacle/-/merge_requests/426))

You can now actually scroll through the Widget Explorer with a single-finger touchscreen scroll gesture, because dragging widgets using touch now requires a tap-and-hold. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=474929))


## Notable Bug Fixes

### Plasma 6.3.1

Fixed a regression that would cause KWin to crash in the X11 session when hotplugging or switching between HDMI screens. (Fushan Wen, [link 1](https://bugs.kde.org/show_bug.cgi?id=499846) and [link 2](https://bugs.kde.org/show_bug.cgi?id=499856)). Consider it a reminder for everyone still on X11 to try the Wayland session again, because the X11 session receives almost no testing from developers anymore!

Fixed a regression that could cause KWin to sometimes crash hours after hotplugging a Thunderbolt dock. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=500033))

Fixed a regression that would cause KWin to crash when you interact with the <kbd>Alt</kbd>+<kbd>Tab</kbd> task switcher while using software rendering. (Vlad Zahorodnii, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/7140))

Fixed a regression that could cause certain Qt-based apps to crash on launch when using the Breeze style. (Antonio Rojas, [link](https://bugs.kde.org/show_bug.cgi?id=499960))

Fixed a case where Plasma might sometimes crash when clicking on the Networks icon in the System Tray, especially when built using GCC 15. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=499927))

Fixed a regression that caused the new "Prefer efficiency" ICC color mode setting to not actually improve efficiency on certain hardware. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=499987))

Panels in auto-hide mode no longer inappropriately hide again when you start dragging Task Manager tasks to re-order them. (Tino Lorenz, [link](https://bugs.kde.org/show_bug.cgi?id=495828))

The new bar separator between the date and time in the Digital Clock widget no longer appears inappropriately when the date has been intentionally suppressed. (Christoph Wolk, [link](https://bugs.kde.org/show_bug.cgi?id=499945))

Fixed an issue that broke the layout of the device tiles on Info Center's Energy page when using a larger-than-default font size or loads of devices with batteries. (Ismael Asensio, [link](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/221))

Fixed two keyboard navigation issues in the Power and Battery widget. (Ismael Asensio, [link 1](https://bugs.kde.org/show_bug.cgi?id=489099) and [link 2](https://invent.kde.org/plasma/powerdevil/-/merge_requests/508))

Fixed an older issue that prevented the keyboard brightness controls on certain laptops from being visible immediately. (Nicolas Fella, [link](https://bugs.kde.org/show_bug.cgi?id=486067))

Fixed an older issue that caused Info Center's Energy page to vibrate disturbingly at certain window sizes. It was, heh heh heh… very high energy! (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=480804))


### Qt 6.8.3
Committed a better Qt fix for the issue whereby the first click after dragging Task Manager tasks got ignored. (David Redondo, [link](https://bugs.kde.org/show_bug.cgi?id=491100))


### Other bug information of note:

* 1 very high priority Plasma bug (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 27 15-minute Plasma bugs (up from 25 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 86 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-02-08&chfieldto=2025-02-14&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
