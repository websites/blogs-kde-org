---
title: "This Week in Plasma: end-of-year bug fixing"
discourse: ngraham
date: "2024-12-21T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

Lots of KDE folks are winding down for well-deserved end-of-year breaks, but that didn't stop a bunch of people from landing some awesome changes anyway! This will be a short one, and I may skip next week as many of us are going to be focusing on family time. But in the meantime, check out what we have here:


## Notable UI Improvements
When applying screen settings fails due to a graphics driver issue, the relevant page in System Settings now tells you about it, instead of failing silently. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=482151))

Added a new Breeze `open-link` icon with the typical "arrow pointing out of the corner of a square" appearance, which should start showing up in places where web URLs are opened from things that don't clearly look like blue clickable links. (Carl Schwan, Frameworks 6.10. [Link](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/437))


## Notable Bug Fixes

Fixed one of the most common recent Powerdevil crashes. (Jakob Petsovits, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=492349))

Recording a specific window in Spectacle and OBS now produces a recording with the correct scale when using any screen scaling (Xaver Hugl, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=497571))

When using a WireGuard VPN, the "Persistent keepalive" setting now works. (Adrian Thiele, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=461319))

Implemented multiple fixes and improvements for screen brightness and dimming. (Jakob Petsovits, 6.3.0. [Link 1](https://invent.kde.org/plasma/powerdevil/-/merge_requests/466), [link 2](https://bugs.kde.org/show_bug.cgi?id=452492), [link 3](https://bugs.kde.org/show_bug.cgi?id=494722), and [link 4](https://bugs.kde.org/show_bug.cgi?id=494408))

Auto-updates in Discover now work again! (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=447245))

Vastly improved game controller joystick support in Plasma, fixing many weird and random-seeming bugs. (Arthur Kasimov, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2666))

For printers that report per-color ink levels, System Settings' Printers page now displays the ink level visualization in the actual  ink colors again. (Kai Uwe Broulik, 6.3.0. [Link](https://invent.kde.org/plasma/print-manager/-/merge_requests/206))

Pager widgets on very thin floating panels are now clickable in all the places they're supposed to be clickable. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=485303))

Wallpapers with very very special EXIF metadata can no longer generate text labels that escape from their intended boundaries on Plasma's various wallpaper chooser views. (Jonathan Riddell and Nate Graham, Frameworks 6.10. [Link](https://bugs.kde.org/show_bug.cgi?id=497253))

Fixed one of the most common Qt crashes affecting Plasma and KDE apps. (Fabian Kosmale, Qt 6.8.2. [Link](https://bugreports.qt.io/browse/QTBUG-129241))

Other bug information of note:

* 2 Very high priority Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 34 15-minute Plasma bugs (up from 32 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 125 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-12-13&chfieldto=2024-12-20&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Significantly reduced the CPU usage of System Monitor during the time after you open the app but before you visit to the History page. More CPU usage fixes are in the pipeline, too! (Arjen Hiemstra, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=496875))

Plasma Browser Integration now works for the Flatpak-packaged version of Firefox. (Harald Sitter, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-browser-integration/-/merge_requests/136))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
