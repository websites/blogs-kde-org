---
title: "This Week in Plasma: Fancy Time Zone Picker"
discourse: ngraham
date: "2025-01-25T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week the bug-fixing for Plasma 6.3 continued, as well as a lot of new features and UI changes that have been in the pipeline for some time; these will mostly land in Plasma 6.4. There's a lot of cool stuff, so let's get into it!


## Notable New Features

Late breaking Plasma 6.3 feature: Discover can now open `flatpak:/` URLs. (Aleix Pol Gonzalez, 6.3.0. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/1012))

The time zone choosers present on System Settings' Date & Time page as well as the Digital Clock widget's settings page have been given a major upgrade: a visual UI using a world map! (Niccolò Venerandi, 6.4.0. [Link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2572) and [link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4873))

![](fancy-time-zone-picker.png)


## Notable UI Improvements

When activating the "Restore manually saved session" option on System Settings' Desktop Session page, the corresponding "Save Session" action now appears in Kickoff and other launcher menus immediately, rather than requiring a reboot first. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=449077))

On System Settings' Users page, the dialogs used for choosing an avatar image are now sized more appropriately no matter the window size, and the custom avatar cropper feature now defaults to no cropping for square images. (Nate Graham, 6.3.0. [Link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5108) and [link 2](https://bugs.kde.org/show_bug.cgi?id=498989))

![](systemsettings-users-kcm-avatar-picker-dialog-sized-properly.png)

On the System Tray widget's settings window, the table on the Entries page now uses the alternating row color style to make it easier to match up the columns, especially when the window has been made enormous for some reason. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498822))

![](system-tray-entries-page-stripey-background.png)

Improved the accessibility of several non-default <kbd>Alt</kbd>+<kbd>Tab</kbd> switcher styles. (Christoph Wolk, 6.3.0. [Link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/651))


Made the top corners' radii and side margins in Kickoff perfect. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=473061))

![](kickoff-perfect-top-corners.png)

Made the Breeze Dark color scheme a bit darker by default. (Thomas Duckworth, 6.4.0. [Link](https://invent.kde.org/teams/vdg/issues/-/issues/88))

![](darker-breeze-dark.png)

Adjusted the visualization for different panel visibility modes to incorporate some animations, which makes them clearer. (Niccolò Venerandi, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498035))

<video class="img-fluid" controls>
  <source src="panel-visibility-mode-animations.mp4" type="video/mp4">
</video>

You can now scroll on the Media Player widget's seek slider to move it without having to drag with the mouse. (Kai Uwe Broulik, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=416128))

Scrolling on the Task Manager widget to switch between tasks is now disabled by default (but can be re-enabled if wanted, of course), as a result of feedback that it was easy to trigger by accident and could lead to disorientation. (Nate Graham, 6.4.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/issues/56))

Re-arranged the items on the context menu for Plasma's desktop a bit, to improve usability and speed for common file and folder management tasks. (Nate Graham, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498737))

![](re-arranged-desktop-context-menu.png)

The Audio Volume widget now has a hamburger menu button when used in standalone form, rather than as a part of the System Tray, where it already has one. (Niccolò Venerandi, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497869))

![](audio-volume-widget-on-the-desktop-with-hamburger-menu.png)

Tooltips for Spectacle's annotation buttons now include details about how to change their behavior using keyboard modifier keys. (Noah Davis, 6.4.0. [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/435))

![](spectacle-annotation-tooltips.png)


## Notable Bug Fixes

Fixed a case where the service providing the screen chooser OSD could crash when certain screens were plugged in. (Vlad Zahorodnii, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498878))

Fixed a case where KWin could crash on launch in the X11 session. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/7039))

Fixed a case where Discover would crash when trying to display apps with no reviews. (Fushan Wen, 6.3.0. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/1013))

Fixed a case where Plasma could crash after creating a new panel with certain screen arrangements. (Fushan Wen, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5096))

Fixed a random KWin crash. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/7051))

Fixed a bug affecting System Settings' Desktop Session page that would cause it to crash upon being opened a second time, and also not show the settings in their correct states. (David Edmundson, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=487028), [link 2](https://bugs.kde.org/show_bug.cgi?id=488292))

Fixed several cases where screen positions and other settings might get reset after waking from sleep. (Xaver Hugl, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=488270), and [link 2](https://bugs.kde.org/show_bug.cgi?id=489457))

You can once again drag files, folders, and applications to Kickoff's Favorites view to make them favorites, after this broke at some point in the past. In addition, the change also fixes an issue where Kickoff's popup would inappropriately open rather than move out of the way when you dragged another widget over it. (Noah Davis, 6.3.0. [Link1](https://bugs.kde.org/show_bug.cgi?id=467587) and [link 2](https://bugs.kde.org/show_bug.cgi?id=454863))

Apps that launch and immediately display a dialog window along with their main window no longer have those windows go missing in the <kbd>Alt</kbd>+<kbd>Tab</kbd> switcher. (David Edmundson, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498098))

Improved OpenVPN cipher parsing so it won't show cipher types that don't actually exist. (Nicolas Fella, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498791))

Activating a Plasma panel using a keyboard shortcut in the X11 session no longer causes it to bizarrely become a window! (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=497596))

System Settings' Touchpad page is no longer missing some options in the X11 session, depending on how you open it. (Jakob Petsovits, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=448953))

Fixed a bug that could cause panels using the Auto-Hide and Dodge Windows settings to briefly get stuck open when activated while a full-screen window was active. (Niccolò Venerandi, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/7040))

Right-clicking an empty area of the applications or process table in System Monitor no longer shows a context menu with no appropriate items in it. (Nate Graham, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498672))

Fixed a bug causing a second "System Settings" item to appear on System Settings' own Shortcuts page. (Raphael Kubo da Costa, 6.4.0. [Link](https://bugs.kde.org/show_bug.cgi?id=498683))

You can once again copy files and folders on the desktop using the <kbd>Ctrl</kbd>+<kbd>C</kbd> shortcut, after this broke due to an unusual interaction between the desktop and a placeholder message added a few versions ago. (Marco Martin, Frameworks 6.11. [Link](https://bugs.kde.org/show_bug.cgi?id=498867))

Fixed a case where a Qt bug could cause apps to crash in response to certain actions from the graphics drivers. (David Redondo, Qt 6.8.3. [Link](https://codereview.qt-project.org/c/qt/qtbase/+/617247))


Other bug information of note:

* 1 Very high priority Plasma bug (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 23 15-minute Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 124 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-01-18&chfieldto=2025-01-24&chfieldvalue=RESOLVED&list_id=3000437&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Fixed a bunch of memory leaks in KScreen. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/libkscreen/-/merge_requests/222))

## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
