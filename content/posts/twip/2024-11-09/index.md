---
title: "This Week in Plasma: Everything You Wanted and More"
discourse: ngraham
date: "2024-11-09T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

This week was full of major feature work and UI polishing, in addition to a lot of bug-fixing! I'm pretty sure everyone will find something to be excited about here:

## Notable New Features

You can now swap the functions of drawing tablet pen buttons if you like the function performed by one of the buttons, but not which button activates it. (Joshua Goins, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=490975))

Info Center now shows all of your GPUs, not just one of them. And they are now indicated in a fancy way! (Harald Sitter, 6.3.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=491527) and [link 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/212))

![](multiple-fancy-gpus.png)

Discover now shows you when apps are either packaged directly by their developer, or verified by a trusted third party. (Aleix Pol Gonzalez, 6.3.0. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/755))

![](verified-app-in-discover.png)

The Printers widget now shows each printer's print queue inline, right there in the widget! (Kai Uwe Broulik, 6.3.0. [Link](https://invent.kde.org/plasma/print-manager/-/merge_requests/186))

![](inline-print-queue.png)

The Task Manager widget now lets you configure whether the "I'm playing audio" icons that appear in the corner of tasks can be clicked to mute the audio. In addition, the audio controls now always appear in the window preview thumbnails, and can't be accidentally disabled. (Petar Margetic, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494906))

When you've set up your system to reboot into the bootloader menu the next time it reboots (and not the firmware screen; that case was already handled), the logout screen how indicates this. (Nikolay Kochulin, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4889))

![](reboot-to-bootloader-menu.png)


## Notable UI Improvements

When you put a pie chart style System Monitor widget on a really skinny panel, the percentage circle in the center now looks nice, rather than being jagged and overly bold. (Arjen Hiemstra, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=494495))

![](tiny-cpu-use-pie-chart.png)

Made the progress indicators on Discover's Updates page more readable. (Nate Graham, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=492656))

The Keyboard Indicator widget now indicates when modifier keys are "latched" or "locked", as they can be when using certain accessibility settings. (Nicolas Fella, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=158053))

Removed the "Settings" launcher menu category! Now all of its contents have been moved into the "System" category. This reduces the number of categories that don't offer meaningful grouping. (Nate Graham, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4883))

![](system-category-full-of-stuff.png)

The Printers widget now shows a little busy spinner for any printers that are currently printing, to make them easier to pick out among others when there are a lot of printers available. (Kai Uwe Broulik. 6.3.0. [Link](https://invent.kde.org/plasma/print-manager/-/merge_requests/191))

Widgets placed on the desktop are now very slightly translucent, just like the popups of widgets placed on the panel. (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=473289))

![](translucent-widgets.png)

When for some reason the system time zone is set incorrectly or not set at all, the Digital Clock widget now tells you what's going on and offers you the chance to fix it yourself instead of just being broken. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=493232))

Added a distinct Breeze icon for System Settings' Shortcuts page. (Joshua Goins, Frameworks 6.9. [Link](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/415))

![](shortcuts-kcm-icon.png)

Improved the Breeze icon shown on password dialogs. (Kai Uwe Broulik, Frameworks 6.9. [Link](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/417))

![](new-password-dialog-icon.png)


## Notable Bug Fixes

Fixed a case where KWin would crash when plugging in external screens. (Xaver Hugl, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=495670))

Fixed a regression caused by a version 0.22 of the `power-profiles-daemon` service that caused power profiles to not be registered properly by Plasma's Power and Battery widget until you manually restarted the Powerdevil service. (Méven Car, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=492859))

Discover's auto-update feature once again works for Flatpak apps. (Harald Sitter, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=471548))

The speaker test window on System Settings' Audio page now always fits its content, no matter what the name of the device or its audio profile may be. (Ismael Asensio, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495752))

Fixed a strange issue that would make GTK 4 apps look too dark when using HDR mode. Now they're a bit too light, but this is a GTK 4 bug. (Xaver Hugl, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=494930))

Plasma panels in "fit content" mode are now compatible with flexible spacers; placing one on a panel no longer causes it to slowly grow to its full width when Plasma starts up — which was hilarious, but wrong. (Niccolò Venerandi, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495378))

Fixed a small visual glitch in KWin's Overview effect that caused windows dragged-and-dropped onto the desktop bar to strangely glide off screen. (Marco Martin, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495444))

Did a major code refactor and bug-fixing spree for desktop icon positioning, which fixes almost all of the outstanding bug reports about icons moving around, and also adds autotests to validate the fixes! You can [read more about it here](https://akselmo.dev/posts/plasma-desktop-icons-positioning-refactor), too. (Akseli Lahtinen, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2607))

Alt key accelerators for items in the Global Menu widget now work on Wayland. (Nicolas Fella, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=483657))

Performing a major system update using Discover no longer makes a pointless and invisible authentication prompt appear *behind* the logout window when you try to reboot to complete the update. (Alessandro Astone, PackageKit 1.3.1. [Link](https://bugs.kde.org/show_bug.cgi?id=495538))

Other bug information of note:

* 4 Very high priority Plasma bug (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 37 15-minute Plasma bugs (down from 40 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 106 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-11-02&chfieldto=2024-11-09&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

The feature to let you record the screen without re-approval if it's the same as one you did last time now also works when using rectangular region recordings. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/328))

Implemented support for the Wayland system bell protocol. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6166))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Filter and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024/) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
