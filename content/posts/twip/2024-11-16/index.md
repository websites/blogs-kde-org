---
title: "This Week in Plasma: Discover and System Monitor with a side of WINE"
discourse: ngraham
date: "2024-11-16T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

This week no major new features were merged, so we focused on polishing up what we already have and fixing bugs. That's right, Phoronix readers; we do in fact regularly do this! And let me also remind folks about our ongoing [2024 fundraiser](https://kde.org/fundraisers/yearend2024): in it, you can adopt a KDE app to have your name displayed as an official supporter of that app. If you love KDE or its apps, this is a great way to show your appreciation.

We're almost halfway to our year-end goal with 6 weeks to go. That's not bad, but I know we can get there quickly and unlock the stretch goals. So [check it out!](https://kde.org/fundraisers/yearend2024) And after that, check out this stuff too:


## Notable UI Improvements

When using a color scheme with Header colors such as Breeze Light and Breeze Dark, the color scheme editor no longer confusingly offers the opportunity to edit the Titlebar colors, which aren't used for such color schemes. Instead, you need to edit the Header colors. (Akseli Lahtinen, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=433059))

The System Tray no longer shows tooltips for items in the hidden/expanded view that would be identical to the visible text of the item being hovered with the pointer. (Nate Graham, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=494463))

The first time you use Plasma to create a network hotspot, it gets assigned a random password by default, rather than no password. (Albert Astals Cid, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/382))

In KRunner-powered searches, you can now jump between categories using the <kbd>Page Up</kbd>/<kbd>Page Down</kbd> and <kbd>Ctrl</kbd>+<kbd>Up</kbd>/<kbd>Ctrl</kbd>+<kbd>Down</kbd>. (Alexander Lohnau, 6.3.0. [Link 1](https://invent.kde.org/plasma/milou/-/merge_requests/46) and [link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2011))

Implemented support for the "Highlight changed settings" feature for most of System Settings' Drawing Tablet page. (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2626))

Discover now shows installation progress more accurately when downloading an app that also requires downloading any new Flatpak runtimes. (Harald Sitter, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494781))

When you have multiple Brightness and Color widgets, adjusting the screen brightness in one of them now mirrors this change to all of them, so they stay in sync. (Jakob Petsovits, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495661))

Added a new symbolic icon for WINE, which allows the category that WINE creates in Kickoff to use a symbolic icon that matches all the others. Also improved the existing colorful icon to better match the upstream branding. (Andy Betts, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=494450))

![](wine-icons.png)


## Notable Bug Fixes

Speaking of WINE, we fixed a recent regression that caused WINE windows to display black artifacts around them. (Vlad Zahorodnii, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=493934))

The feature to save a customized Plasma System Monitor widget as a new preset once again works. And we added an autotest to make sure it doesn't break again! (Arjen Hiemstra, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=485164))

Fixed an extremely strange issue that could cause an actively focused XWayland window to lose the ability to receive keyboard and pointer input when the screen was locked using the <kbd>Meta</kbd>+<kbd>L</kbd> keyboard shortcut. (Adam Nydahl, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495325))

Fixed a recent regression that caused System Monitor to stop gathering statistics for some ARM-based CPUs. (Hector Martin, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495524))

Discover once again allows you to update update-able add-ons acquired using the "Get New \[thing\]" windows, which had gotten broken in the initial release of Plasma 6. (Harald Sitter, 6.3.0. [Link 1](https://invent.kde.org/plasma/discover/-/merge_requests/959) and [link 2](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/339))

Fixed a case where the real session restoration feature in the X11 session wouldn't restore everything correctly. (David Edmundson, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=491130))

Fixed a visual glitch affecting Kirigami's `SwipeListItem` component which would give it the wrong background color when using Breeze Dark and other similar color schemes, and could be prominently seen on Discover's Settings page. (Marco Martin, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=495291))

Fixed a major Qt regression that caused the lock and login screens to become non-functional under various circumstances. (Olivier De Cannière, Qt 6.8.1, but distros will be back-porting it to their Qt 6.8.0 packages soon, if they haven't already. [Link](https://bugs.kde.org/show_bug.cgi?id=494804))

Fixed a Qt regression that caused the error dialog on "Get New \[Thing\]" windows to be visually broken until the window was resized. (David Edmundson, Qt 6.8.1. [Link](https://bugs.kde.org/show_bug.cgi?id=493559))

Fixed another Qt regression that caused clicking on a virtual desktop to switch to it in KWin's overview effect to stop working after you use the Desktop Cube at least once. (David Edmundson, Qt 6.8.1. [Link](https://bugs.kde.org/show_bug.cgi?id=488980))

Other bug information of note:

* 2 Very high priority Plasma bugs (down from 4 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 36 15-minute Plasma bugs (down from 37 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 119 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-11-09&chfieldto=2024-11-16&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

We've re-enabled the ability to turn on HDR mode when using version 565.57.1 or later of the NVIDIA driver for NVIDIA GPU users, or version 6.11 or later of the Linux kernel for Intel GPU users. These are the versions of those pieces of software that have fixed the worst bugs affecting HDR on those GPUs. (Xaver Hugl, 6.2.4. [Link 1](https://invent.kde.org/plasma/kwin/-/merge_requests/6778) and [link 2](https://invent.kde.org/plasma/kwin/-/merge_requests/6767/diffs))

Fixed a performance issue that affected users of multi-monitor setups while using a VR headset. (Xaver Hugl, 6.2.4. [Link](https://bugs.kde.org/show_bug.cgi?id=495400))

Reduced the slowness and lag that you could experience when drag-selecting over a hundred items on the desktop. (Akseli Lahtinen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=493376))

Implemented support for the [`xdg_toplevel_icon`](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/269) Wayland protocol in KWin. (David Edmundson, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/5680))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Filter and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
