---
title: "This Week in Plasma: Feels Like a Good One"
discourse: ngraham
date: "2025-02-01T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week we continued to polish up Plasma 6.3 in preparation for its final release in a week and a half, and I gotta say, it's looking pretty good! Most of the bug reports we've gotten from people running the beta have been minor and quickly fixed, which I'm choosing to interpret to mean that it's a good release. :)

In addition, we landed a bunch more Plasma 6.4 UI improvements! Check it all out:


## Notable UI Improvements

### Plasma 6.3.0

Task Manager previews now only show one copy of the playback controls for grouped apps where only a single window is playing media, and they also try to match the controls to the window where the media is playing. This matching isn't 100% perfect due to a lack of necessary data from browsers, so we'll be working to refine it over time. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2719))

![](task-manager-media-controls-only-on-onethumbnail.png)

App screenshots in Discover are no longer blurry when the app's metadata includes a thumbnail that's smaller than the size it will be displayed at; instead, Discover now displays the full-size version as the thumbnail. (Aleix Pol Gonzalez, [link](https://bugs.kde.org/show_bug.cgi?id=497772))

Fixed the mnemonics and buddy relationships for several complex UI elements on the Wallpaper page in System Settings and the desktop config window. (Kai Uwe Broulik, [link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5119), [link 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5122), and [link 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2752))

On System Settings' Users page, when you use the file picker to choose an avatar image from a file on disk, the file picker now remembers the location for next time too. (Nate Graham, [link](https://bugs.kde.org/405222))

On System Settings' Display & Monitor page, the option to use a display's built-in color profile is now disabled when it would have no practical effect. (Xaver Hugl, [link 1](https://invent.kde.org/plasma/kscreen/-/merge_requests/340), [link 2](https://invent.kde.org/plasma/libkscreen/-/merge_requests/235), [link 3](https://invent.kde.org/plasma/kwin/-/merge_requests/7017), and [link 4](https://invent.kde.org/libraries/plasma-wayland-protocols/-/merge_requests/93))

### Plasma 6.4.0

Made the contents of the context menus for the desktop trash (if you have one there) and its contents more contextually-relevant. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2764))

![](more-contextual-desktop-trash-context-menu.png)

All of those confusing and/or nonfunctional chord keyboard shortcuts throughout Plasma have been removed or replaced with standard-style shortcuts. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-workspace/-/issues/148))

![](no-more-chord-keyboard-shortcuts.png)

On System Settings' Region & Language page, the language chooser sheet now has a search field and responds appropriately to keyboard interactions. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5141))

<video class="img-fluid" controls>
    <source src="search-field-on-language-sheet.mp4" type="video/mp4">
</video>

<br/><br/>

The screen action chooser OSD you can open with <kbd>Meta</kbd>+<kbd>P</kbd> will now cycle through its options if you continue to hold down <kbd>Meta</kbd> and press <kbd>P</kbd>. (Sergey Katunin, [link](https://bugs.kde.org/show_bug.cgi?id=444307))

On Info Center's Energy page, the graph is now displayed better when information isn't available. (Ismael Asensio, [link](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/220))

![](kinfocenter-energy-graph-with-placeholder.png)

Task Manager tooltips from right-screen-edge panels now shift their close buttons over to the left side, to make them harder to click by accident like with left and bottom panels. People using top panels are expected to be so elite that this isn't a problem for them 😎 (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2765))

![](task-manager-tooltip-close-button-far-from-panel.png)

The categories displayed in Discover's sidebar have been synced with the ones  shown in Kickoff/Kicker/etc. (Ujjwal Shekhawat, [link](https://invent.kde.org/plasma/discover/-/merge_requests/1019))

### Frameworks 6.11

The `Kirigami.LinkButton` UI element has been given an appropriate color change when pressed, which will be visible in many apps making use of it, including quite a few Plasma apps like Discover and Welcome Center. (Nate Graham, [link](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1714))

![](hover-effect-for-pressed-links.png)


## Notable Bug Fixes

### Plasma 6.3.0

KWin no longer crashes when the system has been configured to use an ICC profile that's then later moved, renamed, deleted, or otherwise made unavailable at the place KWin expects to find it. Instead, it simply doesn't apply the profile. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=498861))

Fixed two KWin crashes that could be caused by the kernel or graphics drivers providing unexpected data, and a third one caused by a bug in idleness detection. (Vlad Zahorodnii, [link 1](https://invent.kde.org/plasma/kwin/-/merge_requests/7067), [link 2](https://invent.kde.org/plasma/kwin/-/merge_requests/7079), and [link 3](https://invent.kde.org/plasma/kwin/-/merge_requests/7099))

Fixed a case where Plasma could crash on X11 when accessed over VNC without certain XRandr extensions present. (Fushan Wen, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5113))

Fixed a case where the app chooser dialog could sometimes crash when you tell it to always use the chosen app to open files of that type. (David Redondo, [link](https://bugs.kde.org/show_bug.cgi?id=484739))

Fixed a case where the `ksystemstats` daemon (that provides, well, system stats) could crash after you'd finished customizing a widget or the app to remove a sensor. (Arjen Hiemstra, [link](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/107))

Sticky Note widgets on the desktop no longer block the <kbd>Ctrl</kbd>+<kbd>V</kbd> shortcut from working to paste files onto the desktop, even when the note widget isn't focused. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=417250))

Fixed the spinboxes on System Settings' Accessibility page, which would become semi-broken when you typed numbers into them rather than interacting with them using the arrow buttons or up/down arrow keys. They were ironically not very accessible! But no longer. (David Edmundson, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2707))

Fixed two Plasma 6 regressions in the Kicker application menu: one that required two presses of the backspace key to delete characters entered into the search field; and another that made it impossible to navigate between groups of the multi-column search view. (Marco Martin, [link 1](https://bugs.kde.org/show_bug.cgi?id=483253) and [link 2](https://bugs.kde.org/show_bug.cgi?id=482624))

Fixed a bug in the System Monitor app and widgets that caused the number of CPUs to be mis-counted on some systems. (Arjen Hiemstra, [link](https://bugs.kde.org/show_bug.cgi?id=496847))

Filtering text on Info Center's Firmware Security page now works properly, displaying matches on separate lines as expected. (Harald Sitter and Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=499069))

Discover is now more reliable at showing themed app icons on certain distros. (Harald Sitter, [link](https://bugs.kde.org/show_bug.cgi?id=494315))

Fixed the screen color display for games that use the `scRGB` color space in HDR mode. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=499103))

Fixed the colors of the Night Light preview when using a dark color scheme, and when adjusting the color temperature while Night Light is currently active. (Thomas Moerschell, [link 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5128) and [link 2](https://bugs.kde.org/show_bug.cgi?id=498538))

Discover now shows something more sensible when launched with no internet access. (Aleix Pol Gonzalez, [link](https://bugs.kde.org/show_bug.cgi?id=493419))

Fixed a Task Manager bug that caused tooltips to inappropriately re-appear while their task's context menu was open and you hovered over the task again. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=448510))

### Plasma 6.4.0

On System Settings' Window Switchers page, the apps in the preview visualization are now always the same ones, and no longer sometimes display thumbnails that don't match the text and icon. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=499106))

Entering text in the Overview screen's search field now works. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=450014))


### Other bug information of note:

* 1 very high priority Plasma bug (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 24 15-minute Plasma bugs (up from 23 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 93 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-01-25&chfieldto=2025-01-31&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

### Plasma 6.3.0

Improved the accuracy of CPU usage measurements in the System Monitor app and widgets by filtering out `iowait`, which is not really real CPU usage. (Arjen Hiemstra, [link](https://bugs.kde.org/show_bug.cgi?id=497341))

For people who had enabled the "Disable touchpad while mouse is plugged in" setting on X11 in the past, that preference is now migrated to the new feature (which works on both X11 and Wayland) so you don't need to manually re-enable it, and the remain orphaned backend code for the old feature has been deleted. (Jakob Petsovits, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2726))

System Settings' Audio page now does a more thorough job of identifying audio devices that should be marked as inactive. (Harald Sitter, [link](https://bugs.kde.org/show_bug.cgi?id=496682))

Plasma's KRDP remote desktop server now only runs in Plasma itself, and won't start up accidentally in other environments you might also have running on the same system, where it might interfere with or block that environment's own RDP solution. (David Edmundson, [link](https://invent.kde.org/plasma/krdp/-/merge_requests/86))

### Plasma 6.4.0

KRDP now supports FreeRDP version 3. (Jack Xu, [link](https://invent.kde.org/plasma/krdp/-/merge_requests/69))

Deleting a Sticky Note widget also deletes the data file where its text was stored, plugging a data leak and preventing the `~/.local/share/plasma_notes/` folder from filling the system up with orphaned data taking up space. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=492862))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
