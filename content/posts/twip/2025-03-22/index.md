---
title: "This Week in Plasma: 6.4 Improvements"
discourse: ngraham
date: "2025-03-22T3:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover the highlights of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week Plasma 6.4 continued to take shape, with a number of additional user-visible modernizations and improvements — in particular some nice progress on the topics of keyboard navigation, accessibility, and customizing apps' presentation in launcher menus.



## Notable UI Improvements

### Plasma 6.3.4
Added keyboard navigation and interaction to the User Switcher widget's popup. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=501649))


### Plasma 6.4.0
KMenuEdit has received a UI modernization, including a simplification of the default toolbar contents, showing a well-curated hamburger menu by default, and adopting modern-style tool view tabs. (Oliver Beard, [link 1](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/42) and [link 2](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/40))

![](kmenuedit-modernization.png)

When installing apps in Discover, progress feedback now appears right in the place you were just looking, rather than in the opposite corner of the window. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=475845))

![](discover-showing-app-install-progress-inline.png)

Plasma's Calculator widget now announces the calculated result using the screen reader, if one is active. (Christoph Wolk, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/698))

Improved keyboard navigation for Plasma's Calculator widget; now navigation wraps around on the same row or column. (Christoph Wolk, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/696))

In System Monitor's table views, you can now press <kbd>Ctrl</kbd>+<kbd>A</kbd> to select everything, as you would expect. (Arjen Hiemstra, [link](https://bugs.kde.org/show_bug.cgi?id=499112))

After editing a page in System Monitor, the app now scrolls to show a newly-added row if it would otherwise appear out of the visible area. (Arjen Hiemstra, [link](https://bugs.kde.org/show_bug.cgi?id=499052))

KWin's "Dim Screen for Administrator Mode" plugin is now on by default, helping to focus attention on authentication prompts so you're less likely to miss them or lose them, especially with a cluttered desktop full of stuff. (Vlad Zahorodnii, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/7368))

![](messy-desktop-dimmed-to-make-authentication-prompt-more-visible.png)

OSDs are now consistently referred to as "OSDs" in all the places you can disable them. (Nate Graham, [link 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2897) and [link 2](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/331))


### Frameworks 6.13
System Monitor now displays something more user-friendly when asked to report the battery charge level of a device whose battery technology it can't determine. (David Redondo, [link](https://invent.kde.org/frameworks/solid/-/merge_requests/204))



## Notable Bug Fixes

### Plasma 6.3.4
Narrowed the range of data that the Plasma clipboard accepts to be more in line with what it did in Plasma 6.2 and earlier. This prevents Plasma form crashing when copying certain types of content in certain apps. (Fushan Wen, [link](https://bugs.kde.org/show_bug.cgi?id=501623))

Triggering the system bell a zillion times in rapid succession (e.g. by holding down <kbd>backspace</kbd> in an XWayland-using GTK app; don't do that) no longer freezes the entire system. (Nicolas Fella, [link](https://bugs.kde.org/show_bug.cgi?id=500916))

Fixed a bug in the Weather Report widget that caused it to fail to save its settings (size, position, even its existence) under certain circumstances when placed on the desktop, rather than a panel. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=501066))

The User Switcher widget now lays out text for the username more sensibly, neither getting too big with huge horizontal panels, nor overflowing with not-huge vertical panels. (Marco Martin and Nate Graham, [link 1](https://bugs.kde.org/show_bug.cgi?id=356603) and [link 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/712))

Fixed a regression in the new spatial quick tiling code that made global actions to instantly quarter-tile the active window not always work correctly the first time they were invoked. (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=501731))

Fixed a regression that broke pasting numbers into Plasma's Calculator widget. (Christoph Wolk, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/713))

Fixed a regression that caused the icons of Folder View popups in list mode to be displayed with too much transparency. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=501797))

Fixed a bug that caused KMenuEdit to be unable to save changes you made to systemwide-installed Flatpak apps that had previously been customized using the properties dialog. (Oliver Beard, [link](https://bugs.kde.org/show_bug.cgi?id=394476))


### Plasma 6.4.0
Fixed several minor issues affecting the Media Frame widget, including flickering on image change and not remembering customized popup sizes when living in a panel. (Marco Martin, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/709))


### Other bug information of note:
* 3 very high priority Plasma bugs (up from 2 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 21 15-minute Plasma bugs (down from 23 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)


## Notable in Performance & Technical

### Plasma 6.3.4
Non-full-screen screen recordings taken in Spectacle now have significantly higher visual quality when using a fractional screen scale factor. (Vlad Zahorodnii and Noah Davis, [link 1](https://invent.kde.org/plasma/kwin/-/merge_requests/7322), [link 2](https://invent.kde.org/plasma/kwin/-/merge_requests/7310), [link 3](https://invent.kde.org/libraries/plasma-wayland-protocols/-/merge_requests/99), [link 4](https://invent.kde.org/plasma/kwin/-/merge_requests/7326), [link 5](https://invent.kde.org/graphics/spectacle/-/merge_requests/445))


### Plasma 6.4.0
Continued the project of cleaning up Plasma's log output. (Christoph Wolk, [link 1](https://invent.kde.org/plasma/libplasma/-/merge_requests/1289), [link 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2893), [link 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2890), [link 4](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2892), [link 5](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/691), [link 6](https://invent.kde.org/plasma/print-manager/-/merge_requests/231), [link 7](https://invent.kde.org/plasma/bluedevil/-/merge_requests/204), [link 8](https://invent.kde.org/plasma/print-manager/-/merge_requests/229), [link 9](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2891), [link 10](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/693), [link 11](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/694), [link 12](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2895), [link 13](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/701), [link 14](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/700), [link 15](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/695), [link 16](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/699), [link 17](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/686), [link 18](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/708), [link 19](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/706), [link 20](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/707), and [link 21](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/705))



## How You Can Help
KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
