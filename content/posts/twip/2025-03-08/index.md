---
title: "This Week in Plasma: A Very Fixy Week"
discourse: ngraham
date: "2025-03-08T1:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week we took a break from new features and put on our bug-fixing hats, squashing a very large number of bugs and annoyances, because it's important not to break things when you move fast! Some nice UI refinements landed as well.




## Notable new Features

### Plasma 6.4.0
You can now control whether a window has a titlebar and frame from its Task Manager context menu, like you can with various other window tweaks. (Kai Uwe Broulik, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2815))

![](no-titlebar-and-frame-from-context-menu.png)




## Notable UI Improvements

### Plasma 6.3.3
The Digital Clock widget now shows a nicer-looking font picker dialog when you customize the text style of the clock; we went back to the older style after Qt 6 changed the default to something that isn't as suitable for our purposes. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5289))

![](slightly-nicer-font-chooser-dialog.png)

Improved the way screens are presented to remove the technical information in cases where it isn't needed to distinguish screens from one another. (Oliver Beard, [link](https://bugs.kde.org/show_bug.cgi?id=500471))

![](cleaner-screen-descriptions.png)


### Plasma 6.4.0
It's once again possible to configure the lock screen's clock to disappear when the rest of its UI fades out, providing once again for the possibility of a screensaver-like experience. (Kai Uwe Broulik, [link](https://bugs.kde.org/show_bug.cgi?id=493422))

Bluetooth devices are no longer inappropriately shown in the System Tray popup when Bluetooth is disabled but the wireless adapter is still powered on, which is a state that some devices can get into. (Vlad Zahorodnii, [link](https://invent.kde.org/plasma/bluedevil/-/merge_requests/200))

Improved the ordering of search results in the "Add keyboard layout" dialog. (Bharadwaj Raju, [link](https://bugs.kde.org/show_bug.cgi?id=500053))




## Notable Bug Fixes

### Plasma 6.3.3
Fixed two serious issues that could make KWin crash on login or fail to allow login at all on systems with certain types of monitor arrangements, on distros that ship user software with asserts turned on. (Xaver Hugl, [link 1](https://bugs.kde.org/show_bug.cgi?id=500819) and [link 2](https://bugs.kde.org/show_bug.cgi?id=500797))

Finally figured out and fixed for good the issue that could cause the lock screen to sometimes be all black on X11. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=483163))

Fixed a subtle way the screen locker could fail to respond to keyboard actions with certain configurations applied. (David Edmundson, [link](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/273))

Fixed one of the top 20 Plasma crashes — this one a case of crashing after waking from sleep. (Fushan Wen, [link](https://bugs.kde.org/show_bug.cgi?id=499944))

Fixed a strange crash that could happen when configuring Plasma to have multiple "Notifications" widgets. We traced it to a Qt bug and implemented a workaround in Plasma, and then the Qt bug was also fixed! (Marco Martin and David Faure, [link](https://bugs.kde.org/show_bug.cgi?id=500749))

Fixed a case where Plasma could crash when closing apps by middle-click (this is off by default) on the Task Manager while their window previews were visible. (Harald Sitter, [link](https://bugs.kde.org/show_bug.cgi?id=501042))

The names of some NVIDIA GPUs are once again shown correctly in Info Center, after this regressed due to a related change to show nicer GPU names didn't work 100% for all GPUs. (Harald Sitter, [link](https://bugs.kde.org/show_bug.cgi?id=499882))

Fixed a recent regression that made it harder to see release notes for updateable Flatpak and Snap apps in Discover. (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=500776))

Fixed a recent regression that made the "Legacy X11 app scaling" setting not take effect immediately, as it was supposed to. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=499923))

Rapidly clicking the "Next" or "Previous" buttons on the calendar in the "Calendar" and "Digital Clock" widgets now switches to the appropriate month, year, or decade. (Matthias Tillman, [link](https://bugs.kde.org/show_bug.cgi?id=499871))

Disabling animations globally no longer makes it impossible to configure panel widgets while in panel edit mode using their hover pop-ups. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=500717))

Fixed two color scheme issues: one that would make menu text look wrong with creative color schemes that have headers colored differently from the rest of the window, and another that would let an old color scheme's header colors briefly remain visible after changing the color scheme. (Evgeniy Harchenko and David Redondo, [link 1](https://invent.kde.org/plasma/breeze/-/merge_requests/525) and [link 2](https://invent.kde.org/plasma/breeze/-/merge_requests/531))

Fixed some visual glitches visible on blurred Plasma widgets that some people were experiencing with some GPUs when using the new "Prefer color accuracy" setting. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=499935))

KRunner no longer becomes an unreadable mess when the text you enter is so long that it starts to overflow off the right edge. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=500290))

Fixed a keyboard accessibility issue on System Settings' "KWin Rules" page. (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=488703))

Fixed keyboard navigation working incorrectly in the "Application Dashboard" widget when using a right-to-left language. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2791))

Discover no longer tries and fails to uninstall end-of-life Flatpak runtimes that are still in use by apps, displaying an ugly and un-actionable error message in the process. (Harald Sitter, [link](https://invent.kde.org/plasma/discover/-/merge_requests/1045))


### Plasma 6.4.0
Fixed a small visual glitch in the Global Menu widget seen when moving the pointer between open menus. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=499525))


### Other bug information of note:
* 2 very high priority Plasma bugs (down from 3 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 24 15-minute Plasma bugs (down from 27 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 136 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-03-01&chfieldto=2025-03-07&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)




## Notable in Performance & Technical

### Plasma 6.3.3
Implemented support for battery charge thresholds on more devices. (Jakob Petsovits, [link](https://bugs.kde.org/show_bug.cgi?id=497200))

Further improved the way colors are displayed on screen when using Night Light on systems with Intel GPUs. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=500837))

Forcing "Adaptive Sync" to be on no longer reduces the refresh rate of certain screens under certain circumstances. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=487563))

Added little warning messages when you disable power management, complying with new EU regulations taking effect in two months. This is what happens when you become important, folks! (Kai Uwe Broulik, [link](https://bugs.kde.org/show_bug.cgi?id=501057))

Copying text in LibreOffice Writer no longer inserts anchor links into it, shown by little gray brackets around letters. This isn't a new thing, and not really a bug, either; it was happening for ages and ages, but nobody ever noticed until a recent change in LibreOffice to surface these links visually. (Fushan Wen, [link](https://bugs.kde.org/show_bug.cgi?id=500903))

Fixed a couple of behavioral anomalies when manually manipulating windows in the stacking order. (Jarek Janik, [link 1](https://bugs.kde.org/show_bug.cgi?id=478382) and [link 2](https://bugs.kde.org/show_bug.cgi?id=478382))

### Plasma 6.4.0
Fixed a memory leak found in Plasma. (Fushan Wen, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5280))

The `kscreen-doctor` tool can now be used to toggle HDR mode on and off, in addition to its existing abilities to enable or disable it. (Xaver Hugl, [link](https://invent.kde.org/plasma/libkscreen/-/merge_requests/243))

Put a lot of effort into reducing Plasma's log spam caused by binding loops and other warnings. (Christoph Wolk, [link 1](https://bugs.kde.org/show_bug.cgi?id=500374), [link 2](https://bugs.kde.org/show_bug.cgi?id=499024), [link 3](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/687), [link 4](https://invent.kde.org/plasma/print-manager/-/merge_requests/228), [link 5](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1733), [link 6](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2867), [link 7](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2866), [link ](https://invent.kde.org/plasma/libplasma/-/merge_requests/1281), [link ](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2862))




## How You Can Help
KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
