---
title: "This Week in Plasma: Artistry and accessibility"
discourse: ngraham
date: "2025-01-04T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Plasma developers are returning from their holidays and have provided us all with loads of goodies! Yep, this is a big one, especially in terms of the juicy user-facing changes in the areas of accessibility and support for digital artists. There's lots more as well too!


## Notable New Features
It's now (or, depending on how long your memory is, once again) possible to configure your touchpad to be automatically disabled while a mouse is plugged in. (Jakob Petsovits, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=415364))

![](disable-touchpad-while-mouse-is-connected.png)

On System Settings' Graphics Tablet page, it's now possible to map an area of a drawing tablet's surface to the entire screen area; previously only the reverse was possible. (Joshua Goins, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=457703))

![](map-part-of-tablet-to-whole-screen.png)

On System Settings' Graphics Tablet page, it's now possible to customize the pressure range of a stylus to chop off the high and/or low parts, should this be desirable for your hardware or preferences. (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2681))

![](drawing-tablet-pen-pressure-range.png)

KRunner and KRunner-powered search fields can now convert between "rack units" and other units of length. (Lea McLean, Frameworks 6.10. [Link](https://invent.kde.org/frameworks/kunitconversion/-/merge_requests/55))

![](krunner-rack-units.png)

Discover now highlights sandboxed apps whose permissions will change after being updated, so you can audit such changes for shady behavior. (Aleix Pol Gonzalez, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=496983))

![](discover-new-permissions-on-update.png)


## Notable UI Improvements
Overhauled the UI of System Settings' Graphics Tablet page to split it into multiple tabs, which greatly improves the organization. You can see it in the other screenshots of this blog post! (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2302))

On System Settings' Graphics Tablet page, a custom tablet calibration matrix is now expressed in a more standard way: now it appears as highlighted when changed while the "highlight changed settings" feature is in use, and it can also be saved and reset like any other setting. (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2676))

On System Settings' Graphics Tablet page, the pen testing feature now also shows information about pen tilt and pressure, if supported by the tablet and pen. (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2675))

Discover now shows a maximum of two columns even with a very wide window, which results in a less awkward UI because there was rarely or never enough room for a third column to have enough space to make a positive difference. (Rahul Vadhyar, 6.3.0. [Link](https://invent.kde.org/plasma/discover/-/merge_requests/996))

Editing desktop files for apps from the "Edit Application…" menu item in Kickoff and other launcher menus now opens the KMenuEdit app to show the app you'd like to edit, rather than a properties dialog. This lets you easily edit others as well, should it strike your fancy. (Oliver Beard, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-desktop/-/issues/140))

When an application takes control of your screen and input devices because you granted it permission to do so (either in that moment or in the past), a system notification is now shown that lets you know how you can immediately terminate this and return control to yourself. (David Redondo, 6.3.0. [Link](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/313))

Improved the accessibility-related keyboard navigation functionality of multiple Kirigami-based UI components, as well as users of them in Discover and KDE's XDG desktop portal implementation. (Christoph Wolk, Frameworks 6.10 and Plasma 6.3.0. [Link 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1689), [link 2](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1691) [link 3](https://invent.kde.org/plasma/discover/-/merge_requests/997), [link 4](https://invent.kde.org/plasma/discover/-/merge_requests/998), and [link 5](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/338))

Improved support for mnemonics (those little underlines that appear underneath letters of triggerable UI controls when you hold down the Alt key) in Kirigami and a number of Plasma components (Kai Uwe Broulik, Frameworks 6.10 and Plasma 6.3.0. [Link 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1687), [link 2](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1686), [link 3](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1688), [link 4](https://invent.kde.org/plasma/libplasma/-/merge_requests/1238), [link 5](https://invent.kde.org/plasma/libplasma/-/merge_requests/1237), [link 6](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5022), and [link 7](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2686))


## Notable Bug Fixes
Fixed a bug that caused the System Tray's battery icon to always be visible when the `power-profiles-daemon` package isn't installed. (Jakob Petsovits, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494506))

When you set the wallpaper by dragging an image to the desktop (try it, it works!), it's now actually remembered the next time you log in. (Tino Lorenz, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=480509))

Fixed a bug that would unexpectedly require that you copy items twice to get them into the clipboard, but only while the clipboard is configured to not store history and also prevent itself from being empty. (Fushan Wen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=417590))

Other bug information of note:

* 2 Very high priority Plasma bugs (same as two weeks ago). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 34 15-minute Plasma bugs (same as two weeks ago). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 140 KDE bugs of all kinds fixed over the last two weeks. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-12-20&chfieldto=2025-01-03&chfieldvalue=RESOLVED&list_id=3000437&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical
Plasma now respects the system's configured URL for connectivity checks — which can be important especially when customized in an institutional or enterprise environment — rather than always consulting https://networkcheck.kde.org. (Ismael Asensio, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-nm/-/issues/8))


Further reduced the memory usage of Plasma's clipboard system, especially when set up to store a much larger than average number of history items. (Fushan Wen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=480127))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
