---
title: "This Week in Plasma: Final Plasma 6.3 Polishing"
discourse: ngraham
date: "2025-02-08T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week Plasma's contributors spent a lot of time putting the finishing touches on Plasma 6.3 before its final release in three days to make sure it's as good as possible!


## Notable new Features

### Plasma 6.4.0
There's now an option to make panel pop-ups use the floating style even when the panel itself doesn't use that style. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=478324))

![](floating-popups-without-floating-panel.png)


## Notable UI Improvements

### Plasma 6.4.0
Improved the appearance of the Welcome Center page that shows up after you've finished upgrading to a new Plasma feature release. (Oliver Beard, [link](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/187))

![](welcome-center-post-update-page.png)

(Here shown with the appearance it would have had for Plasma 6.2, since 6.4 doesn't exist yet!)

Info Center's energy graph now uses the system's accent color, rather than always being red. (Ismael Asensio, [link](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/223))

![](kinfocenter-energy-page-graph-with-accent-color.png)

When using software-based screen brightness, the minimum brightness level is now significantly darker. (Xaver Hugl, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/7111))

Improved the Kate Sessions widget's keyboard navigation in multiple ways. (Christoph Wolk, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/666))

### Frameworks 6.11
In the open/save dialogs used throughout KDE software, changes to the text in the filename field are now undo-able and the text field itself shows a little undo button. This is especially useful if you mis-click on a file while saving and accidentally overwrite the filename; now you can quickly undo that by clicking the button or pressing <kbd>Ctrl</kbd>+<kbd>Z</kbd>! (Marco Martin, [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1807))


## Notable Bug Fixes

### Plasma 6.3.0
Fixed a case where KWin could sometimes crash after resuming from sleep on certain hardware. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=497278))

Fixed a case where System Settings' Firewall pager could crash during normal usage, and also fail to save changed settings properly. (David Edmundson, [link l](https://bugs.kde.org/show_bug.cgi?id=455385) and [link 2](https://invent.kde.org/plasma/plasma-firewall/-/merge_requests/93))

Fixed a performance issue caused by using the "Sidebar" <kbd>Alt</kbd>+<kbd>Tab</kbd> task switcher visualization that could eventually cause KWin to bog down or hang. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=492506))

Fixed two glitches related to scrolling not working properly while System Monitor's "Configure Columns" dialog was open. (David Redondo, [link](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/338))

When using the Kicker Application Menu widget with a right-to-left language like Arabic or Hebrew, its sub-menus now open to the left if there's space. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5136))

Fixed a subtle issue that could cause automatic screen sleep to stop working after apps block and unblock it multiple times. (David Redondo, [link](https://bugs.kde.org/show_bug.cgi?id=469687))

Uninstalling an app that was made a favorite in the Kickoff Application Launcher now removes it immediately, rather than only after a restart. (Harald Sitter, [link](https://bugs.kde.org/show_bug.cgi?id=497958))

Fixed the Mouse Click KWin effect so it works again in the X11 session.  (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=498779))

Fixed a case where the keyboard language System Tray icon could be missing on login in the X11 session. (Dark Templar, [link](https://bugs.kde.org/show_bug.cgi?id=495684))


### Plasma 6.3.1
Fixed two intermittent crashes in the portal-based screen chooser dialog: one after selecting a virtual output, and another one after an app disconnects from a stream. (David Redondo and Fushan Wen, [link 1](https://bugs.kde.org/show_bug.cgi?id=495160) and [link 2](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/356))

Fixed a case where opening a `.flatpakref` file in Discover could sometimes make it crash after launching. (Aleix Pol Gonzalez, [link](https://bugs.kde.org/show_bug.cgi?id=486057))

Fixed a case where Discover could sometimes crash while trying to load app reviews without network connectivity. (Fushan Wen, [link](https://invent.kde.org/plasma/discover/-/merge_requests/1031))

Fixed a weird bug where repeatedly entering text in the Clipboard widget's search field and then deleting it could sometimes cause items in the list view to end up overlapping. (David Edmundson, [link](https://bugs.kde.org/show_bug.cgi?id=448833))

Improved the reliability with which a trashcan icon on the desktop detects when it should change its appearance based on the presence or absence of files in the trash. (Méven Car, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5120))

Non-square images used for the Kickoff Application Launcher's panel button are once again presented as expected. (Niccolò Venerandi, [Link](https://bugs.kde.org/show_bug.cgi?id=494389))

### Plasma 6.4.0
Fixed two cases where text wasn't translated in Spectacle's placeholder config UI and Info Center's Energy page graph labels. (Noah Davis and Ismael Asensio, [link 1](https://bugs.kde.org/show_bug.cgi?id=499384) and [link 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/225))

When a screen edge has multiple panels of different thicknesses, panel-pop-ups on the thinner panels now appear in the right place. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=487309))

### Frameworks 6.11
Fixed a case where Plasma could crash while trying to render SVG images, which it does a lot of. (Fushan Wen, [link](https://invent.kde.org/frameworks/ksvg/-/merge_requests/59))

Fixed multiple bugs relating to focus and tab-ordering in the open/save dialogs used throughout KDE software. (Marco Martin, [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1805))

Fixed a visual bug most prominently affecting the System Tray, where icons could sometimes look blurry until hovered while using a fractional screen scale factor. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=484888))

In the Properties dialog for symlinks on the desktop, the button to open Dolphin at the target's location now works. (Kamil Kaznowski, [link](https://bugs.kde.org/show_bug.cgi?id=476808))


### Other bug information of note:

* 1 very high priority Plasma bug (1 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 25 15-minute Plasma bugs (up from 24 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 91 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-01-31&chfieldto=2025-02-07&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
