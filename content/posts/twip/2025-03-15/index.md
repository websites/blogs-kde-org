---
title: "This Week in Plasma: File Transfer Progress Graphs"
discourse: ngraham
date: "2025-03-15T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover the highlights of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

This week, Plasma 6.4 began to take shape. A bunch of impactful features and UI improvements landed, not to mention some juicy technical changes in the form of a newly-implemented Wayland protocol and HDR energy efficiency improvements. Just a whole lot of good stuff! Check it out below:



## Notable new Features

### Plasma 6.4.0
After being punted from Plasma 6.3, per-virtual-desktop custom tile layouts are now implemented for 6.4! (Vlad Zahorodnii, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/6893))

Clicking the "Details" button on a system notification showing file transfer progress will now reveal a graph showing the transfer speed over time! (Méven Car, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4851))

![](file-transfer-notification-with-graph.png)

You can now fully disable System Tray icons provided by apps that lack an internal setting for this (looking at you, Discord). Note that this could potentially break apps as they won't know their tray icon isn't being shown, so only use this feature if you know what you're doing! A warning message explains this, too. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5283))

![](disabled-sni-feature-with-warning.png)



## Notable UI Improvements

### Plasma 6.3.4
Plasma's sidebar-style UI elements (e.g. the Activity Switcher sidebar) now overlap panels when shown outside of Edit Mode. This looks nicer and helps communicate focus better. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=480890))

![](sidebar-covering-panel.png)


### Plasma 6.4.0
Improved KRunner search result ordering by adding the power and session actions into the default set of favorite actions, ensuring they appear first when searched for. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=474981))

![](reboot-first-in-krunner.png)

Refined the heuristic for when a panel widget's popup will be displayed centered on the panel or the screen, so that it happens more often in cases where you obviously configured your panel with this in mind. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=500390))

![](centered-kickoff-over-fit-content-panel.png)

In the panel configuration dialog, the little wireframe visualizations for options now all visually reflect their panel's actual position on screen. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=499284))

![](panel-visualizations-oriented-accurately.png)

You can now configure which modifier keys plus a scroll trigger KWin's zoom effect. (Vlad Zahorodnii, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/6939))

Improved keyboard navigation in KRunner's popup: now if the pointer happens to be hovering over an item, you can still use the arrow keys to move the selection highlight to a different item. (Christoph Wolk, [link](https://bugs.kde.org/show_bug.cgi?id=501350))

If you're being slowly driven mad by the system notification telling you how to regain control when an app like Input Leap is using the input devices, you can now disable it like you can any other notification. (David Redondo, [link](https://bugs.kde.org/show_bug.cgi?id=500130))

Plasma widgets in the System Tray that hide completely when they deem themselves not relevant no longer do this when placed in standalone form on the panel; we reasoned that in this case, if you put them there yourself, you probably always want to see them! (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=488455))

Widgets using the `ExpandableListItem` component — commonly seen in the System Tray — now display tooltips on hover for any list items with labels so long they've become elided. This was very uncommon, which is how we missed it until now! (Kai Uwe Broulik, [link](https://invent.kde.org/plasma/libplasma/-/merge_requests/1194))

When you've configured the Kickoff Application Launcher to only show app names or only show app descriptions, you'll no longer see tooltips with the labels you said you didn't want. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=501380))


### Frameworks 6.13
Implemented touch scrolling in open/save dialogs. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=406513))

Improved KRunner search result ordering in another way as well, by returning to the older style of strictly respecting the ordering that the user user configured. (Nate Graham, [link](https://bugs.kde.org/show_bug.cgi?id=489866))



## Notable Bug Fixes

### Plasma 6.3.3
Fixed a bug that could cause Discover to get stuck refreshing forever following flaky network connectivity. (Aleix Pol Gonzalez, [link](https://bugs.kde.org/show_bug.cgi?id=500513))

Fixed some layout glitches affecting the folder chooser dialog at certain window sizes. (Luke Horwell, [link](https://invent.kde.org/plasma/plasma-integration/-/merge_requests/168))


### Plasma 6.3.4
Fixed a bug that would cause fit-content panels with Task Manager widgets on them to not immediately shrink as expected when apps or windows were closed. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=499914))

Fixed a glitch in the Bluetooth wizard's scrollable device view that made scrolling using a touchscreen unreliable. (Marco Martin, [link](https://invent.kde.org/plasma/bluedevil/-/merge_requests/202))


### Plasma 6.4.0
A notorious Plasma 6 panel bug has been fixed: now when there are multiple panels sharing the same screen edge, they're all displayed properly, and reserve only as much space away from the screen edge as the thickest panel. (Niccolò Venerandi. [link 1](https://bugs.kde.org/show_bug.cgi?id=477939) and [link 2](https://bugs.kde.org/show_bug.cgi?id=500668))

Fixed a visual glitch involving the ruler for resizing custom-length panels in auto-hide mode. (Niccolò Venerandi, [link](https://bugs.kde.org/show_bug.cgi?id=500718))


### Other bug information of note:
* 2 very high priority Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 23 15-minute Plasma bugs (down from 24 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 98 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-03-08&chfieldto=2025-03-15&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)




## Notable in Performance & Technical

### Plasma 6.3.4
Improved the pixel-perfection of various KWin effects, including Wobbly Windows. (Xaver Hugl, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/7340))


### Plasma 6.4.0
KWin's codebase has been formally split between an X11 version and a Wayland version, allowing the Wayland version to develop faster and the X11 version to avoid accumulating bugs due to changes in a shared base combined with a lack of testing (82% of users with telemetry turned on use Wayland now). This will continue until Plasma 7, at which point it's highly likely the dedicated X11 session will be removed. Note that the Wayland-only version will continue to run XWayland-using apps just as it can right now. More information about this can be found in [Vlad's blog post on the topic](https://blog.vladzahorodnii.com/2025/03/13/kwin_x11-and-kwin_wayland-split/)!

Added support for P010 color-formatted videos, improving power efficiency for playing full-screen HDR video content. (Xaver Hugl, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/6352))

Implemented support for the `get_input_idle_notification` Wayland Protocol. (Xaver Hugl, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/6971))

Improved the reliability with which the Power & Battery widget is able to detect and display battery information for Bluetooth devices. (Kai Uwe Broulik, [link](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/300))

It's no longer possible to create new Plasma Vaults using the EncFS encryption system, as it's been discontinued and has known security vulnerabilities. You can still use existing EncFS vaults; you just can't create new ones. (Nate Graham, [link](https://invent.kde.org/plasma/plasma-vault/-/merge_requests/57))

Continued to resolve binding loops and QML warnings throughout Plasma, succeeding at handling a large fraction of the ones seen in basic usage that are within KDE's power to resolve without Qt changes. (Christoph Wolk, [link 1](https://invent.kde.org/plasma/libplasma/-/merge_requests/1286), [link 2](https://bugs.kde.org/show_bug.cgi?id=500338), [link 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5308), [link 4](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5308), [link 5](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5306), [link 6](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5305), [link 7](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2886), [link 8](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2877), [link 9](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2879), [link 10](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/424), [link 11](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2880), [link 12](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5311), [link 13](https://bugs.kde.org/show_bug.cgi?id=483859), [link 14](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/685), [link 15](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2885), and [link 16](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2882))




## How You Can Help
KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
