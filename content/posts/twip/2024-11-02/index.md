---
title: "This Week in Plasma: spoooooky ooooooooom notifications!"
discourse: ngraham
date: "2024-11-02T4:00:00Z"
aliases:
 - /2024/11/01/this-week-in-plasma-spoooooky-ooooooooom-notifications/
authors:
 - nategraham
image: thumbnail.png
categories:
 - This Week in Plasma
newsletter: 'a22045a1-6a5e-461e-9ddf-f61ae57022ea'
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to the new home of "This Week in Plasma"! No longer is it a private personal thing on [my (Nate Graham's) blog](https://pointieststick.com), but now it's a weekly series hosted here on KDE's infrastructure, open to anyone's participation and contribution! I'll remain the editor-in-chief for now, and welcome contributions via direct push to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma). And after a post is published, if you find a typo or broken link, feel free to just fix it.

Anyway, this week we added a useful service to detect out-of-memory (OOM) conditions, did some UI polishing, and also a lot of bug-fixing! Check it out:


## Notable New Features

When the system has run out of memory (OOM) and the kernel terminated an app, there's now a little service to detect this and show you a system notification explaining what happened, plus suggestions for what you can do about it in the future. (Harald Sitter, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4204))

![](oom-notifier.png)


## Notable UI Improvements

The Emoji Selector app now does sub-string matching from the middle of words too, so you can find emojis more easily. (Eren Karakas, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=435787))

The grouping indicator in the Task Manager widget (which looks like a little plus sign) is no longer always green; now it follows the current accent color! (Tem PQD, 6.3.0. [Link](https://invent.kde.org/plasma/libplasma/-/merge_requests/1214))

![](accent-colored-grouping-indicator.png)

Appropriate symbolic icons from will now be automatically substituted for apps' system tray icons — when they exist in the icon theme. If you'd like to extend this to more apps, figure out what icon name the app asks for, then [create a symbolic version of it](https://community.kde.org/Guidelines_and_HOWTOs/Icon_Workflow_Tips), append `-symbolic` to the name, and [submit it to the Breeze Icons repo](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests)! (Marco Martin, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=487026))


## Notable Bug Fixes

Fixed a way that misbehaving XWayland-using apps could make KWin freeze. (Vlad Zahorodnii, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=442846))

Fixed a bug causing notifications about modifier key changes (if you've turned them on) to not actually appear. (Nicolas Fella, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=495264))

Fixed a bug in the Bluetooth pairing wizard that caused it to be annoying to enter digits. (Daniil-Viktor Ratkin, 6.2.3. [Link](https://invent.kde.org/plasma/bluedevil/-/merge_requests/189))

Reviews on Discover's app pages now load properly when the app is accessed from the Home page, as opposed to after browsing or searching. (Aleix Pol Gonzalez, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=495597))

Fixed the Task Manager widget's setting to reverse the direction that new tasks appear in so that it works as originally designed, and not only halfway there. (Michael Rivnak, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=489648))

Fixed a bug that caused customized user avatars for other currently-logged-in users to not be displayed in the User Switcher widget. (Blazer Silving, 6.2.3. [Link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/637))

Fixed a bug causing the "Show Logout Screen" item in the desktop context menu to not show enough items if you had customized the "default logout option" in the past, back when we offered that as a user-facing setting. (Nate Graham, 6.2.3 [Link](https://bugs.kde.org/show_bug.cgi?id=495390))

With global animations set to "instant", window thumbnails in KWin's Desktop Grid effect are no longer too small. (Niccolò Venerandi, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=495501))

When you've got the Application Dashboard widget set up with an icon naming style that causes the text to be long and get elided, hovering over the icon now shows a tooltip with the correct full text in it. (Tomislav Pap, 6.2.3. [Link](https://bugs.kde.org/show_bug.cgi?id=494802))

Fixed a case where KWin could crash when moving certain drawing tablet pens to the surface of the pad. (Vlad Zahorodnii, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=493027))

Fixed an issue that caused XWayland-using apps to resize in a janky and jumpy manner. (Vlad Zahorodnii, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=486464), see [detailed blog post](https://blog.vladzahorodnii.com/2024/10/28/improving-xwayland-window-resizing/))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Filter and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024/) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
