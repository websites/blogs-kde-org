---
title: "This Week in Plasma: Great Stuff for 6.4"
discourse: ngraham
date: "2025-03-01T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
<!-- image: thumbnail.png -->
fediverse: '@kde@floss.social'
---

<!-- ![](thumbnail.png) -->

Welcome to a new issue of "This Week in Plasma"! Every week we cover as much as possible of what's happening in the world of KDE Plasma and its associated apps like Discover, System Monitor, and more.

With Plasma 6.3's teething problems largely solved, this week there were a lot of new features and major UI improvements!




## Notable new Features

### Plasma 6.4.0
KRunner is now aware of various types of color codes, and can display the color and other textual representations of it. (Kai Uwe Broulik, [link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/682))

![](krunner-colors-runner.png)

The "Disks & Devices" widget now checks newly-connected disks for file system errors and offers to automatically correct any that it finds. (Ilya Pominov, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3758))




## Notable UI Improvements

### Plasma 6.3.3
Fixed and improved a large number of keyboard navigation issues throughout Plasma, including in the System Tray, Kicker Application Menu widget, Custom Tiling UI, and more. (Christoph Wolk and Akseli Lahtinen, [link 1](https://bugs.kde.org/show_bug.cgi?id=500705), [link 2](https://bugs.kde.org/show_bug.cgi?id=500703), [link 3](https://bugs.kde.org/show_bug.cgi?id=500702), [link 4](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/329), [link 5](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5245), [link 6](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2780), and [link 7](https://bugs.kde.org/show_bug.cgi?id=500666))


### Plasma 6.4.0
Spectacle has gotten a big UI overhaul! Now it launches by default into the Rectangular Region overlay (this is configurable, of course), allowing you to drag a box to screenshot an area or immediately capture the whole screen, annotate on anything, and pick any other type of screenshot or recording. This new UX is much less modal and feels great to use! (Noah Davis, [link](https://invent.kde.org/graphics/spectacle/-/merge_requests/431))

<video class="img-fluid" controls>
    <source src="spectacle-rectangular-region-ui-overhaul.mp4" type="video/mp4">
</video>

<br/><br/>

After considering lots of feedback, it's once again possible to hide the audio player indicators on Task Manager tasks. In addition, you can do the same for the audio controls on individual task tooltips if you want. In a nutshell, now it's even easier to customize the UI of the tooltips to suit your preferences and purposes than it was before! (Oliver Beard, [link](https://bugs.kde.org/show_bug.cgi?id=499891))

The "About This System" page in Info Center and System Settings now indicates the system's memory more accurately, displaying both the physical amount and also the actually usable amount, and offering information about why the numbers might differ. (Oliver Beard, [link](https://bugs.kde.org/show_bug.cgi?id=500412))

![](more-accurate-memory.png)

The color picker on System Settings' "Colors" page once again uses the nicer color picker that it used in the past. (Christoph Wolk, [link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/5243))

Various pieces of text on System Settings' "Display & Monitor page" are now translated, and use fancier typographical characters. (Emir Sari, [link](https://invent.kde.org/plasma/kscreen/-/merge_requests/353))




## Notable Bug Fixes

### Plasma 6.3.2
Switching Global Themes with the "Desktop and window layout" option selected no longer breaks Plasma until being restarted. (Marco Martin, [link](https://bugs.kde.org/show_bug.cgi?id=498175))

Fixed multiple KWin issues related to window dragging and snapping that could cause windows to inappropriately snap to UI elements on other screens, and either move too slowly or crash KWin when dragged along the top edge of a screen. (Yifan Zhu, [link](https://invent.kde.org/plasma/kwin/-/merge_requests/5296))

Fixed a case where KWin could crash with very specific hardware setups after any of the screens went to sleep and woke up. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=500031))

Fixed an issue that could cause the brightness level of an external screen to forget its prior value and reset to 100% every time it went to sleep and woke up. (Xaver Hugl, [link](https://bugs.kde.org/show_bug.cgi?id=494408))

Fixed touch scrolling in the Kickoff Application Launcher widget. (Fushan Wen, [link](https://bugs.kde.org/show_bug.cgi?id=500452))

Improved the way Plasma notifications are read by screen readers. (Christoph Wolk, [link](https://bugs.kde.org/show_bug.cgi?id=500621))

Fixed a bug that caused the cursor to briefly turn into an X after right-clicking on something in an XWayland-using app. (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=499965))

Disabled UI elements in Plasma are no longer inappropriately visually highlightded around their edges when hovered with the pointer. (Nate Graham, [link](https://invent.kde.org/plasma/libplasma/-/merge_requests/1276))


### Plasma 6.3.3
Worked around a GTK bug that caused in-window menus in GTK apps to behave strangely when clicking on one menu and then moving the pointer over to another one. (Vlad Zahorodnii, [link](https://bugs.kde.org/show_bug.cgi?id=490833))

The maximum number of dots shown per day on the calendar event view grid has returned to five, up from three. (Tino Lorenz, [link](https://bugs.kde.org/show_bug.cgi?id=500785))

The weather widget now shows a more accurate icon for the current day's forecast. (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=425770))


### Plasma 6.4.0
It's once again possible to edit the text labels of existing keyboard layouts on System Settings' "Keyboard" page, and and not just newly-selected ones. (Ismael Asensio, [link](https://bugs.kde.org/show_bug.cgi?id=500057))


### Frameworks 6.12
Fixed a minor visual glitch relating to header colors in certain apps not changing properly after switching certain color schemes. (Arjen Hiemstra, [link](https://bugs.kde.org/show_bug.cgi?id=495694))


### Other bug information of note:
* 3 very high priority Plasma bug (up from 1 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 27 15-minute Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 129 KDE bugs of all kinds fixed over the past week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2025-02-22&chfieldto=2025-02-28&chfieldvalue=RESOLVED&list_id=3040618&query_format=advanced&resolution=FIXED)




## Notable in Performance & Technical

### Plasma 6.3.3

It's now possible to choose even more granular scale factors on System Settings' "Display & Monitor" page so that the scale perfectly matches the pixel pitch of the screen. This was something you could always do using the command-line `kscreen-doctor` tool, but now you can do it using the UI as well. (Allan Gardner, [link](https://bugs.kde.org/show_bug.cgi?id=500531))




## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

You can also help us by [making a donation!](https://kde.org/donate) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world.

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
