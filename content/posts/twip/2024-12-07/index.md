---
title: "This Week in Plasma: Oodles of features!"
discourse: ngraham
date: "2024-12-07T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
image: clone-panel.png
---

I promised new features soon, and here they are! There are plenty of positive UI changes too. Hopefully what this week's post lacks in quantity will be made up by depth, because these are some nice changes that have been in development for quite some time. Have a look:


## Notable New Features

It's now possible to clone a panel! (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=412588))

![](clone-panel.png)

KWin's Custom Tiling system now remembers tile arrangement on a per-virtual-desktop basis. (Marco Martin, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6631))

You can now set keyboard shortcuts to move windows between Custom Tiling (as opposed to Quick Tiling) tile zones based on directionality. No default shortcuts were set up for now because all the obvious Modifier+Arrow combinations were already taken. This is an avenue to ponder further in the future. (Akseli Lahtinen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=466687))

It's now possible to limit the upper and lower ranges for tablet pen pressure, not just the shape of the pressure curve. (Joshua Goins, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6058))


## Notable UI Improvements

Categories in Kickoff no longer automatically switch on hover by default; they have to be clicked like all other list items elsewhere. This fixes a host of issues related to unexpected category switching and freezes when moving the pointer rapidly over categories. Those who preferred switch-on-hover can turn it back on if they like. (Noah Davis, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=452636))

The way Quick Tiling (i.e. with <kbd>Meta</kbd>+<kbd>Arrow keys</kbd>) works has been slightly changed; now when trying to tile a window in a direction it can't be tiled in anymore because it has hit a screen edge with nothing beyond it, it will simply sit there, rather than un-tiling and teleporting to a potentially unexpected place. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6857))

System Settings' Display & Monitor page now shows a slider for normal/SDR brightness for each screen, just in case you expected to find it there rather than in the System Tray's Brightness and Color widget. (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kscreen/-/merge_requests/327))

When you hold down <kbd>Alt</kbd>+<kbd>Tab</kbd> to open the window switcher and then keep those keys held down, the selection highlight will now go all the way to the end, but will no longer hilariously wrap around infinitely until you release the keys again. (Ismael Asensio, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=454474))

The active virtual desktop is now remembered per activity. (Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=390295))


## Notable Bug Fixes

Fixed a bug that could cause placeholder and typed text to overlap in KRunner's search field under certain circumstances. (Jack Xu, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=496896))

Metadata displayed for Bing picture of the day wallpapers is now displayed correctly. (George Travelbacon, 6.2.5. [Link](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/642))

When you copy images from Plasma notifications, they can now be pasted into sandboxed apps. (Alessandro Astone, 6.2.5. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4985))

After using an application that goes through the input capture portal (e.g. [Input Leap](https://github.com/input-leap/input-leap)) and it quits unexpectedly, you now regain full control of your pointer and keyboard immediately. (David Redondo, 6.2.5 [Link](https://bugs.kde.org/show_bug.cgi?id=492516))

Keyboard navigation between a filtered subset of windows in the Overview effect now works as you expect it to. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=495536))

When you delete a panel but haven't yet dismissed the option to unto this, the deleted panel no longer inappropriately and surprisingly responds to any keyboard shortcuts that toggle any of their widgets. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=454549))


Other bug information of note:

* 3 Very high priority Plasma bugs (same as last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 35 15-minute Plasma bugs (down from 37 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 110 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-11-29&chfieldto=2024-12-06&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Slightly increased the performance of every app and window that uses `KWindowStateSaver`. (David Edmundson, Frameworks 6.9, [Link](https://invent.kde.org/frameworks/kconfig/-/merge_requests/339))


## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

Thankfully, thousands of you have stepped up in the past week to do just that financially, [donating](https://kde.org/donate) a [record-breaking amount of money](https://pointieststick.com/2024/12/02/i-think-the-donation-notification-works/) to KDE e.V., which is just incredible, awe-inspiring even.

So that's a great way to help out. But if you've got more time than money or want to make a difference more directly, then you can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

<!-- You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world. -->

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
