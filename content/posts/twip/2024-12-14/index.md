---
title: "This Week in Plasma: Better fractional scaling"
discourse: ngraham
date: "2024-12-14T4:00:00Z"
authors:
 - nategraham
categories:
 - This Week in Plasma
newsletter: true
fediverse: '@kde@floss.social'
---

This week's headliner change is something that I think will make a lot of people happy: better fractional scaling! Vlad and Xaver have been hard at work to snap everything to the screen's pixel grid, with the effect that using a fractional scale factor now results in a *lot* less blurriness as well as no more gaps between windows and their shadows. You'll see it in the screenshot below (which was taken at 175% scale) but the effects are subtly better everywhere. Really great stuff!

And lots more too, of course:


## Notable New Features

At very high zoom levels, KWin's Zoom effect switches to a sharp pixel-perfect representation and overlays a grid on top of the screen. This makes it easy to see how individual pixels look relative to other ones, which can be useful for artists and designers. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6878))

![](pixel_grid.png)

KWin now offers you the option to prefer screen color accuracy at the expense of some system performance, should that be your preference (e.g. if you're a digital artist and not a gamer). (Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/issues/256))

If the feature to be able to maximize a window horizontally or vertically by double-clicking on one of its edges doesn't agree with you, you can now disable it. (Vlad Zahorodnii, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6866))


## Notable UI Improvements

Landed a huge overhaul of how fractional scale factors are handled in KWin. Now it makes an effort to always snap things to the screen's pixel grid, greatly reducing blurriness and visual gaps everywhere. I've been using these patches with a 175% scale factor for a week, and everything looks just fantastic! (Vlad Zahorodnii and Xaver Hugl, 6.3.0. [Link](https://invent.kde.org/plasma/kwin/-/issues/257))

On login, Plasma panels now appear on screen only after their contents have been fully loaded. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=447476))


## Notable Bug Fixes

Fixed a nasty bug affecting people using the X11 session that could sometimes cause the lock screen to be all black. (Philip Müller, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=483163))

Fixed a specific instance where you could end up with a black screen when wiggling the pointer while the screen is about to lock. (David Redondo, 6.2.5 [Link](https://invent.kde.org/plasma/kwin/-/merge_requests/6884))

Fixed a visual bug in Discover that caused UI elements to overlap on expanded list items on the Updates page. (Aleix Pol Gonzalez, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=491821))

Fixed the application menu appearing in a wrong position when opened via the window titlebar with Qt 6.8. (David Redondo, 6.2.5. [Link](https://bugs.kde.org/show_bug.cgi?id=495787))

Fixed a bug that could cause windows on a screen that gets disconnected to become lost and stuck in an off-screen position in the new screen arrangement. (Vlad Zahorodnii and Xaver Hugl, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=494001))


You can no longer slightly break the Overview effect's Desktop Grid view by dragging windows outside of the screen area. (Niccolò Venerandi, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=493708))

Dragging an image from the clipboard to the desktop now shows the normal drop menu, rather than creating an empty Media Frame widget. (Fushan Wen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=478584))


Non-rectangular-region screenshots taken in Spectacle and copied to the clipboard can now be pasted into Dolphin as expected. (Fushan Wen, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=491961))

Standalone (not in System Tray) "Power and Battery" and "Brightness and Color" widgets once again work properly, as expected. (Jakob Petsovits, 6.3.0. [Link](https://bugs.kde.org/show_bug.cgi?id=488915))

Fixed a bug in the Breeze Dark icon theme that caused places/folder icons to remain colorful at small sizes where symbolic icons are normally expected. (David Redondo, Frameworks 6.9. [Link](https://bugs.kde.org/show_bug.cgi?id=482648))

Plasma and lots of apps no longer crash when your `/etc/fstab` file contains any loop mounts in it. (Nicolas Fella, Frameworks 6.10. [Link](https://bugs.kde.org/show_bug.cgi?id=497299))

Other bug information of note:

* 2 Very high priority Plasma bugs (down from 3 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888339&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 32 15-minute Plasma bugs (down from 35 last week). [Current list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2888340&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 104 KDE bugs of all kinds fixed over the last week. [Full list of bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-12-07&chfieldto=2024-12-13&chfieldvalue=RESOLVED&list_id=2888341&query_format=advanced&resolution=FIXED)


## Notable in Performance & Technical

Ported the clipboard to use a standard SQLite database, rather than its own internal custom format. This improves reliability, support for saving many data types, and memory efficiency especially with images. (Fushan Wen, 6.3.0. [Link](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4664))

## How You Can Help

KDE has become important in the world, and your time and contributions have helped us get there. As we grow, we need your support to keep KDE sustainable.

Thankfully, thousands of you have stepped up in the past week to do just that financially, [donating](https://kde.org/donate) a [record-breaking amount of money](https://pointieststick.com/2024/12/02/i-think-the-donation-notification-works/) to KDE e.V., which is just incredible, awe-inspiring even.

So that's a great way to help out. But if you've got more time than money or want to make a difference more directly, then you can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved) somehow. Each contributor makes a huge difference in KDE — you are not a number or a cog in a machine!

You don’t have to be a programmer, either. Many other opportunities exist:
* [Triage and confirm bug reports, maybe even identify their root cause](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)
* [Contribute designs for wallpapers, icons, and app interfaces](https://community.kde.org/Get_Involved/design)
* [Design and maintain websites](https://community.kde.org/KDE.org)
* [Translate user interface text items into your own language](https://community.kde.org/Get_Involved/translation)
* [Promote KDE in your local community](https://community.kde.org/Get_Involved/promotion)
* […And a ton more things!](https://community.kde.org/Get_Involved)

<!-- You can also help us by [donating to our yearly fundraiser!](https://kde.org/fundraisers/yearend2024) Any monetary contribution — however small — will help us cover operational costs, salaries, travel expenses for contributors, and in general just keep KDE bringing Free Software to the world. -->

To get a new Plasma feature or a bugfix mentioned here, feel free to push a commit to [the relevant merge request on invent.kde.org](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?scope=all&state=opened&search=this+week+in+plasma).
