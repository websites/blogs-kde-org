---
title:   "kexi konqueror like tabbing and other goodies"
date:    2003-07-30
authors:
  - lucijan
slug:    kexi-konqueror-tabbing-and-other-goodies
---
lots of stuff is happening to kexi lately. (remember kexi is the database "frontend" in koffice).
first of all kexi is now (after jowenn's commit) provideing konqueror like tabbing see http://blogs.kde.org/node/view/108 for a screenshot.
<!--break-->
but beside this nice gui the database part changes quite much. we are about to change our old kexiDB database library (which transparently manages different database engines and different types of database engines)* with a new better designed one which doesn't require a any graphical environment. the new library will also have a better error system allowing e.g. translated normalized error messages. beside that we will write a sql parser which will inform users better about what is actually happening and what won't work or may have a wrong result. e.g. type checking.
the second change in the database part is that we decided to use sqlite (http://www.sqlite.org) as default engine (maybe with some chagned called kexisql). this will result in big changes e.g. we plan to replace our existing project files with a sqlite database which stores project relevant data like connection credentials, forms, queries, reports.
after sqlite is the default engine of kexi users can have their data in one .kexi file, copy them to a floppy to carry them to colegues and friends. we will still support remote engines like mysql and postgres and will implement odbc bindnigs in the near future.