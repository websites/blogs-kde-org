---
title:   "Microsoft ditch IronPython and IronRuby"
date:    2010-08-08
authors:
  - richard dale
slug:    microsoft-ditch-ironpython-and-ironruby
---
<p>By and large I don't really care about what Microsoft do - I don't use their software, and I actively avoid making my career dependent on them. But I am a fan of the C# programming language and think the Qyoto/Kimono bindings for the Qt and KDE apis are pretty neat.</p>

<p>However, the recent decision to <a href="http://blog.jimmy.schementi.com/2010/08/start-spreading-news-future-of-jimmy.html">lay off all the IronPython and IronRuby guys</a> is so monumentally stupid, I don't know where to start. I still think C# and Mono are really innovative, and don't agree with all the Free Software trolling against them, even when it is from Richard Stallman himself. So what should we do? Arno is <a href="http://www.arnorehn.de/blog/2010/07/java-bindings/">clearly not happy</a> with working on C# bindings even though they are technically pretty amazing. Steve Ballmer why are you are still in charge?</p>