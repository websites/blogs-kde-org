---
title:   "Defending Free Software against Oracle's attack"
date:    2010-08-16
authors:
  - richard dale
slug:    defending-free-software-against-oracles-attack
---
<p>I've been fascinated by the Oracle attack on Google's Android. I don't follow sport and just couldn't understand why so many people were getting excited about the World Cup at Akademy. But to me these epic disputes are a great spectator sport, as well as an opportunity to participate individually. The trouble is that it is all a bit slow, even slower than cricket - if it is like the SCO vs Linus train wreck, it could take up to five years to be resolved.</p>

<p>Anyhow, I've been doing some 'prematch' preparation reading all the blogs and news sites, studying the patents and so on. I think everyone should start by watching this <a href="http://sites.google.com/site/io/dalvik-vm-internals">presentation on Dalvik</a> by Dan Bornstein the lead developer of Dalvik. If you watch the presentation it is clear that Dan is no Java guy, and when people ask him why did the Dalvik team choose the Java language for programming Android, he answers that Java has great tooling like Eclipse and there are many people who are familiar with the Java programming language. It was a pragmatic decision. If there were more people familiar with C# and the tooling on Linux was better, then maybe they would have chosen C#. To them Java is just another programming language. Likewise, to me, Java is just another programming language, and C# is just another programming language. The only sort of people who have an obsession about 'Programming Language A' being better than 'Programming Language B' are bad programmers. That doesn't mean that language 'A' may or may not be better than language 'B' for specific uses, but these 'religious types' are obsessed with their choice of language.</p>

<p>So if the choice of the Java programming language for Android was just a pragmatic decision, then why are Oracle suing Google for choosing Java? The best article I have found about the possible motivations is <a href="http://redmonk.com/sogrady/2010/08/14/oracle-v-google">Oracle v Google: Why?</a>.</p>

<p>I don't think Google developed Dalvik to work round licensing and patent problems with Java, they developed it simply because Sun's Java technology wasn't good enough for what they wanted to do. If you watch Dan Bornstein's presentation that is abundantly clear. Designing a new virtual machine runtime is hard, but not that hard. The JVM was influenced was influenced by the Pascal pcode system, and the Smalltalk virtual machine architecture from the 1970s has also been very influential. Recently there have been a pile of virtual machines for JavaScript being developed. Thirty years later after Smalltalk-80 the technology of virtual machines and JIT compilation is really mainstream.</p>

<p>However, Oracle appear to think differently. In their opinion they 'own' certain aspects of virtual machine, JIT compilation and Java .class library optimizations that nobody else is allowed to use without expensive licensing. These are the patents:

	<ol>

	<li><a href="http://www.google.com/patents/about?id=dyQGAAAAEBAJ&dq=6125447">6125447 Protection domains to provide security in a computer system</a>
	<li><a href="http://www.google.com/patents/about?id=G1YGAAAAEBAJ">6192476 Controlling access to a resource</a>
	<li><a href="http://www.google.com/patents/about?id=TzsPAAAAEBAJ">6530080 Method and apparatus for pre-processing and packaging class files
</a>
	<li><a href="http://www.google.com/patents/about?id=U-4UAAAAEBAJ">6910205 Interpreting functions utilizing a hybrid of virtual and native machine</a>
	<li><a href="http://www.google.com/patents/about?id=mEwEAAAAEBAJ">6061520 Method and system for performing static initialization
</a>
	<li><a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=/netahtml/PTO/srchnum.htm&r=1&f=G&l=50&s1=7426%2C720.PN.&OS=PN/7426%2C720&RS=PN/7426%2C720">7426,720 System and method for dynamic preloading of classes through memory space cloning of a master runtime system process</a>
	<li><a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=/netahtml/PTO/srchnum.htm&r=1&f=G&l=50&s1=RE38%2C104.PN.&OS=PN/RE38%2C104&RS=PN/RE38%2C104">RE38,104  Method and apparatus for resolving data references in generated code</a>
	</ol>
</p>

<p>I think the first question to ask is which of the above patents are specific to Java, and which could apply to any virtual machine implementation. 

<ul>
<li> Patent #1 talks about classes but doesn't specifically mention Java. It is really about downloading classes from the internet, which Android doesn't do. So we can count that one out.</li>
<li> Patent #2 is about inter-thread protection domains which AFAIK Dalvik doesn't have. If it did have this, and it was removed I don't think it would be a great loss anyway.</li>
<li> With Patent #3 it does seem to have something to do with what Dalvik does. It talks about repackaging multiple .class files into a single package to make them smaller. If the Dalvik tool to convert .class files to a .dex file does the equivalent to what the patent says, does that mean that it violates the patent? It talks about allocating dynamic memory a lot, whereas the .dex conversion is a build time thing. To me that means the patent doesn't apply, apart from what the patent does being fairly simple stuff to consolidate constant pools and so on.
</li>
<li>Patent #4 is about JIT technology. This is pretty standard stuff in 2010 and there are many JavaScript based JIT compilers. The patent talks about overwriting a specific JVM virtual machine instruction with a native code version. I'm not sure what this means in fact. Surely the virtual machine instructions have been optimized by very clever people at compile time, and runtime optimization doesn't make much sense. Whatever tech this is talking about appears to address some very specific problem of the JVM and it is unlikely that it would apply to the Dalvik VM.</li>
<li>Moving on to patent #5 'Method and system for performing static initialization', we have a technique which clearly doesn't apply to Dalvik because it is all done at runtime, whereas Dalvik does it at compile time when the .dex files are generated. So I don't think this patent applies.</li>
<li>In patent #6 we have 'System and method for dynamic preloading of classes through memory space cloning of a master runtime system process'. Well I think this would be an interesting patent if Sun had actually invented the fork() call, and copy on write of memory that the patent depends on. But they haven't and all the patent is doing is to describe a very common way to use Unix/Linux fork() calls with copy on write memory access. This technique is used by KDE where there is a kdeinit process which has a load of libs pre-loaded, and it forks off sub processes to speed things up. Similary on Maemo/MeeGo there is a process which waits for DBus calls and starts processes. On Android there is a process called 'Zygote' which waits on a socket call and starts processes. The JVM patent only talks about copy on write, it doesn't mention memory mapped files or shared memory which I believe Dalvik uses. This way of doing things is so generic to the way Unix system programmers design systems, that I can't see anything much original in the patent.</li>
<li>Finally we have patent #7 'Method and apparatus for resolving data references in generated code'. This talks about 'new code arriving at a computer' and how it can be given suitable permissions. Actually this is not how Android works and I think we can just dismiss this one as being irrelevant. In Android as far as I know only whole apps are installed - you don't have the concept of a class loader loading remote classes into a running app.</li>
</ul>

</p>So that is my brief run through of what I think of the individual patent claims. I am not a lawyer and so I could be completely wrong, but if enough people do this kind of technical analysis in conjunction with actual laywers, in the same way Groklow did for the SCO vs Linux case I hope we can manage to make the Oracle lawyers look like the technically illiterate people that they are (they are much the same set of apparently useless lawyers that were hired by SCO).</p>

<p>Although I don't really like Java much, Arno Rehn has been <a href="http://www.arnorehn.de/blog/2010/07/java-bindings/>thinking of doing some Java bindings</a> recently for Qt and I've been wondering if we could base them on the Android Dalvik VM and target them at the MeeGo environment. That way we could do our bit for hastening the demise of the regular JVM or the useless MIDP cut down Java environment, and help to make Dalvik the ubiquitous choice for running applications written in the Java language on small devices. The most interesting technical problem might be how to integrate the Android 'Zygote' process starter with the MeeGo DBus driven equivalent. Another interesting problem is that Dalvik supports JNI but Dan Bornstein mentioned that there was a more efficient mechanism for make native code calls. Maybe as the Smoke way of doing language bindings doesn't involve a JNI call for every method in the C++ api being wrapped, we could make use of that.</p>

<p>To me writing code is a means of Free Speech, and the Free Software movement is about preserving my rights of Free Speech in the face of the US patent system which is trying to cut down what I can say. Attacking the Oracle patent claims, and writing software to undermine their market position is the least I can do. If enough people think and do the same, I hope we can cause them trouble.</p>

<p>UPDATE</p>

<p>Charles Nutter (the JRuby developer) has written an <a href="http://blog.headius.com/2010/08/my-thoughts-on-oracle-v-google.html">absolutely superb review of the anti-Google patents</a>. I defer to his views, as he obviously knows much more than I do about the subject. He comes to similar conclusions to me though, and thinks the Oracle patents are fairly light weight and that the case would fail in the context of a rational legal system.</p>
