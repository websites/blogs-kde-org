---
title:   "JWZ on Groupware"
date:    2005-02-16
authors:
  - richard dale
slug:    jwz-groupware
---
I thought this was a <a href="http://www.livejournal.com/users/jwz/444651.html">brilliant blog</a> by JWZ explaining how Netscape fell apart, and why writing 'corporate groupware' driven by specs from faceless managerial types is absolutely not the way to develop anything people would actually want. He was cautioning Nat Friedman about getting too excited about Novell's new Hula groupware.

The best bit was this insight though:

"So I said, narrow the focus. Your "use case" should be, there's a 22 year old college student living in the dorms. How will this software get him laid? 

That got me a look like I had just sprouted a third head, but bear with me, because I think that it's not only crude but insightful. "How will this software get my users laid" should be on the minds of anyone writing social software (and these days, almost all software is social software). 

'Social software' is about making it easy for people to do other things that make them happy: meeting, communicating, and hooking up."

I wonder if KDE's killer app might be that it's got groupware that people find useful, doesn't suck, and is designed and spec'd by hackers rather than corporate management drones?