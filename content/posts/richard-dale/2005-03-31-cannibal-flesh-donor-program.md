---
title:   "Cannibal flesh donor program"
date:    2005-03-31
authors:
  - richard dale
slug:    cannibal-flesh-donor-program
---
I like to get my head round the latest and greatest in 'shiny new ideas', but I'm having trouble with umm 'digesting' <a href="http://www.fleshdonor.org/">this</a> one.
<!--break-->
I can't think of any rational objection. I personally don't eat meat, although I do eat fish, so I'm not really a vegetarian. But I just can't visualise pre-packed meat in the supermarket labelled 'tender young car accident victim', or 'well preserved old granny, good broiler'. I suppose professional marketing people would be able to think up less offputting messages. Maybe there could be a signed note from the recently deceased on the package saying how great they were to eat, and how it might save some animal being slaughtered. A bit like the genetically modified cow in the Hitchhikers Guide to the Galaxy episode, who was could talk in order to sell her good eating properties :)