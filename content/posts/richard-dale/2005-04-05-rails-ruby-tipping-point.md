---
title:   "Rails, the Ruby tipping point?"
date:    2005-04-05
authors:
  - richard dale
slug:    rails-ruby-tipping-point
---
Everywhere I look on the web these days there seem to be enthusiastic articles about Ruby on Rails, the web application framework. 
<!--break-->
On <a href="http://developers.slashdot.org/developers/05/04/04/1520227.shtml?tid=156&tid=1">Slashdot</a>, with nearly 500 comments posted in a matter of hours. Or <a href="http://www.linuxjournal.com/article/8217">Linux Journal</a> or an <a href="http://www.oreillynet.com/pub/wlg/6782">O'Reilly blog</a> where Rael Dornfest discusses what a great combination Rails and Ajax form.

Over on the ruby-talk mailing list/comp.lang.ruby usernet forum, the number of release announcements for all sorts of ruby software is exploding. The downside is that with ruby approaching Warp 9 speed, there has been some strain on the dylithium crystals. As ruby spreads into the 'normal community', flame wars break out over subjects such as whether Winders are better than Macs or whatever, where there was nothing but unfailing politeness and wit just a few months ago.

I'm hoping that the buzz will spread, and tempt more people trying out the QtRuby/Korundum/KDevelop 3.2.0 RAD enviroment. I've just done a release on RubyForge <a href="http://rubyforge.org/projects/korundum/">here</a>. The bindings build against every version of KDE from 3.1.x to 3.4.x, and so does KDevelop 3.2.0 as far as I know. So please try it out, it's a great way to learn Qt or KDE programming - you can follow the various tutorials via the <a href="http://developer.kde.org/language-bindings/ruby/"> KDE Developer's corner</a> site.