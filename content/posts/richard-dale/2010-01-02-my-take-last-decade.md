---
title:   "My take on the last decade"
date:    2010-01-02
authors:
  - richard dale
slug:    my-take-last-decade
---
It seems a long time ago, but in early 2000 I had just submitted my first patches to the KDevelop project and KDE. I had wanted to port the version of Squeak Smalltalk that ran under Apple's 'Rhapsody OS' to GNUStep, and I needed some sort of development environment to do that. 

I spent about a week getting basic Objective-C support working and I was amazed how little time it took. If I hadn't had the KDevelop source it wouldn't have been possible at all. In the meantime, I had found some bugs in the KDevelop C++ parser and submitted fixes to the mailing list. When Sandy Meier, the leader of the project accepted the patches I was really excited. Despite over 20 years of software development experience, this process of a project accepting fixes from 'complete strangers like myself' solely on the basis of whether they were any good, was completely new.

I submitted a patch for KEdit which added a settings option to prevent the current selection being automatically being copied to the clipboard, and it was accepted. That was a big moment for me because it meant that although clipboard handling in KDE 1.x totally sucked compared with OpenStep, I could actually help fix it. I realized that the open software development process in KDE would trump the better design of OpenStep in the long term.

Once I had KDevelop 1.x working with Objective-C, I thought it would be nice to have a complete Qt development environment, and started to work on Objective-C Qt bindings. Since then, I've found my niche in the KDE project and have continued to develop various language Qt/KDE bindings - C, Objective-C, Java, Ruby, C# and JavaScript with varying degrees of success.. 

So after that start, what do I think I have learned in the past 10 years? Quite a lot I would say.

A few years ago I was really blown away by two talks at FOSDEM; firstly Jimmy Wales described how Wikipedia was '10% software and 90% social engineering', and then Richard Stallman explained how the four software freedoms in the GPL license enabled a community of Free Software developers to form.

After that, I felt that a successful large scale Free Software project, like KDE, needed both a suitable license and good governance. I enjoy contributing to KDE directly with code, but I also love 'lurking' observing how things are done, and seeing how we make community decisions. Who is the Jimmy Wales of KDE? I would say Aaron Seigo, and so my third 'hero of the decade' is Aaron for the way he has understood the importance of social engineering in making the artistic and technical innovations of KDE 4.x happen. 

I am a member of the KDE eV and reading the discussions on that mailing list I get the impression that the social governance of the KDE project is really state of the art. So when people ask 'Is KDE 4.0 a revolution or an evolution?' I personally think it is a revolution because of our social engineering skills, and they trump whatever the current software release is. It is our process that counts - 'ie The Community' and the current state of the software (KDE 4.4 SC) is very much a secondary and relatively unimportant thing. Just like I really didn't care that the X-Window based clipboard handling in KDE 1.x was rubbish, the current state of the KDE software is less important because we have the development processes in place to enable future innovations to happen. In contrast, Microsoft with all the money in the World, doesn't have the right processes to make innovations happen. The are trapped in the past, with very rigid hierarchal management structures and little community involvement. That makes me very optimistic about the next 10 years of the KDE project - I feel we have only just laid the groundwork to really get started! So a Happy New Decade to all..

