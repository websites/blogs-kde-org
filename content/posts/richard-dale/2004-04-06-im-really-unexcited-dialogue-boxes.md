---
title:   "I'm really unexcited by dialogue boxes.."
date:    2004-04-06
authors:
  - richard dale
slug:    im-really-unexcited-dialogue-boxes
---
On http://www.nearwildheaven.com/GNOME/ this article presents the latest and greatest in Gnome GUI improvements. 

Do our users love this sort of 'pissing contest' between file save/open dialogs or whatever? Do they prefer to admire the subtle differences between the dialogs in Swing/Gnome/KDE or even Windows file dialog boxes? If I personally had a choice between giving up the KDE dialogs in favour of this new Gnome style to avoid confusing users, do you think I personally give a toss? KDE and Gnome are just toolkit apis, and as soon as they get this sort of nonsense behind them the better in my opinion.

-- Richard