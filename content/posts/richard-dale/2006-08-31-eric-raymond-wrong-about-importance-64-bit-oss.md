---
title:   "Eric Raymond is wrong about the importance of 64 bit OSs"
date:    2006-08-31
authors:
  - richard dale
slug:    eric-raymond-wrong-about-importance-64-bit-oss
---
<p>
I usually find what Eric Raymond has to say interesting and entertaining, and I enjoyed 'The Cathedral and he Bazaar'. But in this <a href="http://www.redherring.com/Article.aspx?a=18176&hed=Linux+Desktop+Window+Closing%3F+"> recent interview</a>, he talks about the importance of the transition from 32 to 64 bit OSs and how it creates a 'window of opportunity' to make the Linux desktop popular, that will only last until 2008.
</p>
<!--break-->
<p>
Certainly all things being equal, 64 bits are better than 32 bits, but I personally doubt whether or not your desktop OS supports 64 bits is going to make much difference at all. I agree there is a window of opportunity ahead, but for entirely different reasons:
<li>
The cost of hardware is dropping rapidly, while the cost of MicroSoft software is not. It doesn't make sense to spend 300 Euros or less on a computer, and then 500 Euros for software to run on it. It also means that PC makers like Dell can't differentiate their offerings on CPU speed/memory size/cheapness anymore. PC makers will need to increasingly rely on good aesthetic design and tight integration with system software like Apple does today. If the PC market becomes like the iPod or mobile phone market, why would I want a badly designed noisy unreliable and hard to set up black box in my living room or workplace? You need more control over you software to integrate in the way Apple does, and you can get that by using Free Software.
</li>
<li>
We can assume every personal computer is always attached to the internet. There will be no stand alone desktop apps, and users will expect networked based features such as collaboration to be built into their software from the ground up. Why do I want to spend hundreds of dollars on word processor/spreadsheet PC software, when there are free ad supported web based alternatives with better collaboration facilities such as Writely?
</li>
<li>
Every device will have 3D graphics and 3D will be ubiquitous - we can assume it's always there. What if MicroSoft just use 3D for 'eye candy' and don't take the opportunity to change the way we interact with computers for the first time in 30 years? The desktop could soon be obsolete, except in Vista perhaps, and replaced by 3D 'shared worlds' like <a href="http://www.opencroquet.org/">Croquet</a>.
</li>
</p>
<p>
Unlike, the 32 to 64 bits transition, these three factors together will be highly disruptive in that MicroSoft won't be able to move easily to the new world without losing its revenue from their old stand alone shrink wrapped PC software cash cows. Here's what ESR  has to say anyway:
</p>
<p><b>Q: Speaking of which, there have been multiple delays in the introduction of Microsoft’s Vista operating system. It sounds like you’ve got a big window of opportunity there that can’t last forever. How could you best exploit it?</b>
<i>A: That’s right; my friend Rob Landley and I have done an analysis which we’re going to publish very shortly suggesting that there is a critical window of vulnerability for changing the dominant operating system. And that is probably going to close in 2008.
<b>Q: Wow.</b>
<i>A: The reason we think that they will close them is because we’ve looked back at the history of the industry and we’ve seen that basically the only time that a dominant platform gets toppled is when the hardware platform changes out from under it, and the biggest driver of hardware platform changing out from under it is best with the changes. We saw one change, the 8- to 16-bit transition.</i>
<b>Q: Yes.</b>
<i>A: Another at the 16-to-32 bit transition, which was masked a little bit, because in that transition Microsoft succeeded in maintaining its incumbency, but they did it with a different software suite. And then there is a 32-bit to 64-bit transition going on now, which I think is going to be our best window for a long time to achieve majority market share, but the hardware trend curves indicate that the 64-bit transition will probably be over sometime in 2008, and that means that the market’s going to be making its collective decision about the dominant 64-bit operating system probably before that.</i>
<b>Q: And will multimedia be one of the killer apps of a 64-bit desktop world?</b>
<i>A: I think good support for multimedia is. It’s not a sufficient condition for 64-bit desktop dominance; there have to be other pieces in place as well. But I think it’s a necessary condition.</i>
</p>

