---
title:   "Learning KDE programming"
date:    2005-02-12
authors:
  - richard dale
slug:    learning-kde-programming
---
Hans Oischinger talks about how he found it hard to learn about <a href="http://oisch.blogspot.com/2005/02/meta-technology-chaos.html">programming the Qt/KDE api</a>.

All of his comments apply to ruby Korundum programming; in ruby you can use slots/signals, KConfig XT .kcfg files, Qt Designer .ui files, KXMLGUI .rc files, DCOP, KDE::Parts (KParts) or subclass KDE::Command (KCommand). But the problem with total ruby/KDE integration is that the docs, if they exist at all, are for the C++ api. 

I really think that KDE is the best application programming framework since NeXTStep 15 years ago or so, but it does concern me that you can't actually walk into a bookshop or download a PDF about learning the api.

Maybe as well as 'Usability, usability, usability' as a top priority for KDE users, we need 'developer docs, developer docs, developer docs' as the equivalent priority for making KDE popular as a development platform..