---
title:   "Comparing colliding mice in C++, Java, Ruby and C#"
date:    2007-06-13
authors:
  - richard dale
slug:    comparing-colliding-mice-c-java-ruby-and-c
---
<p>
Now that there is a final release of QtJambi, I've downloaded it and had a good look at the sources. I'm happy to report that it looks very well written, very thorough and with much attention paid to issues such as performance tuning and working well with Java threads.</p>
<p>
I do wonder why the team have chosen to totally reimplement slots and signals in Java though - I personally prefer the approach taken in QtRuby and Qyoto where QMetaObjects are constructed at runtime that are identical to the C++ equivalents, and the C++ side can't tell the difference. The QtJambi approach means that it will be very hard to implement bindings for the QtDBus classes, without a complete rewrite of the code in Java. Similarly, I think scripting language support or testing toolkits depend on using QMetaObjects to invoke slots via runtime introspection, and a different mechanism will be need for scripting QtJambi apps.</p>
<p>
QtJambi has a Java version of the colliding mice QGraphicsView example, and I've been comparing it with the original C++ version, along with the QtRuby and Qyoto/C# ones. I just used the 'time' command from the command line, and ran the example for one minute with each language. I had a look at the memory consumption with 'top' too.</p>
<p>
C++ used about 10s of mill in a minute on my MacBook, and according to top it took up 15.6Mb of virtual memory, 7.3Mb of resident and 6.2Mb of shared memory. Here are the memory figures from top with the ratio to the C++ value in brackets for the bindings languages:</p>
<ul>
<li><b>C++</b> virtual: 15.6Mb resident: 7.3Mb shared: 9.9Mb</li>
<li><b>QtJambi (java-6-sun-1.6) </b> virtual: 239Mb (15x) resident: 46Mb (6.27x) shared: 20Mb (3.24x)</li>
<li><b>QtRuby</b> virtual: 33.2Mb (2.13x) resident: 16Mb (2.18x) shared: 9.9Mb (1.6x)</li>
<li><b>Qyoto</b> virtual: 45Mb (2.88x) resident: 17Mb (2.32x) shared: 12Mb (1.94x)</li>
</ul>
<p>
So QtRuby and Qyoto/Mono use about twice the memory of C++, and QtJambi uses twice as much again. Next I divided the 'real time' by the 'user time' plus 'system time' figures, and calculated the mill consumption relative to C++:</p>
<ul>
<li><b>QtJambi (java-6-sun-1.6) </b> 1.8 times slower</li>
<li><b>QtRuby</b> 4.1 times slower</li>
<li><b>Qyoto</b> 5.0 times slower</li>
</ul>
<p>
We haven't done much performance tuning for the Qyoto bindings yet, and it may be possible to speed them up and beat ruby. We've been concentrating on getting them functionally complete and reasonably stable rather than as fast as possible, and so it's slightly unfair to measure before a first release. However, I've found that unless you write graphics intensive apps like the colliding mice one it doesn't actually matter and a UI written in QtRuby or Qyoto usually feels just as responsive as a native C++ one.</p>
<p>
I haven't got a current version of PyQt to try the same test, but I would expect it to be pretty much as fast as QtJambi and consume about the same memory as QtRuby or Qyoto though.</p>
