---
title:   "Introspecting Smoke libraries with the 'smokeapi' command line tool"
date:    2010-01-19
authors:
  - richard dale
slug:    introspecting-smoke-libraries-smokeapi-command-line-tool
---
<p>I've recently added a handy command line tool for introspecting the methods in Smoke libraries. Although it is mainly aimed at people using Smoke based language bindings, I think it might be more generally useful and worth describing to a wider audience.</p>

<p>Show all the methods in the QPoint class:</p>

<pre><code>
$ smokeapi QPoint
QPoint::QPoint()
QPoint::QPoint(const QPoint&)
QPoint::QPoint(int, int)
bool QPoint::isNull() const
...
</code></pre>

<p>Note that the qtcore module is always opened by default. Here only 'QPoint' is given as a class name, but you can specify more than one class</p>

<p>Next, show all the methods in the qtgui module with 'popup' in their name:</p>

<pre><code>
$ smokeapi -r qtgui -m popup
QAbstractItemView* QCompleter::popup() const
void QMenu::popup(const QPoint&)
void QMenu::popup(const QPoint&, QAction*)
QRect& QStyleOptionComboBox::popupRect() const
QToolButton::ToolButtonPopupMode QToolButton::popupMode() const
</code></pre>

<p>The '-r qtgui' option means open the libsmokeqtgui library - for KDE 4.4 SC release we added the ability to dynamically open and load the Smoke libraries, and it so that means the tool doesn't need to be linked in advance to any libraries to be searched. The '-m popup' option means match methods with the regular expression pattern 'popup'. No class names are specified and so all classes in the target Smoke libraries are searched. If you use '-i' in conjuction with the '-m' open it will do a case insensitive match.</p>

<p>Find all the signals emitted by KAction, including inherited ones:</p>

<pre><code>
$ smokeapi -r kdeui -p -m ^signal KAction
signal void QObject::destroyed()
signal void QObject::destroyed(QObject*)
signal void QAction::changed()
signal void QAction::hovered()
signal void QAction::toggled(bool)
signal void QAction::triggered()
signal void QAction::triggered(bool)
signal void KAction::authorized(KAuth::Action*)
signal void KAction::globalShortcutChanged(const QKeySequence&)
signal void KAction::triggered(Qt::MouseButtons, Qt::KeyboardModifiers)
</code></pre>

<p>The '-p' options means include the parent classes of any classes specified. If you want to search for slots you can use a pattern of '^slot' to match them.</p>

<p>One limitation is that the Smoke libraries don't included templated methods, and so they we be missed out. Other than that I find it very handy to quickly search the apis without needing to go to Qt Assistant or browse the KDE api docs</p>

<p>I also added a similar tool called 'jsmokeapi' to the JSmoke bindings which gives the method signatures in a JavaScript format.</p>
