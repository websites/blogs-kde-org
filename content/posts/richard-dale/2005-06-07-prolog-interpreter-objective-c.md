---
title:   "Prolog interpreter in Objective-C"
date:    2005-06-07
authors:
  - richard dale
slug:    prolog-interpreter-objective-c
---
Here's a blast from the past - I just found this on an old backup disk. It's a <a href="http://www.tipitina.demon.co.uk/Prolog.tar.gz">prolog interpreter in Objective-C</a> that I wrote in 1993. I was unemployed and bought a NeXSTation with the small amount of redundancy money I got after the company I worked went bust.. as they do ho, hum.. But I subsequently presented it to a company short of a NeXTSTEP programmer, and got a job. So it has great 'sentimental value' I suppose. Welcome to the world of proof trees, WAM interpreters, backtracking, and other strange long forgotten stuff!
<br>
<br>
What about a google summer of coding idea for porting this engine to ruby with a Korundum front end? This code is available under a 'do what the hell you like with it' license :)