---
title:   "KDE4 Korundum hello world working"
date:    2007-04-20
authors:
  - richard dale
slug:    kde4-korundum-hello-world-working
---
<p>I've just got hello world working with the KDE4 version of the ruby korundum bindings. Here's what it looks like:</p>
<pre>
require 'korundum4'

aboutData = KDE::AboutData.new( "tutorial1", "Tutorial 1",
                        "1.0", "KMessageBox popup",
                        KDE::AboutData::License_GPL, "(c) 2006" )
KDE::CmdLineArgs.init(ARGV, aboutData)
app = KDE::Application.new
guiItem = KDE::GuiItem.new( "Hello", "",
                    "this is a tooltip",
                    "this is a whatsthis" )
KDE::MessageBox.questionYesNo( nil, "Hello World",
                              "Hello", guiItem )
</pre>
<p>I got it from the KDE Developer's wiki <a href="http://techbase.kde.org/Development/Tutorials/Programming_Tutorial_KDE_4/First_program">here</a>. That example doesn't have the new KComponentData class that used to be KInstance, and so I'm not sure if you're supposed to pass 'aboutData' to both the KDE::Application constructor and the KDE::CmdLineArgs.init() method as above. Note that you need to require 'korundum4', so it will be possible to run the KDE3 version of Korundum at the same time</p>
<p>
Qt4 QtRuby has QtDBus support, and so it should be possible to have fun calling KDE4 apps over dbus with Korundum. Korundum needs quite a few marshallers still for the new Qt4 list types, but there are over a 1000 Qt and KDE4 classes in the smoke lib which is a good start.</p>
<p>
UPDATE: The KDE::Application constructor doesn't need to be passed a KDE::ComponentData instance. I've fixed a bug in the smoke lib code generation for Q_CLASSINFO macros that was causing some methods to be skipped like this KDE::Application constructor. I've also added quite a few KDE4 list marshallers, and now the Korundum bindings are pretty usable</p>