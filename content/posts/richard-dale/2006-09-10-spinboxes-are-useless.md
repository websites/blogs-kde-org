---
title:   "Spinboxes are useless"
date:    2006-09-10
authors:
  - richard dale
slug:    spinboxes-are-useless
---
<p>
One of my pet hates in GUI widgets is the 'spinbox', and I especially dislike the idea of a floating point spinbox. I think for technical reasons I had trouble wrapping the KDE3 floating point spinbox in korundum, and couldn't get particularly worked up about fixing it. But I was a bit depressed to find out that Qt4 has a floating point spinbox widget (although I didn't obstruct its inclusion in Qt4 QtRuby).
</p>
<!--break-->
<p>
In <a href="http://blogs.kde.org/node/2345">El's blog</a> about improving the usability of KDE's dialogs she uses spinboxes (yuck!). For instance, I see 'pointer threshold' with a default value of 4 pixels and a possible dynamic range of 1 to 20. This means to use the spinbox to raise the value from 4 to 20 you have to click on it 15 times. Surely this is braindead? If the option has a small range of possible values, it can be a pulldown menu, or if it can be a number with a large range of values, then just let the user type in the number, and validate that they have entered a number. I just hope we can ignore MicroSoft as a paragon of usability expertise and just do the right thing as a result of personal inspiration and peer review like the rest of KDE.
</p>