---
title:   "Howard Stearns on Immersive 3D"
date:    2005-10-28
authors:
  - richard dale
slug:    howard-stearns-immersive-3d
---
<p>
I been reading Howard Stearns blog on wetmachine about Croquet and the Brie widget framework he is writing for it. They're all worth reading, but I especially liked this one. He writes <a href="http://www.wetmachine.com/itf/item/289/catid/12">'What Is It About Immersive 3D'</a>
</p>
<!--break-->
<p>
I thought this paragraph was particularly interesting:
</p>
<i>In developing the Brie UI framework for Croquet, I've been forced to think about how Croquet is different from the overlapping window interface in Windows or Mac OSX. I hadn't realized that a lot of the design is built around these frames that hold the application components, and how these frames are divided by application. (These frames are called “windows