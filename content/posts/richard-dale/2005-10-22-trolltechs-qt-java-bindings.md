---
title:   "Trolltech's Qt-Java bindings"
date:    2005-10-22
authors:
  - richard dale
slug:    trolltechs-qt-java-bindings
---
Aaron <a href="http://aseigo.blogspot.com/2005/10/troll-tech-dev-days-05-in-san-jose.html">writes</a>:

<i>haavard announced that by Q1-06 they'll be releasing a tech preview of java bindings for qt4 that will be officially supported. wow.</i>

<!--break-->

I'll be interested in how the java bindings are implemented, and whether they are auto-generated or not. Will it be easy to extend them to wrap the kde libs? I hope they are successful. I think it needs a big company like Trolltech to do the marketing to make a java binding a success - I never managed to make my version very popular. The QtRuby ones seem to be getting more traction than I ever got with QtJava.

I was thinking of starting on a Qt4 version of java bindings based on the Smoke library and dynamic proxies, but there's no point in doing anything now. As I was going to scrap the current Qt3 java bindings, I haven't wasted much time. I'm personally not much of a fan of Java, I don't actively dislike it, but can't get very enthusiatic about it either. It's a bit easy to use than C++, but with java 1.5 additions like generics it no longer seems a particularly simple language to learn.

So this announcement is a relief in a way - I can concentrate improving the QtRuby/Korundum bindings, and ruby support in KDevelop. I don't have to feel slightly guilty for neglecting the java bindings in favour of ruby anymore.