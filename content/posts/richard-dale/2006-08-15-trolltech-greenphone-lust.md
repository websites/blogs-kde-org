---
title:   "Trolltech Greenphone Lust"
date:    2006-08-15
authors:
  - richard dale
slug:    trolltech-greenphone-lust
---
<p>
I've just read about the forthcoming <a href="http://www.linuxdevices.com/news/NS8030785497.html">Greenphone</a> with a Qtopia development kit. If I can port Qt4 QtRuby to it, I want one right now! I could hack together custom apps to access web services like google via GPRS. It has 64 Mb of RAM and 128 Mb of flash, with expansion slot which sounds as though it should be enough to fit a QtRuby development environment. Does it take standard sims, or can you only use it with certain service providers?
</p>
<!--break-->
<p>
I only bought my first ever mobile phone a couple of weeks ago, a Nokia 1600 and I think it's brilliant. It was only 50 Euros or so, and I'm completely blown away how many features such a cheap device has - colour screen with easy to use GUI, contacts list, sms messages etc. I never wanted one before because I don't like phones at work much, as I find them very distracting. I thought having one in your pocket permanently on would make me feel like a doctor on call, but it's actually really good being able to ring up anyone at any time to talk to them or arrange to meet. I find relative to how many things you can do with it, it's very easy to use, and it really shows up how crap the average computer interface is by comparison (even KDE). I think a lot of the reason is that it doesn't have resizable windows that you have to keep fussing and mussing with, and it has some dedicated keys for accessing the menus.
</p>
<p>
I wouldn't have bothered until British Telecom disconnected my phone in the UK because my useless bank, Nat West cancelled the standing order. They cancel a random set of direct debits when you accidentally exceed your overdraft limit, aiming to teach you a lesson or something. When I tried to ring them up and sort it out from abroad they told me you couldn't set up telephone banking by telephone, and they would have to mail me the paperwork, and it would take 10 working days, although they couldn't mail to outside the UK anyway.. So I just lost control of my finances, and nearly got taken to court to have my house repossessed. I think you can use the internet to access your Nat West account details, but they only mail the password to my UK address. So anyway, a big thankyou to Nat West and BT for making me ditch land lines, I can't see myself ever bothering with one again. I'd love to abandon 20th century style banking Nat West style ASAP, and never want to be sent bits of paper through the post ever again..
</p>
<p>
The only slight disappointment is that I assume the Greenphone is only available in green - I'd love a white one that matched my new MacBook. Green looks a bit military to me.
</p>
