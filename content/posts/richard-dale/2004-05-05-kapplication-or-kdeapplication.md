---
title:   "KApplication or KDE::Application?"
date:    2004-05-05
authors:
  - richard dale
slug:    kapplication-or-kdeapplication
---
There's been some discussion on the kde-core-devel list this week about whether or not the KDE classes should be renamed for KDE 4.0. Should the class KMainWindow become MainWindow inside a KDE:: namespace for instance? Some people preferred it, and others thought it was a bad idea.

For the perl and ruby bindings we've already gone ahead and renamed the K* classes, and the Qt classes such as QWidget are similarly renamed as Qt::Widget. Here is an example top level using the fully qualified names:

<pre>
    about = KDE::AboutData.new("knotifywidgetbase", "KNotifyWidgetBase", "0.1")
    KDE::CmdLineArgs.init(ARGV, about)
    a = KDE::Application.new
    w = KNotifyWidgetBase.new
    a.setMainWidget(w)
    w.show
    a.exec
</pre>

Or does this look better?

<pre>
    include KDE
...
    about = AboutData.new("knotifywidgetbase", "KNotifyWidgetBase", "0.1")
    CmdLineArgs.init(ARGV, about)
    a = Application.new
    w = KNotifyWidgetBase.new
    a.setMainWidget(w)
    w.show
    a.exec
</pre>

If you use both 'include Qt' and 'include KDE' you need to fully qualify Application, to disambiguate between QApplication and KApplication. I hadn't actually tried using 'include' with ruby until this morning,  but I think it does make the code look clearer and prettier once you've got used to it.