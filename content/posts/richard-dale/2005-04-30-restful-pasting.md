---
title:   "RESTful pasting"
date:    2005-04-30
authors:
  - richard dale
slug:    restful-pasting
---
I found this great little ruby program on <a href="http://people.warp.es/~isaac/blog/index.php/my-first-ruby-script-17">isaac's random rants</a> blog - it takes the contents of the klipper clipboard, sends it to rafb.net which is 'code snippets temporary storage' site. You put your clipping there, and it returns you the URL back on the clipboard that you can paste into an IRC channel or whatever. I added a 'lang=Ruby' attribute too so the snippet gets labelled as a Ruby one.
<!--break-->
<pre>require 'Korundum'
require 'net/http'

about = KDE::AboutData.new("paster", "RAFB paster", "0.1")
KDE::CmdLineArgs.init(ARGV, about)
a = KDE::Application.new()
klipper = KDE&#58;&#58;DCOPRef.new("klipper", "klipper")
text = klipper.getClipboardContents
text.gsub!(/&#91;^A-Za-z0-9&#93;/) { sprintf("%%%02X", $&&#91;0&#93; )}

Net::HTTP.start('rafb.net', 80) { |http|
    response = http.post('/paste/paste.php',
        "nick=#{ENV&#91;'LOGNAME'&#93;}&lang=Ruby&desc=&cvt_tabs=no&text=#{text}")
    klipper.clipboardContents = "http://rafb.net#{response&#91;'Location'&#93;}"
}
</pre>

I think it shows the power of ruby's dynamism to communicate so simply over both DCOP and the net with such little code. Maybe that will prove a korundum killer feature - acting as a kind of glue between networked services; like DCOP, REST, SOAP and so on.