---
title:   "Qyoto \"hello world\" working"
date:    2005-12-13
authors:
  - richard dale
slug:    qyoto-hello-world-working
---
<p>
Below is a Qyoto/Kimono C# bindings 'Hello World' program written by Arno Rehn. Arno has done some quick performance measurements and he says Qyoto runs faster than a QtJava app running using IKVM, which is encouraging. I wasn't sure whether or not the method calls going via transparent proxies would be too slow, but there are major advantages in not needing C bindings using P/Invoke for each method call.
</p>
<!--break-->
<code>using System;
using Qt;

class MainForm : QDialog
{
    static void Main(String[] args) {
        QApplication qa = new QApplication(args);
        MainForm mf = new MainForm();
        mf.Show();
        qa.SetMainWidget(mf);
        qa.Exec();
    }

    public MainForm() : base()
    {
        this.Show();
        QVBoxLayout qgrid = new QVBoxLayout(this);
        qgrid.SetAutoAdd(true);
        QTextEdit te = new QTextEdit(this);
        te.Show();
        QPushButton button = new QPushButton("Hello World! Are you getting warmer?", this);
        button.Show();
    }
}
</code>
<p>
The next step is to design how slots and signals should work. This is what we're currently trying out; each slot is marked with a 'Q_SLOT' custom attribute, and the attribute data can be accessed via reflection and used to build a suitable QMetaObject on the fly for each QObject based class.
</p>
<code>[Q_SLOT("void mySlot(QString)")]
public void mySlot(string arg)
{
    ...
}
</code>
<p>
Signals are a bit trickier, as it would be nice to have them looking like delegates that you call. I'm not sure yet whether this can be done, so at first I think signals will be defined and used in a more 'Qt-like' manner. Here is an example of how the signals emitted by QApplication might look:
</p>
<code>public interface IQObjectSignals {
    [Q_SIGNAL("void Destroyed()")]
    void Destroyed();
    ...
}

public interface IQApplicationSignals : IQObjectSignals {
    [Q_SIGNAL("void LastWindowClosed()")]
    void LastWindowClosed();
    ...
}

((IQApplicationSignals) myInstance.Emit()).LastWindowClosed();
</code>
<p>
QObject.Emit() will return a transparent proxy constructed with the IQApplicationSignals interface above. The signal 'LastWindowClosed()' is emitted by invoking the method on the proxy. The method call can then be forwarded to an instance of 'SignalInvocation.Invoke()' where the C# arguments are marshalled to C++ ones and qt_emit() for Qt3 or QMetaObject::activate() for Qt4 will emit the signal via the Qt runtime.
</p>
<p>
The next important milestone will be a KDE hello world with slots/signals and virtual method overrides all working - that should take another week or two - maybe sometime in the new year I hope..
</p>