---
title:   "Qt 4 QtRuby hello world working"
date:    2005-06-03
authors:
  - richard dale
slug:    qt-4-qtruby-hello-world-working
---
I've just got hello world working with QtRuby and Qt 4:

<code>require 'Qt'

a = Qt::Application.new(ARGV)
hello = Qt:: PushButton.new('Hello World!', nil)
hello.resize(100, 30)
#a.mainWidget = hello
hello.show()
a.exec()
</code>
<!--break-->
The setMainWidget call is commented out because that method doesn't exist anymore. I still need to get signals/slots and on the fly metaObject creation working, but pretty much everything else is there. So bring on qt_metacall()! I'm hoping its array of void * method args with be a good match for the pretty similar void * array of args in the Smoke runtime.
<br>
<br>
I spent about a day getting working the code generation of the Smoke library against the Qt4 headers, and it now has a grand total of 352 classes which seems a good start. I haven't included any of the Q3* compatibility classes or methods - I'm not sure if they're a good idea for ruby.
<br>
<br>
It took another short day to get the QtRuby runtime to compile - it wasn't too bad. I ran the sources through qt3to4 first, then just hacked away fixing anything which didn't compile, and commented out anything to do with signals/slots. Qt4 does actually seem that bit tidier that Qt3, which is a good start. 
<br>
<br>
This Qt code:
<code>QAsciiDict<Smoke::Index> methcache(2179);
...
Smoke::Index *r = classcache.find(className);
...
QObjectListIt it( *l ); // iterate over the children
QObject *child;

while ( (child = it.current()) != 0 ) {
    ++it;
    obj = getPointerObject(child);
    if (obj != Qnil) {
        rb_gc_mark(obj);
    }
		
    mark_qobject_children(child);
}
</code>
Becomes:
<code>QHash<QString, Smoke::Index *> methcache;
...
Smoke::Index *r = classcache.value(className);
...
QObjectList::const_iterator it; // iterate over the children
QObject *child;

for (it = l.constBegin(); it != l.constEnd(); ++it) {
    child = *it;
    obj = getPointerObject(child);
    if (obj != Qnil) {
        rb_gc_mark(obj);
    }
		
    mark_qobject_children(child);
}
</code>
Looking through the diffs with Kompare, thats about it - just a lot of pretty minor changes. I need to find out how much work
is involved in getting rid of the Q3* in the code, but the porting guide in the docs seems pretty detailed about how to do that. In fact getting it working so easily seems a bit of an anti-climax, maybe I was hoping for a bit more of a struggle ;)
