---
title:   "Adobe Flash on Linux is crap, will it damage the brand?"
date:    2008-03-08
authors:
  - richard dale
slug:    adobe-flash-linux-crap-will-it-damage-brand
---
<p>I recently upgraded from Kubuntu Feisty to Gutsy, and all went well apart from one thing. Konqueror began putting up a crash dialog everytime it accessed a site with Flash, making it pretty much unusable. In fact until I had this problem I didn't realise quite how many pages on the web use Flash.</p>

<p>So I searched for more info about the problem and found Lubos Lunak's blog <a href="http://blogs.kde.org/node/3162>Why Flash sucks</a>, which described what went wrong. He says <i>"The latest Flash update does not work with anything that is not Gecko-based."</i> Well that's that then; they really don't seem to care about Konqueror support. Then I went and had a look at the <a href="http://blogs.adobe.com/penguin.swf">blog about Linux Flash</a> on the Adobe site. It actually reads more like a series of press releases than a blog, because the author doesn't appear interact with the readers of the blog and their comments</p>

<p>See the comments on the blog about the <a href="http://blogs.adobe.com/penguin.swf/2007/12/flash_player_9_update_3_final.html">latest release of Flash for Linux</a>, there are 107 comments and about 95% of them are pretty hostile and pissed off. It reminded me of all the comments on the Internet Explorer 8 blog - some poor developer was having to put up with piles of totally exasperated developers who were sick of wasting their time dealing with problems in the utterly inadequate IE6/IE7 browsers. Flash is being developed very slowly, it has major bugs (greatly increased CPU usage and crashes for instance), it doesn't run on all platforms. A bit like IE really.</p>

<p>I read about a work round where you could install an older version of the Flash plugin for Konqueror to use, while Firefox picked up the newer buggy one. That didn't seem a good idea, and would only prolong the agony. So I found out about the gnash plugin for Konqueror, did an apt-get install, and bingo! no crashes anymore. Fixfox still works with the Adobe version of Flash and I can use that for when Konqueror doesn't work.</p>

<p>Until this experience I had a very high regard for Adobe, technologies like PDF and Postscript are excellent and also well documented to allow for alternative implementations. In contrast Flash doesn't work well, and doesn't have an open specification, and not only that, but they are about the add DRM, digital restrictions management to it. I have no problem with Adobe selling proprietary technology as long as they don't attempt to privatise public infrastructure like the web. I think it is very important to avoid AIR as it is based on the same dodgy proprietary foundations as Flash. Something like Qt with Javascript bindings (or QtRuby even) combined with WebKit can probably do everything you can do in AIR in the way of writing stand alone 'webby' apps. And Plasma also allows you to integrate the desktop better via widgets, which Air doesn't do.</p>

<p>When Sony added a root kit to some of their CDs and then didn't apologise afterwards, that pretty much 100% destroyed the brand as far as I was concerned - they totally lost my trust. I don't think the Adobe Flash is as bad, but it probably has knocked 20-30% off the brand value from my point of view, and reinforced my idea that no public infrastructure should depend on non-Free software as it just isn't suitable for that</p>
