---
title:   "Miguel de Icaza on Open Source Java"
date:    2006-05-20
authors:
  - richard dale
slug:    miguel-de-icaza-open-source-java
---
<p>
I thought Miguel de Icaza had some <a href="http://tirania.org/blog/archive/2006/May-19-2.html">interesting comments</a> to make on the 'should Sun Open Source java issue'. I spent a couple of years working on the Qt/KDE java bindings but never achieved much success in spite of a lot of effort on my part, and I don't think the failure was entirely due to the technical quality of the QtJava bindings.
</p>
<!--break-->
Of course they could have been better, and I would like to have based them on Ashley Winter's Smoke technology like QtRuby/Korundum or the Qyoto C# bindings which are, in my opinion, way ahead of any other language bindings project. It seems to me that the Java community just doesn't understand Free Software, and there is a culture gap.
</p>
<p>
I do hope Trolltech can pull off making successful Qt Java bindings, although the only news of it was on Aaron Seigo's blog a few months ago, and nothing since which makes me wonder if it is going ok.
</p>
<p>
I think Sun's view of Java is so tied up with their fear of forking, that they will never be able to release Java with a satisfactory Free Software license. So I think it is better to bite the bullet and concentrate our efforts on more free alternatives like Mono, because gcj won't shake off Sun's influence on Java culture with terms like 'Pure Java' meaning anything which Sun hasn't invented (such as IBM's SWT) is crap.
</p>