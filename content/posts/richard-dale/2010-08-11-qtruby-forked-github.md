---
title:   "QtRuby forked on github"
date:    2010-08-11
authors:
  - richard dale
slug:    qtruby-forked-github
---
<p>Ryan Melton announced on the kde-bindings mailing list that he had set up a project on github called 'qtbindings' with the aim of doing cross-platform gems for QtRuby. This is great news, and congratulations to Ryan for making it happen</p>

<p>Ryan announced:</p>

<i>"..I put together a new cross-platform gem for the Ruby bindings to Qt and put it out on rubygems.org as "qtbindings".  This should make it a lot easier for people to install the ruby bindings on non-KDE systems. I've tested it out on Windows XP, Ubuntu Linux, and Mac OSX Snow Leopard so far and it seems to work great.  It is mainly the code straight from KDE bindings, but it also includes the patches for building on Windows I submitted to this list back in February, the fix for ExtraSelection, and some new fixes I've put in for the HWND__* classes on Windows.   I will be submitting the new patches back to this list soon.  Some other changes include minor modifications to the CMakelists.txt files, and reorganizing the folders into a standard ruby gem structure that I doubt you would want to incorporate back into the kdebindings repository.  Enjoy and I hope this increases the adoption of the great bindings you guys put together!  :).."</i>

<p>So although it is a fork, it isn't a hostile fork and so I think the benefits of making QtRuby easy to install on Windows and Mac OS X outweigh the disadvantages.</p>

<p>Over on the <a href="http://rubyforge.org/frs/?group_id=181&release_id=44227">RubyForge site</a> there is a new version of QtRuby, qt4-qtruby-2.1.0, which I didn't produce either (Jan Pilz? not sure, need to find out), and that is great news too. A new release there was long overdue.</p>

<p>Meanwhile the Maemo/MeeGo widget testing guys are really keen to have a version of QtRuby that wrapped the MeeGo Touch (ie libdui) libs, and Qt Mobility. They use Gitorious and so a version of QtRuby that you could be with qmake instead of cmake would be really good, and ideally hosted on Gitorious so they would be able to make enhancements and fixes using their Gitorious accounts.</p>

<p>I worked on a major refactoring of QtRuby which is long overdue. But that work has stalled because I have been waiting for KDE to migrate to git. I was expecting it to have happened a month or two ago as we were negotiating with the Gitorious guys for a long time, and it seemed as though we were going to get the migration done about June 2010 time when I was thinking about it last Christmas. That wasn't to be as the Gitorious negotiations fell through, and instead on his recent blog Tom Albers reports that the schedule for git conversion won't start converting the main kde modules like kdebindings until November 17th:</p>

<p><b>November 17th: </b><i>".. 
After the initial flow of individual projects that have moved to git, we are open to assist in moving the KDE modules. How this part will happen is unknown and will probably not happen directly at this point in time.."</i></p>

<p>I have other projects like the JSmoke JavaScript bindings on Gitorious, or the Wt::Ruby bindings on github that I would like to be able to coordinate with QtRuby and the smokegen bindings generation tool. All the other projects are in git, and it is becoming increasingly difficult to know how to cope if I just leave QtRuby in the KDE svn where it has been since the project started seven years ago. But clearly if I don't do something, things will just get into even more of an unmanageable mess than they are at the moment.</p>

<p>As far as I know it will be possible to move to the KDE git repo in advance of sometime next year, otherwise that would be after the KDE 4.6 release, and I'm just not keen on waiting that long. If the move isn't a lot sooner than that, I feel I will just have to give up, and probably treat the 'qtbindings' github repo as the main center of development for QtRuby.</p>

<p>On an unrelated note my employer, Codethink, are looking to qt/qml guys, Alberto Ruiz sent me this:</p>

<p><i>Codethink Ltd. is looking at the possibility of hiring a 
few people with Qt/QML experience, people that have contributed stuff to 
Qt upstream is a plus. Please send your CVs to jobs@codethink.co.uk. 
Codethink is an exciting place to work at, and they allow people to work 
from home which is great, the head offices are in Manchester so if you 
don't like working from home you can always join us from our offices.<i><p>

<p>I can confirm that Codethink is an interesting place to work, and their tee-shirts aren't bad either. So get in contact if it appeals.</p>

<p>I was actually going to investigate QML and language bindings and do a blog about that to make it more relevant to what Alberto is asking for. But I found it a really hard problem and I haven't decided what the best thing to do is yet. Should we try and do a Ruby version of QML and compile it to the same form as if it had been JavaScript, or do we keep all the JavaScript stuff and attempt to mix it up with Ruby code? I don't know yet.</p>

