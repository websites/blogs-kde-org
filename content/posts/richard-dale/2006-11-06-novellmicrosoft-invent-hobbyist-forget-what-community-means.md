---
title:   "Novell/Microsoft invent the 'Hobbyist', forget what 'Community' means"
date:    2006-11-06
authors:
  - richard dale
slug:    novellmicrosoft-invent-hobbyist-forget-what-community-means
---
<p>
The recent Novell/Microsoft agreement purports to give what they call 'Non-Compensated Individual Hobbyist Developers' the rights to use unspecified Microsoft patents. The terms are given in this <a href="http://www.microsoft.com/interop/msnovellcollab/community.mspx">Community Commitments - Microsoft & Novell Interoperability Collaboration</a>. They define a 'hobbyist' as this:
</p>
<p>
<i>Many software developers, often referred to as “hobbyists,