---
title:   "Kubuntu on the HP 2133 Mini-Note"
date:    2009-01-12
authors:
  - richard dale
slug:    kubuntu-hp-2133-mini-note
---
<p>It seems at the moment everyone is looking out for netbooks, Richard Johnson <a href="http://blog.nixternal.com/2009.01.11/penguicon-netbooks-oh-my/>writes about</a> them on his blog today, and I was interested to read the comments. While I was back in the UK for the Christmas break I spent a day in London shopping, and wondered up and down Tottenham Court Rd looking for a netbook to buy. I ended up with an HP 2133 Mini-Note with SuSE 10 preinstalled on it.</p>

<p>At this years Akademy I tried an Asus and found the keyboard too small for me, but an Acer One seemed fine, and I liked the look of that machine. So the Acer was the one to beat when I started my hunt. I looked at Lemovo (keyboard non-standard and too small), Toshiba (keyboard too small), Samsung (too plasticky), MSI Wind (plasticky not sure if Linux version available). They are all about the same price 250-300 UKP/Euros.

<p>But I came across an HP 2133 with SuSE 10 preinstalled, going for 300 UKP in a sale, and found it had a keyboard that was in a different league to the others, and a screen resolution of 1280X768 which again was way better than the competition. Along with 1Gb of memory and a 120Gb hard disk for just 300 Euros - wow! The downside was that it had a slow Via C7 processor instead of the Atom that all the other models had, which might mean the battery life was a bit poor too. So I couldn't make up my mind whether to get an Acer as planned, or take a risk and get the amazing looking HP for much the same money. After a beer or two in a nearby pub and much thinking, I decided that a great keyboard, screen and build quality trumped the better Atom processor in the Acer and went for the HP.</p>

<p>When I got back to my sister's house I was really looking forward to booting up the machine and finding SuSE on it which 'just worked'. Unfortunately, all I got was a blinking cursor and a 'Grub Error 17' message. What a bummer! When I did a search for the problem it turned out it had happened to a lot of other people, which made me a bit concerned about HP's quality control. As the machine doesn't have a CD ROM drive it is actually a bit awkward to install Linux as you need to make a USB stick first, and I didn't really have the tools to do that in the UK, and I would have to wait until I got back to Gran Canaria to try it out.</p>

<Back home I was able to try the Mandriva usb stick that we got at the Glasgow Akademy and that worked fine, apart from only being 800X400 resolution. At least I knew I didn't have some kind of hardware problem, and it should be just a matter of creating a USB stick with a more modern Linux.</p>

<p>As I run Kubuntu on my MacBook I thought I might as well wipe the non-working SuSE and put Kubuntu on instead. I went out and bought a 4Gb usb stick for 10 euros (wow how cheap they are now!), and tried to run a Ubuntu program called 'usb-creator' to put a live CD on the usb stick. Unfortunately usb-creator just hung at startup, and it took a bit of googling around to find out why. It turns out that it isn't good enough to have your usb stick as VFAT 32, but it has to be the right sort of VFAT 32 as created by GParted. So I installed that onto the Macbook booted from the live Kubuntu CD and successfully made my stick. However, on attempting to boot the HP with it I got another Grub error and a flashing cursor. It took me quite a while to find out that I could create a USB stick that worked with my MacBook booted normally, but not when it was booted from the live CD. I've no idea why.</p>

<p>After that difficult start it was pretty much plain sailing and installing Kubuntu was really easy, and everything seems to work perfectly. The only fiddly thing was that you need to run the installer in safe mode, and then change the 'xorg.conf' file once you've installed. After all this effort I was finally able to see what the HP looks like in its full resolution, and the answer is that it is totally stunning. Although the screen is quite small, it is so crisp and bright that I could use KDE4 just like it was running on my MacBook which is 1280X800. The difference in resolution between the two machines amounts to about 3 lines of text in Kate - ie very little. I haven't got 3D acceleration working to be able to use the KWin effects, but KDE4 looks really very nice indeed.</p>

<p>To see what sort of speed the CPU runs at I did the same compile on both the MacBook (2.5 year old 1.8 GHz Core Duo), and the HP with 1.2 GHz Via. It took about 3 times as look to compile the Wt toolkit, 30 mins as opposed to under 10 mins on the Mac. While the compile was running though I could scroll through windows and surf the web just fine.</p>

<pre>
Mac: real 9m 41s, user 8m 45s, sys 35s
HP: real 32m 20s, user 29m 31s, sys 1m 32s
</pre>

<p>That is probably just comparing a single Core Duo processor with the Via, but actually I don't think it's too bad. Nobody is going to build all of kde everyday and then do heavy Valgrind sessions on it, and for my purposes the HP is just fine.</p>

<p>Some other numbers. The HP has 96% of the pixels of my MacBook, while the Acer only has 60%. The HP weighs 1.266 Kg, which is 55% of the Mac at 2.320 Kg. The Mac screen is about 115 dpi, while the HP is about 167.5 according to my measurements. The Nokia is about 225 dpi, and I find that the smallest font I can see on that is 12 point, whereas on the HP I seemed to be able to use exactly the same font sizes as on the Mac with absolutely no problem reading the screen.</p>

<p>I find the trackpad on my Mac pretty much unusable and don't bother with it. The HP trackpad has buttons on either side of the trackpad which a lot of people don't like, but I still prefer to the Mac with only one button underneath its trackpad. The trackpad has a dedicated scroll line on the right hand side, which is really useful when using a browser. The HP keyboard has a larger enter key, a better positioned control key and the left, and a second control key on the right which is missing on the Mac. Overall I think the HP keyboard might have the advantage which is quite amazing for such a small machine.</p>

<p>I haven't really done any battery life tests yet, but it is supposed to give about 1.5 to 2 hours with the three cell battery. If that proves a problem I can go for the six cell battery and the expense of having a pretty heavy netbook</p>

<p>In mid-March HP are coming out with the HP 2140 Mini-Note with an Atom processor and screen resolution of 1330X768m, and everything else pretty much identical to the 2133. That should be a stunning machine, although I bet it will end up costing a fair bit more than the 300 Euros I paid. Still it will still be only about a third the cost of a MacBook Air, which is about the only small machine I know of with better finish and industrial design. Most of the other netbooks now look like something out of a Christmas cracker compared with the sleek metal and black HP..</p> 
 