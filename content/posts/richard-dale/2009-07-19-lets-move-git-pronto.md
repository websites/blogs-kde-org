---
title:   "Lets move to git pronto!"
date:    2009-07-19
authors:
  - richard dale
slug:    lets-move-git-pronto
---
<p>The release team made a decision to branch the KDE svn for 4.3 in advance of the actual release. From the point of view of kdebindings I am finding it a highly error prone messy pain in the arse.</p>

<p>I don't blame the release team, but it really shows how unsuitable subversion has become for a project on the scale of KDE. Various parts of KDE development have different relationships with the release cycle. With kdebindings we move into high gear rather late, when everyone else has frozen their apis and headers for the release. So with the early branching it means that I am unavoidably developing some things in trunk and other things in the 4.3 branch. And being human it is very hard not to screw up, and not accidentally copy the wrong stuff from the branch to the trunk and vice versa. Well it's Sunday, and I now have a 'subversion headache' and am retiring for the day..</p>