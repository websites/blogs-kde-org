---
title:   "Qyoto and Kimono C# bindings"
date:    2005-10-26
authors:
  - richard dale
slug:    qyoto-and-kimono-c-bindings
---
<p>
I started working on some C# bindings a couple of years ago called Kimono, and got it to the 'proof of concept' stage. It uses Transparent Proxies to funnel every call to the Qt api to a single SmokeInvocation.Invoke() method. Inside Invoke() it looks up the method in the Smoke library's runtime, marshalls the arguments and calls it. I've just adapted the code generation to work with the Qt4 classes, and checked the code into trunk/playground/bindings/kimono.
</p>
<!--break-->
<p>
It works be having one proxy per instance to handle normal methods, and another proxy per class to handle static method calls. Here's how they are created by these methods called from the instance's constructor:
</p>
<code>    protected void CreateQApplicationProxy() {
        SmokeInvocation realProxy = new SmokeInvocation(typeof(QApplication), this);
        _interceptor = (QApplication) realProxy.GetTransparentProxy();
    }
    private QApplication ProxyQApplication() {
        return (QApplication) _interceptor;
    }
    private static Object _staticInterceptor = null;
    static QApplication() {
        SmokeInvocation realProxy = new SmokeInvocation(typeof(IQApplicationProxy), null);
        _staticInterceptor = (IQApplicationProxy) realProxy.GetTransparentProxy();
    }
    private static IQApplicationProxy StaticQApplication() {
        return (IQApplicationProxy) _staticInterceptor;
    }
</code>
<p>
The per class proxy uses 'typeof(QApplication)' above as an interface, while the static proxy uses an internal interface like this:
<code>    interface IQWidgetProxy {
        string Tr(string s, string c);
        string Tr(string s);
        void SetTabOrder(QWidget arg1, QWidget arg2);
        QWidget MouseGrabber();
        QWidget KeyboardGrabber();
        QWidget Find(ulong arg1);
}
</code>
<p>
You can then make calls on the proxies like this:
</p>
<code>    public static ArrayList AllWidgets() {
        return StaticQApplication().AllWidgets();
}
</code>
<p>
There are about 380 classes in playground/bindings/kimono/qyoto so it's easy to see what it will look like. So if anyone is interested please review it, and see what could be improved. The build system could be more C#-like, at the moment there's just a hand hacked makefile in there. I assume it should use csant or something. The api is quite easy to change as it's auto-generated. The Smoke library already exists for Qt4, so all the boring code generation work for the project has already been done. It just needs the fun systems programming stuff to integrate with the Smoke library via P/Invoke. 
</p>
<p>
I don't think it will be the fastest binding because every call is going via a proxy, but it means that you can easily intercept calls as point cuts to do AOP style things. Calling code before, after or instead of an existing method.
</p>
Maybe there will be a first version ready when Roberto (I assume it's him working on the java bindings) has a first release of his bindings, early next year. But as I'm starting a proper job soon I'm not sure how much time I'll have for KDE hacking - I really should be concentrating on learning Spanish in my spare time..
</p>