---
title:   "KDE to Gnome - we exist!"
date:    2004-10-15
authors:
  - richard dale
slug:    kde-gnome-we-exist
---
<p>
I've no problem with multiple toolkits on Linux, but I really don't think there is any point in innovating on File Dialogs, or Button Orders. I don't care about whether the Gnome dialogs are better than the KDE ones. That stuff was done 20+ years ago, and anyone who thinks that designing a better File Dialog in 2004 is 'innovative' has lost the plot. So what gets up my nose somewhat is when a Gnome blogger just completely fails to acknowledge that KDE exists. 
</p>
<!--break-->
<p>
Luis Villa says:
</p>
<p>
<i> I installed evo and galeon back in 2001, with apps in gtk, motif, and even raw X. Frankly, when I install stock firefox and stock open office, that's what I feel like I have again- the dialogs don't match, the icons don't match, the behavior doesn't match.</i>
</p>
<p>
He then goes on to <a href="http://tieguy.org/blog/index.cgi/210"> discuss</a> various things:
</p>
<i>Almost definitely we've made it technically too hard to follow them.</i>
<p>
Umm.. In what way? Lets just co-operate via freedesktop.org and ensure that a Gnome app looks just the same as a KDE app. Cut this crap about how Gnome has 'innovated in the GUI field' because it hasn't. And neither has KDE. If you want to investigate something that really is the future check out <a href="http://www.opencroquet.org/"> Croquet</a>. There's just no comparison, we need to consolidate those 20 year old ideas right now, so we can move onto the next thing. Pervasive use of 3D graphics with OpenGl, peer to peer networking all the time, dynamic scripting languages etc and so on..
</p>

