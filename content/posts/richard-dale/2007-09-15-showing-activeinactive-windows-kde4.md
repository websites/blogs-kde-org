---
title:   "Showing active/inactive windows in KDE4"
date:    2007-09-15
authors:
  - richard dale
slug:    showing-activeinactive-windows-kde4
---
<p>There has been some discussion on the kde-core-devel mailing list about a change to how the active window should be distinguished from the inactive windows, where different color palettes are used for the widgets inside inactive windows.</p> 

<p>I tried out the change yesterday, testing flipping between Konsole and Dolphin, and don't personally like it much. Here are a couple of screen shots of what I saw so you can judge for yourself (with a KDE3 kicker in there):</p>

[image:2992]
<p>Konsole at front with Dolphin</p>

[image:2993]
<p>Konsole with Dolphin at front</p>
