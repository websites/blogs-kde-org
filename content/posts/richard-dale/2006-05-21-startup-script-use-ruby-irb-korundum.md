---
title:   "A startup script to use Ruby irb with Korundum"
date:    2006-05-21
authors:
  - richard dale
slug:    startup-script-use-ruby-irb-korundum
---
<p>I've been playing with irb today to work out how to add an interactive Korundum or QtRuby command line. I've come up with this script that allows you to type 'start_kde' in irb, and it displays a KDE::MainWindow. If you right click and select 'Interrupt' from the context menu, it puts you in an irb session context based on a widget within the KDE::MainWindow.
</p>
<!--break-->
<p>
Here is the script, you just need to add it to your ~/.irbrc file:
</p>
<code>
def start_kde
    require 'Korundum'
    KDE::CmdLineArgs.init(ARGV, KDE::AboutData.new("irb", "KDE Main Window", "0.1"))
    KDE::Application.new
    kmainwindow = KDE::MainWindow.new(nil, "IRB Window")

    class << kmainwindow
        def queryClose
            $kapp.quit
        end

        def mousePressEvent(event)
            if event.button() == Qt::RightButton
                if @popupmenu.nil?
                    @popupmenu = KDE::PopupMenu.new
                    self.class.slots :interrupt
                    @popupmenu.insertItem("&Interrupt", self, SLOT(:interrupt))
                end

                @popupmenu.exec(Qt::Cursor.pos)
            else
                super
            end
        end

        def interrupt
            eval("irb $widget", $irb_binding)
        end
    end

    $widget = Qt::Widget.new(kmainwindow) do |w|
        t = Qt::HBoxLayout.new(w)
        t.autoAdd = true
    end

    kmainwindow.centralWidget = $widget
    kmainwindow.show
    $irb_binding = binding
    $kapp.exec
end
</code>
<p>
Enter irb, type 'start_kde' and select 'Interrupt' on the right mouse menu:
</p>
<code>
baldhead duke 506% irb
irb(main):001:0> start_kde
irb#1(#<Qt::Widget:0x302a5af0>):001:0> @factory = KDE::LibLoader.self().factory("libkatepart")
=> #<KParts::Factory:0x30284fd0 name="unnamed">
irb#1(#<Qt::Widget:0x302a5af0>):002:0> @kate = @factory.create(self)
=> #<Kate::Document:0x30281988 name="unnamed">
irb#1(#<Qt::Widget:0x302a5af0>):003:0> @kate.openURL(KDE::URL.new("file:///home/duke/.irbrc"))
=> true
irb#1(#<Qt::Widget:0x302a5af0>):004:0> quit
</code>
<p>
You will see the Ruby text of your ~/.irbrc file displayed by the Kate part. Note that the class is 'Kate::Document' which is not in the Smoke library a (it is in kdebase). The Ruby classes for dynamically loaded KParts are now created at runtime. You can also set properties on dynamically loaded KParts with a simple call like '@kate.my_property = my_value' or '@kate_my_property' to set and get them. Properties can be displayed with a @kate.metaObject.propertyNames call:
</p>
<code>
irb#1(#<Qt::Widget:0x302a5af0>):001:0> @kate.metaObject.propertyNames(true)
=> ["name"]
</code>

