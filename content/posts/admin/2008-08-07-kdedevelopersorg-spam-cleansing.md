---
title:   "kdedevelopers.org spam cleansing"
date:    2008-08-07
authors:
  - admin
slug:    kdedevelopersorg-spam-cleansing
---
<p>Upon user reports we've found a new way spammers abuse our web site, so I've upgraded the drupal to the latest version to be able to install some more anti-spam features. We're currently slowly digging through the damage and delete stuff as necessary. </p>
<p><b>Note:</b>Even though we upgraded to the latest available Drupal version, the "can't login with account names that have any upper case letter in it" bug is still existing. Please make sure that you use lower case only.</p>
<p>In case you notice any regression (other than that you're required to use Captchas in some places) please report it as a comment to this post.</p>
