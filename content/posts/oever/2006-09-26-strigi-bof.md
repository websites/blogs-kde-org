---
title:   "Strigi BoF"
date:    2006-09-26
authors:
  - oever
slug:    strigi-bof
---
The Strigi BoF went well and I'm pleased with the feedback. I'm glad most of the issues requested can be handle by Strigi or will be handled by the application coming from the Nepomuk project.

Now there is time for hacking. The first feature I'll add is possibility to index files via the DBus interface. The second feature will be to add a configuration file framework.

Aaron Seigo did a great suggestion of having the KMetaFileInfo framework use Strigi to retrieve the metadata. This would require porting of the metadata extractors to Strigi, pushing Strigi forward tremendously and having the added boon of easily having indexes for selected metadata.

Some security issues were pointed out. If a remote file, such an email attachement is indexed and if it exploits a hole in a library used for analysis, an attacker could get access to a machine. This means we have to think about limiting the rights of the Strigi daemon with for example AppArmor.

Using Strigi through the DBus daemon is very easy. Still, it would be great to have kdelibs deal with the Strigi access by adding a class to it that handles calls to Strigi. This will be a goal for this aKademy too.

Also, the IRC channel for Strigi is now #strigi.

<a href="http://www.vandenoever.info/software/strigi/akademy2006.odp">ODP presentation</a> (<a href="http://www.vandenoever.info/software/strigi/akademy2006.pdf">pdf</a>)

<!--break-->
