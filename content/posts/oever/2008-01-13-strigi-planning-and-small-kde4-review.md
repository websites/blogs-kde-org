---
title:   "strigi planning and small kde4 review"
date:    2008-01-13
authors:
  - oever
slug:    strigi-planning-and-small-kde4-review
---
Yesterday we had an IRC meeting to plan our activities on Strigi for the near future. It was good to have an IRC meeting again after having been practically offline for over two months.

We put a <a href="http://techbase.kde.org/index.php?title=Projects/Strigi/Strigi_status_meeting_2008">status document</a> online. Don't read too much into it, we were focussing on the work we can do next. We came up with a planning that I <a href="http://sourceforge.net/mailarchive/forum.php?thread_name=c2dbc4260801130240x55937133l35ed009c188803ac%40mail.gmail.com&forum_name=strigi-devel">posted</a> on our <a href="http://sourceforge.net/mailarchive/forum.php?forum_name=strigi-devel">mailing list</a>.

While browsing the other planets, I came across <a href="http://floatingsun.net/2008/01/12/test-driving-kde-40/">a nice little KDE 4.0.0 review</a> from a longtime KDE user.

And because I did not see Sebastian Trueg's blog post on this planet, I'm linking it here: <a href="http://soprano.sourceforge.net/node/17">Soprano 2.0</a> has been released!

<!--break-->