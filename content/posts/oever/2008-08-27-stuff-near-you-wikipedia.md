---
title:   "Stuff near you in wikipedia"
date:    2008-08-27
authors:
  - oever
slug:    stuff-near-you-wikipedia
---
A while back I <a href="http://blogs.kde.org/node/3576">blogged</a> about querying dbpedia with <a href="http://www.w3.org/TR/rdf-sparql-query/">sparql</a>. The queries in that blog were pretty simple. Today, I present a more complicated example.

<code>SELECT ?a, ?long, ?lat WHERE {
  <http://dbpedia.org/resource/Borne%2C_Overijssel> <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?centerlong ;
                                                    <http://www.w3.org/2003/01/geo/wgs84_pos#lat>  ?centerlat .
  ?a <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long ;
     <http://www.w3.org/2003/01/geo/wgs84_pos#lat>  ?lat .
  FILTER ( -(?long - ?centerlong)*(?long - ?centerlong) - (?lat - ?centerlat)*(?lat - ?centerlat) > -0.01 )
}</code>
<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Flong%2C+%3Flat+WHERE+%7B%0D%0A++%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FBorne%252C_Overijssel%3E+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Fcenterlong+%3B%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Fcenterlat+.%0D%0A++%3Fa+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Flong+%3B%0D%0A+++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Flat+.%0D%0A++FILTER+%28+-%28%3Flong+-+%3Fcenterlong%29*%28%3Flong+-+%3Fcenterlong%29+-+%28%3Flat+-+%3Fcenterlat%29*%28%3Flat+-+%3Fcenterlat%29+%3E+-0.01+%29%0D%0A%7D">This query</a> gives you all items in the english Wikipedia near where I live.

You could also look for <a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Flong%2C+%3Flat+WHERE+%7B%0D%0A++%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FGran_Canaria%3E+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Fcenterlong+%3B%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Fcenterlat+.%0D%0A++%3Fa+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Flong+%3B%0D%0A+++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Flat+.%0D%0A++FILTER+%28+-%28%3Flong+-+%3Fcenterlong%29*%28%3Flong+-+%3Fcenterlong%29+-+%28%3Flat+-+%3Fcenterlat%29*%28%3Flat+-+%3Fcenterlat%29+%3E+-0.01+%29%0D%0A%7D">stuff near Gran Canaria</a> or <a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Flong%2C+%3Flat+WHERE+%7B%0D%0A++%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FBerlin%3E+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Fcenterlong+%3B%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Fcenterlat+.%0D%0A++%3Fa+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23long%3E+%3Flong+%3B%0D%0A+++++%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23lat%3E++%3Flat+%3B%0D%0A+++++rdf%3Atype+%3Chttp%3A%2F%2Fdbpedia.org%2Fclass%2Fyago%2FLandmark108624891%3E+%0D%0A++FILTER+%28+-%28%3Flong+-+%3Fcenterlong%29*%28%3Flong+-+%3Fcenterlong%29+-+%28%3Flat+-+%3Fcenterlat%29*%28%3Flat+-+%3Fcenterlat%29+%3E+-0.01+%29%0D%0A%7D%0D%0AORDER+BY+%28-%28%3Flong+-+%3Fcenterlong%29*%28%3Flong+-+%3Fcenterlong%29+-+%28%3Flat+-+%3Fcenterlat%29*%28%3Flat+-+%3Fcenterlat%29%29%0D%0A++">Berlin</a>.
<!--break-->
