---
title:   "Hardy Heron Alert!"
date:    2008-04-24
authors:
  - oever
slug:    hardy-heron-alert
---
<img src="https://blogs.kde.org/files/images/heron.jpg" alt="So long and thanks for all the fish"/>

Near where I live there's a pond. Actually it is right outside of my living room. In this pond there used to be many a goldfish. Until this week.

Last Monday a sneaky thief landed in the middle of our lawn and proceeded to carefully tiptoe in the direction of the pond. This thief was a <a href="http://en.wikipedia.org/wiki/Great_blue_heron">great blue heron</a>. It took about six steps before it noticed that there were two people with big astonished eyes sitting in the house. With every step in our direction, the long black feathers on the back of its head flayed to and fro. But once it had seen us, the heron decided we might interfere with his fishing trip and flew away.

Yesterday, a big white circle of heron excrement showed us that he had been back during the day to metabolize some goldfish. Also, we saw him flying low over our house at about seven in the evening.

And today, the day where many a geek was sitting behind his computer screen in anticipation of a hardy heron, we were again surprised at breakfast by a sneaky heron (hardy is hardly the right word). This time he landed, looked around for five seconds, spotted us and flew off again.

We put a net over our pond, but I think we're out of fish already.



<!--break-->
