---
title:   "The Desktop Summit is so much fun"
date:    2009-07-06
authors:
  - oever
slug:    desktop-summit-so-much-fun
---
The Desktop Summit in Gran Canaria is very enjoyable. The conference center is very
luxurious. It comes with uniformed assistants in every presentation room helping by changing the name signs and refreshing the water. The main conference hall has a wonderful view on the ocean. There are people from KDE, GNOME and many other projects here, so there are many interesting people that I have not met before. The conference is located near the ocean, so attendees can go swimming for lunch. The talks are all recorded with the slides as insets, so if you are not here or cannot attend two talks at the same time, you can view the talks later. 

My presentation (<a href="http://ktown.kde.org/~vandenoever/akademy2009/strigi.odp">odf</a>) about libstreamanalyzer went well. The announcement that Tracker 0.7 will use libstreams and libstreamanalyzer was received with a round of applause. The audience was an equal mix if KDE and GNOME developers and the many questions after the talk indicated that people get what the libraries are about and are interested in using it.

Strigi 0.7 will come with a new set of ontologies as a default. Previously we used the ontologies from xesam.org. From 0.7 we will be using the ontologies from semanticdesktop.org. These ontologies are also used by Tracker. So we will have one set of ontologies for both KDE and GNOME. Expect Strigi 0.7 in the next weeks.

For me there are only two more days here. I will be busy with semantic desktop and koffice meetings before I fly home on Thursday morning.

<!--break-->
