---
title:   "OdfKit Hack Week starts"
date:    2010-06-22
authors:
  - oever
slug:    odfkit-hack-week-starts
---
<a href="http://www.odfkit.org">OdfKit</a> is a project that reuses <a href="http://webkit.org">WebKit</a> technology in a toolkit for working with <a href="http://en.wikipedia.org/wiki/OpenDocument">ODF</a> office documents. <a href="http://www.kogmbh.com/">KO GmbH</a> is sponsored by <a href="http://nlnet.nl/">NLnet</a> to work on OdfKit for three months. This week, Chani, who is on her way to Akademy, is working with me on OdfKit and since she's here an entire week, we're calling it OdfKit Hack Week.

The scope of OdfKit was formulated at Gran Canaria Desktop Summit 2009 and this month is the first time that actual code is going in. OdfKit was born as a reaction to a number of issues. On the one hand there is a need for a cross-platform library to easily add ODF support to command-line and desktop applications and on the other, we see that closed cloud-based office suites are emerging. Initiatives like <a href="http://owncloud.org/">ownCloud</a> emphasise the need for Free Software for web services. OdfKit should make it easier to write alternatives for Google Docs and Microsoft Office Live, but it should also help in adding ODF support to desktop applications.

Why reuse WebKit code?

<a href="http://webkit.org/">WebKit</a> is a success story. The codebase started as <a href="http://nl.wikipedia.org/wiki/KHTML">KHTML</a>, was adopted by Apple and later morphed into WebKit. Currently WebKit is the basis for Safari, Chrome, and many other browsers. On top of that it is a popular part of Qt. With WebKit, it is very easy to add a full-blown browser to a cross-platform application. Surely, a similar approach is possible for ODF software.

The WebKit code is becoming the most widely use browser code for mobile phones. Therefore, it has to be small and fast. Amongst others, Nokia, Apple, Google and Adobe are contributing to WebKit, making it a very competitive project.

WebKit abstracts the native toolkit, be it <a href="http://en.wikipedia.org/wiki/Qt">Qt</a>, <a href="http://en.wikipedia.org/wiki/Cocoa_%28API%29">Cocoa</a>, <a href="http://en.wikipedia.org/wiki/Enlightenment_Foundation_Libraries">EFL</a>, <a href="http://en.wikipedia.org/wiki/GTK%2B">Gtk</a>, <a href="http://en.wikipedia.org/wiki/WxWidgets">wxWidgets</a>, <a href="http://en.wikipedia.org/wiki/Skia_Graphics_Engine">Skia</a> or <a href="http://trac.webkit.org/wiki">one of the other ports</a>. This abstraction is done by the Web Template Framework (WTF). Code implemented using the WTF classes can be compiled for the different WebKit ports. The current projects that support ODF are OpenOffice, KOffice and AbiWord. Each of these is tied to a particular framework. This means that people that want to work on office software are divided by the framework they choose. WebKit does not have this problem: it runs natively in most programming frameworks and any improvement in the core benefits all of the ports.

Great, where can I get it?

At the moment, OdfKit is <a href="http://gitorious.org/webkit/odfkit">a branch of WebKit</a>. By patching WebKit instead of simply copying choice parts, we avoid a lot of overhead. Our branch currently adds IDL files that (will) allow loading, saving and manipulating ODF files from the javascript in a webpage. This week, we will look into how to add and export a C++ API for each of the ports. You can check out the code and compile it for any of the ports.

Is this competition for KOffice?

That is not the goal. OdfKit is not meant to be as comprehensive as KOffice. It should make it easy to write applications with simple but correct ODF support. Loading, saving should be flawless; modifying the document programatically should be easy and should yield only valid ODF. Visualization of the document and simple editing are nice extras.

So what's the program for this week?

We will decide on a program tomorrow and will keep track in this blog about the progress. We already made a pure javascript implementation that can read ODF files from any directory on a web page. (Support for saving to e.g. a WebDav server is a possible extension.) You can have <a href="https://demo.webodf.org/demo/">a preview of it here</a>. This page can visualize the contents of ODF files (WebKit browsers and Firefox only at the moment.).

If you want to join you can find us in #odfkit on freenode and you can join <a href="http://lists.opendocumentfellowship.com/mailman/listinfo/odfkit">the mailing list</a>.
<!--break-->
