---
title:   "deepfind and deepgrep"
date:    2006-10-21
authors:
  - oever
slug:    deepfind-and-deepgrep
---
Do you use <tt>find</tt>?  Of course you do, everybody uses it -- often. It's a nice and quick program for finding files on your disk. A downside to <tt>find</tt> is that it does not list files embedded in other files like .deb, .rpm, .tar.gz, email attachments, and other files. Now there is a version of <tt>find</tt> that does exactly this. It's called <tt>deepfind</tt>.

Do you use <tt>grep</tt>? Of course you do, everybody uses it -- often.  It's a nice and quick program for finding files on your disk that match a particular string. A downside to <tt>grep</tt> is that it does not search in binary files like OpenOffice files, mp3s, Microsoft office files, pdfs and also not in files embedded in other files like .deb, .rpm, .tar.gz, email attachments, pdf and other files. Now there is a version of <tt>grep</tt> that does exactly this. It's called <tt>deepgrep</tt>.

<tt>deepgrep</tt> and <tt>deepfind</tt> are command-line tools and have no GUI dependencies (so GNOME fans may also use it ;-). The programs both use JStreams and are part of <a href="http://www.vandenoever.info/software/strigi">Strigi</a>. Currently, these programs are located in <a href="http://websvn.kde.org/trunk/playground/base/strigi/src/dummyindexer/">SVN</a>. They will be in the next Strigi release. For background information on the workings of <tt>deepfind</tt> and <tt>deepgrep</tt> and how they relate to Strigi, Tracker and Beagle, check out <a href="http://www.vandenoever.info/software/strigi/akademy2006.pdf">my aKademy talk</a>.
<!--break-->