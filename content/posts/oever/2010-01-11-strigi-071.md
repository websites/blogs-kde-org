---
title:   "Strigi 0.7.1"
date:    2010-01-11
authors:
  - oever
slug:    strigi-071
---
This is just a quick note to tell the world about the newest Strigi release. It has version number 0.7.1 and is the recommended Strigi version for use with KDE 4.4 and Nepomuk.

Go <a href="http://www.vandenoever.info/software/strigi/">get it</a>.

<code>
0.7.1
 - Support more fields from ODF documents
 - Improved skipping behavior on streams for large files.
 - Added album art support.
 - Added support for ID3v1 tags.
 - Added MP3 stream metadata extraction, UTF-16 support in tags.
 - Extended the range of metadata extracted by ID3 analyzer.
 - Added a FLAC audio file analyzer.
 - Significantly unbreak the PDF analyzer.
 - Fix scanning trees where permissions are insufficient to read some parts
 - Check for multithreaded version of libxml2
 - Require newer CLucene version (0.9.21)
</code>

Join us in <a href="irc://irc.freenode.net#strigi">#strigi</a> for comments and questions.
<!--break-->
