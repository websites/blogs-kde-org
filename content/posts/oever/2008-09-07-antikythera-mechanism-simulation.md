---
title:   "Antikythera mechanism simulation"
date:    2008-09-07
authors:
  - oever
slug:    antikythera-mechanism-simulation
---
Yesterday, Adriaan <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/639-We-are-out-of-Antikytheran-Mechanisms.html">blogged</a> about the <a href="http://en.wikipedia.org/wiki/Antikythera_mechanism">Antikythera mechanism</a>. This is a fascinating piece of early machinery. Read all about it on the wikipedia page.

Ade called for a plasmoid to be created of it and I think this is a good idea. So I looked around on the web if there is some software simulating the mechanism. I found <a href="http://www.etl.uom.gr/mr/index.php?mypage=antikythera_sim">a webpage</a> where you can download an OpenGL implementation that is compiled for windows. You can run it with Wine on a i386 Linux machine. The source code is not available on the site. I was struck by backside of the mechanism. The inward spiraling lines look very much like the <a href="http://blogs.kde.org/node/3647">Akonadi clock</a> Cornelius blogged about some time ago.

<img src="https://blogs.kde.org/files/images/antfront.png"/>
<img src="https://blogs.kde.org/files/images/antback.png"/>
<!--break-->
