---
title:   "Re: Strigi Loaded"
date:    2008-07-24
authors:
  - oever
slug:    re-strigi-loaded
---
<a href="http://blogs.kde.org/node/3573">Sebastian is apologizing</a> to me for writing code. This is a bit strange and I need to reply to his post. The discussion below is really more suited for a mailing list, but since I did not start it I have no choice in the matter.

First of all, why do you apologize for writing code? That is no reason for apology.
What you could apologize for is not what you present, but how you present it.
You have written some code and talk about it like it is the solution for a big problem. You fail, however, to clearly analyze these problems. The title of the blog is "Strigi Reloaded - The Answer to all our Problems?". This is a title which is worthy of the Bild newspaper. Any title with a question mark behind a sentence which is not a question is a sign that the title is meaningless agitation. It is a method to gather support of fanboys in the absense of real arguments and to sow confusion. I'm not saying that this was your intent, but I am saying that using question marks in titles is a sign of bad writing.

But let's talk content and not form. What are these Problems that deserve a capital P? The first two problems you observe are untrue. You say that strigidaemon has no method to suspend and resume indexing. However, it does. The DBus API of strigidaemon allows you to start and stop indexing. Second, you say that strigidaemon does not report what it is doing. This is also not true; it does report this. In fact, this is even part of the Xesam interface. The reporting is not very elaborate. It says 'Idle' or 'Indexing', but this can be extended.

Not such a good start for a blog in which you are announcing a fork of a desktop search daemon. The list continues with things you have done to integrate indexing more tightly with the desktop. This is a good and noble cause. But the way you approach it is wrong. You mention that you use KDirWatch, Solid, KFreeDiskSpace, KPassivePopup to blend into the KDE user experience. That's very nice but no reason to fork a perfectly fine program. There is an elegant solution which I mention below.

The most serious problem you observe is watching files for changes. This is a major problem for all desktop search programs, but KDirWatch is not the solution.

KDirWatch is not suited for watching large amounts of files. It is a wrapper around inotify (default), FAM and QFileSystemWatcher (used on Windows. QFileSystemWatches is a small version of KDirWatch written by Trolltech. It only emits two signals: <code>directoryChanged(const QString& path)</code> and <code>fileChanged(const QString& path )<code>.) To use this wrapper class you must link libkio which depends on libQtSvg and libQtGui (and all of X).

What we need is a small crossplatform library for monitoring files. A dedicated library that wraps the native implementations for each of these functions in a clean API. A library that would work on any operating system and that would not link to anything that is not strictly necessary. A library that falls back cleanly to configurable polling when the native implementations are not suitable and that is modest in its use of memory and CPU.

It is unfortunate that you use classes that are part of the desktop environment for such low level functionality. This functionality should be in POSIX. Since it is not, it should be in a small helper library.

In conclusion, I like that you are putting effort in improving the desktop search experience. You're beying paid for it after all. But forking a perfectly fine daemon because you want some improvements is not the way to go. I am happy that you managed to build something so quickly: it proves that libstreamanalyzer is awesome.

Now this is the right solution: write a KDE service that uses Solid, KFreeDiskSpace and KPassivePopup to control strigidaemon over DBus. In that way, you do not need to fork it. I am happy to add the required DBus calls to strigidaemon. In this way we can keep strigidaemon desktop independent, small and fast and we avoid a fork and months of flames. And as an added bonus, you should end up with a nice API for Xesam.


<!--break-->