---
title:   "supporting LZMA streams"
date:    2009-01-25
authors:
  - oever
slug:    supporting-lzma-streams
---
LZMA is a relatively new compression algorithm. It is used in more and more places: 7-zip, the Linux kernel and deb and RPM packages. So adding LZMA to Strigi was a desirable step. The code for LZMA can be downloaded from the <a href="http://www.7-zip.org/sdk.html">7-zip website</a>. It is in the public domain.

For decompression, only 3 c files are required. I've added these to the Strigi repository directly. The decompression interface of LZMA is similar to that of GZip and BZip2. It nevertheless cost me quite a bit of time to figure out how to use it exactly. <a href="http://websvn.kde.org/trunk/kdesupport/strigi/src/streams/lzmainputstream.cpp?revision=916409&view=markup">The result</a> is a Strigi::InputStream class that can be used in the same way as the other decompression classes.

Once the InputStream was written, adding LZMA support to .deb files, .rpm files and <a href="http://websvn.kde.org/trunk/playground/base/strigiplasmoid/src/jstream/">jstream:/</a> was simple.

This improvement, along with a better index, will be released in Strigi 0.6.4 this week.
<!--break-->
