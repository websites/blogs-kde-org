---
title:   "Picking up slack"
date:    2007-07-28
authors:
  - oever
slug:    picking-slack
---
Dirk pointed me to an awesome application: <a href="http://sam.zoy.org/zzuf/">zzuf</a>. This program can help you make your program more failsafe when dealing with broken data. Zzuf inserts noise in your programs input. You call it as a wrapper about the code you want to test. The amount of noise is tunable and you can exclude files and directories from being 'fuzzed'. What is really elegant, is that Zzuf adds noise on the fly. The data on you disk is not affected. Zzuf just inserts itself between your program and libc and runs 'read' and 'fread' through its own noisy versions of these calls.

With Zzuf, I've found about five corner cases so far, while fuzzing gigs of data. This tool can really help adding the extra '9' to the percentage of uptime of your programs.
<!--break-->