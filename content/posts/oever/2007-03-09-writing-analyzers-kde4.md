---
title:   "writing analyzers for KDE4"
date:    2007-03-09
authors:
  - oever
slug:    writing-analyzers-kde4
---
You may have heard that KFilePlugin will be replaced in KDE4. In KDE4, we will use analyzers to get text and metadata out of files. Since last monday, you can start porting you analyzers and now I have written <a href="http://techbase.kde.org/Development/Tutorials/Writing_file_analyzers">a tutorial</a> on how to actually go about this. The tutorial uses BMP as a simple example and it should be pretty simple to port the existing KFilePlugin implementations.

If you are looking for a nice job in developing KDE4, check <a href="http://lxr.kde.org/ident?i=KFilePlugin">this list</a> of KFilePlugin implementations that need to be ported.

<!--break-->