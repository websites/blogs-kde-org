---
title:   "Awesome FOSDEM"
date:    2007-02-26
authors:
  - oever
slug:    awesome-fosdem
---
This weekend I was at <a href="http://fosdem.org">FOSDEM</a> and it was great. <a href="http://fosdem.org/2007/schedule/speakers/jim+gettys">Jim Gettys</a> was presenting the One Laptop Per Child progress (<a href="http://ftp.belnet.be/mirrors/FOSDEM/2007/FOSDEM2007-OLPC.ogg">video</a>).

<a href="http://www.vandenoever.info/software/strigi">Strigi</a> saw a lot of limelight. It was prominent in no less then three talks:
<dl>
<dt>Semantic KDE</dt><dd><a href="http://fosdem.org/2007/schedule/events/kde_semantic">abstract</a>, <a href="http://www.kde.org/kdeslides/fosdem2007/2007-02-vandenoever-kde_semantic.pdf">pdf</a> The EU funded Nepomuk project will have KDE as the most important reference implementation. Sebastion Trueg is implementing it and it looks to become a revolutionary improvement of the desktop experience.</dd>
<dt>Strigi Desktop Integration</dt><dd><a href="http://fosdem.org/2007/schedule/events/kde_strigi_desktop_integration">abstract</a>, <a href="http://www.kde.org/kdeslides/fosdem2007/2007-02-flaviocastelli-strigi_desktop_integration.pdf">pdf</a>, <a href="http://www.kde.org/kdeslides/fosdem2007/2007-02-flaviocastelli-strigi_desktop_integration.odp">odp</a> Flavio Castelli talks about how to use Strigi from your application.</dd>
<dt>Strigi Internals</dt><dd><a href="http://fosdem.org/2007/schedule/events/crossdesktop_strigi_internals">abstract</a>, <a href="http://www.kde.org/kdeslides/fosdem2007/2007-02-vandenoever-strigi_internals.pdf">pdf</a>, <a href="http://www.kde.org/kdeslides/fosdem2007/2007-02-vandenoever-strigi_internals.odp">odp</a> You can use the libraries and commandline tools of Strigi to make your desktop search just as fast.</dd>
</dl>

If you want to know more about <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a>, you can find some really nice documents <a href="http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/Deliverables">here</a>. The latest document about the KDE implementation of Nepomuk is <a href="http://nepomuk.semanticdesktop.org/xwiki/bin/download/Main1/D7-2/D7.2_v11_NEPOMUK_KDE_Community_Involvement.pdf">here</a>. It is really worth a read! There was a lot of buzz about Nepomuk and this paper shows you why. The foundations are nearing completion and a Nepomuk-KDE workshop for KDE developers would be a good next step.

<!--break-->


