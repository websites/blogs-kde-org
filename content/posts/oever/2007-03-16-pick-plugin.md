---
title:   "Pick a plugin!"
date:    2007-03-16
authors:
  - oever
slug:    pick-plugin
---
We're working on porting all the plugins to strigi analyzers to make them faster and make the results indexable. 
You can look at the current status <a href="http://wiki.kde.org/tiki-index.php?page=Porting+KFilePlugin+Progress">here</a>. If you feel like helping in KDE then read <a href="http://techbase.kde.org/index.php?title=Development/Tutorials/Writing_file_analyzers">this tutorial</a> or look at the code that's already available. We hope to port most of the plugins quickly.

Porting the will give KDE a great boost. Extracting metadata from files is much faster with the strigi classes. After parting the kfile_png plugin, we can extract metadata (including all comments) from a 4000 png files per second on an average laptop.

<img src="https://blogs.kde.org/system/files?file=images//prog.preview.png"/>

<!--break-->
