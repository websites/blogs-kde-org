---
title:   "Browsing archives in KDE4"
date:    2006-12-17
authors:
  - oever
slug:    browsing-archives-kde4
---
Yesterday, I compiled KDE4 and ported the jstream kioslave to it. This means that now you can open email attachements or embedded files in any KDE application without writing a temporary copy. The screenshot below shows konqueror with a treeview of the testdata that comes with Strigi. You can see that the sizes and modification times of the files are displayed properly.

<img src="https://blogs.kde.org/system/files?file=images//temp/jstream.preview.png"/>

There is discussion on the mailing list about the possibility of integrating this functionality deeper in KDE by adding it to libkio. In any case the kioslave is <a href="http://websvn.kde.org/trunk/playground/base/strigiplasmoid/">available now in SVN</a>.

<!--break-->
