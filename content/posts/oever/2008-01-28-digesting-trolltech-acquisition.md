---
title:   "digesting the Trolltech acquisition"
date:    2008-01-28
authors:
  - oever
slug:    digesting-trolltech-acquisition
---
What a surprise we had today! A coworker came to my desk and told me 'Guess what Nokia has done.' I thought for a bit and tried to infer Nokia's move from my colleagues demeanor. 'They decided to use Windows mobile on their telephones.' was my guess. As you all know by now, the right answer was much more interesting and much less gloomy.

So what will Nokia do with Qt? Many people are worrying about the Nokia's commitment to Trolltech's products. This huge move (for us <em>and</em> for Nokia) certainly requires some analysis. Let's have a look at what Nokia is saying. This image is from their press release:
<img src='https://blogs.kde.org/files/images//trolltech.jpg' alt='Nokia switches to Qt'/>

<a href="http://www.allaboutsymbian.com/news/item/6620_Nokia_to_acquire_Trolltech.php">source</a>

What is clear from this image is that Nokia will use Qt for their business phones and for the accompanying desktop applications. Nokia is planning to release software for the Linux desktop. And they are planning on using Qt (apparently not Qtopia) on their <a href="http://en.wikipedia.org/wiki/Nokia_Series_40">Series 40</a> and <a href="http://en.wikipedia.org/wiki/Nokia_Series_60">Series 60</a> smartphones.

But will allow Trolltech to continue to release brilliant Qt versions? This is what they say:
<ul>
<li><i>The acquisition supports Trolltech's company vision of driving Qt adoption in the commercial and open source markets (Qt Everywhere).</i></li>
<li><i>Trolltech has benefited greatly from the feedback the community has been providing while using Qt to develop free software. We respect the symbiotic relationship Qt has with the community and we wish to continue and <b>enhance</b> this relationship.</i></li></ul>
<a href="http://trolltech.com/28012008/28012008">source</a> (emphasis mine)

According to these quotes and the emphasis in the image, things are looking good from the Nokia side. It seems very likely that the market (IPhone, OpenMoko, Android, Windows Mobile) has forced Nokia to embrace a more open development strategy. Nokia has tried to build a developer community in Maemo. This has worked really well considering how young the project is. Of course they partly built upon the popularity and developer pool of GNOME. But Maemo is not nearly big enough to take on the world of smartphone software: the IPhone is a locked down machine without an SDK and it has more 3rd party applications than Maemo!

Nokia seems to have seen this too and they have decided to buy a project which has been selling itself as the perfect developer environment: the K Development Environment. Of course they cannot buy KDE outright, so they bought Trolltech.

So how do we feel about being bought? Personally, I feel pretty safe. We have a good contract with Trolltech which ensures that Qt, the basis for our project, will remain Free Software for ever. And at the same time, Nokia is (probably) giving us the opportunity to deploy our work on tens to hundreds of millions of computers and smartphones. Computers and smartphones on which people do not necessarily expect to find Windows XP or Vista.

So where will this leave <a href="http://en.wikipedia.org/wiki/Qt/Jambi">Jambi</a>? S60 supports java development via Eclipse, so Jambi might actually be a key technology in the eyes of Nokia, although Jambi is not J2ME MIDP compatible.

And while I'd love Nokia to give me KDE4 on my N800, that is <a href="http://lists.maemo.org/pipermail/maemo-users/2008-January/009136.html">not</a> something they will be providing. Which is fine with me, because I like Maemo.

The big message today is that the power of Free Software is growing. Even if Nokia puts a closed source version of Qt on their machines, it is unlikely that they will close off their machines to our software. If they did that, the KDE project would take a huge publicity hit and so would Nokia. Right now, people are skeptical about both Android and Nokia and OpenMoko is being delayed again and again. Nokia can use Qt to build a healthy ecosystem where Free and proprietary software can thrive. Let's hope they will.

I wish the <a href="http://www.trolltech.com">Nokia Trolls</a> all the best!

<!--break-->

