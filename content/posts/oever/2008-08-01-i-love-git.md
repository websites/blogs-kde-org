---
title:   "I love GIT"
date:    2008-08-01
authors:
  - oever
slug:    i-love-git
---
I have many ideas and limited time. And many of the ideas I want to work on get interrupted by pressing concerns such as bug fixing and high priority features. Because of this, I tend to have many small improvements lying around in my trunk. Then when the time comes to commit a quick bug fix, I face an 'svn status' output with lots of 'A', 'M' and '?' indicators. In short: a time sink.

But today I switched to git. I'd experimented with git before and had to get used to the new commands. But the advantages of maintaining many branches easily was always very appealing.

I used git-svn to check out the code and that was a nice trip down memory lane. git-svn will not just check out the current version of trunk. It will get all of the versions of your project, even when the location of the project in the hierarchy has changed.

Checking out Strigi showed me 'trunk/kdesupport/strigi', 'trunk/playground/base/strigi', 'trunk/playground/base/kitten' and 'branches/work/kde4/playground/libs/archivereader'. That's right: the origin of Strigi was in 'branches/work/kde4' in April 2006.

What is amazing is that storing the entire history of Strigi does not take much more space than a subversion checkout of the current version. git uses 25 MB whereas subversion requires 20 MB.

I've now sorted all pending improvements in branches:
<code>> git branch
* master
  midifiles
  mpegcleanup
  mspack
  pdf</code>
and can quickly switch between micro-projects with 'git checkout $branchname'.

I do not mind at all that KDE is still using subversion because git and subversion play together very well. But I do recommend contributors with a scattered and non-linear way of developing to adopt git. It will bring an uncanny sense of order in the way you work.

Retrieve the move history of a project:
<code>git log |grep git-svn-id|cut -d @ -f 1|uniq</code>

Retrieve author statistics:
<code>git log |grep ^Author|sort|uniq -c|sort -n</code>

<img src="http://mindtouchsoftware.com/random/git_two.jpg"/>

<!--break-->