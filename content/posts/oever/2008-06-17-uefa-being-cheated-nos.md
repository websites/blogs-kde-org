---
title:   "UEFA is being cheated by NOS"
date:    2008-06-17
authors:
  - oever
slug:    uefa-being-cheated-nos
---
The Dutch like it cheap. We do not like rules. That's just the way we are. The people at NOS are no different. They have a contract with UEFA (or so they say) which requires them to broadcast the games of the European Championship <i>only</i> in DRM format.
NOS is using this as an excuse to pendel Microsoft malware.

Well, guess what? They also broadcast it without DRM. The stream is a proprietary protocol, but it is not encrypted. You can even play it under Linux.
We call this 'van twee walletjes eten' (eating from two dykes) or 'pappen en nathouden'.

Finding this out is not easy. You have to look in the code of the broadcasting webpage. But it's possible. UEFA, you have been cheated. Red card for the Dutch, I guess.

For everybody else: happy viewing!

Some of these streams work from Dutch soil when games are being broadcast:
<a href="mms://livemedia.omroep.nl/npo-public?x=1&sid=nos-ek2008-800K-ned1-nondrm">Stream 1</a>
<a href="mms://livemedia.omroep.nl/npo-public?x=1&sid=nos-ek2008-800K-ned2-nondrm">Stream 2</a>
 <a href="mms://livemedia.omroep.nl/npo-public?x=1&sid=nos-ek2008-800K-ned3-nondrm">Stream 3</a>

I wonder if Plasterk knows he has been propagating the lies of NOS to the Dutch parliament.

You can congratulate the NOS <a href="mailto:publieksreacties@rtv.nos.nl">here</a>.
Or <a href="http://www.minocw.nl/ministerplasterk/reageer/index.html">tell the minister here</a>.

<!--break-->