---
title:   "OdfKit Hack Week day 2"
date:    2010-06-25
authors:
  - oever
slug:    odfkit-hack-week-day-2
---
Today was a day of style in the <a href="http://blogs.kde.org/node/4249">OdfKit Hack Week</a>. Enjoying the sun with style. Watching a soccer game with style. Watching the chicken spagetti races with style and most importantly adding a touch of style to OdfKit cum suis.

We ended the morning slightly listless because lists style in CSS is not as easy as we'd like. The CSS code I gave for lists <a href="http://blogs.kde.org/node/4253">yesterday</a> was not tested and seemed correct to me, but neither WebKit, nor Firefox liked it much. After much ado with counter-reset and counter-increment we discovered that, for now, the simple
<code>
 text|list-item {
    display: list-item;
 }
</code>
may offer the most satisfactory solution for now.

Another big topic today was style inheritance. In ODF, like in polite society, style is something you mostly inherit from your parents. This contrasts with CSS which has no notion of inheritance. Let us look at a simple example with a Title and Subtitle style.
<code>
<style:style style:name="Title" style:family="paragraph">
  <style:text-properties fo:font-weight='bold' fo:font-size='20pt' />
</style>
<style:style style:name="Subtitle" style:parent-style-name='Title'  style:family="paragraph">
  <style:text-properties fo:font-size='16pt' />
</style:style>
</code>
is now converted to these CSS rules:
<code>
text|p[style|style-name='Title'], text|p[style|style-name='Subtitle'] {
  font-weight: bold;
  font-size: 20pt;
}
text|p[style|style-name='Subtitle'] {
  font-size: 16pt;
}
</code>

The selector for the first rule has a match to the style 'Subtitle' added. The statements in there are partly overridden by the CSS rule below it. So here too, it is possible to map ODF styles to CSS. Smashing!

By the way, the order of the rules is important here. Later rules may override earlier rules. This is different from CSS.


<img src="http://www.slrcricket.co.uk/img/sport/main/zoomed/green-wooden-umpire-chair-27.jpg" alt="umpire chair" width="150px" /> I'm chairing the cloud session at <a href="http://www.akademy.org">Akademy</a> and hosting the <a href="http://community.kde.org/Events/Akademy/2010/Tuesday">OdfKit BOF</a>!
<!--break-->
