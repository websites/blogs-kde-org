---
title:   "Spot the heron"
date:    2008-06-06
authors:
  - oever
slug:    spot-heron
---
<a href="http://86.87.235.91/" alt="the pond/bait"><img src="https://blogs.kde.org/files/images//02-20080606080100-05.jpg"></a>

Please mail me if you see him.

Video by <a href="http://motion.sf.net/">motion</a> on a Logitech Sphere.
