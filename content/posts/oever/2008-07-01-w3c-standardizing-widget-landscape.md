---
title:   "W3C: Standardizing the widget landscape"
date:    2008-07-01
authors:
  - oever
slug:    w3c-standardizing-widget-landscape
---
The <a href="http://w3.org/">World Wide Web Consortium</a> is looking into the feasibility of standardizing desktop widgets. They have done a <a href="http://www.w3.org/TR/widgets-land/">survey</a> on the widget frameworks available in the market. The frameworks they have surveyed are Konfabulator, Windows Sidebar, Google Desktop Gadgets, Opera Widgets, Mac OSX Dashboard, Web-Runtime by Nokia, and Joost Widgets.

The survey was performed in the first quarter of 2008 and frameworks were chosen because of their perceived prevalence in the market place. Since KDE 4 was only just released and not available as a stable option for any distribution, W3C cannot be blamed for leaving out Plasma.

"The purpose of this document is to provide a holistic overview of the widget space" is what the survey says. It compares the different offerings and tries to unify the nomenclature for the different gadgets/widgets/plasmoids.

In addition to this document, W3C has published a document that "lists the design goals and requirements that a specification would need to address in order to standardize various aspects of widgets." So W3C is trying to see if they can pen down a standard that captures the features of the current products. If they can achieve this goal, it will be easier for developers to write widgets.

This document is a nice documentation and comparison of the frameworks that are out there now. There is a <a href="http://www.w3.org/TR/widgets-land/#apis">table</a> that lists the media capabilities of the different frameworks. It would be interesting to see how Plasma stacks up.

The W3C might consider including Plasma properties in their documentation if the Plasma developers contacted them. Even without adding Plasma explicitly, such an emerging standard would help Plasma developers in supporting all the different widget formats out there.

The document also describes the <a href="http://www.w3.org/TR/widgets-land/#differences0">differences from Java Applets</a>. The authors claim that programming in Java is daunting and that using HTML, CSS and JavaScript is much easier. That is a controversial statement considering that all programs implement HTML, CSS and JavaScript differently whilst all Java implementations (with the same version number) are the same.

<!--break-->




