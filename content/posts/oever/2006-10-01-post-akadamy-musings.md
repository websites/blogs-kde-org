---
title:   "post aKadamy musings"
date:    2006-10-01
authors:
  - oever
slug:    post-akadamy-musings
---
It's sunday morning, the sun is agreeably peeping through the cloud deck and I've just booked a room in a hotel in the Elsass region in northern France after having spent two days at home resting from an exhilarating aKademy.

This was my first aKademy and I'd not met anyone there before. So you can imagine I had quite a hard time remembering all the name/face pairs. Also <!--break-->I had to actively go out and get people to recognize me. For this reason, I proudly wore my badge, on which I'd scribbled 'Strigi' with a thick marker, all aKademy. And this worked wonderfully. Many people already knew Strigi and were enthusiastic about it. I heard many ideas from people on how it might benefit their project and KDE in general.

The first two days with all the presentations were great and I learned a lot, not just about the code but also about the people and what drives them to make this nice code. Also, a lot of time was spent chatting with people and finding out what projects could strike up useful collaborations with Strigi. Most notable in this respect is the Sebastian Truegs work on a KDE implementation of the standards defined in Nepomuk. If done well, this project could be an accelerator of the DBus and Portland technologies and would drastically improve interoperability between KDE programs and between KDE and GNOME programs, simply documenting and encouraging interoperability. Still, this is just the start of the project and I don't want to hype the project too much. Sebastian, of K3B fame, has just started and the topic is a hard. He'd be wise to pick the low hanging fruit while at the same time emphasizing that this is largely a standardization effort that will ultimately give us a more semantic (i.e. clever) desktop.

I'm hoping to catch up with Sebastian this week. He lives not far from Strasbourg so a visit suggests itself. Talk of computers will be forbidden though. After all I'm on holiday.