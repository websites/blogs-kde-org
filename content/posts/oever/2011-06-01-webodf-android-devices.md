---
title:   "WebODF on Android devices"
date:    2011-06-01
authors:
  - oever
slug:    webodf-android-devices
---
Today the WebODF project released an Android app. You can get it from the <a href="https://market.android.com/details?id=org.webodf">Android Market</a> and soon from <a href="http://fdroid.org">FDroid.org</a>. This is just the start. Viewing and editing office documents and in particular ODF files should be possible on all mobile devices. In the WebODF project we want to make this possible.

The Android application is 95% generic WebODF JavaScript and 5% Android specific Java code. For future ports to iPhone, iPad, MediaWiki and many other environments we will put most functionality in the shared JavaScript library and keep the application specific code to a minimum.

If you have a use case for WebODF, join us on <a href="http://webchat.freenode.net/?nick=webodfcurious&channels=webodf">#webodf</a> or <a href="https://lists.opendocsociety.org/mailman/listinfo/webodf">webodf@lists.opendocsociety.org</a> to discuss it.
<!--break-->
