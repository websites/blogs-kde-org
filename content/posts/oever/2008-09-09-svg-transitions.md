---
title:   "SVG transitions"
date:    2008-09-09
authors:
  - oever
slug:    svg-transitions
---
Playing with SVG is a lot of fun. I expanded the spiral clock I made yesterday with animated transitions between different panels.

<a href="http://ktown.kde.org/~vandenoever/spiralbox.svg"><img src="https://blogs.kde.org/files/images/sb1.preview.png"/></a>

The SVG above can be viewed with Firefox 3.0 or Opera 9. It is only 232 lines of SVG and Javascript.

<a href="http://people.mozilla.com/~vladimir/demos/canvas-text-path.html">There's</a> <a href="https://www.svgopen.org/2008/papers/104-SVG_in_KDE/">much</a> <a href="http://svgopen.org/2008/presentations/79-Offline_SQLiteSVG_database_applications_with_Firefox/index.pdf">more</a> <a href="http://www.treebuilder.de/default.asp?file=358730.xml">nice</a> <a href="http://bdn.backbase.com/blog/rus/advanced-3d-animations-and-transitions">SVG</a> <a href="http://www.youtube.com/watch?v=Har-PRP4X9U">around</a>.

SVG is definitely moving forward. With QtWebkit getting better, I hope we will see the ability to have javascripted SVG used <a href="http://people.mozilla.com/~vladimir/demos/photos.svg">as Plasmoids</a>.
<!--break-->
