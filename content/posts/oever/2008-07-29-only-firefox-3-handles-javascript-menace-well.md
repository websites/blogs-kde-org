---
title:   "Only Firefox 3 handles Javascript menace well"
date:    2008-07-29
authors:
  - oever
slug:    only-firefox-3-handles-javascript-menace-well
---
After visiting a really nice collection of <a href="http://www.boomkamp.com/">exhibit gardens</a>, I decided to make an application to see how different plants are related by making a <a href="http://en.wikipedia.org/wiki/Phylogenetic_tree">phylogentic tree</a> generating webpage.

For many groups of species, the familiar relationships have been examined very well, but for the combination of arbitrary species, you have to rely on taxonomic data for now. A convenient resource for getting at this data is <a href="http://www.ncbi.nlm.nih.gov/Taxonomy/">NCBI taxonomy</a> (<a href="ftp://ftp.ncbi.nih.gov/pub/taxonomy/">ftp</a>). This data unfortunately has no timeline distance between the branch points, but it's nice enough for starters.

For implementing the website I do not want to rely on server-side scripts. So the page will have to process the data via javascript. The current development version has a 12 Mb JSON file. This is the minimal amount of data for working on a tree with all species listed by NCBI.

And this is where the problems start. Because javascript is not the most efficient language for doing this. And the <a href="http://ktown.kde.org/~vandenoever/gardentree/nice.html">current version of the page</a> is very heavy for all browsers. In fact, Firefox 3 is the only browser that can load the page in a decent amount of time (8 seconds on a 1.1 Ghz machine). Konqueror 3 keeps asking me if I really want to continue running the script. Konqueror 4 crashes. Opera 9.5 is less than least half as fast as Firefox. So perhaps this page can be used as a nice torture test for javascript developers.

To use <a href="http://ktown.kde.org/~vandenoever/gardentree/nice.html">the page</a>, type latin species names in the text box and press enter. This way you can build up a tree. It's not user friendly at all. I'm still making a more nicer version with links and pictures.



<!--break-->

