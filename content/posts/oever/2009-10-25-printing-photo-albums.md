---
title:   "Printing photo albums"
date:    2009-10-25
authors:
  - oever
slug:    printing-photo-albums
---
One important feature for photo management is missing in the FOSS world:an application for creating photo albums that can be sent away for printing at a printing service. There is however a pretty slick closed source application that works on linux. It can be fiound at for example <a href="http://pixum.co.uk">Pixum</a> (also in.nl and .de). It is based on Qt 4.4 and installs using a perl script which downloads the artwork and the required libraries.  The application is customized for different printing companies that have these customized downloads available from their website. Not all of them
offer the linux or even the macintosh version. This is a shame and probably done to limit the number of different questions users might have.  A standard for these photo album ordering services would be great, but I'm not holding my breath and will recommend Pixum for now.
<!--break-->
