---
title:   "SlideCompare: improving rendering of slides in KOffice"
date:    2010-03-02
authors:
  - oever
slug:    slidecompare-improving-rendering-slides-koffice
---
Rendering slides is a complicated business. Slides can contain tons of different features just like webpages can. People expect that presentations look the same in different programs. Perhaps not pixel-perfect but very similar nevertheless.

OpenOffice and KOffice (and the Maemo/Meego Office Viewer) both have ODF as their main file format. ODF is an open standard and this means exchanging data between these programs should be simple and lossless. To help the developers of these programs find differences in rendering of slides, I have written a program that loads a presentation and shows it as rendered by KOffice and OpenOffice.

As an added bonus, it also shows how these programs render PowerPoint files. PowerPoint files are converted to ODP first and then loaded into each of the two rendering engines. That gives four types of output:
<ul>
<li>Converted by OpenOffice to ODP and rendered by OpenOffice</li>
<li>Converted by KOffice to ODP and rendered by KOffice</li>
<li>Converted by KOffice to ODP and rendered by OpenOffice</li>
<li>Converted by OpenOffice to ODP and rendered by KOffice</li>
</ul>
You can see an example view in the screenshot and screencast below.

The code has been announced on the <a href="http://lists.kde.org/?l=koffice-devel&m=126754452707003&w=2">koffice mailing list</a>.

<a href="http://ktown.kde.org/~vandenoever/slidecompare.ogv"><img src="https://blogs.kde.org/files/images/slidecompare.png"></a>

<a href="http://ktown.kde.org/~vandenoever/slidecompare.ogv">Ogg Theora screencast of SlideCompare</img>
<a href="http://blip.tv/file/3289053">Flash screencast of SlideCompare</a>


<embed src="http://blip.tv/play/hZElgcn4EQA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<!--break-->
