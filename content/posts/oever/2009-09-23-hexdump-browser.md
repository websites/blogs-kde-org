---
title:   "hexdump in the browser"
date:    2009-09-23
authors:
  - oever
slug:    hexdump-browser
---
This morning, I thought: why is XMLHttpReq for xml and text but not for binary files? It turns out you can use it for binary files, but not in each browser and only for remote files. I've written a small implementation of <tt>hexdump</tt>. It loads a binary file like this:
<pre>function load_binary_resource(url) {  
  // this works in firefox, chromium and arora,
  // but binary files are read only partially in konqueror and opera
  var req = new XMLHttpRequest();  
  req.open('GET', url, false);  
  req.overrideMimeType('text/plain; charset=x-user-defined');  
  req.send(null);
  return req.responseText;  
}
</pre>
Then you can access each byte with code like this:
<pre>
  var data = load_binary_resource(file);
  var byte5 = data.charCodeAt(5) & 0xff;
</pre>
As described <a href="https://developer.mozilla.org/En/Using_XMLHttpRequest#Receiving_binary_data">here</a>.
With these techniques I made <a href="http://ktown.kde.org/~vandenoever/test/hexdump.html">hexdump in the browser</a>.
<!--break-->