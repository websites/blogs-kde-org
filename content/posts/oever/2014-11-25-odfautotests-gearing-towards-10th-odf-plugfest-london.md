---
title:   "ODFAutoTests gearing up towards the 10th ODF Plugfest in London"
date:    2014-11-25
authors:
  - oever
slug:    odfautotests-gearing-towards-10th-odf-plugfest-london
---
In two weeks time, users and developers of <a href="http://www.opendocumentformat.org/">OpenDocument Format</a> software will meet up for a <a href="http://plugfest.opendocumentformat.org/2014-london/">two day ODF plugfest in London</a>. In preparation of the plugfest, I have spent last weekend, refreshing <a href="https://github.com/vandenoever/odfautotests/blob/master/doc/01_introduction.md">ODFAutoTests</a>. ODFAutoTests is a tool for creating test documents for ODF software and running these documents through the different implementations. If you want to help out with improving OpenDocument Format, please come to the plugfest, or participate online. Writing tests with ODFAutoTests is a great way to help make the 10th ODF Plugfest a success.

