---
title:   "XML Data Binding for C++"
date:    2006-08-06
authors:
  - oever
slug:    xml-data-binding-c
---
<p><a href="http://www.vandenoever.info/software/strigi/">Strigi</a> has reached the point that the configuration files for it should be more advanced than a text file with one directory per line. Because I have good experience with using <a href="http://www.w3.org/TR/xmlschema-0/">XML Schema</a> for mapping from XML to java and back using <a href="http://jaxb.dev.java.net/">JAXB</a>, I'd been looking for a good toolkit that does the same in C++. The requirements for such a tool are:</p>
<ul>
<li>Simple</li>
<li>Small</li>
<li>Few dependencies</li>
<li>Use XML Schema</li>
</ul>
<p>
A tool I've found is called <tt>xsd</tt>. Behind this trivially simple name is a suite of free software tools that generate different types of C++ code. The latest version of this tool is 2.2.0 and can be gotten <a href='http://codesynthesis.com/products/xsd/'>here</a>. There's an <a href='http://codesynthesis.com/projects/xsd/documentation/cxx/tree/manual/'>extensive manual</a> starting at a simple Hello World application.
</p>
<p>To test the feasibility of this code for Strigi I designed a small XML Schema file and wrote a simple program to accompany it. For Strigi, I'd like to have a configuration file that describes what directories and other sources are indexed and how. Below I've written the different file I used for testing <tt>xsd</tt>. The workflow is pretty easy:</p>

<!--break-->
<ol><li>write XML Schema</li><li>compile c++ from schema</li><li>use data classes in c++ code</li></ol>
<p>If the requirements for the configuration file change, the XML Schema is changed and the c++ code regenerated. Any changes required in the c++ code that uses the data classes will be caught by the compiler.</p>
<p>Nothing is for free: using xsd introduces dependencies in your code. When using xsd, you will need to link your executable with xerces-c. Since this is a widespread library, this is not a big problem.</p>
<p>An example configuration file might look like this:</p>
<pre>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;no&quot; ?&gt;
&lt;s:daemonConfiguration xmlns:s=&quot;http://www.vandenoever.info/strigi&quot;&gt;

  &lt;repository repositoryLocation=&quot;/home/tux/.strigi/mainindex&quot; repositoryType=&quot;CLucene&quot;&gt;
    &lt;fileSystemSource baseURI=&quot;file:/home/tux&quot;/&gt;
    &lt;httpSource baseURI=&quot;http://www.kde.org/&quot;/&gt;
  &lt;/repository&gt;

&lt;/s:daemonConfiguration&gt;
</pre>
<p>
            For convenience, this file is kept simple. What you can see is that Strigi could have configurations for multiple repositories (although this file only shows one). Each repository has one index which contains information extracted from various sources, such as files from the filesystem or web pages. These sources are described by the elements <tt>fileSystemSource</tt> and <tt>httpSource</tt>. These elements are both instances of the more general <tt>fileSource</tt> element.
        </p>
<p>The vocabulary can be described with an XML Schema:</p>
<pre>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
&lt;schema xmlns=&quot;http://www.w3.org/2001/XMLSchema&quot;
 targetNamespace=&quot;http://www.vandenoever.info/strigi&quot;
 xmlns:tns=&quot;http://www.vandenoever.info/strigi&quot;&gt;

 &lt;element name=&quot;daemonConfiguration&quot; type=&quot;tns:daemonConfigurationType&quot;/&gt;

 &lt;complexType name=&quot;daemonConfigurationType&quot;&gt;
   &lt;sequence&gt;
   &lt;!-- a repository contains one index --&gt;
   &lt;element name=&quot;repository&quot; type=&quot;tns:repositoryType&quot;
       minOccurs=&quot;0&quot; maxOccurs=&quot;unbounded&quot;&gt;
   &lt;/element&gt;
  &lt;/sequence&gt;
 &lt;/complexType&gt;

 &lt;complexType name=&quot;repositoryType&quot;&gt;
  &lt;sequence&gt;
   &lt;element name=&quot;fileSystemSource&quot; type=&quot;tns:fileSystemSourceType&quot;
       minOccurs=&quot;0&quot; maxOccurs=&quot;unbounded&quot;/&gt;
   &lt;element name=&quot;httpSource&quot; type=&quot;tns:httpSourceType&quot;
       minOccurs=&quot;0&quot; maxOccurs=&quot;unbounded&quot;/&gt;
  &lt;/sequence&gt;
  &lt;attribute name=&quot;repositoryLocation&quot; type=&quot;anyURI&quot; use='required'/&gt;
  &lt;attribute name=&quot;repositoryType&quot; type=&quot;tns:repositoryTypeType&quot;
    use='required'/&gt;
 &lt;/complexType&gt;

 &lt;simpleType name=&quot;repositoryTypeType&quot;&gt;
  &lt;restriction base='string'&gt;
   &lt;enumeration value='CLucene'/&gt;
   &lt;enumeration value='HyperEstraier'/&gt;
   &lt;enumeration value='Xapian'/&gt;
   &lt;enumeration value='Sqlite'/&gt;
  &lt;/restriction&gt;
 &lt;/simpleType&gt;

 &lt;complexType name=&quot;fileSourceType&quot;&gt;
  &lt;attribute name=&quot;baseURI&quot; type=&quot;anyURI&quot; use='required'/&gt;
  &lt;!-- time between updates for this directory, &lt;= 0 means never --&gt;
  &lt;attribute name=&quot;autoUpdateFrequence&quot; type=&quot;int&quot;/&gt;
 &lt;/complexType&gt;

 &lt;complexType name=&quot;fileSystemSourceType&quot;&gt;
  &lt;complexContent&gt;
   &lt;extension base=&quot;tns:fileSourceType&quot;&gt;
    &lt;sequence&gt;
     &lt;element name=&quot;fileEventListener&quot; minOccurs=&quot;0&quot; maxOccurs=&quot;1&quot;&gt;
      &lt;complexType&gt;
      &lt;/complexType&gt;
     &lt;/element&gt;
    &lt;/sequence&gt;
   &lt;/extension&gt;
  &lt;/complexContent&gt;
 &lt;/complexType&gt;

 &lt;complexType name=&quot;httpSourceType&quot;&gt;
  &lt;complexContent&gt;
   &lt;extension base=&quot;tns:fileSourceType&quot;&gt;
   &lt;/extension&gt;
  &lt;/complexContent&gt;
 &lt;/complexType&gt;

&lt;/schema&gt;
</pre>
<p>This XML Schema file can be compiled into source code with the command <tt>xsd cxx-tree --generate-serialization strigidaemon.xsd</tt>. Here is a simple program that uses this code to read and write this type of configuration file:</p>
<pre>#include &quot;strigidaemon.hxx&quot;
#include &lt;iostream&gt;
using namespace std;

int
main (int argc, char* argv[]) {
    auto_ptr&lt;strigi::daemonConfigurationType&gt; config;

    if (argc &gt; 1) {
        // load an object
        try {
            config = strigi::daemonConfiguration(argv[1],
                xml_schema::flags::dont_validate);
        } catch (struct xsd::cxx::tree::exception&lt;char&gt;&amp; e) {
            cerr &lt;&lt; e &lt;&lt; endl;
        }
    }
    if (!config.get()) {
        // create an object
        config = auto_ptr&lt;strigi::daemonConfigurationType&gt;(
            new strigi::daemonConfigurationType());
    }

    // add elements to the object
    string repositoryLocation = &quot;/home/tux/.strigi/mainindex&quot;;
    strigi::repositoryTypeType repositoryType(&quot;CLucene&quot;);
    strigi::repositoryType repo(repositoryLocation, repositoryType);

    strigi::fileSystemSourceType fs(&quot;file:/home/tux&quot;);
    repo.fileSystemSource().push_back(fs);

    strigi::httpSourceType hs(&quot;http://www.kde.org/&quot;);
    repo.httpSource().push_back(hs);

    config-&gt;repository().push_back(repo);

    // provide a mapping for the namespace we use
    xml_schema::namespace_infomap map;
    map[&quot;s&quot;].name = &quot;http://www.vandenoever.info/strigi&quot;;

    // output the object to the standard output iostream
    daemonConfiguration(cout, *config, map);

    return 0;
}
</pre>
<p>To finish off, here's the Makefile I used to compile this small test program:</p>
<pre>LDFLAGS=-lxerces-c
CXXFLAGS=-Wall -O2
main: main.cpp strigidaemon.cxx

strigidaemon.cxx: strigidaemon.xsd
        xsd cxx-tree --generate-serialization strigidaemon.xsd

clean:
        rm strigidaemon.cxx strigidaemon.hxx main</pre>

