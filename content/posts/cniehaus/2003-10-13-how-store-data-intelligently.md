---
title:   "How to store data intelligently?"
date:    2003-10-13
authors:
  - cniehaus
slug:    how-store-data-intelligently
---
I have been thinking about how to store the data in Kalzium (chemistryapp in kdeedu). Currently I have a class called kalzium in which 110 "elements" are. An "element" is of course an object which stores the data available for one of the 110 elements in the periodic table.

<!--break-->

I give a pointer of the kalzium-object to every object I create, and which needs the data. For KDE 3.3/4 I would like to restructure a lot in Kalzium. So my question is: what is the best way to store my data. Of course the data doesn't change. The user can't add any data. So I could use a global 

<pre>static const QValueList&lt;Element*&gt;</pre>

but I am not sure if that is good. Is it better to give a pointer to all created objects? Or is there even a third way which is the best solution