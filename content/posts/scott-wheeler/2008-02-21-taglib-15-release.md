---
title:   "TagLib 1.5 Release"
date:    2008-02-21
authors:
  - scott wheeler
slug:    taglib-15-release
---
<a href="http://developer.kde.org/~wheeler/taglib.html">TagLib 1.5</a> is out.

As always, file any bug reports that you happen to run into in the bug tracker.  As there are specifically a couple things that I intend to implement (wav / aiff support as well as support for ID3v2 tags in RIFF chunks) I expect a 1.5.1 (or 1.6) to be much faster in coming around this time.

I'd like to give a special thanks to Lukáš Lalinský for the numerous bug fixes, testing and new features, Urs Fleisch, Aaron VonderHaar for their ID3v2 frame implementations and of course Michael Pyne for helping keep the bug list in check.

<!--break-->

<b>Changes from 1.4 to 1.5</b>

  <ul>
    <li>Support for Mac OS X and Microsoft Windows</li>
    <li>Distributed under the MPL (in addition to the previous LGPL license)</li>
    <li>Added support for Speex files</li>
    <li>Added support for TrueAudio files</li>
    <li>Added support for WavPack files</li>
    <li>Added support for ID3v2 general encapsulated object frames</li>
    <li>Added support for ID3v2 unsynchronized lyrics frames</li>
    <li>Added support for ID3v2 URL frames</li>
    <li>Propper exports of all public classes / functions</li>
    <li>Updated the APE::Item API to work with value lists</li>
    <li>Added support to the FileRef class for new Xiph (Ogg) extensions</li>
    <li>Made the samples per frame for MPEG headers accessible</li>
    <li>Made MP3 Xing headers accessible</li>
    <li>Prevent invalid encodings from being written to ID3v1 tags</li>
    <li>Non-Latin1 ID3v2 text frames are automatically converted to UTF-8 on write (if they are not explicitly set to UTF-16)</li>
    <li>Added support for reading ID3v2.2/3 unsynchronized tags</li>
    <li>Made it possible to search for ID3v2 comment frames by description</li>
    <li>Fixed a number of bugs in ID3v2 relative volume adjustment reading and writing</li>
    <li>Added work arounds for iTunes writing invalid ID3v2 frame lengths</li>
    <li>Added work arounds for iTunes not being able to correctly parse numerical ID3v2 genres</li>
    <li>Added work arounds for iTunes putting non-text information in ID3v2 comment frames</li>
    <li>Added a function to export strings to std::wstring</li>
    <li>Added a function to check ASCII compatibility of strings</li>
    <li>Added a function to check Latin1 compatibility of strings</li>
  </ul>