---
title:   "Berlin, T-Minus 39 hours.  Transitions.  Laptop Battle Mannheim.  Assorted Blather."
date:    2006-06-29
authors:
  - scott wheeler
slug:    berlin-t-minus-39-hours-transitions-laptop-battle-mannheim-assorted-blather
---
I hate moving.  I've lost the grace with which I was able to execute such during my early 20s.  Oh, I'm looking forward to being in Berlin, but fear and loathing is sinking in as I look at the mess that is my apartment and realize that it has to be fully packed, moved out of and clean in something like 39 hours.  I suppose I can see where my true loves are in the pile of instruments (9 now), hundreds of CDs and records, assorted pro-audio equipment and naturally a pile of computer gear.  And books.  Lots of books.

This is a true time of transitions for me -- I'm moving, switching jobs, the family has been through a transformative time with the recent death of my father.

KDE has slipped to the background of late and like many aging (Ok, so I just turned 26, but I got into this stuff when I was 20.) F/OSS hackers I'm left wondering if that's a real transformation -- a shift in priorities -- or simply a phase that will be revisited once life settles down a bit.  There's still a desire to come home and code for hours on par with where I was at when I went through the last big transition -- my move to Germany four years ago.  But of late there's, well, life going on.

One of the things that I have been involved in lately, naturally timed with my exodus from Mannheim, is the organization of a live electronic music event, <a href="http://www.laptopbattle-mannheim.de">Laptop Battle Mannheim</a> which is looking like it's going to land well within the range of cool.  We've got a mostly filled out lineup of contestants that range from folks playing their first time live to internationally recognized electronic musicians with dozens of records to their name.  4 of the 5 DJs that we've got (tentatively) lined up for the after-event have international gigging experience.  This also matches up well with the upcoming job transition, where I'll be working on software for electronic music production and performance.  It's looking pretty groovy and we're expecting a turnout of upwards from 600 folks.

Anyway -- moving.  Berlin.  I'll be living just outside of Kreuzberg which should be lots of fun.  There's still all of the packing, moving, van rental complications, work permit blather and associated regalia, but I feel at this point like it'll all work out.  One or two more stressful weeks and my life will be on track in Berlin.

So, the internet goes away tomorrow; I'll catch you guys on the other side of connectedness, roughly three weeks out.
<!--break-->