---
title:   "VFolders, History and Changelogs, Oh my!  (And if you order now: A screenie and rundown of recent speed hacks!)"
date:    2003-08-19
authors:
  - scott wheeler
slug:    vfolders-history-and-changelogs-oh-my-and-if-you-order-now-screenie-and-rundown-recent
---
JuK just got a couple of pretty nifty features in the last week or so.  The implementations had a number of related issues to it made sense to solve them in tandem.  This first of these is a <i>history playlist</i> which, when turned on, keeps a log of all of the things that you've played and the times that you played them.  It's off by default, but you can turn it on via the view menu.
<br><br>
The next big thing is that I finally finished up the code for <i>search playlists</i>.  Basically this rolls together an advanced search with some vfolder-like functionality.  You create a search, in a dialog not unlike KMail's, that creates a new <i>search playlist</i>.  This playlist is automagically updated when your collection changes.
<br><br>
Here's a screenie with some of the new goods:
<br><br>
[image:157]
<br><br>
Here you can see a <i>search playlist</i> that finds everything from the Afro Cuban All Stars and Buena Vista Social Club.  Since these are actually the same group of folks the union makes sense, but it's one that's difficult to pick out in something like the tree view (though you could select both groups at once to see a dynamic playlist composed of the two).  Notice that the search dialog continues to work; here I'm filtering for the year 1997.  Also show here is the history playlist item and the new tree view.
<br><br>
The other thing that I've hacked out in the last week or so were some pretty hefty optimizations for those with large collections.  I created a number of dummy files to boost my collection to up around 12,000 items and KCacheGrind and I fixed up some of the algorithms that should make using JuK with large collections -- say over 10000 files -- much nicer.  I was frightened to notice that at the beginning of profiling start up time with 12,000 items was near a minute and a half.  I had never tested with this many items so I was rather surprised.  I'm now quite happy to say that even with 12,000 items start up time is in the range of 5 seconds.
<br><br>
I've also had a number of folks still using the 1.1 release ask what's been going on for the last several months.  So, while this is certainly incomplete, here's a rough run down of the major features that have been implemented:
<br><br>
<ul>
<li>Incremental search</li>
<li>Search playlists or <i>vfolders</i></li>
<li>Compact and tree view modes (for the list of playlists)</li>
<li>DCOP interface</li>
<li>Configurable key bindings</li>
<li>Configurable passive popups on track change</li>
<li>File name based tag guessing</li>
<li><a href="http://www.musicbrainz.org">MusicBrainz</a> integration for file
    recognition and tag guessing</li>
<li>Playlist <i>unions</i> - selecting multiple playlists and seeing the
    contents of the two of them combined</li>
<li>A history playlist with a list of played items and the times that you played
    them</li>
<li>Search playlists / advanced search / <i>vfolders</i> - there is now an advanced
    search dialog which creates <i>search playlists</i> which are the results of a
    search that can contain multiple terms.  The list is updated automatically when
    the values of the items change</li>
<li>Roughly 100 reported bugs fixed and dozens of smaller features added</li>
</ul>
<br><br>
So, what's next?  TagLib.  TagLib is the id3lib replacement that I've been working on for way too long.  I had a kick of hacking on it a few weeks back where things are getting pretty close to working, but I've been neglecting it while I've worked on the features above.  It's time for it to mature a bit and I want to cut JuK over to using it in the near future.  Soon I'll be back to the JuK bug-bin but it's time for a context switch.  Oh, and who knows what will happen at the developers meeting in Nove Hrady next week.  :-)