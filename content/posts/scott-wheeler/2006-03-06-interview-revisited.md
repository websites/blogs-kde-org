---
title:   "Interview Revisited"
date:    2006-03-06
authors:
  - scott wheeler
slug:    interview-revisited
---
A year and a half ago I first tried out Interview and <a href="http://blogs.kde.org/node/550">noted my thoughts</a> on it.  In the last few days I've revisited it and have to say that the API is still pretty quirky.

As kind of some explorative hacking I've been working on what will likely be a demo-ish Qt 4 based tagger.  The immediate application is of course reworking the central widgets in JuK, but that's not really the main goal.  More what I'm working on is getting a feel for the API so that I can have some idea what and how should be wrapped in a set of KDE libs classes and from there how that will be applied in list-centric applications such as JuK, amaroK and KMail.

The first ugly hackup is <a href="http://developer.kde.org/~wheeler/images/intertag.png">here</a>.  On the surface it's boring; more will follow in the next few days, but it's doing some semi-interesting things like reading information in a background thread and populating the list asynchronously.

Many of the quirks are design based and some of them are just that the docs aren't up to Trolltech's normal quality.  I spent a couple of hours trying to figure out how to have new items from my data model show up in the list before breaking down to read one of the models shipped with Qt (QDirModel) to realize that I needed to call beginInsertRows() before making any changes and then endInsertRows() afterwards.  The API docs don't really make that clear.  Still without sorting, filtering and whatnot it's already at several hundred lines of code.

There are some good things that I can see at this point; it seems that asynchronous updates are much smoother and it seems like also filtering should be faster (in theory).  It should also be possible to do smarter caching of sorting (using sorted inserts rather than iterating over the entire list) so that switching sort columns should be fast as will inserting of new items.  More on that will follow.

Where I've been frustrated with Interview, I must say that Qt's thread safety features in 4.x are much nicer.  The fact that the basic toolkit classes are reentrant and that signals and slots are now thread safe (and potentially asynchronous) is nice.  That's much easier than working with QDeepCopy and having to wrap things into passing things around via QEvents as was the case in Qt 3.

<b>Meanwhile, in other regions of my brain...</b>

I got a bit of time to work on some music this weekend.  Coming from a fairly formal informal background in music (I've studied theory a fair bit, taught bass for a couple of years, etc., though I've never had much in the way of formal training.) as I've gotten into electronic music I struggle some with structure of compositions.  Traditional classical music tends to be set along some relatively straight-forward classical forms; jazz and rock are usually dead-simple formally.  While often the interesting compositions veer quite far from the predefined forms, getting a handle on the genres entails familiarizing oneself with them.  In electronic music I've yet to come up with that "handle".  "This is the way a basic house / techno / trip-hop track works."  (Of course in more academic electronic music it tends to fall formally closer to classical structure.)

So, basically I've decided that I'm going to take a set of tracks that I tend to like and do some formal analysis on them and see if any notable generalities emerge.  If they end up being worthwhile I'll put them up online somewhere...

The reason I'm writing this here is that after some searching I've yet to stumble across anything similar -- anyone giving semi-serious treatment to popular electronic music styles (and me being a music nerd as well, that's kind of how I approach things).  Since comparing to other stuff would of course be nice, on the off chance that someone reading this is familiar with such, kindly leave a comment.  (Books, links, etc. are all fine.)
<!--break-->