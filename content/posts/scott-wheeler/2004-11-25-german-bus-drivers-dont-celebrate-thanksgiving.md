---
title:   "German Bus Drivers Don't Celebrate Thanksgiving"
date:    2004-11-25
authors:
  - scott wheeler
slug:    german-bus-drivers-dont-celebrate-thanksgiving
---
It seems that German bus drivers don't celebrate American Thanksgiving.  Funny.

Somedays I love a nice, well funded public transportation system.  Somedays, like say, today, it makes me want to break things.

<ul>
  <li>I rush out of my appartment</li>
  <li>I make it to the bus stop right on time</li>
  <li>There's a construction crew there</li>
  <li>A few seconds after I arrive the bus comes by, and doesn't stop</li>
  <li><i>But wait!  There's more!</i> Stops at the red light 10 meters away</li>
  <li>I walk up and start pointing and waving to get the bus driver to let me in</li>
  <li>He won't do so because we're not at the bus stop (which he skipped)</li>
  <li>I'll get to work 40 minutes later</li>
</ul>

Ugh.  And actually I've been through this scenario at least 2-3 times in the last year.  Somehow a bit different from say, Chile, where the doors were always open and you hopped on and off while the bus was moving.
<!--break-->