---
title:   "Just so y'all know.  (Learning to speak Texan.)"
date:    2004-11-06
authors:
  - scott wheeler
slug:    just-so-yall-know-learning-speak-texan
---
"Howdy" isn't a question.  This point seems to have confused a number of my European friends.  Sure, it sounds like -- and is probably derived from -- "How do you do?" but it's a simple greeting and is used like the variations on "good day" in various languages / dialects (g'day, bonjour, buenos dias, dobry den, moin, guten Tag, etc.)  If you respond with "fine" this will cause nothing but confusion.  Instead you can reply with the same or another greeting.

Oh, and while I'm making clarifications on behalf of Texas, I should at this point also note that <a href="http://en.wikipedia.org/wiki/George_W._Bush">Bush isn't a Texan</a>, <a href="http://en.wikipedia.org/wiki/David_Cobb">David Cobb however is</a> (note the locations of birth).

Anyway, I may post something useful later.  I'm at least hoping to have a productive weekend.  I got the GStreamer 0.8 backend for JuK checked in and I think I'll be working on some of my search / link stuff for most of the weekend since I want to have a demo ready for Linux Bangalore.
<!--break-->