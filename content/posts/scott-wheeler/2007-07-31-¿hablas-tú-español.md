---
title:   "¿Hablas tú español?"
date:    2007-07-31
authors:
  - scott wheeler
slug:    ¿hablas-tú-español
---
In the world of things completely unrelated to KDE...

My life has been rather concentrated on music the last few months (including the various small-ish will-someday-be-released OSS things that I've been hacking on of late).  One of the things that came up yesterday in a jam session was that based on the group's name, which contains a reference to Spanish, is that it would be fun to have a collection of samples for use in our set, with various voices in various languages saying:

<b><i>"Do you speak Spanish?"</i></b>

So, here's the call, dear blogosphere, send me a wav, mp3 or ogg to <a href="mailto:wheeler@kde.org">wheeler@kde.org</a> with you asking that question in your local language / accent, and let me know where you're from / which language it is.  Bonus points for additonal samples from girlfriends, boyfriends, housemates.  If it pans out and there's enough of a selection to do something musically interesting with, there's a fair chance that your voice will be ringing out over the PA system at some dirty techno club in Berlin within a few months.  :-)

<b>Laptop Battle v2.0</b>

In other music related news, <a href="http://www.laptopbattle-mannheim.de/">Laptop Battle Mannheim</a> and <a href="http://www.laptopbattle-stuttgart.de/">Laptop Battle Stuttgart</a> are both on the horizen and still taking demos for contestants.  (I was helped out a bit with the Mannheim one last year and will be on the jury again this year.)  If you happen to live in those areas and are produce electronic music, consider sending a demo in!
<!--break-->