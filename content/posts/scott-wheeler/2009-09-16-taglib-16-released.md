---
title:   "TagLib 1.6 Released"
date:    2009-09-16
authors:
  - scott wheeler
slug:    taglib-16-released
---
So, after far too long, <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib 1.6 is out</a>.  I finally asked Lukáš Lalinský, who's been the largest TagLib contributor other than myself and veteran of the MusicBrainz project, to step in and take over maintainership as I've been off doing the whole <a href="http://www.directededge.com/">interwebs startup thing</a> for the last year and change and time is exceedingly scarce of late.

The real highlights of this release are a whole bunch of new formats supported:

<ul>
  <li>MP4 [compile time option]</li>
  <li>ASF (WMA) [compile time option]</li>
  <li>WAV</li>
  <li>AIFF</li>
</ul>

The first two are compile time options so that distributions can easily decide if they thing just dealing with the containers / tags of encumbered formats is murky territory.  Tarball has been up for a couple of days.  Enjoy, report bugs and give a big round of thanks to Lukáš!
<!--break-->
