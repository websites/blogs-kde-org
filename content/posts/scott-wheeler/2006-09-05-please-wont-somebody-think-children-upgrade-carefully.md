---
title:   "Please, won't somebody think of the children!?  (Upgrade Carefully)"
date:    2006-09-05
authors:
  - scott wheeler
slug:    please-wont-somebody-think-children-upgrade-carefully
---
Ok, here's a little tip -- <b>turn off your RSS feed before upgrading your blog software</b>.  Really.  Please.  For the sake of all things good and holy.   It just happens to be Anders' feed today, but it seems like about every 2-3 days somebody decideds to upgrade their blog software, thus flooding the planet with everything that they've ever written.
<!--break-->