---
title:   "Zarro boogs and The Shape of JuK to Come"
date:    2003-07-31
authors:
  - scott wheeler
slug:    zarro-boogs-and-shape-juk-come
---
So I'm excited; JuK is down to zero open bug reports.  I think this is the first time that this has been true since about 2 days after it went into CVS.

I'm still working on implementing some of the feature requests -- I made it possible to delete multiple playlists at once today, but it's nice looking at the bug reports screen and seeing a big &quot;0&quot;.  While I'm sure it won't last long it gives me a warm fuzzy feeling.

I also started today on one of the oldest and most requested features -- &quot;vfolders / search playlists&quot;.  The framework has been there for a while and is what drive the tree view mode, but I needed to create a dialog for creating such searches.  I committed the dialog to CVS a few minutes ago and will be filling in the functionality in the next few days.

Also I got word today from Matthias Kretz (one of the other denizens of the suburb of Heidelberg that I live in) spiffied up the ArtsPlayer class; something that worked but I've always known was a hack.  He's said that it should now be possible for it to work with streaming.  We'll see if that pans out.

My (likely) last round of summer visitors just left so I should have more time to get things done in the next few weeks leading up to Nove Hrady.  Hopefully near that time it will be clear what's going to make it into 3.2 and what will have to wait.

<i>Bonus points for those that caught the <a href="http://www.cootiesjazz.com/discs/shapejazz.htm">Ornette Coleman reference</a> in the title.</i>
