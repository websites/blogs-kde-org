---
title:   "Joining the Fun"
date:    2005-07-23
authors:
  - scott wheeler
slug:    joining-fun
---
So, I decided to join in the fun today, after much of the hard work has already been done.  In a few minutes I ported KSig over to KDE 4.  It's not a real port in the sense of it was just screwing around enough with it to make it compile -- Qt 3-isms still abound, but I thought I'd play with a nice-small-toy-ish app to start off before jumping in full swing.  I'll probably continue and clean out all of the Qt 3-ness there for practice.

<img class="showonplanet" src="http://developer.kde.org/~wheeler/images/ksig-kde4.png"/>

At some point I'll need to start looking back at <i>Interview</i> classes and see what we need in terms of KDE wrappers.  I haven't looked at them significantly since one of the early Qt 4 betas.

TagLib 1.4 will also really (yes, I promise this time) go out in the next couple of days.  I've fixed most of the outstanding bugs and dealt with a number of the feature requests as well.  There are two bugs left that I've earmarked for attempting before 1.4, but we'll see.  I still need to do some testing since some of the code I wrote just going by the spec, but didn't have anything to test it on.  So, for those of you with apps using TagLib, this weekend is a good time to pull it from SVN and give it a run-through.

My motivation seems to be returning (it comes and goes in phases -- I haven't done much KDE stuff in the last month or two) and today I was also playing around with some evil hacks to try to come up of a mock-up of a much simplified Konq which I'm tentatively calling <i>Tofu</i>.  I removed probably 50% of the menu entries, most of the toolbar entries, all of the "extensions" (and will probably just munge the one or two important ones into the code).  Tomorrow I want to play with bringing the "search" toolbar for file management down to one line by default (and possibly leaving it on all the time in the file mode thus ditching another menu item).  I also want to ditch view profiles in favor of automatic switching between file view and web browsing modes based on what's actually being done.  I may also try to add a flag to KPart desktop files that specifically specifically indicates that they can be loaded in Konq.

Much of this is complicated by all of the generally wonderful magical component loading of Konq.  Unfortunately that also makes it a huge crap magnet.

Before the flames start I'm not attempting to do anything clean code-wise and I'm also not trying to produce the next generation of Konq or anything.  I'm just playing around with seeing how lean I can get Konq and then may propose merging the best of that back into Konq.  That's all if I actually stay interested in the trimming for a few more days.  :-)

<!--break-->