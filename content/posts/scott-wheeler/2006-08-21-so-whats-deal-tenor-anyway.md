---
title:   "So, what's the deal with Tenor anyway?"
date:    2006-08-21
authors:
  - scott wheeler
slug:    so-whats-deal-tenor-anyway
---
<b>Reposted from <a href="http://dot.kde.org/1155935483/1155982271/1155985508/1155985957/1155992030/">the Dot</a>:</b>
<i>
> Didn't knew about that. On the other hand its still the only information that looks relevant.
> The official kde.org site does not give any hint on the status of neither Kat, nor Tenor; the
> latest information I found is dated 2005. SVN activity is at a low level (latest update 4
> month ago).
>
> IMHO, the fact that Scott leaves a Linux related job in favor of a more multimedia related one
> (whatever this means in detail), <u>may</u> be more relevant than it looks at a first glance.
</i>

I suppose I can drop in some details here:

Daniel was correct that SAP never significantly supported my personal KDE related projects.  (There were however a few times that there were features or fixes that SAP needed inside of KDE that I was allowed to work on, but we're talking about maybe one week of KDE work per year.)

At my current job I'm doing cross-platform pro-audio stuff, which also has nothing to do with KDE, but for the moment I rather enjoy.

Sponsorship for my KDE work has never been something that I've sought.  KDE is one of my hobbies and I'm fine with it staying that way.  (Not to mention that I have to have normal full-time employment to remain in Germany where I've been for several years.)

So, then what's with Tenor?

Like I said above, KDE is my hobby.  Sometimes I feel like working on it, sometimes I don't.  (My life has also been really busy in the last few months, but really it's more of a matter of motivation than time.)  When I don't feel like working on it, I don't.  It's really that simple.

The desire to work on Tenor for me comes in waves.  A couple months back I spent a few weeks hacking on it again.  I haven't touched it since then.

So, will Tenor be in KDE 4?

Maybe.  Really it depends on if and when I feel like hacking on it or if someone else decides to pick it up and run with it.  Think of it as a surprise.  ;-)

One thing that some people who ask me about this find interesting is this graph:

<img class="showonplanet" src="https://blogs.kde.org/system/files?file=images//temp/taglib-development.preview.png">

That, aside from being my first (and likely only) time to play with the Perl bindings for Qt, was the number of changes going into TagLib before I moved it into KDE's CVS.  Every line is about a month (30 days).  The main difference between how I've hacked on TagLib vs. Tenor is just that I kept very quiet about TagLib before I was ready to release.  That's been my modus oparandi for most of what I have developed.  (For instance, I was already using JuK as my day-to-day player before anyone else knew it existed.)

However, just from a searching perspective, I'm pretty excited about Strigi.  I've talked a bit with the developers and almost all of the work that they're doing is orthogonal to the interesting parts of Tenor and the design of Strigi impresses me more than Kat did (both from an API and information retrieval perspective).  They've got multiple backends and building one that used the Tenor store would not be terribly difficult.

Hmm, I'll probably blog this as well since most of the activity on this article is from a couple days back.  Hopefully this clears up some of the current Tenor ambiguity.
<!--break-->