---
title:   "aKademy, world domination and unplugging (and heading to Chile)"
date:    2004-09-03
authors:
  - scott wheeler
slug:    akademy-world-domination-and-unplugging-and-heading-chile
---
So, I suppose I should chime in with an aKademy post too.  I did <a href="http://conference2004.kde.org/cfp-devconf/scott.wheeler-search.metadata.interface.elements.php">two</a> <a href="http://conference2004.kde.org/cfp-devconf/christian.fredrik.kalager.schaller.scott.wheeler-gstreamer.php">presentations</a> there -- the latter with Christian from GStreamer / Fluendo.

<b>Search / Metadata talk</b>

Both went well, in my opinion, though the first had a frustrating number of technical problems.  However it seemed to generate quite a bit of thinking and publicity.  It was entertaining to see it <a href="http://www.sapinfo.net/index.php4?ACTION=noframe&url=http://www.sapinfo.net/public/en/news.php4/Category-28813c6138d029be8/page/0/article/Article-21035412ef7ca0e10e/en/articleStatistic">show back up</a> for our news syndication thing at work.  It seems like of the talks at the conference it may have generated the most mainstream PR.  And also it was exciting to flesh out some more of the ideas throughout the week and to start to turn them into an API.  There are a lot of cool ideas there and I think that we're just at the tip of the iceberg in terms of the interface possibilities that are opened up by a "searchable web of relationships" inside the desktop.

<b>GStreamer talk</b>

The GStreamer talk also went well; we were notably less technical than the other multimedia presentations, which I thought was nice since the majority of the attendees weren't multimedia people.  As Christian noted in his blog, we probably should have prepared more of a demo, but well, that would have required one of us preparing it.  ;-)  I was just glad that we were able to pull together organizing the talk at the last minute.  And it was a reasonable demo that the conference was streamed using GStreamer.

<b>KDE Multimedia</b>

It was also really cool having the largest group of KDE Multimedia people assembled, well, ever.  For our largest get-together -- counting 2 GStreamer guys, 2 NMM guys and Leon from MAS we had 16 people out to gather for dinner.  We all got together for dinner a few times later in the week as well and had quite a bit of good dialog and talk about the future.  I'll be sumarising that on the kde-multimedia list this weekend.  Things seem promising for KDE 4 -- if for nothing else than we actually have people interested in <b>working</b> on KDE Multimedia for KDE 4.

<b>TagLib</b>

In more day-to-day terms I'm planning on releasing TagLib 1.3 this weekend...

<b>Unplugging / Vacation / Tengo que recordar mi español</b>

...and then I'm unplugging and heading off to Chile for three weeks.  Every couple of years I just have to completely get away from my life as a netizen for a while and, well, remember what it's like to be a normal human.  ;-)  You know -- no or limited email access, going out a lot, reading more, etc.  Though I am reasonably dorky since I intentionally planned this for a time in the KDE release cycle where I could disappear for a while and not miss anything major.  I'm also excited that it looks like one of my best friends (from the US) will be meeting me there which would be really cool as I haven't seen her in over a year.

<b>Linux Bangalore</b>

Also started bugging Taj about information on Linux Bangalore this year, which I'm looking forward to attending / speaking at this go around.  I'd really like to see India and one of my old college roommies is living in Bangalore these days, so it'll be cool to meet up with him.
<!--break-->