---
title:   "Tirade against GCC 3.3.2"
date:    2003-10-02
authors:
  - scott wheeler
slug:    tirade-against-gcc-332
---
...and Debian for <i>shipping</i> it.  Just dealt with another user having problems -- folks friends don't let friends use unstable Debian compilers.  Why does it seem that every distro seems to have to take their shot at shipping an unstable compiler?  (At least SuSE's has worked reasonably well for me.)

So consider this a warning of sorts -- GCC 3.3.2 -- though it hasn't been released, is presently the default compiler for Debian unstable.  It generates bad code.  I've noted this in at least two projects and it seems like there's a bug in the optimizer that does evil things.  So, first recommendation -- downgrade.  Second recomendataion -- don't use any optimizations.

That will be all.  Just a &quot;heads up&quot; of sorts for those who are unsuspecting victims of either bug reports, segfaults or other seemingly strange behavior.