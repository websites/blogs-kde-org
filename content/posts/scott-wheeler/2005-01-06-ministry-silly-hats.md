---
title:   "Ministry of Silly Hats"
date:    2005-01-06
authors:
  - scott wheeler
slug:    ministry-silly-hats
---
Yes, that's right folks, it's the Ministry of Silly Hats.  A privileged few of you have been subjected to my gests on the acquiring of an appropriate <i>sombrero de Tejas</i> and well, guess what Santa Claus, err, mom, brought me this year?

None other than a Stetson.  Lyle Lovett eat your heart out.  And Monty Python too while we're at heart eating.

Headgear was in this year, below are featured my brother's girlfriend, myself, my brother and finally my sister (who'd taken to amusing herself with a sash that I'd brought her from India).

<center><a href="http://ktown.kde.org/~wheeler/images/hats-800.jpg"><img class="showonplanet" src="http://ktown.kde.org/~wheeler/images/hats-400.jpg"></a></center>

At any rate, for the first time in months I'm actually back in Germany for a stretch of more than a few weeks, in fact I don't have any plans to leave home in the near future, so I should have a bit of time to get some JuK stuff written for 3.4.  There are still a lot of rough edges with the play queue, now playing thinger and assorted other new, shiny features that could use a bit of polish.  I'd also like to do a bit of profiling and see if I can get the bug count down to a reasonable number.

Additional cool discovery of the day -- there are a lot of reasonably priced paintings on eBay.  I suppose my college era posters may finally be able to go the way of the dodo.

Yeehaw!  ;-)
<!--break-->

