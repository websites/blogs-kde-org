---
title:   "Hack-o-rama -- TagLib, JuK, KDE, MusicBrainz, aKademy"
date:    2004-08-08
authors:
  - scott wheeler
slug:    hack-o-rama-taglib-juk-kde-musicbrainz-akademy
---
Ok, as usual it's been a while, but I've had a reasonably productive last couple of weeks -- I've been hacking on quite a few things and generally doing a good job of overextending myself as usual.  ;-)  Here's a run down of the last couple of weeks:

<br><br>

<b>TagLib</b>

<br><br>
    
<a href="http://developer.kde.org/~wheeler/taglib/">TagLib 1.2</a> was released, Munie and a few other projects were waiting on this release.  TagLib's use is still becoming more and more pervasive.  Beep seems to be in the process of switching over to TagLib as well and Milosz seems to be interested in expanding the C bindings to beyond the generic API.

<br><br>

<b>MusicBrainz</b>

<br><br>

    The folks from <a href="http://www.musicbrainz.org/">MusicBrainz</a> were rather patient and earlier this week I made good on my promise to port libtunepimp from the forked libid3tag that they were using over to TagLib.  Their old id3v2 reading code was GPL'ed and since libtunepimp is LGPL'ed it really made more sense to have LGPL'ed tag reading.

<br><br>

<b>JuK</b>

<br><br>

I also ported JuK over to libtunepimp around the same time and away from the external TRM tool.  That was frequently messed up by packagers and the switchover allowed me to fix 4 bugs along the way.

<br><br>

We've just finished up with the super-duper-extra-hard KDE 3.3 freeze.  CVS is read only at the moment.  Fortunately in the week before Michael and I managed to get JuK down to 9 open bugs -- most of which weren't fixable because of external requirements (i.e. string changes) before the next major release.  Since there was quite a bit of major internal reworkings in JuK between KDE 3.2 and 3.3 I'm glad to say that by release time things were in much nicer shape.  There were still a number of things that I just wasn't able to get done in the 3.3 time frame and we'll end up doing another KDE 3 based release at some point in the next few months.  If KDE ends up going for a 3.4 we'll wait for that, otherwise we'll probably do a separate release.  At any rate -- here are some of the things that <i>are</i> in 3.3:

<br><br>

<ul>
  <li>Major code cleanupds -- the internal design is much more squeeky clean these days.  There are still some dark corners, but the more core classes are much cleaner</li>
  <li>K3b integration for CD burning</li>
  <li>Search playlists are now editable</li>
  <li><i>Album<i> random play</li>
  <li>Much expanded DCOP interface</li>
  <li>Reworked and spiffier track annoucement popups</li>
  <li>Better support for batch based MusicBrainz lookups</li>
  <li>Lots of bugs and other boring stuff that I'm forgetting at the moment</li>
</ul>

<br><br>

    I'll get these stuffed into the <a href="http://developer.kde.org/~wheeler/juk.html">homepage</a> at some point before KDE 3.3 shows up.  I had a lot of things in the works -- but my mom showed up for a two week visit right as we slipped into the hard feature freeze.  The next release should contain some more goodies.

<br><br>

<b>Akademy</b>

<br><br>

I'm rather looking forward to aKademy this year -- somehow I got <a href="http://conference2004.kde.org/sched-devconf.php">scheduled</a> for talks on <b>both</b> days of the developers conference at 10:00 a.m.  Those who know me will appreciate the humor in this.  :-)  I've been messing around with a lot of ideas for my metadata based GUI talk.  There should be some fun stuff there.  I really need to get going a little bit on my demo code.  It sort of does stuff now, but isn't very impressive.  Also I'm going to be doing a <a href="http://gstreamer.freedesktop.org/">GStreamer</a> talk with Christian for the multimedia track.  The only problem is that Christian seems to be making his way around the globe now on an extended vacation and will be arriving back in Europe just a few days before the conference.  Hope we manage things.

<br><br>

<b>Work and Stuff</b>

<br><br>

I've also kind of started a big probably overly optimistic, but interesting push at <a href="http://www.sap.com/linux/">work</a> in the last week or so.  I've been collecting a lot of ideas and observations about things that work well in corporate software development and things that work well in the OSS community and am trying to combine some of both.  Specifically I've been really pushing for some of the better bits of OSS practices -- automated documentation, toolkit usage, active development lists, commit lists with real-time peer evaluation and all that good stuff.  We'll see if it takes off or flops, but after whining for a couple of years I felt like the least I could do was try to put some of the stuff into action.  I guess I'll know more soon.