---
title:   "InterTag, Continued"
date:    2006-03-13
authors:
  - scott wheeler
slug:    intertag-continued
---
This weekend I've done a bit more playing around with InterTag, a small application I mentioned <a href="http://blogs.kde.org/node/1842">recently</a>.

<a href="http://developer.kde.org/~wheeler/images/intertag-1.png"><img class="showonplanet" src="http://developer.kde.org/~wheeler/images/intertag-1-small.png"></a>

The goal is to provide something of a demo app for some of the Qt 4 Interview related stuff and as a bonus come up with some classes that can become central to JuK in KDE 4.  I've now got alternating background colors, a status bar and a very small file menu ("Open" and "Quit").

Right now I have:
<ul>
<li>Interview working with QAbstractModelItem and QTreeView subclasses</li>
<li>Alternating backgrounds in the QTreeView</li>
<li>Background loading of model information in a separate thread</li>
<li>Appropriate tree view headers</li>
</ul>

I still want to get:

<ul>
<li>Inline search</li>
<li>Inline tag editing</li>
<li>Automatic scaling of column widths</li>
<li>Column sorting</li>
<li>Column hiding</li>
</ul>

And then I'll basically consider it done; those are kind of the essentials for list-centric apps.  I'm not really trying to make a real app, but something that will be just a couple thousand lines of code that other application developers can look at and also that will help me decide how to fold some of this stuff into kdelibs.

Probably with the next update I'll put a tarball along with it.  For now it's just in my local CVS.
<!--break-->