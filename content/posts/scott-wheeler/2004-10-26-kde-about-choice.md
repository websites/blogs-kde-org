---
title:   "\"KDE is about choice\""
date:    2004-10-26
authors:
  - scott wheeler
slug:    kde-about-choice
---
I swear, if I hear this or "Linux is about choice" or "Open Source / Free Software is about choice" or "My life sized Richard Stallman blowup doll is about choice" one more time, somebody's gonna get an ass kicking. People, let's step back and look at the absurdity of this statement.

I'm not going to say that you couldn't make something that <i>is</i> "about" choice.  But I can say that it would probably suck for any useful purpose.

Let's face it.  We're not about choice.  We're about functionality (ok, maybe not the RMS doll), or at the very least expression of some ideals that are more meaningful than "choice".

<i>Choice</i> isn't a bounded thing, and taken to its logical extreme and applied to software design you simply arrive at a logical recursion where you're building choices on top of choices on top of choices and so on.  You could (and some have) conceivably have an application that focuses more on how it's set up than how it works.  It's easy to get to a point that you've forgotten what the application is <i>for</i> because it seems to be drowned out in the noise of overloaded functionality.

Again, my standard line:  "The ideal interface makes everyone happy at all points in their time spent using it; configurability is a poor substitute."  Now, of course we can't design the "ideal" interface, and options are all that we're left with in some cases, but that's hardly a license to target the compromise instead of the goal.
<!--break-->