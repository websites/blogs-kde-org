---
title:   "JuK-iness, the one tagging library to rule them all, annoying body parts and family visits"
date:    2004-07-01
authors:
  - scott wheeler
slug:    juk-iness-one-tagging-library-rule-them-all-annoying-body-parts-and-family-visits
---
<p><b>JuK</b></p>

<p>Well, there are a number of new recent things in the JuK world.  A couple weeks ago I finished a major rewrite of a lot of the internal components that's made a lot of things easier, plus it makes working with the code a lot more sane.  I completely refactored some of the older classes, moved more towards some stripped down interfaces for use in internal APIs and other goodness.</p>

<p>Michael Pyne has also joined on in the last month or so and has been implementing a number of features -- most notable are K3b integration for burning audio and data CDs and we worked together on enhancing the DCOP interface.</p>

<p>I'm also continuing on implementing the Audio CD integration -- ideally I'll get ripping and playing working.  Right now I've got the CDDB lookups workings and it's inserting stuff into the playlist, but there's still a lot of work to be done.  Unfortunately I've assumed in too many places that playlist items have files behind them, so I'll have to work around that.</p>

<p>Anyway -- since the image functionality doesn't seem to be working in the updated blog software -- here's a <a href="http://ktown.kde.org/~wheeler/images/juk-cdplayer.png">screenie</a>.</p>

<p><b>TagLib</b></p>

<p>TagLib is also becoming increasingly popular -- there have been a pretty impressive number of external projects from XMMS plugins, to Mono applications, to other KDE stuff that have picked it up and seem to be pretty happy with it.  This is nice because  I feel like it kind of validates my desire to not want to release it as part of kde-multimedia.  I'm hoping to get a 1.2 release out this weekend which will natively support album covers, volume adjustment and loads-o-bugfixes.</p>

<p><b>Sadly, you can't mail order replacements...</b></p>

<p>...and my annoying body parts -- well, at the moment my right arm.  I'm suffering from <i>carpal tunnel syndrome</i> for the first time and have had pain and numbness in my upper right arm for the last few days.  It's been building for the last couple of weeks, but it's finally gotten pretty annoying as of the last couple of days.  I've been trying do some basic ergonomic improvement of my work environment which seems to help some, but still after a few hours typing / mousing becomes painful.  This is a rather annoying setback both at work and for KDE things.  I guess 15 years of using a computer quite a bit and 10 years of bass and guitar finally caught up with me.  I'm probably going to have to take a few days with no computers to try to let things heal a bit.  But that fortunately fits well with the next item...</p>

<p><b>Saying hi to the folks</b></p>

<p>I just found out about 2 weeks ago that my mom is going to show up to visit next Monday.  This will be her first time in Europe since I moved to Germany 2 years ago and only her second time in Europe since the eastern-ish countries opened up, which she hasn't visited before.  I think next Friday we'll probably head out and spend a few days in Budapest and Prague, which I'm looking forward to.  When my parents got divorced a while back she went back to Uni and just graduated (at 47) a few months ago; she claims this is her graudation present to herself.  ;-)  This will also fit well with taking some time off from the computer.</p>

<p>As usual, a huge update before undoubtedly a long bout of silence.  :-) </p>