---
title:   "\"Now Playing\" Bar in JuK"
date:    2004-11-10
authors:
  - scott wheeler
slug:    now-playing-bar-juk
---
So, with some of the new stuff for displaying cover art, I was inspired to hack out a "now playing" bar.  I suppose a picture is worth a thousand words, so here goes:

<a href="http://blogs.kde.org/node/view/711"><img class="rapemewithachainsawthanks"  src="https://blogs.kde.org/images/thumbs/thumb_f843564de9ee1eb52e86a6cd234d9b7a-711.png" /></a>

The links for the artist and album will eventually go to a filtered playlist.  Clicking on the image pops up the full sized image.  There's also a short history there which again, eventually will link back to the previously played items.

The stuff was written so that more things can be added and so that they potentially can be switched out.  We'll see how motivated I am to work on such in the near future, but this isn't bad for a night of hacking.
<!--break-->