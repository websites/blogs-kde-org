---
title:   "Flavor of the Month"
date:    2004-04-21
authors:
  - scott wheeler
slug:    flavor-month
---
So, the week or two I've had a little fun hacking up a Icecream Monitor mode that's more like the Teambuilder monitor.

[image:438]

Teambuilder was always cool conceptually, but in practice never really worked all that well for me.  Icecream actually works but I wasn't terribly fond of the monitor a few weeks back.  The only two options were the listview mode -- which just listed the jobs as they happened without any nice visual indicators -- and the Gantt View mode, which takes about 50% of my CPU just for painting.  So, the above is what I've gradually hacked up.

The initial version didn't support scrolling (so it was really bad for the guys at SuSE where there are a lot of nodes -- it would appear larger than the screen) and didn't display things properly on SMP machines.  I fixed both of those today and also added color-coded host stuff so that it's more quickly visible which jobs are compiling on which machine.

I'd like to add something to indicate the machines' loads, but I currently don't see support for that in the monitor API that I'm implementing.  Maybe that will come along later.

After a pretty intense week of JuK hacking and a frustrating day at work it was nice to take a bit of a break and play with the new toy.  :-)