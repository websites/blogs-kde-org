---
title:   "Human Interface Guidelines (Uhm, the other ones.)"
date:    2005-05-14
authors:
  - scott wheeler
slug:    human-interface-guidelines-uhm-other-ones
---
Those who have been subject to my rants on this topic before are no doubt familiar with my views, but as they've mostly been on IRC and not particularly structured, and there's been some prompting in the KDE community in the last couple of days, so here goes.
<br><br>
The "sexualization" of women in our community really has to stop.  I really get sick of seeing every time a female contributor comes around, or everytime there's an article on the Dot that refers to our female contributors that it's just a matter of time before some jackass decides that it's the right forum to ask if they're single or make other comments that are just there because they're female.
<br><br>
<b>Contributors are important</b>
<br><br>
Contributors are important.  We need them.  We don't need male contributors or female contributors, we need contributors period.  Despite their deficit in numbers, some of the most important members of our community are female.  And who cares what gender they are, really?  They're important contributors, and we as a community need to insure that we're not treating one segment of our community differently just based on their gender.
<br><br>
<b>The KDE social landscape</b>
<br><br>
In a normalized social environment -- one that's fairly well mixed between males and females -- this isn't much of an issue.  In such an environment flirting and such things are a fairly normal, such is afterall our nature.  We are sexual beings.  However there are a few ways that KDE diverges from this.  First, the majority of people in the KDE community are male.  This in and of itself is not a problem.  Some of the thought on the social foundations of engineering and sciences being largely male driven in western society is in fact interesting, but out of scope for the moment.  However, this is coupled with a significant subset of our community being less than great with their social skills.  Specifically, a lot of the members of our community simply haven't really interacted all that much in usual social settings with members of the opposite sex and quite frankly haven't got a clue how to go about such.  The result of this tends to be anywhere from overt sexual objectification to accidental inappropriateness.
<br><br>
At some point we may see things start to level out -- see, that's the interesting bit.  If we aren't asses about this stuff, and we treat contributors more or less equally independant of gender, that will actually help things get closer to a balance with time and then we don't have to actively compensate for our present social dispositions.
<br><br>
<b>It's all fun and games until somebody loses a contributor</b>
<br><br>
And to be clear, while some of the lighter bits of this may seem amusing to some of folks the first go around -- like, I mean, being told you're cute in the right context isn't the worst thing in the world -- it does get old, mostly because of the baggage that comes with it.  The problem is that often along with that comes an objectification that obscures what the person is actually part of the community <i>for</i>.  Sure, having people think you're cute might be fun, but not if it's getting in the way of being taken seriously (or too seriously as can sometimes be the case) when trying to actually do something important.  And I have known people who have actually left the community over this.
<br><br>
<b>Let's be pragmatic</b>
<br><br>
For those male members of our community that feel the need to try to pick up women via the Dot, well, I hate to break it to you but statistics are not on your side.
<br><br>
Here's a little secret -- there are women all around.  Many of them are even single.  And if you can come up with something a little more subtle than saying as if they weren't there, "She's cute.  Is she single?" they'll actually talk to you.  No, really.  This happens all the time.  I've devised a step-by-step plan that may help some of you and I presume is easier to implement than Theobroma's scheming:
<ul>
<li>Find the largest button on the case of your computer.  It's probably labeled something like, "Power."</li>
<li>Press it.  (Not now, stupid, wait until you've finished reading the list.)</li>
<li>You might have to wait about 3 seconds.  Be patient.</li>
<li>Stand up.  Leave your house / dorm room / apartment.</li>
<li>Find some sort of social gathering; again, the local LUG probably isn't statistically your best bet.  There are lots of more statistically promising options though:  bars, clubs, heck, go protest something (Yay activism!  Yes, it's more fun when it's not just bitching on Slashdot.), go to a concert, a religious gathering if that's your bag, use your imagination here.</li>
<li>Talk to people.  No, not just those of the opposite sex.  You see, people run in packs.  Having a social circle works wonders.</li>
<li>After a few iterations of the above, you may find the need to come up with a lame excuse to ask someone out.  Don't worry, most of the lines for this are lame, that's one of the rules of the game.</li>
<li>It probably won't work a lot of the time.  But fear is not lost.  As Lyle Lovett says, "There's more pretty girls than one."  (I suppose it takes a left-winger Texan to work in a country reference to a pseudo-feminist rant.)</li>
</ul>
<br><br>
The summary is you'll be amazed what happens when you actually treat people as well, people.
<!--break-->