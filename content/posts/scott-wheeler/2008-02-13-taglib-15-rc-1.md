---
title:   "TagLib 1.5 RC 1"
date:    2008-02-13
authors:
  - scott wheeler
slug:    taglib-15-rc-1
---
The <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a> 1.5 RC is up.  There have been a huge number of changes since 1.4 (two years ago) and even a number of changes since last week's beta.

I've also updated the documentation on the web server, put the new sources up and also put up a Mac OS Framework.  The real release (or shortly thereafter) will also contain a Windows build as this is the first TagLib release to officially support Windows as well.

Major (file corruption, crashes) or trivial bugs may still be fixed fixed before the release.  If you're using TagLib in a project please consider taking some time in the next week to try out the new RC, valgrind your app with it, etc.  If no major issues are discovered within the next week in approximately one week this will be renamed to TagLib 1.5 and released.

I'll prepare a change log some time before the real release (which I'll do another annoucement for).

Doc suggestions should go to the mailing list (or comments), bugs to the bug base.

<ul>
<li><a href="http://developer.kde.org/~wheeler/files/src/taglib-1.5rc1.tar.gz">Source</a></li>
<li><a href="http://developer.kde.org/~wheeler/taglib/api/">Docs</a></li>
<li><a href="http://developer.kde.org/~wheeler/files/TagLib.dmg">Mac OS X Framework</a></li>
</ul>

<!--break-->