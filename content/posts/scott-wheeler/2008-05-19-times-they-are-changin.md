---
title:   "The Times They Are A Changin'"
date:    2008-05-19
authors:
  - scott wheeler
slug:    times-they-are-changin
---
<img src="https://blogs.kde.org/files/images//businessbooks.jpg" alt="business books" class="showonplanet"/>

There are a few scattered updates in the world-o-wheels of late.  The biggest of which, as a number KDE folks are already aware is that I'll be leaving Native Instruments, where I've been for the last couple of years and starting my own company with a friend or two rather soon.  I'll post a link once we're to the point of launching a public beta.  It's not desktop software, and it's not a consulting service, but this will mean that my primary (professional) development platform will be Linux once again.

I've been considering founding a company for a long while, and having recently been granted permanent residence in Germany it's now legally possible.  I've been going crazy the last couple of months trying to sort out all of the technical, financial and administrative details that will go into getting that off of the ground.  Above is the table next to my bed.

In other news, coinciding with a meet-up for startup founders on the same weekend in Prague, I'll be around the upcoming Ubuntu Developer Sprint on Thursday and Friday.  I'll also be around some of the time at LinuxTag in Berlin the following week.  There's a reasonable chance that I'll make it out to Akademy this year too.  My specialty at conferences seems to be taking embarassing photos, so I'll try to do my worst.

My current employer is also in the process of switching over to using TagLib and so last week as one of the tasks that I wanted to finish up before I'm away from there I implemented, per request, tagging and audio properties for AIFF files.  More or less for free along with that came a generic parser for RIFF formats.  There's been a lot of traffic on the TagLib development list of late, and once life slows down a little bit it looks like it might be time to do a quick 1.5.1 or 1.6 release and then go for a 2.0 push.

<!--break-->