---
title:   "Calligra Words and the road to release"
date:    2012-02-16
authors:
  - boemann
slug:    calligra-words-and-road-release
---
The Calligra Suite recently released the 7th beta, and mostly because Calligra Words is still not ready. Building a new application from scratch takes time.

Until around October the focus was on writing a new text layout system that could provide the rich text support that any user demands these days. We were not able to reuse the code from KOffice as it was simply not up to the job. So this took a lot of effort.

And then just as we thought we only had to do final touches before we could release we found out that the styles subsystem we (myself included) had created in the KOffice days had huge deficiencies as well. Basically the user would lose data related to the style hierarchy. It was a complete mess and took 2 months to sort out.

At around the same time we found that the lists handling were a complete mess as well (again partly to blame myself, but we learn as we go). So we had to sort that out too, which took about the same two months

Then in December we again tried to do some of the final touches to get the release ready. This time focusing on undo/redo of shapes, and how the shapes are anchored to text. That turned out to take a couple of weeks.

In January we looked at undo/redo for normal text editing and to our horror it turned out to be another can of worms, which we havn't solved yet. It's related to the purpose of how the code has gradually changed. We now need to rethink the structure of it. On February 19th I'll fly to Munich for a 4 day mini sprint, graciously sponsored by KDE e.v. to sort this out. At least we have a plan for how we will solve it.

When I get back home we'll probably tag an 8th beta.

But during all this fixing we have also implemented a lot of new features. Table of contents, editable footnotes, bibliography, new statistics docker, a great styles combobox, a novel new approach to the ui in general.

So all in all we have had great progress while also making sure most things work. Obviously we have not solved every little bug, as no application is ever bug free. But at some point we do need to release and that point is coming closer and closer. So stay tuned.
<!--break-->
