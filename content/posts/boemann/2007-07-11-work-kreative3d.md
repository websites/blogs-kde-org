---
title:   "Work on Kreative3D"
date:    2007-07-11
authors:
  - boemann
slug:    work-kreative3d
---
I've started to work more and more on Kreative3D. Although it's not useful for anything right now, I expect that I can have 2d sketching done within a month or two. For those of you who don't know anything about parametric solid modelling a sketchplane is a plane in 3d space where you can draw on. And sketching is the process of drawing on that surface.

You draw with lines and arcs and you then assign constraints to those. A constraint could be that two lines should always stay parallel or stay tangent to an arc or something.

<img width=500 src=http://img400.imageshack.us/img400/7358/kreative3djuly2007jc1.png>

As you can see from the screenshot: The sketchplane is there and I can draw lines, but that is just about it. So what I plan to do in the following months from now is to add tools that create arcs and lines in various ways. I've ripped the tool system from KOffice flake and made a 3d version out of it, so it's actually pretty easy to make new tools. The hard part will be implementing constraint and a solver to make sure they are followed.

I'll also be working on the view tool (the only other tool currently implemented). I can already postion the viewpoint anywhere around the plane so I can see it from any angle, but the view tool needs to be much more powerful to be really usable.

<!--break-->