---
title:   "Introducing KoSliderCombo - a custom yet themed widget"
date:    2007-04-06
authors:
  - boemann
slug:    introducing-koslidercombo-custom-yet-themed-widget
---
I've committed a new widget to KOffice which is a combobox-like widget consisting of a numeric input and a slider that pops up. The really neat thing about this new widget is that it's themeable right out of the box because I've reused QStyle to draw it.

[image:2756 width=440]

For the API I've tried to do thins like in QDoubleSpinBox. It still needs to mature a bit, but it's already usable. If people are interested I would be more than willing to move it to kdelibs