---
title:   "Calligra Words is not a fork of KWord"
date:    2012-02-16
authors:
  - boemann
slug:    calligra-words-not-fork-kword
---
Pretty often I hear people say that Calligra Words is a fork of KWord. As a maintainer I tell you this not true. Sure we have ripped about 20% of the code from KWord but even that has been dismantled and reassemble in a new way.

That doesn't constitute a fork. Calligra Words is effectively a new word processor. We have more in common with Stage than we do with KWord. We even started the development of Words by disabling what was KWord, and then we built Words by slowly adding functionality .
<!--break-->
