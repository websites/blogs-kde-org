---
title:   "Reworked scale dialog in Krita"
date:    2008-05-08
authors:
  - boemann
slug:    reworked-scale-dialog-krita
---
[image:3453 size=original nolink=1 node=3453]

I've worked a bit on the scale dialog for Krita. Neither the old one from the 1.x series nor the new one in current KOffice2 alpha releases were good enough. I've mailed a bit with Ellen which only confirmed that it is indeed a difficult dialog to get right.

In the end I reordered some items, so I hope it's a bit easier to figure out how to use it. I think it has become quite good, but judge for yourself. So much cool stuff coming in KOffice 2.0
<!--break-->