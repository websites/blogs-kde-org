---
title:   "Kustodian - a taskbar and quicklauncher combined"
date:    2009-08-09
authors:
  - boemann
slug:    kustodian-taskbar-and-quicklauncher-combined
---
I'd like to introduce a little pet project of mine: Kustodian, which some people would call a ripoff of the mac dock or windows 7 taskbar. But I maintain it's a thing of it's own, but it indeed has some similarities.

<img src="https://blogs.kde.org/files/images/snapshot1_1.png">

The idea is that when you click one of the icons it launches a new application. The number next to the icon shows how many instances are already open, and when you hover such an icon a dropdown/popup immediately appears with thumbnails of the windows. When you click one of those thumbnails you bring the window to front/minimize/restore just like you are used to.

Some icons/applications on the Kustodian are marked as favorites and appear even when there is no number next to it. If you launch and application that is not already there it will appear and you should be able to mark it as favorite.

Now you might wonder how much of this already works. Well it's a real screenshot so the icons appear and disappear, and there is a list of hardcoded favorites, which can launch new instances. The popup is however blank at the moment, so I'd like to get some help from a <b>plasma person who thinks this project is worth helping out</b>. It follows that you cant actually manage your windows with this thing yet, although I've managed to use it as a day to day taskbar replacement for several months already

Update: I've committed to playground/base/plasma/applets/kustodian, but don't hesitate to contact me on irc #kustodian.

<!--break-->
