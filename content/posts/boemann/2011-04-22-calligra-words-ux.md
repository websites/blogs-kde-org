---
title:   "Calligra Words UX"
date:    2011-04-22
authors:
  - boemann
slug:    calligra-words-ux
---
Hot on the heels of the <a href="http://dot.kde.org/2011/04/15/first-calligra-sprint">Calligra sprint</a> in Berlin I attended another sprint in Berlin. This time the <a href="http://agateau.wordpress.com/2011/04/21/kde-ux-2011/">KDE UX sprint</a> where we discussed many interesting topics (see eg. Aurélien’s <a href="http://agateau.wordpress.com/2011/04/21/kde-ux-2011/">blog</a>)

The two sprints tied nicely together for me as in the Calligra sprint we had a presentation of some usabillity testing on our current ux. This testing was done by Anna, a usability student from Finland. This confirmed what we already knew: Our current ux has quite a number of problems. So at the UX sprint I talked to Celeste, Thomas Pfeiffer, Björn Balazs and Peter Sikking on how to come up with a better user interface.

The discussion centered around our tools system, and accompanying dockers, and I have a proposal for how to make it better so stay tuned over the next couple of months where I'll try and implement it. There is no screenshot as UX design is best done by designers and not the entire "committee of internet users". But as soon as it has been through a number of iterations of development and review by the designers, I promise I will come up with some interesting screenshots.

Oh and I can't help but also note that the huge textlayout rework is already quite stable, and may very likely be merged in time for the first Calligra snapshot May 4th.
<!--break-->
