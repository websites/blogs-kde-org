---
title:   "Mini sprint on undo in Calligra Words"
date:    2012-02-20
authors:
  - boemann
slug:    mini-sprint-undo-calligra-words
---
I'm at PierreSt's house in Munich for a minisprint trying to fix the undo system in Calligra Words. I arrived Sunday evening, and we started immediately discussing how we would solve the issues we are facing. Luckily we seem to have the same idea and understanding of the issues.

Today, Monday, we have started hacking. But let me explain a bit about the problems we are having:

Words is using QTextDocument for storage of the actual characters, but we embellish this with all sorts of homegrown stuff, for inline images, change tracking etc. QTextDocument has it's own undo stack but since Words can do those other things too we need to combine that with undoCommands of our own.

It's when we need to make macro commands that combine our commands with those of QTextDocument that we run into trouble. Partly because the framework wasn't taking subcommanding into consideration, and partly because no one realy understood what went on so we used the framework wrongly. We are now correcting both of these issues.

If all goes well we might create a Release Candidate of Calligra in 2 weeks or so.
<!--break-->
