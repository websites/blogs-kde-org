---
title:   "Dear Aaron,"
date:    2004-09-18
authors:
  - datschge
slug:    dear-aaron
---
first of all I hope you are aware that your heading "a lesson to all" sounds like pure arrogance to everyone who took part in these discussions but is still not considered in any of your reasonings. 

The discussions I was referring to before were started by Frans English in early-mid January this year on the usability list and kopete-devel list. Guess what he proposed? Yes, he wanted to remove Ctrl-Enter and Enter to stay as sole way to send messages in both single- and multiline text field, that what you just realized without even proposing it first. Now guess how the people on the mailing list generally reacted? Certainly less one-sided that you appear to think to be able to handle the issue now.

A couple of points you didn't care to consider or instead chose to outright dismiss so far:

Consistency in the use and handling of single- and multiline text fields. You dismiss all concerns here as purely "technical" and not related to the "use case". This approach is a great service at proving right all the people who claimed that "developer" and "usability expert" are "two kind of humans". And of course it doesn't add anything to the discussion (just look at your second last blog entry's title, " it's spelled "use case" ", usability is not about treating users as idiots, and neither should you with members in discussions, pretty please). Also putting the term "use case" above consistency without elaborating and backing this any further flies straight in the face of any usability effort, since after all, if it's that easy to break consistency, why even bother? Let's end all our discussions with our fitting favorite "use case", way to go indeed! But I digress, let me start what actual issues were ignored so far.

Firstly the most blatant one: you change the default in a bugfix release. Why is this wrong? Backported bugfixes and features are always welcome, with the big exception that they may not introduce new bugs and may not change behavior without the user's consent. The backport of the ability to set Enter for multiline send was a new feature which per se neither introduced a new bug nor changed the existing behavior. Backporting the Enter as default change however does just that, it changes he existing behavior without the user's consent. Everyone saying Boudewijn will be the only one dissatisfied about being patronized in this case is shamming.

Secondly the differentiation between singleline and multiline is there for a reason which is not at all technical. An editable text field with a single line, where content longer than the visible area are scrolled outside of the either end, communicates to the user that it can contain only one item at a time and that the string or message can be applied/executed/sent right away. This is what combo lists like the address bar and chat room systems (=short message talks) like IRC are about. An editable multiple line text field, where content is wrapped at the border, communicates to the user that it can contain longer text which can be tweaked to some degree, is usually review first and contains line breaks. This is what usual text fields are about, it applies to all multiline text areas on websites, it applies to all common editors, it applies to emails and it applies to instant messaging which was originally crafted as email alike system with the added feature of sending and receiving messages in real time if the contact in question is online at the same time as well.

In the mailing list discussion  mentioned above a possible change from Ctrl-Enter to Enter was consequently criticized for following reasons:

* Keeping multiline with word wrap while having Enter for sending breaks consistency with the whole idea of differentiating between singleline and multiline at its core (same for Ctrl-Enter for singleline).

** This includes irritations about being unable to break lines while the multiline text field screams to the user that he can review and tweak it. Unlike the consistent case where the user can simply use the send button the inconsistent solution for clueless users completely takes away the ability to break lines.

** This includes irritations about accidentally sending a message, naturally at a point he wanted to add a line break, at which point the text is surely that long already that he'd ideally wanted to review it first before sending. All those are expected attributes by multiline text fields which are unfulfilled by breaking consistency.

Please keep in mind that I purposely left out the whole "familiarity" bias since the users familiarity with non-KDE platforms and applications is not even remotely related to how KDE has to be consistent with itself by default. KDE by default has to offer a system which doesn't surprise where it shouldn't (ideal usability), and that's exactly the reason for being consistent with itself. While already given familiarities with differing and contradicting preferences can give us a hint what optional features to offer, they shouldn't lead us to think that emulating an existing solution takes preference over staying consistent. You may ask why, here's the answer: Imagine we would take the approach of by default emulating other systems, this for fulfilling existing expectations built through existing familiarities with non-KDE systems, where would we be able to draw the line and not end up as a copycat with some additional/missing tweaks?

KDE can and does do better already. Many existing observations showed that KDE's focus on consistency makes the system easier to grasp for new computer users. For this and the above reasons I request that the change of default key for sending in multiline text fields in Kopete be reverted to the default suggested by KDE's style guide which all applications within KDE's CVS including Kopete should comply with.

Thank you for your attention.

Links:
http://lists.kde.org/?t=107361127200005
http://lists.kde.org/?t=107367188500006
http://lists.kde.org/?t=107416005600003