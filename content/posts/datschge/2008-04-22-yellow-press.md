---
title:   "Yellow Press"
date:    2008-04-22
authors:
  - datschge
slug:    yellow-press
---
There are plenty sources for KDE relates feeds and news, but reading them through the usual neutral feed readers or even on separate sites takes the fun out of the less serious ones. Also the common separations between different parts of KDE (internal devs, external dev, artists, usability experts, bug hunters and pr people) is reflected in the different community cultures (mailing lists, bko, dot, wikis, kde look/apps/files, other forums) which usually never mix (dot comments are probably the closest to that happening). I always thought putting them all together onto one (naturally huge messy) page should give an interesting overview, similar to the yellow press' collection of often useless(ly funny) article collections. As I recently found out about simplepie I decided to test myself how far I would be able to go with an automated yellow press parody of KDE news, all while both making good use of my limited webspace (with "unlimited" bandwidth) as well as satisfying my rather constant need for KDE tinbits from everywhere. The result after a couple of hours today <a href="http://kde.datschge.de/">is here</a> (warning: brutal use of bright colours).