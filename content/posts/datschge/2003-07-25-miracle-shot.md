---
title:   "\"Miracle Shot\""
date:    2003-07-25
authors:
  - datschge
slug:    miracle-shot
---
Update on <a href="http://bugs.kde.org/">bugs.kde.org</a>:

The new script <a href="http://bugs.kde.org/old-bugs-statistics.cgi">old-bugs-statistics</a> is now up and running.<!--break--> It includes links to "bug lists" containing all the possibly outdated reports, making the former "Idle Reports Search" on the front page redundant. I consequently removed it and cleaned up the front page a little. I'm pretty sure that old-bugs-statistics combined with <a href="http://bugs.kde.org/weekly-bug-summary.cgi">weekly-bug-summary</a> and all their included links fulfil most (useful) statistic and bug hunting needs. If not let me know.