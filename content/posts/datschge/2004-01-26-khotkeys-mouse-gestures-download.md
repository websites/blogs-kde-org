---
title:   "KHotKeys mouse gestures for download"
date:    2004-01-26
authors:
  - datschge
slug:    khotkeys-mouse-gestures-download
---
Nothing big, I just created <a href="http://datschge.gmxhome.de/khotkeys.html">two importable khotkeys mouse gesture settings for download</a> mimicking some of the Mozilla and Opera mouse gestures. Feel free to suggest and contribute more. Also a couple of Mozilla/Opera mouse gestures are not realized yet since I don't know which way a mouse gesture would ideally affect an element (ie. link, image etc.) it is hovering.