---
title:   "KDE Branding"
date:    2004-02-25
authors:
  - datschge
slug:    kde-branding
---
The following post is mostly a response to Aaron's comments in his <a href="http://blogs.kde.org/node/view/355">latest blog entry</a>. It's basically about the reason why I like KDE and why I do the contributions I do, so it might be an interesting albeit highly theoretical read.<br>
<!--break--><br>
I think Aaron is approaching the whole issue from a very traditional (that is imo outdated) angle. While I understand what Aaron means I think many of his differentiations are artificial and, while they may reflect their commercial usage today, do not really fit to KDE but rather reflect the classical route how commercial branding is done.<br>
<br>
I'm talking about computers as a commoditized tool where software is ultimately a tool offering specialized features adapted to the very specific situation in which a computer is used. Defaults do not matter here, the flexibility to adapt to any specific situation is what is important. Wrt graphical environments KDE is currently imo an unrivaled unified solution.<br>
<br>
Vendors are users as well for me, they are service companies ideally offering specialized desktop configurations using KDE adapted to the very specific situation for which a given consumer want to use a computer. They, just like any user, need to be educated about the possibilities there are in KDE. Defaults do not matter here, the flexibility to adapt to any specific situation is what is important.<br>
<br>
Branding is for me a mix of public awareness combined with (often pseudo-)education to make a brand stick in people's head as worthy to remember and consider or even superior looking compared to anything else less "branded". KDE imo does not need to resort to pseudo-education (a.k.a. propaganda imo), it already has much to offer that we can and need to educate people (and companies) about.<br>
<br>
KDE as a project should not try to limit itself to a specific audience, may it be new, intermediate or expert users (which are all highly unspecific audiences which still differ highly depending on technical and cultural background anyway etc.) or even service vendors which resell service solutions based on KDE. Defaults are nice for showcasing features, but don't hold your breath for a single set of defaults to be magically able to suit all audiences and specific situations possible, this is not going to happen. Here again flexibility is the key, and education about the flexibility empowers users to make use of computer in new ways (which again opens room for commercial services for others where there is a market demand).<br>
<br>
Defaults are ultimately nothing but a limited showcase of features. KDE does not have a specific audience to stick to, therefore it should not stick to specialized defaults either. KPersonalizer already shows a start of how KDE can quickly adapted to different special cases depending on a users personal expectations and needs. So instead tweaking controversial defaults future work would ideally be done to
<ul>
<li>Make specific features easier to edit for advanced users (think eg. right click menus)</li>
<li>Educate users and companies more than ever about the possibilities KDE offers, and encourage them to make active use of them</li>
<li>Extend KPersonalizer to offer more specialized adaptions (I'm personally considering to create a locked down KDE setting scheme mimicking the feature range of GNOME)</li>
</ul>
Please note that while I consider usability as very important I didn't mention it so far due to the fact that usability is not a universal feature but depends on the actual use cases. Something which is highly useful in one case might be complete overkill in another, and vice versa. So usability improvements would mostly be needed within and introduced to specific schemes available from KPersonalizer. (This of course doesn't exclude the possibility that there actually are areas which need usability improvements regardless of the use cases, but those will need to be solved by consent finding and not by adapting a special use case solution for all other use cases.)<br>
<br>
Thanks for taking the time reading this all. =)