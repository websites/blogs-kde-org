---
title:   "\"Raising a Curtain\""
date:    2003-07-23
authors:
  - datschge
slug:    raising-curtain
---
Hi, here's <a href="http://www.google.com/search?q=datschge">Datschge</a>.

I guess I should introduce myself first, but there's really not much to say.<!--break--> I'm a big fan of Motoi Sakuraba and am crying me myself a river for not being able to visit his live concert last Saturday. -_- I'm putting my oar in into KDE for about one year now. Being an ultimately useless non-programmer I've been digging for parts where I can easily teak ("improve") stuff while hoping to reach my goal, making it easier for "outsiders" to effectively contribute to KDE. I started being vocal during the discussions which led to the kde.org redesign, added noise to the usability list, then decided to pick <a href="http://bugs.kde.org/">bugs.kde.org</a>, something noone else seems to like to tackle, tweaked stuff <a href="http://bugs.kde.org/query.cgi">here</a> and <a href="http://bugs.kde.org/weekly-bug-summary.cgi">there</a>, added an hopefully easy to grasp <a href="http://www.kde.org/support/">supporting KDE page</a> which I hope will be translated and localizated into other languages/localizations soon. Right now I'm "working" on (I'd rather call it 'keeping myself distracting from') writing a decent help page for bugs.kde.org and putting together a pseudo "KDE HIG" informing people (myself!) about all the build in consistency features KDE offer for ages already. Right now I'm totally excited since I got a hack of weekly-bug-summary for showing the amount of old reports and the ratio of old and all reports per product to work in my local bugzilla installation. Thanks to Steve for the inspiration. ^_^

Hm, I think I wrote a little too much here...