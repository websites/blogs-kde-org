---
title:   "Plasma widgets on Maemo5"
date:    2009-10-10
authors:
  - mkruisselbrink
slug:    plasma-widgets-maemo5
---
<img src="http://93.157.1.37/~marijn/6822_100914886597296_100000363546757_21053_2747415_n.jpg" width="400" height="240" align="right"/> Yesterday nokia gave away 300 pre-production n900 devices to all attendants of this years Maemo summit in Amsterdam (in the form of a six months loan, after that they'll have to go back to Nokia). I'm also attending, so I also got one. Deciding what the first thing to port to a new device is is always hard, but in the end I figured that something with plasma might be nice. As maemo5 makes it possible for home-screen widgets to be part of separate processes, I figured it might be possible to adapt plasmoidviewer to act as a simple program to put any type of plasma applet on the normal maemo desktop (actually, I think it was somebody else that suggested this, I just don't remember who it was). So after several hours of hacking (and a lot more hours of compiling Qt and various parts of kde (btw, the just released Qt 4.6 maemo5 technology preview is missing some essential bits like for example qdbuscpp2xml), I managed to figure out just exactly how to get the window to appear on the normal desktop as a widget. At first this didn't look to pretty as you can see in this screenshot:



But after several more hours of hacking and trying to figure out how transparency works in X11, I even managed to get nice translucent applets. Also I figured out how to hook up the normal maemo5 widget configuration system to display the correct configuration dialog when you click on the configure button on one of these plasmoids. So with in the end maybe 20 lines of code, I got a rather good working implementation that makes it basically possible to have any plasmoid you might have on your normal kde desktop, also on your maemo5 home screen. One (somewhat major) problem with the current implementation is that it is not possible to resize widgets, but as far as I can tell that is mostly a limitation of the maemo5 desktop widget system, so I'm not sure if there is anything I can do about it from my side.

<img src="http://93.157.1.37/~marijn/Screenshot-20091010-233039.jpg" />