---
title:   "Photos of the GCDS"
date:    2009-07-23
authors:
  - mkruisselbrink
slug:    photos-gcds
---
<a href="http://mkruisselbrink.smugmug.com/gallery/8987670_TGgB7"><img src="http://mkruisselbrink.smugmug.com/photos/597527988_UKXQV-S.jpg" align="right"></a>
After spending an extra week after GCDS hiking around on La Gomera (another one of the Canarian islands), I finally got home and had time to process some of the photos I took during the desktop summit, so you can go ahead and take a look at them in <a href="http://mkruisselbrink.smugmug.com/gallery/8987670_TGgB7">my gcds gallery</a> on my smugmug page (so if anyone needs a smugmug referral code you can also ping me).