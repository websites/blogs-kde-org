---
title:   "coming back to the world of blog"
date:    2008-11-17
authors:
  - trueg
slug:    coming-back-world-blog
---
It has been a while since I blogged. The reason is simple: the birth of my daughter turned my brain upside down (as in: "as far as I can tell there exists only one thing in the whole world and it is not this blog"). Now, thousands of hours of staring at her later (and also after the very successful last Nepomuk project review) I am finally back to blogging.

And this first blog will not yet mention any amazing new developments in Nepomuk. No, not yet. This is merely a "hello I am back, did you miss me? no? why the hell not? but I thought the world would stop turning without my blog posts." blog post.

So just this one thing: I updated the <a href="http://techbase.kde.org/Development/Tutorials/Metadata/Nepomuk">Nepomuk documentation</a>. It now contains descriptions of all the Nepomuk services and the architecture. So, if you are interested and always wanted to know what all these processes with "nepomuk" in their names are doing, <a href="http://techbase.kde.org/Development/Tutorials/Metadata/Nepomuk">this</a> is the read for you.
