---
title:   "Finally I got my kdedevelopers blog running"
date:    2008-01-30
authors:
  - trueg
slug:    finally-i-got-my-kdedevelopers-blog-running
---
Why I failed before: I have no idea. Maybe the password mails got lost in the spam filter or I was just blind. Who knows. Important is that I am not using blogger which clee will like to hear (although I will be missing the WYSIWYG editor). Anyway, I got it now and I can start blogging about Nepomuk. Late, I know. I always tried to keep away from it but in the end, today there is no way around it. It simple seems the best way to inform people about my work.

Well then, let me toy with the special syntax a bit: <b>bold text</b>, <i>italic text</i>. Don't I just blow you away with my HTML skills? I know, it is amazing!

Ok, enough of this nonesense now. Next entry will be about Nepomuk. I promise. ;)