---
title:   "Nepomuk Appendix A - RDF for Dummies in a Nutshell"
date:    2008-02-12
authors:
  - trueg
slug:    nepomuk-appendix-rdf-dummies-nutshell
---
In my previous posts I used some terms that probably need explaining. The following descriptions should not be used as basis for any exam and may very well scare some academic semantic web professionals, but they get me through the day. And I think they are sufficient to understand most of what is going on with Nepomuk data in KDE.

<a href="http://www.w3.org/TR/rdf-primer/">RDF - The Resource Description Framework</a> describes a way of storing data. While "classical" databases are based on tables RDF data consists on triples and only triples. Each triple, called <i>statement</i> consists of
<pre>subject - predicate - object</pre>
The <i>subject</i> is a resource, the <i>predicate</i> is a relation, and the <i>object</i> is either another resource or a literal value. A <i>literal</i> can be a string or an integer or a double or any other type defined by <a href="http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html">XML Schema</a> (actually it is even possible to define custom literal types). Since RDF was born as a web technology all resources and relations are identified by their unique URI. (Meaning they have a namespace often ending in a <i>#</i> and a name. Typically abbreviation such as <i>foo:bar</i> are used.) Thus, a dataset in RDF is basically a graph where resources are the nodes, predicates the links, and literals act as leaves.

RDF defines one important default property: <i>rdf:type</i> which allows to assign a type to a resource.

<a href="http://www.w3.org/TR/rdf-schema/">RDFS - The RDF Schema</a> defines a set of resources and properties extending RDF. This extension basically allows to define ontologies. RDFS defines the two important classes <i>rdfs:Resource</i> and <i>rdfs:Class</i> which introduces the distinction between instances and types, as well as properties to define type hierarchies: <i>rdfs:subClassOf</i> and <i>rdfs:subPropertyOf</i>, and <i>rdfs:domain</i> and <i>rdfs:range</i> to specify details when defining properties.

This allows to create new classes and properties much like in object oriented programming. For example:

<pre>
@PREFIX foo: &lt;http://foo.bar/types#&gt;

foo:Human rdf:type rdfs:Class .
foo:Woman rdf:type rdfs:Class .
foo:Woman rdfs:subClassOf foo:Human .

foo:isMotherOf rdf:type rdf:Property .
foo:isMotherOf rdfs:domain foo:Woman .
foo:isMotherOf rdfs:range foo:Human .

foo:Mary rdf:type foo:Woman .
foo:Mary foo:isMotherOf foo:Carl .
</pre>

A simple example of how to define an ontology in RDFS (using the <a href="http://www.dajobe.org/2004/01/turtle/">Turtle</a> language). The last two important predicates in RDFS are <i>rdfs:label</i> and <i>rdfs:comment</i> which define human readable names and comments for any resource (the labels are used for matching fields and grouping results in my previous blog on search).

<a href="http://www.semanticdesktop.org/ontologies/nrl/">NRL - The Nepomuk Representation Language</a> was developed in Nepomuk to further extend on RDFS. I will not go into detail and explain everything about NRL but keep to what is important with respect to KDE at the moment.

Most importantly NRL changes triples to quadruples where the fourth "parameter" is another resource defining the graph in which the statement is stored (may be empty which means to store in the "default graph"). This <i>graph</i> (or <i>context</i> as it is called in <a href="http://soprano.sf.net/>Soprano</a>) is just another resource which groups a set of statements and allows to "attach" information to this set. NRL defines a set of graph types of which two are important here: <i>nrl:InstanceBase</i> and <i>nrl:Ontology</i>. The first one defines graphs that contain instances and the second one, well you guessed it, defines graphs that contain types and predicates.

To make this clearer let's extend our example with NRL stuff:

<pre>
@PREFIX foo: &lt;http://foo.bar/types#&gt;

foo:graph1 rdf:type nrl:Ontology .
foo:graph2 rdf:type nrl:InstanceBase .

foo:Human rdf:type rdfs:Class foo:graph1.
foo:Woman rdf:type rdfs:Class foo:graph1.
foo:Woman rdfs:subClassOf foo:Human foo:graph1 .

foo:isMotherOf rdf:type rdf:Property foo:graph1 .
foo:isMotherOf rdfs:domain foo:Woman foo:graph1 .
foo:isMotherOf rdfs:range foo:Human foo:graph1 .

foo:Mary rdf:type foo:Woman foo:graph2 .
foo:Mary foo:isMotherOf foo:Carl foo:graph2 .
</pre>

But making a distinction between ontology and instance resources is not all we gain from contexts.

<a href="http://www.semanticdesktop.org/ontologies/nao/">NAO - The Nepomuk Annotation Ontology</a> already defines resource types and properties you already encountered in KDE: <i>nao:Tag</i> or <i>nao:rating</i>. But it also defines <i>nao:created</i> which is a property that assigns an <a href="http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html#dateTime">xls:dateTime</a> literal to a resource, in our case a graph. This way we store information about when a piece of information was inserted into the Nepomuk repository.

<pre>
foo:graph1 nao:created "2008-02-12T14:43.022Z"^^&lt;http://www.w3.org/2001/XMLSchema#dateTime&gt; .
</pre>

<a href="http://www.w3.org/TR/rdf-sparql-query/">SPARQL - The Query Language for RDF</a> is what we use to query the RDF repository. Its syntax has been designed close to SQL but since it is quite young it is by far not as powerful yet.

Anyway, this is how a simple query that retrieves the mother of Carl looks like:

<pre>
prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;
prefix foo: &lt;http://foo.bar/types#&gt;

select ?r where { ?r foo:isMotherOf foo:Carl . }
</pre>

Or if we take NRL into account:

<pre>
prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;
prefix foo: &lt;http://foo.bar/types#&gt;
prefix nrl: &lt;http://semanticdesktop.org/ontologies/2007/08/15/nrl#&gt;

select ?r where { graph ?g { ?r foo:isMotherOf foo:Carl . } . ?g rdf:type nrl:InstanceBase . }
</pre>

I think this is enough for today. I hope this blog entry helps in understanding the inner workings of Nepomuk better. Let me just give one more hint: Soprano (the RDF storage solution we use in KDE) comes with static QUrl objects for most of the common resource URIs. You find them in the Soprano::Vocabulary namespace.