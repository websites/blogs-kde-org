---
title:   "Nepomuk Virtual Folders - The Next Level"
date:    2008-04-29
authors:
  - trueg
slug:    nepomuk-virtual-folders-next-level
---
Well, maybe "The Next Level" is overstating it but I improved the query API a lot. Not only can we now properly handle all sorts of literal comparisons but we can also use plain SPARQL queries. The latter allow some nice stuff like "<i>Recent Files</i>".
<!--break-->
For anyone interested the "<i>Recent Files</i>" virtual folder is coded using the folloing SPARQL query:

<pre>
select ?r where { 
    ?r a &lt;http://freedesktop.org/standards/xesam/1.0/core#File&gt; . 
    ?r &lt;http://freedesktop.org/standards/xesam/1.0/core#sourceModified&gt; ?date . 
} ORDER BY DESC(?date) LIMIT 10
</pre>

A very simple query that just selects the 10 most recent files.

Also nice are the folders listing all files modified today or yesterday. Anyway, time for a little screenshot and for me to code some query creation GUI (and maybe nested virtual folders).

[image:3442 size=preview]