---
title:   "We Don't Search..."
date:    2008-04-23
authors:
  - trueg
slug:    we-dont-search
---
<b>Virtual Folders in KDE</b>

You can tag files, you can annotate them, Strigi indexes your files, I showed <a href="http://blogs.kde.org/node/3382">how to create new information types and things</a>, but you could not really use it. I suspect you want to find the things again by searching for it. Well, I don't think we should search. We should simply find!
<!--break-->
OK, enough of the bragging. Introducing Nepomuk virtual folders.

<b>Step 1 - Predefined Virtual Folders</b>

There are the default predefined folders. An example (and honestly the only I defined so far) is the folder that lists all music files (actually all music files indexed by Strigi. So, yes, Strigi needs to be running for this to work best.)

[image:3421 width=400 style="position:relative; left:100px"]

<b>Step 2 - On-the-fly Virtual Folders</b>

We can also define our virtual folders on the fly by simply using the <i>nepomuksearch:/</i> protocol in combination with the query (I personally would prefer to hide the protocol in a nice GUI but that is polishing for the future). If we for example want to list all resources related to Nepomuk:

[image:3422 width=400 style="position:relative; left:100px"]

We get files tagged with tag Nepomuk, we get folders that contain the term "<i>Nepomuk</i>" (indexed via Strigi again) and we also get the tags themselves. The latter is most interesting as the tags are Nepomuk resources which do not relate to any file. KIO can list them anyway and even displays the correct type. The display of the correct type which is read from the Nepomuk store was achieved by a very small patch to KFileItem which I hope will be accepted. It basically treats the "<i>application/x-nepomuk-resource</i>" mimetype as a special case very much like "<i>application/x-desktop</i>". But instead of reading the type from the desktop file it is read via <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/nepomuk/html/classNepomuk_1_1Resource.html#f1d04d4eed2a0d5e1f4a496d6259b31f">Nepomuk::Resource::resourceType</a>. Simple but effective.

Well, off to a better example, a real example. Let's say we wrote down some notes using KWrite and saved them in some folder anywhere in the home directory:

[image:3423 width=400 style="position:relative; left:100px"]

Now instead of clicking through the directory hierarchy in search for the file we simple access it using a Nepomuk virtual folder. Be aware that the word "<i>nepomuk</i>" is part of the text itself while the word "<i>notes</i>" is part of the file name.

[image:3424 width=400 style="position:relative; left:100px"]

As a last little example we reuse the information created in <a href="http://blogs.kde.org/node/3382">my last blog</a>: my friend Tudor. He can of course also be accessed through a virtual folder:

[image:3425 width=400 style="position:relative; left:100px"]

Again the type information we created ourselves is used in the KIO slave. Well, that is it for today. I am off to work some more on the folders, making the query language more expressive (at some point we will need variables in there!), and maybe creating a virtual folder service. The latter could then be used not only for KIO virtual folders but also in KMail or some other application to list whatever resources one likes.