---
title:   "Strigi, Nepomuk and KDE 4.2 - hopefully a good team"
date:    2008-07-28
authors:
  - trueg
slug:    strigi-nepomuk-and-kde-42-hopefully-good-team
---
The new Strigi service a <a href="http://blogs.kde.org/node/3573">blogged about before</a> is in svn trunk now. With it Strigi is now enabled by default. But that is no need to be alarmed. I think it behaves quite nicely. After 1 hour and 42 minutes of initial indexing (I did not feel any slowdown of the system in that time) I got this:

[image:3577 size=original]

I think indexing 37574 files in 1:42h with a usage of 222.5 MiB of space is very good (the files on hard disk take a total of 43 GiB). Keep in mind that you can now search all these files and properly link them in Nepomuk. And after this you only get updates anyway. So please enjoy.

<i>Edit: I forgot to mention that this is using the Soprano Sesame2 backend. The Redland backend is much slower and uses way more hard disk space.</i>