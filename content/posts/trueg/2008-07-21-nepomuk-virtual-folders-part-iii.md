---
title:   "Nepomuk Virtual Folders Part III"
date:    2008-07-21
authors:
  - trueg
slug:    nepomuk-virtual-folders-part-iii
---
No, I did not forget about the virtual folders, but I also did not implement subfolders or a graphical query editor. Shame on me, I know. I thought it was more important to focus on stability and a clean API first. And that is what I did.

But before that let my point you to the <a href="http://amarok.kde.org/blog/authors/27-Daniel-Winter">blog of my Google Summer of Code student Daniel Winter<a> who uses Nepomuk to improve Amarok playlists. Cool stuff.

Back to virtual folders: <a href="http://blogs.kde.org/node/3426">Last time</a> we saw that we could list query results wherever KIO was used. This time, I will only give a short update for the users and a way more interesting one for the developers:

<b>The User Perspective</b>

I won't give you much to read but provide a little <a href="http://people.mandriva.com/~strueg/nepomuk-kde-virtual-folders.avi">screencast</a> (<i>Update: now better quality!</i>) showing how virtual folders are now updated automatically:

<i>Normally this is where the video should be embedded now but apparently it gets filtered out, so here is the link again: <a href="http://people.mandriva.com/~strueg/nepomuk-kde-virtual-folders.avi">Nepomuk Virtual Folder Screencast</a></i>

If you are a user and do not care about how stuff works you can stop reading here.

<b>The Developer Perspective</b>

IMHO what we just saw is pretty nice (but there are issues as always: if you know KIO, you know that unused slaves are terminated. The same is true for the virtual folder io slave. Once it is terminated we loose its internal state and, thus, the updates. Well, if you have any ideas to solve this.... the only one I could come up with is a hack to force the slave to not be deleted until not needed for updates anymore).

And why is this so interesting for developers? Simple: the virtual folder KIO slave does "only" convert the search results to proper KIO UDSEntries. The actual results are created by the brand new <i><b>Nepomuk Query Service</b></i>.

This service has a rather simple DBus interface. You call the query method and get a new DBus object which informs about new results and removed results as well as the finishing of the initial listing. And this is totally generic and not restricted to files.

If you want to try it and support my proposition to get it into kdebase for 4.2: you can find it in <a href="http://websvn.kde.org/trunk/playground/base/nepomuk-kde/queryservice">playground/base/nepomuk-kde/queryservice</a>. In the subfolder "client" you find the client library. Use that and you don't even have to care about the DBus communication. Simple.

Keep in mind that the query API is still in development and your input and wishes can still easily be adapted.