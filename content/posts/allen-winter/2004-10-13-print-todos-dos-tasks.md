---
title:   "Print Todos... To-dos... Tasks"
date:    2004-10-13
authors:
  - allen winter
slug:    print-todos-dos-tasks
---
Someone (I forgot who, sorry) asked on the kdepim mailing list for the To-do printout to optionally write the percentage completed.  So I implemented that.<br>
While working on that new feature we discovered that there were two "ghosted"
options on the To-do print dialog that were never implemented, namely the option
to print only unfinished To-dos and the other to print To-dos within a range of due dates.
So I implemented those as well.
<br>
But then I thought why not give the user the ability to print the To-dos in different
sort orders? Like by percentage complete or by due date?  Plus, this gives me a kick
to finally test and commit my Incidence sorting methods I discussed a few blogs back.
So that's my current project.
<br>
Which brings me to libkcal.  And reminds me to nag the web admins to automagically
post the KDEPIM Reference ('make apidox') on pim.kde.org.  I discussed this in my previous blog.
<br>
New Toy Alert! I just got a 160GB external USB hard drive.  Now I can store lots of music (from my personal collection, of course) and pictures from my digital camera!
<br>
That's all for now...