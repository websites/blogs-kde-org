---
title:   "Hello, KonsoleKalendar, and Stuff"
date:    2004-09-08
authors:
  - allen winter
slug:    hello-konsolekalendar-and-stuff
---
Howdy, this is my first KDE developers blog entry!<br><br>
I got into KDE development because I had a feature I asked to be implemented
in KonsoleKalendar -- the itch.  Cornelius asked me if I wanted to work on that
feature... so I did -- the scratch.  So now I'm the KonsoleKalendar co-maintainer
along with Tuukka Pasanen.  KonsoleKalendar is a CLI program so it doesn't seem
to get much attention and it has suffered from lack of attention recently but it
is a handy utility.  I'm happy to say that I've helped to implement new features
like inserting and deleting and modifying events, and also adding new export formats. And, of course, I've fixed a bunch of bugs too.  Check it out <help:konsolekalendar><br><br>
There are two projects I want to get back to work on:
<ol>
<li> A "Special Occassions" Summary for Kontact.  The current "Birthdays/Anniversaries" Summary has stuff from Kaddressbook only.
I want a Summary that also includes birthdays/anniversaries/holidays/etc
from your calendar(s).  I was making good progress on this and thought it
would be ready for 3.3, but...
<br>
<li> A stock ticker program.  So what i did was take the knewsticker code
and started hacking.  I'm getting close on this one.  Eventually I want
to have a Kontact Summary plugin for this too so one can monitor stock prices
along with other news of the day (and the weather, of course).
</ol>
More later...