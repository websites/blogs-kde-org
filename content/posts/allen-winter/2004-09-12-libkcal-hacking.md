---
title:   "libkcal hacking"
date:    2004-09-12
authors:
  - allen winter
slug:    libkcal-hacking
---
I got my hands into the KDEPIM API the past few days... libkcal to be specific.
I wanted a better interface for sorting Events which KonsoleKalendar needed.  For consistency, I added similar code for sorting Todos and Journals.  The patch will be going off to the kdepim mailing list for review in a few minutes (after I test <b>one more time</b>).  This was fun for a change with no human interface to worry about.
<br>
Since my favorite sports team won its first game of the season today (American Football) I am double-happy.
