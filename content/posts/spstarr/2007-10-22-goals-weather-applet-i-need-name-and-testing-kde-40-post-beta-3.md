---
title:   "Goals for the Weather Applet (I need a name for this) and testing KDE 4.0 post-Beta 3"
date:    2007-10-22
authors:
  - spstarr
slug:    goals-weather-applet-i-need-name-and-testing-kde-40-post-beta-3
---
Given the time constraints for KDE 4.0, I have decided to downgrade the functionality for the weather applet. In this release, my aim is to replace kweather functionality completely. This should be feesable to do for the 1.0 release. 

Given that Plasma will likely have changes for KDE 4.1, It would make sense to hold off until more of the plumbing has settled down then all the eyecandy can be added in.

I gave KDE 4.0 post-Beta #3 (October 21nd build) a go last night. For the most part, I was able to use KDE functionally. Konqueror crashed a few times, but expected. Composite worked, exploding windows and all :-) I could not log out or lock screen as it would deadlock X for some reason.

Given I've been ridiculed on IRC for complaining about KDE 4.0's release time/schedule, The best thing I can do is to look at what bugs that can be fixed which I can start looking at tonight.

I would note the Oxygen window decoration is nice however on LCD screens - gamma adjusted or not - it there is no contrast between white windows. I hope the theme will get gradients so that one could distinguish between different windows.

<b>Correction: I have been told by the Oxygen people you CAN adjust the colours of the window decoration, I will play with that tonight.</b>