---
title:   "Fedora Elections and Weather Applet"
date:    2008-06-18
authors:
  - spstarr
slug:    fedora-elections-and-weather-applet
---
Just as everyone else SHOULD be doing....

[image:3521 size="original" nolink=1]
(thanks to whoever drew the artwork)

Go <a href="https://admin.fedoraproject.org/voting">HERE</a> to vote if you're a member of Fedora

With the election going on, I believe we'll have a great board elected to continue the progress Fedora is making. So do your part and VOTE NOW!

On the KDE side of things...

I spoke to one of our famous artists <a href="http://pinheiro-kde.blogspot.com/">Pinheiro</a> for a mockup of the Weather plasmoid. I'm sure he'll have some interesting ideas to cook up. It looks like I won't be able to get this thing in for KDE 4.1 (sigh). With no artwork (and I'm a terrible artist), it's best to leave this in playground. 

I plan on working on some other ions (data source engines) for other Free weather data sources soon if their licenses allow me to do so.

Calling other artists, anyone in the mood to write SVG weather icons? kthxbye :)