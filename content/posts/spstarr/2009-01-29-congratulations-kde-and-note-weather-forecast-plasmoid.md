---
title:   "Congratulations KDE! and a note on weather forecast plasmoid"
date:    2009-01-29
authors:
  - spstarr
slug:    congratulations-kde-and-note-weather-forecast-plasmoid
---
We did it!

<img src="http://www.kde.org/img/kde42.png" alt="KDE 4.2 Released!" title="KDE 4.2 released!"/>

Congratulations to everyone who made this possible! It's this kind of spirit that makes me want to shed a tear of joy.

For those looking for the weather forecast plasmoid (that's the official name of it), it's now in extragear for the KDE 4.2 post-release so distros can now package it.

My plan is to move this to kdeplasma-addons for KDE 4.3 in a few months. We have some features planned that will be really nice :-)