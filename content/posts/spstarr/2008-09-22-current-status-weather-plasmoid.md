---
title:   "Current Status of weather plasmoid"
date:    2008-09-22
authors:
  - spstarr
slug:    current-status-weather-plasmoid
---
Hello, 

Just to let everyone know, I haven't stopped development. Here's a current look of the plasmoid with more changes coming

<img src="https://blogs.kde.org/files/images/weather.png" alt="Weather Plasmoid" title="Weather Plasmoid">

I've been busy with notmart aka <a href="http://www.notmart.org/">Marco Martin</a> and finally the results are starting to pay off nicely. I am still gunning for KDE 4.2 to have the '1.0' version of this plasmoid complete and ready for use.

The configuration dialog is going to be redone as this was designed for a different use case that didn't make sense now. If you'd like to help out email me or find me on IRC :)