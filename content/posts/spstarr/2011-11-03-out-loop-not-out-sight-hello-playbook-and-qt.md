---
title:   "Out of the loop, but not out of sight, hello PlayBook and Qt!"
date:    2011-11-03
authors:
  - spstarr
slug:    out-loop-not-out-sight-hello-playbook-and-qt
---
Please note: This blog is personal and opinions are of my own and do not represent RIM

I've been quiet for a while now. Lots of things going on in my life. I have not forgotten KDE, but I'm still unable to work on anything at this time (more on that in future post). 

But I'm glad to say that having Qt as part of PlayBook is awesome! 
I look forward to seeing what people will do. 

We've ported a number of projects to QNX for the PlayBook and can be found here: <a href="http://blackberry.github.com/">BlackBerry GitHub</a>

I really want to see Open Source flourish on PlayBook, BBX and these steps are great :)
