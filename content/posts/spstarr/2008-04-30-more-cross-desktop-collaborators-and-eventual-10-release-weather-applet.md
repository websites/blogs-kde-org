---
title:   "More Cross-desktop collaborators and the eventual '1.0' release of the weather applet"
date:    2008-04-30
authors:
  - spstarr
slug:    more-cross-desktop-collaborators-and-eventual-10-release-weather-applet
---
<b>Collaboration</b>

I am pleasantly surprised to hear from <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> and others that there has been more collaboration going on that most people might not know about. Learning that Qt 4.x now can use the Glib event loop gives us a lot of abilities to create glue between KDE and GNOME interfaces which is really encouraging.

Also happy to see other GNOME people such as <a href="http://www.figuiere.net/hub/blog/">Hubert Figuière</a> and <a href="http://blogs.gnome.org/rodrigo/2008/04/29/guademy-2008/">Rodrigo Moya</a> are looking at more collaboration between our two teams. This can only be a Good Thing.

I personally look forward to working with other teams together to help us make the Desktop on Linux (and the other Freenixes too) grow.

I had a look at libgweather and wonder how to hook this into the weather engine but haven't examined the code yet. If it can provide similar data such as collecting current conditions as the ions do, maybe create an ion (data source plugin) to load libgweather and hook that in somehow.

<b>Plasma Development</b>

In the meantime, I spent a night of debugging to track down some new behaviours the Plasma Sprint has introduced. Needless to say, some of my code changes to fix the applet seem like ugly hacks but they might be part of a bug somewhere in the new API I hope. Now that I'm Plasma Ready(tm) again, I'm planning for a simple applet release in time for KDE 4.1 (for extragear). 

Now, in previous postings I've talked about all sorts of wizbang features and things, but those are the goals of the project. This first version is going to have a 'button like' look, much like other simplistic applets. It will utilize the SVG skin theming only for now not icon theming. We don't have a free set of icons to use other then kweather's icons. Maybe this will make the Oxygen people cry and demand new icons :-)

Since I'm going for simplistic right now, it will only display current conditions. Let's get something out the door that works. As for KDE 4.2, things should become more exciting.

It was important for Plasma to break the API and even if that slowed down Plasma development, this will give us a much better framework to work from.