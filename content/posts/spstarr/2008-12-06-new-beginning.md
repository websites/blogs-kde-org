---
title:   "A New Beginning"
date:    2008-12-06
authors:
  - spstarr
slug:    new-beginning
---
So,I've moved. This is the first week that I'm in my new place. If you were wondering why I haven't done much coding, that's why ;-)

Getting used to living away from the nest.

Here's some pictures:

<img src="https://blogs.kde.org/files/images/spstarr_condo1.jpg" alt="Condo Picture 1" title="Condo Picture 1"  class="image image-preview " width="480" height="640" />

<img src="https://blogs.kde.org/files/images/spstarr_condo2.jpg" alt="Condo Picture 2" title="Condo Picture 2"  class="image image-preview " width="480" height="640" />

Makeshift workstation area, notice no LCD monitor yet :-)<br>
<img src="https://blogs.kde.org/files/images/spstarr_condo3.jpg" alt="Condo Picture 3" title="Condo Picture 3"  class="image image-preview " width="480" height="640" />

<img src="https://blogs.kde.org/files/images/spstarr_condo4.jpg" alt="Condo Picture 4" title="Condo Picture 4"  class="image image-preview "/>

A view from above with more snow coming tonight<br>
<img src="https://blogs.kde.org/files/images/spstarr_condo5.jpg" alt="Condo Picture 5" title="Condo Picture 5"  class="image image-preview "/>