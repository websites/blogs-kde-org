---
title:   "WeatherEngine now in KDE Trunk!"
date:    2007-09-11
authors:
  - spstarr
slug:    weatherengine-now-kde-trunk
---
We had a Plasma meeting yesterday. One of the things discussed was to move the weather engine bits into KDE trunk. This is now done. 

Richard Moore is planning on taking a look at a bug in the UKMET BBC datasource. Once that issue is fixed. I plan on finishing the ion. I will be adding to the Techbase Wiki information on how the dataengine works, how the data formatting should be done so that each ion can be used by anyone's own weather applets. 

Richard has been busy hacking away on the Plasma JavaScript support bindings. I will be using these bindings to write the applet. It is possible this weekend you might get to see a screenshot of the weather applet displaying current conditions. 

I plan to have the following 'screens' depending on the datasource (ion)

- Current conditions (Screen #1)
   * Place information
   * SVG (or PNG?) icon showing the current reported condition
   * Temperature in your favourate metric (metric, imperial)
   * Comfort Temperature: (Humidex/Heat Index, Windchill)
   * Dewpoint Temperature
   * Pressure (in inches or kilopascals)
   * Pressure tendency (if available)
   * Visibility (in kilometers or miles)
   * Humidity
   * Wind Speed (in kilometers or miles per hour)
   * Wind Direction
   * Wind Gust

This will contain the minimal current observations where available.

- 5 Day forecast (Screen #2, if available)
  * Place information
  * SVG (or PNG) image containing the expected weather condition for that period
  * PNG/JPG/GIF image of the respective Weather agency (Credit requirements) 
    (top left side of applet)
  * Short expected weather summary: Period, Condition expected, High Temp or Low Temp followed by 
    POP (Probability of Precipitation). 
    -I believe if the user clicks on a specific period we can make it display extended weather 
     forecast information (wind speed, etc) via some sort of visualization popup.
    - For future, I will be also providing a rough gestimate of potential severe weather 
      (where available) such as CAPE (Convective Available Potential Energy), SI (Storm Index). 
      This is likely for 2.0 of the applet.

- Almanac Information/Seasonal Normals/Records (Screen #3, if available)
  - Rises/Sets (if available)
    * Sunrise/Set
    * Moonrise/Set
  - Normal temperatures for time of year (if available)
    * High temperature (in metric or imperial)
    * Low temperature
    * Mean temperature
  - Record weather (if available)
    * Record High temperature
    * Record Low temperature
    * Record Rainfall
    * Record Snowfall
 
- Miscellaneous Information (Screen #4, if available) 
  * UV Index
  * UV scale (high, low, extreme, etc)

This is a lot of weather data. The idea is to have the weather applet cycle though these 'screens' on a timed interval, fading in or some other effects to transition between screens. 

In addition to this, some ions provide warning/watch information. This will be displayed in some sort of blingy manner :-)

For configuring the applet, the user will be able to set time intervals for fetching updated information (on 30 minute intervals). 

More on the configuration later.