---
title:   "The Job hunt continues..."
date:    2013-03-31
authors:
  - spstarr
slug:    job-hunt-continues
---
I've been really busy on the job front these days most recently being interviewed at palantir.com HQ. It was a great experience. I made it to the final steps but unfortunately, was not an exact fit for what they needed (I applied for a Quality Engineering role). Even so, I'm proud to have gotten to the decision stage, they aren't an easy company to get into.
<br><br>
Now that my passport is being renewed, hopefully more opportunities will arrive. The economy hasn't made any of this easy looking for work but hopefully something comes around soon.
<br><br>Please take a look at my Linkedin profile. If something catches your interest, contact me :) <br><br>http://ca.linkedin.com/in/shawnstarr/

