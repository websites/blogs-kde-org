---
title:   "Plasma Weather Meeting and things to come..."
date:    2008-07-10
authors:
  - spstarr
slug:    plasma-weather-meeting-and-things-come
---
Our famous Oxygen folks have brought a taste of things to come for the weather plasmoid. This mockup is a work in progress.

Credit goes to Pinheiro and Lee Olson from Oxygen for getting things started!

[image:3553 size="original" nolink=1]

I would like to propose a meeting for next week to discuss some changes for the mockup, and other features we may need. Feedback welcome on the date and time.

This is scheduled for KDE 4.2 timeframe.

And...

[image:3554 size="original" nolink=1]
and wish I could
