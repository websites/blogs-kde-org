---
title:   "A Holiday Present - Weather Plasmoid 1.0 is finished"
date:    2008-12-29
authors:
  - spstarr
slug:    holiday-present-weather-plasmoid-10-finished
---
Well, I've been extremely busy on the weather plasmoid and finally, '1.0' is ready for use. It's now moved to kdereview so that any final bugs can be shaken out. This should arrive in extragear as soon as the review is completed.

<bold>*** If your Qt is old, it WILL crash when moving mouse to focus mouse on the icons. Please get your distro to get latest Qt 4.4.x release!</bold>

You'll notice some differences:

* New! Five day view. Special thanks to Marco Martin and Lee Olson also the wind icon.
* Thanks to the Pinheiro for the new Oxygen Icons
* Dock applet to plasma panel thanks to Marco Martin for this.
* Tooltip support that displays the icon, place, condition and current temperature.
* Rearranged configuration dialog thanks to Davide Bettio for this.
* All the conversion combo boxes now work it you can mix and match any combinations.
* Clickable credits where present (depends on data source) this will take you to the original data from the source's website.
* Popup tooltips when you hover over the 5 day icons to show you what the condition will be.
* Clickable watches/warning links where present (depends on data source).
* If in Canada, if a watch or warning for your area is issued the applet will add the 'Notices' tab on its next update of data.

If you're using KDE trunk, please test and report any bugs you find to me on #plasma or email.

And now, for screen shots!

In this screenshot, two shots using the Environment Canada data source:

<img src="https://blogs.kde.org/files/images/weather-config.png" alt="Weather Plasmoid Config Dialog">

You can click on the watch and warning text and it will open up in a browser to show the official warning text from your weather agency (where supported, only Environment Canada at the moment)

<img src="https://blogs.kde.org/files/images/weather-applet_0_0.png" alt="Weather Plasmoid Applet">

In this screenshot, using the BBCUKMET data source, I configured temperature for Fahrenheit, wind speed in Meters Per Second (m/s) and pressure in Inches of mercury.

<img src="https://blogs.kde.org/files/images/weather-applet2.png" alt='Weather Plasmoid Applet">

Here is the applet docked to Plasma panel

<img src="https://blogs.kde.org/files/images/weather-dock.png" alt="Weather Plasmoid on panel">

* NOTE: The NOAA data source only provides current conditions not long term forecasts.

Thank you for your patience =)
Shawn.