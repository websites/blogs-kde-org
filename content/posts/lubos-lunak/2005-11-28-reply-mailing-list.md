---
title:   "Reply to mailing list"
date:    2005-11-28
authors:
  - lubos lunak
slug:    reply-mailing-list
---
Strange that there are still many people who care if a mailing list changes Reply-To header to point to the mailing list (usually called <a href="http://www.metasystema.net/essays/reply-to.mhtml">Reply-To munging</a>) or not. Like if that matters ... I'm not even sure which mailing lists I'm subscribed to do or don't do that.

It's as simple as using the 'Reply to mailing list' action, which does just that, sends a reply to the mailing list. No need to do 'Reply to all', which has the annoying effect of sending one more copy also directly to some people (and the even more annoying effect of sometimes the mail from the mailing list being discarded because of the direct mail, depending on mail settings).

Or is Kontact/KMail the only mail client with such "advanced" technology?
<!--break-->
