---
title:   "KWin in KDE4.0"
date:    2008-01-11
authors:
  - lubos lunak
slug:    kwin-kde40
---
What was it ... ah, yes ... KDE4.0 has been just released ... just in case you haven't noticed yet in all the other blog posts all around.

No, in fact, the thing I really wanted, was: People, the proper capitalization is "KWin". Not "KWIN", not "Kwin". It is the KDE Window Manager, just like KCalc is the KDE Calculator, KPat is the KDE Patience Game or KMenuEdit is the KDE Menu Editor. Okay :) ?

And, the one more thing I wanted: In addition to the KDE4.0 Visual Guide, which includes KWin, it was planned to also create a KDE4.0 KWin video, which would present most of the effects shipped with KDE4.0 and would explain them (so that people would know how to use them well and also would not go commenting 'useless' about things like the Invert effect which perhaps may be useless for some but can be very useful for others). This unfortunately did not happen in time for 4.0, but hopefully somewhen later. You can now at least check <a href="http://www.digg.com/linux_unix/KDE_4_0_0_KWin_Composite_Showcase">http://www.digg.com/linux_unix/KDE_4_0_0_KWin_Composite_Showcase</a> for a recent video made by somebody else, which I found completely accidentally while Gooling.

One of the things that distracted me from making the video were <a href="http://techbase.kde.org/Projects/KWin/4.0-release-notes">KDE4.0 KWin release notes</a>, which I actually currently consider more important than the video. It includes introduction, setting KWin composite up, basic description of using some of the effects, troubleshooting and various other things that seemed important to mention.

It also includes a FAQ section that will probably grow. Right now it includes explanations about (apparently very popular) why not simply drop KWin in favour of Compiz, why not at least use Compiz plugins in KWin when we don't drop KWin completely and why it is actually worth bothering with KWin when Compiz is so much better anyway. I've got such a strange feeling the list will need to get bigger.

<!--break-->
