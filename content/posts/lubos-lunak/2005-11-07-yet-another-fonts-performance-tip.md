---
title:   "Yet another fonts performance tip"
date:    2005-11-07
authors:
  - lubos lunak
slug:    yet-another-fonts-performance-tip
---
Is it just me or do the blog categories on kdedevelopers.org never match what I want to say? Well, nevermind. The latest performance tip brought to you by SUSE KDE developers (don't ask, I very likely don't know anyway) is here:

In KControl, go to the fonts module, and see if you use Sans Serif, Serif or Monospace fonts. If you've never touched the fonts settings it's very likely you see them there. In Konsole use fc-match to find out what these font aliases (they're not really fonts) actually mean - e.g. here "fc-match sans serif" says Verdana. Now replace all occurences of those three aliases by real fonts as given by fc-match, or simply choose a font you like (for the fixed font it's usually better to use something like Misc Fixed rather than Andale Mono fc-match may give you for Monospace). That's all, now all your KDE apps should start faster. You might want to check also font settings in specific apps if you want. This performance problem should hopefully get fixed soon in fontconfig or Qt.
<!--break-->
