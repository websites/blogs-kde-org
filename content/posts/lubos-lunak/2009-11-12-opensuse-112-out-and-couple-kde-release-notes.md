---
title:   "openSUSE 11.2 is out. And a couple of KDE release notes."
date:    2009-11-12
authors:
  - lubos lunak
slug:    opensuse-112-out-and-couple-kde-release-notes
---
<a href="http://en.opensuse.org/openSUSE_11.2"><img src="http://counter.opensuse.org/11.2/medium" border="0"/></a>

<p>
Oh, yes, just in case you haven't noticed, <a href="http://en.opensuse.org/OpenSUSE_11.2">it's out</a>. However, since I maintain this image of seriousness, purposefulness and so on (which I only occassionally spoil by something like doing strange things to my hair, eating way too much icecream or doing silly things at SUSE outdoor events), I would like here to reference the <a href="http://en.opensuse.org/KDE/ReleaseNotes11.2">KDE release notes for openSUSE 11.2</a>. We recalled at least the Pulse Audio thing a bit too late, so maybe right now the actual release notes do not mention it yet.</p>

<p>
<i>Default Web Browser</i> - yes, we default to Firefox with <a href="http://www.youtube.com/watch?v=sCt6BzFiDts">KDE integration</a>, see the notes for switching back. Nothing more to say here, if you still don't know about this, you apparently don't know about KDE and openSUSE (in which case you're kindly asked to fix that ;) ).</p>

<p>
<i>KDE3</i> - boy, I'm getting really tired of this topic :(. It's really simple: Apparently there is nobody caring about KDE3 enough to do something about it. So, if you are in the minority that still doesn't find KDE4.3 good enough as a replacement, either just stick with an older release, or try to use the unsupported <a href="http://en.opensuse.org/KDE3>KDE:KDE3</a> repository, which right now looks like it still builds, but may not anymore tomorrow if anything changes, since, as I said, everybody feels like having more important things to do than spent time on it (really, we tried to find people who'd be interested in it, but apparently all people capable of doing so already work on KDE4 or are too busy complaining on mailing lists). So, just get over it. KDE3 is dead. It is no more. It has ceased to be. It has expired. It is an ex-KDE3. (Thinking of it, I will skip that is has gone on to meet its maker, just in case).</p>

<p>
<i>Strigi/Nepomuk</i> - they are disabled by default. With Beagle not enabled by default being asked a <a href=https://features.opensuse.org/305296">lot</a> for, and generally considering the situation, we decided this would be a better option (and upstream wasn't really against). Those who want them enabled can easily do so in systemsettings. Well, almost - unfortunately a <a href="https://bugzilla.novell.com/show_bug.cgi?id=548007">problem</a> slipped in that prevents starting Strigi in default installations. See comment #6 in the #548007 bugreport for an easy fix.</p>

<p>
<i>PulseAudio</i> - since KDE not only does not have a hard dependency on PA but also there is basically no integration of PA, having PA with KDE doesn't buy anything to most users, so we decided to disable it by default in KDE installs. This is not the broken PA that was in 11.1, but still, why force it unnecessarily. Those that have a use for PA need to enable it in the YaST Sound module (note that this includes everything running on a system that has KDE as the default desktop).</p>

<p>
Now, onto the <a href="http://news.opensuse.org/2009/10/27/sneak-peeks-at-opensuse-11-2-kde-4-3-experience-with-lubos-lunak/">goodies</a>!</p>
<!--break-->
