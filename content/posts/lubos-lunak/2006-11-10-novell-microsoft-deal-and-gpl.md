---
title:   "Novell, Microsoft deal and GPL"
date:    2006-11-10
authors:
  - lubos lunak
slug:    novell-microsoft-deal-and-gpl
---
Oh, cool. Novell's PR department needed only a week to produce <a href="http://www.novell.com/linux/microsoft/faq_opensource.html">FAQ</a> that'd actually answer frequently asked questions. Press releases full of quotes, long words and other strange stuff are simple, but a plain and clear explanation of what's going on apparently takes time for some unknown reason. And, since pretty much everybody has already tried to interpret various aspects of the deal, including here on Planet KDE, why couldn't I as well? I'm no lawyer, I don't understand it that much and I don't know anything more than what's written in the FAQ and the announcements, but after all that didn't stop many others either.

Say, the part about the GPL "problem". The FAQ, among other things, says:
<ol>
<li>Novell still insists, and always has, that Linux does not infinge on any Microsoft's (on anybody else's) patents.</li>
<li>Novell will not include any patented code in its contributions to open source projects.</li>
<li>Novell, as one of the founding members of <a href="http://www.openinventionnetwork.com">Open Invention Network</a>, will continue to be prepared to defend Linux against any legal attacks against it.</li>
<li>Novell, as a part of the deal, has received from Microsoft a covenant not to sue Novell's customers, that however doesn't apply to Novell directly.</li>
</ol>

So, let's see:
<ol>
<li>Linux does not infringe on any patents - so there's no problem.</li>
<li>Novell does not plan to change that.</li>
<li>If Microsoft decides to sue anybody for the use of Linux, Open Invention Network will sue back with its patents portfolio (welcome to the cold war). Therefore Microsoft will probably think more than just twice about doing it.</li>
<li>Microsoft does not have to sue. Remember SCO? Just vaguely threatening to sue is often enough. It doesn't matter if Microsoft could win or even gain anything from from sueing anybody. At least here from Europe it looks like one can sue anybody for anything in the US, anyway. Small companies probably wouldn't like the idea of Microsoft sueing them, no matter how silly the allegations. They'd still have to defend and stand against Microsoft's army of lawyers. But now they can use Linux from Novell and sleep better at night.</li>
</ol>

The funny thing is that the last point doesn't seem to make sense, strictly technically speaking. There's no patent infringment and Microsoft won't most probably sue anybody anyway. So why bother? Is just the sleep of Novell's customers worth it?

Apparently Novell thinks it is. There seem to be customers who are still afraid of Microsoft, despite not doing anything bad and having already all kinds of guarantees from Novell, so Novell makes a deal with Microsoft to have a guarantee from Microsoft that it will not have any silly lawsuits against Novell's customers. It's funny, it's absurd, c'est la vie.

For the other Linux users nothing changes, they also still don't do anything bad (and even their sleep doesn't change). Microsoft can still try to sue those, just like it could have done until now. In fact, Microsoft can still sue Novell. If you look at it in a way, this part of deal basically says "Listen, Microsoft, if you have a problem with Novell, sort it out with Novell, don't bother the customers". I.e. Novell redirects any possible threats from its customers to itself and Novell customers get better sleep. That's how I basically see it.

And, as for violating the GPL, I'm no lawyer, so could somebody please enlighten me: Linux does not infringe on any patents. There's no patent licensing, at least as far as GPL-ed code is concerned. There are no plans to add anything patented to GPL-ed code. Novell does not put any additional restrictions on GPL-ed code it ships that'd contradict the GPL. So how exactly is the GPL being violated? There's only a promise from Microsoft that it won't try any silly lawsuits against Novell's customers, a promise not to sue somebody who doesn't do anything bad anyway. If that's against the GPL, even against its spirit I'd say, I must have missed that part. And, while the FSF sometimes goes to quite some extremes, I hope it won't go as far as twisting GPLv3 this way (because, if nothing else, then Microsoft can simply make a public vague promise it won't sue anybody and we're all suddenly violating the GPL).

I don't feel like commenting on anything else regarding the deal or even whether I like it or not. I'm getting tired of all this anyway.

(A disclaimer is probably in order, just in case: This is just a personal opinion, more or less based on reading of the relevant announcements and FAQ, followed by resisting the urge to spread the first silly reaction that came to mind [that's a good thing to do, by the way, so this is recommended in general]. I have no idea what my employer (=Novell, just in case somebody doesn't know) will think about this (if anything at all), especially given that I may be wrong, just like everybody else. No animals were harmed during making of this blog entry and no signing of anything with one's own blood was involved either. Blah blah blah, yadda yadda yadda.)
<!--break-->
