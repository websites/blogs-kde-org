---
title:   "News from the Wobblyland, part IV"
date:    2007-04-24
authors:
  - lubos lunak
slug:    news-wobblyland-part-iv
---
I did a presentation on compositing managers at the local LinuxExpo last week. Using kwin_composite for demonstrations. And KWin actually did its job quite fine (although it had taken quite some effort to get it there, with KWin being <i>almost</i> ready for it for more than a week). What did somewhat worse was the human factor - having more than 20 shortcuts wasn't a very good idea and failing to show the final and best feature because the bloody modifier was supposed to be Ctrl was a bit embarrassing. Oh well. C'est la vie. The good thing is that there are more things to show.

First video, <a href="http://www.youtube.com/watch?v=gMVB8LYMR6o">DimInactive and DialogParent</a>, shows a feature asked for in <a href="http://bugs.kde.org/show_bug.cgi?id=46226">bug #46226</a> - dimming out inactive windows in order to increase contract for vision impaired users. Fixing it without compositing support was next to impossible. The video shows also darkening of windows blocked by modal dialogs, I think I've already shown a static picture of this one before.

Second video, <a href="http://www.youtube.com/watch?v=zGRdfI5WKIg">Zoom and Magnifier</a>, may also look like accessibility features, but they'll definitely come handy also e.g. when visiting some pages where the designed was an idiot thinking that using 6pt "big" fonts is perfectly fine.

Third one, <a href="http://www.youtube.com/watch?v=ZQWkt6_6pGs">FallApart and Flame</a>, are some eyecandy (not really useful in practice if you ask me, but maybe that's just me). And yes, the second one _is_ the window burning down, it's just that there's no actual flame drawn, since I'm so bad at graphics. Feel free to do something about it, should be very simple.

Fourth <a href="http://www.youtube.com/watch?v=zLSsAPBiudE">video</a> shows live thumbnails during various actions and, as the last thing, a thumbnail of the window is added to an edge of the screen - should be useful for keeping an eye e.g. on a running compilation. Just in case somebody asks, the video is one of <a href="http://www.novell.com/video/">Novell videos</a>, called Take Command.

Fifth video is just plain <a href="http://www.youtube.com/watch?v=SWaSz4smYlg">PresentWindows</a>, the well-known effect showing all windows. The thing that should make it more interesting is the filtering of the windows (the underlining was done with a touchpad, bear with me, I hate touchpads).

The last one, the one with the Ctrl shortcut, is <a href="http://www.youtube.com/watch?v=LMnmGdk1ODs">DesktopGrid</a>. A slightly more improved version of the pager. Just like the cube this is a way to make a more intuitive representation of virtual desktops, maybe not as flashy but IMHO somewhat more useful.

And finally, since the kwin_composite branch, while still far from being done, is not really different from trunk when compositing is disabled, it will be merged soon back to trunk, defaulting to compositing turned off.
<!--break-->
