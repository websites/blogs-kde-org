---
title:   "Why Flash sucks"
date:    2007-12-20
authors:
  - lubos lunak
slug:    why-flash-sucks
---
As one of my colleagues notes, this statement holds true on its own. However, for those like me too blind to see some things, there are two things about Flash you should know:
<ol>
<li>The latest Flash update fixes various critical security issues.</li>
<li>The latest Flash update does not work with anything that is not Gecko-based.</li>
</ol>
Do the maths yourself. Or just watch it happen as soon as your distribution's update hits your machine.


I'm not going to comment on the first one, but, as a person who has spent a good portion of the last two weeks trying to find a solution, I know some things about the second.


First of all, this is apparently perfectly fine with Adobe. According to their <a href="http://www.adobe.com/products/flashplayer/productinfo/systemreqs/#os">system requirements page</a>, they care only about Firefox, Mozilla or SeaMonkey. Tough luck if you use something else (by the way, according to the <a href="http://www.desktoplinux.com/news/NS8454912761.html">latest Linux desktop survey</a>, this something else is at least a quarter of Linux users). Imagine a critical security issue that will exist only when using Flash in Konqueror or Opera but not in Firefox (for whatever reason, like Adobe testing Flash only in Firefox) - care to guess what happens in such case? After all, you can give it a little test - just try to find out what happens if you complain to them that Flash doesn't work for you in Konqueror.


You know, I haven't been a huge fan of <a href="http://en.wikipedia.org/wiki/Moonlight_(runtime)">Moonlight</a>, but there's definitely at least one thing to like about it - it's open source. Doesn't work? Have a look at the code.


Not so with Flash player. The reason why the latest Flash doesn't work originally appeared to be its <a href="http://blogs.adobe.com/penguin.swf/2007/12/flash_player_9_update_3_final.html">new support of some Mozilla XEmbed-based extensions to the plugins API</a> (funny thing about that link is, it says that it finally makes it work with Opera, while in fact it's exactly the <a href="http://my.opera.com/csant/blog/2007/12/04/linux-opera-flash-and-plugins">opposite</a>). After adding XEmbed support to Konqueror, it still didn't work. The page about the <a href="http://developer.mozilla.org/en/docs/XEmbed_Extension_for_Mozilla_Plugins">XEmbed extension</a> has demo code hardcoding a hard dependency on Gtk2, so maybe adding Glib2 eventloop support will make it work? After all, the Flash system requirements page mentions this (well hidden in a footnote, if you look close enough), but not really, tough luck, even though the <a href="http://multimedia.cx/diamondx/">DiamondX</a> testing plugin already works. Do you know what a ballistic approach to debugging is? You either hit, or you don't. Here next hit is that this Flash version doesn't handle properly repeated NPSetWindow() calls, which however happens to be the case with Konqueror. So finally, does it work? Well, kind of, if you don't mind it crashing everytime you leave a page. And it crashes in XtRemoveTimeOut() (incidentaly, a function that should not be called by a Gtk2-based plugin). That's been already taken care of as well - it's enough to give Flash the user agent string from Firefox, suddenly, no crash (I have no idea how Maksim managed to find this out - probably involved sacrificing chicken or something).

So, when can you expect the latest Flash to work with KDE? I have no idea. There are some preliminary patches I've made and <a href="http://bugs.kde.org/show_bug.cgi?id=132138 ">committed to SVN</a>, those may or may not work for you, depends on how lucky you'll get. Given that people report random crashes with those, I really meant to say lucky. Right now I'm trying to debug another issue where a proper fix that should remove a race condition actually prevents it from working. I also can't get keyboard focus right, even though debug output from DiamondX seems to look correct and I can't see any significant difference to Firefox. Maybe my fault ... maybe not, given what I know about how Flash handles keyboard input (you don't want to know).Opera people apparently have been more lucky with dealing with this, their latest beta should work better, but I could notice some problems there as well.

So, sorry. E.g. OpenSUSE will probably ship KDE updates for this tomorrow, but I suppose you'll be better off using the one true browser for the time being, if you need Flash. Next time I provide KWin videos only on youtube, somebody please yell at me. A lot. I'll deserve it.

PS: Yes, I know a Flash developer contancted kfm-devel somewhen in the past asking about XEmbed support. That still doesn't make debugging Flash any less painful, which is why the workarounds for it will take unknown time. It also doesn't change anything about Adobe not really giving a damn about Konqueror or Opera, if they could suddenly drop backwards compatibility (even though the code is probably still there, if it can still call XtRemoveTimeOut() and crash on it).

<!--break-->
