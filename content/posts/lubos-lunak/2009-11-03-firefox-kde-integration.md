---
title:   "Firefox KDE Integration"
date:    2009-11-03
authors:
  - lubos lunak
slug:    firefox-kde-integration
---
<p>
Now that the mention of the <a href="http://en.opensuse.org/KDE/FirefoxIntegration">Firefox KDE integration</a> I've done has reached also <a href="http://dot.kde.org/2009/11/03/opensuse-kde-sneak-peek-lifts-lid-innovations">the dot</a>, I guess it's time for a couple of things that don't quite fit into an article but I should probably say them somewhere anyway.</p>

<p>
First of all, this is nothing against Konqueror. I still use Konqueror. In fact when a couple of months back there was a wave of "let's ditch Konqueror" voices on the planet and kde-core-devel, I was one of the people opposing that (and I still think it's not an option, as Konqueror is still the best KDE browser there is - Aurora is Qt-only, Rekonq is at version 0.&lt;somesmallnumber&gt;). For the next openSUSE release we will evaluate the possibilities again and Konqy may end up as the default again if it's considered to be up to the job. Speaking of which, Konqueror has never really been 100% default in openSUSE, as the desktop icon, which is what the common user uses, has always been Firefox, so we can also say the we just fixed the inconsistency (and it's <a href="http://en.opensuse.org/KDE/ReleaseNotes11.2#Default_Web_Browser">really simple to switch it back</a>). But the real reason was that many people are simply not satisfied with Konqueror, so we decided to try to not ignore the reality for a change. I myself use Konqueror, but if somebody else wants to browse the net on my home machine, they get Firefox. I don't like this, but what the hell, c'est la vie.</p>

<p>
So, when we decided to make Firefox the default, we also faced the problem that we switched to something that from KDE user's point of view sucked in pretty much all aspects except for the browsing itself. The idea of Firefox desktop integration on Unix ranges from not bothering with it at all, over using generic not-really-desktop stuff like mailcap, to thinking Unix==GNOME. Normal Firefox in KDE offers to open PDF files with Evince,  shows /usr/bin in a filedialog when you decide you'd like to open the PDF in some other application, has inconsistent (not just reversed) button order in dialogs and other yummy things. There have been attempts to solve this e.g. by creating a Qt version of Firefox, but those AFAIK have never led to something usable in practice, and with WebKit now part of Qt I somehow fail to see the motivation for anybody to try once more. And in this situation we had just a short time before openSUSE 11.2 feature freeze.</p>

<p>
The trick, of course, was using magic. The Firefox with KDE integration is still the same Gtk Firefox, just with a bunch of hooks calling an external helper. I don't have the ability of some other KDE developers to have clones, and I'm not crazy enough to try to mix Gtk and Qt in one process (which, despite the possibility of a shared event loop, should be nowhere near trivial). So it's nowhere near the extent of the Qt port, and maybe that's why it has worked out (as we all should know, perfect is the enemy of good).</p>

<p>
The helper and the patches at available in a <a href="http://gitorious.org/firefox-kde-opensuse">Gitorious repository</a>, they are made to match the <a href="https://build.opensuse.org/project/show?project=mozilla">openSUSE package</a> so they may not possibly apply cleanly to upstream sources, but feel free to play with it. Actually, you are more than welcome to do anything you want with it (wanna be the maintainer of it? no problem). With the problem more or less solved, and with other things to do, I don't have any further big plans for it. I will try to push this upstream if possible (I have no idea what faces will Firefox developers make when they see it), but besides that, there are still many other areas that need some magic. And, after all, the ultimate plan is still that Konqy will one day rule the world ;).</p>

<!--break-->
