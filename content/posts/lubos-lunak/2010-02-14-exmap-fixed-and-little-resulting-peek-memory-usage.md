---
title:   "Exmap fixed, and a little resulting peek at memory usage"
date:    2010-02-14
authors:
  - lubos lunak
slug:    exmap-fixed-and-little-resulting-peek-memory-usage
---
I have fixed Exmap, my still favourite tool to measure system memory usage, to compile with latest kernels, and also to work on x86_64 (the latter was a bit of guess-work, but I think I got it right). KSysGuard seems to be getting close, and with Exmap unmaintained by its author :( I don't feel like doing this forever, but for now, it's still possible to get exmap from my <a href="http://download.opensuse.org/repositories/home:/llunak:/kernel/">home:llunak:kernel repository</a>. And as I don't feel like trying to do cross-distro kernel packages in the buildservice, those not using openSUSE are left with either trying to package it for their other distro, or pick out the patches from the .src.rpm .

While I was at it, I had just a little look at memory usage. Since I had done quite some comparisons of KDE3's memory usage with other desktops in the past, the first thing that came to my mind was doing that quickly again. As these days LXDE appears to be the new lightweight kid on the block, I tried that one, and also Xfce. Finally there's TWM, basically just to show the memory usage without any desktop. All of them are default desktops on openSUSE 11.2 for a new user with a file browser and terminal open, the only exceptions being adding a mixer to the default Xfce setup for a reason that will be obvious later and not using the <a href="http://blogs.kde.org/node/3959">nvidia driver</a>. LXDE is from the X11:lxde repo, KDE version is 4.3.5 that'll soon end up in an online update. So, here it is (for those who don't want to find out what all those values <a href="http://ktown.kde.org/~seli/memory">mean</a>, the most important number here is the 'Effective Resident TOTALS').

<a href="http://blogs.kde.org/node/4165?size=_original"><img src="https://blogs.kde.org/files/images/kde.preview.png"></a>
<a href="http://blogs.kde.org/node/4164?size=_original"><img src="https://blogs.kde.org/files/images/lxde.preview.png"></a>
<a href="http://blogs.kde.org/node/4163?size=_original"><img src="https://blogs.kde.org/files/images/xfce.preview.png"></a>
<a href="http://blogs.kde.org/node/4162?size=_original"><img src="https://blogs.kde.org/files/images/twm.preview.png"></a>

Of course, this is not really comparable to my <a href="http://ktown.kde.org/~seli/memory/desktop_benchmark.html">old memory usage test</a>, for a number of reasons, such as this being x86_64 machine, the setup being different, and so on.

It's interesting to note that LXDE actually loses to Xfce. That 'python' there is in fact GMixer. That really shows that you don't get lightweight things unless you check the setup yourself. And it probably also shows that you can get lightweight things only if also your expectations are lightweight.

It also shows that KDE4's memory usage is not as bad some some might think, although it would be nice if somebody would be bored enough to <a href="http://ktown.kde.org/~seli/memory">analyse it in more detail</a>. There seem to be enough people bored enough to just complain about KDE4 performance but not do anything else, and this is actually pretty simple. Or do you need an Akademy talk for that or what?

<!--break-->
