---
title:   "On KWin wobbling and such stuff"
date:    2006-07-12
authors:
  - lubos lunak
slug:    kwin-wobbling-and-such-stuff
---
As some might have noticed, KWin is supposed to <a href="http://dot.kde.org/1152484104/">get compositing support</a>, allowing a wider range of various effects and replacing KDE3.x's separate kompmgr (developed by Thomas Lübking, based on the original xcompmgr, and according to e.g. <a href="http://linuxeyecandy.blogspot.com/2006/01/moving-from-metacity-to-better-city.html">this</a> doing rather well for its time). This is in line with today's general belief that compositing managers should not be separate but part of window managers, as it allows for example better syncronization of effects and their wider range (and it will also allow us to get rid of the plain C that kompmgr is, bleh).

What happened was that at the KDE four core meeting I had some trouble with my laptop and before Trolltech people managed to bring me another machine to use (thanks!) my possibilities were somewhat limited to things like talking to people (like to Zack about compositing managers), reading text files (like thoroughly reading docs about the new X extensions that compositing managers use or sources of already existing compositing managers) or thinking (like thinking about how much KWin's design would be affected by adding compositing support). Surprisingly it turned out mostly looking like rather simple thing to do and I actually wonder why there hasn't been already a small army of eager graphic coders trying to add that to KWin sooner. Anyway, it apparently ended up being me, so I tried it. Using Fredrik's great compositing manager (that was originally meant as kompmgr replacement for KDE4) was big help.

Now, to stop all the cheering, there's one problem. For officially being a GUI developer I'm awfully lame when it comes to graphics. KWin in the <a href="http://websvn.kde.org/branches/work/kwin_composite/">kwin_composite</a> branch should have better potential than kompmgr, but right now it can't even do as much as kompmgr. The plan was that the graphics part would be done by Zack, because, as somebody mentioned, he appears to be very talented in that area, however it seems the problem there is that he appears to be very talented in way too many areas, so I guess I rather shouldn't count on him finding time for this.

So plan B is to find that army of graphic coders and make it come out of hiding. It looks like I've already found one member, but more wouldn't hurt. I can do all the other parts like handling of effect plugins, tracking window damage and so on, but I apparently can't do the graphics part. So if you know OpenGL or XRender and want to see something more than just plain transparency and shadows in KDE4, -> <a href="https://mail.kde.org/mailman/listinfo/kwin">kwin@kde.org</a>. KWin seems to be in need of somebody who will design the system for graphical effects and code it.
<!--break-->
