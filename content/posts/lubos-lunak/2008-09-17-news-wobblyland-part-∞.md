---
title:   "News from the Wobblyland, part ∞"
date:    2008-09-17
authors:
  - lubos lunak
slug:    news-wobblyland-part-∞
---
Dear LazyWeb ... erm, I mean DoItYourselfWeb. As you may or may not have noticed, KWin now again defaults to compositing enabled, if possible (the <a href="http://blogs.kde.org/node/3652">self-check</a> will possibly still need polishing a bit, but that's why it's enabled by default now, right; and the little trick for detecting too bad performance needs some testing too). This is true for both to-be-KDE4.2 KDE trunk and to-be-openSUSE11.1 packages.

One of the problems with that is that there are a variety of graphics cards available out there and only very few of those in here. Meaning I actually have no idea if and how well it works e.g. with Intel i915 or Radeon X1300. Currently the logic for enabling compositing by default is 'is it nvidia or intel driver, if yes, try to enable, otherwise sorry' (yes, no ati/amd right now).

So, just in case you'd feel like, I created a <a href="http://techbase.kde.org/Projects/KWin/HW">page at techbase</a> with a list of cards, and you can add yours. The instructions are at the top. A nice list would be actually very useful to me. Thanks ;).
<!--break-->
