---
title:   "News from the Wobblyland"
date:    2006-10-31
authors:
  - lubos lunak
slug:    news-wobblyland
---
Current <a href="http://websvn.kde.org/branches/work/kwin_composite/">kwin_composite</a> branch can finally do some simple effects. The obligatory screenshot:

[image:2493 size=original]

(I'm good at impressive screenshots, am I not?) This screenshot shows the animation for a newly appearing KWrite window. This time no manual hacking in of the effect, this is what is in SVN. The window is smaller because ScaleInEffect makes the new window appear as scaling up to its size and it is transparent because FadeInEffect makes the new window appear as fading in. And just in case anybody wonders why the animation is not moving in the picture, then that's of course because it is just a picture. I admit it is somewhat dull that way, but I haven't figured out how to do better (well, I didn't trying that hard, anyway). There is the source if you want to see it for real.

In other news, I've added comments and design description to the code, restructured the TODO file and added also a commented howto plugin, so if somebody would like to see it in action with some other card than nvidia, or, after seeing it in action, thinks that it's not really that impressive or interesting, then you know what to do.

Technically, right now kwin_composite should about match Kompmgr's possibilities, of course with the added bonus of being able to develop much further (as soon as somebody does that development, indeed). Which means that COMPOSITE_TODO has now entries listing all the Kompmgr effects, saying that they should be rather easy to add, using either effects or changes directly somewhere in the compositing code. With the exception of shadows, as I still need to decide on how to do them.

Speaking of shadows, it is rather interesting how some people call these things "3D desktop". Sorry, it is not 3D, it is very flat and mostly very faked. If you have Compiz/Beryl running somewhere, just for example see how e.g. the shadows make the desktop feel a bit like 3D, but as soon as you rotate the desktop using the cube and look at it a bit from the side, it's suddenly flat. And shadows have also other interesting effects, for example if you configure them to be very large, very visible and very offset to the right, then configure the minimize effect to take very long and rotate the window during minimizing, then it is very visible that it is just a fake. See it? It is of course correct that the shadow rotates together with the window, but real shadows shouldn't really rotate also around the window. Anyway, not that I really think I could do better with KWin. The funny thing is that this doesn't really seem to be possible to solve in some nice way.

<!--break-->
