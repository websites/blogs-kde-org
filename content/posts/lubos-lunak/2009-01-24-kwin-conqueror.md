---
title:   "KWin the Conqueror"
date:    2009-01-24
authors:
  - lubos lunak
slug:    kwin-conqueror
---
I recently noticed that although I have already talked about using KDE4's KWin in KDE3 or any other window manager in KDE instead of KWin, there is one thing missing in the mix: Using KWin without the KDE desktop.

Yes, perfectly possible. KWin is actually a KDE application like any other (well, more or less) and so just like other KDE applications it can run in GNOME, Xfce or even standalone, as long as the KDE libraries are available. And while some people may wonder about the sense of this, <a href="http://linuxeyecandy.blogspot.com/2006/01/moving-from-metacity-to-better-city.html">some</a> <a href="http://ubuntuforums.org/showthread.php?t=62525">apparently</a> <a href="https://www.redhat.com/archives/fedora-test-list/2008-April/msg01018.html">see</a> <a href="http://ubuntuforums.org/showthread.php?t=392671">it</a>.

 So, let's have a look at how to use KWin in GNOME, Xfce or standalone. As the title suggests, there will be some resistance, but that can be overcome ;).

<b>KWin standalone:</b> Let's start with the simplest case. And also probably the least likely. It is possible to use KWin alone without any desktop, just like it is possible to use for example WindowMaker or FVWM. However, unlike those, KWin is not very suited for this task, as it is a pure window manager. Meaning, it provides only window management features and not additional elements of the desktop like the taskbar, desktop background or application launching. So while such usage is possible, I don't expect many people would try it, without additional tools to provide other desktop functionality. If you try it, it's simple - just launch KWin, that's it.

<b>KWin in Xfce:</b> In Xfce KWin must replace Xfwm4, the default window manager. This is done by running <i>'kwin --replace'</i> (e.g. using the Alt+F2 dialog). After this, you will have Xfce with KWin running, in order to get the same setup the next time, you also need to save the session - start logout and in the logout dialog make sure to check the option for saving the session. Now, the next time you start Xfce, it will launch KWin as it window manager ... except that, it won't (<i>IMPORTANT!</i>). Current stable Xfce versions have a <a href="http://bugzilla.xfce.org/show_bug.cgi?id=4793">bug that will make its session manager crash during startup with KWin</a>. This problem is supposed to be fixed with the 4.6 beta versions, but if you have 4.4, you will need a workaround before you first start Xfce this way:

<code>
d=${DISPLAY:-:0}
f="$HOME/.cache/sessions/xfce4-session-$HOSTNAME$d"
if test -r "$f"; then
    cat "$f" | sed 's/_RestartCommand=$/_RestartCommand=false/' >"$f".sav
    mv "$f".sav "$f"
fi
</code>

You should run a script with these contents before Xfce is started. I'm not sure what exactly is the Xfce equivalent of KDE's $KDEDIR/env/*.sh , but if you use Xfce, you should know better anyway. If you don't know, adding these commands to .profile in your $HOME should do too.

<img src="https://blogs.kde.org/files/images/bbb2_0.png">

<b>KWin in GNOME:</b> This one will be ever a bit more complicated. It appears that there is no UI way of replacing Metacity in GNOME and that choosing a different window manager is done using $WINDOW_MANAGER. So you can for example add <i>'export WINDOW_MANAGER=kwin'</i> to your $HOME/.profile file. Next time you start GNOME, it will use KWin instead of Metacity. However you will probably soon find out that for whatever reason Metacity is not only GNOME's window manager but also shortcut manager - Alt+F1 for opening the desktop menu of Alt+F2 for the run dialog will not work anymore. There appears to be no simple way to trigger those from command-line, but <a href="http://icculus.org/openbox/index.php/Help:GNOME/Openbox">at least OpenBox people have already run into this problem</a>, so why not reuse their solution with thanks? After you install OpenBox (openSUSE users can get it from the <a href="http://download.opensuse.org/repositories/X11:/windowmanagers/">X11:/windowmanagers</a> repository), there will be also tool <i>gnome-panel-control</i>. Now you need to bind Alt+F1 to command <i>'gnome-panel-control --main-menu'</i> and Alt+F2 to <i>'gnome-panel-control --run-dialog'</i>. Bad luck with GNOME's tool for custom shortcuts I guess, given what is written above, so you can try with <a href="http://hocwp.free.fr/xbindkeys/xbindkeys.html">XBindKeys</a> - please refer to its documentation for how to configure the shortcuts and how to make it autostart in GNOME.

<img src="https://blogs.kde.org/files/images/aaa2_1.png">

That's it. In all three cases, if you need to configure KWin, run <i>'systemsettings'</i> and find the relevant option there. Since KWin is usually seen as a component of KDE, you will need to have a look in several places. Shortcuts are together with other global KDE shortcuts in <i>'Keyboard&Mouse'</i>, functional settings are in <i>'Window Behavior'</i>, desktop settings are in <i>'Desktop'</i> and the looks are in <i>'Appearance'</i>. You can access most of KWin's settings also by right-clicking any titlebar to get KWin's popup menu.

Something more I think I should mention: I've normally used KDE4's KWin in KDE3 session (as in, for normal real work, not just testing) and I couldn't notice any performance impact because of this mix (perception, I don't normally use my computer with a stopwatch in one hand :) ). The overhead of running KDE4's KWin in KDE3 should be about the same like when running it in Xfce or in GNOME. After all, you can do <i>'zypper install kde4-kwin'</i> or an equivalent and try it yourself.
<!--break-->
