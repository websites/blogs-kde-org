---
title:   "openSUSE KDE bug squashing - take a part"
date:    2009-11-30
authors:
  - lubos lunak
slug:    opensuse-kde-bug-squashing-take-part
---
So, openSUSE 11.2 is out, and that means a lot of people start using it and, well, occassionally run into bugs and sometimes even report them. As much as 11.2 appears to be a fine release, this is bound to happen now too, and that means that the number of KDE bugreports for openSUSE in the Novell bugzilla will grow again and will need to be handled.

And now it seems to be a good time to do some housekeeping there. Many of the bugreports there are old by now, for older KDE releases, or even for openSUSE releases that are no longer supported (bye bye openSUSE 10.3). Having too many bugreports means a lot of effort needs to be spent just managing them - if developers and packages are supposed to fix problems, they first need to know which ones are important and should be worked on first. For this reason we have <a href="http://en.opensuse.org/Bugs:KDE/Screening">guidelines</a> for sorting KDE bugreports, however, with the recent the release of 11.2, there hasn't been time to review all recent bugreports, and many of other bugreports are not valid anymore.

This is exactly the time when you can help KDE in openSUSE. If you like the 11.2 release and would like to contribute back, if you've already wanted to contribute but didn't know how or thought that you don't have the required knowledge, or even if there is a bug you'd like to see fixed and would want to help the developers and packagers to have more time to concentrace on it, this is the chance. We are starting another KDE bug squashing session, with the aim to review and sort all <a href="https://bugzilla.novell.com/buglist.cgi?emailtype1=substring&emailassigned_to1=1&query_format=advanced&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&email1=kde-maintainers">KDE bugreports for openSUSE</a>. And it is not difficult - all you need is openSUSE 11.2 installed, the ability to use Bugzilla, and the <a href="http://en.opensuse.org/KDE/BugSquashing">howto</a> describing all the details.

So if you want to be part of the openSUSE KDE team, come (today, tomorrow, this week, whenever you want) to <a href="irc://irc.opensuse.org/opensuse-kde">#opensuse-kde IRC channel on FreeNode</a>, ask if you have any questions, consult the <a href="http://en.opensuse.org/KDE/BugSquashing">howto</a> and you can pick from the <a href="http://en.opensuse.org/KDE/BugSquashing/Bugreports">prepared groups of bugreports</a> and let's squash some of those bugs.
<!--break-->
