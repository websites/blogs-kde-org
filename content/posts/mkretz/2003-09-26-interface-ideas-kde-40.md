---
title:   "Interface ideas for KDE 4.0"
date:    2003-09-26
authors:
  - mkretz
slug:    interface-ideas-kde-40
---
So, finally my first blog entry...

Lately I was doing a lot of work on KCModule and related classes like KCMultiDialog that uses KDialogBase and KJanusWidget. It's all pretty nice and cool what functionality they provide but I think the interfaces can be a lot better for KDE 4.0. While working on it and hitting limits everywhere I sometimes have ideas on how to improve the interface but it's not possible in the 3.x cycle.

The problem is I will, for sure, forget these things after I finished my work so I should probably write these interface ideas down now. I'm not sure if we really want to have a kde4 branch in CVS already, but I would like to be able to commit interface ideas to CVS... or publish 'em somewhere else, but I really think CVS is the best place for that.

I'd be interested to at least tackle the KCModule interface (while I'm not sure anymore that an interface change is really needed if I add my KCModuleProxy class) and look into getting insertion of pages (instead of appending at the end of the list) for KJanusWidget/KDialogBase working.