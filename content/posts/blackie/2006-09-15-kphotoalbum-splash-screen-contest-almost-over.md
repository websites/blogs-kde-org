---
title:   "KPhotoAlbum Splash Screen Contest is almost over"
date:    2006-09-15
authors:
  - blackie
slug:    kphotoalbum-splash-screen-contest-almost-over
---
Just one more day (till tonight midnight), and the KPhotoAlbum splash screen contest is over. I am truely impressed with the amount and quality of submissions. Have a look at http://www.kphotoalbum.org/splashscreen.htm

I promissed the KPhotoAlbum community that I for once (ahem) would not be a cruel dictator, but instead allow them to vote on which one we would use. And am I glad I did that, there is at least 5-6 which I simply can't choose among, because they are so good :-)

Any suggestions on how to run such a vote is more than welcome.
<!--break-->