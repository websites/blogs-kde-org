---
title:   "The movie: World Trade Center"
date:    2006-08-12
authors:
  - blackie
slug:    movie-world-trade-center
---
<p>I just don't know, I really don't know. Whether I liked the movie, that is. Had it been completely fiction, then I would have scratched it right away. Directly translated from Danish I would have called it <i>emotional porn</i>. On the other hand, this was not fiction, it was about a terrible event that has happend in my life time.</p>

<p>In any case, it was a very special feeling leaving the show room along with 200 other people, with no one saying a single word.</p>

<p>I learned a new word the other day (which I of course have forgotten again), then meaning of the word was something like <i>bitter sweet</i>, it was explained to me as <i>the feeling you have when you think of a happy moment with a loved one who is now dead</i>. I guess that rather well describes the feeling I had when I left the cinema.</p>
<!--break-->