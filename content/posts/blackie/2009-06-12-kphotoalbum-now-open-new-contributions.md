---
title:   "KPhotoAlbum - now open for new contributions"
date:    2009-06-12
authors:
  - blackie
slug:    kphotoalbum-now-open-new-contributions
---
The last three years have been no fun in the history of KPhotoAlbum. We have
been struggling with porting <a
href="http://www.kphotoalbum.org">KPhotoAlbum</a> to Qt4/KDE4.

During that period we for sure have lost many users that got bored from the
lack of releases, and also many contributors that got fat up by
listening to me saying no, no, no and no again to new features.

Recognizing the enormous work the port would be, I had the strong belief
that the best path to getting through it would be to
concentrate on the port alone, and not get lost in a million new features
at the same time. I'll let history judge if that was a good or a bad
approach.


Anyway, now we are through, and a KDE 4 release have been a few month ago,
and new development has started!

To that end, I've spent the last week on adding a <a href="http://www.kphotoalbum.org/getting-involved.html">new section to the
website trying to help people get started with contributing</a>. Among
other things a number of junior tasks have been identified.

So if you are interested in joining KPhotoAlbum development, why not drop
by #kphotoalbum at irc.kde.org this weekend, and get started with your
contribution.

<!--break-->