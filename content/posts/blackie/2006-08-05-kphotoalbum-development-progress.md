---
title:   "KPhotoAlbum Development Progress"
date:    2006-08-05
authors:
  - blackie
slug:    kphotoalbum-development-progress
---
<p>In my cubicle at work, there is this dude (Hi Steve) who almost everyday
complains that I never blog anything, so let me start doing that
occationally from now on - I'm sure Steve will remind me :-)</p>

<p>KPhotoAlbum development for the next release is really comming along real
nice now, there seem to be three major topics covered in the next release:
<ul>
<li> SQL backend. Tuomos is making good progress on his <a href="http://code.google.com/soc/kde/appinfo.html?csaid=1CC928D3AAB664D6">Google Summer of Code
project on adding an SQL backend to KPhotoAlbum</a>.

<li>Video support. Darn have I been asked for this like a million times, so
now it finally seem to be in KPA. So KPA will be able to categories videos
like images, and it is capable of playing them back.

<li>Improved support for annotating images. One of the cool thing about KPA
is its capabilities to annotate lots of images fast. The annotation dialog
is getting lots of minor but important improvements. Stuff like sub
categories now being shown in list boxes, items not
matching the typed text disapears etc.
<img src="http://www.kphotoalbum.org/categories.jpg"/></p>

<p>So everyone, please keep me on my toes of reporting progress here.</p>

<p>Cheers<br/>
Jesper.</p>
<!--break-->