---
title:   "Wireless in the sky"
date:    2006-08-07
authors:
  - blackie
slug:    wireless-sky
---
<p>Here is something new (at least for me), a blog written in 30.000 feet over greenland. I'm going back to beautiful downtown Bridgeport Conecticut, for what hopefully will be my last stay with a customer. Heck when I return from the customer in about four weeks, then I've been hand holding them for a whole year.</p>

<p>So next four weeks will be about fitness center visits, a lot of KPhotoAlbum programming from my hotel room, and perhaps a few visits to <a href="http://www.myspace.com/thegreenroomlounge">The Green Room</a> (My favorite bar in the area)</p>

<p>Talking about KPhotoAlbum, Tuomas (whose name I missspelled in my blog Saturday, sorry), has written a progress report on his KPhotoAlbum SOC report, see it at <a href="http://www.kphotoalbum.org/news.htm#item0025">The KPhotoAlbum News Section</a>.</p> 
<!--break-->