---
title:   "KPhotoAlbum support for Videos"
date:    2006-08-17
authors:
  - blackie
slug:    kphotoalbum-support-videos
---
<p>KPhotoAlbum development is going really well in the evenings these days. Currently I'm working on the video support in KPhotoAlbum. Actually, tonight I finished the HTML generation, check out <a href="http://www.kphotoalbum.org/export-demo/index.html">a web page with videos and images generated from KPhotoAlbum</a></p>

<p>In just 15 days I'm going home to my lovely girlfriend <b>permanently</b> (greetings encoded to my coworkers who've I've spent a year with now, and my girlfriend who I haven't), man am I looking forward to that!</p>

<p>Finally, if some of you out there want to become my star, then hurry send some Danish Candy to Holiday Inn, Beautiful Downtown Bridgeport, Connecticut (and address the envelope: "Dope for your Danish addict")</p>
<!--break-->