---
title:   "Coorporation Between KimDaBa and Bibble"
date:    2005-11-25
authors:
  - blackie
slug:    coorporation-between-kimdaba-and-bibble
---
I've been staying with a customer for month now and lives/work in a cubicle landscape - rather different from working from home which I'm used to.

After approx one and a half month I got to talk about digital images with one of my inmates, and I told him about KimDaBa. First, that is rather extraordinary that I manage to work almost two month with a guy without having told him anything about KimDaBa :-) What is much more extraordinary is that he previously worked with Bibble Labs, and is close friends with the owner. Bibble is an image processing application that works on Linux. WHAT ARE THE ODDS?
Well a new camera has been on my wishlist for a long time, but this opportunity was just too great, so I bought myself a new camera, and will try to see how KimDaBa can work together with Bibble.

The camera is a Canon Rebel XT and comes with a EFS 18-55 lense. With it I bought a Canon 50mm f/1.8 II camera lense, and a Canon EF 75-300mm f/4-5.6 III telephoto zoom lens.

So finally, everyone of you who have hoped for KimDaBa to be better with RAW files, stay tuned, now I've got a camera spitting them out, so it won't take long before KimDaBa does something with them.
<!--break-->