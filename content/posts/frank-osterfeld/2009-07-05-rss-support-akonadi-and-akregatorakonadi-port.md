---
title:   "RSS Support for Akonadi and the Akregator/Akonadi port"
date:    2009-07-05
authors:
  - frank osterfeld
slug:    rss-support-akonadi-and-akregatorakonadi-port
---
<i>If you're a developer interested in RSS in KDE (and maybe you are at GCDS right now), please scroll to the end.</i>

Akregator development was slowed down for quite some time: The development team was basically reduced to one person, me, and I had not much time for it. That's why Akregator only slowly recovered from porting regressions introduced during the KDE4 port. Also, the article storage layer showed its limitations (see startup time and memory consumption) and the metakit backend implementation lacks robustness, thus it became clear that something must happen to make both the user experience pleasant and hacking Akregator fun again. Another long outstanding wish of mine was to decouple feed list handling and item storage from the core Application to a central desktop service, to make both accessible to other applications without the need to run Akregator. 

Nowadays, the natural choice to achieve al this is to use Akonadi (although the idea was already in my head, before Akonadi even existed, I think). Luckily, Dmitry Ivanov came along and implemented RSS support for Akonadi (<a href="http://websvn.kde.org/trunk/playground/pim/krss/">SVN</a>, <a href="http://techbase.kde.org/Projects/PIM/RSS_framework_for_Akonadi">Dev-Wiki</a>) as his last year's GSoC project.
It's currently going under the working title "krss" (we're still looking for the final name, so let the suggestions come). krss currently comes with two Akonadi resources (local feed list and one syncing with the Newsgator online reader) and libkrss, a client library for convenient access. There is also a little RSS reader, krssreader, which serves as demo application. 

Dmitry did an excellent job here with designing and implementing krss, and also giving feedback (and sometimes debugging) to Akonadi. And, at least as important, he sticked around and now a year later, we decided that to go forward, stay motivated and to get to the real world usage scenarios, it's time for <a href="http://techbase.kde.org/Projects/PIM/Akonadi/PortingStatus#Akregator_Port">porting Akregator to Akonadi/krss</a>, improving krss while doing so. We started with that a few weeks ago and to my own surprise, this went quite smoothly so far. After a weekend, we had the basics ported and most of the old Feed and Article classes removed. Now, the port is already usable for feed reading (for certain definitions of "usable", that is). Data migration from Akregator also works, even if importing large Akregator archives shows some performance issues and the need for some more optimization sessions. Here's the mandatory screenshot:

<a href="http://www.kdab.net/~frank/akregator-akonadi1.png"><img src="http://www.kdab.net/~frank/tn-akregator-akonadi1.png" alt="Akregator/Akonadi port screenshot"/></a>

Doesn't look too exciting? The UI of course didn't change much while porting, the only notable change there is that krss doesn't group feeds in hierarchical folders but in a flat set of tags: Every feed can have n tags assigned, and the pseudo-tag "All Feeds" lists all of them.

So what are our plans with krss?

<ul>
<li>KDE 4.4, definitely: Finish the Akregator to Akonadi port and make krss ready for prime-time. The goal is to have all current Akregator features supported by 4.4.</li>
<li>KDE 4.4, if possible: move libkrss to kdepimlibs. This is needed to have applications outside of KDE-PIM use krss. As this implies keeping a stable and binary-compatible API, we will do this only for 4.4 if we think the API is ready.</li>
<li>Port applications, write new ones, add more shiny features.</li></ul>

Moving RSS handling to Akonadi, krss could help to improve the "RSS experience" for all of KDE by sharing feed list and item storage within existing applications such as Amarok, KTorrent or RSSNow! or easily writing new ones, e.g. a Konqueror plugin or a dedicated stand-Podcast client. Integration of RSS-enabled applications in KDE could be vastly improved, a topic deserving at least another blog entry. Its also time to reach out to the developers of  applications like the ones mentioned above to find out what's missing to fullfill their needs.

If you at GCDS and either working on an existing application using RSS, or have interest in krss and Akregator, just poke me. Or <a href="mailto:osterfeld at kde.org">mail me</a>, but it might take me some time until I read it). Depending on the interest, I might also set up a little BoF/informal meeting at the beach during next week. If you're not at GCDS, you are of course also invited to join the fun. It's the perfect time to get involved and make krss and its apps rock.
<!--break-->
