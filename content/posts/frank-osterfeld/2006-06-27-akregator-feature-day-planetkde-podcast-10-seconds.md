---
title:   "Akregator feature of the day: PlanetKDE podcast in 10 seconds"
date:    2006-06-27
authors:
  - frank osterfeld
slug:    akregator-feature-day-planetkde-podcast-10-seconds
---
As Roberto blogged about how he <a href="http://cablemodem.fibertel.com.ar/lateral/weblog/2006/06/26.html">converted his blog into a blog/podcast hybrid</a> using <a href="http://www.talkr.com/">Talkr</a>, I thought I should tell you about a well-hidden, half-finished feature in Akregator: Basic Text-to-Speech support.

To use it, you need <a href="http://accessibility.kde.org/developer/kttsd/">kttsd</a> installed and set up, which does all the heavy lifting for Akregator.

After you have got it running, start Akregator and enable the speech toolbar (Settings->Toolbars->Speech Toolbar in the menu):

<img src="http://osterfeld.pwsp.net/images/akregator-tts.png"/>

Now, just select the articles you want to have spoken and click the parrot to enqueue and play them. 

Isn't that great? All the people on PlanetKDE are podcasting to you now!
<!--break-->