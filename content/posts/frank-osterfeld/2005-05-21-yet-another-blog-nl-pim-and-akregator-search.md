---
title:   "Yet another blog, NL-PIM and Akregator search"
date:    2005-05-21
authors:
  - frank osterfeld
slug:    yet-another-blog-nl-pim-and-akregator-search
---
Ok dudes, now I finally started a blog at kdedevelopers.org. Working on <a href="http://akregator.sf.net/">Akregator</a>, a tool that many people read blogs with, I am not really an early adopter when it comes to blogging myself ;-). Well, there is the <a href="http://akregator.sourceforge.net/blog/">Akregator blog</a> where we blog from time to time what's going on in Akregator development, so check this out if you're interested.

I look forward to the <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">NL-PIM meeting</a> taking place next week in Annahoeve, Netherlands. It's the first time I actually meet the PIM people. In the last days we were thinking about adding more sophisticated search and filtering functionality to Akregator (maybe using something similar to the labels in GMail). So the challenge is to come up with a concept that is a) powerful and b) easy to grok and use. It might also make sense to have a KDE-wide solution for KDE4, instead of an isolated Akregator-only "we do it all the other way and confuse people" approach. Labels could be stored using Tenor (still waiting for the prototype, Scott ;-) ). I will discuss this on the PIM meeting (having usability people there is really a good thing) and hope that things get clarification.

P.S.: Tomorrow my CVS/SVN account gets one year old. Time for a little birthday party ;-)
<!--break-->