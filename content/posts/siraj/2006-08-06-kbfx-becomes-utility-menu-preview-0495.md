---
title:   "KBFX Becomes a Utility Menu ::Preview for 0.4.9.5"
date:    2006-08-06
authors:
  - siraj
slug:    kbfx-becomes-utility-menu-preview-0495
---
Hi.
After few months of coding finally this sunday I have some thing to write about. in the new version of kbfx "Code named kbfxPlasma-kde3" but don't confuse this with KDE4/Plasma I have a good reason to call this plasma ( look here >>> http://www.kbfx.org/siraj/preview/test.htm"). yes..it's water effect with some coloring and which I think looks bit like plasma?, lets try to see what kind of new features are going to be available on kbfxplasma..so what's this Utility thing? well Kbfx now supports a data interface where it loads it's data from plugins..separating GUI from Data..and who is to credit for this amazing idea? well..it's our Aaron again! I thank him a lot for his Super duper idea!  to make it real, we wrote the Whole KBFX from scratch..(from line one 10,000..(where it stands at this moment);

1.) UI Changes..
        in the current and the older versions of KBFX we had the layout hardcoded? which means the menu can take only one shape..but I found that as a reason for lot of artists making skins that looked like "Vista" and "XP"( -:) and I was to blame). now the Aritist has the FULL controll over where each widget should be placed and it's geometry. ok..I can give a quick screenshot of how this will look this is a mock but this is very simple to skin and the code is ready..Nookie is actually making the skin  elements. (u can find it
here>>http://www.kbfx.org/images/articles/20060525195007409_1.png)...

 We are also making a completely new Config dialog..and this time we are not using KHTML ( those who used it know ..why (:-)). 
	ok..the Screeny is here http://www.kbfx.org/images/articles/20060730222113394_1.png this s a qtdesigner ui file on display and PhobosK is making the signal/slots and try to make it work..it should be done as soon as he's back in Kbfx HQ>

the other MAJOR thing is that, we Use a QCanvas to draw the dynamic content..which makes it very fast to load and render .. and 100% flicker Free as QCanvas is Double buffered .. we some how got the mouse events to reach down to each QCanvasItem..and they detect MouseEvenent. and also all items support Alpha Channel.which lets u see the background?. now the new View has 3 Layers. 
1.) the Backgound
2.) The Items ( say if we can have 80% opacity and  u will see the background
3.) The Effects Layer..( which in the above Wink demo) lets u add effect like  water / fire ..
:-)( water is already done...Fire is in progress...)

4.) A big problem we had for months is the Scrollbars now the old one is gone, replaced and the new scroll bar is a result of lot the feedbacks we got from Usability experts..look here ((http://www.kbfx.org/siraj/snapshot28.png))

4.1) the size is not static..! and u can also have SVG themes so size was a big problem for many>> with this new code ..all that is will be fixed

5.) As you can see..the search box is gone up..and it's no longer a KHistoryKombo..rather a as u type search Field which displays the results on the view ...( bit more fancy I think). 
6.) what u don't see is the toolbars? OH..well tool bar is simply the bar on the bottom and the bar on the left on the mock. it just lets u drag/drop applications and application groups that u want "One Click" access so that u don't search or browse for it. so we will  have two types of buttons ..
1.) a button that expands
2.) a static button that executes a command .. 

6.) new keyboard support : yeap..it supports keyboard now..and we have some handy short cuts.too..when we release our new kbfx..we will make a note about the new way to run apps from ur keyboard..

7.) the Applications Groups can fold.. the exact demo is in new slashdot web site..where u click on the group header and the items fold..but with kbfx it also moves up..making the fold action more usefull. 


8.) I must say a word about the new Data interface or this blog will be incomplete. 
	ok the concept is simple and it's based on Aarons idea..so I don't want to take credit for his ideas. I just converted the English to Qt/C++ that's all. 
1. The data is not hard coded ? and the menu tries to load plugins which contain data. the menu has a single data Unit ..for example this can be a Single desktop file, or a AmaroK song?. or the file u opened last time?. and KBFX interface tries to display this data  Unit using a Render Unit..which means a QCanvasItem ...so the rest of the code is about how to glue this Rendering unit with the Data unit...and how to group and organize them.. we use things like QMap/QptrList to handle Grouping..so it's fast and since we are using pointer..no memory waste as well.
and this is why KBFX will act as a Utility menu..letting u load many types of data and not just applications. ... 

still there is a long TODO list..to complete so that we can give our "very caring KDE  users" ..... this is just a update..before some one actually sets a tombstone "with KBFX" label. so it's not dead..(-:)..only waiting a new child..)...we will release the Menu as soon as all the feature are finished..till then Have fun compiling KDE4..bcos as soon as this new menu is done..we try to finish the Qt 4.2 version.. where we will use OpenGL and interface with plasma data engine..!!!




bye!