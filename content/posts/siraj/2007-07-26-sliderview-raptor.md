---
title:   "SliderView Raptor"
date:    2007-07-26
authors:
  - siraj
slug:    sliderview-raptor
---
last several months have been a hard run, having exams one after the other, but I was able to commit several line now and then for raptor project, this is just one widget that we will use with raptor 
<href src="http://upload.ruphy.org/slideicons.mpeg">
<img src="http://upload.ruphy.org/snapshot017.png">http://upload.ruphy.org/slideicons.mpeg</img>
</href>
one of the main widgets we will have on raptor is the sliderview. it's a very customizable view unlike on kickoff, where it slides left and right, Raptor Slider View is meant to be 100% compatible with Plasma API and not be some widget of it's own. it's very similar to what u would see on a slide show but multiple slides per view. arrangement can differ based on user perf, It will support Stack like view and Grid view just like it was proposed on Appeal Project first by Aaron . the view can be skinned using SVG. at the moment i'm working with pinheiro (Amazing Oxygen Artist) and Nookie (good old Buddy) on the skin elements soon we will get the skin perfect and done. I uploaded a mpeg to ruphy's upload server so if can't compile it. see it here http://upload.ruphy.org/slideicons.mpeg. next step is to float it on plasma and add the True Transparency support to the menu view launcher. that should happen in few days from now. 