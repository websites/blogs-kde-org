---
title:   "Top Level Transparent windows on Mac "
date:    2008-09-20
authors:
  - siraj
slug:    top-level-transparent-windows-mac
---
video Link : http://www.youtube.com/watch?v=GLMrAx4AYqk

We all know making the top level window transparent is easy on X11 with XCompsite stuff. and I thought I could get away with some simple flag or a function call on mac to do the same. but when actually needed to do it.. I found that this is was impossible :(, unless Qt widget creation was modified. (http://trolltech.com/developer/knowledgebase/faq.2007-02-08.3940505976/).  so if you have the same problem as me.. here is a patch that enables Transparency on the top level windows.   this is very unfortunate I hope some day this problem will be solved, and if you know a better way to do it, without patching QT it would be helpful too :). the patch is kinda ugly so if you need it just drop me a comment or an email ;-)




