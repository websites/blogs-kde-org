---
title:   "KBFX: Lets Evolve"
date:    2007-01-15
authors:
  - siraj
slug:    kbfx-lets-evolve
---
Yes..the moment we release KBFX slik is down to hours. and we are working on the release and .getting the packages ready for you to download and install . and to give you a hands on experience of some things KDE4 plasma can bring you. While we finish up the work over here. we have a written a small booklet for you to read, about the up coming release . You can download the booklet in PDF format => KBFX 
<table style="width:auto;"><tr><td><a href="http://picasaweb.google.com/sirajrazick/KBFX/photo#5017577636355079202"><img src="http://lh4.google.com/image/sirajrazick/RaIERqfjfCI/AAAAAAAAAA4/hV0KiTNo8w8/s288/promosmall.jpg"></a></td></tr><tr><td style="font-family:arial,sans-serif; font-size:66%; text-align:right">From <a href="http://picasaweb.google.com/sirajrazick/KBFX">KBFX</a></td></tr></table>

0.4.9.3 SILK.pdf
Also some live demo can be viewed from http://www.kbfx.org/siraj/demo/ . this shows you kbfx menu in action 

links : 
1.) http://www.kbfx.org/siraj/KBFXslik.pdf
2.) http://www.kbfx.org/siraj/demo/


New Features :

1.) Faster Load time
2.) The most extensible menu Ever 
3.) search As u type
4.) Appealing and Easy Configuration
5.) More Stable then Ever
6.) Maximum Eye-Candy




