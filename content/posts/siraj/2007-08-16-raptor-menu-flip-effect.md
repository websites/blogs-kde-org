---
title:   "Raptor Menu flip Effect"
date:    2007-08-16
authors:
  - siraj
slug:    raptor-menu-flip-effect
---
Finally it was the day for playing with the QTransformations and QGV. and the result is kinda nice, but not perfect still . in the coming day's i'll probably make it more functional and put the 3D flipping effect in to more practical use. so basically what I have done is add some basic icons zooming and rotate the QGV around  Y axis which make the view flip like magic. 
video is here : http://upload.ruphy.org/raptor3dflip.ogg

and screenshot below
<img src="http://upload.ruphy.org/shot02.png"/>