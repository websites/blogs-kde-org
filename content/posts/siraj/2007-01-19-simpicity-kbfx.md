---
title:   "Simpicity of KBFX"
date:    2007-01-19
authors:
  - siraj
slug:    simpicity-kbfx
---
two days after the release of kbfx silk in which we try to simplify the backends and front ends, we got a nice comment on kde-look. actually this was really unexpected. 

--
"I really dislike the latest kbfx, I wanted simplicity, that is why I switched to kbfx "

Disliking that's perfectly alright..but disliking for lack of simplicity ? hehe. very funny . i can just explain with two screenshots 

1.) KBFX the new "So called Complex Skin" 

<img src="http://www.kbfx.org/siraj/screenshots/snapshot97.png" align="left"> 

2.) then a screeshot of the so called "Simple Menu"

<img src="http://www.kbfx.org/siraj/screenshots/snapshot99.png"> 


now the strange thing is both these two new skins are using the same code but different skins. and different layout cofig with kbfxlayoutrc. I guess..this simplicity is too complex for the dude who posted the comment. any way..it just made me make a another skin for kbfx so it's alright i guess. * and maybe one skin can be complex than the other , but don't make the whole application complex. :-)
also really glad that the new code is able to deliver the complexity and the simplicity using the same code base .

