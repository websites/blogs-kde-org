---
title:   "Krossing the borders to KDE-Bindings"
date:    2008-07-11
authors:
  - moenicke
slug:    krossing-borders-kde-bindings
---
The first day of the KDE-Bindings / Kross Meeting in Berlin at the KDAB Office is still going on. After starting slowly this morning, because of staying up very late after having fun at a restaurant here in this area where were waiting for all the people to gather together, we caught up very quickly with doing bugfixing and discussions about how to plug hot technologies together. Which ended up with a commit by Richard Dale who made an impressive demo on how to create a smoke lib and a ruby extension within 20 minutes. The example allows to script Ruby applications using QtScript. In the afternoon and evening we had presentations about the particular projects and languages of the developers, from Lua, Python, Ruby, C# and PHP to the Kross Framework and Kross Plugins as for KDevelop or Krita. So far I can tell this meeting seems to be coming together well, and we are all looking forward to having fun during the next day or two and end up filled with new ideas and solutions.

