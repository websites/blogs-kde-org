---
title:   "Calligra Words earns bronze"
date:    2011-06-21
authors:
  - dipesh
slug:    calligra-words-earns-bronze
---
After a long time of bugfixing, adding missing features and improving Calligra in general the <a href="http://blogs.kde.org/node/4423">progress</a> made on Calligra Words finally reached bronze status.

Over the last months we got it managed to replace the previous layout-code with a <a href="http://blogs.kde.org/node/4433">new layout-engine</a> that speeds up the layout-process by a factor of ~20. We where able to add nested tables, sections, different anchor-strategies and much more plus got the memory-consumption significant down. The effort to cleanup our source-code pays out in maintaining and extending the current base.

We where able to improve the compatibility with our native <a href="http://www.oasis-open.org/committees/office/">Open Document</a> file format and extended the filters that translate from the <a href="http://www.microsoft.com/interop/docs/officebinaryformats.mspx">Microsoft 95-2003 binary format</a> and the <a href="http://msdn.microsoft.com/en-us/library/aa338205%28v=office.12%29.aspx">Microsoft 2007-2010 Office Open XML format</a> to our native ODF format to a level where we are able to compete with free (as in libre) and commercial office suites.

Our collection of unittests did grow to ~1600 low-level QTest-based unittests and a few thousand high-level tests that operate direct on our <a href="http://websvn.kde.org/trunk/tests/kofficetests/">large collection</a> of ODF, Microsoft binary and XML files. Those tests are run after every commit in an automated way making it easy to discover regressions as soon as possible. Then we have tools for automated <a href="https://projects.kde.org/projects/calligra/repository/revisions/master/entry/tools/scripts/profileOfficeFileLoading.py">performance measurements</a> and <a href="https://projects.kde.org/projects/calligra/repository/revisions/master/show/tools/cstester">cstester</a> which is another high-level tool that compares the layout-results between commits using screenshots to test for unwanted side-effects.

In the last months the Calligra team growth to a size we never had under the old umbrella. Work on Calligra Mobile, Calligra Active, connectors for graphical shapes, a new poppler-based PDF-importfilter, change-tracking, a reference-tool and much more areas happened. With such a healthy project progressing well it's just a matter of time till we reach silver and then gold status to be finally forked as OfficeKit by Apple.
