---
title:   "To live and to let (others) die"
date:    2008-04-06
authors:
  - dipesh
slug:    live-and-let-others-die
---
Imagine you have a family - so something like one wife, 1.5 children and a dog - and you decided that it's better to feed them at least once per day and to provide some kind of home to survive during the cold days.

Both of those basic requirments are not that simple any longer since prices continue to increase on a monthly base while you still earn only around 4 Euro per hour (what is at least close to one meal for one person - ok, there are still those taxes that eat half of the 4 Euro, but it's at least still >0!) on your day-(and night-)job(s).

Then someday a quit usual thing happens. Your employer goes into an insolvency cause of mismanagment or to just increase the shareholder-value or whatever else. It will not be easy since there may a few days if not longer you will miss the income and there is still those family that likes to keep on to eat. Well somehow you got it managed and then...

...then a court asks you to pay back your last few months of salary you earned plus interest plus the money the court and lawyers do like to have for that decision. The reason? Well, cause YOU SHOULD KNOW that your employer may go into insolvency and bigger fishes still like to get money from your ex-employer and they are more important!

Strange? No, that's another new law we have here in germany since a few years to protect big fishes and there shareholders and it's already heavy in use. Welcome to the new world-order and we can promise you, this is only the start...

Details (only in german and only blogs cause such things are for sure not reported in our mainstream-press cause it would probably result in <a href="http://en.wikipedia.org/wiki/Image:Prise_de_la_Bastille.jpg">terror</a>);
http://karlweiss.twoday.net/stories/4826887/
http://www.spiegelfechter.com/wordpress/326/insolvenz-und-der-arbeitnehmer-ist-der-dumme

<b>Update:</b>
question; http://dip.bundestag.de/btd/16/062/1606297.pdf
answer; http://dip21.bundestag.de/dip21/btd/16/064/1606488.pdf

<b>Update 2:</b>
And for the case you are not from/in germany but from/in another part of Europe and still believe that this may a german-only thing, then I would like to point out, that this new law was introduced through the European Union backdoor (http://dip.bundestag.de/btd/15/000/1500016.pdf). So, it's very likely that same is valid for other countries within Europe too and that your mainstream-press just doesn't report about this.
