---
title:   "Cursor focus tracking using QAccessible"
date:    2010-07-20
authors:
  - dipesh
slug:    cursor-focus-tracking-using-qaccessible
---
<a href="http://websvn.kde.org/trunk/KDE/kdeaccessibility/kmag/">KMag</a> used to zoom into part of the screen got just today an additional mode: Follow Focus Mode. That means that kmag can now follow either the mouse pointer or the keyboard cursor.

<a href="http://kross.dipe.org/focustracking.mpeg">Video of KMag with 'Follow Focus' mode</a>

Extending for example KWin's full-screen zoom plugin can be done with something like;
<pre>
QDBusConnection::sessionBus().connect("org.kde.kaccessibleapp",
    "/Adaptor", "org.kde.kaccessibleapp.Adaptor", "focusChanged",
    this, SLOT(focusChanged(int,int)));
</pre>

This is work in progress and only works with Qt and KDE applications. Adding support for others shouldn't be that difficult with qtatspi. To try out checkout kdeaccessibility from SVN trunk (>=r1152354) and;
<pre>
export QT_ACCESSIBILITY=1
kmag
</pre>
