---
title:   "Progress on Calligra Words"
date:    2011-05-05
authors:
  - dipesh
slug:    progress-calligra-words
---
End of last week the work done on the <a href="http://blogs.kde.org/node/4403">new textlayout-library</a> got merged into the master-branch. Months of hard work on refactoring the basement Words, Stage and other Calligra applications are build up on got a face. While there still stays enough work we got beyond the mountain and are enjoying the fruits this brings us today and tomorrow.

This fruits, from the pov as developer and user of Words, are;

* More then 10-20 times faster layouting. We are layouting 100 pages in 1-2 seconds now (~20 seconds before, test-doc was the ODF 1.1 specs but that can be easily confirmed with just any ODT). This is independent of the used graphicssystem (native, raster or OpenGL) and without us spending a single minute on optimizing the textlayout-library yet. Those 10-20 factors performance-jump was reached by addressing design-mistakes made in the past.

* The new design enables nested tables, real anchoring of content e.g. within table-cells and even complex dependency-management between layout-areas. The later enabled us to fix the pain headers, footers, footnotes and embedded objects where before. We have full control over the layout-process now.

* Debugging is possible cause we don't depend on crazy combinations of QTimer::singleShot's connected with each other any longer. Backtraces showing the full stack-trace are a useful thing to have. This payed out during hunting of bugs within last days already.

* The textlayout-library is within <a href="https://projects.kde.org/projects/calligra/repository/revisions/master/show/libs/textlayout">Calligra libs</a> and designed to be reusable. In fact that library is already shared between different applications and plugins within Calligra.

* The clear separation between the layout-process and the applications/plugins will also help us on our mission to improve the unittest-coverage. We don't need to test against the applications or plugins any longer but can test the layout independent of them. We are even able to test only <a href="https://projects.kde.org/projects/calligra/repository/revisions/8b4da79c1414bd38c2cea7832091a33a88124d5e">small parts</a> of the whole layout-process now.

* As a side-effect the layout- and Words-code is way easier to understand and maintain. There are still more possibilities to decrease the entry-level future like adding <a href="https://projects.kde.org/projects/calligra/repository/revisions/master/entry/libs/textlayout/DESCRIPTION.txt">more</a> developer-documentation.

When everything wents well we will provide a first <a href="http://community.kde.org/Calligra/Schedules/2.4/Release_Plan#Snapshots">snapshot-release</a> ~end of this week. If you happen to have a look don't forget that we just land a huge Calligra Words refactoring last week. While all our energy is on stabilizing that work now, things may still be buggy. Please create bug-reports at <a href="http://bugs.kde.org">bugs.kde.org</a> for anything you may run into and help us to improve the quality of the <a href="http://www.calligra-suite.org">Calligra Suite</a>.
