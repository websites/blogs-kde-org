---
title:   "KDE Everywhere"
date:    2008-06-05
authors:
  - dipesh
slug:    kde-everywhere
---
Isn't it funny if you browse around the web and keep on to run into all those Eee-PC clones out there and note that nearly all of them are running KDE@Linux. Yet other examples I did run into are the <a href="http://www.acer.com/aspireone/pagina2.html">Acer Aspire One</a> and the <a href="http://www.linuxdevices.com/news/NS7223327753.html">ultra-mini PC</a>. Both are running Linpus (a for small displays optimized GNU/Linux-distribution based on Fedora 8 ) with the <a href="http://www.linpus.com/xampp/webmaster/Products/Linux9.3.htm">"Friendly KDE desktop environment"</a> :)

KDE4 will go there even some steps future with it's by-design cross-platform, modular and innovative architecture while KDE3 keeps on to be the perfect super-stable super-long-time branch for next few years where only bugfixes are allowed to land. What a good starting point into the age of the world-wide <a href="http://wadejolson.wordpress.com/2008/06/02/be-free-kde-new-image-series-starting/">digital freedom</a>.
