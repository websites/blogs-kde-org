---
title:   "Progress on Kross"
date:    2007-04-05
authors:
  - dipesh
slug:    progress-kross
---
It's a while I blogged last time and a lot happened during that decade. So, let's summarize some lately highlights around the <a href="http://kross.dipe.org">Kross</a> Scripting Framework;

The famous <a href="http://websvn.kde.org/trunk/KDE/kdeutils/superkaramba/">Superkaramba</a> got Kross as optional backend and guess what, we got it managed to stay backward-compatible. For example <a href="http://www.kde-look.org/content/show.php?content=24626">Aero AIO</a> and <a href="http://www.kde-look.org/content/show.php?content=32185">A-foto</a> are already running with the next generation of Superkaramba with Kross. Fantastic work Alexander Wiedenbruch provided here.

KOffice2 already rocks and scripting in the next generation <a href="http://wiki.koffice.org/index.php?title=KWord">KWord2</a> improved a lot. Beside dozend of sample scripts to demonstrate what's possible, KWord also got a nice python script to connect with OpenOffice.org using Kross and PyUNO to import content from all by OOo supported fileformats and it <a href="http://techbase.kde.org/Development/Tutorials/KWord_Scripting#Connect_KWord_with_OpenOffice.org">just works</a>. Meanwhile <a href="http://wiki.koffice.org/index.php?title=KSpread">KSpread2</a> offers a script that uses the <a href="http://opendocumentfellowship.org/projects/odfpy">ODFPY</a> module to export selected sheets to custom OpenDocument spreadsheet or document files.

The tutorials about scripting in <a href="http://techbase.kde.org/Development/Tutorials/KWord_Scripting">KWord</a>, <a href="http://techbase.kde.org/Development/Tutorials/KSpread_Scripting">KSpread</a> and <a href="http://techbase.kde.org/Development/Tutorials/Krita_Scripting">Krita</a> was assimilated by our great new techbase.

The python and ruby interpreter backends got tons of improvments and landed in <a href="http://websvn.kde.org/trunk/KDE/kdebindings/">kdebindings</a> finally, yay.

...and probably much more I already forget about :)