---
title:   "Coffice - Calligra on Android available now"
date:    2013-03-24
authors:
  - dipesh
slug:    coffice-calligra-android-available-now
---
<b>Introduction</b>

Coffice (Calligra Office or coffee-in-office) is a new project that tries to make <a href="http://www.calligra-suite.org/">Calligra</a> available on mobile platforms like Android, Blackberry 10, Jolla SailfishOS and Ubuntu Phone.

With the MeeGo-saga, where Calligra was the office suite that Nokia shipped with the N9, a huge chunk of focus went on trimming Calligra for mobile platforms, improving performance and compatibility with ISO OpenDocument (ODF) and Microsoft Office formats (binary and XML). When MeeGo got finished focus shifted to other platforms. Our always present Linux Desktop got extended with a great port to <a href="http://www.kogmbh.com/news.html#calligra-windows-alpha">Windows</a> and <a href="http://www.appup.com/app-details/krita-sketch">Krita Sketch</a>, both done by KO GmbH. More then a year ago we also saw a first port of <a href="http://blogs.kde.org/2012/01/12/calligra-android">Calligra on Android </a> that unfortunately never got polished enough to be published on Android's app-store(s).

Meanwhile I had the luck to build up <a href="http://necessitas.kde.org/">Qt on Android</a> expertise thanks to my employer <a href="http://www.kdab.com">KDAB</a>.

Enter Coffice.

<b>The Goals</b>

Unlikely previous attempts that always tried to bring a 1:1 port of Calligra to mobile platforms including all dependencies I defined different goals. Those are:

* Focus on a Calligra Words (word processor) ODT viewer. Since bringing a whole Office suite to another platforms is a huge task and I am a small team I had to focus. Later on I plan to add doc/docx support, editing, saving and Calligra Sheets (spreadsheets) and Calligra Stage (presentations).

* Slim! Ever since ~10 years ago when I joined KOffice/Calligra the size of the suite was overwhelming. A huge chunk is Calligra itself, that consists of a dozen of very specialized applications. That I addressed with my focus on Words, on a selected number of plugins. The other reason for that size are the dependencies. Among them kdelibs and all it drags in. During MeeGo-times we handled that with a slim hand-modified kdelibs but decided very fast that this was the wrong way. Instead we got or will get what is commonly known yet as <a href="http://community.kde.org/Frameworks/Building">kdeframeworks</a>. A much more modular kdelibs that solves the huge chunk of dependencies that are dragged on to other platforms where we (as app developers) are not particular interested in Linux Desktop integration but more into the platform that app runs on.

<b>Work Done</b>

* Coffice uses qmake rather then cmake. I decided for qmake cause qmake works out of the box on all mobile platforms that are supported by Qt. If cmake does that too, and its moving that direction with cmake being a first-class citizen in Qt5, it may make sense to switch to cmake again (or not) - don't have a strong opinion on that cause at the end its a tool to reach the goal.

* Coffice is 100% Qt-only. For that I introduced the coffice/fake library which maps kdelibs-API direct to Qt without all the functionality, without dbus, without daemons, etc. In most cases not even with implementation at all. Its a thin-layer to get Calligra or parts of it compiled and linking without adding any kind of patches to Calligra itself. My goal here was to *not* break the desktop (sounds familar? :) ) or uglify the code or make it more difficult to maintain that code at our first-class tear, the Linux Desktop.

* Most of calligra-libs compile+link, some selected plugins (the important ones) compile+link and Calligra Words itself compiles+links.

* I added a simple QML1 app to display.

* In total the size of the Package and installed application is 2.5MB (excluding Qt but everything else included).

<b>Work to do</b>

* Polish, get things fast, fix bugs, add Microsoft binary and xml filters, bring Calligra Sheets and Calligra Stage over too, etc.

* Compile versions for Blackberry 10 and Jolla SailfishOS (needs devices for testing, hint, hint :) ).

<b>The Result</b>

It works and the first version is available at <a href="https://play.google.com/store/apps/details?id=org.kde.calligra.coffice">Google Play</a> and can be installed on your Android >=2.1 device.

Screenshot:
<img src="http://kross.dipe.org/calligra/odfdoc.png">

<b>The Code</b>

In the Calligra repository you will find a branch named coffice. That's (a yet 2 weeks old) untouched Calligra master with an additional top-level directory named "coffice". All work happens within that directory. Long-term plan may to get that into master or not. It depends since a stable branch with controlled merges from head has also advantages. We will see.

In that directory is a file named "Calligra.patchfile" that contains a patchset against things outside of that "coffice" directory. That file is not supposed to be there and  plan is to rework the patches in there (~50 lines or so only left atm) and merge them into master. Currently its needed to "cd calligra && git apply coffice/Calligra./patchfile". Then "qtcreator coffice/coffice.pro" will load the project, allow to setup the target-environment(s) and produce packages.

<b>Like it?</b>

The <a href="https://mail.kde.org/mailman/listinfo/calligra-devel">Calligra mailinglist</a> welcomes developers and users.

All that software including Calligra and Coffice are free, have no ads, are opensource. If you like that please <a href="http://www.kde.org/community/donations/">donate to KDE</a> to keep things running.

Like to get Qt-software on to Android/BB10/Windows/OSX/Linux/...? My employer: <a href="http://www.kdab.com">KDAB</a> - we do platform independent software solutions.

Need expertise in ODF, OpenDocument, file-formats, Qt/HTML5: <a href="http://www.kogmbh.com">KO GmbH</a>

Commercial <a href="http://qt.digia.com/Product/Licensing/">license</a> and <a href="http://blog.qt.digia.com/blog/2013/03/13/preview-of-qt-5-for-android/">support</a> are available from Digia. Enjoy the <a href="http://creative-destruction.me/2013/03/15/benefits-of-co-existing-open-source-commercial-software/">benefits</a> :)

<b>Update</b>

Great feedback in the web related to the <a href="http://lwn.net/Articles/544250/">READ_PHONE_STATE</a>. Will set/raise the minSdkVersion and added a comment at related <a href="https://bugs.kde.org/show_bug.cgi?id=309678">bug 309678</a>. Thanks!

<b>Update 2</b>

Yes, its coming to <a href="http://f-droid.org/">F-Droid</a> too.
