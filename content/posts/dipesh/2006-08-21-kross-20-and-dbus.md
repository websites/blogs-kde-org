---
title:   "Kross 2.0 and DBus"
date:    2006-08-21
authors:
  - dipesh
slug:    kross-20-and-dbus
---
While my account on kdedevelopers.org exists for quit a long time, I just got the feeling that it may an idea to blog a bit about the progress done lately on <a href="http://dot.kde.org/1152490640/">Kross, the scripting framework</a> and in what direction it moves with kde4.

First let's look where it was with kde3. Krita, Kexi and KSpread from the <a href="http://www.koffice.org">KOffice</a> suite shipped with plugins for Kross. So, all of them had bindings to provide parts of there functionality to the <a href="http://www.ruby-lang.org/">Ruby</a> and the <a href="http://www.python.org/">Python</a> languages. Those bindings where language-neutral, what means all supported interpreter-backends had the same rich set of application-functionality to work with. Beside that some developers started to use Kross for there own projects. Just some days ago I got a nice Konqueror sidebar plugin which uses Kross to embed PyQt-widgets into konqi's sidebar (a lot of thanks goes to Krzysztof Lichota, fantastic work - the sidebar is named cbar and looks very promising :-). Well, Kross got contributions from various helpers. Some of who pointed bugs out, some who improved the <a href="http://kross.dipe.org/tutorial/">tutorial</a> written by Cyrille Berger, some just sayed that they like the idea to don't cut down to only one language. So, to all of you: a lot of thanks, you made kross2 possible! :)

Ok, so what changed with Kross 2.0? Well, nearly everything. Let's put it into a list;

1) It's not any longer needed to have bindings, just plug Kross into your application and its scriptable!

Kross now is able to deal with the new Qt4 QMetaType-system as well as QDBus. This means, that we you just need somewhere a line like "Kross::Manager::self().addObject(MainWindowInstance, "MyObject");" (where MainWindowInstance could be any by QMetaType known instance or just a from QObject inherited class). At the script-level we are then able to just "import MyObject" and work with the MyObject, so call the slots the object offers as they are scripting-functions, access the children the object has, etc., etc. Passing QVariant's or object-instances around as arguments or returnvalues just works transparently on the fly.

2) DBus

Since DBus is the way to go and since with the <a href="http://doc.trolltech.com/4.2/qdbusabstractadaptor.html">QDBusAbstractAdaptor</a> class there exists now an offical way of providing bindings, we are just able to reuse exact those classes. So, once your application supports dbus, Kross is able to reuse them as first-class citizen and offers transparent access to the scripting-backends. How does this differ from e.g. the python-dbus package? Well, first method-calls don't go through the dbus-socket and then it's not dbus-related at all except, that we are able to reuse what your application offers anyway: a clean interface to the outside world. But that's not all, we are not limited to what's possible with dbus. We are able to exchange instance-pointers what enables creation of e.g. PyQt widgets and passing them as native C++ widgets to your application (as demonstrated already with Kross on kde3). Think of the possibility to embed PyQt or QtRuby widgets into your C++ app :) Also we are not limited to the subset of functionality DBus offers. Full QVariant-support is not a problem and whatever the interpreter-backend supports, is possible.

3) hmmmm... ?

Well, it's all at an early stage. So, just some minutes ago I <a href="http://cia.navi.cx/stats/author/sebsauer/.message/c255a">commited</a> a very small testcase to show how the thing may look like once the port is done. Currently there are still bugs everywhere, missing features marked in the sources with TODO, the Ruby-backend does not work at all (so, only Python for now, will change soon) and there still stays a long way to go. Well, till Akademy I'll try to get it at least in a stage to fill some minutes of my <a href="http://conference2006.kde.org/conference/talks/2.php">presentation</a> (be warned, to listen to my presentation may hard since my english is on a george bush level, but I am at least a good entertainer :)
