---
title:   "SuperKaramba with JavaScript"
date:    2007-05-14
authors:
  - dipesh
slug:    superkaramba-javascript
---
Once in a while we had an interesting pool about the <a href="http://blogs.kde.org/node/2425/results">Preferred Scripting Language</a> where the result showed something of a concensus that Python and Ruby are the primary preferred scripting languages (from within those small list of possibilities the pool offered).

Just some weeks ago the next generation of <a href="http://netdragon.sourceforge.net/ssuperkaramba.html">SuperKaramba</a> got <a href="http://blogs.kde.org/node/2779">Ruby support</a> what means, users are able to write KDE4 desktop gadgets using Python or Ruby.

Just some minutes ago we extended those list with support for the KDE JavaScript language. Right, now even the - according to the pool - theird most preferred scripting language is supported and we also added the <a href="http://websvn.kde.org/trunk/KDE/kdeutils/superkaramba/examples/JavaScriptClock/clock.js?view=markup">JavaScript Clock sample</a> to demonstrate, that it just works :) During the recompile or the time till your distributor provides up-to-date packages, you may also like to take a look at the new <a href="http://techbase.kde.org/Development/Tutorials/SuperKaramba">SuperKaramba Tutorial</a>.
