---
title:   "Back into translation"
date:    2005-06-24
authors:
  - quique
slug:    back-translation
---
When <a href="http://dot.kde.org/1115285369/">KDE moved from CVS to SVN</a>, the old kdeextragear modules disappeared, and the extragear applications were dispersed in a number of new modules: graphics, multimedia, office, utils, etc.

This is of course way better, but I was in charge of translating kdeextragear-3 and kdeextragear-libs to Spanish (previously I had translated kdeextragear-2), and now I didn't know anymore what my task was: most of my apps were in the graphics module, which had already been assigned to other translator.

Anyway, I was getting a bit bored of updating the same apps (digikam, etc) over and over, so I asked <a href="http://jaime.robles.nu/">Jaime Robles</a>, the <a href="http://kurly.org/kde/">KDE Spanish translations</a> coordinator, to assign me one of the other new modules: <a href="http://websvn.kde.org/trunk/extragear/network/">extragear/network</a>. Today I got an e-mail from him, stating his approval :-) 

I'm about taking two weeks holiday in the beutiful Caribbean island of <a href="http://en.wikipedia.org/wiki/Cuba">Cuba</a>, but when I come back home, I'll start translating konversation, kmldonkey, ktorrent, etc right away.

UPDATE: No fly tickets left :-( That means I'll start translating on Monday.
 