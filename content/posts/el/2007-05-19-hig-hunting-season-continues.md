---
title:   "HIG Hunting Season continues"
date:    2007-05-19
authors:
  - el
slug:    hig-hunting-season-continues
---
Today Olaf published the second checklist in the scope of the <a href="http://dot.kde.org/1178743323/">HIG Hunting Session</a> - it is about <b><a href="http://dot.kde.org/1179568832/">Text and Fonts</a></b>.

Just like last week, we ask users of the <a href="http://dot.kde.org/1178891375/">KDE 4 Alpha</a> release to review applications along checklists and report infringements in the bug tracking system. More details in the weekly <a href="http://dot.kde.org/1179568832/">dot article</a>! 


Happy Hunting!


<!--break-->