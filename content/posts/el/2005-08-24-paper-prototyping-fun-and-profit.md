---
title:   "paper prototyping for fun and profit"
date:    2005-08-24
authors:
  - el
slug:    paper-prototyping-fun-and-profit
---
A few weeks ago, we did a session on [w:paper prototyping|paper prototyping]* at the Berlin Open Software Usability meeting. Sven was currently implementing a new PDF import dialog for <a href="http://www.gimp.org">the GIMP</a>, so we chose it for our exercise. In his <a href="http://svenfoo.geekheim.de/index.php/2005-08-24/paper-prototyping-for-fun-and-profit/">blog</a>, he describes the procedure and confronts the results of our prototyping with the current dialog. <a href="http://antena.de/antenna-flog/">Antenna</a> was our testperson and took some nice <a href="http://antena.de/antenna-flog/?page_id=110">pics</a>.

In <a href="http://conference2005.kde.org/">Malaga</a>, Tina and Florian will do a similar <a href="http://conference2005.kde.org/cfp/marathon/tina.and.florian-paper.proto.php">exercise</a>. Fun guaranteed :)


<br>

* oops - this wikipedia entry does not seem to exist! volunteers needed!


<!--break-->