---
title:   "Kivio User Research"
date:    2006-09-21
authors:
  - el
slug:    kivio-user-research
---
For the redesign of Kivio, Tina and me set up a user survey to learn more what people do with diagramming applications, what types of diagrams they create and what features they really need. The results of the survey are important in several ways:

<!--break-->

<ul>
<li><b>Feature Priorisation</b><br>

In the free comment fields, the participants gave valuable insights into the way they create diagrams, what they like and need with regard to diagramming applications, and what they do not like. A big part of the answers was very detailed and showed a deep understanding of their requirements.<br>

One of the most important features for the next generation of Kivio is higher-quality stencils, customised stencils and stencil sets, as well as auto routing. For more details, see the <a href="http://wiki.openusability.org/kivio/index.php/Survey_Results"> summary of results</a>.</li>

<li><b>Target Users </b><br>

As the next step, we tried to find distinct groups of users among the participants. In order to find these groups, we did a two-step cluster analysis with the data. This, by the way, is a very important method for user research which is not supported by any free statistics software!  <br>

As a result, we found four user groups: The researchers, the professional system administrators or software developers/architects, the OSS developer, and the student of computer science. Here are four typical representatives of these groups (hehe, never mind if they do not 100% fit yourself):


[image:2375 width=600 class=showonplanet align=center]


If you want to learn more about them and their relation to diagramming, let me know.

</li>

<li><b>Follow-Up Interviews and Tests </b> <br>

A big part of the survey participants left their email address which allows us to ask them questions if we need further input. Last week, I thought about the interaction paradigm of the shapes editor. I wondered how often people actually need to modify their shapes, if these are permanent modifications or just for the current diagram, etc. The participants who volunteered to participate in further studies were of high value here, as I simply sent them an email and asked to answer some questions. Thanks to all who answered, and sorry for my email provider bouncing gmail! It's going to be fixed soon. 

</li>
</ul>



<b>At aKademy, we will perform further user research:</b> We would like to observe users of diagramming software perform typical tasks. So if you are a user of a diagramming application and attend aKademy, be welcome to participate! It would be great if you could bring your favourite diagramming application =)





 
 