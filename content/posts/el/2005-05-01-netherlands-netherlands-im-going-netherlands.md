---
title:   "Netherlands, Netherlands, I'm going to the Netherlands :)"
date:    2005-05-01
authors:
  - el
slug:    netherlands-netherlands-im-going-netherlands
---
Today, Fab confirmed my accommodation for the KDEPIM meeting in Achtmaal, Netherlands. There are at least three reasons why I'm really happy to go there:
<br>

<ol>

<li><b>"Speaking Usability"</b><br>
During the last two or three weeks, I've spent a lot if time thinking about how to facilitate the usage of Kontact for less experienced users. Cornelius, Danimo and me discussed some ways, and I finally managed to create a few screenshots and wrote explanatory lines. The problem with such usability proposals is that they are mostly too long - so every developer pitches on the sections that are of concern for him. But without reading the whole document, the context does not become clear and some suggestions simply seem to be stupid =| And even if everybody agrees with the suggestions, they often do not fit in the current project plan and after a few months they are obsolete or forgotten. Meeting each other and "speaking usability" face to face is the best way to avoid this =)</li>

<li><b>Visiting the Netherlands</b><br>
Well, I haven't been there for at least five years!! After the KDE meeting, I'll straightly head to the sea, lie down in the dunes, and enjoy being there <b>*happyness*</b></li>

<li><b>Seeing some of you guys<br></b>
Lastly, I'll see some of you guys. Always a good reason to travel a few hundred kilometres =) </li>

</ol>

<!--break-->
