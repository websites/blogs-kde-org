---
title:   "Paris Group Foto"
date:    2006-06-27
authors:
  - el
slug:    paris-group-foto
---
Ken posted the group foto from the Ubuntu dev summit in Paris today:

[image:2146 width=600 class=showonplanet align=center]
http://bootsplash.org/14_3.jpg

Even if I really needed the recovery weekend afterwards in Paris, I much enjoyed the summit. Guess this was the first time I attended a FOSS developer meeting that had such a tough schedule: Based on spec topics that were formulated prior to the meeting, the "BoFinator" planned our days to attend meetings from 9 am to 6 pm in which we discussed and drafted the scope of each spec.

This approach may be a bit too strict for many freedom loving developers, but for me as a usability guy this was just great:  Usually,when I want to keep track of the things that happen in a project of Kubuntu's or Ubuntu's size, I either have to read cryptic techie mails, extract the essence and translate it to a human-readable language, or I have to ask stupid questions. Here, I can simply browse a number of specs with mostly self-explaining titles, and whenever something seems UI-related, I can subscribe to it. Wow =)



<!--break-->

