---
title:   "Is KDE accessible out-of-the-box?"
date:    2005-11-02
authors:
  - el
slug:    kde-accessible-out-box
---
Two weeks ago, I met Lars Stetten from <a href="http://www.linaccess.org">linaccess</a>. Linaccess is a project supporting free software for disabled people. Lars is partially sighted computer science student from Giessen. Some of you may know him from aKademy 2004 (there is an interesting interview with him on the <a href="http://dot.kde.org/1095054549/">dot</a>). 

Well, I asked Lars to show me how he usually works with KDE. I switched on the HighContrastDark-big colour scheme myself, but then gave the computer to him. I was interested to see if it is possible for a partially sighted person to use KDE out of the box, assuming a high contrast scheme is switched on. Lars is a long time KDE user by heart, but has not used a default KDE for a long time.

In short: We did a small usability test of an out-of-the-box KDE for partially sighted people.


<br>
<b>What do partially sighted people need in KDE?</b>

For partially sighted people, high contrast schemes facilitate the recognition of interface elements as the contoures are strengthened. There are different types of contrast schemes, small and big ones, black on white and white on black. Lars has problems to identify black font on light background and therefore uses the big dark colour scheme, which looks like this:


[image:1583 align=center width=400 class=showonplanet]

<br>


<b>So did KDE work out of the box for Lars?</b>

I'm sorry to say - it did not. Even if the larger part of the applications adopted the colour scheme allowing Lars to identify their UI elements, there were important applications that required further configuration. 

For example, the <b>Run Command</b> dialog did not adopt the dark colour scheme for its line edit, forcing Lars to manually configure it. Run Command is Lars' application starter as browsing the complex KMenu is hard for him.

The same accounts for <b>Konsole</b>: Command line tools are key applications for people who have problems identifying elements of a complex user interfaces because they allow for quickly launching and configuring applications. Konsole did not switch to the dark color scheme (see screenshot), but even worse, it did not react to the default keyboard shortcut <alt-s> to switch to the settings menu. It was therefore impossible for Lars to use Konsole.

This was only the result of a half hour session with one single, partially sighted person. Can you figure out how many other parts of KDE are lacking accessibility support?

<br>

<b> ... why I worry about that?</b>

In his <a href="http://dot.kde.org/1095054549/">dot interview</a>, Lars was asked about the support of handicapped working in Linux and Windows. He stated that the situation in Linux is not so good, but that the support in Windows is not better. There is only a tiny market for handicapped accessible software, which makes commercial software very specialised, inflexible and expensive. The software is sponsored by health insurance fonds in some (few!) European countries, but that is the exception from the rule. 

Open Source software has the power to fill this gap, if we would all (and I include myself!) listen a bit more to our <b>accessibility experts</b>. Instead, you often here the words 'I should make my software accessible? Why? There are other tools!'. 

I wonder: Which ones??


<!--break-->






