---
title:   "KDE HIG Configuration dialogs - Update"
date:    2007-03-13
authors:
  - el
slug:    kde-hig-configuration-dialogs-update
---
<b>@ 800x600 rule</b>

So far, I'm not sure if we should go for a fixed maximum size or a maximum default size. There is one good reason for a strict maximum size: It better prevents configuration dialogs from being overloaded. "Designing for 800x600" may be understood as "hey, it fits onto the page!". A tricky developer might reduce the height of scalable widgets like the table in the screenshot below, and argue that users can resize the dialog if they want to have a better overview over the element in the table. 

I expect that a maximum size will make you think more deeply of what options should be provided on a page, and what should go into an advanced popup.

[image:2719 class=showonplanet align=center]

Also, 800x600 is a rule of thumb. If you really really need more space, you can of course add ~50 Pixels to each dimension. And this rule applies to configuration dialogs only. Other dialogs will still be resizable.

The one strong argument against fixed dialog sizes is accessibility: When increasing the font sizes, the dialog should resize proportionally even for fixed sizes. This, however, should be done by the API. Is it feasible that we get this in the early life cycle of KDE4?


<b>@ Advanced sections</b>

As there was a comment against Advanced sections ("You never find what you are looking for"), I want to make sure that the guideline is understood correctly: The first choice should always be to embed advanced options with their context. That's why foldout sections or popup dialogs are preferred. Only when you have advanced options that don't fith into the other groups, provide an advanced section.

Foldout sections are still missing in the API, by the way ;-)


<b>@ Configuration menu</b>

In the standard section, I suggested to move notification into the general configuration dialog. I suggested this as there are various new types of notifications which are not covered in the standard notifications dialog. Notifications are an important setting and they should be covered in one place. That's why they should be located in the general configuration dialog.

I still think that toolbar and shortcut settings should remain in separate dialogs. It's a consistent location all over the applications, and they are rather advanced settings which shouldn't bloat the general configuration dialog.


<b>@ about:config</b>

One suggestion was to introduce searchable <a href="http://www.linuxjournal.com/article/8004">about:config</a> dialogs which list all configuration options in text form. I remember to have heard this suggestion at akademy as well, and wonder if it's something that is desired by a broader part of the community?



<b>@ Reset to Defaults</b>

I completely forgot about the "Reset to Defaults" guideline. In KDE3, "Defaults" works differently in <a href="http://blogs.kde.org/node/1805">(almost) every application</a>. What we need is a common solution. 

A quick-fix would be to provide a split button that provides two options: 

<ul>
<li>"Reset [page title]" e.g. "Reset Fonts"</li>
<li>"Reset all Settings" </li>
</ul>


<!--break-->



