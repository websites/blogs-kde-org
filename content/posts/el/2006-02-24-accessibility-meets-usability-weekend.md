---
title:   "accessibility meets usability weekend"
date:    2006-02-24
authors:
  - el
slug:    accessibility-meets-usability-weekend
---
Tonight, Olaf and Gunnar from the KDE Accessibility team will come over to Berlin. Together with the <a href="http://www.linaccess.org">linaccess</a> crew, we will perform usability tests with KDE's, and possibly Gnome's accessibility features during the next two days. <a href="http://tina-t.blogspot.com/">Tina</a> will support me on the usability side.

Our test participants will be members of the linaccess team, four partially sighted users and two blinds. From the linaccess side, the meeting was organised by Lars Stetten, a partially sighted student of computer science I've <a href="http://blogs.kde.org/node/1584">blogged about</a> some time ago.

Joko Keuschnig from the governmental accessibility competence centre <a href="http://www.barrierefrei-kommunizieren.de">barrierefrei kommunizieren!</a> offered us to host the meeting, and helped a lot with the technical organisation. Thanks, joko :)

I promised to record the test sessions, both screen signal and face/corpus of the participants, but I still don't really know how to do it. There is no tool for Linux that allows to record both, camera and screen signal without losing too much quality. In the scope of OpenUsability, <a href="https://blogs.kde.org/blog/72">Scott</a> is currently hacking gstreamer to provide us with a fully functional recording tool.

.... do you think you'll make it till tomorrow, scotti? 

<!--break-->

