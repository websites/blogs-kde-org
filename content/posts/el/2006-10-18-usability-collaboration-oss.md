---
title:   "Usability collaboration in OSS"
date:    2006-10-18
authors:
  - el
slug:    usability-collaboration-oss
---
Few OSS projects have a distinct usability community - and even in large projects like KDE and Gnome there is only a fistful of trained usability contributors. Why so? 

For an article which will be published in the German Open Source Yearbook 2007, I spent the last weeks comparing and analysing usability efforts in different OSS projects. Here are my top-four factors influencing usability in OSS: 

<ul>
<li><b>Vision and Target Users</b><br> When a clear vision is missing, usability work is undirected and ideas are likely to die in endless discussion threads. That is, for example, what happened to the <a href="http://openusability.org/projects/gimp">GIMP usability community</a> on OpenUsability. </li>
<li><b>Responsibilities and Decision Paths</b><br>Yes, we love freedom in OSS projects - but when clear responsibilites and decision paths are missing, or when usability is not promoted by the main contributors, usability people have a hard time getting their suggestions implemented. We experienced the first in KDE where it took quite a while to learn who to ask to get things done :)</li>
<li><b>Communication Channels</b><br>Separate mailing lists for devel and usability make it difficult to keep track of the others' considerations and decisions. Summarising discussions and making them available on a central place helps to avoid misconceptions. <a href="https://launchpad.net/distros/ubuntu/">Launchpad</a> is a good example for this.</li>
<li><b>Long-term Engagement and Motivation</b><br>Continuous engagement of usability people is preferable to one-time feedback as long-term goals can be followed. The above aspects as well as a friendly welcome by the community influence the motivation to stay.</li>
</ul>

It is great to see that the awareness of the necessity of a vision, of decision paths and usability has grown in the Open Source world. KDE tries to make up for this - as many discussions at akademy show. Still: Fresh OSS projects, like the collaboration software <a href="http://www.mindquarry.com">mindquarry</a>, are better off: They can define processes, decision paths and usability integration <a href="http://weblogs.goshaky.com/weblogs/page/mindquarry?entry=usability_in_open_source_projects">right from the beginning</a>.

<!--break-->

