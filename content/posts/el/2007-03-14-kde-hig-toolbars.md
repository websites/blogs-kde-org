---
title:   "KDE HIG: Toolbars"
date:    2007-03-14
authors:
  - el
slug:    kde-hig-toolbars
---
A few weeks ago, Celeste posted <a href="http://weblog.obso1337.org/2007/kde4-application-toolbar-specs/">specs for standard toolbars</a> for the major KDE applications. Now, we wrote down some <a href="http://wiki.openusability.org/guidelines/index.php/Guidelines:Tool_Bar">general guidelines</a>. Again, please review and give feedback in the comments section, or write to me (ellen kde org). There are still screenshots missing for the standard toolbars.

Especially, we'd appreciate feedback on:

* What standard toolbars (applications) and toolbar components (e.g. zoom toolbar, navigation in documents) should we add?
* Who wants to help us with implementation suggestions?
* If button groups are used, do we still need separators or do the button groups take care for visual separation?


<!--break-->