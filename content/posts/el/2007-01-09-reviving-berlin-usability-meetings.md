---
title:   "Reviving the Berlin usability meetings"
date:    2007-01-09
authors:
  - el
slug:    reviving-berlin-usability-meetings
---
<i>Let's give it another try! </i>

After both Berlin-based usability get-togethers ("Open Source Usability Stammtisch" and "Usability Professionals Stammtisch") somehow died during last summer, we decided to join the two efforts. Starting tomorrow, we'll have regular meetings (2nd Wednesday/month) at <a href="http://store.newthinking.de/Veranstaltungen/usability-stammtisch/">newthinking store</a> in Berlin Mitte. For each get-together, a topic is prepared by one of the participants and worked out during the session.

>> This week it's <b>Rapid Prototyping</b> - methods, tools, and what prototypes to use in which context.
Where? Newthinking store, Tucholskystr 48, Berlin Mitte
When? Wednesday, January 10, 19.30

For further information and topic suggestions sign in to the <a href="http://de.groups.yahoo.com/group/berlin_usability/">berlin_usability yahoo group</a>.


<!--break-->
