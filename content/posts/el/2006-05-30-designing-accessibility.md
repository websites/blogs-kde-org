---
title:   "Designing for Accessibility!"
date:    2006-05-30
authors:
  - el
slug:    designing-accessibility
---
As announced I summarised the results of our <a href="http://blogs.kde.org/node/1823">Accessibility meets Usability weekend</a> in a (long but interesting) <a href="http://www.openusability.org/download.php/104/usability-meets-accessibility-weekend-fullreport.pdf">report available on OpenUsability (PDF)</a>. We did usability tests with several KDE features for partially sighted people and the Gnopernicus screen reader for Gnome. The goal of the usability tests was not to achieve statistical data, but to gain an understanding of the needs of the represented user types.

As a general conclusion we found that while both KDE and Gnome provide very good tools to make the Linux desktop usable for partially sighted and blind users, they are lacking consistent support among the major desktop applications. In KDE, key applications like the text editor Kate or the shell Konsole did not apply high contrast colour schemes; in Gnome, the contents of crucial tools like the software installation (Ubuntu) could not be read by Gnopernicus and were therefore "invisible" for the blind users. 

Many of the described problems could be avoided if you try to keep the following guidelines in mind while developing:

<ul>

<li><b>Use scroll panels -</b> <br> with extremely high font sizes, a panel's height may exceed the screen height. If you do not provide scrollbars, the panels are likely to be "cut off" and a user will not be able to reach all contents without resizing.</li>

<li><b>Make dialogs resizable -</b> <br> if a dialog is not resizable <i>and</i> has no scrollbars, users may be unable to reach contents at the bottom. Make dialogs resizable <i>anyway</i> to allow every user to reach the confirmation buttons.</li>

<li><b>Make scrollbars exclude the tabs -</b><br> to keep the tabs as navigational device visible on the screen, set the scrollbar below the tab navigation.</li>

<li><b>Avoid complex and visually cluttered user interfaces -</b><br> for example double-nested group boxes, checkbox and button deserts, or overcrowded toolbars. Otherwise it is especially hard for partially sighted users to keep track of related items.</li>

<li><b>Avoid fixed background colours or images -</b><br> make them adjust to the overall colour scheme (except there are compelling reasons not to do so).</li>

<li><b>Allow your contents to adjust to the overall colour scheme -</b><br> you do not need to default the contents to the overall colour scheme, but allow the user to manually do so in an easy way.</li>

<li><b>Make all interface elements accessible via the keyboard - </b><br> fancy html widgets are cool, but make sure they are accessible.</li>

<li><b>Use the standard interfaces and widgets -</b><br> to make sure your application is and will be accessible later, use standard interfaces and widgets.</li>

</ul>

Olaf, Gunnar, Gary or the Gnome accessibility guys sure have some more guidelines to add! Maybe they want to write a follow-up blog? =)

[image:2048 width=300 class=showonplanet] [image:2049 width=300 class=showonplanet]
[image:2051 width=300 class=showonplanet] [image:2050 width=300 class=showonplanet]



<!--break-->
