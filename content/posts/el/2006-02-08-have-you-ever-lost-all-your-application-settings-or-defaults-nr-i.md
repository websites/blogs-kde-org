---
title:   "Have you ever lost all your application settings? (or: Defaults Nr. I)"
date:    2006-02-08
authors:
  - el
slug:    have-you-ever-lost-all-your-application-settings-or-defaults-nr-i
---
... because you pressed this button?

[image:1800 align=center width=320 class=showonplanet]


The <i>Defaults</i> button in KDE's Configure... dialogs resets all application settings to the factory defaults. In a tabbed dialog design, this does not become clear at all: The label does not tell if it refers to the current tab or to the whole dialog. For the user, this is especially difficult to learn as KControl behaves differently: Here, the <i>Defaults</i> button refers to the currently active tab only. 


<!--break-->