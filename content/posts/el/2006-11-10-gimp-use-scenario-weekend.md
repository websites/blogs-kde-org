---
title:   "GIMP use scenario weekend"
date:    2006-11-10
authors:
  - el
slug:    gimp-use-scenario-weekend
---
Our <a href="http://blogs.kde.org/node/2245">first OpenUsability sponsored student project</a> is making good progress: Last weekend I participated in a meeting to collect use scenarios for the GIMP, which was part of the student project offered by <a href="http://www.mmiworks.net/eng/publications/blog.html">Peter Sikking</a>. 

Together with <a href="http://www.kamilagiedrojc.com/">Kamila Giedrojc</a>, the first OpenUsability student, I spent most parts of the previous week to observe photographers, graphic, icon and interface designers in their natural work environment. The results served as input for Peter and his weekend session to develop use scenarios. We came up with eight to-the-point scenarios, sufficiently describing the full usage range of the next generation of the GIMP. Sometimes complex things simplify themselves - this does not require magic, but an experienced information architect.

In spite of being busy working with Kamila I much enjoyed <a href="http://zrusin.blogspot.com/2006/11/expressing-myself.html">Zack's and Simon's visit to Berlin</a> early last week. Hach, more KDE people move to Berlin, please!!


<!--break-->