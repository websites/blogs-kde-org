---
title:   "22c3 Work Results: File Browsing Behaviour - Report from User Interviews and a Konqueror Usability Test"
date:    2005-12-31
authors:
  - el
slug:    22c3-work-results-file-browsing-behaviour-report-user-interviews-and-konqueror-usability
---
This year, the chaos communication congress was productive for me: I finally manged to summarise the results of the user interviews and Konqueror usability test we did in August. 

Download: <a href="http://www.userbrain.de/kde/report_filebrowser-screening-test_08.05.pdf">Report in PDF [2,5 MB]</a>

Summary:

In a screening test on file browsing behaviour, 21 participants were interviewed on their habits and preferences regarding file management. Six out of the 21 participants then conducted a usability test with Konqueror, the KDE file manager. 

The goal was to get a  first insight on how users manage the information stored on their computer. Special consideration was given to typical file structures (e.g. broadness and depth of hierarchies), preferred views and icon sizes in a file browser, archiving and deleting behaviour, and usage of drag and drop.

As expected, file structures differed widely among the participants. Still, it was found that less experienced and average users often prefer broad over deep hierarchies, but at the same time try to avoid more than 20 files in a directory. In order to keep a manageable file structure, they delete or archive files from time to time. 

Regarding file views it was found that less experienced and average users are lazy to change the defaults, or to switch views in a certain folder. Drag and drop is a popular way of copying and moving files, but is less trusted than context menus.

Regarding Konqueror, the usability test identified some problems with drag and drop, the handling of the views, and inconsistencies in the interface. In the test, the users appreciated Konqueror's thumbnail previews of images, PDFs, HTML and text files. Regarding embedded views, they liked image and HTML previews, but encountered problems with the sound preview. All in all, the users claimed that it was quite easy to use, and that they would soon get used to the interface. 


<!--break-->