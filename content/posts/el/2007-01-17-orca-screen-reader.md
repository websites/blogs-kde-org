---
title:   "Orca screen reader"
date:    2007-01-17
authors:
  - el
slug:    orca-screen-reader
---
One year ago, some people from <a href="http://linaccess.org">linaccess</a>, <a href="http://www.barrierefrei-kommunizieren.de">barrierefrei kommunizieren</a>, KDE Accessibility and Usability (including myself) passed a weekend on testing the usability of FLOSS accessibility solutions - you might remember our reports from the <a href="http://blogs.kde.org/node/2052">Accessibility meets Usability weekend</a>. Among others, the <a href="http://www.baum.ro/gnopernicus.html">Gnopernicus</a> screen reader was usability tested by two blind users. Short after the test, the development of Gnopernicus was (at least partly) stopped for the benefit of <a href="http://live.gnome.org/Orca">Orca</a> which relies on AT-SPI technology.

As preparation for a workshop about German screen readers for Linux I'll attend next week, we now did a first evaluation of the Orca screen reader with Gnome. In cooperation with barrierefrei kommunizieren, I had the opportunity to test Orca with Sebastian, a blind student of computer science. Our focus was mostly set on functioning, but also usability and German localisation. 

All in all, we were amazed by the current state of Orca and its integration with the Gnome desktop. Application whose integration was problematic with Gnopernicus, e.g. OpenOffice.org, can be widely operated with Orca.

You can download the <a href="http://www.openusability.org/download.php/128/usability-meets-accessibility-II.pdf">full report (PDF, 719 KB)</a>.

<!--break-->