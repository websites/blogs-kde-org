---
title:   "World Usability Day - Berlin event"
date:    2006-11-13
authors:
  - el
slug:    world-usability-day-berlin-event
---
It's this time of the year again: Tomorrow, it's <a href="http://www.worldusabilityday.org">World Usability Day</a>, that means many events about usability all around the world. In Berlin, there is an <a href="http://usabilitytag.de">event at Meistersaal</a> close to Potsdamer Platz. 

If you happen to be around Berlin, have a look at the <a href="http://www.usabilitytag.de/programm.html">programme</a>. There will be a number of talks describing ways to better integrate usability with the development process - a topic OpenUsability has paid a lot of attention to during the last year.



<!--break-->