---
title: At the Akademy (2024)
authors:
  - ngompa
categories:
 - Akademy
date: 2024-10-10
discourse: ngompa
SPDX-License-Identifier: CC-BY-SA-4.0
---

While I've been part of the KDE community for quite some time (almost a decade now!),
Akademy is still a relatively new experience for me. This is only the second Akademy
I've been to in person, with the last one was Akademy 2022 in Barcelona, Spain.

This time around, Akademy was in Würzburg, Germany at the University of Würzburg.
As a lot of KDE contributors are based in Germany, I was excited to have the opportunity
to meet so many more than I did last time.

### Talking Fedora to KDE

This is the first Akademy I've attended where I did not have any talks, which meant I was
free to attend talks and BoFs to give feedback directly.

Carl Schwan's retrospective on KDE's accessibility goal and the new goal about improving
Plasma's input features led to great conversations with Carl Schwan and Jakob Petsovits
about experiences in Fedora with these aspects of Plasma and how to move forward on making
the experience great. In particular, we discussed the state of the [Maliit Keyboard](https://github.com/maliit/keyboard)
and the inability to have IMEs and on-screen keyboards working together. I attended a session
about Plasma Discover to talk more about our needs with Discover related to customization of
featured applications. 

Nate Graham led a great discussion on the release schedules of KDE software. In that, the
merits of aligning KDE Frameworks, Plasma, and Gear schedules was heavily debated. It was
probably one of the most exciting discussions I was in all week!

Timothée Ravier and I ran a small Fedora KDE meetup at Akademy, where we reviewed what has
been going on at the Fedora level and had a small discussion about the interaction between
Fedora and KDE.

I'm very excited by the steady progress being made across the board.

### Wicked Ways with Wayland

I wasn't just at Akademy to do Fedora things. I was also there to meet up with the KWin folks
and figure out the strategy of Plasma Wayland. Xaver Hugl, Vlad Zahorodnii, and I talked a lot
about this and wound up meeting up with Matthias Klumpp to discuss the needs of scientific
applications from Wayland. This led to us working together to develop [a protocol to support
"warping" pointers](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/337)
(to reposition a pointer in a given location in an application space).

Through this, I also started looking at some of the other protocols we use in KWin that are
candidates to stabilize and started on [the protocol used by clipboard managers](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/336).

Depending on how these go, I have more on my list to look at. 😉

### And more...!

There was a lot more that I did and attended, as the opportunity to meet up with so many people
could not be wasted! There's so much going on that I somewhat lost track of things too! 😅

Akademy was awesome, I'm energized and hopeful for the future of the KDE community and I look
forward to next year!

You too can be part of the awesomeness! [Get involved](https://community.kde.org/Get_Involved) or [donate to KDE](https://kde.org/donate/) to be part of the awesome!
