---
title:   "Trip to California"
date:    2006-05-15
authors:
  - stefan teleman
slug:    trip-california
---
Just got back from a nice trip to California. Mostly Berkeley and San Francisco.

Pics from this trip are <a href="http://www.flickr.com/photos/skipjackdes/sets/72057594135027664/">here</a>.

And yes, this blog is related to KDE. :-) The clue is in the photo set.

