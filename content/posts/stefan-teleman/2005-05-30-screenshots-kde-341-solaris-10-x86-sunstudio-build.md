---
title:   "Screenshots! KDE 3.4.1 on Solaris 10 x86 SunStudio build"
date:    2005-05-30
authors:
  - stefan teleman
slug:    screenshots-kde-341-solaris-10-x86-sunstudio-build
---
Here are some screenshots of KDE 3.4.1 running on Solaris 10 x86,
built with SunStudio 10. This is going to by my next KDE release
(along with the UltraSPARC build as well).

<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot0.png?BCt22mCB2w_GYGJ4">Screenshot 0</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot1.png?BCt22mCBub.mlScg">Screenshot 1</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot2.png?BCt22mCBshtjrqr.">Screenshot 2</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot3.png?BCt22mCBUGDH9wgs"> Screenshot 3</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot5.png?BCt22mCBDB_TmE7x">Screenshot 4</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot6.png?BCt22mCB4jOhybHZ">Screenshot 5</A>
<A HREF="http://us.f2.yahoofs.com/bc/429b6692_7bc6/bc/KDE341/KDE341Screenshot7.png?BCtB4mCBhBbSzAJ.">Screenshot 6</A>


In the true spirit of Internet Marketing, Yahoo! Briefcase does not seem to work. They did promptly ask, and charged me for, USD $34.99, to get a decent amount of space (130 MB). Oh, i almost forgot: in order to increase Yahoo! shareholder value by cutting costs, all the Customer Service requests or complaints are now handled by a very experienced team of drooling baboons.

So, i'm hoping this URL will work:

<A HREF="http://f2.pg.briefcase.yahoo.com/bc/osscreenshots/lst?.dir=/KDE341&.order=&.view=l&.src=bc&.done=http%3a//f2.pg.briefcase.yahoo.com/">Screenshots Directory</A>

