---
title:   "QT/Jambi on Solaris/Sun Studio 11"
date:    2007-06-11
authors:
  - stefan teleman
slug:    qtjambi-solarissun-studio-11
---
QT/Jambi, recently released, works on Solaris 10 with Sun Studio 11 and QT 4.3.0.

Mandatory pretty screenshot:

<a href="http://www.stefanteleman.org/qtjambi/jambisolaris.jpg">http://www.stefanteleman.org/qtjambi/jambisolaris.jpg</a>.

And some patches for Sun Studio/Solaris:

<a href="http://www.stefanteleman.org/qtjambi/">http://www.stefanteleman.org/qtjambi/</a>.

:-)





