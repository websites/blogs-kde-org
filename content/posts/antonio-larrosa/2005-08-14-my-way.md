---
title:   "My way"
date:    2005-08-14
authors:
  - antonio larrosa
slug:    my-way
---
Well, I've been so busy lately that I haven't had time to tell much of what I'm doing.

I've given 5 KDE talks in 2 weeks (3 one week, 2 at the next weekend). One in Tenerife at the Summer Courses of the "Universidad de Verano de Adeje", two at the Campus Party (in Valencia) and another 2 at Mollina, Málaga, into the second technological days of Free Software in Andalucia. I've met many people (some old good friends, some new friends), and everyone was great. 

I've also been checking the wifi ranges at the university with our wifi sponsor, and some people of Lima finally got the printed posters, t-shirts for the akademy-team and some other things we're preparing.

Also, some people have asked for more information on how to move around Malaga and mainly from airport<->residence<->university. For that I've updated the <a href="http://conference2005.kde.org/location.php">location page</a> of the akademy web site. But to make it easier, and since some people expressed their doubts about what they'd find when arriving to Malaga, I've gone and made some photos of the place so that you can see what you'll get (WYSIWYWG).

This is the photograph of the bus stop of Bus number 31 where you have to get out of it in order to go to the residence (the one just before the Plaza José Bergamín, which you can see in front of the image <img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4818.JPG">.

This is the front of the residence:
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4820.JPG">.

And now, this is the way from the residence to the university, shown in several steps, and with arrows in either black, white or green pointing the direction you should follow. I hope that clears any doubt of the route to follow.
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4821.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4822.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4824.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4825.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4826.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4827.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4828.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4829.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4835.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4836.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4837.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4842.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4843.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4845.JPG">
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4848.JPG">

And that one is the computer science faculty where the akademy is held . Now, just turning 180 degrees, you can see the hospital
<img src="http://developer.kde.org/~larrosa/akademy/route/IMG_4846.JPG">.

Hope that and the information I wrote here clears everything about the route to get from one place to another. Stay tuned, tomorrow
I'll add the route to get from the university to the residence :)

