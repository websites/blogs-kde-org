---
title:   "The future is written in tags"
date:    2006-04-25
authors:
  - antonio larrosa
slug:    future-written-tags
---
It's important to have information organized, and everyone who tried know it's difficult to organize things efficiently. Lately most applications are trying to help the user to do exactly that, and in my opinion that will be the factor users will use to choose one application over another: how the application allows them to organize the information they work with.

For example, amarok is doing an excellent job with that, it knows all the music files the users have and it keeps it very well organized. Users can search for an artist, for a song title, for the year the song was published, etc. without caring where the information is in the hard disk.

Another example is kimdaba/kphotoalbum or digikam, they both allow the user to create tags and assign them to pictures, which together with the exif information allows for very useful searches to be done in order to find specific pictures on user's albums.

So we have tags in kimdaba and tags in digikam, both applications write their data in different places (kimdaba in a xml file, digikam in a sqlite one), which is a pity and forced me to do a conversion script in order to import in digikam my kimdaba tags (and the corresponding image associations I did over a year of using kimdaba) once I choosed to just use digikam for downloading pictures as well as for organizing them.

That makes me think... why don't both applications store their tags in the same place? and going a step ahead, why doesn't every application store their tags in the same place? Maybe we could have a kind of tagging server for KDE 4, which applies tags for files. Having the tags and the file associations in a central place could allow for more interesting searches. Also, applications could register new tags automatically (for example, kaddressbook could create person tags for each of the contacts automatically, or korganizer could create tags in an event subfolder for each of the events). Having a centralized tag list would allow digikam, kimdaba, basket, amarok, konqueror, etc. to unify the tags that they would use to tag things.

I guess kat, beagle and others already do something similar like full text indexing and those things, but anybody knows if they offer tag lists too?

Something to think about.

Btw, if someone wants the kimdaba -> digikam tag/caption conversion script, just email me but keep  in mind that it's not yet meant to be used by "normal" users :) and currently you need to know python in order to use it.
<!--break-->