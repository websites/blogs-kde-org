---
title:   "University residence at aKademy 2005 and more"
date:    2005-06-27
authors:
  - antonio larrosa
slug:    university-residence-akademy-2005-and-more
---
Today I got a phone call from my <A href="http://www.uma.es/ficha.php?id=578">university residence</a> contact (a really nice and friendly person, btw). She told me that she was allocating the rooms everyone would get, but she had some problems...


First of all, there are many people who put names in the "room mates" section that either have not registered yet, or even have told me that they would be in a (different) hotel, so you can guess that's confusing for the person allocating the rooms and trying to please everyone. Also it's difficult to take into consideration the room size preference introduced into each person registration. I told her that in both cases (room size and room mates) they were not "a must" and so it didn't matter much if someone who asked for a 2 bed room is put in a 4 bed room or the other way around, unless you specifically have a reason to ask for something in particular (as some people do).

Also, she asked me if I told everyone the way the residence is organized. I think I did, but just in case I'll tell again. <A href="http://www.uma.es/ficha.php?id=578">The university residence</a> is divided into apartments, which have either one or two rooms. Apartments have either 2, 3 or 4 beds divided in the rooms as 2, 2+1 or 2+2 (in those cases, using <a href="http://en.wikipedia.org/wiki/Bunkbed">bunkbeds</a>). That mean you can be sharing an appartment with 2 other people but still have a single room, all for you.

Btw, each apartment also seems to have an internal toilet (with bath) and even a small kitchen. Here you have a photo of the residence:
<img src="http://www.uma.es/publicadores/servcomunidad/wwwuma/Residencia01.jpg">
(that kind of street is in fact a closed precinct)

Talking about other things, today I've been told at work that tomorrow I'll have to go to Barcelona to install some servers for a client. I'll go in the morning and go back to Málaga in the afternoon, so it will be quite fast, but I hope to find some time to call TSDgeos and meet somewhere in Barcelona.

Finally, some days ago I took some screenshots of a map that seems to not put restrictions into using its contents. Here you can see a view of the university (where the aKademy will be held) and then a view of where the university residence is (marked with a star).
<img src="http://developer.kde.org/~larrosa/akademy/map2.jpg" width="50%"><img src="http://developer.kde.org/~larrosa/akademy/map5.jpg" width="50%">.

As you can see, both maps can be put next to each other. The distance between both points is roughly 2 Km.

<img src="http://developer.kde.org/~larrosa/akademy/map6.jpg" width="377" height="230">

In that image you can see a full view of Malaga, with the position of the university, and the beaches :)  (no, the wifi from the university won't reach there, and btw, you don't want to get your notebook to the beach unless you want a notebook with sand everywhere...)

The original map is at
<a href="http://www.andalucia.org/modulos/Callejero/geo/mostrarMunicipio.php?modulo=Callejero&opcion=4&idioma=spa&provincia=MA&municipio=290672&x=20&y=9">this page</a> but once you've seen these screenshots, I would rather go to http://es.map24.com and search those places with the java interface, which works really nice and is more detailed.

Ok, time to sleep for less than 3 hours before getting my plane...