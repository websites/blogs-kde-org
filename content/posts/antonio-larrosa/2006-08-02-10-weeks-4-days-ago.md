---
title:   "10 weeks 4 days ago"
date:    2006-08-02
authors:
  - antonio larrosa
slug:    10-weeks-4-days-ago
---
Wow, it's been a long time since my last blog entry, exactly 10 weeks and 4 days...

A brief summary of the last weeks would be: a great time at a couple of talks (at the 802.party in Jerez de la Frontera, Cádiz and at the Technological Park of Málaga), some great time working on the digikam-web interface and related things, and boring time being until late at work (my advice: don't have anything to do with gsoap). Fortunately, today I've started my holidays :) and although usually I would use that time to develop something, this year I'm going to travel with some friends around Spain, so I guess I'll have little internet access from next Saturday on.

We're renting a car and travelling around 3000 Km. stopping at Mérida, Salamanca, Ciudad Rodrigo and other towns around Salamanca, Burgos, Pamplona, some small towns in Navarra, Zaragoza, the Pyrenees (the side of Navarra), Madrid and back to Málaga. If anyone thinks we cannot miss something in any of those cities, please tell me :) .

Btw, the digikam-web interface is practically complete, and this includes a python digikam module that may be useful for other things. For example, I've also implemented a couple of scripts to export and import the tags, commentaries and properties of the images in an album. That finally allows one to share not only the pictures, but also the tags and commentaries with other digikam users. Also, since different users may have the tags organized differently, there's also a tag conversion editor, that allows to map a set of tags to a different set of tags, save the conversion mapping, and reuse it when you import other albums from the same source.

Let's see if I can publish some of that before starting the travel.

Btw, yesterday I received a letter from the Ministry of Labour and Social Affairs of Spain acknowledging the time I worked in Germany between the 2000 and 2001 ... better late than never :) .