---
title:   "Lots of stories"
date:    2005-11-16
authors:
  - antonio larrosa
slug:    lots-stories
---
It's been a long time since the last time I blogged around here, so this will be a bit long, but anyway, I'll try to be brief to not use too much time writing. I've been wanting to write for many days, but always had many other things to do with more priority. Today I've thought "now or never", so let's try to explain everything that happened in the last weeks.

<b>the meeting</b>

First of all, I was invited, together with <a href="http://blog.mundolinux.net/">Cayetano</a> (the president of <a hreF="http://www.linux-malaga.org">Linux-Malaga</a>) to be part in the meetings of the local government for the "Plans for Strategic Actions for the Province of Malaga" organized by MADECA. Some weeks ago we went to the first meeting. There were representatives from different towns in Malaga, influential people from the university, directors of important technology business and associations, the president of the association of industrial parks, someone from the technological park of Andalucia... and Cayetano and me :). The discussions ranged from seeing the state of technology in the province, to seeing what could be done in order to improve it.
As a curiosity someone mentioned some people in some city halls have email addresses that they don't read since they're still used to receive faxes... That's Andalucia. In any case, it was interesting, it only lasted a couple of hours and there'll be meetings once every 6 months or so, so it won't take too much of my time . Btw, we definitely NEED business cards.

<b>the installation</b>

Some of you know that I didn't update my svn copy since around april or may, since I wanted to keep my computer stable during the preparations of the akademy and above all, I didn't want to loose any mail or be disconnected for too long. Now that akademy is nearly over (yes, it hasn't finished yet for me), I can update everything, so I did backups of everything on my computer and notebook (I'm the backup-everything,-don't-lose-anything guy),during a couple of weeks in which I burned 27 DVDs (8 music, 2 videos, 2 mails, 11 source code/configuration/akademy data/documents/general backup and 4 <a href="http://www.dvdisaster.org">ECC</a>, btw, I recommend using <a href="http://www.dvdisaster.org">dvdisaster</a> if you care about your CDs/DVDs). And finally, I was prepared to install <a href="http://www.opensuse.org">SUSE 10</a>, just some days before <i>their announce</i>.

The installation of the operating system itself was quite fast (to be true, it was nearly completely done automatically while I was having lunch :) ), but the reconfiguration of everything is a bit more slow. First, it took me more than a week to be able to restore my <a href="http://www.kolab.org">kolab</a> data into the new kolab 2.0.1 I installed (that means I was more than a week without being able to read mail). The reason was simple: I didn't really do a backup of kolab, but a full copy of it, which is not the same.
Anyway, I learned a lot about cyrus imapd (which kolab uses), so I can restore mails much more quickly in the future :).

In general, SUSE 10 is probably the best SuSE distribution to date. I was gladly surprised to see my rt2500 based wifi pci card as well as the ipw2200 wifi on the laptop being automatically detected and configured, which is something I couldn't imagine (specially since the rt2500 always gave me problems).

[image:1621 align=right size=preview width=131 height=180]

<b>the risk</b>

No, I'm not talking about the game, but about high-risk sports. I've started doing an introductory course on <a href="http://en.wikipedia.org/wiki/Rappel">rappel</a>, climbing and speleology with some friends. It's quite interesting and definitely a change from the last months (years?) in which I was basically being radiated with electrons coming from a Cathode Ray Tube for the whole day everyday. Now that only happens 6 days per week...


<b>the travel</b>
Two weekends ago, I went to <a href="http://en.wikipedia.org/wiki/Salamanca">Salamanca</a> with some friends to meet a good friend we have there. On our way there, we passed through Madrid briefly (just to change transportation), and some (other) good friends I have in Madrid went to take us to dinner on the brief period of time in which we stopped there. On that same weekend, we took the opportunity to go to <a href="http://en.wikipedia.org/wiki/