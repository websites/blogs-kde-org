---
title:   "Holiday report"
date:    2006-08-16
authors:
  - antonio larrosa
slug:    holiday-report
---
I arrived yesterday from a 10 days holidays around Spain. There are some things that I'd like to share with everyone, so I'll try to tell a brief report about it.

As I said in my previous blog entry, I've been with other 4 friends on a rented car travelling around Spain. We did some late changes to the planning, so finally, we went to: Mérida, Salamanca, Ciudad Rodrigo, Burgos, Fustiñana (a small town in Navarra), Zaragoza, Olite, Pamplona, San Sebastián and Madrid. In total: 3120 Km.

First, I'd like to say I think the people who manage the History Museum of Mérida (next to the Roman theatre), the Cathedral of Salamanca, and the Cathedral of Burgos are doing a fantastic job and should know that people thank them for that. On the contrary, the managers of the Cathedral of San Salvador (La Seo) in Zaragoza, the Basilica of El Pilar also in Zaragoza and the managers of the El Prado museum (in Madrid) are doing it quite bad in my opinion (specially the ones at El Pilar). Why? Because the first ones allow the public to make photographs as long as you don't use flash (which would damage paintings, walls, etc.) but the others don't allow to make any photograph at all, which I can't understand, since there's no difference between an eye seeing a picture and a photo camera capturing light to make a picture without flash. And why do you think that I said the worst were the ones at El Pilar? Because there's a professional photographer inside the basilica taking pictures of small children near the Virgin and later they sell the pictures to the proud parents of the children. And of course, he's even allowed to use flash. I guess his flash doesn't do any damage to the building. Incredible.

Talking about other things, walking around Salamanca I saw this, and <a href="http://blogs.kde.org/node/1425">I thought of Ellen</a>, so I had to take a picture for her. A traffic light with an even improved usability from the ones at Málaga with leds on the other side of the street.
[image:2255 height=200] [image:2256 height=200]

In Zaragoza, we went to a concert of "Los Delinqüentes" (those learning spanish, ignore the spelling). I didn't know the group a month ago, but I heard a few songs since then, and was prepared for anything. In general it was a funny and nice concert with a flamenco accent. I counted 21 people I saw casually smoking something which smelled "nice".

In Pamplona, the city hall organized a trip to see the meteor shower of the Perseids and we enrolled to it from Málaga. So at 22:00 we were waiting for the bus which would take us to the mountain of Unzué to see the stars, when we noticed that the bus would take us there at 20:00 instead of 22:00 . Having nothing better to do, we searched for Unzué in a map, and went there by car. After arriving to the town of Unzué and asking a group of around 10 children (the only persons we found on the street) for a bus coming from Pamplona, we found them and joined the group :). They were quite happy that we finally arrived, since they were waiting for us for a while, given that we organized the trip from so far away. Talking with some people around there, we talked about Linux and I took the opportunity to "evangelize" them into using KDE :) . Even after arriving so late and seeing few meteors (since we had a full moon), we have a good memory of that trip.

In San Sebastian, I found that the beach of "La Concha" (the shell) is even nicer that what I expected: quite wide, great sand, crystal-clear water... wonderful. I also found an interesting thing, a real d-bus !

[image:2257 height=230]

Our last stop was in Madrid, where I met some old friends with whom I had a great time taking pictures at the El Prado museum and along the afternoon.

In general a wonderful trip. Now I'll have to find some time to assign tags to the 2084 pictures I have from it (in total 6 Gb). Anyway, not all of those are mine. I only made 1500 ...
<!--break-->