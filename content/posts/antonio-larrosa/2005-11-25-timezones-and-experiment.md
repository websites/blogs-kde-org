---
title:   "Timezones and an experiment"
date:    2005-11-25
authors:
  - antonio larrosa
slug:    timezones-and-experiment
---
The other day I spent quite a lot of time at work working with timezones and daylight savings in an application. This application interacts with an airport system to get the times at which the planes arrive and so, it has to do some calculations for timezones and DST.

That part of the system is done in python, but altough python has a very useful datetime class, it somehow partly supports timezones. Fortunately, someone at #python pointed me to <a href="http://pytz.sf.net">pytz</a>, which is a very nice library to work with those things (and I can assure you that working with different timezones and specially daylight savings is a real pain). I've never liked discontinuities, and even less discontinuities in the space-time. The problem is that every year there are two such discontinuities. In Spring, there's a jump ahead, I mean, suddenly, a discontinuity in time makes a whole hour to disappear. And in Autumn it's even worse since the space-time fabric folds and a whole hour gets repeated, but you can do different things and even being at two different places at the same hour (think of the possibilities!), so it gets quite more complicated. Definitely not nice.

In the documentation of the pytz library I read something interesting. I quote: "the intention [of Riyadh Solar Time, tried in Saudi Arabia for 3 years] was to set sunset to 0:00 local time [...]. In the best case caused the DST offset to change daily and worst case caused the DST offset to change each instant depending on how you interpreted the ruling". I find starting to count the hours of the day when the sun sets quite interesting, I guess the basic idea is "let's make 0:00 have a meaning instead of being just a number that happens during the night". But why when the sun sets? My days don't start when the sun sets (some days my day is not even half done when the sun sets :) ). I would say it makes more sense to set 0:00 at sunrise, but then... the sun rises at different hours in different cities (specially but not uniquely if they're not in the same meridian), why should one city be setting the hour for the other cities? That doesn't look to me like the best way to force a kind of timezone where 0:00 is sunrise/sunset. That mean that in this hypothetical kind of time, each city would have its own timezone adjusted so that 0:00 is sunrise. But let's go one step beyond. Why should 0:00 be the time at which the rays of light from the sun arrive to the roof of my building? The day certainly doesn't start at that time, in fact, I'm most probably unconscious, sleeping at that time every day. For me the day starts when I wake up, so wouldn't it make sense to make 0:00 the time when I wake up? That would give a real meaning to 0:00.

So we've solved the problem of having a meaningful time at 0:00, the only problem with this would be that of synchronization with other people. When you talk by phone with a friend and he says "I'll be there at 10:00",you would have to ask him what time is it now, look at your watch, calculate the offset, and then adjust 10:00 from his timezone to yours. So, how can you know when will he really arrive? Ok, you don't know, but let's be serious, you cannot be sure of when he will arrive using standard watches neither, or do your friends really always arrive on time? Also, don't you have traffic jams in your city? so, why do you insist on trying to predict what will happen at a given time? 

Ok, I know this sounds absurd, but if it was tried for years on a whole country, why can't I put some more thoughts into the basic idea and give it a try?

Maybe I should use this "timezone" mechanism for a day or two in the next days. Would be a funny experiment.

... or maybe I shouldn't... it's already 20:10 ...
