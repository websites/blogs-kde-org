---
title:   "I should also try to blog more often"
date:    2006-03-27
authors:
  - antonio larrosa
slug:    i-should-also-try-blog-more-often
---
I was going to post something here when I noticed Martijn's post titled <a href="http://blogs.kde.org/node/1880">I should blog more often</a>, and like him, I think I should do the same.

My last post was in January, and there have been a lot of things happening in the meantime that even I'm surprised to find I didn't post about them. I'll talk in this post briefly about past issues, and I'll talk in the next one about the unfortunate events of last week.

For example, I didn't post about the great time I had at the Open Source World Conference with Till Adams, Josef Spillner, Andras Mantia and Alexander Dymo from KDE, as well as someone (sorry, I can't remember his name) from eGroupware and Lars Eilebrecht from The Apache Foundation. Apart of the parties and the travel to the Alhambra we made during the weekend after the conference finished on Friday, I got a nice feeling about the OSWC, I'm not sure exactly of the reason: Maybe the organization was much better than the previous edition, maybe it's because it was the first time I prepared a presentation in the Lessig style (90 slides for a 15 minutes presentation) which was well received, maybe it's because some of my friends (those friends who didn't know anything about my life as a linux/kde developer) went there and could see that linux has a lot of people behind it, maybe it's because I was allowed to go to the OSWC instead of going to work those 3 days of February... in any case, it was great, and there are photos to <A HREF="http://developer.kde.org/~larrosa/photos/OSWC-2006/images.html">prove it</a>.

Btw, I bought a book during the OSWC that I'm reading bit by bit during my scarce free time: <A href="http://www.oreilly.com/catalog/timemgmt/">Time Management (for system administrators)</a>. The book talks about system administrators and even at the beginning tries to explain why the book is only for them, but I find most of the ideas also useful for me, as a developer. And in general, it gives many hints as to how optimize and mainly, how to organize your tasks. You know, I've always been obsessed with time. One of my pastimes since I was a child was to optimize the way to go from one place to another taking the shortest route (sometimes even walking in exactly a right line across the street if the traffic allowed it).

I have a new habit since a couple of weeks too. Since there are at least 4 routes I can take with my car in order to go to work, I'm timing exactly how long it takes me to get to work following each of the routes. Also, the day of the week is an important factor since there's usually a traffic jam on a couple of routes on mondays and there're also traffic lights, etc. so I've calculated I'll probably have to be timing travels for nearly three months before being able to choose the optimized route for each day of the week.

Ah, I didn't post anything neither about the wonderful akademy-es in Barcelona, where I met known KDE developers/contributors like Albert Astals, Adeodato Simó, Isaac Clerencia, Jorge, Jaime Robles, Sebastia Pla, Pedro Jurado, Victor Barba, Alex Exojo, and many I'm sure I forgot. I also met some people I didn't know before, but who are in any case great KDE/opensource contributors too, like Pedro Lopez (from rosegarden, qjackctl, and other midi applications) and Antoni Mirabete. With both of them I had a very interesting talk that I hope is fruitful.

I also had a great time playing Mao (thanks Dato, Isaac and Jorge for showing us about it ), which is probably one of the funniest and most interesting card games ever.

My photos from the akademy-es haven't been uploaded yet, but I hope to do so some day, in the meantime, Jorge <a href="http://people.warp.es/~jorge/fotos/index.php?galerie=Akademy-ES">uploaded his photos here</a> (btw, thanks for the photo at the piano, I even seem to be playing :) )

Btw, I worked a bit during those days (specially on the plane back to Málaga) in a time tracker application for my employer. Tedial (the company I work for) has a new policy that involves having to mark the time spent in any of the assigned tasks everyone has. Up to now, I was using a mixture of karm (to know just the number of hours I work each week) and <a href="http://basket.kde.org">basket</a> (in order to track tasks and TODOs). The problem with karm is that the interface is not as handy as I'd like, the export format is not compatible with TimeTracker nor anything directly importable into the time database of my employer and it doesn't keep track of all the data I need, so I started a new project using pyqt, which I'm doing on my spare time, thus I'll put it on my homepage under a GPL license. In fact, I had to rewrite it completely during my flight back to Málaga, since I forgot to get a copy of the code I had already done (and no, the plane is not that long, it's just that with PyQt you can work _fast_).

I'll probably try to get some of the ideas of the Time Management book into this application. 

(continues in the next post)