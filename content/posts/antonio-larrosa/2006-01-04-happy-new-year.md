---
title:   "Happy new year"
date:    2006-01-04
authors:
  - antonio larrosa
slug:    happy-new-year
---
Sometimes I doubt if I have a blog or not since I write seldomly here, anyway, I wouldn't like to let the opportunity to pass to be able to say I hope all of you KDE developers, users, artists, and contributors in general have a successful year and can find lots of reasons to smile (yes, I say that 4 days later than most people, but hey, better that than nothing, isn't it? )

On a related note, I think most people here follow the Santa Claus/Papa Noel tradition, instead of the "Three Magic Kings" (dunno if there's a better translation for that). I've always thought that three persons carrying presents (and helped by three camels and three servants) can carry more than one fat guy in a bag (leaving apart that they generate more jobs). So think about it for next christmas.

Btw, for the end of the year party, I met a group of friends into the house of one of them. I was in charge of the music, so I took my notebook with amarok (from svn). I applied the <a href="http://www.kde-apps.org/content/show.php?content=31509">smartdj patch</a> in order to have the tempo of the songs visible. Unfortunately the patch doesn't apply cleanly to the svn version, but I applied it manually to the sources (I'll send my patch soon to its original author). Apart from some problems with iso8859-1 chars into the utf8 filesystem (which shouldn't have been there anyway), smart dj works really nice. Btw, the people at the party (mostly windows users) loved amarok :).



