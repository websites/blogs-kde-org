---
title:   "Trackballs and mouse wheel"
date:    2006-01-26
authors:
  - antonio larrosa
slug:    trackballs-and-mouse-wheel
---
Wow, I'm happy today.

The story starts some years ago (around 2001, if I'm not wrong), when I bought a trackball (a Trackman Marble FX from Logitech). This model has 4 buttons, the three usual ones on all mice, and a 4th button that (in Windows) was used to emulate a wheel (pressing that button and moving the ball, allows to have a mouse wheel that can  pan freely, horizontally and vertically on documents, which, btw, is really cool when working with images). After some time, I grew tired of having that useless button there and added support for it in XFree86 and Qt. I sent patches everywhere, but the XFree86 patch was sent when they were in a freeze and somehow they lost the patch. Anyway it was working for me so I didn't care much and just waited until I found that they added a patch to support wheel emulation done by another person that I never heard of (credits go to Henry T. So.) His patch was definitely his work since it was different from mine, not worse, not better, but different so I didn't have any problem with that (except for thinking about how many other patches were probably lost like mine). The only problem is that that patch didn't work, so I submitted <a href="http://cvsweb.xfree86.org/cvsweb/xc/programs/Xserver/hw/xfree86/input/mouse/mouse.c?rev=1.49&content-type=text/vnd.viewcvs-markup">a small fix</a> and finally linux had working support for wheel emulation.

Is it really like that? Not completely. Soon after that I had to upgrade my distribution and ... puff... the mouse wheel emulation stopped working. Don't ask me why, it simply didn't work, no press events were generated, xev didn't get any event... pressing that button simply was ignored. I blamed the new kernel (when upgrading the distro I also upgraded the kernel from 2.4 to 2.6), but I really didn't know the reason. Some years later I upgraded the distribution again, it still didn't work and I never found the time to get the X sources and look what was the reason for the breakage.

Since the button didn't work, I got used to ignore it and never press it. Some months ago I installed SuSE 10 and today I suddenly pressed the 4th button and noticed that the window scrolled up a bit. That made me think that if the mouse was generating again Button 4 press events, maybe the wheel emulation subsystem could work too (since the main problem with that button on my trackball is not the emulation itself, but to recognize the button press events, I guess that due to some Logitech protocol weirdness). so I went to my xorg.conf file, I modified the mouse section to read:
<code>
Section "InputDevice"
  Driver       "mouse"
  Identifier   "Mouse[1]"
  Option       "Buttons" "4"
  Option       "Device" "/dev/input/mice"
  Option       "Name" "PS2++ Logitech Mouse"
  Option       "Protocol" "explorerps/2"
  Option       "Vendor" "Sysp"
  Option       "EmulateWheel" "true"
  Option       "YAxisMapping" "4 5"
  Option       "XAxisMapping" "6 7"
EndSection
</code>

restarted the server... et voilá! the wheel emulation works again!

If you ever have the opportunity to try a two-directions scroll wheel emulation on a trackball, please do it. It feels extremely intuitive and it works more nicer than you can imagine.