---
title:   "A web interface to digikam (continued)"
date:    2006-05-03
authors:
  - antonio larrosa
slug:    web-interface-digikam-continued
---
During last weekend I managed to find some time to continue developing the web interface for digikam that I mentioned in <a href="http://blogs.kde.org/node/1961">a previous blog entry</a>, so I added some new features:

* When viewing the contents of an album, now it's possible to search for images/movies from that album having a tag associated.
* When viewing the list of albums, it's possible to search for images/movies among all albums having a tag associated.
* When viewing any set of images (like the contents of an album, or the result of a search in an album), you can click on "Download images" and it will generate a tar file in the cache directory containing the images the user is viewing in the selected size. If someone else tries to download the same set of images, the same tar file is reused.
* It's possible to configure a different set of albums for different users to see (using http basic auth), or a default album list.
* It stores in its own sqlite db the images downloaded by each user.
* I've created a script that reencodes the videos used in published albums using mencoder and sox. In the case of videos, I think it's better to pregenerate all of them in the cache directory since encoding videos takes too long to do in realtime (any commentary about that?). In my examples, I had a video generated with my camera that takes 220 Mb. After reencoding it, it takes 10Mb, so reencoding videos is a must.
* I've also tried to put all the configuration parameters in a single file so digikamweb is easier to publish.
<!--break-->

There are currently just a few TODO items (like having internationalized texts) in my list about this app, so I hope to consider it completed really soon.

  


