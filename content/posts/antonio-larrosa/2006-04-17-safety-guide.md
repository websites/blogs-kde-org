---
title:   "Safety guide"
date:    2006-04-17
authors:
  - antonio larrosa
slug:    safety-guide
---
This is a photo of a safety guide I found in my plane from Madrid to Málaga last Sunday:

[image:1940 size=preview]

I don't know how others read that, but what I read is that Iberia recommends "for your safety" not to use Windows ;-).<!--break-->