---
title:   "KDE talks"
date:    2006-01-26
authors:
  - antonio larrosa
slug:    kde-talks
---
It's been a long time since I blogged for the last time, so to compensate I'll post a few blog entries today :)

First, it seems this year I'll be giving some more KDE talks than last year. For a start, there's the <a href="http://www.opensourceworldconference.com">OSWC (Open Source World Conference)</a>, from the 15th to the 17th of February. I'll be meeting Till Adams, Alexander Dymo, Andras Mantia and Josef Spillner there from the KDE team, all signs are saying that the OSWC will be a good meeting point for long time not seen friends from other projects too, so it seems we'll have a good time there.

Also, in the beginning I thought I would have to fight hard with my bosses to ask for a morning to spend at the OSWC, but I was surprised when some days ago I was approached by my boss and I was told that my employer is having a shared booth with another company (*) and that since they were offered to give a talk, they thought that maybe I could give it.

(*) Note that my employer doesn't develop open source software, we just use it.

So I'll probably give two talks, one as a KDE member and one as a <a href="http://www.tedial.com">Tedial</a> employee, but both about KDE.

But maybe the best is that I was told I could go to the OSWC the three full days :)

Some other good news about the OSWC is that <a href="http://www.linux-malaga.org">Linux-Malaga</a> is having a booth, and since they're so nice (ok, that's subjective ;) ), they'll allow us to show KDE there and to give away over 500 Kubuntu CDs that Jonathan Riddell has sent me (thanks Jonathan!).

Another important event that is coming soon is the newly created (this year will be the first edition) Akademy-es. It will be organized in Barcelona from the 3rd to the 5th of March and the idea is to get together all the KDE users, developers and contributors from Spain. I'll blog more information as soon as it arrives :).

There's also another event, but I'm still not sure if it's completely confirmed or not, so I'll blog later about it.