---
title:   "I'm now the proud owner of a semi-b0rken HP nx5000 notebook"
date:    2007-04-10
authors:
  - pipitas
slug:    im-now-proud-owner-semi-b0rken-hp-nx5000-notebook
---
<p>So I now have a semi-working notebook for private Linux/KDE purposes again. May be able to build my own version (and follow the development of KDE(4)) again....</p>

<p>Paid 150.- EUR. Got it with a 120 GByte harddisk. It's a HP nx5000 model (same thing as 30 participants of aKademy 2005 [Ludwigsburg] may be familiar with). But it came with a specific, known kaputt-ness: </p>

<p>Its battery seems to be defective. It only boots when powered via the power supply (but there is no more "loading battery" light active, and in the BIOS the battery test is disabled). Unfortunately, I could not yet test it with an alternative battery, so it could also be a different defect (on the mainboard?) that prevents the loading process... </p>

<p>Is anyone of you familiar with that particular defect for that model? Does someone have a b0rken nx5000 laying under his/her bed, with a different defect, but with a potentially still working battery? I'll test it for you, and if it works for me, we can enter into price negotiations   ;-)</p>

<p>(But I'll be offline from end of this week most of the time, until May 5th.)</p>

<!--break-->