---
title:   "Will Google pick klik2 for GSoC 2008?"
date:    2008-03-15
authors:
  - pipitas
slug:    will-google-pick-klik2-gsoc-2008
---
<p>I've received 2 emails asking for more blogging and info about current klik2, especially since FOSDEM 2008.</p>

<p>Unfortunately, I don't have much time these weeks for 'FOSS-work', due to pressing 'work-work' duties. Since about October, I'm entangled deeply (too deep, if you ask me) in a major project that leads me to spend 5-6 days a week in Frankfurt (250 km away from home), with 12-14 hours a day on a customer site. Now compute: how much time does that leave me on evenings (in the Hotel) and weekends (at home), given that one-way travel time Stuttgart &lt;=&gt; Frankfurt is about 3 hours, and daily travel time inside Frankfurt is about 30 minutes?</p>

<p>This project takes much longer than initially thought. (Also, it involves only a very little bit of Linux, Solaris and Open Source, and much more MS Windows and proprietary software.) However, now there's the first light at the end of the tunnel. A few more weeks, and sometime mid-May my routines will be back to more normal.... *sigh*.</p>

<p>Anyways, while there are some more current klik2 development news, here's just one item for now: <a href="http://code.google.com/opensource/gsoc/2008/faqs.html">GSoC</a>!</p>

<p>The klik team has submitted to become a mentoring organization for the <a href="http://code.google.com/opensource/gsoc/2008/faqs.html">Google Summer of Code 2008</a>. If you are interested in some of the possible student projects, visit our <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008">GSoC idea's page</a>.</p>

<p>We give a short description of each idea, as well as listing required skills and expected difficulty level to implement it, the programming language to use and some reference links to study:</p>

<ul>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#klik2_Updater">klik2 Updater</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#klik2_Security_Framework">klik2 Security Framework</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#Enhancement/Replacement_for_fakechroot">Enhancement/Replacement for fakechroot</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#Packaging_the_klik2_client">Packaging the klik2 client</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#Porting_klik2_to_*_BSD_and_Opensolaris">Porting klik2 to *BSD and Opensolaris</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#Automate_klik_Recipe_Testing">Automate klik Recipe Testing</a> </li>
<li> <a href="http://code.google.com/p/klikclient/wiki/GSoC_2008#Replacing_Server-side_APT_with_Server-side_Smart">Replacing Server-side APT with Server-side Smart</a> </li>
</ul>

<!--break-->

<p>We do not yet know if Google will pick us. They'll announce the list of mentoring organizations early next week only.</p>

<p>We know some high-profile people and organizations are keeping an eye on klik's progress, and wishing us well. See for instance this statement we recently found: </p>>

<dl> <dt>Quote:</dt>
    <dd><i>"Beware proposals for new package formats. That way lies madness. (Except for breakthrough ones like klik. Once their suckage is reduced, that kind of thing will be very useful.)" </i></dd>
</dl>

<p>Where to find this? It was in a <a href="https://lists.linux-foundation.org/pipermail/packaging/2008-February/000702.html">posting</a> to the LSB-'packaging' list. Who said it? It was Dan Kegel, who works for Google (mainly on <a href="http://www.winehq.org/">Wine</a>-related stuff, AFAIU). Does this forebode a favorable outcome for our GSoC application? I don't know. Dan may not be involved with the GSoC decision at all, and not even care that much...</p>

<p> However, even if we aren't amongst the selected 'Google GSoC Few': this list of potential student projects should be of interest to anyone who wants to join us regardless of GSoC and help with klik2 development and testing.</p>
