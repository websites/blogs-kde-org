---
title:   "Upcoming KDE-3.4 is shining already"
date:    2005-02-26
authors:
  - pipitas
slug:    upcoming-kde-34-shining-already
---
It looks like the upcoming 3.4 release will have a gorgeous new default look and feel to it. Stephan Binner produced a <a href="http://ktown.kde.org/~binner/klax/">"sneak preview" live CD with KDE-3.4RC1 called "Klax"</a> (375 MByte size for the iso image), and OSDir now features an <a href="http://shots.osdir.com/slideshows/slideshow.php?release=265&slide=1">excellent series of updated screenshots</a>.
<!--break-->