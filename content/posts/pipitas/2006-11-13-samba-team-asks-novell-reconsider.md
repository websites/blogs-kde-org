---
title:   "\"Samba Team Asks Novell to Reconsider\""
date:    2006-11-13
authors:
  - pipitas
slug:    samba-team-asks-novell-reconsider
---
<dl>
<dt>Quote (incomplete):</dt>
<dd><i>"For Novell to make this deal shows a profound disregard for the relationship that they have with the Free Software community. We are, in essence, their suppliers, and Novell should know that they have no right to make self serving deals on behalf of others which run contrary to the goals and ideals of the Free Software community."</i>
</dd>
</dl>

For complete statement, visit <a href="http://news.samba.org/announcements/team_to_novell/">http://news.samba.org/announcements/team_to_novell/</a>.

<!--break-->