---
title:   "FBI can spy on you by remotely turning on your cellphone mic (even if it is powered down)"
date:    2006-12-10
authors:
  - pipitas
slug:    fbi-can-spy-you-remotely-turning-your-cellphone-mic-even-if-it-powered-down
---
<p>Did you know that the FBI (and therefore, the CIA, and probably most police and secret service organisations around the world), have technology to remotely turn on your cell phone microphone to listen to you and all conversations around you? The technique even has a name: 'roving bug'.</p>

<p>According to <a href="http://news.com.com/2100-1029_3-6140191.html">this report</a> by <i>c|net</i>, we may safely suspect that most current devices can be used for wiretapping even if switched off (because there a really complete power down never happens unless you remove the battery).</p>

<p>While technologies like this were suspected to exist by <a href="http://en.wikipedia.org/wiki/Tinfoil_hat">tinfoil-hat brigadists</a> since a long time, now it is kinda officially confirmed. This type of eavesdropping came to light in <a href="http://www.politechbot.com/docs/fbi.ardito.roving.bug.opinion.120106.txt">an opinion</a> published this week by U.S. District Judge Lewis Kaplan. Therein Kaplan ruled that the "roving bug" was legal because federal <a href="http://www.law.cornell.edu/uscode/html/uscode18/usc_sec_18_00002518----000-.html">wiretapping law</a> (click on of the 3 buttons at the bottom to access the real link) is broad enough to permit eavesdropping even of conversations that take place near a suspect's cell phone. </p>

<p>And declared "suspects" we all are, as soon as we need to be....</p>

<!--break-->