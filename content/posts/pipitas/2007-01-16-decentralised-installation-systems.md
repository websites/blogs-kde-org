---
title:   "\"Decentralised Installation Systems\""
date:    2007-01-16
authors:
  - pipitas
slug:    decentralised-installation-systems
---
<p>Thomas Leonhard, author of Zero-Install, has written an excellent <a href="http://osnews.com/story.php/16956/Decentralised-Installation-Systems/"> article</a> about <i>"Decentralised Installation Systems"</i>. I don't agree with every little detail of it, but it is definitely worth a read and worth some serious thoughts.</p>