---
title:   "One 'Meta KIO Slave' to bind them all...."
date:    2004-11-14
authors:
  - pipitas
slug:    one-meta-kio-slave-bind-them-all
---
<br>
<b><i>"My precious...."</i></b>
<br><br>
It seems George Staikos' recent <A href="http://osdir.com/Article2159.phtml">article for O'Reilly</A> featuring KDE's KIO-Slaves stroke the right chord with many people. It has prompted some additional activities.
<br>
First, it triggered me to <A href="http://blogs.kde.org/node/view/700">blog/write down</A> an old idea about how to increase the visibility of these powerfull tools to more users. (KIO-Slaves are are quite "old" to KDE. They are built into KDE already since a long time, but many regular KDE users are not aware of them, or their usefulness, or how to use them properly.)
<br>
Next there came a <A href="http://lists.kde.org/?l=kfm-devel&r=1&b=200411&w=2">thread on kfm-devel</A> which discussed several ideas. 
<br>
Last, Dik Takken came up with a <A href="http://www.kde-look.org/content/show.php?content=17800">wonderful proposal</A>. 
<br>
I think Dik's suggestion is much better than <A href="https://blogs.kde.org/images/a426a5d2e453c2e570116f57a4c85824-699.png">mine</A> (which I do withdraw now). Hopefully it will be implemented in KDE 3.4 already, not only 4.0 !
<br>
Dik's suggestion is to create a folder like shown below, and make it the default Konqueror location. He liked to call it "Virtual Filesystem". I prefer (at least for now, while we discuss KIO Slaves) to call it a "Meta KIO Slave". Both do the same: they give users easy "clickable" access to a whole bunch of real KIO Slaves, without needing to know what the exact syntax is for typing the correct <A href="http://en.wikipedia.org/wiki/URL">URL</A> into the Konqueror (or FileOpen, SaveAs,... ) location bar line edit widgets.
<br>
Here is one of Dik's illustrations of the idea:
<br>
<IMG  class="rapemewithachainsawthanks" src="http://www.kde-look.org/content/pre1/17800-1.png" alt="Mockup for Meta Kio Slave 'locations:/'" border="0">
<br>
Dik's suggestion currently only is a mockup. There is no code yet. And there is no-one writing it either, yet. But the base idea is excellent. It can be expanded to a full-blown feature set that makes that dialog a "portal" site to all the powers of KDE KIO. Read on for more thoughts about this...

<!--break-->

<br><br>
The concept of "locations:/" is very clean. To make it fullfill its intended purpose, namely, 
<ul>
<li>that all important kioslaves should become more exposed to users,</li> 
<li>that their use should be made as easy as possible, and </li>
<li>that it should sport a clean user interface, </li>
</ul>
that darn thingie should by default be very well visible, and have a few more "features". Dik already suggested a "wizard" to help users use a certain KIO Slave. His mockup is here:

<br><br>
<IMG src="http://www.kde-look.org/content/pre2/17800-2.png" alt="Mockup for kio_slave setup wizard" border="0">


<br><br>
My suggestions for a complete feature set that should go with the future <i>KDE KIO Slave Portal</i> are here:
<ul>
<li> <b>make it the default</b> for Konqui's "Home" button on all new installations, hmmm... maybe even for all upgrades to KDE 3.4 (with the option, of course, for users to change it to something else, like, back to $HOME). Complete documentation would create the desired visibility.</li>
<li> <b>create a shortcut</b> with icon "Locations" on each desktop on the upper left corner. That could create additional visibility.</li>
<li> <b>each one</b> of the kio-slave icons in the "locations:" folder should have a wizard associated with it in a right-click context menu. That could help the ease of use.</li>
<li> <b>each one</b> of the kio-slave icons in the "locations:" folder should have a well-worded tooltip as well as a WhatsThis entry, as well as a  complete documentation (the end user documentation in <a href="/help:/kinfocenter/protocols/index.html#protocols">this field</a> currently sucks big time... but don't blame Lauri, please, if you could follow the last link!). That could help to increase the ease of use.</li>
<li> <b>only the most important</b> kio-slaves should be directly visible in the "locations:" folder (max. 6 items). The rest should be hidden in a 
subfolder. (To add more items, click on subfolder, which opens on separate window, and drag'n'drop icons from subfolder to main "location:" folder. Explain this to user, when subfolder is opened... ). This could help keeping the user interface clean...</li>
<li> <b>design a context menu</b>, available by right-mouse click, for all items in "locations:". The context menu should have standard entries, for each single KIO Slave, to...
<ul><i>
<li>...the specific "Config Wizard" (helps to set up f.e. a correct "fish://" login to a remote server),</li>
<li>...the specific "Usage Requirements" (explain what required apps need to be installed, like "smb" needs (lib)smbclient, or whatever),</li>
<li>...the specific "Quick Help" (explains in 3 sentences what that kioslave is good for and shows an example URL), </li>
<li>...the specific "Manual" (complete manual for that kio slave -- many still need to be written!), </li>
<li>...the specific "History" (gives quick access to previously used URLs for that kioslave),</li></i>
</ul>
</li>
</ul>

I think such a thing could be made one of the key new features for KDE 3.4. To many long-time users most of the kioslaves and protocols would appear like something completely new inside KDE, and it would justify making a certain "marketing splash" about it.

<br><br>
Hopefully, <A href="http://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway">"fuse-kio"</A> (currently in alpha or beta stage) will in 3.4 become a solid and stable citizen of this "kioslave community" of little KDE helper utilities. Because fuse-kio can proof to be a core component to make KDE the "Integrative Desktop", empowering also many non-KDE apps with additional features such as transparent remote access to all sorts of resources, even from their own native, unchanged file dialogs.

<br><br>
Hopefully someone (or more people) will step forward to write the code and do work needed for making "locations:/" become a reality. 

<br><br>
In return, I, for one, pledge to organize the writing of the missing documentation (or do it myself) for all those KIO Slaves which appear in in "locations:/" and are un-documented.... <i><b><A href="mailto:feifle_at_kde_dot_org">Deal?</A></b></i>

