---
title:   "klik avalanche (1): DNS problems -- use \"IP 134.169.172.48 klik.atekon.de\" in /etc/hosts"
date:    2005-09-21
authors:
  - pipitas
slug:    klik-avalanche-1-dns-problems-use-ip-13416917248-klikatekonde-etchosts
---
<p>Seems like my Dot story on klik has kicked off a little avalanche....</p>

<p>
One thing however that keeps it from growing right now is a DNS problem with the <A href="http://134.169.172.48/">klik.atekon.de</A> ISP provider. The klik server's IP address is <a href="http://134.169.172.48/">134.169.172.48</a>. To work around the DNS problem, you should add the following entry to your <i>/etc/hosts</i> file:
<code>
134.169.172.48  klik.atekon.de
</code>

This will help for installing the klik client scripts:

<code>
wget klik.atekon.de/client/install -O - | sh
</code>

Or, alternatively put the IP into the command:

<code>
wget 134.169.172.48/client/install -O - | sh 
</code>


It will also help fetching and testdriving <a href="http://134.169.172.48/">klik packages</a> from the commandline like this: 

<code>
${HOME}/.klik klik://scribus-latest
${HOME}/.klik klik://xvier
</code>

However, this may not help for cases where you use a browser that doesn't look into the hosts file before contacting a <a href="http://en.wikipedia.org/wiki/DNS">DNS</a> server.
</p>