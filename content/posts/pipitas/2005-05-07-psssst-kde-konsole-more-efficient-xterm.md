---
title:   "Psssst.... KDE konsole is more efficient than xterm"
date:    2005-05-07
authors:
  - pipitas
slug:    psssst-kde-konsole-more-efficient-xterm
---
Sometimes you learn surprising things while you drive a car.<br><br>

Last time this happened was on Thursday evening. <A href="http://www.sambaxp.org/ndex.php?id=25"><b>SambaXP conference</b></A> had ended in Goettingen, and I gave <A href="http://samba.org/~tridge/"><b>Tridge</b></A> a lift to the Frankfurt Airport. For me it was only a very little detour on my way to Stuttgart. Tridge had a 30 hour journey back home to Australia in front of him (21 hrs of pure flight time).<br><br>

During the 2-hour ride we chatted about various things, mostly technical in nature. Things covered were <A href="http://samba.org/ftp/samba/slides/tridge_sambaxp05.pdf"><b>Samba4</b></A>, its extensive usage of <A href="http://samba.org/ftp/samba/slides/tridge-ldb.pdf"><b>ldb</b></A> (embedded LDAP-like database) to make it a full Active Directory domain controller, and various other topics. Tridge is a very patient and friendly person when it comes to explaining complex technical questions to a non-coder like myself. At one time we happened to touch on the question of performance in X applications. I mentioned how well <A href="http://konsole.kde.org/"><b>konsole</b></A> worked as a replacement of multiple xterms, when you use its tab feature.<br><br>

Now Tridge is a KDE user, but he was sitting on the co-driver seat and looking at his notebook screen with some opened <A href="http://dickey.his.com/xterm/xterm.html"><b>xterms</b></A> running on his KDE desktop. He told me that he used xterms because he knew all their features so well, since many years. He didnt seem to like "bloated GUIs" while using a terminal. And it was important to him to use the screen estate taken by scrollbars for more display room and instead just hit <tt>&#091;CTRL&#093;+&#091;PgUp&#093;</tt> or <tt>&#091;CTRL&#093;+&#091;PgDown&#093;</tt> to scroll back and forth. OK, no big deal. Konsole can do both, switch off scrollbars and use these very shortcuts for scrolling. Moreover, it can bind its shortcuts to almost any key combo you come up with, just as you please. Tridge started konsole to look at it for the first time since long ago. He discovered konsole's "fullscreen mode" which he seemed to like a lot.<br><br>

Most important to him is that scrolling does happen very efficiently, since he uses it a lot when coding or reading mail. Being the hacker he is, he applied two very simple tests to verify how much he'd loose when he switched from xterm to konsole: measure the time to run an <tt>ls -lR</tt> in a large source directory with many subdirs, and measure time for a <tt>cat /usr/dict/words</tt>.<br><br>

I had never run such a test. All I was aware of were <A href="http://aseigo.blogspot.com/2004/10/konsole-vs-xterm-or-proof-that-kde-is.html">Aaron's memory usage benchmarklets</A> (which had been the basis for my <i>"You can efficiently replace multiple xterms with one tabbed console window"</i>-argument towards Tridge).<br><br>

So we were both very surprised. to see konsole not only being close to xterm's performance, but being more than double as efficient! Tridge is now considering to switch from xterm to konsole, probably after giving it a day or two of "probationary period".<br><br>

Today I remembered that little episode, but wasn't sure any more about the figures. So I repeated the experiments. The glorious results are here:<br>
<br>
<pre>
+-----------------------------------------------------------------------------------------------+
|                                 konsole vs. xterm performance                                 |
+-----------------------------------------------+-----------------------------------------------+
|   konsole                                     |    xterm                                      |
+-----------------------------------------------+-----------------------------------------------+
| kurt@p9004:~> time cat /usr/dict/de/all.words | kurt@p9004:~> time cat /usr/dict/de/all.words |
| Aachen                                        | Aachen                                        |
| (....)                                        | (....)                                        |
| zzgl                                          | zzgl                                          |
|                                               |                                               |
| real 0m5.481s                                 | real 0m15.527s                                |
| user 0m0.005s                                 | user 0m0.005s                                 |
| sys  0m0.128s                                 | sys  0m0.124s                                 |
+-----------------------------------------------+-----------------------------------------------+
| kurt@p9004:~/SVNstuff/samba> time ls -lR .    | kurt@p9004:~/SVNstuff/samba> time ls -lR .    |
| (....)                                        | (....)                                        |
|                                               |                                               |
| real 0m1.441s                                 | real 0m4.275s                                 |
| user 0m0.123s                                 | user 0m0.309s                                 |
| sys  0m0.156s                                 | sys  0m0.196s                                 |
+-----------------------------------------------+-----------------------------------------------+
</pre>
<br>
Are you also, like me, curious about how other terminal emulators work as compared to xterm? Well, I did a short showdown of xterm vs. <A href="http://directory.fsf.org/gui/gnome/gnome-terminal.html"><b>gnome-terminal</b></A> too. The good news to our Gnome-friends is: gnome-terminal also beats xterm! Look at these figures:<br>
<br>

<pre>
+-----------------------------------------------------------------------------------------------+
|                             gnome-terminal vs. xterm performance                              |
+-----------------------------------------------+-----------------------------------------------+
|    gnome-terminal                             |    xterm                                      |
+-----------------------------------------------+-----------------------------------------------+
| kurt@p9004:~> time cat /usr/dict/de/all.words | kurt@p9004:~> time cat /usr/dict/de/all.words |
| Aachen                                        | Aachen                                        |
| (....)                                        | (....)                                        |
| zzgl                                          | zzgl                                          |
|                                               |                                               |
| real 0m14.092s                                | real 0m15.527s                                |
| user 0m0.005s                                 | user 0m0.005s                                 |
| sys  0m0.132s                                 | sys  0m0.124s                                 |
+-----------------------------------------------+-----------------------------------------------+
| kurt@p9004:~/SVNstuff/samba> time ls -lR .    | kurt@p9004:~/SVNstuff/samba> time ls -lR .    |
| (....)                                        | (....)                                        |
|                                               |                                               |
| real 0m3.409s                                 | real 0m4.275s                                 |
| user 0m0.136s                                 | user 0m0.309s                                 |
| sys  0m0.152s                                 | sys  0m0.196s                                 |
+-----------------------------------------------+-----------------------------------------------+
</pre>
<br>
Please don't take these results too seriously. Do not regard them as anything close to a scientific benchmark. I bet you could tweak them in many ways, depending for instance on the font the various terminals use. <br><br>

Just take them as what they were to me: the result of my default workstation environment (KDE HEAD from latest CVS, <i>self-compiled with <b>--enable-debug</b></i>) and Gnome (with gnome-terminal 2.8.0 as installed on an <i>apt4rpm</i>-updated SUSE-9.1). And a reason to be proud of KDE, that we can confidently advertise even to people who are looking for the highest performance they can get.<br>
<!--break-->