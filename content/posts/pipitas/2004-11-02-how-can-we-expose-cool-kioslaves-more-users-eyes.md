---
title:   "How can we expose cool kio_slaves to more users' eyes?"
date:    2004-11-02
authors:
  - pipitas
slug:    how-can-we-expose-cool-kioslaves-more-users-eyes
---
<br>
KDE needs to find ways to expose the many different KIO Slaves and their usefulness to more users' eyes. How about this:

<ul>
<li>separate the protocol part from the host/path in the various addressing fields/location bars.</li>
<li>turn the protocol part into an (editable) drop down list of available items. </li>
</ul>
If we want it more fancy, we could just make it a little drop-down to the left of the address/location bar, and depending on the selection, it auto-fills 'http://', 'ftp://' or whatever into the lineedit field. And vice-versa: whatever you type in the line-edit as the protocol-part gets selected in the drop-down. This way users will become quickly aware of many more kio_slave options and start to play with them and also use them...

<br><br>
This could be used in Konqui's location bar as well as in File Open dialogs of various applications. Here's a quick'n'dirty mockup:<br><br>

<IMG src="https://blogs.kde.org/images/a426a5d2e453c2e570116f57a4c85824-699.png" alt="Mockup for URL protocol/kio_slave drop-down list" border="0">

<br><br>
I can already hear the objections (as I heard them when I tried to discuss the idea at <A href="http://conference2004.kde.org/">aKademy</A> with some people):

<ul>
<li>"This makes it more difficult -- users are used to type <em>'http://'</em> or <em>'ftp://'</em>..."</li>
<li>"How do I then 'copy'n'paste' a complete URL from the address field?"</li>
<li>"This looks too different from what I am used."</li>
</ul>
<i>Users' old habits?</i>
<br>
I do not think KDE users would have much difficulty to adapt to a little change here. After all, they adpated to tabbed browsing quite easily too, despite all the objections of people claiming that "Multi Document Interfaces" (MDI) were deprecated and not well liked by users....

<br><br>
<i>Copy'n'Paste?</i>
<br>
I am sure some KDE hackers will very quickly come up with a way to copy the complete URL correctly from the address field, even if the protocol part came from a drop down list....

<br><br>
<i>Looks too different?</i>
<br>
OK -- make it configurable then. Find an easy way to switch it back to the "old", traditional look'n'feel. Best, if you could switch it on and off directly inside the new location bar widget.

<br><br>
I recently saw Eric Laffoon supporting a similar idea in a discussion on the <A href="http://dot.kde.org/">Dot</A>. So maybe the suggestion finds a few more supporters, and even someone who can implement it.

<br><br>
It would for sure expose our KIO Slaves to much more users. Users using them strengthen KDE and Linux in many ways, turning them into much more confident ambassadors for our platform. Many of our KIO Slaves with their respective protocol handlers such as 'fish://', 'webdav://', 'vnc://', 'print:/', 'locate:/', 'info:/',
'man:/', 'fonts:/', 'camera:/', 'audiocd:/', 'settings:/' or 'trash:/' are still very much underrated und rarely used. Not because they are un-usable -- but because their existence and power is too little known. Anything that increases their visibility Is Good (TM).<br><br><br>
<!--break-->

