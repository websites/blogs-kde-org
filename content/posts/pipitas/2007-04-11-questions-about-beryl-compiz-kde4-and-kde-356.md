---
title:   "Questions about Beryl, Compiz, KDE4 and KDE 3.5.6"
date:    2007-04-11
authors:
  - pipitas
slug:    questions-about-beryl-compiz-kde4-and-kde-356
---
<p>I'm sure I will be using KDE 3.5.6 for most of my personal Linux time over the next few months. And I'm evenly sure I want to follow the KDE4 developments (esp. regarding KDEPrint). But I'm also curious to have another look at Beryl and Compiz. (I'll certainly not be using it for any length of time -- last time I had a look, about a year ago, my head was dizzy from all the wobbly-ness of the windows moving, and the bottle-genie-ness of windows minimizing+maximizing; however, I'm looking for some cool 'demo-ware' to impress and make envy my collegues if I happen to give a scheduled presentation to them coming June...)</p>

<p><i><b>So how do I set it up best if I want to install</b></i> (and use) <i><b>KDE3, KDE4, Beryl and Compiz side by side on the same box?</b></i> Are there any tutorials, guides or howtos available?</p>

<p>Could I have 4 different demo users (kde3, kde4, compiz and beryl) which by default run their respective environments?</p>

<p>Regarding Beryl+Compiz: I'm not in any way familiar with either one of them, and I'd not be surprised if both of them did RPM-conflict (and I had to install one of them exclusively at a time for playing with). Just tell me if it is so.</p>

<p>Regarding KDE3 and KDE4: that's what I'd spend most of the time to set up. My hope was the openSUSE build service will make this a breeze. But that hope was replaced by a somewhat bleak outlook, after I downloaded a rather small kde4-$something RPM from the build service to poke into it: it looks like it installs all binaries into /usr/bin/.... which to me sounds like a recipe for more failures and problems than should necessarily have to happen.</p>

<p>I'd *love* to be proven wrong; but my gut feeling would be better if I could spot a "clean" separation like "everything goes into "/opt/kde4/{bin,lib,...}/". With that big hotch-potch kettle of mixed kde3 with kde4 binaries in any user's $PATH I smell problems after problems after problems coming along when I'm going to run that setup...</p>

<p>Also, I did not find any "Howto run+test KDE4" type of document in the openSUSE wiki; maybe it's just my imagination that I think I'd have to define different $PATHs and $LD_LIBRARY_PATHs and other $env_vars for each of the test users. Maybe it is all solved and prepared in a user friendly way already? Maybe I just have to select at the KDM screen which type of session I want to run? Maybe I'm just putting thoughts were no thoughts are required any more?</p>

<p><b>P.S.:</b> I'll surely read any replies and hints from you -- but it may be only after May 5th until I can respond again myself.</p>


<!--break-->