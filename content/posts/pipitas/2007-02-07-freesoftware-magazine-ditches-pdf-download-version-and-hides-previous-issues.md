---
title:   "\"Freesoftware Magazine\" ditches PDF download version (and hides previous issues)"
date:    2007-02-07
authors:
  - pipitas
slug:    freesoftware-magazine-ditches-pdf-download-version-and-hides-previous-issues
---
<p>What a stupid idea! How shortsighted, to not consult with their readers beforehand!</p>

<p><A href="http://www.freesoftwaremagazine.com/">www.freesoftwaremagazine.com</A> have announced that they will stop offering their publication as a PDF for download. Instead, they will go "online only"....</p>

<p>I'm now halfway through reading their <A href="http://www.freesoftwaremagazine.com/articles/editorial_16#comment">100+ reader feedbacks</A>, and have yet to encounter a single one saying <i>"Yeah, I didn't like that PDF anyway!"</i> Most are arguing fiercly to retain the PDF version, or at least ask politely to get it back.</p>

<p>The PDF version of the magazine was one of its strongest assets to have had. I'm really puzzled by the amount the makers of that product have been   out of touch with their own audiences, and how they could announce such a decision without prior consultation with their target group. And it had not even been "easy" to get the PDF either. Yes, the download was free of charge. But there was a need to registration, and there was no direct link from the portal site to get to the nice stuff. And still they had built a rather big and distinct group of fans and followers. Whom they have now alienated all at once... How sad.</p>

<p>For now, you can't even find a link to the first 15 issues of FSM as PDF. Not even when you are a subscriber, not even when you are logged in! And the online versions of their new material even misses to provide a "printable version".... WTF? How much and  how fast can a publication degenerate at once, after it previously seemed to have especially cared for readers who do value how gentle paper is on their eyes offline (as compared to the agressive, artificial light that tires them down faster and requires them to be online for reading)?! Not to talk about the big group of interested people living in areas of this globe where it is not a common thing to be "always on"?</p>

<p>Now the FSM production as PDF *may* have been expensive, and they may even have lost money on it, lots of money. That I don't know. But that is also not a reason they give us for the turnaround. Tony Mobily's editorials says: <b><i>"Paper—as well as PDF—is now a thing of the past..."</i></b> If this indeed is their conviction and the true reason for the twist, it is a very stup^Will-informed judgement. If it is just an excuse to cover up another real reason (like "financial difficulties" may be), the argument will not gain more traction either.</p>

<p>The response of their readers to them seems to be very resounding.</p>

<p>One reader's response is typical: <i>"What? No PDF? No read nor digg then."</i> Another one: <i>"By dropping PDF and quality composition, you lose your Ace, your trump card. With them, you will be like most other web-sites discussing free-software. I may or may not want to visit your site. Without the PDF hook, I now will be just as likely to visit other sites."</i></p>

<p>Tony gives a lot of reasons that are indeed valid points going against PDF, and  in  favor of HTML publishing. But he throws out the baby with the bath water, and completely forgets the advantages that go with PDF for many potential readers and use cases.</p>

<p>And he seems to look at it in a very rigid "EITHER -- OR" way. It didn't seem to occur to him that they could have expanded from PDF to HTML, while retaining their PDF version as well.</p>

<p>And last: of course, a so called "$freesoftware" $magazine should have explored the option to use Free Software for the job. And it <b>*is*</b> possible to create HTML and PDF output from a single source automatically! And neither is or was there is any rule that you need to use Quark Express or Pagemaker payware in order to create high quality PDFs. Ever heard about <A href="http://www.scribus.net/">Scribus</A>, Tony?!? And if you are not so much interested in DTP quality layout any more -- what about Mike Sweet's (of CUPS fame) <A href="http://www.htmldoc.org/">htmldoc</A> then? You can test an <A href="http://www.easysw.com/htmldoc/pdf-o-matic.php">online HTML--&gt;PDF converter here</A>...</p>

<p>Heck, and  if they have no "techies" in their ranks, they could even use OpenOffice for HTML and PDF generation. Just create a simple style that works well for both formats.... Ts, tss, tsss...</p>

<p>Of course, they are free in their decision. I just fear that they did not consider they may have to basically start anew and from scratch in building again a devoted audience (because their old one will be rapidly dissolving).</p>

 <!--break-->