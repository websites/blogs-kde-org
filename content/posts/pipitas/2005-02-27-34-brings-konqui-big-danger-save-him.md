---
title:   "3.4 brings Konqui into big danger! Save him!"
date:    2005-02-27
authors:
  - pipitas
slug:    34-brings-konqui-big-danger-save-him
---
<br />KDE developers: If you ever log off from your current KDE-3.4cvs session, think again! Couldn't you keep it running? For Konqui's sake?
<br />Because if you really log off, you'll bring Konqui into big danger. He is about to take a nap on a crescent moon. But the poor guy will be falling off the first moment his sleep will be deep enough!<img align="right" hspace="6" vspace="3"  src="http://webcvs.kde.org/*checkout*/kdebase/ksmserver/shutdownkonq.png?rev=1.4" class="showonplanet" />
<br />The reason: Konqui has a rest position that is completely unbalanced. He is starting to pass into napping mode high up there with not even the slightest natural equilibrium!
<br />Ever since I have this logout pic, I never dared to actually complete a logout – I always was forced to cancel it due to my deep concern for his enduring health. 
To make Konqui safe (and let me logout and sleep well again), the dangling leg and the dangling tail should hang down on different sides of the crescent, no? So anytime he could start loosing balance towards one side, a small lever-increasing, even sub-conscious movement of the limbs on the opposite side would easily restore equilibrance….
<br />Take a look at any breed of cats of prey: all those who can climb and sleep in trees disollows its juniors to let hang down all dangling limbs on the same side of the bough!<br />
;-P
Cheers, Kurt 
<br /> 
<b>P.S.:</b> Oh, and please dont start explaining to me “Konqui is a Dragon, and dragons can fly!