---
title:   "KDE bugzilla: bug #140006"
date:    2007-01-13
authors:
  - pipitas
slug:    kde-bugzilla-bug-140006
---
<p><b><a href="http://bugs.kde.org/show_bug.cgi?id=140006">#140006</a>:</b></p>

<p>Yesterday I received an e-Mail from a user who asked to keep his name and affiliation confidential. He is involved in preparations for a rather large Windows --> Linux/KDE migration, involving several thousand workstations, most of them in a managed KIOSK environment. They'll not be ready before 12 months from now for the first serious rollout, but they are already testing with KDE 3.5.3. Once they are ready, he hopes to be able to use KDE 3.5.5 or 3.5.6 (should it ever happen). They'll *not* use KDE 4.0, even if it were released this autumn, and it will be at least 3 years before they'd ever consider to use any 4.0.x version. And here is what he said about KDEPrint:</p>
<dl><dt>&nbsp;</dt><dd>
    <p><i>"Printing is going to be pretty important to us. And KDE's printing
     system has already proven to be designed with an architecture
     underneath that will be able to meet most of our current needs. The
     only thing is that there are a few corners where we find some more
     polishing is asked for."</i></p>
    <p><i>"I understand that you do not intend to change any fundamental
     things in KDEPrint after 3.5.5 is out. However, be aware that we
     are able to build our own packages from Subversion, and we follow
     any improvements that you guys are able to come up with. I want you
     to understand that at least for _our_ users, improvements to the
     post-3.5.5 branch will not be in vain, and we will make use of
     everything you are willing to backport from your 4.0 improvements."</i></p>
    <p><i>[....]</i></p>
    <p><i>"Another thing, also about printing. What about adding a series of
     'unit tests' to KDE subversion? I'm not exactly sure if unit tests
     is the correct term. But it is surely a good idea to have a few
     KDE and OpenOffice test files, as well as PDFs which can be used to
     test standard print options. A lot of things could be tested with
     scripts that send jobs to a virtual printer printing to file. Also,
     it would be good to test everything against new CUPS versions 'in
     the making'. Is there a possibility to make a CUPS test server run
     alongside the standard server, but binding to a different port than
     631? I saw you mentioning a possibility like this on the CUPS
     mailing list some time ago."</i></p></dd>
</dl>
<p>Don't you too think this is a great suggestion? I do. And that's why I forward this into a bug report. From my past few days of sifting and ploughing through KDEPrint's bugzilla, I noticed that quite a few users are using KDEPrint for more than just occasional printouts, and they are aware of some of its most advanced features. They submit bug reports not just once or twice, but several times a year. These user would probably make good use of some test scripts and standard procedures to test KDEPrint code from Subversion on a regular basis.</p>

<p>I have some ideas in mind what I could contribute to start this off fairly soon. And as soon as a serious switch from PostScript to PDF as the standard spooling and print processing format in CUPS will kick in, this type of heavy testing is called for in any case.</p>

<p>Now.... I'd just like to hear from at least 3 people who have not yet contributed to KDEPrint (bug reporting activities don't count here) that they'd like to participate, and that they'll test my first script once I'm ready to release it.  &nbsp; &nbsp;&nbsp;&nbsp; :-) </p>

<!--break-->