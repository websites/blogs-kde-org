---
title:   "HP Uses Qt For Its FOSS Printing Software"
date:    2005-04-09
authors:
  - pipitas
slug:    hp-uses-qt-its-foss-printing-software
---
<p>
Late this afternoon, a talkback at <A href="http://linuxtoday.com/">LinuxToday</A> annoyed so much that I had to respond. A PR story outlining that HP turns now to Linux as the OS for their new series of <A href="http://en.wikipedia.org/wiki/Network-attached_storage">NAS</A> devices (Network Attached Storage) prompted an extremely uninformed guy to headline his bickering. <i>"HP -- Open Source Leech"</i> and then go on: <i>"...HP is again leeching on the opensource community. Hey HP, where are the drivers for your scanners, printers and all your laptops?"</i></p>
<p>
At first, I didn't have the inclination for a response. Later, I decided to give him a rather polite, factual answer.</p>
<p>
Fact is, that HP indeed does sponsor <A href="http://en.wikipedia.org/wiki/FOSS">FOSS</A> software development on all the named fields: their scanners and printers are increasingly supported by <A href="http://hpinkjet.sourceforge.net/">HPLIP, the HP Linux Imaging and Printing Project</A>. </p>
<p>
HP drivers provide first class Linux printing support for more than 300 of their models. All their model families are present in HPLIP: DeskJets, OfficeJets, Photosmarts, Business Inkjets and some LaserJets. </p>
<p>
HPLIP extends its reach now even to network scanning support. Their more recent devices report back to the workstation even full status and supply information.</p>
<p>
For what I know, Linux drivers for HP printer where actually the first to be developed with the support of the vendor itself. HP gives an example to the complete industry here... </p>
<p>
Finally, what is sweetest about the new HPLIP stuff: they have a very nice GUI. Guess what? It is based on Qt! (plus, on PyQt and Python)</p>
<p>
<em>
((sarcasm mode="on"))
Yes, Qt from Trolltech! The same <a href="http://en.wikipedia.org/wiki/Qt_toolkit">Qt</a>, that, due to its evil <A href="http://en.wikipedia.org/wiki/GPL">GPL</A> license, is supposedly preventing big and small companies and <A href="http://en.wikipedia.org/wiki/ISV">ISVs</A> from developing for the KDE Desktop platform. (Because, you know, big companies will never develop GPL software, not even small projects. They require to get everything for free, like the LPGL offers to them.) Hmmm.... HP must have gotten something completely wrong then....
((sarcasm))</p>
</em>

<!--break-->
