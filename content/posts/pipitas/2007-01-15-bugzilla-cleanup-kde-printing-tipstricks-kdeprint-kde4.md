---
title:   "Bugzilla Cleanup ; KDE Printing Tips+Tricks ; KDEPrint in KDE4"
date:    2007-01-15
authors:
  - pipitas
slug:    bugzilla-cleanup-kde-printing-tipstricks-kdeprint-kde4
---
<p>It looks like one of the recurrent problems of people using KDEPrint's more advanced features is with <i>"number-up"</i> printing, combined with <i>"print duplex when I have a simplex printer"</i> (yes, you can turn the stack of paper round and feed it a second time through the printer; hope and pray it doesn't munch it). This then piles up like here:</p>

<ul>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=82123"><b>#82123</b></A>&nbsp;&nbsp;&nbsp;: Printing odd/even pages with multiple pages per sheet gets silly results (DUPLICATE)</tt></li>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=107936"><b>#107936</b></A>&nbsp;&nbsp;: [KDE4] multiple pages on one sheet not in right order  NEW</tt></li>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=92962"><b>#92962</b></A>&nbsp;&nbsp;&nbsp;: Option for number-up-layout=???? (page ordering on multiple pages) missing in kprinter (DUPLICATE)</tt></li>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=97669"><b>#97669</b></A>&nbsp;&nbsp;&nbsp;: multipage reverse printouts in wrong order (DUPLICATE) </tt></li>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=103666"><b>#103666</b></A>&nbsp;&nbsp;: [KDE4] facilitate margin adjustment for double-sided printing (DUPLICATE)</tt></li>
   <li><tt>Bug <A href="http://bugs.kde.org/show_bug.cgi?id=108484"><b>#108484</b></A>&nbsp;&nbsp;: printing multiple pages per sheet with reverse option selected (DUPLICATE)</tt></li>
</ul>

<p>What bug reporters had not realized: KDEPrint can do most of what they want <i>already</i>. (It's just not easy to find, and maybe not easy to setup. But really easy to use once setup.)</p>

<p>So, time to ruminate some of the stuff that has been written, shown, talked about, tutored and showcased elsewhere and oftentimes. And yet it still is very little known. (This blog won't change it, I'm sure. But maybe a few dozen people will learn a bit, and keep it in mind.).</p>

<b>First case:</b> reporter of <A href="http://bugs.kde.org/show_bug.cgi?id=82123">bug #82123</A> wants to layout 2 pages per sheet, and then to print only the odd sheets of the result. He selects everything in the main print dialog and then finds the result his printer spits out "silly". Because he gets "1+3", "5+7", "9+11" etc. (1st pic):  </p>
[image:2628 alignment="right" hspace=12 vspace=12 border=1 size=thumbnail][image:2627 alignment="right" hspace=12 vspace=12 border=1 size=thumbnail]
<p>But he wanted "1+2", "5+6", "9+10", see? (2nd pic)

<p>Why does he want this? In his bug report he doesn't explicitly tell, and one has to guess: he wants a 2-up layed-out double-sided printout, but on a simplex printer.  OK, fair enough.</p>

<p>While my 14 year old son would just use the "Range" feature of KDEPrint and type in "1,2,5,6,9,10,..." to select whichever pages he wants on each print run (before bothering to fill in a lenghty bugzilla entry calling the encountered behavior silly), here we seem to have someone who likes the power of automation, but failed to set it up.</p>

<p>In KDEPrint, we don't do anything on our own to the printfile that we receive. We let CUPS do the work, it knows better. What KDEPrint does, is provide a GUI frontend to all the rich CUPS functions, collecting the  user input and translate it into the commandline parameters for CUPS. CUPS then can manipulate the jobfile before it passes it on to the printer. So KDEPrint is dependent on how well CUPS works, and in which order it chooses to apply the different options to a jobfile. </p>

<p>It happens so, that CUPS always applies the page selection first, before doing the number-up layout thing. So it is expected behavior if you select "odd pages only" and "2 pages on one sheet" to get "1+3", "5+7", "9+11". We can't change that... but!</p>

<p>But we can ask some external utilities to come to help us.</p>

<p>The main dialog of KDEPrint isn't made for people who want to print duplex on simplex-only printers, and want at the same time their 2-up page selections behaving like they order. For those, we integrated a troup of helper tools. But they have no place on the main print dialog. They are called "pre-filters". You can find them on the "Filters" tab after you click on "Properties" in the main dialog. We can run a helper to select "odd" pages, and another one to make a "2-up" layout, and we can stack them in any order we want. </p>

<p>So, fact is, KDEPrint <b>can</b> serve this man, and serve him well. Here's how, <A href="http://bugs.kde.org/show_bug.cgi?id=82123#c12">step by step</A>: </p>

<pre>
 * Don't set anything in the main print dialog! Leave "All" pages, and "All" 
   pageset, don't select any number-up there. 
 * In the "Properties" part of the print dialog set up a chain of "pre- 
   filters". 
 * That is: locate the "Filters" tab 
 * On the Filters tab enable two filters (in that order!): as the first, use 
   the "Multiple Pages per Sheet" filter; as the second, the "Page 
   Selection/Ordering" filter 
 * Make sure the two pre-filters are set up correctly: multiple pages set to 
   "2", the page set to "odd". 
 * I hope you can discover by your own how to set these things (hint: there is 
   an icon with a wrench, and its tooltip says "Configure filter"). 
 * After you've printed the front pages of the sheet, now repeat the same 
   prodedure with the back pages. (You may want to use one of the "Reverse" 
   switches for reverse order of printout in order to avoid the manual sorting 
   of your during first pass printed sheets). 
</pre> 

<p>[image:2629 alignment="right" hspace=12 vspace=12 border=1 size=preview] Sounds complicated? Yes it is. But the job wanted to be done isn't sooo simple either. Look at the supposed "bug report". The reporter couldn't give a simple description of the task, so how can the solution be simple? (Sometimes it works out like this, but not here...)</p>

<p>But wait, I'm not ready yet.</p>

<p>[image:2626 alignment="right" hspace=12 vspace=12 border=1 size=preview] Does the man need to set up this complicated thing every time he wants to treat his cheapie printer with the "2-up, duplex-printout" trick? No. KDEPrint and CUPS have a solution for him: He can save this setup as a "printer instance", and give it a "speakting" name. I'd call it "make-my-printout-like-bug82123-reporter-wants-it". &nbsp; &nbsp; &nbsp;  ;-) &nbsp; &nbsp; &nbsp; Once he's done that, no more printjob setup! No more thinking about the details. Just select the printer instance in the drop-down menu; all options, prefilters etc. will be set right already (apart from "number of copies" maybe). Just click "Print" and be done.</p>

<p>Here are 4 more hot tips+tricks for your printing pleasures:</p>

<ul>
  <li>use kpdf for print previewing (run "kaddprinterwizard --kdeconfig", click "Preview"). It can do a few tricks that kghostview can't. Like, remember the size of its window. Let you search for strings in the preview (if fonts are embeded). It renders sometimes pages (if they contain big bitmaps) much faster than kghostview. (It starts a bit slower, because it secretly runs "ps2pdf" to convert the previewed PostScript to PDF first.)</li>
  <li>create printer instances even for the "Special Printers". Want regularly PDFs layed out as a booklet so you can print them at work on the office printer in one go? Create a a printer instance with "psbook"/"Pamphlet Printing" as a prefilter, and make it an instance of your "Print to File (PDF)". </li>
  <li><i>WhatsThis</i> is your friend when you use the KDE print dialog.</li>
  <li>Uhmm... I forgot the fourth one now.</li>
</ul>

<p>Finally, let me quote a <a href="http://bugs.kde.org/show_bug.cgi?id=82123#c16">response that I left</a> in the comments section of this bug entry:</p>

<pre>
   I invite you to create a detailed and concrete proposal how to implement 
   this. No, I don't mean to write the C++/Qt code (you probably can't code, 
   like I myself can't; if you can, please offer your help). I mean create 
   some gimp-ed mockups, some drawings, some rough visual representations of 
   how it should look like in your opinion. Do some "paper prototyping". But 
   take into account the *overall needs* of KDEPrint which must cater for a 
   lot of different apps: KDE's own ones *as* *well* *as* *external* 
   *applications*. 
 
   How should the overall layout of the printing dialog be looking: Where to 
   place which buttons? Where to position other widgets?  How does the 
   workflow for the user look like? What is differnt if she whats to preview 
   first? What is different if she prints from a browser? From a KOffice 
   application? From kpdf? What if he has access to 20 different printers 
   (big office)? What is different for home users from corporate users? What 
   is different for big TCP-connected laserprinters from small color inkjets 
   connected via USB? Don't forget installation/setup tasks, don't forget job 
   control wishes, don't forget security... 
 
   I'm sure that for a good design and layout of the dialogs we will also find 
   people who'll write the code to implement it. I'm not sure you'll even sit 
   down for an hour and think about it... 
</pre>

<p>And <a href="http://bugs.kde.org/show_bug.cgi?id=103666#c3">this</a>:</p>

<pre>
   For KDE 4.x we'll have to see. People with skills and time and love to look 
   after this "boring" printing stuff are rare -- and they are also rare inside 
   KDE. So it is not ruled out that we have lots of nice discussions and many 
   pretty feature requests and nasty bug reports, but not much will change for 
   printing when shiny new KDE 4.0 is out. 
</pre> 


<p>I think this is a fair statement about our current status for KDE4/KDEPrint development.</p>

<p>KDEPrint rocks, and kprinter rocks. Yes, they can be improved in a lot of spots, and overall. And we have some great ideas too. But someone with enough time needs to do the work. So likely, when KDE4 comes out KDEPrint will still rock. But the competition will have come closer. And it will also have many un-implemented wishlist items, and will <b>not</b> have had a complete usability overhaul.</p>

<!--break-->