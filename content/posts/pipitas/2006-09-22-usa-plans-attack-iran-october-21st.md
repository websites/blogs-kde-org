---
title:   "USA plans to attack Iran from October 21st ?"
date:    2006-09-22
authors:
  - pipitas
slug:    usa-plans-attack-iran-october-21st
---
<p>Are the USA planning to attack Iran? According to news stories, the deployment of a major "strike group" of ships is prepared to head for Iran's western coast. The ships include the nuclear aircraft carrier <i>Eisenhower</i> as well as a cruiser, destroyer, frigate, submarine escort and supply ship. Naval forces already received their formal PTDOs ('prepare to deploy orders') with a date set for being ready to go at October 1st:
</p>
<ul>
<li><a href="http://www.thenation.com/doc/20061009/lindorff">"War Signals?"</a> (<i>The Nation</i>)</li>
<li><a href="http://time-proxy.yaga.com/time/magazine/article/0,9171,1535817,00%20.html">"What Would War Look Like?"</a> (<i>Time Magazine</i>)</li>
<li><a href="http://www.rawstory.com/news/2006/Pentagon_moves_to_secondstage_planning_for_0921.html">"Senior intel official: Pentagon moves to second-stage planning for Iran strike option"</a> (<i>The Raw Story</i>)</li>
</ul>

<p>Is this the next stage on the way to the proclaimed goal of establishing a <a href="http://en.wikipedia.org/wiki/PNAC"><i>New World Order and New American Century</i></a> ?</p>

<p>Only 4 weeks to go until we are amidst the next major world crisis? Bush and his crownies are playing with the nuclear fire. Are they aware of the consequences?</p>

<p>Are <b>we</b>???</p>
<!--break-->