---
title:   "\"Julius Caesar had Gaul; Bush just has gall\""
date:    2006-11-05
authors:
  - pipitas
slug:    julius-caesar-had-gaul-bush-just-has-gall
---
<dl>
<dt>Don't you love it if a Brit with a good command over the subtleties of the English language takes on his U.S. friends?  Here is a hilarious piece (of satire?, of drawing historic analogies?) by Terry Jones in The Observer (UK):</dt>

   <dd><i><br>"In 59BC, Julius Caesar declared he was so shocked by the incursions of the dangerous Helvetii tribe into Gaul, and the suffering of the Gaulish peoples, that he had himself appointed 'protector of the Gauls'. By the time he'd finished protecting them, a million Gauls were dead, another million enslaved and Julius Caesar owned most of Gaul. Now I'm not suggesting there is any similarity between George W Bush's protection of the Iraqi people and Caesar's protection of the Gauls."</i>
   <br>
   <i>"[Cesar] desperately needed a military victory to boost his standing in Rome and give him the necessary popular base to seize power."</i>
   <br>
   <i>"George W Bush, on the other hand, [....] didn't need to boost his popularity, because the popular vote had nothing to do with his getting into power in the first place."</i>
   </dd>
</dl>

<p>&nbsp;  (full text <a href="http://observer.guardian.co.uk/comment/story/0,,1939780,00.html"><b>here</b></a>)

<!--break-->