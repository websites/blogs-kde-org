---
title:   "Recommendations to (K)Ubuntu Dapper users: How to restore an uncrippled CUPS [1: web interface admin functions]"
date:    2006-06-23
authors:
  - pipitas
slug:    recommendations-kubuntu-dapper-users-how-restore-uncrippled-cups-1-web-interface-admin
---
<p><b>[1] Restoring the CUPS web interface admin functions</b></p>

<p>(K)Ubuntu Dapper maintainers have crippled the CUPS 1.2 web interface. If you open <A href="http://localhost:631/">http://localhost:631/</A> (link only works if you have CUPS up and running) you'll even  get notified about it:</p>

<dl>
  <dt>--></dt>
     <dd><i>Administrative commands are disabled in the web interface for security reasons. [....] <tt>/usr/share/doc/cupsys/README.Debian.gz</tt> describes the details and how to reenable it again.</i></dd>
</dl>

<p>So it is "security reasons", huh? I'll deal a bit later with this statement, and how valid or not it is.</p>

<p>Let's first look at the way the measure is implemented. After all, the motivation may be completely valid. But if it indeed is, then users surely would deserve a completely clean way of disabling it, no? An implementation of that cropping measure that keeps them fully informed? Instead, the message is rather hidden amongst the rest of the page. Have a look (click screenshot on the left for full size): </p>

[image:2107 align="left" size="preview" hspace=8 vspace=4 border=0 class="showonplanet"]

<p>It certainly doesn't look like a lot of "usability" thought has been wasted on that change. The warning gets lost amongst the rest of the page. The page still has many other elements in it which demand attention. It has lots of links links (11, to be precise), which are even emphasized by button-like images, get high-lighted by "mouse-over"... and, in the end, they are completely non-functional and misleading because upon clicking them they do not do what their labels indicate.</p>

<p>Well, when I say "completely non-functional", you may be curious and try it. So did many other users. And lost lots of time by doing so. Because the links do indeed take you to the next pages. However the next pages will <i>not</i> have the warning quoted above. They'll present themselves like they were fully functional. They have more seductive links and buttons, such as inviting to <i>"Add Printer"</i>, <i>"Modify Class"</i>, or <i>"Set Printer Options"</i>. If you click on those, new dialogs come forth. You can do a lot of stuff. Until the end, when you want to commit your changes: then the password dialog comes up. But no password will work. Not the root password, and not your user password. By that time you may or may not remember the half-hidden warning of the main page (which you may or may not have noticed in the first place...).</p>

<p>Now, <b><i>if</i></b> the packagers already go to the length of patching not only a half-hidden sentence into the web interface, but even change the CUPS source code to keep a CUPS-1.1 functionality (more about that later) that was removed in 1.2;, given this background, I'm surely not asking too much if I want these changes to be implemented in a more unambiguous way so users are not led to complain in CUPS.org newsgroups about missing features. At least it would give a more honest picture about the Dapper packagers' changes to the users. What about....</p>

<ul>
  <li>....making the sentence better highlighted (like colorize the font in red), so it is easily noticed by users?</li>
  <li>....removing the buttons + links that misleadingly suggest users can still <i>"Manage Printers"</i>, <i>"Manage Server"</i>, and so on?</li>
</ul>

<p>It's easy. Even I can do it. And I'm neither a Web, HTML, PHP nor C or C++ developer, just a user that has some shell scripting experience. Have a look (click screenshot on the right for full size):</p>

[image:2108 align="right" size="preview" hspace=8 vspace=4 border=0 class="showonplanet"]

<p>Of course, the best choice in the interest of users would be to not cripple the web interface at all! But, Ubuntu packagers: <b><i>if</i></b> you decide for crippling the web interface, better remove all those useless buttons altogether! They only seduce users into clicking them; when then the next web pages indeed does appear, users naturally expect them to also work (which they don't).</p>

<p>Oh, and before you tell me, that in this case users could not easily restore the full CUPS functionality, because of the removed links: they can. Just provide two versions of the relevant file: <tt>/usr/share/cups/doc-root/index.html{.ubuntu,.orig}</tt> and put it into the README how to enable one or the other. Heck, you could even wrap all the required changes into the <tt>dpkg-reconfigure cupsys</tt> script!</p>

[image:2110 align="left" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>So what things do users miss when the web interface is disabled?</p>

[image:2111 align="left" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>The first few shots are what you may already be familiar with. Most of these are functions were already there in CUPS 1.1.</p>

<p>Screenshots (click on thumbnails for full size) are more illustrative than words. You aren't allowed to...</p>

[image:2112 align="right" hspace=4 vspace=2 border=0 class="showonplanet"]

<ul>
   <li>...set printer options,</li>
   <li>...add new printers,</li>
   <li>...create classes of printers,</li>
   <li>...set an error or operation policy for a printer,</li>
   <li>...start/stop printers,</li>
   <li>...pause/resume/cancel jobs,</li>
   <li>...move jobs to different printer,</li>
   <li>...use the auto-discovery feature for uninstalled network printers</li>
</ul>

<p>and much, much more.</p>

[image:2115 align="right" hspace=4 vspace=2 border=0 class="showonplanet"]
<p>Particularly annoying is the disabling of two features. First, the new easy to use, one-click configuration settings demonstrated in the screenshot to the right (click to enlarge). They let users easily access features like:
&nbsp; [x]&nbsp; Show printers shared by other systems
&nbsp; [&nbsp;]&nbsp; Share published printers connected to this system
&nbsp; [&nbsp;]&nbsp; Allow remote administration
&nbsp; [&nbsp;]&nbsp; Allow users to cancel any job (not just their own)
&nbsp; [&nbsp;]&nbsp; Save debugging information for troubleshooting
 </p>

[image:2116 align="right" hspace=4 vspace=2 border=0 class="showonplanet"]
<p>Second, removing the ability to access and edit the cupsd.conf configuration file from the web interface (including the ability to restore a working, default setup):</p>

<p>OK, winding up the complaining part now. Back to re-enabling the full functions of the web interface...</p>

<p>Why is that damn web interface even important, you ask? There are several reasons, each one good enough on its own. The closer you get to printing or admin functions required in "small businesses" or "enterprises", the more important do they get. But even home users would benefit from acknowledging them:</p>

<ul>
 <li>it is easier to use for newbie users than the commandline interface is</li>
 <li>it is the only non-commandline user interface supported by the CUPS developers themselves</li>
 <li>it is the only non-commandline user interface that works the same across all platforms and distros (sans the insane modifications some packagers make)</li>
 <li>it even works in non-GUI terminals, if you use a text based browser such as links, lynx or w3m</li>
 <li>it is the only user interface that is always up to date with the current release of CUPS (all third party frontends do necessarily have a delay before they support new features)</li>
 <li>the Gnome CUPS Manager supports only a fraction of the CUPS 1.1 or 1.2 functionality</li>
 <li>while the KDEPrint CUPS Manager supports all of the CUPS 1.1 functionality, it is not yet up to date with CUPS 1.2, and even if -- KDE is not every user's choice!</li>
 <li>it is currently the only user interface that supports some of the newest cool features in CUPS 1.2 (such as the "one click reconfiguration of CUPS"; or the SNMP network printers  autodiscovery tool -- for details about this, wait for my next entry in this blog series)</li>
</ul>

<p>So how do we re-enable it now? The named <i>README.Debian.gz</i> essentially tells us: run the two commands  </p>

<ul>
  <li><tt>adduser cupsys shadow </tt> &nbsp; and   </li>
  <li><tt>adduser joe lpadmin </tt> &nbsp; (replace "joe" by your own name)</li>
</ul>

<p>and be done. The second one should add user joe to the <i>lpadmin</i> group (which by default is the only user group allowed to manage CUPS functions) and establish his ability to administer CUPS; the first one should let him also do so via the official CUPS web interface, not just the commandline (or frontends such as KDEPrint's Printer Manager, or Gnome's CUPS Manager), because the Dapper CUPS daemon runs from the <i>cupsys</i> account. </p>

<p>So, adding user cupsys to the shadow group, and each user who should be able to administer CUPS to the lpadmin group will do the trick: the CUPS web interface in principle will work again to its full extend (sans some bugs which may still be present in Dapper's packages).</p>

<p>There's more to it. CUPS 1.1.x for more than 3 years had an officially supported configuration directive <tt>"RunAsUser Yes"</tt>, which allowed the CUPS daemon to run without root privileges. This measure, in theory, is meant to increase security. However, CUPS developers found it didn't increase security in practice, but on the other hand led to many, many more user problems with the base functionality. Therefore they removed RunAsUser again in CUPS 1.2.0. Debian and Ubuntu however want to keep this function that was removed upstream. Maintainers patch CUPS and hardcoded it to run the scheduler program ("cupsd") and the *.cgi apps responsible for the web interface with the access rights of the "cupsys" account:</p>

<pre>
  root@obiwan:/home/kurt# ps u | egrep '(cupsd|cgi)'
    cupsys   11040  3.8  2.4  8372  6212 ?   SNs  07:42  10:18 /usr/sbin/cupsd
    cupsys   11044  1.8  1.4   132   126 ?   SNs  07:43   0:08 /usr/lib/cups/cgi-bin/printers.cgi
</pre>


<p>In effect, this is a hardcoded <tt>"RunAsUser Yes"</tt> with <tt>"User cupsys"</tt> setting. Therefore that "cupsys" user needs to be made part of the "shadow" group whose members are allowed to read the <tt>/etc/shadow</tt> password file. And user joe needs to be in the "lpadmin" group so he can manage printers without needing full root privileges. </p>

<p>I'm not going into the detailled arguments of that particular point ("RunAsUser") in question now. Admittedly, I'm not very much convinced by the Debian/Ubuntu camp's arguments -- but I can't argue too well against them either. For some more details, readers may want to look at <A href="http://www.cups.org/newsgroups.php?s1+gcups.general+T0+Q%22Q.+Proper+way+to+startup+cupsd%22">one of the many relevant threads</A> in the CUPS forums.</p>

<p>However, I am very much convinced of all other points I have to make. And that is why this series will continue...</p>

<!--break-->