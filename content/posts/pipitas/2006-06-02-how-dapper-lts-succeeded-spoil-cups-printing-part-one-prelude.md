---
title:   "How Dapper LTS Succeeded To Spoil CUPS Printing (Part One -- The Prelude)"
date:    2006-06-02
authors:
  - pipitas
slug:    how-dapper-lts-succeeded-spoil-cups-printing-part-one-prelude
---
<p>Yesterday (K)Ubuntu Dapper was released. The final version. I'm sure it is a great release, and most users will find it highly satisfactory for all their needs.</p>

<p>However, this blog is not all huggin' 'n luvin' for Dapper. It is a rant. I want this to be a wake-up call. It will sound negative to most of you. So be it....</p>

<p>Two months ago I <A href="http://blogs.kde.org/node/1899">blogged about Dapper's printing problems</A> which had shown up in the various beta- and pre-releases.</p>

<p>Seems this is not fixed in the final. Because also yesterday, I received 3 phone calls and 2 written messages  from customers or other people (one found me via my <A href="http://www.openbc.com/">"Open Business Club"</A> profile where I had signed up 2 weeks ago... heh) whom I gave paid or unpaid support in the past. All of them told me that Dapper had printing problems; the problems appear regardless of whether you run it from the Live CD or wether it is installed onto the harddisk.</p>

<p>Now, I did not have a current Dapper downloaded yet. So I could not verify myself. One of my contacts (he just had a fresh install from the Live CD completed) offered me to let me ssh in remotely so that I could have a closer look at the problem. But, alas!, no ssh login was possible. The openssh-server package seems to not be installed by default. WTF?? How are community or professional support people supposed to help Dapper users if it is not possible to remotely log into the system??</p>

<p>Anyway, the story continues. I was able to step the guy (he is rather new to systems belonging to the ${debian} family) through the process of <i>apt-get install</i>-ing an SSH daemon onto his system, so I could poke a bit at it. I didn't have much time, but these 10 minutes were enough to inflict a cold fury into my brain. How careless did the Dapper printing packager(s) put their stuff into the system! </p>

<p>Back in January, they insisted to gamble Dapper's printing capabilities onto CUPS-1.2. CUPS-1.2 was not yet even in Beta at the time, and there had not yet even been announced any binding release schedule. So they used an arbitrary SVN checkout (it was the then current revision "r4929"). That's risky, in case CUPS 1.2 was not released in time for Dapper's maiden voyage. But that's also OK -- after all, if it works out, all users will benefit, right?</p>

<p>However, I regard it as irresponsible behaviour to include bleeding edge software into a distro release, and then not care about its well-behaviour. Bleeding edge shipments just for the sake of sporting a higher version in the Distrowatch comparison charts is silly.</p>

<p>I had expected that one of the maintainers really dives into the new CUPS code, or at least into its new feature set, makes himself familiar with it, and creates a default CUPS configuration that gives Dapper users the best CUPS experience they could hope for. I'd somehow expect that they would subscribe to the CUPS.org mailing lists, ask questions, discuss how to take advantage of CUPS' new stuff and in general keep current with developments. Is that an unreasonable expectation?</p>

<p>Nothing of this happened. Yes, you could say, they were short of time and overloaded with work. I can sympathize with that; it's what happens to all of us FOSS activists. However, I do not sympathize with the maintainers <i>patching CUPS 1.2 away from its default setup</i> as provided by <A href="http://www.CUPS.org/">CUPS.org</A>, and then leave that work half finished and in big parts un-usable! Guys, please: if you...</p>

<ul>
 <li>...have enough "Knowhow" and confidence (or should I say arrogance?) to deplete me of features that you regard as a "security risk"</li>
 <li>...and have enough time to patch the default CUPS web interface to tell me you changed its behaviour away from its default</li>
</ul>

<p>...then I expect you to at least do a <i>complete</i> job. And a complete job would involve for you...</p>

<ul>
 <li>...to know what actually are the new features of CUPS, and how to enable and use (and how to disable) them</li>
 <li>...to fully document your changes and provide a working HOWTO to restaure all features you've taken away from your users</li>
</ul>

<p>I got a preliminary suggestion to make to you: If you can't deliver that, or if don't have time to follow through, just don't make your hands dirty at all! Don't then change stuff away from what CUPS ships as default setup. Leave it as is. Package that, and be done!</p>

<p>Someone made me aware of <A href="http://dot.kde.org/1149158725/1149176458/">a recent Dot comment</A>. I think especially one paragraph nails the issue on its head...</p>

<dl><dt>...and I'll quote it here:</dt>
  <dd><i>"We all remember the <A href="http://www.catb.org/~esr/writings/cups-horror.html">rant of ESR</A>, which he (wrongly) directed towards CUPS developers, when he was unable to configure his home printer because the RedHat printer config tool did mess up what CUPS itself would have done just fine... Is this another opportunity to flame Mike Sweet and the CUPS.org folks, just because their marvellous work gets spoiled by overshooting distro "experts" who make Linux unusably secure and safe??"</i></dd>
</dl>

<p>Now I'm just waiting for the Kubuntu Dapper CD download to complete. Once that's done, I can verify if my sources were right with what they said about Dapper+CUPS printing. I'll let you know about my further findings. This blog series will continue over the (long) weekend....</p>

<!--break-->