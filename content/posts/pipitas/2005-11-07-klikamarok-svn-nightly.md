---
title:   "klik://amarok-svn-nightly"
date:    2005-11-07
authors:
  - pipitas
slug:    klikamarok-svn-nightly
---
<p>Bonus not-FAQ:<p><br>
<b>Q: </b><i>What is the cool klik-ification of the week??</i>
<b>A:</b> Without even the slightest doubt, this would be <A href="klik://amarok-svn-nightly">klik://amarok-svn-nightly</A>. There are input ingredients to the recipe:  <A href="http://amarok.kde.org/blog/">Eean</A> provides these, Debian Sarge packages. He builds them every night from current amarok SVN. The resulting .cmg contains an embedded Xine engine. Dependencies are drawn into the .cmg from <A href="ftp://debian.tu-bs.de/">ftp://debian.tu-bs.de/</A> pool, via the magic of probono's <i>server-side apt</i>. For me this snapshot of amarok-1.4-SVN worked great even over an NX session (250 km distance). The amarok-svn-nightly.cmg file is about 9.1 MByte in size. The amarok playing NX server ran on SUSE-9.3, the local speakers were attached to a SUSE-8.2 box. Yes, the .debs build on Sarge resulted in a single .cmg file, working well on an alien distro. klik magic! However, the embedded Xine engine was not auto-discovered. I had to go through the settings to make it work. More testers still welcome. (Remember, you have to type "ian@monroe.nu" to start the download.) </p>
</p>
<!--break-->