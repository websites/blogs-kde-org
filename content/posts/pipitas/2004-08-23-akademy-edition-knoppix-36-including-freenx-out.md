---
title:   "\"aKademy Edition\" of Knoppix-3.6 including FreeNX is out..."
date:    2004-08-23
authors:
  - pipitas
slug:    akademy-edition-knoppix-36-including-freenx-out
---
 ... and the nice thing about it is that the final touches of the work has been done during the "Knoppix Remastering" tutorial at <a href="http://conference2004.kde.org/">aKademy</a>, held by Klaus Knopper himself. 

I am really looking forward to see the FreeNX server written by Fabian Franz running from CD. I guess a lot of people will be vexed about their own ignorance having so long dismissed NX for reasons which will now appear just ridiculous to everybody who looks at the actual source code of FreeNX server.