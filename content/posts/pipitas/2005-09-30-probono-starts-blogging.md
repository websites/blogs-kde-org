---
title:   "probono starts blogging"
date:    2005-09-30
authors:
  - pipitas
slug:    probono-starts-blogging
---
<p>A few days ago probono started blogging... and I didn't know or notice. Creative as he is, he called the thingie <a href="http://klik.atekon.de/blog/">"klikblog"</a>...   ;-)</p>


<dl>
<dt>If even I didn't know, how should the world at large? So let me quote in full his entry for today (hrmm... no, it was yesterday in our part of the world):</dt>
<dd>
<br />
<br />
<p align="left"><img src="http://www.recuperabaterias.com.br/images/skype%20logo.jpg" alt="Skype" align="left" class="showonplanet" />
<i>
"NewsForge asks to <a href="http://internet.newsforge.com/article.pl?sid=05/09/22/2039237&#038;from=rss">Send in the Skype clones</a>, so here they are: Which do you like best, <a href="klik://skype">klik://skype</a>,  <a href="klik://gizmo">klik://gizmo</a>,  or <a href="klik://wengophone">klik://wengophone</a>? Or do you prefer KDE&#8217;s <a href="klik://kphone">klik://kphone</a>? <a href="klik://kiax">klik://kiax</a> or <a href="klik://minisip">klik://minisip</a> maybe?"
</i></p>
</dd>
</dl>

<p>Welcome to The Club, probono!   ;-P </p>

<!--break-->