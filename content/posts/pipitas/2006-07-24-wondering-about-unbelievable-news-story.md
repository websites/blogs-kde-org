---
title:   "Wondering about that unbelievable news story...."
date:    2006-07-24
authors:
  - pipitas
slug:    wondering-about-unbelievable-news-story
---
<p>Following the <A href="http://blogs.kde.org/node/2188">news story</A> about the unsuccessfull British shipment (<A href="http://www.dailymail.co.uk/pages/live/articles/news/news.html?in_article_id=397124&in_page_id=1770">stopped in Bulgaria</A>) of radioactive material to the Iranian military (useful also to build a "dirty bomb"), of course a lot of questions come to mind, such as:</p>

<ul>
<li>how come that lorry was only caught in Bulgaria?</li>
<li>how many border controls did it previously cross without being suspected?</li>
<li>how many more such transports have already successfully arrived in Tehran which were never caught?</li>
<li>how many more such shipments are still planned and scheduled?</li>
<li>how many more European states/companies are involved in that sort of secret trading?</li>
</ul>
<!--break-->