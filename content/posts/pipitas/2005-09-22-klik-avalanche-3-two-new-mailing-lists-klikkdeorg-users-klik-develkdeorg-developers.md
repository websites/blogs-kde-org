---
title:   "klik avalanche (3): two new mailing lists -- klik@kde.org (users) + klik-devel@kde.org (developers)"
date:    2005-09-22
authors:
  - pipitas
slug:    klik-avalanche-3-two-new-mailing-lists-klikkdeorg-users-klik-develkdeorg-developers
---
<p>Not only has IRC channel #klik on Freenode.net seen quite some influx in recent days. We now also run two new mailing lists hosted by KDE, both related to <A href="http://134.169.172.48/">klik</A>. <a href="http://wire.dattitu.de/">Dirk</a> was so nice to create them on very short notice for us: </p>

<ul>
 <li><A href="mailto:klik@kde.org">klik@kde.org</A> is meant for end-user support; subscribe here: <A href="https://mail.kde.org/mailman/listinfo/klik">https://mail.kde.org/mailman/listinfo/klik</A></li>
 <li><A href="mailto:klik-devel@kde.org">klik-devel@kde.org</A> is for discussions about future innovations for klik; subscribe here: <A href="https://mail.kde.org/mailman/listinfo/klik-devel">https://mail.kde.org/mailman/listinfo/klik-devel</A></li>
</ul>
<hr>
<p>Here the description of the (user-oriented) <A href="mailto:klik@kde.org">klik@kde.org</A> list:</p>

<ul><li>
<i>
<p>klik is an innovative approach to (and a new implementation of) the old idea to make software runnable, "installed", by just copying it over to a system. </p>

<p>The concept of "AppDir" bundles is realizing that idea for Mac OS X (and did so already in NeXT), where everything an application needs is supplied in the application's self-contained sub-directory. </p>

<p>What klik adds to this idea is to melt the AppDir into a compressed zisofs or cramfs file system image. Result: instead of "1 program == 1 AppDir directory" klik advanced to a <i>"1 program == 1 file"</i> paradigm. </p>

<p>Another innovative approach of klik is to just supply  few-kiloByte sized "recipes" to klik clients requesting a certain software re-packaged into an AppDir compressed filesystem image, and letting the client himself automatically fetch the correct binary files (.rpm, .deb, .tar.gz packages) from distro and third party repositories and create his own bundle. </p>

<p>This makes klik very scalable, and the klik server will not be overloaded by the need to distribute complete multi-MegaByte sized binary bundles.</p> 

<p>Currently klik bundles need to be "mounted" with the help of the "loop" device, and the mountpoints need to be pre-defined once (by the root user) in /etc/fstab. In the future, we hope that the FUSE (Filesystem in UserSpacE) extension of the 2.6.14 Kernel, can help to remove these (and more) limitations which currently still hinder klik's full blossoming.</p>

<p>This list is for end user questions and discussions of the current klik technology, its supplied bundles, and the "recipes" it uses. It is for feedback of users having problems getting certain klik bundles to run, or wanting to relate their own klik success stories to the user community. It is *not* to discuss development issues of the current klik implementation or design suggestions for a future klik2 design: for <i>development</i> discussions please join the list <a href="mailto:klik-devel@kde.org">"klik-devel@kde.org"</a> at <a href="https://mail.kde.org/mailman/listinfo/klik-devel ">https://mail.kde.org/mailman/listinfo/klik-devel</a>.</p>
</i>
</li></ul>
<hr>
<p>Here the description of the (developer-oriented) <A href="mailto:klik-devel@kde.org">klik-devel@kde.org</A> list:</p>

<ul><li>
<i>
<p>1. This list is mainly meant for *development* discussions of current klik technology and all discussions around "klik2" (hey, this is just the working title for now), the next generation of klik. It is meant for people who intend to contribute code and ideas to klik2. Also, it is the forum of people who are maintainers of current klik recipes, and who exchange ideas with co-maintainers of other packages or with the klik developers.</p>
<p>2. This list is *NOT* for end user questions and discussions around the current klik technology, its supplied bundles, and the "recipes" it uses. For the <i>"end user support list"</i> of klik, subscribe to "klik@kde.org" at <a href="https://mail.kde.org/mailman/listinfo/klik ">https://mail.kde.org/mailman/listinfo/klik</a>.</p>
</i>
</li></ul>
<hr>
<u><h4>A few words about KDE + klik + other environments:</h4></u>

<ol>
 <li>The klik-devel@kde.org and the klik@kde.org lists are (obviously) hosted by KDE and we are grateful for that.</li>
 <li>It is true, klik originally started into its (so far short) live as the "<b>K</b>DE-based <b>L</b>ive <b>I</b>nstaller for <b>K</b>noppix+Kanotix".</li>
 <li>It is also true, that klik originally supported mainly KDE apps being klik-ified because KDE uses no absolute paths and its applications are more easily relocatable.</li>
 <li>But klik in the meantime has outgrown this initial scope.</li>
 <li>There are now a lot of non-KDE programs working well with klik.</li>
 <li>There is a growing number of Gnome apps which are in the klik server's repository.</li>
 <li>The "fake" URL of "klik://name-of-app" now works also with Firefox, Mozilla and even elinks (!!).</li>
 <li>klik is not an "only KDE apps"-supporting project, but open to all.</li>
</ol>

<!--break-->