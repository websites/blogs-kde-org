---
title:   "How Can Coders Give Access To Bleeding Edge Development Binaries To Help Their Non-Techie Contributors?"
date:    2005-09-14
authors:
  - pipitas
slug:    how-can-coders-give-access-bleeding-edge-development-binaries-help-their-non-techie
---
<p><b><i>How can KDE developers find ways to make binary packages of bleeding-edge code directly available to be run by usability experts for early feedback?</i></b></p>

<p>And if you are less interested in cooperating with usability people (like mornfall ;-P ), you may still ask yourself:

<ul>
<li>How can KDE developers make their bleeding-edge packages available to beta-testers? </li>
<li>How let translators see (in their actual, lively GUI context) the strings they work on? </li>
<li>How to enable art designers to actually *run* a program they are contributing icons and other artwork for? </li>
<li>How can these groups of non-technical KDE contributors (who are not typically compiling KDE every night from the latest checked-out sources) get a full preview of what will go out to our users on Release Day?</li> 
<li>How can this happen long before the final release, practically 5 minutes after the code was written and compiled, and long before there are official packages created by everyone's favorite distro?</li>
</ul>
</p>

<p>At <A href="http://conference2005.kde.org/">aKademy</A> I discussed this question with various people. By now everyone in the KDE community is aware of <b>one</b> of my proposed solutions (or that's what I like to think): use an <A href="http://www.nomachine.com/">NX</A> or <A href="http://freenx.berlios.de/">FreeNX</A> driven KDE Application Server, and provide remote access to the programs in question.</p>

<p>But you surely do go wrong if you imagine that my only occupation is NX. I have <i>another</i>, complementing proposal about how to tackle this same problem. One that is an especially nice fit in cases where testers and users do not have network connections.</p>

<p>One that works for Live CD distributions (Knoppix, Kanotix) as well as for Debian, SimplyMEPIS, Linspire, Ubuntu, Kubuntu and <A href="http://www.openSUSE.org/">openSUSE</A>/SUSE Linux 10.0.</p>

<p>I am currently working on a little <a href="http://dot.kde.org/">Dot</a> article outlining the proposal. (No, it's got nothing to do with NX.) </p>

<p>Be curious.</p>

<!--break-->