---
title:   "klik2 development: Milestone 3 is ahead"
date:    2008-02-03
authors:
  - pipitas
slug:    klik2-development-milestone-3-ahead
---
<p>Due to too little testing, the Milestone 3 release announcement is postponed until Wednesday. It may actually happen even later, should I have no time to really do it on Wednesday evening, due to my work-work obligations.</p>

<p>Anyway, here is what we have <A href="http://code.google.com/p/klikclient/wiki/PreliminaryRoadmap">planned for M3</A>.</p>
<!--break-->

<ul>
<li><b>More applications tested.</b> Specifically, we're now also adding Qt3-, KDE3- and FLTK-based apps to the mix. The 15 selected applications to be tested with M3 are: xvier, gobby, glade-3, hardinfo, xchat wireshark, grisbi, gtklp, xpp, htmldoc, skype, textmaker, opera, 3dchess, kdissert.</li>

<li><b>Two more klik commands supported.</b> Specifically, <i>"klik pack"</i> and <i>"klik unpack"</i> should work after M3 is reached:</li>

<ul>
  <li><i>klik pack.</i>Assume you have a klik2 application image of gobby on your Desktop. Then this command <tt>"klik unpack /home/kurt/Desktop/gobby_0.4.5-1.cmg /home/kurt/tmp/my-gobby.cmg/"</tt> should extract all the .cmg with all its contents (directory tree and files) into the given directory (which should exist and be writeable).</li>

  <li><i>klik unpack.</i> Assume you have an image tree of a xchat as a klik2 application in /home/kurt/my-xchat-dir/. Then this command: <tt>"klik pack /home/kurt/my-xchat-dir/ my_xchat.cmg"</tt> should create you a new .cmg. If you did put in some smart modifications of the right files, you may now have a 'personalized' xchat with all the settings you prefer that you can put on a USB stick and run from there, on any Linux computer, or from a Live CD that you booted.</li>
</ul>
</ul>

<p>Meanwhile, probono has been working on a facelift for the klik2 website. The web service presents the available klik2 applications to the end user. Unfortunately, we can't yet publish the exact URLs, since the server may not yet be prepared for even a mild version of slashdotting, and this is still very much work in progresss. Anyway, here are some screenshots.</p>

<p>All these show the page for "gobby", one of our 15 <A href="http://code.google.com/p/klikclient/wiki/MileStone3">Milestone 3 test cases</A>.  A lot of the content of that page is dynamically created.</p>

<p>Notice how there are 5 different tabs in the UI: Overview, Screenshot, Details, Feedback, Popularity and Contents (click on thumbnails to see their full size).</p>

<dl>
<dt><b>"Overview".</b> [image:3250 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>This tab gives you the link to download the recipe which your local klik client will use to build the portable klik bundle. Notice how the "download button" has a "Runs on" section with various distro icons. If you test klik://gobby on a new distro (and if you send off the feedback from the automatically popping up dialog!) the distro icon gets automatically added. "Mouse over" on one of the icons shows how many users have so far tested it, and how many succeeded. (Here the screenshot shows 3 out of 3 tests for Ubuntu 7.10 were successful).<br><br></dd>

<dt><b>"Screenshot".</b> [image:3255 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>This tab gives you a screenshot showing the app you want to download. The shot is automatically retrieved from the Freshmeat website. (So if you are a developer and want your application's screenshot on the klik website updated, or appear at all, make sure that you have your app registered on Freshmeat and have a nice one uploaded...). gobby currently shows no screenshot, because gobby is not registered at <A href="http://freshmeat.net/">Freshmeat</A>.<br><br></dd>

<dt><b>"Details".</b> [image:3251 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>This tab gives you a link to the recipe XML file that your klik client would receive and process when you klik-ed gobby. It also displays the .desktop file that will be used (it will end up as /usr/share/applications/gobby.desktop inside the gobby.cmg) that will be built by your klik2 client.<br><br></dd>

<dt><b>"Feedback".</b> [image:3252 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>On this tab you can see the last few feedbacks that the web service received from users who actually used the "Send" button (instead of "Cancel") on the automatically popping up dialog after you run and downloaded a klik://gobby for the first time. You can easily identify which distro and which kernel have been used, which klik client Subversion release was installed, and some more... <br><br></dd>

<dt><b>"Popularity".</b> [image:3253 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>This tab gives you a graph about that application's general popularity over time (not necessarily the same as its popularity as a klik bundle) as seen by Debian's <A href="http://popcon.debian.org/">"popcon"</A> utility. The graph is generated from data which users of Debian's <i>popularity-contest</i> package have contributed. <br><br></dd>

<dt><b>"Contents".</b> [image:3254 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]</dt>
   <dd>This tab provides a dependency graph for all packages that ended up automatically bundled into the .cmg application image. The image is created with the help of graphviz. For example, you can see for our gobby case here: gobby needs <i>libart-2.0-2</i>, which in turn needs <i>libavahi-common3</i>, which in turn needs <i>libavahi-common-data</i>, and so on...<br><br></dd>
</dl>

<p>[image:3257 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]Lastly, here is a screenshot of the current feedback table of our Milestone 3 testing. This table is created from your feedback automatically, and can be seen by klik2 developers. (Unfortunately I can't publish the URI for that table, because it would put too much load onto our web server if too many people used it. Hence this screenshot only for now.)</p>

<p>The challenge for M3 is to fill in all the gray fields with some real results and to make most of the red fields turn into yellow or green.</p>

<p>Funnily, now one of the applications that seems to create most problems is the simple "xvier". We used xvier in klik1 as the reference application, because it always worked, on all distros... <b>if</b> the klik1 client had been installed correctly. So if <i>klik://xvier</i> didn't work, we could tell the user "your klik1 client is faultily installed".</p>

<p>Maybe we need to make "klik://3dchess" that reference for klik2?</p>

<p>(Remember, if you want to help testing klik2 for the upcoming Milestones, visit <A href="http://code.google.com/p/klikclient/wiki/HowToTestGUI">our Wiki for some short SVN instructions</A> specific to your distro.)</p>
