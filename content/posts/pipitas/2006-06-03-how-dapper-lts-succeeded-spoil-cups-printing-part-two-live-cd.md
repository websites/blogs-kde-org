---
title:   "How Dapper LTS Succeeded To Spoil CUPS Printing (Part Two -- The Live CD)"
date:    2006-06-03
authors:
  - pipitas
slug:    how-dapper-lts-succeeded-spoil-cups-printing-part-two-live-cd
---
<p>Kubuntu Dapper CD download finished now. Took long enough to complete....</p>

<p>I'm booting one of the spare machines here in the office, a very cheap desktop workstation (Intel P IV 1.80 GHz, 256 MByte RAM, 20 GByte HD) into a full KDE 3.5.2 desktop system right now... </p>

<p>...</p>

<p>Woot! It is giving me the full resolution of the 1280x1024 LCD screen attached to it. It is surprisingly fast and responsive. I would have expected this system to behave rather slow with only 256 MByte of RAM, given that it runs from CD... but it is amazingly responsive! Its look is also very slick. I like it. I'm very pleased on this front.</p>

<p>Other positive items I've to note:</p>

<ul>
  <li>The setup of the UnionFS file system seems to be just perfect, as may be seen in the next point</li>
  <li>I can use "apt-get install" without any hitch to add more software (due my RAM limitations, I restrained it to <i>"apt-get install openssh-server"</i> though)</li>
  <li>networking is unbearably slooooow; Konqueror seems to wait 10-20 seconds before it even tries to start connecting to any URL I give it (<i>UPDATE: do "<tt>echo 'KDE_NO_IPV6=1' >> /etc/environment</tt>" to disable IPv6 support and speed up Konqui's networking</i>)</li>
  <li>The new "System Settings" application is a good step in the right direction; newbie users typically are overwhelmed when one exposes them to all offerings of kcontrol</li>
</ul>

<p>Things I didn't like too much:</p>

<ul>
  <li>No sshd is included; no remote login to the running system possible (think of support to newbies or some such)</li>
  <li>The local harddisk and its partitions are not shown to the user -- hence, they can not be mounted into the Live system (unless you know the commandline to do so); Dapper developers, have a look at how easy Knoppix makes this step for its users!</li>
  <li><i>System Settings --> Hardware --> Keyboard</i> didn't let me select the German keyboard layout; the CD had booted into an US layout (I had not paid attention to select the right one during the initial boot pause) [Yes, <b><i>I</i></b> know I should look in <i>Regional &amp; Accessibility</i> -- but does every newbie know this too?]</li>

</ul>

<p>Now let's look at the Live CD's printing capabilities...</p>

<p>These do indeed "under-perform", to use a modern business management term (or should I use geek language and say "they suck more than a Hoover vacuum cleaner top-of-class model"?).</p>

<TABLE width="95%" cellspacing="6" border="0" align="center" bgcolor="lightGray">
<TBODY align="left">
<TR align="left">
<TD colspan="1">  
<p><b>Executive summary:</b></p>
<ul>
<li>It is not possible to install a working printer into Dapper running from Live CD</li>
<li>It is not possible to print from the Live CD to remote printers shared by other CUPS servers</li>
</ul>
</TD>
</TR>
</TBODY>
</TABLE>

<p>Don't say these printing features are not possible from a Live CD; <b><i>just look at Knoppix how to do it</i></b>! I tried all methods known to mankind to install a printer into the Live CD Dapper. All with the same success:</p>

<ul>
<li>It didn't work via the "KDE Add Printer Wizard"</li>
<li>It didn't work via via the "System Settings" application</li>
<li>It didn't work via from the commandline with the CUPS "lpadmin" utility</li>
<li>It didn't work via the CUPS admin web interface </li>
<li>(It did work to add printers by manually creating printers.conf with an editor, copying the PPDs to their designated place and restarting cupsd -- but these printers wouldn't print then)</li>
<li>It is not possible to auto-discover remote printers shared by another CUPS server</li>
<li>It is not possible to print to remote printers shared by remote CUPS-1.1.23 servers even after printer discovery was enforced</li>
</ul>

<p>I surely do wonder if the Dapper CUPS packagers have ever tested these functions? All the error messages are very obvious; and any cursory printing test will let you stumble across them:</p>

<dl>
  <dt><b>"KDE Add Printer Wizard"</b> and <b>"System Settings"</b>:</dt>
    <dd>Both pop up a dialog saying: <i>"Unable to change printer properties. Error received from manager: client-error-request-value-too-long"</i> (<A href="http://blogs.kde.org/node/2070">screenshot</A>)</dd>
  <dt><b>"lpadmin" command:</b></dt>
    <dd>Returns a message saying: <i>"lpadmin: Request Entity Too Large"</i></dd>
  <dt><b>CUPS admin web interface:</b></dt>
    <dd>Displays a nearly empty page saying in big letters: <i>"413: Request Entity Too Large"</i> (<A href="http://blogs.kde.org/node/2071">screenshot</A>)</dd>
</dl>
[image:2070 align="right" hspace=4 vspace=4 border=0 width="300" class="showonplanet"]

<p>The failure to install a printer can't be caused by correct drivers being amiss on the CD; these are there, all of them. 67 MByte worth of PPDs (more than 2600 files) from <A href="http://www.Linuxprinting.org/driver_list.cgi">Linuxprinting.org</A> are packed onto the system. They just don't install when setting up a printqueue, and this is indicated by above error messages.</p>

<p>I tried to create various printers, using various drivers, and I all named them "dappertest&lt;N&gt;" (with &lt;N&gt; being just a number).</p>

<p>In all these cases, in fact a printqueue was created. However, it did not associate a driver with it; so the queue remained a "raw" one, which for most common use cases is non-functional (unless you have a PostScript print device, and know how to generate and send PostScript to it). The CUPS web interface reports <i>"Local Raw Printer"</i> in its "Make and Model" field. "<tt>lpstat -p</tt>" correctly lists the printer names I had assigned; "<tt>lpstat -v</tt>" lists the printer device URIs I had used; "<tt>lpoptions -p dappertest3 -l</tt>" tells me <i>"Destination dappertest3 has no PPD file!"</i>. </p>

[image:2071 align="right" hspace=4 vspace=4 border=0 width="300" class="showonplanet"]

<p>But printing to the "raw" queues doesn't work either (though I know how to do it...). Sigh!</p>

<p>I did one last check: create a "good" printers.conf file with an editor, place the PPDs into the appropriate sub directory, restart CUPS and see if that works. Well, it didn't. CUPS listed the new printers just fine (commandline, web interface and kprinter all display them; CUPS listed the printer device URIs completely and correctly (again, commandline, web interface and kprinter); and CUPS also listed the the PPD-derived print options (only commandline and kprinter -- the web interface did not, because not only have Dapper packagers deprived their users from doing administration tasks via the CUPS web, but their instructions about how to restore that behaviour do not work either...). </p>

<p>I can only assume that Ubuntu CUPS packagers' patching of the pristine CUPS sources and configuration (amongst other things, they're changing file permissions and ownerships) has introduced some weird bugs. Because I know for a fact, that all these things do work with a CUPS that is compiled from pristine 1.2.0 sources. (Yes, there is a 1.2.1 release out since 2 weeks, and it fixed about 30 different issues -- but none which are related to what I described here).</p>

<p>All this does not bode well for the next test: installing Kubuntu onto a harddisk and look how good or bad its CUPS printing support is. I'll do that tomorrow and report back then. Stay tuned.</p>

<!--break-->