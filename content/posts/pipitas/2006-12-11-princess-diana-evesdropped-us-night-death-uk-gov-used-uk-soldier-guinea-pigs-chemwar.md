---
title:   "Princess Diana evesdropped by U.S. on night of death; UK gov used UK soldier Guinea pigs in ChemWar experiments"
date:    2006-12-11
authors:
  - pipitas
slug:    princess-diana-evesdropped-us-night-death-uk-gov-used-uk-soldier-guinea-pigs-chemwar
---
<p>In recent months I acquired a habit of poking around from time to time on the <a href="http://observer.guardian.co.uk/">Guardian/Observer</a> website. Here is a collection of info atoms I picked up today:</p>

<ul>
<li>Some updates to <a href="http://observer.guardian.co.uk/uk_news/story/0,,1968664,00.html">Princess Diana's last 24 hours</a> alive:</li>
  <ul>
  <li>the night she died, the American secret service was eavesdropping her telephone conversations. (What's funny is that the source article says this happened "without the approval of the British security services"... Remember, the night she died she was partying in <b>Paris</b>, which is neither part of the US nor of the UK last time I checked).</li>
  <li>the driver of her Mercedes, Henri Paul, (who also died in the crash) had a second job (I bet without having told her): he was in the pay of the French secret service.</li>
  <li>his 14 (!) bank accounts had a combined balance of +100.000 £ (that's roughly +150.000 €, or +200.000 $US) in between them.</li></ul>Funny coincidences, huh?<br><br>

<li>The UK government <a href="http://observer.guardian.co.uk/uk_news/story/0,,1968718,00.html">used human Guinea pigs for chemical weaponry experiments</a>, for more than 30 years.</li>
  <ul>
  <li>While initial tests of <a href="http://en.wikipedia.org/wiki/Sarin"><i>Sarin</i></a>'s effects on the human skin were conducted with German prisoners of war, up into the 80s the 'research' was conducted on British soldiers.</li>
  <li>One of the known crime sites was the Porton Down military base.</li>
  <li>There are hundreds of known victims who suffered health damage from these <a href="http://en.wikipedia.org/wiki/Mengele">Mengele</a>-like experiments.</li>
  <li>Their current diseases include memory loss, flashbacks and lethargy.</li>
  </ul>I'm no longer surprised about the way these people are 'defending Western freedom' in Iraq, Afghanistan and elsewhere. Just don't claim I agree with you.
</ul>

<!--break-->