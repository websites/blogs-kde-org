---
title:   "Cool and Uncool"
date:    2007-04-10
authors:
  - pipitas
slug:    cool-and-uncool
---
<dl>
<dt><b>My Cool Discovery of Today:</b></dt>
<dd>A few days ago a few little known Chinese friends of KDE must have translated the <a href="http://techbase.kde.org/Getting_Started/Build/KDE4_(zh_CN)">"Getting Started/Build/KDE4"</a>-page on the <a href="http://techbase.kde.org/">TechBase wiki</a> into Chinese ("zh_CN"; AFAIK, this is the "Simplified Chinese" as used on mainland China).</dd>

<dt><b>My Uncool (but Confirmed) Apprehension of Today:</b></dt>
<dd>On one system (not mine) where I tried it, Konqueror can't print nor print-preview the page correctly; while Firefox on the same system *can* (should I say: <i>"...of course"</i>?!) do it... So don't tell me "You don't have the Chinese fonts on that system -- they need to be installed".</dd>
</dl>

<p>See also....</p>
<!--break-->
<p>...<a href="http://bugs.kde.org/show_bug.cgi?id=121224">bug 121224</a>.</p>