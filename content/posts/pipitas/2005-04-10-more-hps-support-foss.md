---
title:   "More on HP's support for FOSS..."
date:    2005-04-10
authors:
  - pipitas
slug:    more-hps-support-foss
---
<i>My last blog entry regarding re. "HP supporting their printers and scanners with open source drivers" made 2 private mails come into my mailbox. They challenged me with the other product type mentioned in the LinuxToday feedback, namely notebooks. One of them said, that beyond printer drivers, HP did nothing to support OSS, and a well known big name company with a 3-letter name was much better behaving in that respect.</i></i>
<i>What follows is a reproduction of my response to these mails (just HTML-ized and enhanced with inline-d hyperlinks).</i><br><br>

<hr>


<dl><dt>.</dt>
<dd>Dear Brendan,<br>
dear Dave!<br><br>
Well, I do not know if you could ever expect from any company to support an OSS project that is active on a field <b>not</b> related to their own business... Corporations are not there for philantropic reasons, and they don't support OSS just for trying to be nice. They do it to advance their own business, no?<br><br>
Anyway.... So while I am not employed by HP or any of its subsidiaries, and also have no contract rewarding me with any goodies should I create positive PR for them, here is what I know about it.<br><br>
As far as their "OSS supporting activities beyond printer drivers" go, I am aware of at least 4 distinctive fields where HP regularly puts in money and ressources:
<ul>
 <li>a good part of the <A href="http://www.debian.org/">Debian</A>'s server infrastructure is provided and/or hosted HP.</li>
 <li>several <A href="http://www.samba.org/">Samba</A> developers are able to work fulltime on Samba development by being employed by the HP labs.</li>
 <li>several <A href="http://www.x.org/">X.org</A> developers are able to devote work hours on X development while being on a HP payrole.</li>
 <li>the <A href="http://www.kde.org/">KDE project</A> receives a big help from HP for their aKademy series of conferences. </li>
</ul>

Of course, all these areas match to HP-related business units, which could benefit from better software and from the publicity that comes by putting up resources.<br><br>

However, somehow it appears to me like there is a very uneven reception about which big company does how much for Open Source, and how well this is known...<br><br>

For an interesting thought about a possible explanation for this apparent mismatch you may want to have a look at an <A href="http://us1.samba.org/samba/news/articles/lu46-allison.pdf">interview with Jeremy Allison</A> (from Samba Team fame) recently published by <A href="http://www.linuxuser.co.uk/">Linux User & Developer</A>:

<dl><dt>.</dt>
 <dd><i> "HP actually does quite a bit, and they don't get much credit for it. I don't quite know why that is. IBM seems to have a very high pro