---
title:   "How to simulate a slow network with 'wanem'"
date:    2007-10-15
authors:
  - pipitas
slug:    how-simulate-slow-network-wanem
---
<p>Some of you may remember my blog post <a href="http://blogs.kde.org/node/1878">"How to simulate a slow network (after all, QT_FLUSH_PAINT=1 doesn't work with Qt3)"</a> from nearly two years ago. </p>

<p>After all, I'm still getting a private mail or two every half year inquiring about it. That blog post did describe in some detail how you can use a command like </p>

<pre>  {/usr}/sbin/tc qdisc add dev lo root handle 1:0 netem delay 20msec</pre>  

<p>to emulate a network connection across the loopback interface that is handicapped by a 20msec latency delay. (Of course you can use a different interface, like eth0 to slow down the connection there, and a bunch of many different parameter to fine-tune to your needs). </p>

<p>If you missed that previous post, you may want to <a href="http://blogs.kde.org/node/1878">read it again</a>... </p>

<p>The "tc" command is part of the <i>iproute(2)</i> package available on most distros.</p>

<p>This morning I discovered a new tool that may help with the same scenario (which is: <i>"I need to test some application to run across network links which are crappy, but I can't test it because my own network connection is excellent...."</i> ). It is called <a href="http://wanem.sf.net/">"wanem"</a> (for 'Wide Area Network Emulator') and available on Sourceforge.</p>

<p>wanem is a complete Live CD, Knoppix-based. It allows you to configure all parameters you want via a comfortable web interface: available max bandwidth, latency delay, package loss, -- if you want, with different setting for uplink and downlink.</p>

<p>Underneath wanem is the same "tc" commandline used that my previous blog posting dealt with. However, the web interface does allow you to set up the thing more easily, with hardly any prior in-depth networking knowledge. The drawback is that you need to run it on a machine of its own (but that does not need to be any very new and shiny hardware, as long as it boots the Live CD).</p>

<p>Best, if that machine has two separate NIC interfaces that you use to put the machine in between your test client and your test server application (but if you read the docu, it tells you how you can use it with a single interface as well).</p>

<p>I've not yet tested using the Live CD ISO image to boot a VMWare or a VirtualBox machine, and use that for testing, but it should work nearly the same. (You can at least be sure that you'll not get *better* network performance than the settings you're using, and you want to test the bad conditions anyway...).</p>

<p>wanem auto-creates a few shell scripts that run "tc" for you with the desired settings. You can save and re-use those scripts at any later time, without re-configuring the web interface anew. Very convenient.</p>

<p>wanem is the perfect tool for you, if you are *not* a guru when it comes to TCP/IP, networking, routing, proxying, firewalling, gatewaying and iptables commandlines, but if you nevertheless want to test your application's performance when using network connections of all kinds and qualities. (Yes, that could be some simple web application. Yes, that also includes KDE3 and KDE4 applications supposed to run over a network connections via NoMachine NX, FreeNX, NX Free Edition, VNC, LTSP, remote X11, fish, smbclient, 2X or X2Go [Heh, I already can see how now a few people out there will start to consult Google about a few of the strange abbreviations I used in the last sentence...  :-) ] ).</p>

<p>BTW, wanem comes with some <a href="http://wanem.sourceforge.net/documentation.html">excellent documentation</a>. I wonder why the <a href="http://sourceforge.net/project/showfiles.php?group_id=200013">download figure</a> for the iso image (available since July 2007) is over 1000 while the main PDF document was downloaded less than 200 times. Is it still the same as it was many years ago already, that everybody starts installing and using an application first, without ever bothering to read the docu, and when the first problem occurs, just complain at a mailing list about it (without bothering to read the... oh, I said that already)? </p>

<p>Or is it that wanem is just so easy to use that nobody ever comes across a problem?  :-)</p>

<hr><p>Anybody ever tested compiz or kwin4 over the network?</p>

<!--break-->