---
title:   "OpenPrinting/LinuxFoundation: \"Hiring for Implementing PDF Printing Workflow\""
date:    2007-09-08
authors:
  - pipitas
slug:    openprintinglinuxfoundation-hiring-implementing-pdf-printing-workflow
---
<p>Here is a recent announcement from the OpenPrinting workgroup, hosted by the Linux Foundation. It didn't receive any widespread publication, AFAICS. But it deserves to:</p><i><dl><dt>&nbsp;</dt><dd>
<b>OpenPrinting/LinuxFoundation: "We are Hiring Students/Interns for Implementing the PDF Printing Workflow"</b>

<p><em>Posted by:</em> Till Kamppeter<br>
<em>Date:</em> August 29, 2007 09:08AM</p>

<p>One of the <A href="http://lwn.net/Articles/179511/">decisions</A> which was made on the <A href="http://www.linux-foundation.org/en/OpenPrinting/MeetingInfo#April_2006_-_OSDL_Printing_Summit_in_Atlanta.2C_GA">OSDL Printing Summit in Atlanta</A> last year and <A href="http://blogs.kde.org/node/1934">widely accepted by all participants</A> was to switch the standard print job transfer format <A href="http://www.linux.com/articles/53732">from PostScript to PDF</A>. This format has many important advantages, especially</p>
<ul>
 <li>PDF is the common platform-independent web format for printable documents</li>
 <li>Portable</li>
 <li>Easy post-processing (N-up, booklets, scaling, ...)</li>
 <li>Easy Color management support</li>
 <li>Easy High color depth support (> 8bit/channel)</li>
 <li>Easy Transparency support</li>
 <li>Smaller files</li>
 <li>Linux workflow gets closer to Mac OS X</li>
</ul></i><br />
<!--break-->
<i><p>To turn this into reality work is needed in many components of the printing infrastructure. The <A href="http://sourceforge.jp/projects/opfc">japanese team of the OpenPrinting work group</A> has already the needed CUPS filters in their Subversion repositories.</p>

<p>What is still missing is to make the universal print filter foomatic-rip (most printer drivers are integrated into the printing system with this filter) handling PDF input and to make the built-in printer drivers of Ghostscript also working with other renderers than Ghostscript, like XPDF/Poppler for example.</p>

<p>These two projects are now open for students or interns. If you like to take one of these challenges, go to the detailed project description and/or contact Till Kamppeter (till.kamppeter at gmail.com).</p>

<p>Till</p>

<p>OpenPrinting Manager<br>OpenPrinting (linuxprinting.org) Forum and web site administrator</p>
</dd>
</dl><br>
</i></i>
<p>You can also look at a more <A href="http://www.linux-foundation.org/en/OpenPrinting/Implementation">detailed project description</A>.</p>
