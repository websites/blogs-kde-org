---
title:   "Another piece of printing code that might be missing on KDE4..."
date:    2007-01-25
authors:
  - pipitas
slug:    another-piece-printing-code-might-be-missing-kde4
---
<p>Over at the <a href="http://www.cups.org/newsgroups.php?g+T0">CUPS mailing lists/forums</a>, an ongoing discussion thrashes out some changes that will affect the future of Linux desktop printing. One point is about the "Foomatic" drivers. This driver family is not "native" to CUPS -- Foomatic is a clever trick to plug Ghostscript as an add-on into the CUPS filtering system. So the question was, how could they blend into CUPS in a more harmoneous way? What could be done to not cause so many support calls for <a href="http://www.easysw.com/">Easy Software Products</a> complaining about Foomatic drivers, which they don't develop, and don't offer support for? (ESP is the company of Mike Sweet, the main CUPS author). </p>

<p>These calls typically come in to ESP from customers because the Foomatic database does put for some drivers the keyword "(Recommended)" into the user's face when he installs a printer. However, these "recommended" drivers do not work fully for example for Windows clients when printing to a Samba/CUPS print server.</p>

<p>Now Mike has now drafted a document <a href="http://www.cups.org/newsgroups.php?gcups.general+v:28392"><i>"RFC: New Driver Rating/Information Attributes"</i></a> and asks for comments.</p>

<p>Whatever the outcome of that discussion will be: there are going to come quite some changes in the way CUPS and Foomatic will transport printer driver information to the user who is tasked with installing a printer.</p>

<p>It means we need someone in KDE to write some new (KDE4?) code for this: parsing the PPD (or the output of a "cupsGetPPDs" call), and translating this into a pleasant and usable GUI interface for our users.</p>

<p>Currently there is nobody in sight.</p>

<p>If this remains the case, KDE4 will be falling behind the competition when it comes to printing -- after leading the pack all these years on this field...</p>

<!--break-->