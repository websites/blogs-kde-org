---
title:   "Woohoo -- Aaron is coding on nxc libs and a FreeNX Client!"
date:    2005-05-22
authors:
  - pipitas
slug:    woohoo-aaron-coding-nxc-libs-and-freenx-client
---
Aaron's got a <A href="http://aseigo.blogspot.com/2005/05/working-with-new-coders-artists-needed.html">better laptop</A>. Hey, that isn't very spectacular in itself. But what thrills me, is that the first thing he has worked on with that new asset of his is......... <i>(drum-drum-drum-druuuummmm!)</i> .... <A href="https://mail.kde.org/mailman/listinfo/freenx-knx">FreeNX</A>, or rather <A href="http://cvs.berlios.de/cgi-bin/viewcvs.cgi/freenx/nxc/">its client libary, nxc</A>.<br><br>

Woohooo! Aaron, come here, fetch yourself a big hug and two cookies!  ;-) <br><br>

KDE has already an NX client *cough*. It is called <A href="http://websvn.kde.org/trunk/kdenonbeta/knx/">kNX</A> and is at home in the kdenonbeta module of <A href="http://webcvs.kde.org/">CVS</A>/<A href="http://websvn.kde.org/">SVN</A>. It works, sort of. It doesn't support SSL encryption yet. Thinking of its coming into this world, it actually is a brilliant little thing: it was created in only two half-day coding sessions during <A href="http://dot.kde.org/1088363665/">last year's LinuxTag</A> (while visitors watched the progress, and often enough wanted to get a demo of some other KDE program in between). <br><br>

Unfortunately, since that date, the original authors didn't have any time free to get kNX into a really polished shape, not even as a standalone application. Other duties of life, such as university exams and job had to take precedence.<br><br>

Things have become better since LinuxTag, though: 
<ul><li>The FreeNX <A href="http://developer.berlios.de/project/memberlist.php?group_id=2978">core contributor team</A> has expanded to 6 people. </li>
<li>The FreeNX-kNX <A href="http://freshmeat.net/redir/freenx/56028/url_homepage/freenx-knx">mailing list</A> has silently grown to well over 300 subscribers. </li>
<li>The FreeNX <A href="http://freshmeat.net/projects/freenx">Freshmeat project</A> is amongst the fastest <A href="http://freshmeat.net/stats/#popularity">growing in popularity</A> (besides NoMachine NX, of course); it even received some <a href="http://freshmeat.net/stats/rating/?expand=rating#rating">favourable ratings</a>. </li>
<li>There were a few FreeNX releases, each of which added new features the (<A href="http://mail.kde.org/pipermail/freenx-knx/2005-May/001240.html">last one</A>, code-named <i>SambaXP Edition</i>, added printing and <A href="http://us3.samba.org/samba/news/#freenx-0.4.0">file sharing</A>).</li> 
<li>The new <i>nxc library</i> has been <A href="http://home.nc.rr.com/moznx/">created by Lawrence Rouffail</A> from the origins of some older <A href="http://www.nomachine.com/">NoMachine</A> code.</li> 
<li>The first usage for nxc was in the <A href="http://home.nc.rr.com/moznx/">moznx browser plugin</A> (which lets you install an NX client and run an NX session from a browser window).</li> 
<li>The nxc and FreeNX projects joined forces. </li>
<li>nxc now allows to create NX clients with only a few lines of code. </li>
<li>FreeNX moved to a <A href="http://developer.berlios.de/projects/freenx/">CVS repository</A>.</li> 
<li>nxc got checked in too. </li>
<li>Fabian (the FreeNX lead developer) a few weeks ago did a first raw version of linking nxc into kNX and found the result works better than the original code.</li>
<li>The main weakness remained all the time: lack of a good KDE-integrated NX client.</ul>

Now that Aaron shifts his engine into first gear, I am sure it won't be too long until we see a very nicely and well integrated NX client appear in KDE. NX as well as KDE deserve better than the current proof-of-concept kNX state.<br><br>

Aaron, I don't see your nxc code improvements in the repository yet. <i>Shall we set you up with a FreeNX CVS account?</i><br><br>
<!--break-->

