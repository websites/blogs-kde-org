---
title:   "AFPL Ghostscript 8.54 released -- and 8.54 is now GPL'd as well!"
date:    2006-06-15
authors:
  - pipitas
slug:    afpl-ghostscript-854-released-and-854-now-gpld-well
---
<p>Boy, did I nearly miss the news! Was it because of the soccer world championship? Anyway, the news rocks. I wonder why it was not picked up by any of the major news sites. (Heh... I'm sure they will now.) </p>

<p>I'll just quote what I read 10 minutes ago on <A href="http://advogato.org/person/raph/">Raph Levien's blog</A>. (Raph is the lead developer of Ghostscript since Peter L. Deutsch stepped down a few years ago. I met Raph in person last year at the LWE in San Francisco, and not only is he extremely knowledgeable about PostScript, Ghostscript, Fonts and Graphics in general, he is also a very nice person.) OK, here is the quote from his blog, dated 7th of June:</p>

<dl>
<dt>---></dt>
<dd><i>
<p> I have some great news to report. The leading edge of Ghostscript development is now under GPL license, as is the latest release, <a href="http://sourceforge.net/project/showfiles.php?group_id=1897&amp;package_id=108733">Ghostscript 8.54</a>.

</p><p> By switching to the GPL, we're reaffirming our commitment to the free software world. One big reason for this decision was to reduce the lead time between bugs being fixed in the development tree and users seeing the fixes, especially those users dependent on Linux distributions.

</p><p> Moving forward, we'd also like to resolve the effective fork with "ESP Ghostscript," so that our development tree is suitable directly for use in Linux distributions without a lot of extra patches. It would be very nice if all the GPL patches could be incorporated into the main tree without any license restrictions (which means that we need
copyright assignment), but realistically, we'll still have to implement an apartheid system of some kind, so that a GPL-only subdirectory exists that gets deleted out of our commercial releases.

</p><p> As <a href="/person/rillian/">Raph Giles</a> has <a href="http://www.advogato.org/person/rillian/diary.html?start=80">posted</a> recently, we're looking for a person to oversee this integration work, and to work more closely with the distributions and others in the free software community. Please let either of us know if you're interested. This might also be a good time to remind people of our "<a href="http://www.ghostscript.com/article/32.html">bug bounty</a>" program, which pays a nice little bonus for fixing bugs in our tracker marked with the "<a href="http://bugs.ghostscript.com/buglist.cgi?keywords_type=allwords&amp;keywords=bountiable&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">bountiable</a>" keyword.

</p><p> We haven't been getting a lot of development work from the free community recently, but we continue to get extremely valuable testing, patching, and other quality assurance. Thanks again to everybody in the community for this - it's much appreciated, and putting our leading edge development branch into GPL is one way of saying "thank you." I'm excited about the potential for working more closely with people in the free software world.
</p></i>
</dd>
</dl>

<p>I wonder which one will be the first distro to have packages ready...</p>
<!--break-->