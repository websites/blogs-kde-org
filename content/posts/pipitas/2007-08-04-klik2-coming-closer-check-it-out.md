---
title:   "klik2 is coming closer -- check it out    :-)"
date:    2007-08-04
authors:
  - pipitas
slug:    klik2-coming-closer-check-it-out
---
<p>I'll show you the current mount table of my openSUSE notebook:</p>

<pre>
  <b>02:25 lnx5000:~ > mount</b>
  /dev/hda1 on / type ext3 (rw,acl,user_xattr)
  fusectl on /sys/fs/fuse/connections type fusectl (rw)
  proc on /proc type proc (rw)
  sysfs on /sys type sysfs (rw)
  debugfs on /sys/kernel/debug type debugfs (rw)
  udev on /dev type tmpfs (rw)
  devpts on /dev/pts type devpts (rw,mode=0620,gid=5)
  /dev/hda6 on /home type ext3 (rw,acl,user_xattr)
  securityfs on /sys/kernel/security type securityfs (rw)
  /dev/hdc on /media/InfoStream7.5.00 type iso9660 (ro,nosuid,nodev,noatime,uid=1000,utf8)
  fusecram on /tmp/app/4/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/5/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/6/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/7/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/8/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/9/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/10/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/11/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/12/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/13/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/14/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/15/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fusecram on /tmp/app/16/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/17/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/18/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/19/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/20/mnt type fuse (rw,nosuid,nodev,user=kurt)
  fuseiso on /tmp/app/21/mnt type fuse (rw,nosuid,nodev,user=kurt)
</pre>

<p>You wonder what these 21 mounts, using fusecram and fuseiso are? It's how my current testing environment of <a href="http://klik.atekon.de/wiki/index.php/Klik2">klik2 technology</a> looks like.</p>

<p>klik2 will not use the loopmounting any more (you may be aware that it had several disadvantages, like being limited to a maximum of 7 concurrently running klikified applications). But now FUSE is widespread enough; most consumer distros ship with kernels that are FUSE enabled, and most also do setup the system to allow non-root users fusemounting in user space. (The 'iso' and the 'cram' names represent just 2 different ways of compressing the application images, via 'isofs' and 'cramfs'...)</p>

<p>These tests are based on <A href="http://mail.kde.org/pipermail/klik-devel/2007-August/000160.html"><b>Lionel Tricon's work</b></A> of the last 12 months, who brought klik2 goodness closer to a release.</p>

<p>If you want to give it a shot,...</p>

<ul>
  <li>...download the "klikruntime" tarball from Lionel's website at <a href="http://lionel.tricon.free.fr/KLIK/">http://lionel.tricon.free.fr/KLIK/</a></li>
  <li>...extract it</li>
  <li>...download the ready-made .cmg files of ksudoku, kdissert, pdfedit and Scorched3D (a game I didn't even know existed)</li>
  <li>...and then simply run <i>~/klikruntime/bin/zAppRun ./path/to/the/cmg-file.cmg</i> </li>
</ul>

<p>...to see what the state of klik2 is right now.</p>

<p><i>"What's the coolness about klik"</i>, you ask? <i>"I've never heard of it before."</i> Read <a href="http://klik.atekon.de/wiki/index.php/User%27s_FAQ"><b>the FAQ</b></a>!</p>

<p><u>In short:</u> klik implements a <i>"1 application == 1 file"</i> approach (hey, but it is still Alpha software)... and combines it with some comfortable 1-click web service coolness to run an application without "installing" it, and without messing with your valuable <b>system</b>. You can move the *.cmg (which encapsulates a given  application and its direct dependencies as 1 single file) to any place you like (USB stick, CD-RW, external harddisk, smb-mounted share,...) and run it from there as long as you have an <A href="http://en.wikipedia.org/wiki/Linux_Standard_Base">LSB</A>-compliant base system as your OS. It makes possible to "sandbox" applications. It makes possible to run different versions of the same application side by side when they "normally" would conflict. In the near future, we will have even more cool features coming.</p>

<p>klik is designed to make utilizing new software easier than ever before (by skipping the "installation" step), and making it at the same time "grandma safe".</p>

<p>klik2 brings the era of "application virtualization" to the Linux platform. (In case you do not yet know: this is a topic that is going to be hyped very soon on the proprietary MS Windows platform -- and it indeed does solve quite a few problems which are prominent and widespread there).</p>

<p>If you are interested in the upcoming klik2, come visit the <A href="irc://irc.freenode.net/klik">#klik IRC channel</A> on Freenode. Ask what time we'll hold our first-in-a-while (or next) developer meeting online...</p>

<!--break-->