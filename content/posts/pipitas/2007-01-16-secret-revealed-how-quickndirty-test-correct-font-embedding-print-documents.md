---
title:   "A Secret revealed: How to quick'n'dirty test for correct font embedding in print documents."
date:    2007-01-16
authors:
  - pipitas
slug:    secret-revealed-how-quickndirty-test-correct-font-embedding-print-documents
---
<p>I decided to leak a far well-guarded secret. It is about quick'n'dirty-testing of font embedding in print files. Pay attention: <i>I use kpdf as the KDEPrint "preview" application. By default, your system most likely uses kghostview.</i></p>

<p>To change it:</p>
<ol>
  <li>[alt]+[f2]</li>
  <li>type "kaddprinterwizard --kdeconfig", </li>
  <li>click "Run"</li>
  <li>click "Preview"</li>
  <li>enbable "kpdf"</li>
</ol>

<p><b><i>"WTF?! WHAAAAAT??"</i></b>, I hear you screaming, <b><i>"kpdf is for</i> <i>PDF, while the printing produces</i> PostScript<i>, and that can't be viewed by kpdf!"</i></b>.</p>
[image:2633 align=left hspace=12 vspace=12 border=1 size=thumbnail][image:2634 align=right hspace=12 vspace=12 border=1 size=preview]
<p>Yes and no. Yes, the printing application produces PostScript to sent off by kprinter to CUPS (or to the "Print to File ({PDF,PostScript})" dummy printers. And the same PostScript gets send by kprinter to the preview program (if previewing is enabled). When a recent kpdf version is fed with PostScript instead of PDF, it doesn't error out as in older versions. It silently runs "ps2pdf" (based on Ghostscript) to convert it to PDF and display this converted PDF.</p>

<p>Using kpdf for print previewing may have a few disadvantages:</p>
<ul>
  <li>ps2pdf will run first before a preview file is available</li>
  <li>ps2pdf will take a bit of CPU and memory and temporary disk space</li>
</ul>
<p>But kghostview possibly can consume _much_ more CPU and memory and temporary disk space when it chews on some nasty PS type input files directly -- files, which may go through ps2pdf well and fast...</p>

<p>But OTOH, you definitely have various advantages with kpdf over kghostview:</p>
<ul>
  <li>the preview window is easily resizeable</li>
  <li>the preview window remembers its lastly used size</li>
  <li>the preview image dynamically adapts to the size (if you use "Fit to Page")</li>
  <li>the preview is searchable (if fonts are correctly embedded)</li>
  <li>the preview app lets you go to "File --> Properties --> Fonts" and see the used font</li>
  <li>the preview window title bar shows you the path to the temporary PostScript file</li>
  <li>the preview will possibly be rendered much faster if you happen to print from a KDE $image application </li>
</ul>
<p>Soooo, you can save the temporary preview PostScript file (it's created by your app, and should be the same that is sent to CUPS) for debugging should you need it. You can compare the fonts that are embedded (or not) to the ones you used when you created the document (or someone else created it).</p>

<p>Especially if you test PDF files for printing (and if you try to debug wtf this one PDF doesn't print properly), here's a hidden catch and benefit:</p>
<ul>
  <li>kpdf does use <b>not</b> Qt (like all other KDE apps) to produce the printfile</li>
  <li>kpdf does instead use "pdftops" and the "Poppler" library, derived from Xpdf, to do this</li>
  <li>so printing the PDF from kpdf will produce a Postscript file created by Poppler/Xpdf</li>
  <li>telling kprinter to preview the file with kpdf (again) will prompt the (Ghostscript-based!) ps2pdf (re-)conversion</li>
</ul>
<p>You'll now have 2 windows of kpdf open:</p>
<ol>
  <li>the "original" PDF</li>
  <li>the previewed PDF, that has undergone a "(Poppler-)pdftops" --> "(Ghostscript-)ps2pdf" conversion</li>
</ol>
<p>For both windows you can look at the fonts and other details. For both windows you can look look at the "File --> Properties --> Fonts" details, and compare if they are the same, if all originally embedded ones are still embeddded,  or if a font was replaced by another one. In other words: it is a quick'n'dirty, but comfortable check if one important part of the software stack used in your printpath works as expected, or if you need to blame CUPS or the specific printer driver for any failure.</p>

<p>One example from today was this <A href="http://bugs.kde.org/show_bug.cgi?id=96394">bug report</A>. The reporter had this problem: <em>"There is a barcode in the pdf file located at: [<A href="http://bugs.kde.org/attachment.cgi?id=12053&action=view">$other_testfile</A>] This barcode displays correctly, but when the file is printed the barcode is printed as a solid black block."</em></p>

<p>What did I do to check this out for bug status CONFIRMED or INVALID? Open the file, look at the document info (yup, the barcode is embedded and has the name EanP36), print the file with "Preview" enabled in kprinter, look at the barcode at the bottom of the file (yepp, looks fine), check the document info (OK, the same barcode font still embedded, and the other fonts are also the same ones).</p>

<p>Actually, the font names will have funny prefixes like "WGVWHM+" before the real name. This is an indication that the font was not <i>completely</i> embedded into the file, but only as a "subset" of the full range of its glyphs.</p>

<p>In any case: <A href="http://bugs.kde.org/show_bug.cgi?id=96394">bug #96394</A> is now closed. WORKSFORME + INVALID. (Of course, at the time of reporting it may have been perfectly valid bug report about one of the components of the conversion chain; the reporter must have had a buggy Ghostscript or kpdf or poppler library -- the report was more than 2 years old).</p>

<p>You <A href="http://bugs.kde.org/attachment.cgi?id=12053&action=view">try it now</A> yourself, please. I would be interested if there are still installations out there that do not work for this particular file. Use the procedure I described. If it fails for you, tell me which versions of kpdf, Ghostscript, Poppler you've installed. At least one of these components must be outdated then.  Oh, and you don't need to waste paper: you can cancel the printing after the preview. And you don't need a real printer either: Just use a "Print to File (XXX)" printer in kprinter's list.</p>

<p>I intend to find someone who makes this procedure script-able (Bash and DCOP/DBUS, and possibly Kross) in order to contribute to the implementation of my <a href="http://bugs.kde.org/show_bug.cgi?id=140006">previous</a> uhmm... <a href="http://blogs.kde.org/node/2619">proposal</a>. Some nice, quick regression test script with a few standard (to be determined) input files, that creates a text report if everything turns out as we want it, eh? That can be run by anyone, to see if his own system also works as expected....</p>

<p><b><i>Now if only kpdf had a DCOP interface to query for embedded fonts, page sizes and other things....</i></b> </p>

<!--break-->