---
title:   "\"World now at the mercy of the sanity and honesty of the President of the United States\""
date:    2006-10-19
authors:
  - pipitas
slug:    world-now-mercy-sanity-and-honesty-president-united-states
---
<p>The 17th of this month was a historic day. </p>

<p>The US President has now acquired un-controlled, despotic powers. Everybody can be declared "an unlawful enemy combatant". Everybody! US inhabitant or not. Innocent or not. Without ever seeing a judge or a court. Just because the President deems so. And if that happens, you'll be defenseless against torture. Because that's is now legal too. The bill has been rubber-stamped by the Congress, with only minimal opposition by <i>some</i> democrats.</p>

<p><a href="http://video.google.com/videoplay?docid=-5517942312906824233"><b>Watch this video! It is a brilliant piece of the sharp Keith Olbermann journalism.</b></a></p>

<p><i>"Under this law the only thing keeping anybody out of Guantanamo is <b>the sanity and honesty of the President of the United States</b>.</i>"</p>

<p>The President now essentially has absolute power. Sounds very much like Hitler's <i>Erm&auml;chtigungsgesetz</i> (Enabling Act) of 1933. </p>

<p><font size="+1"><b>Be very concerned!</b></font></p>

<!--break-->

