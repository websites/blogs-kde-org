---
title:   "SVS, a klik-alike application for the Windows OS?"
date:    2006-04-16
authors:
  - pipitas
slug:    svs-klik-alike-application-windows-os
---
<p align="justify">It looks like the <A href="http://klik.atekon.de/wiki/index.php/klik:About">general idea</A> of <A href="http://klik.atekon.de/">klik</A> has come to the MS Windows world now too. I mean the idea of klik (or of Apple's AppDirs, if you want) how to handle software programs: namely, as by and large self-contained units that bring along all their direct dependencies within one single entity; units that are easy to relocate to a USB stick or to a CD-RW medium without a need for any installation process; units that maintain the ability to instantly run from any different place they might be saved to.</p>

<p align="justify">While I was browsing the Internet on Thursday afternoon (that was on Atlanta Airport, waiting to board the plane taking me back to Stuttgart), I discovered the <A href="http://www.svsdownloads.com/"><i>Software Virtualization Solution</i></A> (SVS) website. It said this: <i>"The SVS client isolates applications so that they can be turned on and off or removed instantly. It works on Windows 2000, Windows XP, and Windows 2003."</i> And: <i>"SVS is a completely new way to use software. By placing applications and data into managed units called virtual software packages (VSP's), SVS allows you to instantly activate, deactivate or reset applications and to completely avoid conflicts between applications, without altering the base Windows installation. Say goodbye to 'DLL Hell' and 'Registry Rot' forever!"</i> </p>

<p align="justify">Today, curious about what our Windows competitors may have learned from <A href="http://klik.atekon.de/maint/">us</A> (or what they may even have developed in excess to what we have), I tried to boot up my company notebook running Windows XP. But it didn't want to boot right now -- probably the same problem with the battery/powersupply combo that hit me already twice before.</p>

<p align="justify">I remember to have read the name of Altiris, the company that created SVS before, even very recently. But I can't remember exactly in which context, and I surely missed the SVS bit then.</p>

<p align="justify">Of course, using SVS to create a self-contained "layer" (as they seem to call their bundles) for an existing proprietary application XYZ first requires you to have the appropriate license of named XYZ.</p>

<p align="justify">However, www.svsdownloads.com seems to be a site that hosts a lot already-bundled, ready to launch software applications were you do not have to bother: from what I remember, there were listed programs like Firefox, Thunderbird, Internet Explorer, <A href="http://www.wengo.com/">WengoPhone</A>, BitTorrent, Azureus, OpenOffice, Abiword, <A href="http://www.videolan.org/">VLC Media Player</A>, Inkscape, Gimp, Blender, <A href="http://flightgear.sourceforge.net/">Flightgear</A>, NVU, Stellarium... (I missed <A href="http://KDiff3.sf.net/">KDiff3</A> though). They even had <i>Firefox 2.0 Alpha 1 (Bon Echo)</i>, like <A href="http://firefox2-alpha.klik.atekon.de/">we do</A>. :-) </p>

<p align="justify">These and some other applications are covered by various free-enough licences (ranging from free-as-in-beer to GPL-compatible); so downloading and putting them to work should pose no licensing problem.</p>

<p align="justify">So if any one of you occasionally has to use Windows, have a look at it. Feel free to tell me about pros and cons of SVS (before I waste my own time with it...heh).</p>


<!--break-->