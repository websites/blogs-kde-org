---
title:   "klik, Blogs.... and SUPERsuse!"
date:    2005-09-26
authors:
  - pipitas
slug:    klik-blogs-and-supersuse
---
<p>probono has introduced a new feature on the <A href="http://klik.atekon.de/comments.php">klik comments website</A>. It auto-creates a right-hand column with current quotes from Google search results: sites and Blogs linking back to the klik website.</p>

<p>I was amazed by the recent tide of spanish language blogs linking to klik. There must be exisiting a Spanish equivalent to Slashdot, and there must have been a klik story on there. There is no other explanation.</p>

So I researched, but failed. Instead, via the <a href="http://www.google.com/blogsearch?hl=en&filter=0&q=klik.atekon.de&btnG=Blogs+durchsuchen&scoring=d&sa=N&start=10">Google Blog Search</a>, I found these:

<dl>
<dt><a href="http://meondigitalworld.blogspot.com/2005/09/click-klik-linux-has-lacked-point-and.html">/me on Digital World</a>:</dt>
	<dd><sub>"Linux has lacked point and click installtion. Not anymore guyz. <A href="http://klik.atekon.de/">Klik</A> is here to change this forever. I am a Mandrake/Mandrivia fan, even though my distribution is not officially supported, i decided to give it a shot. Tried helix player and awesome it simply worked :). No installation, just <A href="klik://helixplayer">klik://helixplayer</A> huh!!!"</sub></dd>


<dt><a href="http://www.nascencetech.com/newvillageboy/2005/09/19/klik/">The Educated New Village Boy</a>:</dt>
	<dd><sub>"There's been an important development in the <A href="http://www.kde.org/">KDE</A> world that the rest of the Open Source community would do good to note. Klik, according to the <A href="http://dot.kde.org/1126867980/">introductory article</A> on <A href="http://dot.kde.org/">dot.kde.org</A> is a 'system which creates self-contained packages of programmes installable over the web with a single click'. The gist of it is that you get to download a full binary including all dependencies packaged within that allows you to run the an application without a care for typical Linux dependency hells."</sub></dd>


<dt><a href="http://www.linuxiran.org/modules/news/article.php?storyid=1313">linuxiran.org - News</a>:</dt>
	<dd><sub>"Have you ever heard of Klik? Yes, we are talking click in KDE slang. Klik is a system (read technology) which creates self-contained packages of programmes installable over the web with a single click."</sub></dd>

<dt><a href="http://supersuse.blogspot.com/2005/09/superklik.html">SUPER Journey</a>:</dt>
	<dd><sub>"Probono (Simon, the man behind KLIK) has created a SUSE based implementation of KLIK, which I am happily using, apart from the DNS outage KLIK had the other day. Thanks to Kurt Pfeifle's article series this amazing technology is now seeing prime time. You can find more on that on <A href="http://www.opensuse.org/SUPER_KLIK">SUPER KLIK</A> integrated into SUSE 10.0 this should really expand the normal desktop to become a 1 click run environment. Instead of installing applications are KLIKED, that's it. If you would like to see how Matt and myself imagine a 'klik' able and 'apt' able desktop based on SUSE 10.0 to be download SLICK on <A href="http://www.opensuse.org/1_CD_install">1 CD install</A>. When 10.0 comes out, the SUPER project and Simon will have most of the SUSE 10.0 OS klikable."</sub></dd>
</dl>

<p>The last quote is from <A href="http://www.opensuse.org/User:Agirardet">Andreas Girardet</A>'s blog. Andreas lives in New Zealand and used to be the man behind Yoper (an experimental distro that created some positive PR splash on <A href="http://www.distrowatch.com/">distrowatch</A> a year ago or so), and he works now for SUSE/Novell. His pet project is <A href="http://www.opensuse.org/SUPER">SUPERsuse</A> ("SUPER" == <b>SU</b>se <b>P</b>erformance <b>E</b>nhanced <b>R</b>elease).</p> The <A href="http://supersuse.blogspot.com/2005/09/con-kolivas-and-super.html">latest news</A> from his Blog also announces that Con Kolivas, a Kernel hacker renowned for his work to enhance Linux Kernel performance, had pledged to be the SUPERsuse Kernel patch maintainer.</p>

<p>Cool news! It also shows just how attractive SUSE has again become in the wider community since Novell's move to establish the <A href="http://www.openSUSE.org/">openSUSE</A> project. With a fully integrated <a href="http://www.opensuse.org/SUPER_KLIK">klik support into SUPERsuse</a>, openSUSE seems to be one of the most exciting current OSS endeavours, hosting even some rather experimental projects for <A href="http://en.wikipedia.org/wiki/Geek">Geeks</A> and <A href="http://en.wikipedia.org/wiki/Nerd">Nerds</A> under its big umbrella.</p>

<!--break-->