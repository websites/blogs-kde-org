---
title:   "Kernel 2.6.14 with FUSE Support -- Big Bonus for klik"
date:    2005-10-29
authors:
  - pipitas
slug:    kernel-2614-fuse-support-big-bonus-klik
---
<p>Kernel 2.6.14, released 2 days ago, has an few important new feature, that will is welcomed by all klik developers with big expectation: <A href="http://fuse.sourceforge.net/">FUSE</A> support (<i>Filesystem in User SpacE</i>) </p>

<p>Currently, klik needs entries like these in /etc/fstab:</p>

<pre>
  /tmp/app/1/image /tmp/app/1 cramfs,iso9660 user,noauto,ro,loop,exec 0 0
  [.....]
  /tmp/app/7/image /tmp/app/7 cramfs,iso9660 user,noauto,ro,loop,exec 0 0
</pre>

<p>These lines in fstab allow for loopmounting a .cmg file (a compressed -- via cramfs or zisofs -- file system image) underneath the mountpoints /tmp/app/[n]/ with the help of the .zAppRun script (part of the klik client), which enjoys standard user privileges only. But the requirement for these 7 fstab entries is a severe limitation for klik:</p>

<ul>
<li>First, to <i>add</i> them into fstab root privileges are required. (This happens during klik client installation. From then on, klik users do not need to become root any more to run a klik-ified package. Certain apps ask for root privileges to run -- but they do so too if "installed" properly). The "user" qualification also implies "nosuid" for all executables as well, so on this front klik is as safe as it can be.</li>
<li>Second, standard kernel setups allow only for up to 8 concurrent loopmounts. (klik generously abstains from using the 8th too -- since it wants to allow for KNOPPIX and Kanotix getting their lifeline too :-) and uses only 7). This means, a user can not run more than 7 klik-ed applications concurrently. (Well, he can, if he knows how to increase that number via a boot parameter, or by adding <tt>options loop max_loop=32</tt> into <i>/etc/modprobe.conf</i> and unloading and reloading the cloop device).</li>
</ul>

<p>FUSE changes that. FUSE in the Kernel does away with the fstab requirement to let users loopmount klik's .cmg images. And it doesn't impose such a low limit for the number of mounts either. </p>

<p>But I expect less than half of Linux users to upgrade to a Kernel 2.6.14 even within 12-18 months from now. Therefore, klik will continue to work the way it does for time being.</p>

<p>But klik development for FUSE support <i>is</i> happening, nevertheless: <A href="mailto:dmiceman@mail.ru">Dmitry Morozhnikov</A> has developed the <i>fusecram</i> and <i>fuseiso</i> FUSE modules (<a href="http://ubiz.ru/dm/fusecram-20051029.tar.bz2">download source code</a>). These enable everyone with a 2.6.16++ Kernel to take a glimpse at the future of klik.</p>

<p>Other klik related development news are:</p>

<ol>
<li><a href="mailto:killerkiwi2005@gmail.com">Jason Taylor</a> has succeeded in getting prototypes of a klik thumbnailer working for the KDE and Gnome environments. This will allow for an application-specific icon to be included into each .cmg file, and extracting it when the .cmg is "installed" onto a system -- without violating the "1 application == 1 file" paradigm. (The icon will transparently be extracted from the klik package by the klik client scripts).</li>
<li>The first klik recipe maintainer has received some initial "training". He even created his first private klik package (<A href="http://www.kde-apps.org/content/show.php?content=25125"><i>kchmviewer</i></A>, but it is not yet klik-released).</li>
<li>Several IT journalists have researched and investigated via mail or in the IRC channel background info about klik's wellbeing. So the next 2-3 months will see some online and printed stories about our current activities. (I wonder when the one big website which has my own klik articles queued up since more than 4 weeks now will finally publish them. Grrrmmm....).</li>
<li>I put some fixes into the  <A href="klik://k3b">klik://k3b</A> and the  <A href="klik://amarok">klik://amarok</A>  recipes. But I've not yet remove the "pending" status from them. This means there will be a slight hurdle in front of you, before you can testdrive the new recipes: you'll have to type in my KDE mail address to get the download of ingredient files (.rpms and .debs) started which you klik client then cooks up into the final .cmg.</li>
<li>Unfortunately, I did not get any helpful response from different KDE people I pinged about the problem with the KDE application manuals inside klik bundles. Since the manuals are usually started and displayed by khelpcenter, and/or via the help:/ KIO slave, a klik-ified KDE application, bundling its own bleeding-edge documentation (think KOffice-1.4.2 or 1.5.0 soon) inside the .cmg has no easy way to have these displayed if the user clicks on the help menu... :-( </li>
</ol>

<!--break-->

