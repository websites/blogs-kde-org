---
title:   "It's 'Hardy Heron'... not 'Hungry Hungry Hippo'"
date:    2007-08-29
authors:
  - pipitas
slug:    its-hardy-heron-not-hungry-hungry-hippo
---
So... the world is <a href="http://www.jonobacon.org/?p=1017">made to know</a> already what Ubuntu 8.04 will be named: it's 'Hardy Heron'. This choice has a good and a bad side to it. 

First the good one: it made me dicionary-lookup the 'heron' word (and stealthily confirm the 'hardy' one, since I wasn't completely sure about its meaning any more).

The bad one: I lost my secret bet about the next codename (which was 'Hungry Hungry Hippo').