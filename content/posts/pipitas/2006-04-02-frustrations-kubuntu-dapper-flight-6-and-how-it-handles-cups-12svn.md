---
title:   "Frustrations with Kubuntu Dapper Flight 6 and  how it handles CUPS 1.2svn"
date:    2006-04-02
authors:
  - pipitas
slug:    frustrations-kubuntu-dapper-flight-6-and-how-it-handles-cups-12svn
---
<p>A few weeks ago, <A href="https://blogs.kde.org/blog/57">Jonathan</A> had asked me on IRC in passing why kprinter and KDEPrint 3.5.1 didn't work with CUPS-1.2. My reply had been like <i>"CUPS-1.2 hasn't even released an alpha or beta tarball -- w.t.h. does Ubuntu Dapper plan to include an SVN version of a piece of core software which has a yet unknown release date??"</i> Of course, this is not Jonathan's personal field of work -- Kubuntu just inherits the CUPS version and setup which the Ubuntu main developers decided for.
</p>
<p>Regarding an impending CUPS-1.2 release, prospects have become much brighter in the meanwhile. During March, Mike Sweet released 2 Betas and the first Release Candidate. So a final 1.2.0 release seems pretty close now.
</p>
<p>After reading last night that <A href="http://cdimage.ubuntu.com/kubuntu/releases/dapper/flight-6/">"Dapper Flight 6"</A> was available, I decided to download the <A href="http://cdimage.ubuntu.com/kubuntu/releases/dapper/flight-6/dapper-live-i386.iso">Live CD version</A> of <A href="http://www.kubuntu.org/">Kubuntu</A>. Download took 6 hours, but luckily was completed after I woke up. 
</p>
<p>So the first thing I opened after booting was Konqueror with "<a href="http://localhost:631/">http://localhost:631/</a>". Indeed, a headline <i>"Common UNIX Printing System 1.2svn"</i> welcomed me. Hmmm... which version from SVN did they use? I know for a fact that the exact revision is shown if you compile from sources. So why did the Ubuntu developers patch it away? Was it in the name of "user friendlyness"? I can't see any reason why they then left in the 3 letters "svn", or even the "1.2" ones. At any rate, for the sake of clarity, it is *much* better to leave alive all info available when offering pre-beta software to one's users -- especially if it is a testing release itself like "Flight 6" is one.
</p>
<p>I had to run <tt>"dkpg -l cupsys"</tt> to conclude from the "r4929" part of the package name that this current Dapper Live CD included an arbitrary SVN version that's already <i>10 weeks</i> old. So my next riddle was: <i>"If they indeed do include bleeding edge, untested core system software in Dapper -- why don't they do it for real then? Why don't they go for the most recent revision available? Or why didn't they at least take the official Beta 2 release from 3 weeks ago?"</i> -- CUPS SVN Revision 4929 is at least 10 weeks old; Beta 2 (which happened around revision 5261) was released on 10th of March; RC1 is from 24th of March. Current SVN revision is at r5362...
</p>
<p>Anyway, the CUPS localhost:631 page welcomed me with this message:
</p>
<ul><li> <i>Administrative commands are disabled in the web interface for security reasons. Please use the GNOME CUPS manager (System > Administration > printing).  /usr/share/doc/cupsys/README.Debian.gz describes the details and how to reenable it again.</i>
</li></ul>
<p>Needless to say, that Kubuntu doesn't include something called <i>"GNOME CUPS manager"</i>, neither in the menu, nor to be found from the command line.
</p>
<p>However, that README.Debian.gz didn't include any description of "details and how to reenable it again". Very frustrating! Now, <b><i>I</i></b>'m probably able to find out what to do once I take some more time; but this morning I only had 30 minutes -- and these were not enough for me to get it into a working condition. (I'm now on a train, with just a few notes from this mornings experience, writing  all this down).
</p>
<p>But what was even more of an annoyance is that not only <i>administrative</i> commands were unavailable. The *whole* of the CUPS web interface seemed to be disabled! Not even the help pages were accessible! All I can see is the start page; clicking any of the links does do nothing. I wonder how they made that. (My first thought, that they had removed the executable bit from CUPS' <i>admin.cgi</i> was not confirmed -- all CUPS *.cgi programs are there, with all exec bits set).
</p>
<p>Looking at the cupsd.conf file reveals this: They have set <tt>"Browsing Off"</tt>. And that's not even inside the main cupsd.conf file -- they use the (very uncommon) <tt>"Include"</tt>  directive, to suck in a file named <tt>"browse.conf"</tt> that has this setting.
</p>
<p>Now talk all about security you want; but <tt>"Browsing Off"</tt> just blocks a user from auto-discovering other printers attached to CUPS print servers which may be in the same network and which announce their queues via a simple UDP package broadcasting mechanism. I can't see how this substantially increases security for users (you also don't make using WiFi an Olymic Steeple Chase either, do you?) -- but of course, you do decrease the comfort, user-friendlyness, power-to-get-work-done, usability and fun to work with Kubuntu.
</p>
<p>I suspect that the CUPS packager is just very unfamiliar with CUPS browsing. I remember an email discussion with one of the Debian guys from a few years back, which clearly showed that the person in charge then initially was completely clueless about how CUPS browsing worked. So let me repeat:
</p>
<pre>  Browsing On
  #BrowseAddress @LOCAL
</pre>
<p>is completely safe for CUPS clients. <tt>"Browsing On"</tt> makes them pick up and autodiscover any (UDP) printer announcements from local CUPS servers. It enables them to print with "zero configuration". It makes printing "just work", out of the box (provided the CUPS server is correctly configured). It gives them all printers without installing any printer locally. It gives them all driver information to be used from their clients without any need to set up a printer driver locally. <b><i>It does not turn their box into a print server! It does not make their box broadcast themselves. It does not make their box listening to and accepting TCP/IP connections. It does not do any dangerous things.</i></b> Only this...
</p>
<pre>  Browsing On
  BrowseAddress @LOCAL
</pre>
<p>...will convince their local cupsd to act as an active node that announces its own locally installed printers to the world around itself. @LOCAL is a shortcut that includes all local interfaces (i.e. the ones named ethN, wlanN, lanN,...), whatever their current IP address is, but excludes all "non-local" (a.k.a. dialup) interfaces, like DSL adapters, modems and ISDN cards (i.e. the ones named pppN,  isdnN,...). And even if these announcements do happen, it takes a few additional configuration settings to actually also accept print jobs from remote clients.
</p>
<p>Anyway, in the 30 minutes I had I was unable to figure what I could do to make the CUPS web interface work on Kubuntu Dapper Flight 6 as it is intended to work. (Hmmm... maybe there is even a bug in this old revision of CUPS SVN involved). Also (and needless to say after all this), KDEPrint's CUPS admin interface didn't even start up. I'm giving up on Kubuntu Dapper Flight 6 for now...
</p>
<p>But I'm trying to be positive in all my frustration, so here is the bright side of it: the Ubuntu developers *at least* included a clear hint on a well visible spot so a user can discover that things are unusual, and may conclude that all common advice he <i>may</i> get on any of the standard printing support lists ([<A href="http://www.cups.org/newsgroups.php?gcups.general+T">one</A>], [<A href="http://www.linuxprinting.org/forums.cgi?group=linuxprinting.general">two</A>]) may just be a dog's breakfast. That is way better than what f.e. SUSE does: they also patch CUPS away from the officially documented default settings, but without patching the CUPS documentation at the same time.
</p>

<!--break-->