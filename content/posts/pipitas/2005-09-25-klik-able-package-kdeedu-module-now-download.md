---
title:   "klik-able package of KDEEdu module now for download"
date:    2005-09-25
authors:
  - pipitas
slug:    klik-able-package-kdeedu-module-now-download
---
<p>The klik-enabled <A href="http://edu.kde.org/">kdeedu-3.5</A> packages compiled from today's Subversion checkout are <A href="http://opensuse.linux.co.nz/klik/testing/">now up for download</A>. (We do not offer yet a working <i>klik://kdeedu</i> link, though). They have passed some initial testing on SUSE-9.3 (with KDE-3.4.2), SUSE-10.0 and Kanotix 2005-03-DVD. They should be working fine on all KDE 3.4 systems. The package uses a $KDEHOME of $HOME/.kdeklik so it does not interfere with your standard $KDEHOME. It builds its own $KDESYCOCA, so startup needs some time. Let me know of any additional systems it works on (I am currently learning how to prepare this -- I need your feedback), as well as the exact error messages that pop up.</p>

<p>To install the 20 kByte klik client scripts, run "<tt>wget klik.atekon.de/client/install -O - | sh</tt>" and follow the instructions. Then, to get the maximum of debugging output, run the package like this: "<tt>sh -x .zAppRun kdeedu-latest-debug.cmg</tt>". My last blog has some more hints.</p>

<p>What I am interested most in is this:</p>

<ul>
<li>Successes and failures</li>
<li>Which OS/version are you using?</li>
<li>Which KDE version is running on there?</li>
<li>What are the exact error messages you are seeing? (No, not the ones where the Kernel tells you it can't mount -- these are a different thing)</li>
<li>Specifically, which libraries are not found?</li>
</ul>

<p>Next to come is a klik-able <A href="http://www.koffice.org/">KOffice</A> package (to show all these <A href="http://dot.kde.org/1127515635/">Alan Yates</A>-es of the world how lean a fast, complete and powerful a Free Software Office Suite can be made and yet support the OpenDocument format to the fullest). </p>

<p>And then, a complete KDE-3.5... Welllll, just kidding.  ;-)</p>
<!--break-->