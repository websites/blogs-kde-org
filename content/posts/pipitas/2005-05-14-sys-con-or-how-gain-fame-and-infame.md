---
title:   "Sys-Con, or how to gain fame and infame"
date:    2005-05-14
authors:
  - pipitas
slug:    sys-con-or-how-gain-fame-and-infame
---
Aaron <a href="http://aseigo.blogspot.com/2005/05/ethics-what-are-those.html">commented</a> on the interview made by Free Software Magazin with the owner-boss of various IT websites, some of which had been the platform for a poisonous pro-SCO/anti-Groklaw propaganda campaign, culminating in personal attacks against Pamela Jones, the brain and heart behind <A href="http://www.groklaw.org/">Groklaw</A>, orchestrated by one (now infamous) Maureen O'Gara. <br>
After senior editors of the "Linux World Magazine" threatened to resign over the scandal, O'Gara was ditched.<br>
I hadnt seen the interview Aaron mentioned. But I had read the <a href="http://www.sys-con.com/read/85049.htm"><i>"Letter to Our Readers"</i></a>, signed by Mark R. Hinkle (Editor-in-Chief), who also happens to be the COO of Win4Lin/NeTraverse. This piece prompted very mixed feeling during reading. It more sounds like marketing speak than anything else. It prays the prayer of "we are good, we do good, we do not harm anyone and we wish Pamela Jones all the best".<br>
I just couldnt take it for its face value. I rather felt annoyed by reading through it.<br>
Now Dee-Ann LeBlanc and James Turner, the two Senior Editors of Linux World Magazine have <a href="http://turner.linuxworld.com/read/1278212.htm">in fact resigned</a>, refering to the interview Aaron also took issues with. (BTW, I'd not wonder if the <a href="http://dee.linuxworld.com/read/1278292.htm">links</a> should soon be dangling, because their former bosses pullled their accounts.)<br>
I just wonder, if Sys-Con has any business connections with <a href="http://www.linuxworldexpo.com/">LinuxWorldExpo</a> too? I'll look that up later. Just now, I dont want to know more details about that felt. I just feel too pissed off already...
<!--break-->