---
title:   "Breaking News: Microsoft now loves Novell -- Reason To Be Concerned?"
date:    2006-11-03
authors:
  - pipitas
slug:    breaking-news-microsoft-now-loves-novell-reason-be-concerned
---
<p>
My head is dizzy from seeing all the <a href="http://www.novell.com/news/press/item.jsp?id=1196">current</a> <a href="http://www.novell.com/linux/microsoft/">news</a> about "Microsoft <font color="red" size="+2"><b> &hearts;&hearts;&hearts;&hearts;</b>s </font> Novell". Had no time to carefully read all the stuff. What struck my eyes was this sentence:
</p>

<dl>
<dt>Quote:</dt>
<dd><p><i>"This is a watershed moment for Linux. It fundamentally changes the rules of the game."</i><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.novell.com/linux/microsoft/openletter.html"><i><small>(Joint letter to the Open Source Community, From Novell and Microsoft)</small></i></a></p></dd>
</dl>


<p>Sure it could be a watershed.</p>

<p><b><i><u>If</u></i></b> it "fundamentally changes the rules of the game", my (mental) knee jerk reflex is: <b>be very concerned</b>.... Is Novell now selling out??  </p>

<dl>
<dt>Quote:</dt>
<dd><p><i>"We're really excited about this deal, and we hope you are too."</i><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.novell.com/linux/microsoft/openletter.html"><i><small>(Joint letter to the Open Source Community, From Novell and Microsoft)</small></i></a></p></dd>
</dl>

<p>Sorry, I don't trust that peace. The contract is for 5 years. Fizzles out in 2012. So what happens in 2013? I'm thinking from the p.o.v. of the Free Software "community".  (At the same time, I do not doubt that this new cozy relationship is for the good of Novell's short term economic interests, and their employees.)</p>

<p>Remember: Software patents <a href="http://fsfeurope.org/projects/swpat/">are evil</a>. And if they are evil, any cosy agreement based on software patents is tainted.</p>

<br><hr><b>[UPDATE: <i><a href="http://www.groklaw.net/">Groklaw</a> is more than just concerned. They write (even without my questionmark): <a href="http://www.groklaw.net/article.php?story=20061102175508403"><u>"Novell Sells Out"</u></a>.</i>]</b><br>&nbsp;<br>
<!--break-->