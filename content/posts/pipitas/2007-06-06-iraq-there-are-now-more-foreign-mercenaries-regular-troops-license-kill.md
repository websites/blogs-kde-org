---
title:   "\"In Iraq there are now more foreign mercenaries than regular troops. With a license to kill.\""
date:    2007-06-06
authors:
  - pipitas
slug:    iraq-there-are-now-more-foreign-mercenaries-regular-troops-license-kill
---
<p>According to <a href="http://comment.independent.co.uk/columnists_a_l/johann_hari/article2611720.ece">this commentary</a> in the online version of <a href="http://www.independent.co.uk/">The Independent</a>, there are now more hired mercenaries in Iraq than there are regular US and UK troops. 

<p>Of course, information about this practice is not officially released by any government: instead, the mercenaries are very often called "security contractors", "civilian operatives", or "reconstruction workers". The author of the commentary says: <i>"Britain alone has 21,000 in the country, raking in $1.6bn a year."</i> American mercenary companies such as <a href="http://en.wikipedia.org/wiki/Blackwater_USA">Blackwater</a> seem to take a big slice of that particular business cake too. They seem to hire men from around the globe to fill in their vacancies, including many from third world contries such as Colombia.

<p>I don't expect you to read the full article <b>now</b>. But keep in mind to make up for that leeway, next time you watch some TV news about a topic like "Again 3 US civilians reported to have been kidnapped and killed in Iraq"....

<!--break-->