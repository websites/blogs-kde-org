---
title:   "Microsoft Director says \"Joint or Separate - The Content Matters\""
date:    2006-11-10
authors:
  - pipitas
slug:    microsoft-director-says-joint-or-separate-content-matters
---
Jason Matusow, Microsoft's Senior Director for IP and Interoperability, emailed me to point <a href="http://blogs.msdn.com/jasonmatusow/archive/2006/11/07/joint-or-separate-the-content-matters.aspx"><b>to his blog entry</b></a>. There he responded to <a href="http://blogs.kde.org/node/2507"><b> my own one</b></a> that hints to the fact that <a href="http://www.novell.com/linux/microsoft/openletter.html"> Novell calls</a> a <i>"joint letter to the Open Source Community <b>from Novell <u>and</u> Microsoft</b>"</i> a document that <a href="http://www.microsoft.com/interop/msnovellcollab/open_letter.mspx">Microsoft calls</a> <i>"An Open Letter to the Community <b>from Novell</b>"</i>.

<!--break-->

