---
title:   "Playing with Opera9's \"Widgets\""
date:    2006-03-25
authors:
  - pipitas
slug:    playing-opera9s-widgets
---
<p>I should have been doing some serious work during the last 30 minutes, but....</p>

<p>...I came across this weekend's <A href="http://snapshot.opera.com/unix/Weekly-181/intel-linux/">Opera9 build</A> (no. 181) and decided to make a new <A href="http://klik.atekon.de/">klik</A> (*) recipe for it.</p>

<p>What is most interesting to see is how the Opera engineers are integrating the new "Widgets" concept into their browser. Widgets-like things are popping up everywhere around various software projects now; KDE4 with <A href="http://plasma.kde.org/">Plasma</A> will have builtin support for them too (whatever their name will be). </p>

<p>I liked the "Comic" one best; though I didn't get most of the last 3 Garfield jokes. The "Dictionary" one looks cool too; but it didn't fully get me on the hook; I'm a pretty heavy <A href="http://www.tu-chemnitz.de/~fri/ding/">"ding"</A> user -- ding is one of the first little applications I tend to start whenever I log into a computer account. (Which reminds me: I only once came across an occasion, where ding wasn't installed. Hmmm... do we have a <A href="klik://ding">klik://ding</A> recipe at all? Let me check... yep, <A href="http://ding.klik.atekon.de/">it's there</A> now. Even with a brand-new <i>en&lt;--&gt;de</i> translation wordlist  :-).</p>

<p>I can totally see how within a year or two, there will be tens of thousands of these little widgety programs around to pick from. But I also see how 99.9% will very likely be more of the "crap" quality type, from where only a few are shining out.</p>

<p>The interface for offering widgets to the Opera9 user to "select" from, "download", "keep" and "pin" them to the desktop is pretty easy and straight forward. Basically, it is one klik mostly (ooops... that was a funny typo; I'll let it go un-corrected :-P ). What surprised me most was that they worked out of the box with our pretty "standard klik-ify it!" Opera9 recipe. (Yes, the placement of the widgets on my desktop was a bit weird, and they kept jumping around in a frenzy at times; but I for now I blame for this behaviour the Opera9 development status. For now...)</p>

<p>Looking at these widgets and toying around with them could be inspiring new ideas into Linux desktop developers. And after all, klik makes this more easy than ever.</p>

<p>So what do people think?</p>

<hr>
 <small><dl><dt>(*) klik</dt>
<dd>Guessing from my email echo on previous blog entries, a portion of readers is always new to klik -- so you may want to check out this <A href="http://klik.atekon.de/wiki/index.php/What_is_klik_%28explained_in_less_than_50_words%29">50-word-description-of-klik</A>, the <A href="http://klik.atekon.de/wiki/index.php/How_to_install_klik_in_20-seconds">Howto-install-klik-in-20-seconds</A>, the <A href="http://klik.atekon.de/wiki/index.php/User%27s_FAQ">User's FAQ</A> and my <A href="http://dot.kde.org/1126867980/">introductionary article</A>). If you are already an Opera user, you should probably take <a href="http://opera9.klik.atekon.de/wiki/index.php/Wiki_page_for_opera9_klik#Hints_Of_Caution">a few simple pre-cautions</a>, if you value the current contents of your ''$HOME/.opera'' directory. klik-ing opera9 is 100% safe for your ''system'' in any case, since the klik will not touch your existing Opera-8.53 (or whichever) installation at all. Just take care of your "dotfile" user settings (but this you would have to do in any case when running a new version, klik or not-klik)</small></small>.</dd>
</dl>
<!--break-->