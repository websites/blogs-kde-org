---
title:   "Meet in Linuxprinting booth, LWE San Francisco (Aug 8-11) ?"
date:    2005-07-25
authors:
  - pipitas
slug:    meet-linuxprinting-booth-lwe-san-francisco-aug-8-11
---
<p>Woohoo!</p> 

<p>I'm going to the <A href="http://www.linuxworldexpo.com/live/12/events/12SFO05A/">LinuxWorldExpo</A> in San Francisco (Aug 8-11). I'll help run the <A href="http://www.linuxprinting.org/">Linuxprinting</A> booth in the <A href="http://www.linuxworldexpo.com/live/12/events/12SFO05A/exposition/CC999813">.org Pavilion</A>.</p>

<p>The flight is booked, <A href="http://marriott.com/property/abouthotel/default/sfodt?WT_Ref=mi_left">hotel</A> reserved -- thanks to the generous sponsorship of <A href="http://www.ricoh-usa.com/">Ricoh Corporation</A>, who are helping also our new <A href="http://printing.kde.org/">KDEPrint</A> maintainer <A href="http://www.kde.org/areas/people/cristian.html">Cristian Tibirna</A> to be present at the event.</p>

<p>Other people to be there: <A href="http://www.linuxprinting.org/till/">Till Kamppeter</A> of Linuxprinting.org and <A href="http://www.linuxprinting.org/database.html">Foomatic</A> fame (also the maintainer of the <A href="http://www.mandriva.com/">Mandriva</A> printing and digital imaging packages), <A href="http://www.levien.com/">Raph Levien</A> (<A href="http://www.ghostscript.com/">Ghostscript</A>), <A href="http://www.usenix.org/publications/library/proceedings/lisa98/brochure/photos/powell.gif">Patrick Powell</A> (<A href="http://www.lprng.com/">LPRng</A>), <A href="http://sourceforge.net/users/dsuffiel/">David Suffield</A> (<A href="http://hpinkjet.sourceforge.net/">HPLIP</A>) with a small selection of current HP printers, George Liu (Ricoh), Hitoshi Sekine (Ricoh), and last not least: our aforementioned Cristian (KDEPrint).</p>

<p>The plan is to show all state-of-the-art software that is currently available for Linux and Unix. One vehicle we are going to use is a complete 8-page pamphlet (working name for now is settled to "Linuxprinting Journal, LWE Edition" -- we are open to suggestions, however), that we will printed on a color laser printer, "live" at the booth.</p>

<p>The printer used is a brand-new Ricoh multifunctional model, Aficio CL7300, with a nominal print speed of 28 ppm for color printing. That device will be able to print on A3 (or, in the US we should maybe use 11x17 inches?), pleat the printout sheets in the middle ("center-fold") and finally put staples into the rabbet ("saddle-stitch"). It comes with a builtin Image Scanner that provides "scan-to-email", "scan-to-folder", "scan-to-FTP", TWAIN scanning, and "scan-to-print" capabilities.</p>

<p>The content of the "Linuxprinting Journal" will, of course, contain a series of useful articles about Linux printing software: CUPS, Ghostscript, PostScript, Foomatic, kprinter, HPLIP will make their appearance. Visitors will be able to take printouts away as we can do additional printruns at any time (because this all-in-wonder machine will be at our booth, fully functional). We are even pondering to take a digital image of each visitor, put it onto the front page of the pamphlet, print it and hand it over as a "personalized printout" to him/her.</p> 

<p>A professional layout for the pamphlet is guaranteed: we will be stress-testing the <A href="http://www.scribus.org.uk/modules.php?op=modload&name=News&file=article&sid=100&mode=thread&order=0&thold=0">Scribus-1.3 technology preview</A> for this.</p> 

<p>Of course, Till, Cristian, Raph, Patrick, David, George and Hitoshi will be there to demo Foomatic printer drivers, KDEPrint power, advanced Ghostscript usage, LPRng setup, latest HPLIP features and the Ricoh printer specifications, or anything related to these topics, as well as answer questions of visitors and journalists.</p>

<p>All in all, this will be quite an exciting experience for me.</p>

<p>Oh, and did I say that we will also bring <A href="http://www.nomachine.com/">NoMachine NX</A> and <A href="http://freshmeat.net/projects/freenx/">FreeNX</A> into the picture? </p>

<p>Yes, we will be able (if the IT God doesn't cut our internet connectivity from the LWE booth) to have remote helpers do our Scribus layout for us. Because Scribus will run on a <A href="http://en.wikipedia.org/wiki/Application_server">remote KDE application server</A>. A prepress professional with lots of Scribus skills has promised to be available online and do the layout for us. (He uses a Mac NX client, btw.). So we only need to write the content as ASCII text and upload it to the remote application server, where under the hands of the publishing pro it will turn into a nice layout. </p>

<p>Heh, did I say "upload"? Of course, this will be completely transparent. Using <A href="http://kate.kde.org/">Kate</A> and KDE's ueber-cool <A href="http://en.wikipedia.org/wiki/KIO/">KIO Slave Technology</A>, we will be able to open the remote file with the standard KDE file dialog just as if it were a local one, and we can still decide wether we should prefer to use <i>webdavs://nx-app-server.kde.org/lwe-folder/</i> or <i>fish://host2.kde.org/lwe-folder/</i> (What would <b>you</b> advice us??)...</p>

<p>Oh, wait. No need to use <i>webdavs://</i> (or some such) directly and manually, with the extra password authentication it takes for each access. If we use FreeNX to work on the application server in a remote session, we could also test the new "nxfish" utility that is slated for release in FreeNX-0.5.0 during the LWE week. nxfish is able to "mount" any remote resource in a completely transparent fashion for each NX user. (And each user may have individually different resources assigned to him, I might add.) The NX user is automatically authenticated once his session runs (and the administrator can any time remove the resource during the session).</p>

<p>So, assuming our Layout Professional in France readies our promised Scribus document template in time (he will create color separated "PreFlight-checked" PDF X/3 for us), we should be able to connect to the remote KDE application server via NX, open the document in the remote session, and print it to our local Ricoh color laser printer.</p>

<p>You think we would have to do a manual file transfer from the remote NX server to one of our local notebooks first, and print it from there? </p>

<p>Wrong. </p>

<p>Nothing complicated like that is required :-) . We will be using a beta version of a nifty new tool written by Fabian. This will make sure that each KDE users connected to a remote NX session will "see" all the same printers inside the session as he would see when looking at an un-connected local KDE session on his notebook or workstation. And he will be able to use the correct printer driver from the NX session (hey, he does not even have to configure that -- it is pure and 100% zero-conf printing for the FreeNX user). Last to mention, but not least important: it will be completely secure. Each user only sees and has access to his own local printers. So our layouter in France will make proof-prints to his local PCL HP inkjet (using the latest HPLIP drivers, of course), while we will do production prints to the booth's PostScript Ricoh color laser, each one auto-using the correct printer driver. And we both will use our own, correct color management profiles.</p>

<p>Compare that to the mess and overly-complicated handling of printing when using <A href="http://en.wikipedia.org/wiki/Terminal_server/">Windows Terminal Services</A> or <A href="http://en.wikipedia.org/wiki/Citrix_ICA">Citrix MetaFrame</A>, or <A href="http://en.wikipedia.org/wiki/Tarantella,_Inc.">Tarantella</A>. And in other remoting solutions this is not possible at all (SunRays or <A href="http://en.wikipedia.org/wiki/VNC">VNC</A> couldn't even attempt to run the type of <b><i>trans-oceanic collaborative document production and printing workflow that allows for global mobility of all participants</i></b> which we are pulling off for LWE).</p>

<p>So, if you are at LWE, come visit me at the Linuxprinting booth in the .org pavillon. I might be busy a lot, but do not shy away from trying to talk personally to me (if you are a journalist, you are persistent anyway, as I already know. ;-) )</p>
<!--break-->