---
title:   "\"How they stole the 2006 mid-term election\" (Reading Recommendation #1)"
date:    2006-11-07
authors:
  - pipitas
slug:    how-they-stole-2006-mid-term-election-reading-recommendation-1
---
<dl>
<dt><p>Today's reading recommendation #1. Extracts:</p></dt>

   <dd><p><br><a href="http://www.gregpalast.com/how-they-stole-the-mid-term-election"><b>HOW THEY STOLE THE 2006 MID-TERM ELECTION</b></a><br><small>by Greg Palast, for The Guardian (UK), Monday November 6, 2006</small><br><br>Here's how the 2006 mid-term election was stolen.<br><br>Note the past tense. And I'm not kidding.<br><br>And shoot me for saying this, but it won't be stolen by jerking with the touch-screen machines (though they'll do their nasty part). While progressives panic over the viral spread of suspect computer black boxes, the Karl Rove-bots have been tunneling into the vote vaults through entirely different means.<br><br>For six years now, our investigations team, at first on assignment for BBC TV and the Guardian, has been digging into the nitty-gritty of the gaming of US elections. We've found that November 7, 2006 is a day that will live in infamy. Four and a half million votes have been shoplifted. Here's how they'll do it, in three easy steps:<br><br><i>Theft #1: Registrations gone with the wind</i><br><br>On January 1, 2006, while America slept off New Year's Eve hangovers, a new federal law crept out of the swamps that has devoured 1.9 million votes, overwhelmingly those of African-Americans and Hispanics. The vote-snatching statute is a cankerous codicil slipped into the 2002 Help America Vote Act — strategically timed to go into effect in this mid-term year. It requires every state to reject new would-be voters whose identity can't be verified against a state verification database.<br><br>Sounds arcane and not too threatening. But look at the numbers and you won't feel so fine. About 24.3 million Americans attempt to register or re-register each year. The New York University Law School's Brennan Center told me that, under the new law, Republican Secretaries of State began the year by blocking about one in three new voters.<br><br>How? To begin with, Mr. Bush's Social Security Administration has failed to verify 47% of registrants. After appeals and new attempts to register, US Elections Assistance Agency statistics indicate 1.9 million would-be voters will still find themselves barred from the ballot on Tuesday.<br>[....]<br>   &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; (<a href="http://www.gregpalast.com/how-they-stole-the-mid-term-election"><b> ----> more</b></a>)</p></dd>
</dl>   

<!--break-->
