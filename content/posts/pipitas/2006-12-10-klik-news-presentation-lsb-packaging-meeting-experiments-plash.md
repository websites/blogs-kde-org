---
title:   "klik news: presentation at LSB packaging meeting; experiments with 'Plash'"
date:    2006-12-10
authors:
  - pipitas
slug:    klik-news-presentation-lsb-packaging-meeting-experiments-plash
---
<p><a href="http://klik.atekon.de/blog/">probono</a> last week gave a presentation to the participants of the <a href="http://www.freestandards.org/en/LSB_face-to-face_%28December_2006%29">LSB packaging meeting</a>, which took place in Berlin (hosted by SAP). <a href="http://klik.atekon.de/presentation/">His slides</a> are available on the klik website.</p>

<p>Expect some improvements and changes in klik in the next few months as a result from the discussions that took place there.</p>

<p>From the klik development front the most exciting news are about <a href="http://klik.atekon.de/wiki/index.php/Plash">some experiments</a> of utilizing a thing called <a href="http://plash.beasts.org/">"Plash"</a>. Plash is little know so far, so let me throw a fast outline at you:</p>

<ul>
<li>Plash stands for the "principle of least authority shell" (see also <a href="http://en.wikipedia.org/wiki/Principle_of_least_authority">POLA</a>).
<li>Plash can run programs while giving them access to only the files and directories they need to run.
<li>Plash 'virtualizes' the filesystem as seen by the programs. 
<li>Plash gives to each process its own namespace.
<li>Plash's namespaces for applications contain only a subset of all files present on the system. 
<li>Plash requires no kernel modifications.
<li>Plash is implemented by modifying GNU libc, replacing the system calls that use filenames. 
</ul>

<p>To give an example: the open() system call is modified. Under Plash, open() sends a message to a file server via a socket. If the request is successful [i.e. the client program using open() is allowed to access the file], the server sends back a file descriptor. Processes run as user 'nobody' in a chroot jail. </p>

<p>If this outline has sparked your interest in Plash and want to learn more, go grok this <a href="http://www.cs.jhu.edu/~seaborn/plash/README.txt">Plash README</a> next. </p>

<p>If you've ever looked at klik before and how it works, you'll probably see the enormously useful potential that lies in the marriage of klik with Plash.</p>

<p>If you've not looked at klik before, you surely missed a fun experience with using new programs on your system. In this case, visit the <a href="http://klik.atekon.de/">klik website</a>, read the <a href="http://klik.atekon.de/wiki/index.php/User%27s_FAQ">user's FAQ</a>, and give some new programs like <a href="http://pdfedit.klik.atekon.de/">pdfedit</a> a spin....</p>

<!--break-->