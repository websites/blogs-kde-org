---
title:   "A commercial Qt application"
date:    2007-07-19
authors:
  - amantia
slug:    commercial-qt-application
---
Today I discovered an application that is using Qt4. It was a complete surprize, as this is a software that you can download from a photo-shop's web page to create so called "photo-books". Unfortunately it is a Windows only application, but I was happy when I saw that QtCore4.dll & others were installed (inside wine). :)
 The application is in Romanian, but as I saw it was developed in Germany. It is called CEWE Fotocartea Mea (CEWE My photobook) and can be downloaded from <a href="http://dls.photoprintit.de/download/Data/8314/setup_CeWe_Fotocartea_mea.exe">here</a>.