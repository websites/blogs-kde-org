---
title:   "The story of some bugfixes"
date:    2012-09-30
authors:
  - amantia
slug:    story-some-bugfixes
---
 This is the story of how bugfixes can happen. For each bug there is a bug reporter. It doesn't really matter if it is another developer or a non-developer user, as in the end it is just a user. Or does it? Let's see.

The first bug I fixed not so long ago was a line on the chat asking "what's ctrl+shift+l supposed to do in kmail?" from Albert (tsdgeos). Sounded like user support, but turned out that the reason he asked me was that pressing this combination crashed KMail. Of course, I tried and could reproduce it. It was related to Favorite Folders and as nor the reporter, nor I had such folders configured, it was rather straightforward where the problem is. Half an hour ago the bugfix was committed, everybody is happy. 

The second was more complicated. There was a long standing report from our KDE expert David Faure that resulted in losing the mail content on move. It wasn't the first time I looked at the code to find the problem, without success. We couldn't even reproduce the bug. Then suddenly a message came on IRC: he could reproduce it again and found a pattern how it happens: it happens if the source mail account (resource) is in offline mode, meaning KMail *might* not have access to the whole mails, only to the cached parts (the mail header). Having this information finding the bug was relatively easy and next day David has the patch. But he wasn't happy: now he got an ugly message box with an error that doesn't mean too much, an error that doesn't tell what is the problem and what could be the solution. So I looked up how this could be fixed. The solution wasn't straightforward, involved changing several pim related components, including adding a new DBUS method that is used to communicated between different Akonadi part, but in the end I had it...only to realize that although I get a nice error now when copying mails, I can't read my mails. Because it gives a DBUS related error. I've added a method returning a string, but from the error it said that it was expecting a string, but got a boolean. Confusing.
 Looked at the code, looked again, tried different things: no go. I told to David (knowing that he is an extermly good developer), that here is the code, here is the error, I have no idea what to do.
He looked at, and all seemed fine. It was late, we both called it a night and went to sleep. Of course the problem bothered me, so next day I wanted to give it a go. Guess what, David was also online (this was Saturday), he was also interested in the problem. :) To make the story short, he found the issue: the problem was a <code>setDelayedReply( true );</code> call I overlooked. That caused basically QDBus to ignore our return value, that must be sent later (and the code sent a boolean there). David did a refactoring, I found a corner case, we both tested, all was fine. After a few final touches, like making the string translatable the patch was ready for review. As David said, it was a nice teamwork.

The last one was the same day at evening: a complaint that KMail cannot move/delete folders from an IMAP inbox. Weird bug, as most of us use IMAP, so this should have been noticed before. I asked for details, I asked for different logs, to try different scenarios. The reporter followed everything I told promptly, and the surroundings of the bug started to take shape. He had an IMAP server without support for ACL (access rights). In that case we should assume everything is fully accessible. And so was everything for him, but the top-level inbox. Having that information, by reading the code I found the faulty line - a line that was weird, and unneeded. The line was introduced because of a bugfix, so I looked up the bug only to find users complained it is still not fixed. No wonder, as the fix was not doing anything about the original problem, but unfortunately created other problems. I have to admit that the API involved is easy to be used in a wrong way, but hard to fix now. For those interested (and to avoid such API), here is the problem:
<code>
// Returns the rights the user has on the collection.
Collection::Rights Collection::rights() const
{
  CollectionRightsAttribute *attr = attribute<CollectionRightsAttribute>();
  if ( attr ) {
    return attr->rights();
  } else {
    return AllRights;
  }
}
</code>
<code>
// Sets the @p rights the user has on the collection.
void Collection::setRights( Rights rights )
{
  CollectionRightsAttribute *attr = attribute<CollectionRightsAttribute>( AddIfMissing );
  attr->setRights( rights );
}
</code>

And the problematic commit looked like this:
<code>
Collection root;
root.setRights( Akonadi::Collection::CanCreateCollection );
</code>

You see, by default, a Collection object doesn't have any rights set. If Collection::rights() is called it will return AllRights. What happens after the above setRights() call? The Collection will have now right attributes (CanCreateCollection), so a further Collection::rights() returns that attribute. But *only* that one. Certainly not the behavior one would expect. We can debate that the plural form (setRightS) suggests that, but I still find bad API. 
Back to the reporter, luckily the problem could be fixed with the developer tool called "akonadiconsole", so he was happy. Other users will get the fix in the next release.

From the three reporters two were KDE developers. One not having too much knowledge about the PIM part, the other having some knowledge, as he is the one running in the most weird PIM Issues, and we just tell to him to fix his own bugs, while the third was somebody who I never met before, so I assume he is a regular user.

What helped in all cases was communication: giving valuable information (like how to reproduce, what is your configuration, etc) and carefully following the instructions the developer makes. Especially if the developer cannot reproduce it, the only real chance to fix it is that the user acts as the eyes and hands of the developer. If I tell to my hand to type "foo", but it types "bar", nothing good will result in it, as I'd expecet that "foo" was typed and act further accordingly. 
 So bug reporters, please try to gather as much information from your side as possible when creating a report and always follow the requests a developer makes.

You should not draw a wrong conclusion from the above: that reporting on *development channels* on IRC helps and you should always come there with your bugs. IRC is great for instant communication. It is very bad though as you might not find the right person there when you report. 
Use the bugtracker. Wait patiently for feedback (e.g. I work on KDE only from time to time and many others do the same). It is ok to ask about certain bug on IRC, but don't forget to report it. It is also ok, once the developer responds and starts to work on the bug to contact him on IRC (if you know its nick), as communication is faster on it. 
 Just saying something doesn't work on IRC will also not do any good, unless you are prepared to help track down the issue. And then you will suddenly become part of the team. It is not that hard to be a KDE contributor, no? :)

To answer my first question: does it matter if the reporter is a developer or a user? In certain aspect it does, I couldn't fix the bug (at least not that easy) without David. It might be slightly easier or faster to get out information from a developer. But if the communication is good, it doesn't matter too much. 