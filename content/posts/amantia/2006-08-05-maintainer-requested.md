---
title:   "Maintainer requested"
date:    2006-08-05
authors:
  - amantia
slug:    maintainer-requested
---
Following the call to ask for maintainers for those applications that do not have one, here is a post requesting maintainer for two applications from the KDEWebDev module. 
The first one is <a href="http://kimagemapeditor.kdewebdev.org/">KImageMapEditor</a>, the HTML image map editor application that can be embedded in Quanta Plus or used as a standalone application. It has only a few <a href="https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&product=kimagemapeditor&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bugidtype=include&bug_id=&votes=&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Bug+Number&cmdtype=doit&newqueryname=">open bugs</a>, and some automatic porting was done for KDE4, but requires more work. It was basicly not maintained through 3.5.x cycle, aside of keeping it working.
The second is <a href="http://kfilereplace.kdewebdev.org/">KFileReplace</a> that has a wider target audience, and this fact shows up also in the number of <a href="https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=&long_desc_type=allwordssubstr&long_desc=&product=kfilereplace&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bugidtype=include&bug_id=&votes=&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Reuse+same+sort+as+last+time&cmdtype=doit&newqueryname=">open bugs</a>. It is not web developer specific application, but it found a home in our module, mostly because I took over his maintainership after it was abandoned on SourceForge and we found it to be useful. 
It suffered a rewrite during KDE 3.4 and 3.5 and got several enhancements (thanks Emiliano), but it needs a new maintainer now. The biggest job is to go away from using processEvents which causes nothing but trouble. 
Using threads or at least timers would be much better.
Both applications need an updated homepage as well.
Anyone interested in these apps should contact me or us at the <a href="https://mail.kde.org/mailman/listinfo/quanta-devel">Quanta developer list</a>.
<!--break-->