---
title:   "Eurovision 2006"
date:    2006-05-21
authors:
  - amantia
slug:    eurovision-2006
---
This is fun. A real rock band won the Eurovision contest in 2006. Finland sent Lordi, a melodic metal band in horror/sci-fi masks to Athens and they won with a big margin. I heard the first Lordi songs about 2 days ago when I listed to some music I got from a friend and I listen to them only once, but it was OK. Of course I couldn't remember anything from the songs, but the intro and the style (and the voice of Udo from Accept), but it wasn't bad. And I read in an older magazine that they participated in the Finnish Eurovision and it seems that they won there (today I read that some people protested against it even at the president of Finland). Finland is a nice place and they have some very good rock bands there, the best known might be Nightwish, HIM and maybe Stratovarius (but there are much-much more good bands from such a small country). But Eurovision is not known for its rock-friendliness, rather for trendy and light music. But I looked at it today, maybe just to see what are the current trends and to have some fun. Sincerely there were only a few that I could say they were OK, even if the music was not of my style (Germany, Lithuania for example). They were mostly rated down. ;-) Lithuania was the best, we laughed a lot on them with my wife. If you have a chance to see their performance from this shown, do it!
Lordi was nothing really special with this song, it was an OK song, but not a big hit IMHO. Today I listened again to their album (its a newer one) and the new album seems better than the winner song. But for whatever reason the countries constantly voted on them and they won with about 50 points and getting a record number of points. 
This story is bigger than the bird flu in Romania which is now about 20 km from my place.

Hell froze over! :-) 