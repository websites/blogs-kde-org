---
title:   "KDE 4.2 - progress in a year"
date:    2009-01-25
authors:
  - amantia
slug:    kde-42-progress-year
---
More than a year ago I wrote a <a href="http://blogs.kde.org/node/3062">post about KDE 4.0</a>, I was quite unsatisfied with how in was and that we are going to release a product that has defects and in the eyes of the users will be a step back. I actually switched to KDE4 as my main desktop sometime during the 4.1 developing cycle. Since then I use KDE trunk on one machine and whatever my distro (openSUSE) provides on another one. There is always a shock when I have to use the distro packages. They did a very good job on integration and in many cases the distro package looks more polished than my self compiled one, still I was always liked the trunk version better. The improvement between 4.1.x and 4.0.x and 4.x.x and 4.1.x is just so big, using the older version is like going back several years. Not talking when I use KDE 3.5 on some other machines. I miss KDE 4.2 a lot in that case. Was it good that we released 4.0 a year ago? I think it was bad from PR point of view, but probably needed to actually have a 4.2 like the one will appear soon in the wild.
 Yes, there are still issues, yes there are some applications that aren't ported or their port is not up to the expectations (yet). Luckily, unless your distribution did it wrong, it is possible to run the KDE3 applications under KDE4, without much hassle. 
 In the previous blog I complained about performance. My system is almost the same, except the video card is a newer one. And buying a new card at that time caused more trouble, and virtually no visible speedup at that time. Meantime the drivers improved (also due to KDE!), KDE improved (both kwin and plasma), and now I can use my system with effects enabled without thinking about performance. The current performance problems are actually caused by the flash plugin and its wrappers, in many case they start to use 100% CPU power. I'm not sure it can be fixed by us or the wrapper developers, what I know that both Konqueror and Firefox suffer from this problem. I just had to close down Firefox running in a KDE3 session because the X server for that session used completely one core.
 I'm happy now with KDE4 and trunk already has some improvements compared to 4.2 that I enjoy. :) I'm amazed by the progress of KDE, aren't you amazed as well?

PS: If you miss Quanta being ported and you know C++/Qt, you should help. The only way to make it a KDE4 application is to finish the port, it won't happen magically if noone works on it. :(
 