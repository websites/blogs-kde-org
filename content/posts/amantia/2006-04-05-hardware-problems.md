---
title:   "Hardware problems"
date:    2006-04-05
authors:
  - amantia
slug:    hardware-problems
---
I find todays hardware less and less reliable. Lately I feel it on my own skin. The story started with some mess I made on my hard disk and an upgrade of SUSE to 10.1 beta9. I reorganized my partitions a little, and after that my old win partition did not want to boot. Well, let's reinstall it, but the installer din not want to start from the CD. After a while it started. Whatever, I thought it's the win installer, as I never tried it since my upgrade to AMD64, so I forgot about it (I don't really use win, I just keep for the case that somebody brings me a software that doesn't run on Linux). 
 After I installed SUSE I had strange crashes, complete system freezes. As I also modified some settings in the BIOS, I still did not suspect anything, but that my BIOS changes made the system unstable or SUSE is still very beta. One of such freezes almost destroyed a reiserfs partition, I could only recover with --rebuild-tree, and I had to run it twice as for the first time it crashed. 
After that I tried to compile KDE. From time to time it produced some errors, but what it made me really suspect that this is not a software problem was frequent crashes of g++ when compiling KOffice. After a reboot even make did not start, either gave me a bus error or segmentation fault. At this stage I decided to test the memory. In less than a minute I found that it gave me 3000+ errors. 
 I'm a little disappointed with this. It's good that I know the cause, it's also good that the memory is in warranty, but it is sad to see such failures from a well known vendor. This was a 6 month old Corsair Value Select memory, a pair of 512MB modules. Some investigation revealed that one of the modules has problems. So now I am using only 512MB in single channel mode and waiting to confirm the replacement of the module(s). Hopefully the dealer will do it without problem, as it is a friend of mine.
 And now back to the first sentence: if you go through the blogs on planetkde.org or read some LUG lists, you can see what kind of hardware problems did others have. The most frequent would be hard disk failures. The second might be memory failure. But there are others as well, like badly designed laptops. What's happening in hardware business? Is it really good to try to reduce costs that much and keep releasing new products like crazy? It seems that in this rush the consumer is who will loose the most, but the vendors will do as well, when brands with good reputation start to release bad products (I heard that from ASUS from example, but this Corsair problem is also  bad. Plextor also released a problematic DVD-RW.) 
 In the past there were some products that should be avoided (I remember some VXPro-II chipset), but from my experience the problem was not that the hardware failed after some time: either it was a bad design and never worked correctly or it was OK and worked for years. Even in case of cheap, low-end brands. I had an Acorp motherboard with AMD K6 on it, plus some noname S3Savage graphics card and memory, Quantum hard disks, a Creative 16x CDROM from around 1999 (some parts, like the case is from 1995 or so) and it still works for my father! From time to time you need to clean the fans, the CD drive, but it works, and is stable. Same for other PCs I know around. In the library where I work part-time, there are 586 and Pentium I computers in use. 
 And my experience with recent hardware:
- an ASUS motherboard did not want to boot after a cold start without a BIOS upgrade
- an AOpen Socket A motherboard failed after about 2 years (boot problems, caused by failed capacitors). This was mine, I had to buy a new one.
- a power supply failure after some months for my sister
- memory failure for one of the computers in library (about 1.5 year old memory)
- monitor failure at the library (I don't know how old it was, but max. 6 years)
- memory failure for me
- my floppy drive NEVER worked correctly. Luckily I use very rarely, so I don't care. Actually I had a drive which became defect, it got replaced, but this is also unreliable.

Finally, I hope this decrease in quality will not happen for OSS software, especially KDE. 
