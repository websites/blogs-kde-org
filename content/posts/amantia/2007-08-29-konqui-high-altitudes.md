---
title:   "Konqui at high altitudes"
date:    2007-08-29
authors:
  - amantia
slug:    konqui-high-altitudes
---
I was missing from development for about 2 weeks in August, because I had a vacation. With 3 friends of mine, we planned a hiking/climbing journey in the Alps for this summer. I haven't been on a real vacation for a long time, only for small 2-3 days of resting or one day climbing in the mountain. The original plan was pretty ambitious: climb Europe's highest mountain (if we do not count the Caucasus Mountains at the border of Europe and Asia), the Mont Blanc and the second highest peak, Dufour Spitze between Switzerland and Italy for one team, and the Weishorn in Switzerland for the second team.
 Well, things usually don't follow your plans, and so it happened this time. Those who were at Glasgow probably noticed my problems with my knees. On one of the training trips it started to hurt badly, and at one point I barely could walk. It required serious treatment, both "electrical" and through medicine. The doctors warned me that this might require a surgery in the future, altough they said I can still climb, just more carefully. This problem also stopped me to continue the training, so I wasn't in the mountains for several weeks, nor did I do other serious exercises. Only resting and waiting to recover in the hope I can at still go at least on one of the mountains or part of it.
 To make the story short, none of the peaks was climbed by me, but this was only partly because of my knees. The weather wasn't too good this year, some climbers died on M. Blanc a few weeks before we went there due to a storm. We were lucky to find 2 days of relatively good weather, but this meant to hurry. Also we had only 3 days to spend on the mountain, because we had to move further to Switzerland to the second peak. Unfortunately I got sick when we arrived at 3200m, and even if I recovered until next morning, I got sick again at 3800m near the Gouter Hut. This is where I abandoned my try to reach the peak. We climb the Gouter face in bad weather conditions (wind, snow, it was a storm, which turned back all the teams who tried to reach the summit that morning), in the night. As the weather forecast for the next day was bad (and wrong...), I didn't spend a night there, but came back the same day to 3200m. Two of my friends did an attempt to climb the summit and succeed, but almost had to spend a night above 4000m in a shelter, because of the fog that came down. As they didn't have sleeping bags, risked and came down to the tent in the fog. Some climbers remained in the shelter and looked really bad the following day.
 The Dufour Spitze idea was abandoned from the start, instead with my friend we hiked around Zermatt in the hope to make good pictures about the Matterhorn (couldn't because of the weather), and on the day when the weather was forecasted to be excellent, we climbed a 4165m high peak, the Breithorn. So at the end I could still go to a 4000'er, even though on the one which is considered to be the easiest (and yes, it was easy, especially because we had the acclimatization from the previous days). The very same day the two other friends hiked the 4505m high Weisshorn, a hard and demanding mountain, with very narrow ridge where only one of your boots fit on...
 We did some more hiking on lower areas (below 3000m), and on the descent from Gornergrat my knees finally said it was enough. So on the following days, I did nothing but driving and walking around in villages or where we stopped with the car. The joy was only interrupted on the last day by the Swiss police, who for whatever reason did a complete verification of our luggage and my car at the police station. I hope they were disappointed when after almost 2 hours of searching, asking and investigating find nothing illegal and wrong. Unfortunately this wasn't my first (bad) experience with Swiss police or their border control officers. I'm 99% sure all of them was due to having the wrong country flag on my license plates.
 There were other annoyances as well (like the backdoor of my Nikon F80 broke and it is very hard to get a replacement, while digital Nikon SLR's are very expensive), but after all it was a good trip, and I could do more than I thought after my knee problems started.
 And what does it have to do with KDE? Well, I warned all of my KDE T-Shirts during this trip, on purpose. :) Unfortunately I always forgot to take picture with them on the actual summit (or highest part). But did below of them. 

1) At the Tete Rousse base camp in the Ireland t-shirt. Behind me is the Bionassay.

<img src="http://andras.kdewebdev.org/other/tete rousse.jpg"></img>

2) On the top of Breithorn. On the left the Mont Blanc is visible, on the right the south face of the Matterhorn.

<img src="http://andras.kdewebdev.org/other/breithorn.jpg"></img>

3) Konqui below the Breithorn.

<img src="http://andras.kdewebdev.org/other/konqui-breithorn.jpg"></img>

4) The Breithorn peak

<img src="http://andras.kdewebdev.org/other/breithorn2.jpg"></img>