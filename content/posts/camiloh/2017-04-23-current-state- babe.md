---
title:   "Current state of Babe"
date:    2017-04-23
authors:
  - camiloh
slug:    current-state- babe
---
<img src="https://cdn-images-1.medium.com/max/2000/1*1ozxULy3YolgAHvzUfQHVw.png">

A better view of this post can be found here: 
https://medium.com/@temisclopeolimac/current-state-of-babe-9fb56ce16ac6


To continue my last post about Babe [1] where I wrote about a little of its history, in this new entry (I’ve now switched from the KDE blogs to Medium) I will tell you about the current state of Babe and the features implemented so far.
[1] https://blogs.kde.org/2017/04/14/introducing-babe-history

So welcome to this walk through Babe:

To start: In the last post I wrote about wanting to make in the first place a tiny music player, and that’s why I first started the GTK3 version of Babe and tried to make it look like a plain playlist highlighting the cover artwork.

The now present version of Babe still sticks to that idea, but also, to make of it a more powerful music player, it has an integrated music collection manager, with different views and features.

One of the core points of Babe is the Babe-it action. Babe-it basically means make a track a favorite and put it in the main playlist. A Babed track is a new hot song you really dig for now, like a new song you found on YouTube and added to your collection via the Babe Chromium extension ;).

The Babe-it action can be found on the contextual menu to mark as babe any track on your collection and also in the playback toolbar controls to mark as Babe the current playing track. Also via the native KDE notification system you can Babe or Un-Babe the current playing song without having to interact with the babe interface.
Contextual actions on a track. Babe-it comes first ;)

<img src="https://cdn-images-1.medium.com/max/2000/1*g-jwCgGxSKA9LOFDREzFcA.png">
<img src=" ">

View Modes:

When you first launch Babe it will make use of the called “Playlist Mode”, but in the case scenario where there is not music to play, then it will change to the “Collection Mode” to let you add new music sources or drag and drop music tracks, albums or artists.

Babe also has a “Mini Mode” that just displays the artwork and the basic playback functions.

So Babe has 3 view modes:

Collection Mode where all the collection manager views are visible: all tracks, artists, albums, playlists, info, online and settings view.

At the left you found the views toolbar and at the bottom the so called “Utilsbar” where all the useful actions for each specific view are placed.

<img src="https://cdn-images-1.medium.com/max/1000/1*e2OjisuMgCxkfoj1LUaoAA.png">


Playlist Mode where only the current tracks on the playlist and the artwork on top are visible.

In this mode you also get a mini “Utilsbar” This bar has 4 main utilities:
1- the calibrator, which allows you to: clear the list and only put your Babes on play, to clear the whole list to put together a new playlist, to save the the current list as a playlist, to use a different playlist from the ones you already created and finally to remove the repeated songs from the list.
2- a go-back button which takes you back to the view you were before at the Collection Mode.
3- a filter option that will let you quickly put on play your search queries without having to go back to the Collection Mode. Search queries can go like this: “artist:aminé,artist:lana del rey,genre:indie” or simply “caroline”.
4- and finally an open dialog for you to select a file on your system.(you can also drag and drop those files instead)

<img src="https://cdn-images-1.medium.com/max/800/1*P0pN_Xedtc_a5QNEHqqKoA.png">


Mini Mode that only displays the artwork and the basic playback functions.

<img src="https://cdn-images-1.medium.com/max/800/1*6dZkrny1g4wYtl-f-dVfjw.png">

The Collection Mode Views:
You can browse your collection by all tracks, albums, artists or playlists.
Those views have a permanent set of actions at the bottom Utilsbar:
Add the tracks on current table to the main playlist
Save the tracks on the current table to a playlist

Also there is a playAll button that shows when hovering the artwork, when pressed it will clear the main playlist and started playing all the tracks from the artist or from the album, depending of which type of artwork was clicked.


Menu Actions:

From the contextual menu you can perform these actions on a track:
Babe-it or Unbabe-it
Put it on Queue
Send to phone by using the KDE Connect system
Get info from the track, such as music lyrics, artist bio, album info, related artists and tags
Edit the file metadata
Save track to a location
Remove track from the list
Add track to an existing playlist
Rate it from 1 to 5 stars
And finally set a color mood for the track. Tracks with a colored-mood can color the interface.

Babe Chromium Extension:

By making use of the Babe Chromium extension you can fetch your favorite YouTube music videos and add them to your local music collection. Babe will find relevant information to catalog them and the add them to the main playlist for you to listen later on.
Babe makes use of youtube-dl, so the supported sites by it are also supported on Babe, such as Vimeo… etc


The Info View:

The info view is going to be the main focus point on future development, the main idea for Babe is to become a contextual music collection manager, that meaning that Babe will let your discover new music based on the tracks you collect and listen to, and also show you contextual information about those.
Right now this is still work on progress but for now you can enjoy the following features:
While showing info about a track you can click on the generated tags to look fro those on your local collection

The Search Results
This is another focal point for babe, but I will cover this one in a future blog post.
Hope you like and I’m hoping to read about what you think and your ideas.