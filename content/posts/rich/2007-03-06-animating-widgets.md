---
title:   "Animating Widgets"
date:    2007-03-06
authors:
  - rich
slug:    animating-widgets
---
Qt 4 includes a useful class called QTimeLine that can form the basis of animations, it's used in QGraphicsView but is also more generally applicable. I did a bit of hacking this weekend and wrote a class that illustrates how it can be used to create a fairly general mechanism for animating QWidgets. The code as it stands needs some work, but it can animate the properties of any QWidgets. You basically say widget X has propery P which should vary from A to B. For example:

<pre>
    QLabel kde(ann);
    QPixmap logo( QString("logo.png") );
    kde.setPixmap( logo );

    QSize ksz = kde.sizeHint();
    QPoint kstart( ann->width()-ksz.width(), 0 );
    QPoint kend( 0, ann->height()-ksz.height() );

    ann->addAnimation( &amp;kde, "pos", kstart, kend );
</pre>

This will cause a QLabel containing the KDE logo to move from the top-right of the containing window to the bottom left. I think a tool like this should be useful for plasma.

You can see the demo here:

<img src="http://xmelegance.org/devel/widgetanimation.png">

Obviously a static image doesn't show you much. The source code is at <a href="http://xmelegance.org/devel/widgetanimation.tar.gz">http://xmelegance.org/devel/widgetanimation.tar.gz</a>.

<!--break--> 