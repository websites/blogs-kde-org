---
title:   "Qtscript Binding Generator"
date:    2008-03-10
authors:
  - rich
slug:    qtscript-binding-generator
---
In 4.0 plasma had support for writing applets using Qt's built in javascript interpreter QtScript, but the facilities have been fairly limited. In KDE 3.x KJSEmbed gave us reasonably complete bindings to the Qt and KDE api's, allowing us to write applications such as a web browser in 10 lines of javascript. I'm glad to say that some plans that were discussed at the KDE conference in Glasgow are now a reality and Kent has released a <a href="http://labs.trolltech.com/page/Projects/QtScript/Generator">qtscript binding generator</a> based on the one used for QtJambi. The result is that we will very soon have good access to the Qt API from qtscript.

I've been playing with the output of the generator for a couple of weeks now and while it still has a lot of rough edges, it's definitely a solid foundation. Even better from my point of view as a KDE developer, the Jambi generator was designed from the start to allow you to use it to write bindings for Qt based code that is not part of the main Qt API. Using it to build bindings to the plasma APIs and in future the kdelibs API is definitely feasible.

<!--break-->