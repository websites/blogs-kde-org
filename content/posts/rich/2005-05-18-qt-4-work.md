---
title:   "Qt 4 Work"
date:    2005-05-18
authors:
  - rich
slug:    qt-4-work
---
Some progress has been made on the Qt 4 front this week. I've implemented a simple QHttp based version of XMLHttpRequest that will ultimately be bound to KJSEmbed/Qt4. This will mean that the scripts will be able to access web based XML services such as news feeds, weather reports etc. I've now also got a working build environment to begin helping with the kdelibs porting effort.
<br>
<br>
A couple of hints for getting Qt 4 working with the KDE 4 branch:
<ul>
<li>After building Qt in debug mode you need to symlink all the XX_debug.so libs to XX.so for the kdelibs configure to find them.
<li>You need to use the version of unsermake that is in the KDE 4 branch (and also use qt-copy) for things to work properly.
</ul>

The end result is a nice kdelibs that doesn't build. :-)
