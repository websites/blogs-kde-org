---
title:   "KJSEmbed gains momentum"
date:    2004-06-21
authors:
  - rich
slug:    kjsembed-gains-momentum
---
A few minutes ago, I was thinking that kjsembed is picking up users less quickly than I would have expected (given how easy it is to use). In the short time between then and now I've found out about two people/projects using it that I wasn't aware of, which has cheered me up.
<br>
<br>
The first is the <a href="http://www.kalyxo.org/">Kalyxo</a> project which is aiming to improve KDE/Debian integration. It seems they're using it to write a gui for building Debian packages. Apparantly they're missing support for Process which isn't in the 3.2 release, making me very tempted to make an interim release of KJSEmbed available. The version in HEAD has much more power than the previous release, and as far as I'm aware has no particular 3.3 dependencies.
<br>
<br>
The second thing I found was a <a href="http://sandro.giessl.com/log/archives/2004/06/19/experimenting-with-kjsembed/">blog entry</a> on <a href="/www.planetkde.org">PlanetKDE</a> from Sandro Giessl where he talked about a script he's written in kjsembed that lets you get the pronunciation for words from the webster online dictionary. The script is pretty neat and makes good use of the features of KJSEmbed:
loading guis from .ui files, embedding KParts and support for KIO::NetAccess. The fact he found it pretty easy to use is a good sign too.
<br>
<br>
I wonder if we should have some sort of website to which people can upload useful scripts they've written, maybe a section of KDE-apps.org or something?
<br>
<br>
Anyway, it's nice to know that people are finding this code useful, and it encourages me to continue working on it. So, if you're doing something with kjsembed, even if it's just quick hacks etc. please let me know!

