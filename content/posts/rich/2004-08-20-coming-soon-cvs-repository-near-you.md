---
title:   "Coming soon to a CVS repository near you"
date:    2004-08-20
authors:
  - rich
slug:    coming-soon-cvs-repository-near-you
---
It's been a little while since I posted what I've been up to, so here's an update. I've been busy at work, so I've had less time for KDE than I would have liked, but even so I've made good progress on kasbar 3. One of the projects I've been working on in my day job will also be open source and may be of interest to some people.

I've done a bunch of work on the new kasbar codebase and I'll be merging it into HEAD this weekend. There's still a lot of work to do, but the scary part of refactoring the code is pretty much done. Things like support for the 'window needs attention' property are working nicely, and you can now ungroup things. The grouping of tasks on inactive desktops is also working nicely. There's still a lot to do here both in terms of grouping and better/more effecient eye candy but progress overall is good.

One of the projects I've been writing at work is a fancy new port scanner that I'm very pleased with - it is designed along the lines of scanrand by Dan Kaminsky and lets you perform port scans in a highly controlled manor. My scanner doesn't try to do quite as much as scanrand itself which has resulted in an efficient and stable implementation of the concept. The code is currently written in python, but I have also recoded most of it as C++. One or both implementations will be released as GPL once we are happy with them. The most unusual thing about this code is that it creates (and tears down) a complete TCP session rather than performing a SYN scan. I can't think of many situations where it is appropriate to use raw sockets then write half a TCP stack in python yourself!
