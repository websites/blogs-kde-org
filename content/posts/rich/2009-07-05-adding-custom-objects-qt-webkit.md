---
title:   "Adding custom objects to Qt Webkit"
date:    2009-07-05
authors:
  - rich
slug:    adding-custom-objects-qt-webkit
---
One question  I've seen come up several times on #qt and qt-interest is how to
add custom (application specific) APIs to those available by default in
QtWebKit. This is actually pretty easy (once you know how) as I'll show
below. This post will show a simple example of how to make an object available
from javascript, including calling methods on the object and returning values
from C++.
<p>
There are two things you really need to know in order to perform this
integration, the first is the addToJavaScriptWindowObject() method of
QWebFrame, this allows will make the specified QObject visible from
javascript. The second thing you need to know is that objects published in
this way will vanish each time the javascript window object is cleared -
ie. every time the user navigates to a new page. To prevent this from causing
problems, QWebFrame provides a signal that tells you whenever the object is
cleared allowing you to re-add your custom API. This is actually much simpler
than it sounds!
<p>
The core of this is really implemented in two methods in the example, they're
shown below:

<pre>
void MyApi::setWebView( QWebView *view )
{
    QWebPage *page = view->page();
    frame = page-&gt;mainFrame();

    attachObject();
    connect( frame, SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(attachObject()) );
}

void MyApi::attachObject()
{
    frame-&gt;addToJavaScriptWindowObject( QString("MyApi"), this );
}
</pre>

This code is all that you need in order to make all of the public slots of the
MyApi object visible to javascript. The MyApi class provides two public slots:

<pre>
public slots:
    void doSomething( const QString &param );
    int doSums( int a, int b );
</pre>

The first slot simply logs a message to the debug output, the second returns
the sum of its two arguments (yes, slots can return things!). They're called
from javascript like this:

<pre>
  MyApi.doSomething( 'Hello from JS page 2!!!!' );

  sum = MyApi.doSums( 2, 3 );
  alert( 'C++ says the sum is ' + sum );
</pre>

And that's all there is to it! You can download the code from <a
href="http://xmelegance.org/devel/qtwebkitextension.tar.gz">http://xmelegance.org/devel/qtwebkitextension.tar.gz</a>.
<!--break-->
