---
title:   "Getting the details of an SSL connection"
date:    2011-01-09
authors:
  - rich
slug:    getting-details-ssl-connection
---
SSL is a pretty complex topic, and whilst Qt makes the APIs pretty easy to use, there's a lot going on underneath and it can be hard to debug. In order to make life a bit easier, I've written a simple tool that will connect to an SSL service then dump every piece of information Qt makes available concerning it. Of course the aim here is both that the tool be useful in itself, and also that it provide a good illustration of how to use the APIs concerned.
<p>
The first step, naturally enough, is to connect to the service using SSL - as you might expect, this uses the QSslSocket class. In order to be informed if we manage to connect successfully, or if we hit a problem we connect to some useful signals/
<ul>
<li>encrypted() - This is emitted if we manage to successfuly connect to the service.</li>
<li>sslErrors(const QList&lt;QSslError> &amp;) - This is emitted if there are problems establishing a secure connection.</li>
<li>error(QAbstractSocket::SocketError) - This is emitted if the connection fails. It is declared in QAbstractSocket. Note that ssl errors can get reported here too.</li>
</ul>
The connectToHost() method is the one that triggers the actual connection, and by default performs the SSL handshake etc. for us:
<pre>
void Connector::connectToHost()
{
    qDebug() &lt;&lt; "Connecting...";

    d->sock = new QSslSocket(this);
    connect( d->sock, SIGNAL(encrypted()), this, SLOT(ready()) );
    connect( d->sock, SIGNAL(error(QAbstractSocket::SocketError)),
             this, SLOT(socketError(QAbstractSocket::SocketError)) );
    connect( d->sock, SIGNAL(sslErrors(const QList&lt;QSslError> &amp;)),
             this, SLOT(sslError(const QList&lt;QSslError> &amp;)) );
    d->sock->connectToHostEncrypted( d->host, d->port );
}
</pre>
If we get an error during the connection, then one of the two error handling slots we've connected gets called. If it's a socket error then we have a problem such as a refused connection we can't do much, however if it's an error caused by one of the SSL checks then we handle it differently. Normally, we should act as Qt does by default and treat SSL errors as hard errors - if we don't then SSL is unable to protect us against attacks. For the purposes of this example however, we want to be more lenient since we're specifically writing a way to figure out what's going on. As a result, our SSL error handling looks like this:
<pre>
void Connector::sslError( const QList&lt;QSslError> &errors )
{
    foreach( const QSslError &error, errors ) {
        qDebug() &lt;&lt; "SSL Error: " &lt;&lt; error.errorString();
    }

    // This is only used because we are interested in dumping all the info
    // do NOT use if you want real security (it introduces man-in-the-middle
    // attacks).
    d->sock->ignoreSslErrors();
</pre>
If we manage to establish the connection ok, then we'll call a couple of functions that tell us the details. The first gives us the SSL certificate used by the remote server we've connected to, the second tells us the cipher we're using. We'll pass these to a couple of functions that print out the details, then terminate the application:
<pre>
void Connector::ready()
{
    qDebug() &lt;&lt; " === Peer Certificate ===";

    QSslCertificate cert = d->sock->peerCertificate();
    dumpCertificate( cert );

    QSslCipher cipher = d->sock->sessionCipher();
    dumpCipher( cipher );

    qDebug() &lt;&lt; "Done";

    qApp->quit();
}
</pre>
First, we'll look at how we dump the information about the cipher we're using. QSslCipher provides accessors that let us see the details - it's worth remembering that users will not be able to understand this information, so any interface for general use should be designed with rather more care.
<pre>
void Connector::dumpCipher( const QSslCipher &amp;cipher )
{
    qDebug() &lt;&lt; "\n== Cipher ==";

    qDebug() &lt;&lt; "Authentication:\t\t" &lt;&lt; cipher.authenticationMethod();
    qDebug() &lt;&lt; "Encryption:\t\t" &lt;&lt; cipher.encryptionMethod();
    qDebug() &lt;&lt; "Key Exchange:\t\t" &lt;&lt; cipher.keyExchangeMethod();
    qDebug() &lt;&lt; "Cipher Name:\t\t" &lt;&lt; cipher.name();
    qDebug() &lt;&lt; "Protocol:\t\t" &lt;&lt;  cipher.protocolString();
    qDebug() &lt;&lt; "Supported Bits:\t\t" &lt;&lt; cipher.supportedBits();
    qDebug() &lt;&lt; "Used Bits:\t\t" &lt;&lt; cipher.usedBits();
}
</pre>
Now let's take a look at how we dump the certificate information. A certificate contains a lot of information (not all of which is accessible using Qt's API). In order to make the output managable, I've grouped it into three sections - information about the subject, information about the issuer, and finally information about the certificate itself.
<p>
The first thing we do is dump the certificate itself in a format known as PEM which a base64 string. Then we move onto the subject. We have quite a bit of information about the subject of a certificate, but two parts are particularly important for SSL - the common name and the subject alternative names. These two fields are the ones that are matched against the name of the host we're connecting to in order to determine if the certificate is valid for this host. After the information about the subject, we dump similar information about the issuer. Finally we display the information about the certificate itself which here is primarily just the dates for which it is valid.
<pre>	
void Connector::dumpCertificate( const QSslCertificate &amp;cert )
{
    qDebug() &lt;&lt; cert.toPem();

    qDebug() &lt;&lt; "== Subject Info ==\b";
    qDebug() &lt;&lt; "CommonName:\t\t" &lt;&lt; cert.subjectInfo( QSslCertificate::CommonName );
    qDebug() &lt;&lt; "Organization:\t\t" &lt;&lt; cert.subjectInfo( QSslCertificate::Organization );
    qDebug() &lt;&lt; "LocalityName:\t\t" &lt;&lt; cert.subjectInfo( QSslCertificate::LocalityName );
    qDebug() &lt;&lt; "OrganizationalUnitName:\t" &lt;&lt; cert.subjectInfo( QSslCertificate::OrganizationalUnitName );
    qDebug() &lt;&lt; "StateOrProvinceName:\t" &lt;&lt; cert.subjectInfo( QSslCertificate::StateOrProvinceName );

    QMultiMap&lt;QSsl::AlternateNameEntryType, QString> altNames = cert.alternateSubjectNames();
    if ( !altNames.isEmpty() ) {
        qDebug() &lt;&lt; "Alternate Subject Names (DNS):";
        foreach (const QString &altName, altNames.values(QSsl::DnsEntry)) {
            qDebug() &lt;&lt; altName;
        }

        qDebug() &lt;&lt; "Alternate Subject Names (Email):";
        foreach (const QString &altName, altNames.values(QSsl::EmailEntry)) {
            qDebug() &lt;&lt; altName;
        }
    }

    qDebug() &lt;&lt; "\n== Issuer Info ==";
    qDebug() &lt;&lt; "CommonName:\t\t" &lt;&lt; cert.issuerInfo( QSslCertificate::CommonName );
    qDebug() &lt;&lt; "Organization:\t\t" &lt;&lt; cert.issuerInfo( QSslCertificate::Organization );
    qDebug() &lt;&lt; "LocalityName:\t\t" &lt;&lt; cert.issuerInfo( QSslCertificate::LocalityName );
    qDebug() &lt;&lt; "OrganizationalUnitName:\t" &lt;&lt; cert.issuerInfo( QSslCertificate::OrganizationalUnitName );
    qDebug() &lt;&lt; "StateOrProvinceName:\t" &lt;&lt; cert.issuerInfo( QSslCertificate::StateOrProvinceName );

    qDebug() &lt;&lt; "\n== Certificate ==";
    //qDebug() &lt;&lt; "Serial Number:\t\t" &lt;&lt; cert.serialNumber(); // This seems buggy
    qDebug() &lt;&lt; "Effective Date:\t\t" &lt;&lt; cert.effectiveDate().toString();
    qDebug() &lt;&lt; "Expiry Date:\t\t" &lt;&lt; cert.expiryDate().toString();
    qDebug() &lt;&lt; "Valid:\t\t\t" &lt;&lt; (cert.isValid() ? "Yes" : "No");
}
</pre>
The end result of all this is a small command line tool that will connect to the host specified and dump all this information. It should be useful for debugging any issues that may be encountered when connecting to an SSL site using Qt. There are a number of possibile enhancements that could be made (eg. dumping the whole certificate chain) but even now, the output of this tool should make tracking down SSL issues in Qt applications a lot simpler. The code, as ever, is in my <a href="http://gitorious.org/qt-examples/qt-examples/trees/master/sslinfo">qt-examples</a> gitorious repository. An example of the output generated is <a href="http://xmelegance.org/devel/sslinfo.txt">here</a>.
<!--break-->
