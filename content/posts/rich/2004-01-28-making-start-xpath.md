---
title:   "Making a start on XPath"
date:    2004-01-28
authors:
  - rich
slug:    making-start-xpath
---
I've started taking another look at how to best add support for XPath to KHTML. I've got a bunch of code I'm busy reexamining that lets you define an AST for XPath. It needs cleaning up but I think it is a decent first cut.
<p>
XPath is the first step towards supporting standards like XSLT but is damn near useless on its own, so I don't see that this is big news. That said, I haven't posted in a while...

