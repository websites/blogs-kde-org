---
title:   "A trip down memory lane"
date:    2012-11-17
authors:
  - rich
slug:    trip-down-memory-lane
---
 I was digging through some old backups on Friday looking to see if I had any old versions of the Qt source code lying around after Eirik mentioned during his devdays talk that the release tar balls for lots of the early releases including Qt 1.0 had got lost... I didn't find those, but I found some gems I didn't know I had.
<p>
First, there's a presentation on the <a
href="http://xmelegance.org/KDE-internals/">KDE internals</a> I don't know where I gave, I suspect it was at MANLUG (the Manchester Linux User Group).
<p>
Next, there's a presentation <a href="http://xmelegance.org/simplinux-1999/The%20design%20of%20the%20K%20Desktop%20Environment/">The design of the K Desktop Environment</a> I gave at Simplinux in Faro, Portugal back in 1999. This one has a bit more depth than the first It's from back before we released KDE 2.0. This one talks about us using CORBA. The biggest thing in retrospect that happened at this conference is I met Nuno Pinheiro KDE's artist in chief. The colour scheme of the slides might make your eyes hurt (that's why Nuno's so important), I recommend the text version myself!
<p>
Finally there's a presentation <a href="http://xmelegance.org/KDE2-Technologies.pdf">The Technology Behind KDE 2.0</a> that was given after the release of KDE 2.0 but before the release of KDE 2.1. I've no idea where I gave this one. It mentions the removal of CORBA, and brings in important tools like KSycoca that we still use today. It also talks about <strike>DBUS</strike>DCOP which was the conceptual parent of the DBUS API that is used all over linux today.
<p>
A nice little trip down memory lane I hope you'll agree.
<!--break-->

Edit: Changed to a pdf link for the last one since google docs required a sign in.
