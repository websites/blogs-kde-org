---
title:   "Preparing for Qt4"
date:    2005-03-26
authors:
  - rich
slug:    preparing-qt4
---
I've started looking into porting KJSEmbed to Qt 4. To begin with, I've recreated most of the Q_CLASSINFO demo I posted recently using the Qt 4 equivalents. Listing the slots of an object is even easier than before, as is finding a marker interface:
<pre>
    const QMetaObject *moa = metaObject();

    // Find the marker interface
    int index = moa->indexOfClassInfo( "MyMetaInfo" );
    QMetaClassInfo ci = moa->classInfo( index );
    Q3CString cs = ci.value();

    QLabel *l = new QLabel( cs, this, "hello label" );
    l->adjustSize();

    // List an object's slots
    const QMetaObject *mo = l->metaObject();

    // The loop lets us walk up the inheritance tree
    do {
	printf( "=================\n" );

	for ( int i = 0 ; i < mo->memberCount(); i++ ) {
	    QMetaMember mm = mo->member(i);
	    if ( mm.memberType() != QMetaMember::Slot )
		continue;
	    
	    printf( "%s\n", mm.signature() );
	    
	}

	mo = mo->superClass();
    } while( mo );
</pre>
It shouldn't be too hard to move on to calling slots from here.
<hr>
EDIT: Well, it turned out to be pretty easy:
<pre>
    mo = l->metaObject();
    bool ok = mo->invokeMember( l, "setText",
				Q_ARG(QString, QString("I was called") ) );
    printf( "Call result: %d\n", ok );
</pre>
This sets the text of the label to 'I was called'.

