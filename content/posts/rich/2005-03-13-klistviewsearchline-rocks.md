---
title:   "KListViewSearchLine Rocks!"
date:    2005-03-13
authors:
  - rich
slug:    klistviewsearchline-rocks
---
KListViewSearchLine is a great class - I just added search capability to to kdcop with 2 lines of code and a little bit of work in designer!

First filter<br>

<img src="http://trinity.fluff.org/~rich/devel/kdcop.png"><br><br>
<!--break-->
Second Filter<br>

<img src="http://trinity.fluff.org/~rich/devel/kdcop1.png">