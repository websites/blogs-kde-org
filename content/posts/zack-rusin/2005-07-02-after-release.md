---
title:   "After the release"
date:    2005-07-02
authors:
  - zack rusin
slug:    after-release
---
I miss Snapple ice-tea. For some reason they don't seem to have it in Norway. They've got Snapple lemonade which I don't like at all. Right brand, wrong product. And the whole approach to cooking seems to be different here. For example last week we were making pancakes. We got a wrong flour at first (it was too fine). So I and Simon went to the store to find better one. Me being me, I grabbed some girl, dragged her to the right aisle and asked her which flour is for making pancakes. We took the flour she pointed out but noticed that there wasn't a difference between the one we had at home and the one she told us to get. Since the girl I just asked was a little chubby (which is why I asked her in the first place, I figured she knows a lot about pancakes) I decided to ask a skinny girl. She said we should get the same flour the other girl did. So we decided that we must be wrong and bought what they advised. It, of course, turned out to be the wrong flour. I was crushed.

Yesterday at Trolltech we had the first "Creative Friday". It's a concept, which was introduced after the Qt 4 release, where every engineer at Trolltech takes Friday to do something creative and innovative. Everyone can work on whatever they please as long as it is (surprise, surprise) creative. It's a great idea and will make Friday's even more fun around here. 

The Exa is in the X.Org CVS and drivers are being actively ported to it which is simply fantastic. There's still a few bugs left and I'd like to do two more changes. As reported by <a href="http://lists.freedesktop.org/archives/xorg/2005-July/008456.html">Thomas Winischhofer</a>, who's doing an amazing job reporting problems and porting the SiS driver, Exa performs as promised :)

Today is my birthday so I'm uncertain how long I'll feel like sitting in front of the computer and be fixing Exa bugs. Besides I kinda wanted to get a new skateboard as a birthday gift for myself today :)

<!--break-->