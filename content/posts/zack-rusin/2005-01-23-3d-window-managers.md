---
title:   "3D Window Managers"
date:    2005-01-23
authors:
  - zack rusin
slug:    3d-window-managers
---
So I got bored late in December and decided to just write something that would be fun. So I started hacking on what became Impresario.

Impresario started as a very simple window manager. Based on Qt4 it was supposed to be a very lightweight window manager that I could use while porting other parts of KDE to Qt4. 

Once I started working on it I very quickly got focused on eyecandy. Especially the window effects. I became obsessed with having the window minimization, closing, maximization and initial showing up animated... somehow. There's a finite number of 2d transformations that you can do on a pixmap which forced me to introduce a third dimension.

Impresario started rendering to a backing pixmap, which was being converted to a opengl texture and then smacked onto a polygon. So far so good, right? Wrong. Lets look at some of the fundamental problems 3D window managers would have to solve to be usable right now:

1) Triple buffering - most modern toolkits or even just apps do
   double-buffering by default. The way 3D window managers have to
   work is a) render the contents of a window to a buffer which can be
   transformed into a opengl texture, b) that opengl texture needs to be
   displayed. The "a" step introduces useless buffering. Ouch.

2) OpenGL apps. Running OpenGL app in an OpenGL app. Ouch, tricky.

3) Loss of hardware acceleration for OpenGL apps. Imagine that you
   found a way to enable another OpenGL app to run when your window
   manager is running. It runs the same way any other app would. Your
   WM gets the apps Window and renders it to a pixmap and displays it
   thus effectively avoiding hardware acceleration for the app. Ouch.

4) Performance loss for dynamic apps. Movie players, non-opengl games,
   etc things which constantly update. With "1" in effect you do not
   want to see how this thing performs.

5) Introduction of a new API. If you have a 3D wm you want to have
   those funky 3D apps on your desktop, right? Well, we need to make
   sure our wm manages them in a completely different manner than the
   rest. Plus we're stuck in "2" trying to merge two OpenGL scenes. We
   need a new API to specify those things. 

Once those would be solved we need to figure out how to make them usable. My favorite example is in the Looking Glass presentation at http://www.freedesktop.org/software/XDevConf/LG-Xdevconf.pdf . Look at the three last slides, the part that says "How would this look on today's 2D". The answer is "a lot more usable". My suggestion is: shear it and then rotate by about 127 degrees. It's already unusable, so make it cooler looking ;)

Not to say that any of them would be impossible to solve. In fact LG guys are probably doing a great job at it but personally I'm definitely not going to pursue this route. Impresario is back to being a simple wm. 

Getting Xserver to work on OpenGL is a lot better alternative. For 3D WM's at least, since for 2D apps it will have very little effect. At least now, when most of the apps you see do client side rendering, just pushing images over to the server. So it is going to take a while to fix a lot of the issues that we're having because it's not just the server that would have to be worked on. The cool thing about the next generation of toolkits is that they will have different rendering backends. For GTK+ it will be handled by Cairo, while Qt4 does it in Arthur. 
An interesting experiment might be comparing performance of Qt4's Arthur + OpenGl to Cairo + Glitz. 
<!--break-->