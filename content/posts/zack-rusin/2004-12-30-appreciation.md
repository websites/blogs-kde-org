---
title:   "Appreciation"
date:    2004-12-30
authors:
  - zack rusin
slug:    appreciation
---
I've been looking at some of the bug reports lately and I noticed something weird. People are getting outright hostile when their wishlist items are closed as WONTFIX. I mean, that was always the case, but it's been happening a lot more frequently lately. It's really bothering me that people like Coolo, Aaron, Lubos and many others are getting a lot of heat for closing stupid reports. Maintaining applications like KWin, Kicker or KDesktop is really an ungreatful activity. So if you see a guy who wanted to have a girlfriend simulator in KDE, or something equally silly, insulting one of the developers (who are usually your very good friends) you just want to smack him (the bad kind of smacking - the kind that's not ended by "who's your daddy?" screams)

On a sidenote, I took some pictures yesterday night and I put them up: http://bddf.ca/~zrusin/ . And yes this is <a href="http://aseigo.blogspot.com">Aaron's</a> server. I keep threatening him that once we finish the current project at work I'll take a week off and will visit him in Calgary. Apparently there's a new vegan restaurant near his place and that seems like a good enough reason to travel to the other side of the continent to hang out with him.
<!--break-->