---
title:   "KSpell"
date:    2003-07-20
authors:
  - zack rusin
slug:    kspell
---
Geiseri definitely made my day with this site, so I wanted to thank him by fixing kspell. I looked over the logs to see which commits broke it. It was broken twice in revision 1.101 (coolo commit) and then in 1.104 in a commit of Don's patch by Laurent. I fixed it but the code I saw there did not make me happy at all. If Laurent won't want to maintain it, I'll start doing it since it simply begs for a maintainer.
In other news I'm still playing with <a href="http://jackit.sf.net">JACK</a>. In general I like it a lot and I do think that with GStreamer it's a decent alternative to aRts. Hopefully tomorrow I'll commit the JACK aRts output plugin. I had to write a pure std ring buffer implementation for it, which seems to be working rather nicely (aRts doesn't use Qt in case you didn't know and the current HEAD depends on GLib which makes me wonder why are we hosting it in our CVS). Anyway, if you have a PowerBook remember thart aRts won't work if you'll try to run it in full-duplex mode - disable it from KControl and restart KDE to hear some sound.