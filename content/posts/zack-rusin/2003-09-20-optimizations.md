---
title:   "Optimizations"
date:    2003-09-20
authors:
  - zack rusin
slug:    optimizations
---
I haven't updated my blog in quite a while. The car accident, moving to a new place and a lot of work didn't leave with me a lot of spare time. 
People have been complaining about the speed of kabc (or rather the lack of it). I wrote a few simple tests. Run them a few times and started looking at the profile data. I was quite happy with what I found. There are still things I see as bottlenecks but we're getting there and I'm quite confident we can make kabc very fast. 
Aaron asked me to profile libkcal. It's going slowly, because I'm lacking good tests. In kabc, I knew where most of the slow paths at least may be, in libkcal I have no idea. We'll see how it goes. 
Martijn was also telling me that something has to be done about Kopete's memory footprint.
Today I also grew tired of the way we render large jpeg files. Which pretty much means that sometime this week I'll allocate enough time to fix bug 39693 (and a lot of other bugs that are related to the slow/memory consuming jpeg loading). 
If there's one thing I'm very unhappy about is that I didn't switch KNode's article viewer to khtml before the feature freeze. It still uses KTextBrowser which is simply bad for KNode needs. I also have to fix the in-place suggestions in KMail and KNode composers. I promised coolo to do it like two weeks ago. 
I've been toying around with the idea of adding Makefile.am checker to kde-emacs. Most of the errors seems to repeat themselves all over the place, which is already a good reason to automatize it. You'd M-x kde-emacs-check-makefile and it'd make sure your makefile follows our standards. 