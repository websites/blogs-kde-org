---
title:   "KNode in Kontact"
date:    2003-08-04
authors:
  - zack rusin
slug:    knode-kontact
---
Sunday, I woke up, there was nothing on tv, I was bored. I sat down and did something that had to be done and no one was really ready to do. <a href="http://www.automatix.de/~zack/knodekontact1.png">KNode in Kontact</a>. KNode definitely wasn't ready for that kind of use. It took me most of the day. About ~5000 lines of code :( A rather huge patch, which hopefully someone will feel like double checking before I'll commit it. I also went to see Tomb Raider 2 with my brother and a friend of mine and got the ".NET Framework Security" book. 
In any way, hopefully someone out there will enjoy using KNode from Kontact. Not that the last two relate in any way to KNode or Kontact, but I felt like adding it ;)