---
title:   "Just having fun"
date:    2005-05-29
authors:
  - zack rusin
slug:    just-having-fun
---
Being a non-meat-eater in Norway is to say the least interesting. It's been a while since I've heard people say things like "oh, you don't eat meat? we got chicken", or "oh, don't worry, we got fish". 
I like moving. Change is always fun. I'm slowly learning Norwegian. I was really disappointed though because I missed the Salvador Dali exhibition that was in Philly. If you live somewhere near Philly and have seen it, let me know how was it. 

Last weekend I was a little bored so I decided to play a bit with image manipulation using fragment shaders. http://ktown.kde.org/~zrusin/qglimage.tar.bz2 is the effect of that. It's an OpenGL image viewer (requires Qt4 and NVIDIA's Cg toolkit available at http://developer.nvidia.com/object/linux_cg_toolkit.html with some more info at http://developer.nvidia.com/object/cg_toolkit.html). What does it do? Besides being incredibly fast, of course. It allows you switching fragment shaders dynamically on run-time and seeing the effects of it on the image that was loaded. It's a really neat feature, that I'll definitely utilize in the next generation of the KDE image effects. If you have any cool fragment shaders that can be used in an image manipulation library let me know.

Besides that I've been working on replacing the accelleration architecture in Xorg this week. "Switching the architectures" is in itself a trivial task. It's the "driver switching" part that makes people go crazy (fortunately for me that's a noop either way). The goal of it all is to allow everyone to finally run composition managers. 

Implementing algorithms for drawing pixel precise, aliased primitives is a lot of fun. I and Gunnar were banging our heads against those things for two days this week as we wanted to make the rasterization in Qt4 simply perfect. I think the results are very impressive. A running joke this week was that Qt4 will only support aliased lines if they're from 10,10 to 20,20 - either go with times and use antialiased primitives or implement everything in your app using drawPoint calls ;)

<!--break-->