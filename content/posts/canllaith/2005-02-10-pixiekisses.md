---
title:   "pixiekisses"
date:    2005-02-10
authors:
  - canllaith
slug:    pixiekisses
---
This week I'm rewriting the kicker handbook and I'm wondering how one jazzes up documentation about the absolute basics. How do you not send your audience to sleep explaining the concept of a taskbar? It's fairly obvious but it has to be documented somewhere. We can't expect the mac users to pick things up by themselves. They might start to make a habit of it, and anarchy would ensue. Who would we torment?

I absolutely love the new look for KDE documentation. It's really slick and professional, and even my shoddy writing looks good when wrapped in such gorgeousness. Something I'd really love to see is some more people joining the hard-working and really talented docs team. A lot of people seem to think it has to be a big involvement, contributing to KDE. Documentation is a great place to start as itdoesn't have to be time consuming. A paragraph here, an FAQ entry
there. You can write in plain text and we're happy to add the markup.

(end thinly-veiled-plug)

Then again, today I saw a pretty good argument for some developers not being allowed to write their own documentation.

aseigo: "The taskbar is... wait. if you can't figure it out you shouldn't be using a computer. Fuck off and and go back to paper you idiot."

Now I just need to figure out how to work that into "Chapter 1: Using Kicker."

*** Edit: Note to self - disable hard line wrapping in emacs before typing up blog entry in it that will then get further mangled by being pasted into silly little html form. Sheesh ***