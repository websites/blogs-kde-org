---
title:   "The Ministry of Truth"
date:    2005-03-01
authors:
  - canllaith
slug:    ministry-truth
---
I'm currently doing some research focused on marketing and promotion for a KDE related project, and I'd really love it if people reading this could help me out. I'm looking for the opinions of primarily windows and apple users who have switched to using KDE on UNIX-like platforms. There are two main points I'd like to collect data about:

1.) Before you switched, what was your biggest concern? 

Examples: Will FreeBSD support all my hardware? Will FreeBSD let me open the MS Word documents I get for work or school?

2.) After you switched, what was the most positive surprise you had using an alternative operating system?

Examples: I didn't realise Linux had such a large range of software available for it!
I had no idea that spyware and viri weren't a problem on Linux.


If anyone has a moment to answer these questions I'd really love it! Post comments, email me at jes.hall AT kdemail.net or give me a yell on IRC. The Ministry of Truth needs YOU!

** Edit - I could have -sworn- I selected personal and not DCOP... *blush*

