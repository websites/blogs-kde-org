---
title:   "Contiguousness"
date:    2005-04-11
authors:
  - canllaith
slug:    contiguousness
---
It's a small, small world.

While staying in Wellington I thought I'd take the opportunity check out the local LUG. It seems to be a collection of some pretty nice people of varying geekiness, most of whom I'm interested in getting to know better. A nice man who has the same model laptop as I do. We exchanged some pointers on how we'd gotten various things working. A younger guy, burning with New Gentoo Convert Zeal. Seems very bright and funny in a goofy way. There was one other girl there - yay! 

We naturally gravitated to each other. She's here on an internship from Germany and runs Fedora on her laptop. We chat about why we're interested in linux and discover we have crossed paths online. She was at aKademy, and on returning Aaron showed me photographs of her. No wonder she looked familiar! This happened at the previous LUG, and at the one tonight we've made a date to do various geeky things on the weekend.

It's strangely cool to hear her talk about people she's met in RL that I've spent hours most days talking to on IRC. New Zealand is neither of our homelands, yet somehow we've managed to stumble across each other in a small obscure LUG here. 

Oh, and she has great taste. She stole Aaron's beer and thinks Rainer is very cute. 

;)