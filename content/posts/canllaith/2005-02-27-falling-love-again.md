---
title:   "Falling in love again...."
date:    2005-02-27
authors:
  - canllaith
slug:    falling-love-again
---
I've been fortunate enough to get my hands on a replacement laptop thanks to someone who has now secured my life-long adoration. My venerable dell latitude csx that has served me for much of the last year has been acting up lately eating up batteries and having the charger malfunction. Not very good and rather frustrating. 

<img src="http://bryson.co.nz/files/l400/l400.jpg">

Enter the pictsie! Latitude L400. 1.5kg, 12 inch screen and just generally gorgeous all over. It's a pentium III 700MHz with 256MB ram. Certainly perfectly adequate for websufing, email and technical writing. So I'm writing yet another linux-on-laptops page for this little cutey. I'm sure Andrew (my partner) is sick of hearing me rave about it to him over IM by now, and I'm not even home with it yet ;)

Hopefully now I'll have a bit more time to hack if I'm spending a bit less time fighting with my computer. Unfortunately, I still get to fight with KDE cvs head. Yay :(

http://bryson.co.nz/files/l400/l400.html

In progress linux-laptop page on my new laptop. It's ok, you can just pretend to read it and nod and smile convincingly while I gush.

;)