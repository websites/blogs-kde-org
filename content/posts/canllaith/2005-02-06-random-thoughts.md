---
title:   "random thoughts"
date:    2005-02-06
authors:
  - canllaith
slug:    random-thoughts
---
*aseigo* damn, you need to start a KDE blog
*aseigo* theobroma has</peer-presure>
*canllaith* I need to start a KDE blog?
*aseigo* damn straight =P
*canllaith* Ok fine. Where do I sign up.

Random trivia: 808 is the area code for Hawaii. I think it's clear who we can blame that now people might possibly stumble across my boring blog :)

Well, what do I say? I've been thinking for a while about starting a blog to shamelessly plug some of the things going on in KDE documentation. There are so many things that I consider are pretty exciting going on, with only a small handful of people working on them. 

I'm a pretty recent contributer and so far I've just been overwhelmed with the patience and friendliness of the people in the KDE project. The docs team have been fantastic with teaching me the ropes of CVS and answering questions, no matter how inane.
I've been making baby-steps here and there on really minor things on kicker, and once again have felt really valued and appreciated, even if teaching me to do something does take twice as long as simply having a more skilled person do it themselves.

This causes a positive feedback loop, since it makes newbie contributers want to help out the people who help them. New elements are fed into the system and the overall energy is increased, augmented, transcended.

I love the community feeling of KDE :) 



