---
title:   "A collection of randomness for your reading pleasure"
date:    2006-02-08
authors:
  - seele
slug:    collection-randomness-your-reading-pleasure
---
Friday is my last day at .gov where I will be moving on the bigger and better things.  For the past 14 months Ive been through three natural disasters (Indian Ocean tsunami, Katrina/Rita hurricanes, Pakistani/Indian earthquake), watched a few funerals on TV (Pope John Paul II, Rosa Parks, Coretta Scott King) and scandal galore (too many to list).  

I can't say I'll miss the job very much, but I will miss my coworkers.  They were really the ones who made the job interesting, and while being a consultant I wont get that same 'Office' atmosphere.

<hr />

There needs to be a better solution for syndicating my blog.  I have two active blogs, this one and <a href="http://weblog.obso1337.org/">my personal one</a>.  The difficulty with this is that I often cross blog because I have the same interesting things to say to both audiences.  The KDE developers blog is the account which gets syndicated to Planet and LinuxChix, but it misses a lot of the random usability and interaction design rants I write.  Although Planet reaches a lot of people, not everyone reads/syndicates it so I would be missing people who read only KDEDevelopers.  But I've also had my personal blog for many years.  What a dilemma, because one will have to go.

<hr />

I will be doing some interesting work in the upcoming weeks in which I should be able to donate some of the materials to the KDE-EDU project (I still have to work out the details to see what deliverables I can release from the contract).  I will be conducting in some in-depth requirements research for education software for children from Kindergarten to 12th grade.  Non-proprietary materials such as personas, survey results, usability studies, and style guides could greatly help the KDE-EDU project in creating better educational software.  I'm excited for the project mostly because I'll be able to give some of my daily work back to KDE.

<hr />

For those of you who play WoW (world of warcraft): <a href="http://www.okcupid.com/tests/take?testid=7630199738550734280">an addiction test</a>

<b>Well on your way!</b>
<i>You are 73% Addicted!</i>
You play a lot, and you're starting to get hooked. Keep it up and soon you too will be part of the ever-growing group that is totally addicted. Or, see the warning signs now and get out while you still can!

Truthfully, if I took this test a few months ago I probably would have been 90-95% addicted.  I've been very busy lately and havn't had the time/opportunity to sit and play for hours on end because of homework, housework, and other random things.  My second character (undead rogue) is slowly leveling up, so I think once she gets to 45/50 I'll be in get-to-60 mode. 

<hr />

El mentioned in her <a href="http://blogs.kde.org/node/1801">recent blog entry</a> about an issue with the Defaults button in many of the application configuration UIs.  She also <a href="http://mail.kde.org/pipermail/kde-usability-devel/2006-January/000250.html">posted this thread</a> (list membership required for archive) about this issue, which led to a discussion about it in #openusability.

A preliminary solution would be to create '[Reset all defaults]   [Reset (Fonts) defaults]' buttons to better save the user from accidently resetting ALL defaults instead of just the current tab defaults.  In the end we'll just have to test it and see if it is a better solution and by how much.

<hr />

I will be in Las Vegas March 17-20 if anyone is going to be in the area.  Also, there were some small talks about having a Usabiltiy/HCI-WG meeting in Washington, D.C. in the beginning of April before a meeting in Georgia. If there is any interest in this, drop me a line so I can work something out.

<hr />

And finally, a funny story:

This morning, I bought a banana from the cafe in my building.  The only bananas they had were green, and I tried to pick the ripest one.  When I got to my desk, I thought about my banana and said 'I wonder if it would riped if I put it on my laptop?'.  Fruit, afterall, ripens with the help of heat not time, and my laptop runs very hot.  And so I put this banana on my laptop (I use an external keyboard and mouse) and told my friends about it. 'Do you think this will work?', I ask my coworker as he looks at me with a strange look.  'Guess what I did!', I tell a friend who proceeds to laugh at my silly idea.  

Well guess what.  Four hours later my banana no longer has green spots, so by the time I leave for class I should have a nice ripe snack for the drive.  Whoo!

<!--break-->