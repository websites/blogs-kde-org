---
title:   "Contextualizing Shortcuts and Separating Man from Machine"
date:    2005-12-30
authors:
  - seele
slug:    contextualizing-shortcuts-and-separating-man-machine
---
<a href="http://blogs.kde.org/node/1641">One month ago</a> I presented some ideas for an application repository.  This interface would give users an unrestrictive way to search and explore software installed on their system within several contexts.  The system is one part of a response to dissastisfaction with the KMenu.  The other parts of my solution include an interface to quickly launch applications and a "smart" system to help the user relate data and applications together as tasks.

Here is part 2: The Application Launch Menu (including contexualization)

Hundreds of applications could be installed on a system, but only a small percentage of them are actually used.  The KMenu is bulky and slow to navigate through for finding an application you know and use often.  Moderate and advanced users who are more familiar and comfortable with the system may create more easily accessed shortcuts, however over time they may collect and become clutter.  How can we fix this?

Well first off, just about <b>everything</b> is in the KMenu.  Software we use, software we dont use, software we launch from documents instead of shortcuts, software we didnt know was even installed... its a lot of "stuff".  Although we may create shortcuts on our desktop or use the CLI to quickly launch known applications, at some point we still resort to browsing through it to find something we havnt used in a while or ever.  The application index, as <a href="http://blogs.kde.org/node/1641">previously mentioned</a>, provides and easier and more intuitive interface to facilitate our browsing and searching. That helps fix the times we want to discover software.

What about the software we already know and the documents we open often and treat as applications?  The launch interface should help curb the desktop clutter we resorted to when the KMenu was too much for the job.

But first, let's define a context.

Currently the KMenu has no context.  It is a one-stop-shop for system configuration, session control, application shortcuts, searching... everything!  Part of the difficulty it has in its organization and information visualization lies in the lack of context.  Since we dont separate user applications from system applications, we have to create a hierarchy to define that.  Since we dont have user preferences or meta data or contextual information, we try to guess categories and labels the user will find intuitive.  By the time we actually get to the software, there are so many hoops the user has gone through its a suprise they havnt forgotten what they were looking for.

Tasks related to interacting with user information such as documents, email, and images are very user-centric.  User's manipulate their information with use of tools which can manipulate them.

Tasks related to things such as hardware configuration, changing backgrounds, and screen resolution are
machine-centric.  They effect how the system looks, behaves, and reacts to the user.  It doesn't effect the user's data.  Changing the widget theme could be a goal of the user but the machine is what is changed, not the user's documents.

"But what about the grey area of sysadmins who regularly edit config files by hand?"  Then those files become information for those users, and the applications they use to manipulate them are in a user context.  Monitoring network traffic does not effect the users data and is a reflection of the machine. Tasks such as viewing apache logs or editing configuration files create a user (not machine) context for those system files and it becomes user information.

Provided is a mockup of what the user's menu could look like:

<img src="http://gallery.lebwog.com/seele/albums/Screenshots/kicker_info.png" alt="user info context menu" width="327" height="428" align="left" hspace="5" vspace="5" />

<b>User's Shortcuts</b>

Similar to the frequently used programs in the Windows Start Menu, the left half of the menu (column closest to the mouse) is a collection of shortcuts the user can drag and drop and rearrange in whatever order they want.  If what the user is looking for is not in their shortcut list (or contextual menus I describe in a bit), they may browse applications (a link to the application index is below the list of links) or directly search for it using the name or a description (with the search bar at the top of the menu).

Users expect effort to be required while browsing, so a link to the application index where they start from the beginning will suffice.  However, if the user had an idea of what they want, they are less forgiving of the effort required.  The search bar can pass the query to the application index and present the user with the results page.  That way, there is one action necessary to get a result yeilded in a short amount of time.  If the query was sufficient, their target item should be available in the results.  If they need help or to refine their search, query help and lower rated hits could be suggested.

<b>'Tasks' and Smart-menus</b>

To the right of the user's favorites are a series of contextual menus, user- and system-defined.  This panel contains collections of files (applications, contacts, and documents) which relate to each other and are often accessed together.  These menus can act as shortcuts to contexts greater than one or two files.

<i>Creating "Tasks"</i>

With these menus, users will be able to create custom "tasks", documents which are frequently needed in order to complete a goal.  There could also possibly be a button which will "open all files" to provide a one click way to begin a task.  Each item is also clickable to be able to open individual items.

<i>Panel "Buttons"</i>

You may have seem this panel design before, including Microsoft Outlook.  These types of button-menu interfaces work well to create a context based on a descriptive label.  They have tested well qualitativly (acceptance/satisfaction) and quantitativly (performance/comprehension) in usability tests.  From a design perspective, they are a good way to provide additional space while preserving context and understanding of the information.

<i>System-defined Menus</i>

The smart-menus will be described in more detail in my next installment.  Basically, the user will be able to create 'smart' filters to generate automatically updating, yet somewhat bounded lists of their information.  Examples include "Recently Installed Applications" or "Files opened with Kate in the past 48 hours".

I realise now I should have also created a 'machine' system menu to help you visualize the different contexts.  It would be fairly similar with the ability to create 'tasks' such as 'website administration' or 'review and edit user accounts'.  User your imagination and if you think of a frequent scenario that could be sticky, send me a mail.

Next time I will go in to more detail about the information itself and some of the mechanisms we could use to help create better organization, labelling, "smart" filters to help users create tasks, file tagging and meta contet, and all that good stuff.  Knowing how the information works together with the system for the user will help bring the application index and the launch menus together for a solution which could ultimatly replace the KMenu.
<!--break-->