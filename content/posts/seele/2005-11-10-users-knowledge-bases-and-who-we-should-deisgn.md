---
title:   "Users, Knowledge Bases, and Who We Should Deisgn For"
date:    2005-11-10
authors:
  - seele
slug:    users-knowledge-bases-and-who-we-should-deisgn
---
Learning applied to a finite set of knowledge (knowledge base or KB), such as the interaction and functionality of an interface, can be described in a sigmoid curve. It helps describe the learning process, as well as visualize "the learning curve" and "memory retention" humps in beginner and advanced users.

Heres a little visualization of Beginner/Normal/Advanced users:

<img src="http://weblog.obso1337.org/wp-content/themes/default/images/Logisticcurve.gif" alt="Logistic Curve (sigmoid)" width="320" height="260" />

<b>Yellow area: </b>Newer/beginner users.  These users do not have a KB of the interface however have knowledge and affordances of the world (how things work).  Interfaces use metaphors and symbols from our every day world to help relate certain actions, so this knowledge makes their KB > 0.  Since their KB is so small, initial learning is slow until they have a solid foundation to build on, and then it explodes exponentially as a moderate curve.  This learning hump is sometiems called "learning curve".

<b>Green area: </b>Our target audience (moderate users).  They have built up a KB which allows them to quickly associate concepts with similar ideas, and learning is exponential.  Users spend the most time as moderate users, and most never leave this "category".  They learn quickly, however there is a plethora of knowledge to be acquired before reaching a level which is significantly greater than the average.

<b>Blue area: </b>Advanced users.  (Yeah, probably all you guys reading this are stuck somewhere in the blue area.)  They have high knowledge of the interface, but growth tapers off as the amount of new knowledge competes with the maintainance (memory retention/degredation) of the current KB. Because of this memory maintainance and the amount of effort necessary to progress, it is impossible to know everything at once, so they have a limited KB < 1.

The graph is a little deceiving.  I just used a normal function to give you an idea of how the curve works.  Effort in the moderate area of the curve is normally much greater.  Also, as the KB and degree of difficulty increases, amount of effort needed is increased.  This all differs from user to user and interface to interface.

You get the point. But what does this all mean?

Linux has always had a high learning curve, but the users using it were highly adept with technology (or became so) and that difficulty was understated.  The contributer community is comprised mostly of advanced users.  Its easy to forget about the users we dont talk to or work with on a daily basis, yet continually grow as a major demographic.

If we want to succeed in the desktop market, these moderate, normal, every-day users must be our focused user. Theyre growing in numbers and if we want them to continue to grow, we have to pay better attention them.  Cluttered menus, complicated options, and a plethora of features turned on by default dont help the newer (yellow) users overcome the learning curve (which prevents growth in to the moderate (green) group), and it makes it difficult for the moderate (green) users build on their KB and make progress with the interface.