---
title:   "No More Noise"
date:    2005-12-15
authors:
  - seele
slug:    no-more-noise
---
Many of you have probably seen or read some of the emails being posted on the OSDL DA mailing list.  Theyve been on slashdot, blogs, and forwarded to other mailing lists.. but what have they accomplished?

Nothing.

Thats not to say nothing important gets discussed on the mailing list.  After the meeting it seemed as if people were getting organized and starting to discuss and solve problems.  Barely two weeks later, we have cross postings and flaming from seemingly professional contributers.  People who dont know the meaning of the word are bashing 'Usability' and contradicting themselves in the next line.  Gnome and KDE attack and defend their software through the guise of their sponsors.  People are yelling and spamming and arguing for the sake of such, and original comments are getting lost.

Professionals eh?  Usually that requires some degree of <i>professionalism</i>.

The list has turned in to a match between who has the biggest and loudest mouth, waving their software around as if they were comparing penis size.

Really, I have better things to do with my time.  I can't justify pouring through 50 emails a day just to find one intelligent message to comment on.  Not all of us get paid to do this.  Not all of us have jobs which encourage us to read mailing lists and participate in the community for 8-10 hours a day.  So when we volunteer our time and sanity, we do it out of love for OSS with pie in the sky ideals for making the world a better place.  Its just not worth me getting stressed out and upset about something that is supposed to be enjoyable.  Thats what my job is for.

I care about KDE, I care about usability, and I care about the OSS community.  If I didnt care so much I wouldnt use what little vacation I have to go to meetings and give presentations.  I wouldnt spend my weekends and evenings answering emails, setting up calls, and reviewing interfaces.  I dont care about getting paid or popularity or how many articles I get mentioned in.  I want to help give people an affordable, comparable, and usable alternative to the overprices operating systems which have taken over the world.  I want to give people a choice.

But right now, I'm going to sit in my corner, read and write papers, talk to my friends in the lounge, and continue giving willing developers interface usability feedback.  Sure, we need people to participate in cross-project initiatives to fill some of the gaps the community is challenged with, but its not going to be me.

The OSDL desktop architects group is an important one, but until people grow up and get organized I'm withdrawing my participation.  Someone else can filter noise all day.

/rant

<!--break-->