---
title:   "KDE Usability Poster for WUD"
date:    2005-11-02
authors:
  - seele
slug:    kde-usability-poster-wud
---
November 3rd is <a href="http://www.worldusabilityday.org/">World Usabiltiy Day</a>, an event sponsored by the <a href="http://www.upassoc.org/">Usability Professionals Association</a> and <a href="http://www.humanfactors.com/">Human Factors International</a>.

The local D.C. Metro chapter of UPA is <a href="http://www.upa-dc-metro.org/events/2005/11-03-05.htm">participating</a> in World Usability Day by hosting a poster session during the larger event held at the <a href="http://www.aia.org/">American Institute of Architects</a>.  Even though the theme of the event is "e-Government", other projects, case studies, and test results were invited to participate.  So, what would be a better time to promote KDE Usability than during World Usability Day?  

I spent a lot of hours on this poster and am proud of the results. El and Jan over at <a href="http://www.openusability.org/">Open Usability</a> have also coordinated an <a href="http://www.usabilitytag.de">event in Berlin</a>, and will be displaying a copy of the poster at their event as well.  I want to thank everyone who participated with creating the poster, it really came out great.

The poster session is from 6pm to 7pm at the AIA, with the panel presentations from 7pm to 9pm.  Registration is $10 at the door, so if you are interested in usability and in the D.C. area, I hope to see you there!

<a href="http://obso1337.org/hci/kde/WUD_KDE_Poster.pdf"><img src="http://weblog.obso1337.org/wp-content/themes/default/images/wud_kde_poster_thumb.gif" alt="WUD KDE Poster Thumbnail" /></a>