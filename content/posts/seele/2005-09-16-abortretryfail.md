---
title:   "Abort/Retry/Fail"
date:    2005-09-16
authors:
  - seele
slug:    abortretryfail
---
Last night I figured I would do something useful and go through the Suse 10.0 rc1 installation.  It is a very pretty and straightforward installation process provided you have the CD media to install packages.  One button label, which was EVERYWHERE, bothered me a bit:

[Abort]

This is a very strong word.  In English, it is much stronger and negative than Cancel (which is generally neutral). It is rarely (if ever) used in UIs.  I'm not sure if average users who have never see it would understand 'Abort' in the context of a user interface.  It is much more serious than 'Cancel' (which many users have encountered and understand to a degree) and users may even be <strong>afraid</strong> to click on it because it sounds so serious and they dont know what might happen.

The label is scattered all throughout the installation and yast2 applications.  I would suggest evalutating the meaning and action of "Abort" and rename it to something less menacing such as "Cancel" or something more explicit and descriptive of the individual action.
