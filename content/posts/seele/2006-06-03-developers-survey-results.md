---
title:   "Developers Survey Results"
date:    2006-06-03
authors:
  - seele
slug:    developers-survey-results
---
<p>In an effort to get to know better the needs of developers are from the Human Interface Guidelines, I solicited several groups of developers to participate in a survey which ran for two weeks in May.  The outcome was very good with 52 participants providing their comments and suggestions.</p>

<p>After reviewing the results and compiling them in to a report I think several things are safe to assume about developer's relationship with the HIG:</p>
<ul>
<li>They don't trust it.</li>
<li>They don't trust us.</li>
<li>They expect more from it.</li>
<li>They didn't know it existed.</li>
<li>They might actually use it.</li>
</ul>

<p><b>They don't trust it.</b> The old guidelines are just that -- old.  Being out of date and inaccurate are strong reasons why developers might not use them.  Being incomplete also raised some questions in their validity.</p>

<p><b>They don't trust us.</b>  Some of them flat out said they didn't trust them (and those who write them) and won't until they see some theory behind the claims.  Trust in the usability community has always an issue.  Theory is fine and dandy, but misunderstood or out of context its useless.  Just because you don't like it or don't understand it shouldn't be grounds for creating inconsistent interfaces.  If you really feel that strongly against that particular guideline, there are better ways to change them than ignoring them.</p>

<p><b>They expect more from it.</b> The quality and quantity of the guidelines are an issue.  There isn't enough there and what <i>is</i> there is dated and old.  If the HIG is to be a definitive guide then it needs to be current and updated frequently.  Important issues need to be added in order for them to be referenced, there were many comments of fruitless searches because content was missing.</p>

<p><b>They didn't know it existed.</b>  Apparently it isn't very easy to find the guidelines because many of the participants didn't know they existed, or had looked for them and never found them.  Along with a community-wide effort to promot the guidelines, it has to be more easily accessible.</p>

<p><b>They might actually use it.</b>  No one was really against having guidelines, infact many were optimistic they would use it if it met their explicit demands.  If we can solve the Q&amp;Q (quality and quantity) problem which is present in the existing guidelines, I'm optimistic the new guidelines will be happily adopted.</p>

<p>What you've been waiting for: KDE4 HIG Developers Survey Results (<a href="http://obso1337.org/hci/kde_hig/KDE4_HIG_Developers_Survey.pdf">PDF 601KB</a>)</p>
<!--break-->