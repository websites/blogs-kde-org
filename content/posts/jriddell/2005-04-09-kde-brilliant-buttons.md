---
title:   "KDE Brilliant Buttons"
date:    2005-04-09
authors:
  - jriddell
slug:    kde-brilliant-buttons
---
The wee 80x15 website banners have become a popular to show your allegiance and make a much more sophisticated alternative than the old style of banner which started long long ago with "Netscape Now".

I made some for KDE (and Kubuntu) using this <a href="http://www.lucazappa.com/brilliantMaker/buttonImage.php">Brilliant Button Maker</a>.

Grab them from the <a href="http://kde.org/stuff/clipart.php">KDE Clipart</a> page to show your preferences.

<img class="showonplanet" src="http://kde.org/stuff/clipart/kde-desktop-brilliant-button-80x15.png" width="80" height="15"  />

<img class="showonplanet" src="http://kde.org/stuff/clipart/kde-user-brilliant-button-80x15.png" width="80" height="15" />

<img class="showonplanet" src="http://kde.org/stuff/clipart/konqueror-brilliant-button-80x15.png" width="80" height="15"  />

<img class="showonplanet" src="http://www.kubuntu.org/kubuntu-banner.png" width="80" height="15"  />
<!--break-->