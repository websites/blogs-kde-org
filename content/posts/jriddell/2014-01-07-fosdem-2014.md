---
title:   "FOSDEM 2014"
date:    2014-01-07
authors:
  - jriddell
slug:    fosdem-2014
---
<img src="http://www.mysqlperformanceblog.com/wp-content/uploads/2013/11/FOSDEM14.jpg" width="185" height="59" />

I'm going to FOSDEM for 2014, are you?  FOSDEM is a massive free software meeting with more projects than you knew existed.  We need help on the KDE stall.  We also need visitors in the devroom.  Finally we need KDE people to come and eat pizza on Saturday evening.  <a href="http://community.kde.org/Promo/Events/FOSDEM/2014">Add yourself to the wiki page</a> if you want to help KDE at FOSDEM.
