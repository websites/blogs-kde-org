---
title:   "Linus Comes to Edinburgh"
date:    2013-10-23
authors:
  - jriddell
slug:    linus-comes-edinburgh
---
Linus is visiting Edinburgh today for a Q&A session at LinuxCon.

Asked where he'd like to see Linux in 5 years time he said he started Linux because he wanted to use it on his desktop but he's been disappointed how that hasn't taken off.  He wishes desktop communities would stop arguing amongst competing technologies and would work together.  How true. (And I continue to feel smug about being part of a community which hasn't started a new project to replace any desktop or desktop technology).

<a href="http://www.flickr.com/photos/jriddell/10438182383/" title="DSCF8725 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7391/10438182383_d36a2a884c.jpg" width="500" height="375" alt="DSCF8725"></a>

<a href="http://www.flickr.com/photos/jriddell/10438024296/" title="DSCF8726 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3698/10438024296_fd5aef8bca.jpg" width="500" height="375" alt="DSCF8726"></a>
