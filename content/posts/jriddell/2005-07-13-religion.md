---
title:   "Religion"
date:    2005-07-13
authors:
  - jriddell
slug:    religion
---
Appologies to readers of planet.ubuntu.com, the return of kdedevelopers.org has done funny things to the RSS feeds (and planet.kde.org is even more broken).

Edinburgh got taken over by anarchists the other weekend.  We had to board up Subhi's new internet caf&eacute; to protect the rather fancy SIP phone setup he has inside.

<img src="http://jriddell.org/photos/2005-07-12-jonathan-jon-subhi-anarchist-protection.jpg" width="400" height="300" class="showonplanet" />

Some people take their Free Software a bit too seriously, take sladen for example...

<img src="http://jriddell.org/photos/2005-07-12-paul-kubuntu.jpg" width="400" height="300" class="showonplanet" />

<!--break-->
