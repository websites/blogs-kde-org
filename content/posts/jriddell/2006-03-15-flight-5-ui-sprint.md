---
title:   "Flight 5, UI Sprint"
date:    2006-03-15
authors:
  - jriddell
slug:    flight-5-ui-sprint
---
Last week was the Kubuntu and Ubuntu UI sprint in London where kwwii got strict instructions to turn up the bling on Kubuntu's artwork.  The Gnome Clearlooks developers were also there to turn Ubuntu orange.

Today I released <a href="https://lists.ubuntu.com/archives/ubuntu-announce/2006-March/000057.html">Kubuntu Flight 5</a>, featuring the espresso live CD installer, 40% more blue, skim and the groovy simple program installer adept_installer.

UI sprinters:

<img class="showonplanet" src="http://bootsplash.org/uisprint1-small.jpg" width="300" height="200" />

<!--break-->