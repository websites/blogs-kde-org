---
title:   "Owncloud Packaged - Free Trial!  Samba Support - At Last"
date:    2011-02-22
authors:
  - jriddell
slug:    owncloud-packaged-free-trial-samba-support-last
---
Not infrequently people ask what's the best cloud "solution" for KDE.  This is not a very useful question since "cloud" is woefully overloaded as a term.  But now there's a simple answer, I packaged <a href="http://owncloud.org/">OwnCloud</a>, the file storage web app from KDE.  It's in natty or maverick-backports for 10.10 users.  The licencing was the most fiddly thing here, if KDE is to get into web apps we need to update our licence policy which doesn't take into account AGPL nor the annoying PHP licence.

And now an exclusive offer to the first ten readers of this blog, a free trial of my very own OwnCloud!  <a href="">Try it now</a> [trial over, hope you had fun], upload files, download files, work out why the HTML5 media player doesn't work, attach by WebDav, it's all good fun.  It's running on a RackSpace cloud machine which costs a super-cheap 1.5 US cents an hour.

<a href="http://people.canonical.com/~jriddell/owncloud.png"><img src="http://people.canonical.com/~jriddell/owncloud-wee.png" width="400" height="288"></a>
ownCloud
<hr />
Filesharing has been one of a few basic missing feature in KDE for years.  There's some really old support which at one time worked by editing Samba config files, and now mostly serves to break Samba config files.  Black belt Kubuntu ninja Rodrigo Belem implemented Samba sharing supporting using the modern interfaces which is now in KDE Git master (and Kubuntu natty packages).  It even uses PackageKit to install Samba for you if you don't have it already.

I'm also close to implementing Samba browsing in system-config-printer-kde, the top requested missing feature.  I also fixed up many of the worst bugs which have been hanging around for far too long.

<a href="http://people.canonical.com/~jriddell/samba.png"><img src="http://people.canonical.com/~jriddell/samba-wee.png" width="400" height="239" /></a>
Samba sharing - at last.  Next step WebDav (it'll be like having your very own cloud).

<hr />

Top new Kubuntu contributor Romain turned up recently wondering what he could help with.  He's been tidying up Language Selector which installs language packs.  It's now integrated into System Settings, has a bunch of small UI tweaks and no longer runs as root.  Lovely.

<a href="http://people.canonical.com/~jriddell/language-selector.png"><img src="http://people.canonical.com/~jriddell/language-selector-wee.png" width="400" height="239" /></a>
<!--break-->
