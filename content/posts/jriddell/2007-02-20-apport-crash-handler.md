---
title:   "Apport Crash Handler"
date:    2007-02-20
authors:
  - jriddell
slug:    apport-crash-handler
---
I just uploaded support in Adept for Apport, the Ubuntu crash handler.  Ubuntu uses a modified kernel which calls a user space application when an application crashes and that writes a report with information including the core.  Adept Notifier now watches for these reports appearing and runs the Apport frontend when they do, which uploads all the data to the bug tracker if the user so wishes.

Ubuntu also has a repository with packages that contain all the debugging symbols for all the software in the archive.  So once a crash has been uploaded the developer can looks at the core dump and get a backtrace.

Of course KDE has had a crash handler for years and so Apport is only for non-KDE applications, but it's a step ahead of telling the user to download the debugging packages, install gdb and post the backtrace.  I wonder if this would be a sensible replacement?  

Thanks to Michael Hofmann for doing the Qt frontend.

<img src="http://kubuntu.org/~jriddell/tmp/apport.png" width="508" height="105" />

<img src="http://kubuntu.org/~jriddell/tmp/apport2.png" width="442" height="394" />

Kubuntu hero of the day prize goes to Tonio who spent two days trying to work out why kdesktop isn't loading its translations properly, it was all my fault.
<!--break-->
