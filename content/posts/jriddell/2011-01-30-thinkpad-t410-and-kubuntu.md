---
title:   "Thinkpad T410 and Kubuntu"
date:    2011-01-30
authors:
  - jriddell
slug:    thinkpad-t410-and-kubuntu
---
Thinkpad T410

Laptop refresh time is here, thanks Canonical.  Faced with the prospect of finding a computer to spend a grand on I took the bus trip to Fry's in Dallas.  Fry's is a large computer supermarket, I don't recommend it for the cafeteria but they do have a large selection of laptops to browse.  The staff are annoyingly keen to help you, until you start asking about the finer details of processor ranges and mention Ubuntu.  All their laptops have glossy screens, a phenomenon I do not understand.  

So I went back to the hotel and ordered a ZaReason laptop.  ZaReason are a small US company who make laptops with Kubuntu pre-installed. Unfortunately with only 24 hours to go before I left the country they couldn't put one together in time for me.  I did get an e-mail from their CEO saying they were desperately trying to put one together in the 45 minutes before the parcel lorries left but it wasn't to be.  

Option three was to order a Thinkpad, an option that has always served me well with every other computer I've bought.  I looked at various models and was for a time tempted by the weeny 10 inch screen X series for portability, but that would mean finding an external monitor for use at home.  The 15 inch screen W series might be fun but wouldn't fit my rucksack.  So I went for a T410.

Hardware is lovely, feels solid, lighter than my old model, internal speakers sound better too, screen is brighter.  Most importantly the monitor isn't reflective and glossy.  Interesting new ports one the outside that I'll probably never use include eSATA, DisplayPort, yellow Always On USB and ExpressPort.  Obvious missing feature though is an SD card reader, have these gone out of fashion? (Tenner for one on eBay will nicely fill the ExpressPort slot)?  3G is built in but doesn't seem to work (https://bugs.launchpad.net/ubuntu/+source/linux/+bug/554099).  I'm tempted to buy an Ultrabay SATA adaptor and replace the CD drive with a solid state disk, I hear it makes the laptop nice and silent and run super fast for read/writes.

I loaded Windows.  It really hasn't improved.  It gets in your way all the time.  I just want to browse the internet please!  No, I need to read a third click through licence for something I don't even want.  Internet Explorer seems to be full of buttons and terms I don't understand.  Do I want to set my Accelerators?  I really have no idea.  I don't want 5 reminders that I have to enable virus protection, I want it to be secure without it hassling me.  Why does the Thinkpad setup application use Windows 95 themeing, isn't that a bit out of date.  No I don't want to pay Microsoft even more money for the privilage of using a word processor.  Why is this operating system remotely popular?

Pleasingly Linux just works.  Wifi is fine.  Compositing great (Intel i915).  Suspend is all good.  External video works.  Volume buttons works.  Only wee issue is that mute button turns off hardware and software mute, but if software is already mute then it turns it mute off and hardware mute on.  

The keyboard has an elongated Esc and Delete key which takes a little getting used to but is a very sensible idea.

I'm generally very pleased with my Thinkpad T410 running Kubuntu and would recommend it to anyone with a thousand pounds needing spent.

<img src="http://people.canonical.com/~jriddell/thinkpad-t410-kubuntu.jpg" width="400" height="300" />

<b>Update: </b> there is an SD card reader, it's hidden at the front underneith the curve of the plastic so you can't see it unless you have the laptop at eye level.
<!--break-->
