---
title:   "CMake 2.6.2 in backports"
date:    2008-11-13
authors:
  - jriddell
slug:    cmake-262-backports
---
KDE trunk now needs CMake 2.6.2.  A few people have asked where it can be found for Kubuntu, fear not friends it's in intrepid-backports.  You can enable backports through Adept by ticking the "Unsupported Updates" box in the sources editor.
