---
title:   "Akademy Day 1 Photo Blog"
date:    2014-09-06
authors:
  - jriddell
slug:    akademy-day-1-photo-blog
---
<img src="https://pbs.twimg.com/media/Bw3-FXXCYAASCmV.jpg:large" width="341" height="192" />
Some of the Kubuntu Devs

<img src="https://lh6.googleusercontent.com/-g7odmZgW-S0/VAr-463xggI/AAAAAAAAAKA/-a-zApXG0uA/w958-h539-no/14968992819_bf8e822229_b.jpg" width="319" height="119" />
Talking and hacking in the corridor

<img src="https://pbs.twimg.com/media/Bw3HkotIUAAr4dM.jpg" width="192" height="341" />
Sebas celebrates the release of Plasma 5

<img src="https://pbs.twimg.com/media/Bw2bvBJIMAEmRLm.jpg" width="600" height="337" />
David Explains Frameworks 5

<img src="https://pbs.twimg.com/media/Bw1kcL5CMAArvvd.jpg" width="600" height="337" />
Morning exercises led by President Lydia

<img src="https://scontent-a-ams.xx.fbcdn.net/hphotos-xaf1/v/t1.0-9/10600435_10152406408528918_5181206918244102057_n.jpg?oh=b152697e06a26c2cd1353211ed189793&oe=5498498C" width="320" height="180" />
3D printing

<img src="https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/10649839_10152406134948918_4672687510736099427_n.jpg?oh=7c26c6940b2cc6116e4ea682975d48a3&oe=549DAA45&__gda__=1419563280_9f26f47c845c37645033853ddf45f31e" width="270" height="480" />
Konqi and family
