---
title:   "Debconf in Edinburgh"
date:    2007-06-18
authors:
  - jriddell
slug:    debconf-edinburgh
---
Debconf rolled into town and took over Edinburgh university's lovely Teviot building. Makes quite a change being able to cycle for 10 minutes to get to a conference rather than travelling for a day.  Me and Kenny have been keeping an eye out for things to copy and things to improve for Akademy (now only 11 days until arrivals).  

DebianDay was the user talks day, and as with user days at Akademy in the past most of the people there were developers, but there clearly were some users.  Knut from Trolltech showed off the greenphone and also some educational apps.  Bdale said how much HP supports Debian and free software in general (it's true, they're Akademy gold sponsors).  Nick from positive internet gave the best talk I've seen on why the freedom of free software is important, quoting from a case in the 18th century about whether English publishers could copy a Scottish published book.  Chris Halls from Credative UK described what his company is doing to support and make money from free software.  Finally Patrick Harvie the parliamentarian, who should also be at Akademy, gave the view of why Free Software is important in the public sector.  

Sunday was the start of the conference proper.  In the introduction session the outgoing DPL spoke of what had improved under his watch, and the incoming one spoke of what he wanted to improve.  SELinux was the first talk (way too complex according to oor Ian Jackson, but the guy implementing it seems certain he can make it useful for desktop people without getting in the way like Fedora).  The dudes from Sun came along and discussed the state of free Java, there's still a small bunch of non free stuff in there, and its hard to port to other architectures because it needs itself to build, but there's a project called Iced Tea which is filling in the gaps.  And it should all be free soon.  Interestingly a talk later in the day looks at some figures for Debian including the top 5 languages and in Etch Java is the 4th most used language behind only C, C++ and Shell and beating Perl.  Crazy, but Lisp is in 6th place which is also a bit nuts.  How long would it take to write Debian from scratch with closed development methods?  9 years and 12,000 coders said some statistical analysis (so they're three years behind schedule).  

"An outsider's thoughts on the Ubuntu / Debian relationship" was a polite flame on why Ubuntu hurts Debian.  I think many of his points may be right, the success of the Ubuntu community and project may take away some new developers, some publicity and be yet another distro for software and hardware companies to support.  But equally   I think it also brings in just as many developers who would be scared off by Debian's new maintainer process but can contribute to Debian through Ubuntu and companies have always had the issue of supporting multiple distros.  Saying Ubuntu uses a loophole in the GPL and that there's only one linux kernel and only one wikpaedia is factually wrong and shows quite a misunderstanding of the freedom in free software.

The "Dependency based boot sequence" talk showed that you can easily find bugs in the existing boot sequence by using the dependency data the boot scripts now contain.  I'm not sure if they'll be changing to upstart any time soon but it does show the need for an event based boot loader.

A good conference, quite scattered about (hard to find people often) and details like having timetables available and someone introducing the talks are lacking, but they do have an impressive video team and the venue is excellent.  <a href="http://streams.video.debconf.org:8000">Streams here</a>, <a href="https://debconf7.debconf.org/wiki/Schedule">schedule here</a>.

<a href="https://gallery.debconf.org/album18/dsc00776"><img src="http://kubuntu.org/~jriddell/tmp/dsc00776.sized.jpg" width="320" height="240" /></a>
Bdale (did I mention he got HP to give Akademy loads of money?)
<!--break-->
