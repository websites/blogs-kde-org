---
title:   "Akademy BoFs"
date:    2012-07-12
authors:
  - jriddell
slug:    akademy-bofs
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/AkademyLogo225px.png" width="225" height="170" />

A week in Estonia, the lovely sunshine, the magical forest and all the BoFs you could want.

<b>Qt Quick Training</b> The main event of the Monday was KDAB running a Qt Quick training session.  Qt Quick continues to be full of swish bling although the widget sets are all a bit incomplete.  The good news is there's not a lot of QML  change in Qt 5.  Kevin even showed us some new bling from Qt 5 which makes snowflakes, pretty.

Tuesday was the <b>KDE e.v</b> AGM.  Amazingly enough we had a volunteer for treasurer.  This is such a valuable job to have filled so we're very lucky to have Augustin actively want to do it.

On Wednesday I sat in on the <b>KDE Telepathy</b> BoF which made a challenging TODO list for 0.5 which we expect to include in Kubuntu.  vhanda also ran a BoF on <b>Constructive criticism for Nepomuk</b> that looked at the many problems it causes and where best to start fixing them.

Thursday saw the <b>Plasma Media Center</b> BoF and I went over a couple of the problems with getting it to work in a distro, it's been a long time coming so it'll be great to have it finally working, but it needs a newer Plasma Active to be released.

<a href="http://www.flickr.com/photos/jriddell/7500934500/" title="DSCF7038 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8011/7500934500_8fae067dc9.jpg" width="500" height="375" alt="DSCF7038"></a>
Nepomuk Sparkles

<a href="http://www.flickr.com/photos/jriddell/7500933816/" title="DSCF7042 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8003/7500933816_11653091e5.jpg" width="500" height="375" alt="DSCF7042"></a>
A Plasma Active app hacking session

<a href="http://www.flickr.com/photos/jriddell/7554960830/" title="DSCF7044 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8424/7554960830_be0892b2ee.jpg" width="500" height="375" alt="DSCF7044"></a>
The Indian continent is a real growth story for Akademy this year, over a dozen people came from there.  Here Shantanu and Sinny go over Plasma Media Center

<a href="http://www.flickr.com/photos/jriddell/7554960428/" title="DSCF7048 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7130/7554960428_b6a26d3fcd.jpg" width="500" height="375" alt="DSCF7048"></a>
Vishesh takes on the entire Estonian navy!
<!--break-->
