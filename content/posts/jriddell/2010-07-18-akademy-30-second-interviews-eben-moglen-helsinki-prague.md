---
title:   "Akademy 30 second interviews, Eben Moglen, Helsinki, Prague"
date:    2010-07-18
authors:
  - jriddell
slug:    akademy-30-second-interviews-eben-moglen-helsinki-prague
---
<b>Interviews</b>

I did some <a href="http://people.canonical.com/~jriddell/akademy/30-second-interviews-2010/">30 second video interviews</a> at Akademy in Helsinki.  You can download them to get a feel for the people and place of Akademy.  Unfortunately I've been unable, in the limited time I've devoted to it, to convert them to Oggs, ffmpeg doesn't want to do it.  I've also been unable to find a simple HTML5 video gallery script that would make a simple HTML page with the videos embedded.  Do let me know if you have the answers to those.  I recommend Frank's interview for an insight into what our board spends its week doing.

<b>Eben Moglen Talk</b>

Before Akademy I went to see Eben Moglen give a talk at the Scottish Society for Computers and Law.  Eben is a much overlooked rock star of free software, having written the GPL licences and made many other significant contributions.  

His talk was about the dangers of embedded software, taking the example of the software in cars because it is a topical issue but also in other life critical areas such as aeroplanes and medical devices.  Failures in the software result in people dieing but unlike other life critical engineering there is no regulatory overview.  Various governments have strong regulatory requirements and testing for the mechanical parts of a car but none for the software.  Given the complexity of the software that would be the a difficult job even if regulators cared, which they should start to do after the Toyota issue.  Eben's solution, predictably, is to make all such software be required to be free software.  That way all bugs become shallow and problems can be quickly fixed.  Unfortunately the current situation in Europe is not promising, the EU Commission bans software on medical devices from being free software.  This is in the belief that it would be a security problem if such software was modifiable.    There does need to be controls on who can modify such software, but banning it from being Free Software (and the breaches of the GPL which result) is clearly a bad idea.

Some questions from the audience including one from Fred Macintosh who very nearly become my MP a few weeks ago, if he's put "interested in Free Software" on any of the 67 leaflets his campaigners put through my door I'd have been far more likely to vote for him.

<b>Travels</b>

I left Akademy at the end last week and travelled to Helsinki.  My Friend in Helsinki wasn't in so I gatecrashed a group of international cancer researchers and played Pictionary instead.  Later I went to the beach and went for a canoe around Helsinki harbour.  

No rest from travel though.  This weekend I caught a ferry to Amsterdam then a sleeper train to Prague for Canonical's Platform Sprint.  Journey was all good (except for the frightening number of stag and hen parties on the ferry).  As usual booking land based international travel is a pain with several unconnected booking systems.  Prague turns out to have an awesome artificial white water slalom course so I spent the day surfing on the waves.

<b>Politics</b>

The Liberal half of the new UK government set up a website <a href="http://yourfreedom.hmg.gov.uk/">to discuss repealing laws</a>.  Predictably enough most of the ideas are nuts so I decided to see if the site would work for what it was intended for, repealing bad laws.  If you are a UK citizen, please give your rating to <a href="http://yourfreedom.hmg.gov.uk/restoring-civil-liberties/repeal-children-and-young-persons-harmful-publications-act-1955/idea-view">repeal the obscure Children and Young Persons Harmful Publications Act</a> which bans comics.  I like (some) comics and a law which bans them, even if it doesn't get used, is a bad law.  I wonder if Nick Clegg will listen.

<img src="http://farm5.static.flickr.com/4101/4787177502_3362b79e73.jpg" width="500" height="375" />
Helsinki Sun Set
<!--break-->
