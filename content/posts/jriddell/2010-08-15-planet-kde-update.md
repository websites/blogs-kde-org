---
title:   "Planet KDE Update"
date:    2010-08-15
authors:
  - jriddell
slug:    planet-kde-update
---
<a href="http://www.planetkde.org">Planet KDE</a> is your insight into the lives and activities of KDE, a community making lovely free software.

Occasionally I get asked to add feeds to Planet KDE which aren't KDE contributor blogs.  News feeds, user blogs and non-English blogs are the usual requests.  So today, thanks to the helps of others, I've updated our software and added opt-in support for these different types of feeds.  Just click the configure button at the top of the page.  

If you have a KDE project news feed (including KDE related distro news), a KDE user blog, or a KDE blog in Spanish or another language do <a href="http://bugs.kde.org/enter_bug.cgi?product=planet%20kde">file a bug</a> or add it yourself to the feed (it's in svn) with the new define_feedclass attribute.

I also updated the software to Rawdog 2.12 which should help keep the memory usage on the server down.

Thanks to Ade, Stuart and Kurt for helping with various bits.

<b>Update:</b> The new feeds are not added to the RSS feed (they were yesterday but I stopped that now).  Also the history is longer, you get 5 pages now instead of just 2, click on the "older blog entries" link at the bottom.
<!--break-->
