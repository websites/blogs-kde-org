---
title:   "Qt Releases, KDE Hires"
date:    2009-03-04
authors:
  - jriddell
slug:    qt-releases-kde-hires
---
<a href="http://untangled.biz/blog/kde/qt-release-madness/">Qt 4.5 came out today</a> and I've just uploaded it to the archives.  Lots of shiny new features and speed. Qt Creator also came out and it's available in Jaunty too as an easy to use IDE.

Aurelien <a href="http://agateau.wordpress.com/2009/02/26/exciting-change/">blogged that he got hired by Canonical</a> to work on the desktop experience team.  Today former Kolab developer <a href="http://sonofthor.dk/">Bo Thorsen</a> turned up on #kubuntu-devel, he's also been hired.  Good luck guys.
