---
title:   "Bazaar 1.0 Released"
date:    2007-12-16
authors:
  - jriddell
slug:    bazaar-10-released
---
The <a href="http://bazaar-vcs.org/">Bazaar</a> team released version 1.0 of the world's finest revision control system.  Congratulations to them all.

I think the best explanation of the power of Bazaar is the <a href="http://bazaar-vcs.org/Workflows">workflows page</a>, you can use it in the old central repository mode if you wish but there's a number of others ways of doing revision control and bzr's strength is that it works well with all of them.

You can find an introduction on my <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">talk from Kubuntu Tutorials Day</a> or just <a href="http://doc.bazaar-vcs.org/latest/en/mini-tutorial/index.html">read the tutorial</a>.

Even better news is the memory leak in bzr-svn has been fixed so once that update gets into distros it'll be easy to branch KDE's SVN.

<img src="http://bazaar-vcs.org/htdocs/bazaarNew/css/logo.png" width="144" height="149" />
<!--break-->