---
title:   "Microblogging on Planet"
date:    2008-10-12
authors:
  - jriddell
slug:    microblogging-planet
---
I added microblogging support to <a href="http://planet.kde.org/">Planet KDE</a>.  A surprising number of KDE developers seem to be communicating their lives over Twitter so it seems something that should be opened up for all to read.  It's in a sidebar that you have to click to show so it shouldn't get in the way if you're not interested.  It's a bit of an experiment, I'm not entirely convinced of the usefulness of it but we shall see.  Currently only Twitter is known to work but it's probably not hard to ensure other microblogging sites work (does Facebook let you have a public status update feed?).  I expect the content of the microblogs will be generally more personal than normal blogs, currently it's quite Amarok biased since those Amarok people love Twitter. You can add yourself through the normal means, <a href="http://websvn.kde.org/trunk/www/sites/planet/planetkde/config?view=log">editing the config file</a>, <a href="http://bugs.kde.org/enter_bug.cgi?product=planet%20kde">submitting a bug</a> or poking me on IRC.

Also in Planet KDE news, if you missed out a couple of days you may find you've missed out on important blogs.  No more with the <a href="http://planetkde.org/old.html">old blogs page</a> which shows the next 30 blogs.

Ooh, <a href="http://forum.kde.org/">new KDE forums</a>.  Lovely to see another piece of the improved KDE infrastructure in place.
<!--break-->
