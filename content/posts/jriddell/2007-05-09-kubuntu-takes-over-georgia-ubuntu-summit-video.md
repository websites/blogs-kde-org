---
title:   "Kubuntu Takes Over Georgia; Ubuntu Summit Video"
date:    2007-05-09
authors:
  - jriddell
slug:    kubuntu-takes-over-georgia-ubuntu-summit-video
---
When I was 12 we had a question in the school's annual general knowledge quiz "What is Georgia?".  The three correct answers were: a country in eastern Europe, a state in the US and the dog of my English teacher.  The dog died and I don't know much about the US state but we had a nice talk about the country this morning.  

Georgia is rolling out Kubuntu in all their schools.  This is being done as part of a project to bring the internet to every school in Georgia.  I don't have many details yet but it should become a Canonical case study soon enough so it will be used as an example of how free software is taking over the world one country at a time.

<a href="http://www.flickr.com/photos/jriddell/489867805/"><img src="http://farm1.static.flickr.com/223/489867805_2778add26b_m.jpg" width="240" height="180" class="showonplanet" /></a>
<small>Kubuntu in Georgia talk</small>

The Fridge editors are being slow but you can grab the official <a href="http://kubuntu.org/~jriddell/ubuntu-uds-sevilla-720x576.ogg">UDS Sevilla video (50MB)</a> to experience the fun of being locked up in a basement for a week with 100 geeks.

<img src="http://kubuntu.org/~jriddell/uds-video.png" width="180" height="132" />
<small>KDE board talking business with Canonical bizdev</small>

<!--break-->
