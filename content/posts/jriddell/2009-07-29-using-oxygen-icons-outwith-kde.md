---
title:   "Using Oxygen Icons Outwith KDE"
date:    2009-07-29
authors:
  - jriddell
slug:    using-oxygen-icons-outwith-kde
---
Occationally people ask if they can use Oxygen icons on their website or in their (non-KDE) application.  The answer is that of course you can, it's all lovely free software, you just need to include the LGPL and credit the Oxygen dudes.  <a href="http://techbase.kde.org/Projects/Oxygen/Licensing">I wrote this handy guide</a> for people who want to do so.
