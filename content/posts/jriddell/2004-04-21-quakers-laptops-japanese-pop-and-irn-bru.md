---
title:   "Quakers, laptops, japanese pop and irn bru"
date:    2004-04-21
authors:
  - jriddell
slug:    quakers-laptops-japanese-pop-and-irn-bru
---
We got on well with the GNOMEs at the Linux User Expo in London today.  Except for when I filled in their missing poster space with a spare KDE logo, they didn't take that too well.

We had some experienced users ("I don't use KDE, I use <a href="http://jriddell.org/larswm/larswm-screenshot-2.png">LarsWM</a>" or the type) and some less experienced ("so how much do you charge for this Umbrello UML Modeller").

No news from Rich at the awards ceremony yet, we're up against the Sun Java Desktop and the Ximian Desktop, both of which look remarkably like GNOME.

Right now there's half a dozen people and half a dozen laptops including some of the Free Software Quaker massive but also Charles and Howells.  Charles, could you pass the Irn-Bru please?

Some of George's photos are at http://jriddell.org/photos/
