---
title:   "Calligra's Second Release"
date:    2012-08-13
authors:
  - jriddell
slug:    calligras-second-release
---
<img src="http://starsky.19inch.net/~jr/tmp/B9Q92.png" width="500" height="154" />
Calligra has made a second release.  <a href="http://www.kubuntu.org/news/calligra-2.5">Packages are aviailabe for Kubuntu</a>.  With this release I'm confident Calligra is ready for bigger exporure and we're evaluating it for Kubuntu 12.10.
