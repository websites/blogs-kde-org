---
title:   "Viruses"
date:    2006-01-24
authors:
  - jriddell
slug:    viruses
---
Today I published an article pointing to a <a href="http://dot.kde.org/1138055794/">KlamAV tutorial</a>.  I'm not sure how useful klamav is since e-mail viruses don't affect gun/linux but I think the claims that we do not get viruses are wrong.  The other day I found my machine going sluggish and noticed that today my router had decided to forward incoming connections on port 22 to my laptop (I'd asked it to ages ago and it never worked at the time).  I had an account on my laptop with username test and password also test that I had used at some point to check out what a first time user sees of Kubuntu.  Some computer had ssh'ed into my laptop, checking for various common usernames and presumably accounts where the password is the same as the username and having succeeded to get into my computer it copied a program rootkit over to try and get root access meanwhile using my computer to portscan various IP ranges for other vulnerable computers running ssh.  Self copying code doing malitious things, sounds like a virus to me.
