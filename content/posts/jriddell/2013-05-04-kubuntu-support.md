---
title:   "Kubuntu Support"
date:    2013-05-04
authors:
  - jriddell
slug:    kubuntu-support
---
<img src="http://www.emerge-open.com/holding/EmergeOpen-logo.jpg" width="693" height="153" />
Some time ago I got a phone call saying Canonical were stepping back from their support of Kubuntu.  But Free Software doesn't like to quietly disappear and I've been spending the last year getting back what went missing.  Last week I had a meeting to get the last bit in place, commercial support.  A nice guy called Niall from <a href="http://www.emerge-open.com">Emerge</a> spent a lot of time and energy getting an agreement from Canonical to be able to provide the commercial support then found a company to actually provide that.  So I drove down to a part of England called Huddersfield to visit the offices of <a href="http://withsupport.co.uk/">With Support</a>, a small company doing Linux support.  Their office has a fresh cut wood smell coming from all the fresh cut wood.  Tariq is the head man having built up the business from scratch.  He started off doing Linux desktop support but has moved away from that due to lack of business, hopefully being an official Kubuntu partner will fix that.  
<img src="http://withsupport.co.uk/sites/default/files/wslogo_1_0.jpg" width="384" height="141" />
We installed Kubuntu to take a look from a user support point of view and found a bunch of issues.  The main one was krfb not working to share the desktop, this is important as a way to get access to the user's computer.  We didn't look at Kiosk but he considers it important to be able to lock down KDE's settings for companies that want it, I think this should still work but it'll need testing.  Muon was a lovely package manager but no good if it doesn't know about packages which if you install without a network connection is the case until the cron job runs, ug.  QApt seemed to not be doing what it needs to either installing .deb packages or working with the directory share plugin to install samba.  And then he had some features he thought the users would expect like new applications highlighted in the application menu.

Then we had a conference call to talk business.  The important thing about this deal is all profits go to the Kubuntu Council to spend on things to help Kubuntu.  We'll need to set up a Paypal account or other way to pay and a transparent way to split money between us.  And it'll need putting that on the website in some suitably clear marketing talk.  I'm very excited.
