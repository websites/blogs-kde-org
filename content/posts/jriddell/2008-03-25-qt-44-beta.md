---
title:   "Qt 4.4 Beta"
date:    2008-03-25
authors:
  - jriddell
slug:    qt-44-beta
---
With our own beta out the way I got a moment to compile Qt 4.4 beta.  It adds QtWebKit packages for those who missed it in my last 4.4 packages.

https://edge.launchpad.net/~jr/+archive

[As general advice, when adding personal repositories such as these don't run dist-upgrade, only install the software you need else you'll get broken software that I've been playing with.]
<!--break-->
