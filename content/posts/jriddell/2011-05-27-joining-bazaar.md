---
title:   "Joining the Bazaar"
date:    2011-05-27
authors:
  - jriddell
slug:    joining-bazaar
---
I've always been a fan of Bazaar.  A revision control system that lets you work however you want and without needing to be Linus Torvalds to understand it.  So it's a big privilage to be taking a sabbatical from Kubuntu and rotating to work with the Bazaar community.  

We started off with a sprint in London last week and I got myself somewhat familiar with the codebase to bzr (the main library and command line UI), qbzr (the graphical command line plugins which use Qt) and UDD (Ubuntu Distributed Development, which hopes to import all Ubuntu packaging into Bazaar).

So far I've been working on some bugs tagged "easy".  I added a hook to catch exceptions from bzr serve which will be used by Launchpad, I tidied up the error message when making a pointless commit, I added error messages when merging into empty branches and when merging non-existent files.  

I also did some paired programming with John working on allowing bzr uncommit to use deltas rather than whole tree change diffs, this took us into some deep places within bzr and it was impressive watching John write code faster than I could read it.  I also worked on improving exception handing in qbzr with Martin Gz.  Finally I looked into some UDD failures and backported xz to hardy which is needed for importing lzma compressed packages.

I've worked with some new and useful engineering practices.  Every bit of code in bzr has a unit test and such tests are generally written before writing the actual code to fix the issue.  Often unit tests will fail when changing some code and it's an important methodology to prove the programme is as bug free as it can be.  I also got familiar with the patch review process, every change in bzr except typo changes needs a second person to review it and approve it.  Approved changes are sent to PQM which is the Patch Queue Manager which merges in the patch and runs the test suite before pushing it to trunk.  I've also learnt a bunch of Python bits that I wasn't too familar with.

I think the most disappointing area is the lack of API documentation in the Python culture.  Often methods and sometimes whole classes are not documented, and member variables never are.  I can write patches only for a reviewer to say "well foo knows about bar so you don't have to do the extra effort to find the relevant bar" but you would never know this without being very familiar with the code.  Fortunately in Python it's fairly easy to become familiar with the code.

<img src="http://people.canonical.com/~jriddell/bzr-sprint-may-2011-group-photo.jpg" width="400" height="300" />
Bazaar Sprinters High Above London, Jelmer, Martin P, Martin Gz, Jonathan, Max, Andrew, Vincent.
<!--break-->
