---
title:   "Kubuntu Tutorials Day"
date:    2010-06-21
authors:
  - jriddell
slug:    kubuntu-tutorials-day
---
<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay"><img src="https://wiki.kubuntu.org/KubuntuTutorialsDay?action=AttachFile&do=get&target=kubuntu-logo-lucid-tutorials-day.png" width="400" height="157" /></a>

<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a> is back, and this time it's with special guest star speakers.

Incase you missed it in previous years (<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay/Old">check out the logs, there's some interesting sessions in there</a>), Kubuntu Tutorials Day is a few hours of interactive IRC sessions on topics around Kubuntu and KDE development.  If you've always wanted to get started in helping out Kubuntu or programming in Qt this is the perfect opportunity.  

<h2>Guest speakers</h2>

<b>Johan Thelin</b>, the author of best selling book "Foundations of Qt Development" will be talking on <b>Start Coding Qt</b>.  If you're going to code Qt, best start by learning from the best.

<b>Alan Alpert</b> from Nokia will be running a session on <b>Introduction to Qt Quick and QML</b>.  Qt Quick is the exciting new way to program, centred around design rather than code.

Here's the full programme:
18:00UTC, Kubuntu Maverick: An exciting six months ahead, Jonathan Riddell
19:00UTC, Introduction to Qt Quick and QML: The exiting new way of developing applications, Alan Alpert, Nokia
20:00UTC, Packaging and Merging with the Ninjas: How to make and merge .deb packages, Rohan Garg (shadeslayer)
21:00UTC, Start Coding Qt: Learn how to use the world's best GUI toolkit, Johan Thelin (author of Foundations of Qt Development)
22:00UTC, Beastie Hunting: Finding the important bugs amongst the masses, Ralph Janke (txwikinger)
end of talks onwards, Kubuntu Q & A: Ask us anything you want to know about Kubuntu and KDE, Kubuntu Team 

Wednesday 7 July, 18:00UTC, #kubuntu-devel IRC channel on freenode
