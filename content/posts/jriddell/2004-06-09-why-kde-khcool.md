---
title:   "Why KDE is K^HCool"
date:    2004-06-09
authors:
  - jriddell
slug:    why-kde-khcool
---
<p>I did a talk to Edinburgh GNU/Linux Users Group and again to Tayside KGX Users Group this week past.  Discussed how to become a KDE Developer, some nice KDE features and software patents (EU elections tomorrow and this weekend, please vote Europeans).  I have just put up the <a href="http://jriddell.org/programs/kde-talk-06-2004/">quickly put together slides</a>.</p>
