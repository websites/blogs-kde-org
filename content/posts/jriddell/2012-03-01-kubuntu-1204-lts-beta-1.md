---
title:   "Kubuntu 12.04 LTS Beta 1"
date:    2012-03-01
authors:
  - jriddell
slug:    kubuntu-1204-lts-beta-1
---
<img src="https://wiki.kubuntu.org/KubuntuArtwork?action=AttachFile&do=get&target=kubuntu-logo-lucid.png" width="400" height="78" />

This week I've been on Ubuntu release driver duty for Beta 1.  Ubuntu has lots of flavours there days and they all need to be nudged to ensure they get their testing and announcements done in time.  We only had a few hiccups, some of the flavours had to be respun late last night for fixes and do lots of testing today.  Ubuntu (poor under-resourced flavour that it is) also didn't update their upgrade instructions in good time and some grumping at them was needed (sorry, I get grumpy quickly these days with my traumatised brain).  Slashdot <a href="http://linux.slashdot.org/story/12/03/01/2047217/ubuntu-1204-lts-precise-pangolin-beta-1-released">linked to the wrong URL</a> for downloading Ubuntu CDs so I had to put in a quick redirect to point them at the announcement where the correct URLs are.

Here is the <a href="https://wiki.kubuntu.org/PrecisePangolin/Beta1/Kubuntu">Kubuntu Beta 1</a> announcement showing nice features like Telepathy-KDE and a big OwnCloud update.

And for anyone worried about the future of Kubuntu, <a href="http://www.kubuntu.org/news/12.04-lts-announce">Kubuntu 12.04 to be Supported for 5 Years</a> reaffirms that we will be treating 12.04 like any other LTS, only 2 years longer.  It also affirms that we will be continuing Kubuntu in the same way I have run it for the last 7 years, as a successful community made Ubuntu flavour.
<!--break-->
