---
title:   "Kubuntu 8.10 Beta is Out!"
date:    2008-10-02
authors:
  - jriddell
slug:    kubuntu-810-beta-out
---
8.10 beta is out.  This is our big switch to KDE 4 so do test out the upgrade and let us know how it works.  <a href="https://wiki.kubuntu.org/IntrepidIbex/Beta/Kubuntu">Read the announcement</a> or go <a href="https://help.ubuntu.com/community/IntrepidUpgrades/Kubuntu">straight to the upgrade instructions</a>.

You can <a href="https://wiki.kubuntu.org/IntrepidIbex/Beta/Kubuntu/Feedback">leave feedback here</a> or by filing bug reports or poking us on IRC.

<img src="http://www.kubuntu.org/~jriddell/alpha5-compositing-wee.png" width="400" height="250" />
Ooh, compositing by default.
<!--break-->