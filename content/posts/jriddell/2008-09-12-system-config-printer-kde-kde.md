---
title:   "system-config-printer-kde in KDE"
date:    2008-09-12
authors:
  - jriddell
slug:    system-config-printer-kde-kde
---
I added system-config-printer-kde into kdeadmin.  This is a Kubuntu application that saw a very early version in Hardy and is now usable to do various printer settings.  It's always nice to add new applications and fill in gaps in our offering.  Plenty more to do should there exist somewhere out there a free software developer interested in printing :)


