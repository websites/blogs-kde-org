---
title:   "8.04.1 Released, Planet Powers"
date:    2008-07-04
authors:
  - jriddell
slug:    8041-released-planet-powers
---
Two micro blogs while I wait for ibuprofen to stop my wisdom tooth from hurting so much...

<a href="http://releases.ubuntu.com/kubuntu/hardy/">Kubuntu 8.04.1</a> has been released, featuring a large bunch of updates.  

<hr />

I can now edit the planetkde configuration file.  If you're too lazy to e-mail clee, feel free to ping me on IRC.
