---
title:   "Access keys in Konqueror"
date:    2004-10-15
authors:
  - jriddell
slug:    access-keys-konqueror
---
<p>Today I chanced upon another feature where KDE leads, HTML access keys.  <em>a</em> tags can (and should) have accesskey="x" elements.  I used them in <a href="http://yfgm.quaker.org.uk">this website</a> I made last year.  Unfortunatly they are almost completely useless because you don't know they're there, that website has to mark them out with <em>span</em> tags and a stylesheet to underline them.  Then you get the problem in Mozilla that if you make say 'f' as an access key you find your File menu becomes quite inaccessible.  In Konqueror just press Control and the access keys available pop up as wee tooltips.  Then press the key you want.  Clever.</p>

<img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/29f1b37496fbaae7dd8fe11292531971-674.png" />
<!--break-->