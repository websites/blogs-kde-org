---
title:   "NHS Lothian Tech Managers' Ignorance to be Saved by NHS Hack Scotland"
date:    2013-03-14
authors:
  - jriddell
slug:    nhs-lothian-tech-managers-ignorance-be-saved-nhs-hack-scotland
---
There’s been chat that my <a href="http://www.nhshackscotland.org.uk/">NHS Hack Scotland</a> post implies there’s no
point in going to the event, that in light of the NHS Lothian FOI request anything created at the weekend has no chance of being used in the NHS. I wouldn’t be attending (and judging!) if that were true! 
 
The tide of opinion on Free Software in public services in Scotland is turning for the better. In the <a href="http://www.scotland.gov.uk/Resource/0040/00407741.pdf">Scottish
Government’s Digital Strategy</a> they talk about ensuring that "the public sector has an understanding of the advantages of open source material" - there’s a review going on at the moment to turn that into reality. Some organisations might take longer than others to get to this understanding, - in an ideal world they’d all wake up and realise 
that Free Software is awesome and fits perfectly with public services, but at the moment even baby steps are progress.
 
NHS Hack Scotland is at least partially about raising the profile of Open Source software in the NHS, about showing the great things the Open Source community can produce and that it’s not something to be scared of. If it’s a success it means medics and managers will be going back to their health boards all over Scotland and making a noise about the great stuff they made and the friendly, creative people they made it with. 
 
NHS Lothian might not have an Open Source policy but ignorance doesn't mean prohibition, they run plenty copies of Firefox. Now’s the perfect time for them (and other health boards) to get a little push from staff that have seen the light.
<img src="http://www.nhshackscotland.org.uk/files/5613/6249/5890/nhshackscotlandlogo.jpg" width="400" height="475" />