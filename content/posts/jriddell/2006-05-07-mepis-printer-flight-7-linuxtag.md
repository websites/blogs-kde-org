---
title:   "MEPIS Printer, Flight 7, LinuxTag"
date:    2006-05-07
authors:
  - jriddell
slug:    mepis-printer-flight-7-linuxtag
---
In the interests of making sure KDE works with CUPS 1.2 the lovely people at MEPIS (now based on Kubuntu) bought me an HP printer.  Alas it didn't work (foomatic's fault, not KDE) so I guess there's still some issues to be worked out there.

<img src="http://static.flickr.com/51/142122758_73bcdaea54_m.jpg" />

<a href="http://cdimage.ubuntu.com/kubuntu/releases/dapper/flight-7/">Flight 7</a> got released, probably our final testing CD before the release candidate then the release.  Now is the time to test the new live CD installer Ubiquity, unlike the beta it shouldn't randomly wipe your hard disk but do still take a backup.

On Saturday I took a quick trip to Germany for a busy day to visit LinuxTag.  Mark Shuttleworth had invited all the KDE developers to have a meeting with him and the Kubuntu developers, we'll post more soon, probably on the dot.  The stall was excellent, Ken did a great job with the banner and all the German Kubuntu guys did a top job manning the stall (except Mirjam, who was a woman).  Several people said this was a quiet linuxtag compared to past years but the Kubuntu stall was never short of people asking questions.  The official LinuxTag DVD was a Kubuntu Live CD thanks to Amu.  

<img src="http://static.flickr.com/52/142123096_be03ebc1c0_m.jpg" />

The above photo was taken during Mark's talk which cleared the exhibition floor of people.  

<img src="http://static.flickr.com/51/141328334_0ffb1e269c_m.jpg" />

Mark later told me about his talk "<i>I decided then and there not to be a stripper</i>"

I only had time for a quick visit to the KDE stall but it was great to meet a few developers I didn't know of.

<img src="http://static.flickr.com/45/142123739_022ba555dc_m.jpg" />

I'm trying out flickr for <a href="http://www.flickr.com/photos/jriddell/search/tags:linuxtag/">my linuxtag photos</a>, kflickr works really well.

<!--break-->
