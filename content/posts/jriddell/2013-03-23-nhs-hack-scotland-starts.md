---
title:   "NHS Hack Scotland starts"
date:    2013-03-23
authors:
  - jriddell
slug:    nhs-hack-scotland-starts
---
NHS Hack Scotland is starting here in Edinburgh brining coders and medics together for the weekend

<a href="http://www.flickr.com/photos/jriddell/8582532450/" title="DSCF7393 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8092/8582532450_efd69e0230.jpg" width="500" height="375" alt="DSCF7393"></a>
Morning wake up

<a href="http://www.flickr.com/photos/jriddell/8582533254/" title="DSCF7395 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8529/8582533254_5ed65756e5.jpg" width="500" height="375" alt="DSCF7395"></a>
The pitches get written

<a href="http://www.flickr.com/photos/jriddell/8581432965/" title="DSCF7394 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8378/8581432965_cdc7734b8f.jpg" width="500" height="375" alt="DSCF7394"></a>
Introductions
