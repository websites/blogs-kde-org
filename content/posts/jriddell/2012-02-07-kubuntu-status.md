---
title:   "Kubuntu Status"
date:    2012-02-07
authors:
  - jriddell
slug:    kubuntu-status
---
From <a href="https://lists.ubuntu.com/archives/kubuntu-devel/2012-February/005782.html">my kubuntu-devel posting</a>.  See also <a href="https://lists.ubuntu.com/archives/kubuntu-devel/2012-February/005781.html">Jason's posting</a>.

Today I bring the disappointing news that Canonical will no longer be funding my work on Kubuntu after 12.04. Canonical wants to treat Kubuntu in the same way as the other community flavors such as Edubuntu, Lubuntu, and Xubuntu, and support the projects with infrastructure. This is a big challenge to Kubuntu of course and KDE as well.

The practical changes are I won't be able to work on KDE bits in my work time after 12.04 and there won't be paid support for versions after 12.04.  This is a rational business decision, Kubuntu has not been a business success after 7 years of trying, and it is unrealistic to expect it to continue to have financial resources put into it.

I have been trying for the last 7 years to create a distro to show the excellent KDE technology in its best light, and we have a lovely community now built around mostly that vision, but it has not taken over the world commercially and shows no immediate signs of doing so despite awesome successes like the <a href="http://lwn.net/Articles/455972/">world's largest Linux deployment</a>.

The first question to answer is whether the world needs Kubuntu - a regularly released community-friendly distro with a strong KDE focus.  There is no other major distro out there that matches that description but others arguably come close.

If it does then we need people to step up and take the initiative in doing the tasks that are often poorly supported by the community process.  ISO testing, for example, is a long, slow, thankless task, and it is hard to get volunteers for it.  We can look at ways of reducing effort from what we do such as scrapping the alternate CD or automating KDE SC packaging.

I expect to do other desktop team tasks in my work time such as Qt.  I can't do much free software work in my spare time for now because of my poor health (slowly recovering I'm pleased to say).

I hope and expect Kubuntu can continue. I encourage Kubuntu devs to apply to UDS so we can have discussions on how to continue it and keep the dream alive.

Jonathan
<!--break-->
