---
title:   "You Need a Passport to get to the British Isles"
date:    2007-04-20
authors:
  - jriddell
slug:    you-need-passport-get-british-isles
---
A surprisingly large number of people seem to think that being in the EU means you can wander around everywhere without a passport.  This alas is not so with the British Isles.  You can not get in on the strength of one of those pink bits of cardboard with a photo pritt-sticked on top that the Germans call a drivers licence.  Your laminated piece of paper that is an identity card will not suffice.  If you are going to Debconf, Akademy, Guadec, Python UK or any of the other conferences happening on our archipelago this year and you are not coming from the UK or Ireland make sure you sign up for a passport toot sweet.
<!--break-->
