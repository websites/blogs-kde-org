---
title:   "CD Testers Needed"
date:    2009-10-21
authors:
  - jriddell
slug:    cd-testers-needed
---
All hands on deck for the release candidate CD (and DVD and USB and upgrades) testing day.  See the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO tracker</a> for what needs tested (duplicates tests always welcome) and join us in #kubuntu-devel to coordinate.
