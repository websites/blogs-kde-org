---
title:   "Travels with a Brompton in the Cévennes"
date:    2008-12-06
authors:
  - jriddell
slug:    travels-brompton-cévennes
---
Like <a href="http://www.gutenberg.org/etext/535">Robert Louis Stevenson</a> I live in the world's most beautiful city of Edinburgh. Sometimes I wonder if anywhere can match the splendour of my home town so I decided to travel and see what the world had to offer.  

I decided to make use of the wonderful community that is KDE and travel around.  KDE people really are great, you can turn up in almost any city in the world at less than a days notice and find a friendly place to stay with good company and local food to sample.

<b>Paris, Stuck in the Traffic</b>

Took the train to Paris and met Anthony Mercatante.  We went to a Parisian restaurant and he ordered some Parisian food, I said I'd have whatever he was getting and a raw stake turned up.  I don't normally eat meat so this was a bit of a forced introduction into foreign food but it's important to sample the local culture, even if it is raw meat.

We went to a science festival to see a public lecture on bicycling in Paris, bicycling is Paris is an extream sport for people who love adrenaline and risk only.  My French is not as good as it should be for half a childhood of being tought it so I didn't catch all the tips for cycling and remaining alive.  Later I went for a cycle and didn't die.  Paris does have the most crazy traffic jams.  They have 8 way junctions with busses and lorries coming from four sides.  Nobody cares about the traffic lights and the concept of a box junction has clearly never been explained.  We got stuck for a good half an hour with nobody moving anywhere and boxed in by busses.  Overall I think this was a very authentic Paris experience.

<b>Toulouse, La Ville Rose</b>

Took the TGV to Toulouse to stay with Kevin Ottens and his beau.  They took me to an authentic Toulousian family meal and had Pizza d'Escargos and fine wine.

The next day I cycled across Toulouse and discovered a new word, fleuve which means "really big river", living on a small landmass I'm always impressed by the size of rivers abroad.  I found Annma who introduced me to part of her family and Toulousian Canard (duck meets blender in a tin, impressivly unlike any convenicence food you'd get in Scotland).  The next day Kevin showed me around town and I got to see my first authentic French grève where the proletariat were revolting and the bourgoise were in a rather grand palace telling them to eat cake.  We toured the palace and saw lots of naked women, which was socially acceptable because it was art.

In Toulouse I did something I've wanted to do for years and spent most of the time speaking French.  This was absolutely exhausting but I was impressed with myself at being able to do it at all.  Hopefully next time I'll be perfectly fluent.

<a href="http://kubuntu.org/~jriddell/blog/2008-12-toulouse.jpg"><img src="http://kubuntu.org/~jriddell/blog/wee/2008-12-toulouse-wee.jpg"  width="500" height="375" /></a>
Kilt!

<b>Avignon, My Castle is Bigger than Yours</b>

I left to find Avignon and to sample the hospitality of David Faure.  Avignon is the only city I've been to outside Scotland which gets close to Edinburgh for beauty.  The Pope's Palace is very impressive and over the water the King of France wanted his own so they had a competition to build the most spectacular castle.  I did some Open Street Mapping during my cycles around the countryside and put the first cycle route in France onto the map.

David took me to my own private jazz band preformance which was pretty swinging.  The KDAB Avignon office beats the KDAB Toulouse office by having its own private swimming pool but unfortunately it was closed just the week before I arrived.  Oh and Don't tell anyone but I think David is sleeping with his secretary.

<a href="http://kubuntu.org/~jriddell/blog/2008-12-brompton-avignon.jpg"><img src="http://kubuntu.org/~jriddell/blog/wee/2008-12-brompton-avignon-wee.jpg"  width="500" height="375"/></a>
Brompton!

<b>Germany, Mediaeval Marketing</b>

Time to leave KDE behind for a weekend and find a Quaker meeting in Germany for <a href="http://emeyf.quaker.eu.org">EMEYF</a>.  As in England the Quakers in Germany had cause to get away from the authorities in centiries past so they hid in a town called Bad Pyrmont near the middle of nowhere.  We sat in silence as Quakers do and spoke when moved to speak.  Trips to Syria and Ireland were planned.  Then we went to see the town which is absolutely beautiful. Almost as beautiful as Edinburgh.  It's the sort of place you could retire to when you grow old.  We drank the funny water from the spring and climbed a hill in the dark.

But enough silence, I cought a train to Stuttgart to find Frederick of Parley and Lydia of Amarok.  They took me to a mediaeval market where the entire town was done up to look like the 16th century.  We drank mulled wine and I tried some archery.

<a href="http://kubuntu.org/~jriddell/blog/2008-12-fire.jpg"><img src="http://kubuntu.org/~jriddell/blog/wee/2008-12-fire-wee.jpg" /></a>
Fire!

<a href="http://kubuntu.org/~jriddell/blog/2008-12-lydia-frederick.jpg"><img src="http://kubuntu.org/~jriddell/blog/wee/2008-12-lydia-frederick-wee.jpg"  width="500" height="375" /></a>
Cute!

<b>San Francisco, Sharing a Toothbrush</b>

Time to travel further afield, I went to San Francisco and grabbed some floor space from blauzahl.  She works in an office marked Plant Pathology in a building marked "Preserving the rural life".  I think she spends most of her time just triaging KDE bugs.

Then, FOSSCamp!  Canonical's general free software unconference.  Experiences have been shared with KDE bug team talking to Gnome bug team and Oxygen talking to Tango.  Desktop Experience talked about interesting new things which should get announced next week.  KPackagekit was discussed, mobile devices were considered, ideas floated around.

<a href="http://kubuntu.org/~jriddell/blog/2008-12-06-uds-hot-tub.jpg"><img src="http://kubuntu.org/~jriddell/blog/wee/2008-12-06-uds-hot-tub-wee.jpg" width="500" height="375" /></a>
Kubuntu, the only distribution to offer hot tub parties
<!--break-->
