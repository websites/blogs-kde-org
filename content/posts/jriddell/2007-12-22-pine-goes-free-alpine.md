---
title:   "Pine Goes Free with Alpine"
date:    2007-12-22
authors:
  - jriddell
slug:    pine-goes-free-alpine
---
Many a year ago I decided it was worth sacrifising something in the name of freedom and switched from the frustratingly not quite free Pine e-mail app to Mutt.  Mutt is free but has the most insane keybindings and is generally not as slick a user interface as Pine.  Plus it doesn't have the nice feature of keeping your main inbox open while you look at other e-mail boxes, and if your inbox is as large as mine that means a bunch of time lost just for reading a mailing list.  So it was a lovely surprise to see <a href="http://www.washington.edu/alpine/">Alpine, a free Pine</a> from the original authors at Washington University.  So back to integrated editor, sensible keybindings and being able to read mailing lists without spending 10 minutes opening my inbox again.  Yay freedom.
<!--break-->
