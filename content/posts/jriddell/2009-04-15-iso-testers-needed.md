---
title:   "ISO testers needed"
date:    2009-04-15
authors:
  - jriddell
slug:    iso-testers-needed
---
Testers are needed for the proposed CD and DVD ISOs for the Kubuntu 9.04 Release Candidate.  Upgrades from <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu">8.10</a> and <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu/8.04">8.04</a> need testing too.  Report your results on <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO testing</a> and #kubuntu-devel.
