---
title:   "The Start of Something Amazing with KDE 4.0 Release"
date:    2008-01-11
authors:
  - jriddell
slug:    start-something-amazing-kde-40-release
---
Back when I was attending my first KDE conference there was an interesting talk announcing that Qt 4 was in development.  Now 4.5 years later that development has been released to the world in it's full form, KDE 4.0!

This is a .0 release and there's plenty of rough edges but I'm really impressed by how it has come together to be a great usable desktop in the last few weeks.  Plasma and Oxygen have especially improved into something amazing.  But the most important thing is the framework is the best in the world for creating a desktop environment on, that will always be KDE's winning strength.

<a href="http://kubuntu.org/announcements/kde-4.0.php">Kubuntu has packages</a> for 7.10 and Hardy.  There's also a live CD to try out if you're too scared to install.

Thanks to all the KDE contributors. Next week I'll be at the release event in the US.  But in the mean time we already held a massive release party in Edinburgh, see the exclusive <a href="http://kubuntu.org/~jriddell/kde-4.0-release-party/">release party videos</a> for searching interviews, a million quids worth of fireworks and a hundred thousand people celebrating.

It's the start of something amazing.

<img src="http://static.kdenews.org/jr/kde-4.0-banner.png" width="427" height="178" />
<!--break-->
