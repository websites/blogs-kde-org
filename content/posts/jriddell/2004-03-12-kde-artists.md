---
title:   "KDE artists"
date:    2004-03-12
authors:
  - jriddell
slug:    kde-artists
---
KDE artists is an interesting project limited by the number of people who are actually talented enough to create high quality artworks (not me).  There isn't much coherency to the group, people who know what they're doing are often too busy to reply to those who don't and posts to the list can go unanswered.  The website is quite limited too.  Changes to improve this include a <a href="http://wiki.kdenews.org/tiki-index.php?page=KDE+Artists">wiki page</a> and hopefully the bugs.kde.org entries will soon point towards the mailing list.  There's also an IRC channel #kde-artists on freenode which may or may not catch on.  

Everaldo of course is the genius behind Crystal icon theme, unfortunatly he doesn't speak much English or, just as importantly, nobody on kde-artists speaks much Portugese.  However he has said that all SVG files for icons are now in CVS, with Abobe Illustrator files available from ftp.suse.com.  Apparantly only about a quarter of the icons are made from SVG, the rest are just made in Photoshop.

Anybody wanting to tackle any of the <a href="http://bugs.kde.org/buglist.cgi?product=artwork&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">artwork beasties</a> would be welcome I'm sure.  Might make an interesting challenge for a Quality Team newbie.
