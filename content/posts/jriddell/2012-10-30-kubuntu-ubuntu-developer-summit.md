---
title:   "Kubuntu at Ubuntu Developer Summit"
date:    2012-10-30
authors:
  - jriddell
slug:    kubuntu-ubuntu-developer-summit
---
It's UDS time again and we're in sunny Copenhagen.  You can join in by <a href="https://blueprints.launchpad.net/ubuntu/+spec/topic-r-flavor-kubuntu">looking at the sessions</a> and <a href="http://summit.ubuntu.com/uds-r/">look up on the schedule</a> when to join, then you can listen in to the audio and type into the IRC channel which is projected into each room.  The event this time feel bigger than ever using a whole conference venue on the outskirts of town and full of people from Ubuntu and Linaro.

<a href="http://www.flickr.com/photos/jriddell/8138696657/" title="DSCF7126 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8335/8138696657_643c122cc1_z.jpg" width="480" height="640" alt="DSCF7126"></a>
This surreal hotel is like Ikea all over. I'm curious for the design rationale for the windows from the hotel room beds to the toilet.

<a href="http://www.flickr.com/photos/jriddell/8138730934/" title="DSCF7123 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8188/8138730934_51a408abfa.jpg" width="500" height="375" alt="DSCF7123"></a>
A large plenary talk, mine is tomorrow, I hope I think of something interesting to say.

<a href="http://www.flickr.com/photos/jriddell/8138730206/" title="DSCF7124 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8323/8138730206_3d285b9bd8.jpg" width="500" height="375" alt="DSCF7124"></a>
KDE and Kubuntu people enjoy a beer

<a href="http://www.flickr.com/photos/jriddell/8138727606/" title="DSCF7130 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8330/8138727606_217073d4f4.jpg" width="500" height="375" alt="DSCF7130"></a>
A developer session planning the next six months in Kubuntu
<!--break-->
