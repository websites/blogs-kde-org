---
title:   "Between Akademy and Feature Freeze"
date:    2008-08-23
authors:
  - jriddell
slug:    between-akademy-and-feature-freeze
---
Akademy finished, it was great.

Next week is feature freeze in Ubuntu land, so we are working hard on filling the distros with the necessary features, if not always beastie-free.

We got a new Adept in thanks to the excellent mornfall, so now people can install and upgrade and manage their packages again.  Some bits were missing though so I implemented an equivalent of adept_batch for installing packages from a command line with a GUI (used by Amarok's codec installer for example).

<img src="http://kubuntu.org/~jriddell/install-package.png" width="455" height="184" />

I also implemented a notifier to tell you when there are updates

<img src="http://kubuntu.org/~jriddell/update-notifier.png" width="279" height="114" />

This includes a feature called upgrade hooks used when package updates have actions which should happen after the update.

<img src="http://kubuntu.org/~jriddell/update-notifier-hooks.png" width="389" height="311" />

It also keeps an eye on Apport, the Ubuntu crash handler.  Today I turned off drkonqi in KDE apps in favour of Apport as an experiment with our Bugs team to see if they can handle forwarding bugs upstream as necessary.  Unlike drkonqi, Apport uploads to Launchpad which magically fills in the backtrace and does some duplicate testing.

<img src="http://kubuntu.org/~jriddell/update-notifier-apport.png" width="524" height="123" />

KPassivePopup could do with some love though, it's ugly and covers the systray icons.

Google Summer of Code Ended.  I had three students and they all passed, but there's plenty more work to do.  Umbrello got lovely new GraphicsView bling.  The printing dialogue now exists but applications are yet to be talking to it.  Hopefully the students will hang around and continue to work on the projects.

It you want to get a feel of Akademy I took some short video interviews up on <a href="http://radio.kde.org/">KDE://Radio</a>, now including this example of near native Flemmish.

<img src="http://kubuntu.org/~jriddell/ook.png" width="320" height="240" />
<!--break-->
