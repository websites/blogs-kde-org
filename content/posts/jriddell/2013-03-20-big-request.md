---
title:   "Big request"
date:    2013-03-20
authors:
  - jriddell
slug:    big-request
---
Sometimes I get the best e-mails...
<pre>
From: "Przemek (cojack)"
Date: Mon, 11 Mar 2013 09:42:19 +0100
Subject: Big request 
To: jr@jriddell.org
 
Hello, 
I have big request to you, please don't ever stop working on/for Kubuntu.
 
Thanks in advance, 
Best regards, Przemysław Czekaj. 
</pre>