---
title:   "Kubuntu Geek Out"
date:    2013-11-23
authors:
  - jriddell
slug:    kubuntu-geek-out
---
Two of my favourite things in one evening.

<a href="http://www.flickr.com/photos/jriddell/11017120275/" title="DSCF8752 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7393/11017120275_108a87d836.jpg" width="500" height="375" alt="DSCF8752"></a>
Save the Day viewing!  Who's your favourite Doctor?

<a href="http://www.flickr.com/photos/jriddell/11017214946/" title="DSCF8753 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2880/11017214946_69f4194a71.jpg" width="500" height="375" alt="DSCF8753"></a>
Followed by the Munich city Kubuntu rollout viewing, amazing how they have saved so much money but they did it for Freedom.
