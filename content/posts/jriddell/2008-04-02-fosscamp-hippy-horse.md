---
title:   "FOSSCamp, Hippy Horse"
date:    2008-04-02
authors:
  - jriddell
slug:    fosscamp-hippy-horse
---
<a href="http://www.fosscamp.org/">FOSSCamp</a> is happening in Prague next month.  This is a general free software get together with sessions on whatever participants want (it happens just before the Ubuntu Summit but is otherwise unrelated).  A good number of KDE people are expected but more welcome.  <a href="http://stompbox.typepad.com/blog/2008/03/announcing-foss.html">Jorge explains all</a>.

<img src="http://stompbox.typepad.com/blog/images/2008/02/28/fosscamp.jpg" />

<hr />

The <a href="https://wiki.kubuntu.org/HippyHorse/Omega/Kubuntu">Hippy Horse</a> edition yesterday caused much jolity and occational bewilderment.  Wasnae me.

<a href="http://kubuntu.org/~jriddell/tmp/foolsSS.png"><img src="http://kubuntu.org/~jriddell/tmp/foolsSS-wee.png" width="300" height="188" /></a>
<!--break-->
