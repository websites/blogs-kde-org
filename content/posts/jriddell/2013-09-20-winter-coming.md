---
title:   "Winter Is Coming"
date:    2013-09-20
authors:
  - jriddell
slug:    winter-coming
---
Winter is coming so why not snuggle up with Kubuntu? Kubuntu keeps everyone warm.

<img src="http://people.ubuntu.com/~jr/nim-kubuntu.jpg" width="400" height="534" alt="kubuntu jumper" />
