---
title:   "Kubuntu, Pure KGX"
date:    2005-03-18
authors:
  - jriddell
slug:    kubuntu-pure-kgx
---
<p>Today we announced <a href="http://www.kubuntu.org.uk">this little project</a> I've been working on.  Ubuntu has been stealing the lead with the non-business distribution recently so it's time to take it back for KDE.  We are the first distribution with KDE 3.4 and the live CD is a great way to test it out.  Still plenty of work to be done to catch up.  Not all the KDE 3.4 modules are in yet, the artwork still has to be tweaked and all the little applications that make a distribution easy to use (package manager and update watcher, network config etc) need a lot of work.  Please do come and join us, the mailing lists are at <a href="http://lists.ubuntu.com">lists.ubuntu.com</a> and we're on #kubuntu on freenode.  Many thanks go to KDE of course and also the KDE Debian packagers.  Please try out this preview and leave us feedback on <a href="https://www.ubuntulinux.org/wiki/Kubuntu">the wiki page</a>.</p>

<p><img src="http://www.kubuntu.org.uk/konqi.png" class="showonplanet" /></p>
<!--break-->