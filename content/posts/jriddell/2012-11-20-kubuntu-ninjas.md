---
title:   "Kubuntu Ninjas"
date:    2012-11-20
authors:
  - jriddell
slug:    kubuntu-ninjas
---
It's Kubuntu Ninja time!  We have the tars for the next KDE Software Compilation beta and now we need to package them up.  There's an awful lot of them so do come and help.  We're in #kubuntu-devel if you fancy helping, just ask how to get started and hang around until someone is available to help.  Then you could end up like this good looking bunch seen here at the recent Ubuntu Developer Summit in Copenhagen...

<a href="http://www.flickr.com/photos/jriddell/8169377493/" title="DSCF7136 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8349/8169377493_127c320792.jpg" width="500" height="375" alt="DSCF7136"></a>
Rohan (shadeslayer), Aurelien (agateau), Jussi K (Tm_T), Scott K (ScottK), Michał (Quintasan), Steve (SteveRiley), (Michael) mikhas, Alex (afiestas), Jonathan (Riddell)
<!--break-->
