---
title:   "Cool KDE Users"
date:    2013-02-23
authors:
  - jriddell
slug:    cool-kde-users
---
I get nice comments on IRC occationally, here's an especially nice one recently:

<gorgonizer> Can I thank all the Kubuntu Ninjas for their superlative efforts with Raring (amazingly stable for Alpha 2), KDE SC 4.10 and KDE Telepathy..  it is amazingly smooth and stable, and uses a lot less memory than previous releases.. very impressed..

I was watching a film about the Pirate Bay last night on BBC's Storyville, turns out the people who run The Pirate Bay run KDE.

<a href="http://people.ubuntu.com/~jr/tmp/browsers2.png"><img style="-webkit-user-select: none; cursor: -webkit-zoom-in;" src="http://people.ubuntu.com/~jr/tmp/browsers2.png" width="764" height="445"></a>

But most impressive enough was an e-mail from PJ the world's most elite legal geek from Groklaw who e-mailed me saying:

"let me say thank you for KDE.  I am a KDE girl, and I have been for years. So thank you."

KDE: the desktop of choice for elite pirates and legal geeks.
<!--break-->
