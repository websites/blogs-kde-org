---
title:   "12.04 Feature Freeze This Week"
date:    2012-02-15
authors:
  - jriddell
slug:    1204-feature-freeze-week
---
This week is feature freeze in Ubuntu land and the Kubuntu community have been working hard to get in as many as possible before the deadline.

<a href="http://people.canonical.com/~jriddell/blog2/telepathy-kde.png"><img src="http://people.canonical.com/~jriddell/blog2/telepathy-kde-wee.png" width="300" height="419" /></a>
Telepathy-KDE is working nicely.  We're still deciding if it's better to go with the new-but-not-much-tested Telepathy-KDE or the old-but-unmaintained Kopete by default.

<a href="http://people.canonical.com/~jriddell/blog2/lightdm-kde-wee.png"><img src="http://people.canonical.com/~jriddell/blog2/lightdm-kde-wee.png" width="300" height="234" /></a>
LightDM Plasma Theme, not likely to replace KDM yet for lack of testing but a promising way to get a login manager with all the features

<a href="http://people.canonical.com/~jriddell/blog2/pavu.png"><img src="http://people.canonical.com/~jriddell/blog2/pavu-wee.png" width="300" height="208" /></a>
Oxygen-GTK3 theme, so your GTK apps fit in with the Plasma desktop.

<a href="http://people.canonical.com/~jriddell/blog2/krita.png"><img src="http://people.canonical.com/~jriddell/blog2/krita-wee.png" width="300" height="333" /></a>
Calligra office suite featuring Krita the world's best painting app.  Handy for updating hackergotchis.   MS Office file import/export is reported to be better than LibreOffice because of the top work by <a href="http://kogmbh.com/">KO GMBH</a>.  Not the default yet but the signs are good for fixing the "we don't have a KDE office suite by default" bug in the next year.

<a href="http://people.canonical.com/~jriddell/blog2/rekonq-owncloud.png"><img src="http://people.canonical.com/~jriddell/blog2/rekonq-owncloud-wee.png" width="300" height="215" /></a>
Rekonq 0.9 and Owncloud 3.0 are both in progress.

<a href="http://people.canonical.com/~jriddell/blog2/active.png"><img src="http://people.canonical.com/~jriddell/blog2/active-wee.png" width="300" height="225" /></a>
Plasma Active!  Could a Kubuntu Active remix be around the corner?  We'd be the first distro with a free software tablet UI if we can get it done.

If you want to know how my health is doing see my personal blog entry <a href="http://jriddell.org/diary/?p=104">Recovery from Severe Traumatic Head Injury</a>.
<!--break-->
