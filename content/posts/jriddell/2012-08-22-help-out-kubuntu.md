---
title:   "Help out Kubuntu"
date:    2012-08-22
authors:
  - jriddell
slug:    help-out-kubuntu
---
<img src="https://wiki.kubuntu.org/KubuntuArtwork?action=AttachFile&do=get&target=kubuntu-logo-lucid.png" width="400" height="75" />

It's never too late to help out Kubuntu in the cycle
<a href="https://blueprints.launchpad.net/ubuntu/quantal?searchtext=kubuntu">Here's our work items for this cycle</a>.

Some of the easier ones:
track down and update install docs everywhere for having a USB/DVD image but no CD image
review kubuntu active for app selection and good experience
Kubuntu Quantal Docs - carry on where top contributor LittleGirl had to leave off
Update ubiquity to match GTK frontend (the GTK frontend is getting a load of features which will mean the alternate images can go away like LVM partitioning)
ensure OwnCloud juju charm works (we like owncloud here at Kubuntu)
deal with Kubuntu unique strings now we aren't using Launchpad (should be translated separately either in KDE or in Launchpad).

and many more

Join us in #kubuntu-devel on IRC to help out
<!--break-->
