---
title:   "Going to Nigeria"
date:    2009-03-07
authors:
  - jriddell
slug:    going-nigeria
---
A few months ago I got a phone call from Mustapha asking me to come to a conference he was organising in Nigeria.  I get these requests occationally and usually can't make it because I go to far too many conferences as it is but Nigeria sounded fun and exciting.  So now I'm in Abuja waiting for the plane to take me back north to Kano.  I've no idea what to expect.  Nigeria to me means a crowded country with people everywhere, lots of films and books, problems with rubbish and pirates.  I've no idea what people wear or eat or how they get around or how they make a living.  I'll be an ethnic minority for the first time in my life in a place with Sharia law, that's probably not an issue but I wonder if the wealth difference will make me feel
uncomfortable.  I don't know what I'll be talking about or who the audience will be, but it'll be along KDE and Kubuntu lines and there are rumours of some 500 people attending, goodness.  So far I can confirm that it's stonkingly hot here, the saharah desert is indeed made of sand and, excellent news, they use the right sort of power sockets.  The life of an international freedom fighter is an exciting one and spreading it is best of all, I hope I can manage it.
<!--break-->
