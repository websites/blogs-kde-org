---
title:   "FOSDEM, DPI, KDE In London"
date:    2006-03-02
authors:
  - jriddell
slug:    fosdem-dpi-kde-london
---
FOSDEM at the weekend was great, there's something really exciting about having all the different Free Software communities together.  As well as the <a href="http://dot.kde.org/1141040477/">KDE talks</a> I went to see mjg59 talking about ACPI (but sadly failed to catch up with him regarding Kubuntu laptop support), the debian-installer talk,  Automated Display Configuration for X and making SuSE packages.  We sold out of KDE merchandise and gave out 800 Kubuntu CDs.  I even managed to get a bed on the sleeper back home defeating the best efforts of the staff at Waterloo.

The X settings for dots per inch is supposed to ask your monitor how large is it and set DPI accordingly, this means a 10 point font is the same physical size on a 21" monitor and a 14" monitor.  However it never really works like that since very often the monitor returns an incorrect value, or none at all, or you change your monitor, or you just like larger fonts.  And most fonts are optimised to look best at 96 or 120 DPI anyway.  So gnome-settings-daemon sets the DPI (of Xft) to 96, this means KDE apps suddenly change their font size as soon as your start anything which starts gnome-settings daemon.  So for dapper Simon coded into Guidance to set Xft's DPI to 96 or 120 depending on what's best for your monitor.  Problem solved, except for people who previously had 75 DPI in Kubuntu breezy (the fallback default for X) and changed their font sizes accordingly will now end up with fonts that are far too large, sorry folks.

For <a href="http://www.kde-look.org/index.php?xcontentmode=44">KDE Everywhere</a> I took this photo of the K logo with Big Ben, the colour of the sky at the time caused one KDE developer to comment that it looked like an invasion of the Daleks was about to happen.

<img src="http://jasmine.19inch.net/~jr/away/2006-02-27-fosdem/2006-02-26-kde-big-ben.jpg" width="400" height="533" />
<!--break-->
