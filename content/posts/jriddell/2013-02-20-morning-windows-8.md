---
title:   "A Morning with Windows 8"
date:    2013-02-20
authors:
  - jriddell
slug:    morning-windows-8
---
Microsoft have decreed that every new Windows 8 computer has to come with UEFI, a new firmware to replace the old 80s BIOS.  They've also decreed that every new Windows 8 computer has to come with SecureBoot which means it'll only boot an operating system signed by MS.  This is only for security of course, nothing to do with making it harder to make competing operating systems, oh no.  So I had to hunt out a new laptop to test this on which means going to a real physical shop since no website will list which firmware a computer has it being a most uninteresting feature.  My local Sony Centre found me the very last 13" cheapo Vaio laptop in the country to use as a test machine.

So what is Windows 8 like?  After a morning of using it I really can't think of anything nice to say.  It's just full of poor UI and broken experience.

On first bootup it asks important things like to pick a colour you like (seriously) and then for your Microsoft account so it can "authorize" you.  Except after going through the first run settings it still says I need to authorise it to be able to change the wallpaper.

It asks for more details to set up McAfee anti-virus crap, I choose to skip that but an hour later I wonder why the fan is running at full speed and it turns out McAfee is using 50% CPU anyway, whatever for?

Windows 8 has two interfaces and it never can decide which is best.  One is the transitional desktop but it looks blocky and ugly because the theme is designed to be similar to a tablet theme.  One is the Metro tablet UI which is just blocky and ugly and for the sake of simplicity doesn't show you a lot of common UI elements like a busy cursor, so if a programme is taking ages to start up you get no feedback at all, just a blank screen.  

There's an app store so I installed the first free application it offered (a London underground map) but that didn't seem to install so I gave up.  An hour later I got a notification saying it had installed, why does it take an hour to install a simple application?

My router has an interesting feature where you can put a USB drive in it and share files with SMB.  This works great with Kubuntu but despite SMB being Microsoft's very own protocol it can see but not access the device.

I plug in my printer on Kubuntu and 20 seconds later it's working.  In Windows the same thing happens but it takes 5 minutes.  What on earth is it doing for those 5 minutes?

Plug in a projector in Kubuntu Raring and the new kscreen tool does the right thing to bring up a second desktop.  In Windows 8 precicely nothing happens.  You have to do it manually.

The web browser is Internet Explorer and it has Bing shortcuts everywhere.  This is illegal abuse of monopoly and a breach of the EU decision to require MS to offer a choice on first use.  I hope somebody goes to jail.  But not before they stop IE from crashing whenever I try to write a blog critising it.  

<a href="http://www.flickr.com/photos/jriddell/8492592646/" title="DSCF7257 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8235/8492592646_2056ef9278.jpg" width="500" height="375" alt="Internet Explorer has stopped working." title="Internet Explorer has stopped working."></a>
Microsoft you fail.
<!--break-->
