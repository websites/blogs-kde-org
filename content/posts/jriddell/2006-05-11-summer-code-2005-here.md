---
title:   "Summer of Code 2005 is Here!"
date:    2006-05-11
authors:
  - jriddell
slug:    summer-code-2005-here
---
What could DHL be doing delivering me a parcel I wondered early this morning.  Well it seems my Google Summer of Code 205 t-shirt turned up, just in time for me to start looking through the 200 KDE applications and just as many Ubuntu applications for Summer of Code 2006.

<img src="http://kubuntu.org/~jriddell/100_0533.JPG" class="showonplanet" />
<!--break-->
