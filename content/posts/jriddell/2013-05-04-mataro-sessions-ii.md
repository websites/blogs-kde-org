---
title:   "Mataro Sessions II"
date:    2013-05-04
authors:
  - jriddell
slug:    mataro-sessions-ii
---
The Ubuntu Developer Summit should have been this week but got cancelled in order to increase transparency.  But you wouldn't want to let an expensive hotel booking go to waste so there seems to have been a meeting of Canonical engineers this week anyway just without the community. <a href="https://twitter.com/xdatap1/status/329692017691095040">Twitter says some grumpy things</a>.  Well two can play at that game: I've been to every UDS except the <a href="https://wiki.ubuntu.com/MataroConference">Mataro Sessions</a> back in 2004 so this week I'm down the road from Mataro with a dozen KDE and Kubuntu people to discuss what we're all working on.  So far mgraesslin has shown his plans for KWin (including no small part of slagging off Mir and the instability of X in Ubuntu) and now Kevin is talking about the status of KDE Frameworks 5.

<a href="http://www.flickr.com/photos/jriddell/8706248023/" title="13050006 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8133/8706248023_bf2c4448fb_n.jpg" width="320" height="180" alt="13050006"></a>
Sebas makes a good manager

<a href="http://www.flickr.com/photos/jriddell/8706248611/" title="13050007 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8130/8706248611_dfbb238bd6_n.jpg" width="320" height="180" alt="13050007"></a>
Martin draws diagrams of KWin

<a href="http://www.flickr.com/photos/jriddell/8707373200/" title="13050008 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8274/8707373200_21fb1ca527_n.jpg" width="320" height="180" alt="13050008"></a>
Alex takes on the British Empire

<a href="http://www.flickr.com/photos/jriddell/8707373854/" title="13050009 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8260/8707373854_608b4b7cbd_n.jpg" width="320" height="180" alt="13050009"></a>
Lunch time
