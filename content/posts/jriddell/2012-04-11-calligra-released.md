---
title:   "Calligra Released"
date:    2012-04-11
authors:
  - jriddell
slug:    calligra-released
---
<img src="http://people.canonical.com/~jriddell/calligra-logo-transparent-for-light-600.png" width="436" height="282" />
<a href="http://www.calligra.org/news/calligra-2-4-released/">Calligra has been released</a>, KDE's document suite of applications.  It has applications for word processing, spreadsheets, presentations, drawing and much more.  Congratulations Calligra team.  <a href="http://www.kubuntu.org/news/calligra-2.4.0">Install the packages</a> from kubuntu-ppa/backports or direct from 12.04.
