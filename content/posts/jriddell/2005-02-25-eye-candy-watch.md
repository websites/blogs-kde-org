---
title:   "Eye Candy Watch"
date:    2005-02-25
authors:
  - jriddell
slug:    eye-candy-watch
---
Tonight on KDE eye candy watch...

Basse produces a rocking new Konqi image for the logout dialogue:

<img src="http://webcvs.kde.org/*checkout*/kdebase/ksmserver/shutdownkonq.png?rev=1.4" class="showonplanet" />

Then follows it up with this cute little number for the About KDE dialogue:

<img src="http://www.kde.org/stuff/clipart/konqi-official-logo-aboutkde-100x167.png" class="showonplanet" />

And within minutes, yes minutes, is producing this new pose:

<img src="http://www.kde.org/stuff/clipart/konqi-klogo-official-100x125.png" class="showonplanet" />

which can be found in such elite places as <a href="http://ktown.kde.org/~binner/">Binner's home page</a> and on fab's shiny laminated FOSDEM posters.

The more astute of you may have noticed new colours on the about screens and a new revision of the Official KDE Logo (Registered Trademark in Bukina Faso and other countries):

<img src="http://www.kde.org/stuff/clipart/kde-application-info-screens-200x64.png" class="showonplanet" />

Which is based on the new (but not yet final) <a href="http://wiki.kde.org/tiki-index.php?page=Colors">corporate identity guidelines colours</a>.  And speaking of those colours (not just blue contrary to rumour) you may have noticed the new <a href="http://www.kde.org">www.kde.org</a> from cullmann which makes suble use of CSS2's little implemented text-shadow property (unlike <a href="http://www.ubuntu.com">ubuntu.com</a> which overuses it to crimial extents, bless their hearts those who use browsers that don't support the properties they add to their CSS).

Finally we have a decision on the KDE 3.4 splashscreen:

<img src="http://www.kde.org/stuff/clipart/splashscreen-3.4-400x56.png" class="showonplanet" />

Praise and moans to kde-artists mailing list and the newly active #kde-artists IRC channel.
<!--break-->