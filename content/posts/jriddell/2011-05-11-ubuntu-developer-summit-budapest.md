---
title:   "Ubuntu Developer Summit in Budapest"
date:    2011-05-11
authors:
  - jriddell
slug:    ubuntu-developer-summit-budapest
---
This cycle's Ubuntu Developer Summit is in Budapest.  

The Qt 5 summit was rather exciting and unexpected.  The good news is it's not a big Qt 3 -> 4 style transition, it will remain source compatible, it's simply a break in ABI to tidy things up and allow for modularisation.

<a href="http://wstaw.org/m/2011/05/11/plasma-desktopgp1838.jpg">Amusing screenshots of Plasma done up to look like Unity aside</a> one should never take microblogs seriously, Kubuntu is using Plasma and always will along with the best of KDE Software (e.g. we're continuing to use Rekonq). The team has been looking at Plasma Active, the inititive to take KDE software to a wide range of consumer devices and will be working on a Kubuntu Active to bring it to the world.

I'm enjoying 

<img src="http://people.canonical.com/~jriddell/uds/DSCF5621.JPG" width="350" height="263" />
The Grand Ballroom

<img src="http://people.canonical.com/~jriddell/uds/DSCF5625.JPG" width="350" height="263" />
The KDE and Qt people at UDS (Richard Dale has also been spotted)

<img src="http://people.canonical.com/~jriddell/uds/DSCF5629.JPG" width="350" height="263" />
The Linaro showcase evening demoed QtWebKit and KWin on ARM

<img src="http://people.canonical.com/~jriddell/uds/DSCF5630.JPG" width="350" height="467" />
Weirdest thing, this hotel in Budapest won the Robert Burns International Foundation award, no idea why.

<img src="http://people.canonical.com/~jriddell/uds/DSCF5631.JPG" width="350" height="263" />
Alex Fiestas in a heated debate during a session.

You can join in, see the <a href="http://summit.ubuntu.com/uds-o/">UDS Schedule</a> for what's happening and see the <a href="http://uds.ubuntu.com/participate/remote/">remote participation page</a>.
<!--break-->
