---
title:   "Medibuntu to Disappear, libdvdcss now direct from VideoLan"
date:    2013-09-11
authors:
  - jriddell
slug:    medibuntu-disappear-libdvdcss-now-direct-videolan
---
Medibuntu the archive with libdvdcss and a number of other package which couldn't be put into the official Ubuntu archive is to disappear.  Gauvain Pocentek who has been maintaining it for some time is wanting to move himself and his server on.  At Blue Systems and in Kubuntu we looked over the packages in the archive and realised they were all obsolete or unnessecary because most are now in the official Ubuntu archive or have better equivalents which are.  The main exception is libdvdcss which is illegal in the US so can't go in the archive.  But fear not I've been working with the developers of libdvdcss at VideoLan to set up an archive directly at VideoLan.  It works with either Ubuntu or Debian of pretty much any release

See the <a href="http://www.videolan.org/developers/libdvdcss.html">libdvdcss page on videolan.org</a>.

I'm doing a stable release update for <a href="https://bugs.launchpad.net/ubuntu/+source/libdvdread/+bug/1223928">libdvdread's install-css.sh</a> script for versions of Ubuntu supported on the desktop.

If you spot any documentation that suggests people use Medibuntu please update and point them to videolan for libdvdcss.
