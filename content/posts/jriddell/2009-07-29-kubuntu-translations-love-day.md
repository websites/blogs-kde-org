---
title:   "Kubuntu Translations Love Day"
date:    2009-07-29
authors:
  - jriddell
slug:    kubuntu-translations-love-day
---
Ubuntu's new translations coordinator David Planella is holding a <a href="https://lists.ubuntu.com/archives/kubuntu-devel/2009-July/003002.html">Kubuntu Translations Love Day</a> in #kubuntu-devel tomorrow (Wed 29th).  Do drop by at any time and let us know all your i18n problems.
