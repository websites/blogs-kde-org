---
title:   "9/10/10"
date:    2010-10-09
authors:
  - jriddell
slug:    91010
---
It's the nicht afore release day.  Announcements need to be tidied up, upgrades need final testing, release notes need completing, web pages readied.  I'm in for a long evening.  All going well, tomorrow morning you should find the internet successfully choked as everyone downloads 10.10.  It's going to be exciting.  Join #ubuntu-release-party to get in the mood.

<img src="http://people.canonical.com/~jriddell/1day.png" width="187" height="151" />
<!--break-->
