---
title:   "Akademy is Here!"
date:    2010-07-02
authors:
  - jriddell
slug:    akademy-here
---
Akademy started with a pre-conference welcome drinks evening today.  Lots of KDE developers old and new.  

There is a <a href="http://www.flickr.com/groups/akademy2010/">Flickr group for Akademy 2010</a> but it seems I can only post 6 photos to it, which is a strange restriction.

<img src="http://farm5.static.flickr.com/4101/4755272497_a0f80f7c75.jpg" width="500" height="375" />
Lots of Nokia people here

<img src="http://farm5.static.flickr.com/4077/4755272487_1f037e114a.jpg" width="500" height="375" />
Chit chat

<img src="http://farm5.static.flickr.com/4101/4755272499_6ff36c4f34.jpg" width="500" height="375" />
Someone thoughtlessly organised a sports tournament during Akademy

<img src="http://farm5.static.flickr.com/4135/4755933548_fa4a714426.jpg" width="500" height="375" />
Free beer doesn't stop people from hacking

Finland turns out to be lovely, full of trees and liquorice and cheese eaten with jam.  The only bad thing is the mosquitoes which rather take the fun out of a post-sauna sun bathe by eating you alive.

<img src="http://farm5.static.flickr.com/4123/4755272485_6111c84bff.jpg" width="500" height="375" />
Tm_T asked for this one
<!--break-->
