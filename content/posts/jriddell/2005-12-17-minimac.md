---
title:   "minimac"
date:    2005-12-17
authors:
  - jriddell
slug:    minimac
---
Having released a few too many Kubuntu flight CDs without testing the powerpc builds I bought a minimac.  I did intially think I would keep MacOS X on it since it's always useful and interesting to be able to play with another operating system.  But by the time the Kubuntu powerpc CD had downloaded I'd had enough of it and wiped the whole hard disk.  The graphics in macOS X are of course lovely, and the search engine spotlight is just right, but this is also an operating system that is unable to minimise or maximise windows properly and that doesn't even have an applications menu so you can find what programs are installed.  Further problems:

<ul>
<li>No way to set mimetypes that I can find, I want .doc files associated with Pages not with the pre-installed version of MS Office which comes up with "your trial edition of MS Office expired in November 2004, would you like to buy it?"</li>
<li>Half the software needs upgraded on first run, I upgraded iTunes and after installation is requires a reboot, very Windows 95</li>
<li>fink doesn't seem to have any significant software in their archives any more</li>
<li>neooffice is an impressive attempt to port OpenOffice to native MacOSX.  That means without an X server but there's no native widgets and it's OpenOffice 1 not OpenOffice 2.</li>
<li>Funky music making program garageband is completely broken, gives a "code 0" error on startup and won't let you exit.</li>
<li>safari has no statusbar which means you don't know where a link is going to</li>
<li>You can alt-tab between applications but not between windows in an application</li>
<li>Kubuntu has a bug where some of the KControl modules don't fit on a 1024 screen so you have to alt-drag the window around.  MacOS has the exact same bug in some of it's system settings modules and applications, except there's no way to drag the windows around.  I guess all the Mac developers have high resolution widescreen monitors.</li>
</ul>

<!--break-->