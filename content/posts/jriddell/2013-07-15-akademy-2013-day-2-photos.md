---
title:   "Akademy 2013 Day 2 in Photos"
date:    2013-07-15
authors:
  - jriddell
slug:    akademy-2013-day-2-photos
---
<a href="http://www.flickr.com/photos/jriddell/9288739810/" title="13070024 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5504/9288739810_26318136ae.jpg" width="500" height="282" alt="13070024"></a>
Lydia tells us how to talk to each other

<a href="http://www.flickr.com/photos/jriddell/9285962265/" title="13070030 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3682/9285962265_72e5613198.jpg" width="500" height="282" alt="13070030"></a>
An audience waits to see which of them will win an Akademy Award

<a href="http://www.flickr.com/photos/jriddell/9288739558/" title="13070027 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7369/9288739558_bc52b1076b.jpg" width="500" height="282" alt="13070027"></a>
Vishesh wins an Akademy Award!

<a href="http://www.flickr.com/photos/jriddell/9285961795/" title="13070031 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7361/9285961969_44b77074e5.jpg" width="500" height="282" alt="13070031"></a>
The organising team get a round of applause, <a href="http://www.flickr.com/photos/jriddell/9285961795/">see the video</a>.

<a href="http://www.flickr.com/photos/jriddell/9285982593/" title="DSCF8040 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2861/9285982593_fba6258088.jpg" width="500" height="375" alt="DSCF8040"></a>
Akademy on the water

<a href="http://www.flickr.com/photos/jriddell/9285981971/" title="DSCF8042 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2854/9285981971_94bd45e607.jpg" width="500" height="375" alt="DSCF8042"></a>
Seeing the sights by water

<a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/" title="ak2013-group-photo by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5459/9288772280_2e1c20f25d.jpg" width="500" height="190" alt="ak2013-group-photo"></a>
<a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/">2013 Akademy Group Photo</a>, please add yourself to the <a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/">tagged version</a>

<a href="http://community.kde.org/Akademy/2013/Photos">More photos listed on the wiki</a>