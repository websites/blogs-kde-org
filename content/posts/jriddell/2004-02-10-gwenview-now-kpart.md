---
title:   "Gwenview now with KPart"
date:    2004-02-10
authors:
  - jriddell
slug:    gwenview-now-kpart
---
Annoyed at the lack of directory image browser for Konqueror and the primitive image viewer I turned Gwenview into a Konqueror KPart.  You can get it from kdeextragear-1 CVS, all comments are welcome.

In my very humble opinion this is much nicer than the existing Konqueror image part which doesn't fit-to-window for example that it should be a part of the main KDE install or at least kdegraphics.  Also the existing image browsers in kdegraphics seem to be past their best.

