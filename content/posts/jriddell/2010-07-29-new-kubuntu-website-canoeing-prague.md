---
title:   "New Kubuntu Website, Canoeing in Prague"
date:    2010-07-29
authors:
  - jriddell
slug:    new-kubuntu-website-canoeing-prague
---
After many months <a href="http://www.kubuntu.org">kubuntu.org</a> got a new look, complete with new logo.  Many thanks to Ofir for his patience in seeing this through.

<hr />

During the Platform sprint in Prague I took Aurelien Gateau, doko and a couple of nice chaps from SuSE Prague canoeing on the awesome whitewater course near the city centre.<br />

<img src="http://farm5.static.flickr.com/4146/4841174254_38a1a99464.jpg" width="500" height="375" /><br />
Looking confident at the top<br />

<img src="http://www.flickr.com/photos/jriddell/4841174536/" width="500" height="375" /><br />
This flatwater is easy<br />

<img src="http://farm5.static.flickr.com/4085/4840563081_58e496265b.jpg" width="500" height="375" /><br />
King of the wave<br />

<img src="http://farm5.static.flickr.com/4107/4841174690_b16b2e2027.jpg" width="500" height="375" /><br />
This blury photo is the last anyone has seen of Aurelien, if you live downstream of Prauge please look out for him in his blue canoe<br />
<!--break-->
