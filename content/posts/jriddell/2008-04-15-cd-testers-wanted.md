---
title:   "CD Testers Wanted"
date:    2008-04-15
authors:
  - jriddell
slug:    cd-testers-wanted
---
Kubuntu and variants Release Candidate is due out this week and next week is the final thing, so its solid install testing until then.  We need people to download the daily and daily-live CDs and install them.  Upgrades and netboots also need testing.  See the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO Tracker</a> for what needs doing, the <a href="https://wiki.kubuntu.org/Testing/Cases">test case</a> for the all new KDE 4 and the old KDE 3 procedure and join us in #kubuntu-devel and #ubuntu-iso for coordination.
<!--break-->
