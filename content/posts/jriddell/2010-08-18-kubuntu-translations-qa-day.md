---
title:   "Kubuntu Translations QA Day"
date:    2010-08-18
authors:
  - jriddell
slug:    kubuntu-translations-qa-day
---
This Friday (20th August) me and Ubuntu translations man David Planella want to spend the day making sure translations in Kubuntu are in tip top shape.  Do join us on IRC in #kubuntu-devel if you want to help out.  See the <a href="https://wiki.kubuntu.org/Kubuntu/TranslationsDay">wiki page</a> for some things we will be checking.
