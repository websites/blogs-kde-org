---
title:   "Why have you stuck with Kubuntu?"
date:    2012-09-03
authors:
  - jriddell
slug:    why-have-you-stuck-kubuntu
---
<img src="https://wiki.kubuntu.org/KubuntuArtwork?action=AttachFile&do=get&target=kubuntu-logo-lucid.png" width="400" height="75" />

A <a href="http://www.kubuntuforums.net/showthread.php?59611-Why-have-you-stuck-with-Kubuntu">fun thread</a> on independent website <a href="http://www.kubuntuforums.net">kubuntuforums.net</a> asking why use Kubuntu rather than the multitude of competition.

Some answers:

<i>
(1) I prefer the KDE look
(2) The friendly community
</i>

<i>
I just like KDE and Kubuntu seams to give the best KDE I have tried 
</i>

<i>I use it because I like Ubuntu, but but want customization options. Ive tried a lot of other DEs but none is easier to use and more customizable than KDE.</i>

<i> the community rocks</i>

<i>I use Kubuntu because it's Ubuntu, but with the awesomeness of KDE. So the familiar technology, coupled with the superior DE means I have never been happier with Linux on my desktop.</i>

<i>I love its pure, unmitigated, raw POWER! It allows me to do more with fewer keystrokes or mouse clicks than ANY other OS available. Dolphin is just an example. </i>

<i>Kubuntu offers a compassionate community, the excellence of KDE and the breadth of the Ubuntu ecosystem. </i>

<i>Kubuntu has a good and familiar look (for those of us coming from Windows), a good base in Ubuntu, and the ease in which to add software and repositories. I also find it cool that both KDE and (K)Ubuntu have 6-month release cycles, meaning a new version of Kubuntu means a new KDE (though a couple months off). </i>

Lovely.
<!--break-->
