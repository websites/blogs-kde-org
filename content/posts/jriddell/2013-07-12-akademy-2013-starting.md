---
title:   "Akademy 2013 Starting"
date:    2013-07-12
authors:
  - jriddell
slug:    akademy-2013-starting
---
KDE Meeting in Bilbao!

<a href="http://www.flickr.com/photos/jriddell/9272621646/" title="DSCF8017 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7327/9272621646_d8f191a690.jpg" width="500" height="375" alt="DSCF8017"></a>
The KDE e.V. AGM, fewer people than normal, only about 50 people (~1/4 of the membership) due to it being held a day early.

<a href="http://www.flickr.com/photos/jriddell/9272621092/" title="DSCF8024 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3735/9272621092_f89705d1ce.jpg" width="500" height="375" alt="DSCF8024"></a>
Lunch time of wine, beer and Crudités on the terrace infront of the Guggenhein Museum, possibly the most classy Akademy ever.

<a href="http://www.flickr.com/photos/jriddell/9269835701/" title="DSCF8026 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3801/9269835701_1fa60d0380.jpg" width="500" height="375" alt="DSCF8026"></a>
While the votes were being counted on the new KDE e.V. board member Phillip (elite Kubuntu developer) and Vishesh (elite friend of Kubuntu) sipped Cava and ate cake.  KDE is awesome.

<a href="http://www.flickr.com/photos/jriddell/9272619924/" title="DSCF8028 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2829/9272619924_7371cf4de1.jpg" width="500" height="375" alt="DSCF8028"></a>
The pre-Akademy party included live Jazz music.

<a href="http://www.flickr.com/photos/jriddell/9269834863/" title="DSCF8032 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3803/9269834863_f3f6a4e432.jpg" width="500" height="375" alt="DSCF8032"></a>
And more Crudités, why truely you are spoiling us no?

It's hot and fun here in Bilbao.

<img src="http://people.ubuntu.com/~jr/akademy/akademy-2013.png" width="400" height="178" />

