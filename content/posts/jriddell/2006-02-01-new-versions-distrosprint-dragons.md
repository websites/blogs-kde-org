---
title:   "New versions, Distrosprint, Dragons"
date:    2006-02-01
authors:
  - jriddell
slug:    new-versions-distrosprint-dragons
---
Both KDE and KOffice decided to release new versions today.  <a href="http://kubuntu.org">Kubuntu</a> has packages for both.  <a href="http://konversation.kde.org">Konversation</a> released a new version which was going to be in time for Kubuntu dapper but they delayed to get some last minute bugs out, the good news is that we got an upstream version freeze exception and the new Konversation is now it.  It's nice to have apps start shaping their release schedules around Kubuntu, there really is something in this regular release idea.

The Ubuntu distrosprint is in full swing in London, no agenda, no BoFs, just hard core hacking.  Porting kpdf to poppler will make security updates a lot easier but didn't get us in the good books of the kpdf developers who have fixes in their own xpdf fork which the poppler developers don't want because it would derivate too much from xpdf.

The #quaker geek mafia met in Southampton at the weekend to warm up flurble's house.  We went hunting for Drakes but only found this Chinese species instead.

<img src="http://jriddell.org/photos/2006-01-30-flurbleparty-paul-dragon.jpg" width="400" height="300" class="showonplanet" />
<!--break-->
