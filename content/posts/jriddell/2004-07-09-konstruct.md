---
title:   "Konstruct"
date:    2004-07-09
authors:
  - jriddell
slug:    konstruct
---
No Debian CVS packages, my usual way of staying up to date, have turned up for KDE 3.3 so I turned to Konstruct for compiling beta 1 and it's very nice.  It's even nicer after adding an extra 512Megs of memory.

It did take a night and a day to compile, so this is what being a Gentoo user is like.

KDE 3.3 doesn't seem to be a massive improvement over 3.2, I'm not sure what should go on expo flyers, but I do like the new alt-tab behaviour.

What happened to the new Control Centre, wasn't that one of the objects of KDE 3.3?
