---
title:   "Qt 4.7 Beta in experimental PPA"
date:    2010-05-22
authors:
  - jriddell
slug:    qt-47-beta-experimental-ppa
---
If you want to try out Qt 4.7 and the interesting new world of Qt Quick ("from now on /the/ way to make GUIs"), Kubuntu 10.04 users can try it from the <a href="https://edge.launchpad.net/~kubuntu-ppa/+archive/experimental">experimental PPA</a>.  The Qt Creator packages there have Qt Quick designer integration.
