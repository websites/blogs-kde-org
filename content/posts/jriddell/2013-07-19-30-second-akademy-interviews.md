---
title:   "30 Second Akademy Interviews"
date:    2013-07-19
authors:
  - jriddell
slug:    30-second-akademy-interviews
---
Who is KDE?  I did some <a href="http://www.flickr.com/photos/jriddell/sets/72157634699856818/">30 second interviews</a> on the booze cruise last night to give you a flavour.

<a href="http://www.flickr.com/photos/jriddell/sets/72157634699856818/">
<img src="http://people.ubuntu.com/~jr/flickr-interviews.png" alt="30 Second KDE Akademy Interviews" width="800" height="349" />
</a>
