---
title:   "Akademy Day 0 Photo Blog"
date:    2014-09-06
authors:
  - jriddell
slug:    akademy-day-0-photo-blog
---
<a href="https://www.flickr.com/photos/jriddell/15151000825" title="DSC 0685 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5586/15151000825_b84ed1853c.jpg" width="500" height="281" alt="DSC 0685"></a>
Welcome to Czech, it has beautiful sunsets

<a href="https://www.flickr.com/photos/jriddell/14964285889" title="DSC 0689 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3888/14964285889_cd50dab9d2.jpg" width="281" height="500" alt="DSC 0689"></a>
Welcome to Akademy at the Brno University of Technology

<a href="https://www.flickr.com/photos/jriddell/14964284989" title="DSC 0688 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5566/14964284989_1c72a61862.jpg" width="500" height="281" alt="DSC 0688"></a>
Gladhorn whips the KDE board candidates into shape

<a href="https://www.flickr.com/photos/jriddell/15127975756" title="DSC 0694 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5557/15127975756_04f45f867c.jpg" width="281" height="500" alt="DSC 0694"></a>
Red Hat shows us their new office building as they serve us beer

<a href="https://www.flickr.com/photos/jriddell/15151016595" title="BwybLw0CUAAz0oM.jpg:large by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5560/15151016595_3f332e68e6.jpg" width="500" height="286" alt="BwybLw0CUAAz0oM.jpg:large"></a>
KDE for Yes
<!--break-->
