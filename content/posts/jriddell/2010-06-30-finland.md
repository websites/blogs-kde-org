---
title:   "Finland!"
date:    2010-06-30
authors:
  - jriddell
slug:    finland
---
Finland, Finland, Finland,
The country where I want to be,
Pony trekking or camping,
Or just watching TV.
Finland, Finland, Finland.
It's the country for me.

<img src="http://ivan.fomentgroup.org/blog/wp-content/uploads/2010/06/igta2010.png" width="380" height="200" />

as in previous years we are proud to sponsor

<img src="http://akademy.kde.org/sites/akademy.kde.org/files/images/canonical_med.png" width="240" height="30" />

Me and Aurelien Gateau and Michael Casadevall will be coming from Canonical.  It's going to be great!
<!--break-->
