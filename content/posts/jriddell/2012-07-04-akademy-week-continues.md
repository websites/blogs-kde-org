---
title:   "Akademy Week Continues"
date:    2012-07-04
authors:
  - jriddell
slug:    akademy-week-continues
---
<img src="http://starsky.19inch.net/~jr/akademy-2012.png" width="400" height="178" />

A whole week of KDE goodness here at the IT College in Tallinn.

Favourite talks:
<b>Jeroen van Meeuwen</b> gave a talk about bug management, something we're often not very good at in free software due to lack of manpower.  I'd love to have a quick and convenient way of marking bugs for the next kubuntu release for example, and what's quick and convenient for me posting it via IRC, we recently got a bot who could do this but it can't mark it for the next release due to daft security issues, grr.

<b>KDE Frameworks 5 for Application Developers</b> by David Faure looked at some of the API changes coming.  KDElibs if becoming KDE Frameworks (and there's no KDE 5, it's all going to be more modularised).  Many of the KDE classes are going away having been moved into Qt and there's a bunch of the frameworks which don't depend on other bits of KDE so apps can easily pick them up without fearing about adding large dependencies e.g. a gettext based translation framework.  

<b>A New Hope: Open KDE Devices</b> Aaron spoke about his Vivaldi tablet.  He made the very valid point that KDE can't rely on third party companies to get round to shipping KDE devices for us, so it's up to us the community to do that.  He had most things working in the Vivaldi tablet but then the manufacturer sent a new model which broke various things so he has had problems getting it working again.  More Linux engineers needed in KDE.

<b>The more we do the more there is to be done</b> by Nuno was inspirational because he's such a productive designer and the artwork is a real high point of KDE.  

<b>Qt Project Achievements and How KDE Is Helping</b> by Thiago made what I think will become an increasingly fashionable suggestion of merging the Qt and KDE communities.  We're both open source communities now and as David Faure spoke before much of KDE Frameworks is going into Qt.  Maybe we'll get to the stage where KDE Frameworks is just addons to Qt (like Phonon is now) and KDE is just the applications that use Qt.

<b>Lightning Talks</b> I gave one on Archive Admin top 10 Rejection Reasons. Don't forget your COPYING file or it's not going in!

<b>Will Schroeder</b> from KitWare (of CMake fame) <a href="http://blog.jospoortvliet.com/2012/07/keynote-about-open-science.html">spoke about science</a> and the problems that copyright and patent restrictions cause on it.  He said we should be selfish and share because that allows you to go after the majority of the software market.

The <b>Open Innovarion Network</b> were here for a couple of talks.  Raffi Gostanian encouraged us to document any good idea we had, anything that keeps us excited at night after thinking it up.  OIN would then publish that and it would be a resource for patent examiners to look for state of the art ideas so they can't be patented by anyone else.

<b>Peter Grasch</b> spoke about his accessibility projects with robots.  He has recently moved Simon into KDE and said it is something everyone should do immediately because of the community help that KDE gives the project.

In the <b>Sponsor presentations</b> I gave a quick talk about how I jumped ship from my old employer because there were so many people wanting and needing Kubuntu to continue.  KDE is amazing, the world needs it.

<a href="http://www.flickr.com/photos/jriddell/7492837444/" title="DSCF7022 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8004/7492837444_ae7918525a.jpg" width="500" height="375" alt="DSCF7022"></a>
A busy weekend of talks

<a href="http://www.flickr.com/photos/jriddell/7492837256/" title="DSCF7027 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7270/7492837256_5bf14aa3b7.jpg" width="500" height="375" alt="DSCF7027"></a>
The Akademy Award winners and organising team
<!--break-->
