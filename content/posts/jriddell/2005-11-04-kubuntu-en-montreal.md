---
title:   "Kubuntu en Montreal"
date:    2005-11-04
authors:
  - jriddell
slug:    kubuntu-en-montreal
---
This week Kubuntu is in Montereal for the Ubuntu Below Zero summit.  This is not a conference with lots of people giving talks, although we do have talks each morning and lightning talks in the evening, I <a href="https://wiki.ubuntu.com/UbuntuBelowZero/LightningTalks">took some notes</a> in my usual style and there are videos on that page too.  This conference is about specs.  <a href="https://launchpad.net/distros/ubuntu/+specs">Lots of specs</a>.  We sit in small groups for an hour at a time and discuss the specification then go and write it up.  This is not the usual way to do free software development and may seem somewhat harsh to outsiders but it works really well at defining what we are going to do for the next 6 months.  <a href="https://launchpad.net/people/jr/+specs">My specs page</a> has most of the Kubuntu ones.

FreeNX Fabian is also here and has managed to port FreeNX to X.org 7.  Which is incredible.  Maybe we will see FreeNX packages in Dapper.

In Montreal the toilets play music, the people know if you speak French or English by just looking at you and the waiters don't let you leave the restaurant until you have paid the mandatory 15% tip.  

Some of Kubuntu Below Zero team:

<img src="http://jriddell.org/photos/wee/2005-11-01-ubz-jonathan-stephan.jpg" />
<!--break-->
I need a haircut.
