---
title:   "Message Indicator in KDE"
date:    2009-08-12
authors:
  - jriddell
slug:    message-indicator-kde
---
Recently Aurelien Gateau of the Canonical Desktop Experience team implemented the Message Indicator for KDE and Konversation.  Now if you get messages when you're away from your computer or not looking at IRC it'll put them into the message indicator when you can happily not get distracted by them (unlike popup notifications) but can easily find them when you want to.

<img src="http://people.ubuntu.com/~jr/message-indicator-none.png" width="128" height="76" />
Nobody has pinged me, I can get on with something else

<img src="http://people.ubuntu.com/~jr/message-indicator-ping.png" width="110" height="71" />
Someone has mentioned my name, but I'm busy, I can easily ignore it.

<img src="http://people.ubuntu.com/~jr/message-indicator-message.png" width="290" height="270" />
When I want to know who's been trying to get hold of me I have this handy list, clicking on a name will take you to the channel or query window.

It's all a vast improvement on the popup notifications in my opinion, but feedback is welcome.  Hopefully the applet can go into kdeplasa-addons when it's ready.  Other apps need patches, KMail, Kopete and Quassel seem like obvious candidates, talk to Aurelien if that seems interesting.
<!--break-->
