---
title:   "In Berlin"
date:    2009-02-05
authors:
  - jriddell
slug:    berlin
---
Germany, land of Kinder eggs, regional sausage varieties and underground spaceship themed hacklab-bar community collectives.  I'm in Berlin for the Canonical Platform Team (previously called Canonical Distro Team) week long sprint where we have taken over a floor of the very large East Berlin Holiday Inn. East Berlin is a cubist painter's delight, all the buildings are large and cube shaped.  60 geeks on a 3MBit ADSL line is painful, especially downloading CD images to test alpha releases, the pain is mostly over now though and the alpha should be out in the next hour or so.

I had a meeting with the security team about how to support Amarok needing MySQL 5.1 while Ubuntu Server people want 5.0, we came to a reasonable compromise of including MySQL inside the Amarok source package.  Not pretty, but it keeps everyone happy.

The new desktop experience team have been looking at KDE and I went over how to start on the message indicator in a Plasma happy way with team lead David.  Mark is super keen to make sure the work they're doing on desktop experience happens in KDE as well as Gnome, which I think is a great thing, and full time KDE/Qt talent should be on their team soon.  The removing actions idea is disputed but that is only one part of their notifications work.

At the end of our hard days hacking Canonical types have been enjoying some fun around Berlin.  On Tuesday we went go-karting, I had an excellent practice run but during the big race I was smashed into by one Sebastien Bacher.  With knee dripping with blood I was out of the race but my team mates put in a sterling performance to come in second.

Last night we went to c-base, a space ship themed community hacklab/bar and I chatted with some people from the Ubuntu-berlin LoCo.  Nice to meet a Kubuntu user who is as interested in canoeing as I am.

<a href="http://kubuntu.org/~jriddell/medals.jpg"><img src="http://kubuntu.org/~jriddell/medals-wee.jpg" width="300" height="225" /></a>
Ooh, shiny medals.
<!--break-->
