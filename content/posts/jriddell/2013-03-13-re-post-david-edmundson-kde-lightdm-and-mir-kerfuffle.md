---
title:   "Re-post: David Edmundson KDE, LightDM and the Mir Kerfuffle "
date:    2013-03-13
authors:
  - jriddell
slug:    re-post-david-edmundson-kde-lightdm-and-mir-kerfuffle
---
<pre>A re-post of <a href="http://www.sharpley.org.uk/blog/lightdm-mir-wayland">David Edmundson's blog KDE, LightDM and the Mir Kerfuffle</a> for Planet Ubuntu</pre>

<p>With Canonical's decision to make a new display server, there's been some questions as to how this affects LightDM and the KDE front end I've spent a long time working towards.</p>
<p>It's a perfectly sensible question, LightDM has heavy Canonical sponsorship, and a display server needs to be supported in the display manager.</p>
<p>Canonical (and Ubuntu) have decided not to adopt Wayland as their new display server, but a new in-house system called Mir. We in KDE have already made the decision that Wayland is the future, and work in kwin has already begun on that. Having a Display Manager that supports a Wayland system compositor is essential to our long term strategy.</p>
<p>I've been asked to address this a lot, so I'll put my thoughts in a blog post.</p>
<b>The back story</b>
<p>After a bad experience customising KDM for a really important and scary client I wanted to redo the UI and customisation experience of KDM.  </p>
<p>I wanted to rewrite the whole UI and config side, so started looking through KDM code. It was around this time Robert Ancell posted about LightDM, a new display manager that aimed to be greeter agnostic. This was around 2 years ago when everyone was getting excited over Wayland, it was clear it was in LightDMs roadmap. </p>
<p>This seemed like a win, win situation. I get an easier platform to write my new login manager on *and* I get to bring Wayland support to KDE. </p>
<p>I wrote Qt bindings around LightDM upstream, along with a reference QWidget based greeter. I then started working on the KDE greeter in our repository.</p>
<p>The KDE greeter is approaching version 0.4. It is included in many distros, and generally feedback has generally been very positive. </p>
<b>The current state</b>
<p>Whilst LightDM is made by Canonical it is community driven and all patches go through review where anyone can comment. I have an opportunity to argue if anything is greeter specific in the libraries.</p>
<p>LightDM is used by my KDE greeter (used in some distros, not all), XFCE, and Razor Qt and of course Unity. </p>
<p>The Qt library was originally only used by us and Razor Qt, but with Unity's move to QML this means that Canonical are now dependant on the libraries I made. I am still in charge of the Qt library and still get final say on all reviews, I have rejected some Canonical employee patches as needing a rewrite and them with some of mine, it feels like a real open meritocracy community.</p>
<b>The rant</b>
<p>The golden-egg of using LightDM in KDE was that we wouldn't have to support all the boring things we need to make a display manager work, we wouldn't need to support a Wayland system compositor we get it for free. We all write stuff that helps each and open source progresses faster.</p>
<p>If I'd known they weren't going to add Wayland support, I'm not sure I would have invested my time in LightDM. I don't feel decieved, they thought they would do it at the time and Canonical are perfectly within their rights to decide to do something else.</p>
<p>The problem for me isn't that Canonical changed their mind, but that they didn't (or the developers weren't allowed) to tell me! If you know for 6 months that you're not going to do something you said you would it's rude not to tell people. It now sets our schedule back and that's really really frustrating.</p>
<b>Where does this leave us?</b>
<p>The state of LightDM hasn't got _worse_ however it does mean we need to add Wayland support ourselves. I've heard the argment; if we need to add Wayland support in something else, is it worth using it? I've been asked to address this, I'm writing this blog post to express my feelings, then I'll be having a meeting later this week to discuss things.</p>
<p>Our requirements are:</p>
<li>We need to have a display manager that works in Qt5 in the very near future.</li>
<li>We need a display manager that supports Wayland as a system compositor in the medium term.</li>
<p>Our options are:</p>
<li>Patch KDM to support X and Wayland (something hard to do, this code is built on top of XDM) AND fix Qt5 support (again not trivial in this case)</li>
<li> Write something new from scratch. In this case we still need to write a Wayland system compositoranyway!</li>
<li>Patch LightDM to also support Wayland (given it's already switching between Mir and X, the infrastructure is in place, it's designed to be able to switch backends and we have half-done Wayland patches to start with). It already all works in Qt5.</li>
<p>Writing a display manager is one of the things that sounds simple but in reality is very difficult; but there's a lot of stuff behind the scenes which is really difficult to get right: a tonne of environment variables to set, Xauthority files to set up, .dmrc files to manage, all sorts of session hooks let alone remote sessions and security.</p>
<p>From the above, I think my viewpoint on the matter is pretty clear, we just have a bit more work ahead than I'd initially hoped for.</p>
<p>That said, I will be having a meeting with a few interesed parties later this week to discuss future direction.</p>