---
title:   "Ubuntu Developer Summit in Waloonia"
date:    2010-05-11
authors:
  - jriddell
slug:    ubuntu-developer-summit-waloonia
---
Politics politics.  Here in Waloonia we have to spreak French, walk half a kilometre north and you are barred from speaking French and have to speak Dutch.  Go a further kilometer north and you're in Brussels where you have to speak French but in practice everyone spreaks English.  And they manage this all without bothering to have a government.  Almost as crazy as home where the old prime minister resigned to make way for a posh English chap who will doubtless steal our milk and make us pay a poll tax.  Really I should have been in London tonight to visit Buckingham palace and put myself forward for the job of Prime Minister and had the chance to found the Pacifist Free Software Kingdom of Scotland (plus southern principalities), but I missed my chance all because I'm at the Ubuntu Developer Summit planning the next six months of Kubuntu.

We've had a guests from a couple of my favourite companies here for the first couple of days.  Thiago and Jergen from Nokia's Qt dropped by to convert the world to goodness and correct pronounciation ("it's called cute").  Secret basement meetings with design teams got some of the inner world of Canonical developers converted while Qt Quick got the designers wanting to know when my 4.7 packages would be ready so they can stop using that Flash rubbish.

<img src="http://people.canonical.com/~jriddell/DSCF0668.JPG" width="500" height="375" alt="photo" />
DX Team consider conversion

Then Paul Adams made a humungous entry to covert the world of groupware to Kolab.  We get lovely testing three times a day for our KDE PIM packages, they get to make server packages and the hopeful promise of Kolab on a server CD before the next LTS.

Kubuntu developers are here in style. Jon the Enchilada made it through the volcanic ash and Daniel Nicoletti will wow us with improvements to KPackageKit.

<img src="http://people.canonical.com/~jriddell/DSCF0669.JPG" width="500 height="375" alt="ping" />
Brussles Ping?  UDS Pong!
<!--break-->
