---
title:   "Okular package available"
date:    2006-11-02
authors:
  - jriddell
slug:    okular-package-available
---
I've now added Okular packages to the <a href="http://kubuntu.org/announcements/kde4-3.80.2.php">KDE 4 archive</a>.  Okular is the rocking replacement for KPDF in KDE 4 featuring support for numberous file formats I've never heard of.

<a href="http://kubuntu.org/~jriddell/okular.png"><img src="http://kubuntu.org/~jriddell/okular.png" width="320" height="240" /></a>
<!--break-->
