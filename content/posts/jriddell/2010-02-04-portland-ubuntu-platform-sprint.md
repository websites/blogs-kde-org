---
title:   "Portland Ubuntu Platform Sprint"
date:    2010-02-04
authors:
  - jriddell
slug:    portland-ubuntu-platform-sprint
---
The Ubuntu Platform team (the people Canonical employs for Ubuntu) is having a sprint in Portland.  Portland is a nice city where you can be wandering down the road and come across 100 tweed wearing cyclists coming the other way.

<img src="http://people.canonical.com/~jriddell/portland/DSCF0149.JPG" width="500" height="357" />
My how they've grown

<img src="http://people.canonical.com/~jriddell/portland/DSCF0150.JPG" width="500" height="357" />
Kees' collection of every shipit CD ever nears completion

<img src="http://people.canonical.com/~jriddell/portland/DSCF0151.JPG" width="500" height="357" />
Voodoo doughnut, a local speciality.  I got half way through eating this before gaining diabetes
<!--break-->
