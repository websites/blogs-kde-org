---
title:   "Tu Parle Bazaar?"
date:    2011-09-16
authors:
  - jriddell
slug:    tu-parle-bazaar
---
<a href="https://bugs.launchpad.net/bzr/+bug/83941">Bug 83941 Bzr doesn't speak my tongue</a> has been closed.  Bzr can now be translated.  If you want to help bring bzr to those who prefer to work in non-English languages please help translate at <a href="https://translations.launchpad.net/bzr">Launchpad</a> (you will need to be in the appropriate Launchpad translations team).

The translation will involve quite a bit of specialist language (what is French for "colocated branch"?) and I expect there are strings yet that need to be added to the translation file.  I also need to look at translations for plugins.
<!--break-->
