---
title:   "Walk!"
date:    2004-08-28
authors:
  - jriddell
slug:    walk
---
Why is everyone so surprised when I tell them I walk to the youth hostel from aKademy rather than take the bus or shuttle car?  Really it's not that amazing.  Of course I'd rather cycle (or take a rickshaw) but I don't have one with me.  It only takes 40 minutes and it might work off that pizza you had earlier today.