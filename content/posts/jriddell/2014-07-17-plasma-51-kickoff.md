---
title:   "Plasma 5.1 Kickoff"
date:    2014-07-17
authors:
  - jriddell
slug:    plasma-51-kickoff
---
<img src="http://www.kde.org/announcements/plasma5.0/plasma-5-banner.png" width="800" height="289" />

We had a fun two hour meeting in #plasma yesterday to decide on the tasks for the next release.  It's due out in October and there's plenty of missing features that need to be added before Plasma 5 is ready for the non-geek.

Previously Plasma has used wiki lists to manage the Todo list but this is very old-school and clunky to manage.  Luckily Ben the KDE Sysadmin has just finished saving some children from a burning building before he swooped in and set up 
<a href="https://todo.kde.org/?controller=board&action=readonly&token=15ea7072a1f2be97963e83e1193f5fe8f1cf431272101b119d1d2237003a">todo.kde.org</a> just in time for us to add some 30 Todo items.

There will also be a meeting next week to discuss Wayland action items, come along if you want to help Martin shape the future.

<a href="https://todo.kde.org/?controller=board&action=readonly&token=15ea7072a1f2be97963e83e1193f5fe8f1cf431272101b119d1d2237003a"><img src="http://people.ubuntu.com/~jr/todo.kde.org.png" width="300" height="707" /></a>
<a href="https://todo.kde.org/?controller=board&action=readonly&token=15ea7072a1f2be97963e83e1193f5fe8f1cf431272101b119d1d2237003a">Plasma 5.1 Todo list</a>

Meanwhile former tabloid rag turned bastian of quality journalism <a href="http://www.omgubuntu.co.uk/2014/07/kde-plasma-5-released">OMGUbuntu said 
KDE Plasma 5 Arrives with Fresh New Look, True Convergence</a>.  More nice comments: "<i>I tested the neon iso yesterday and it looks absolutely stunning!</i>".

I'd like to thank Scarlett for putting in loads of hours perfecting the KF5 and Plasma 5 packages and now Rohan is tidying KF5 up and uploading to the Ubuntu archive.
