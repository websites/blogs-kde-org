---
title:   "Blog Moved"
date:    2014-11-22
authors:
  - jriddell
slug:    blog-moved
---
I've moved my developer blog to my vanity domain <a href="http://jriddell.org">jriddell.org</a>, which has hosted my personal blog since 1999 (before the word existed).  Tags used are Planet KDE and Planet Ubuntu for the developer feeds.

Sorry no DCOP news on jriddell.org.
