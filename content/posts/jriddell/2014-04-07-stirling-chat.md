---
title:   "Stirling Chat"
date:    2014-04-07
authors:
  - jriddell
slug:    stirling-chat
---
Lydia brought a load of friends over from Germany to visit the sights of Stirling.  Paul threw a party for her friends.  I canoed up the Firth of Forth to visit and drank lashings of ginger beer.

<a href="https://www.flickr.com/photos/jriddell/13692248884" title="photo by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3741/13692248884_5d181396e6.jpg" width="375" height="500" alt="photo"></a>
