---
title:   "New Blue Systems Office Edinburgh"
date:    2014-03-05
authors:
  - jriddell
slug:    new-blue-systems-office-edinburgh
---
The Blue Systems office in Edinburgh has moved across The Meadows to the Grassmarket to a larger office which is also surrounded on two sides by curious artist collectives and the occasional hipster café.  Hosted in <a href="http://www.thisiscodebase.com">Edinburgh's new technology incubator Codebase</a> we are in another building which is nicer on the inside looking out, this time with a view of our local volcano Arthur's Seat.

<a href="http://www.flickr.com/photos/jriddell/12946415893/" title="blue systems edinburgh by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3822/12946415893_2db38dc522.jpg" width="500" height="281" alt="blue systems edinburgh"></a>

I'd like to thank Cloudsoft for squatting in the office with me, making software to manage your applications in the cloud they are hiring now if you fancy a job in the most beautiful city in the world..
<small>
   In terms of skill set we are looking for, the main thing is that they are                                                  
   good software engineers rather than specific skills. However, here are a                                                   
   few potential areas:                                                                                                       
    1. Experienced Java programmer (and/or other languages a big plus; Java                                                   
       is not a pre-requisite).                                                                                               
    2. Devops experience.                                                                                                     
    3. Good sys admin skills.                                                                                                 
    4. Distributed computing (i.e. understands the architectural                                                              
       considerations etc).                                                                                                   
    5. Cloud/virtualization.                                                                                                  
    6. Javascript / web-dev (for one of them, perhaps)                                                                        
</small>
<a href="http://www.cloudsoftcorp.com/"><img src="http://www.krome-design.co.uk/wp-content/uploads/stationery-design-business-card-reverse-cloudsoft.png" width="502" height="325" /></a>