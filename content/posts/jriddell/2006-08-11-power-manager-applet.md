---
title:   "Power Manager Applet"
date:    2006-08-11
authors:
  - jriddell
slug:    power-manager-applet
---
For the <a href="https://wiki.kubuntu.org/KubuntuPowerManagement">Kubuntu Power Management</a> spec I've made a new power management applet.  This is a fairly simple frontend to HAL's power management features and replaces the ageing klaptopdaemon (the alternative kpowersave already duplicated much of what was in HAL).  Thanks to Sebas for starting this off.

There's a bunch of things I know that need fixed or changed including UI improvements from Usability Ellen.  But if you want to give it a shot now it's available in <a href="http://websvn.kde.org/trunk/playground/base/guidance/powermanager/">KDE's SVN</a> as part of Guidance. Also <a href="http://kubuntu.org/~jriddell/tmp/kde-guidance_0.6.7ubuntu1-0ubuntu1_i386.deb">Dapper .debs</a> and it's in Edgy as the latest kde-guidance. Leave feedback on the <a href="https://wiki.kubuntu.org/KubuntuPowerManagementFeedback">feedback page</a>.

There's a couple of notable problems with it though, the first is that being Python it'll have a larger startup time than C++ and take up more memory.  The other is that the python-dbus bindings only work with the glib mainloop so it has to keep polling to find changes, that's quite evil in a power management applet.  It may well be ported to Qt 4.2 c++ once that's out with the dbus and system tray goodness it'll bring.

<a href="http://kubuntu.org/~jriddell/powermanager.png"><img src="http://kubuntu.org/~jriddell/powermanager-wee.png" width="300" height="225" class="showonplanet" /></a>
<!--break-->