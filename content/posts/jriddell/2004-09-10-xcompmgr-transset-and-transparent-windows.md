---
title:   "xcompmgr, transset and transparent windows"
date:    2004-09-10
authors:
  - jriddell
slug:    xcompmgr-transset-and-transparent-windows
---
With the release of Xorg 6.8 I decided I ought to work out how to get the fancy trasparent windows stuff working so I could demo it at the <a href="http://www.kde.me.uk/index.php?page=linuxworld-expo-london-2004">expo in London next month</a>.  I wrote a short <a href="http://www.kde.me.uk/index.php?page=x-6.8-xcomposite-howto">guide</a>.

It works, it's fun but it's extremely slow.

<img class="rapemewithachainsawthanks" src="http://kde.me.uk/transparent-windows-wee.png" />
<!--break-->
