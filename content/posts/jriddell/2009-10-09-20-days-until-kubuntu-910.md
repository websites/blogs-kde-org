---
title:   "20 Days Until Kubuntu 9.10"
date:    2009-10-09
authors:
  - jriddell
slug:    20-days-until-kubuntu-910
---
Kubuntu 9.10 is on the final lap with freeze coming next week followed by the release candidate and then the final thing.  I'm pleased to say it's shaping up to be a decent release.  Of course compared to 9.04 that isn't hard, at least it connects to the network fairly reliably.  The trouble with being a KDE distro is that when KDE is crappy we end up being crappy too, on the bright side, when KDE rocks, we rock with it.

We have KDE integration with OpenOffice which I think is great.  We have social website integration in a bunch of places.  We have a preview of Netbook which is perfectly usable, made from Plasma Netbook, packages compiled on ARM and USB Creator.  We brought standardisation to visual notifications (which involved freedesktop.org being whipped into shape).  Our installer is bling bling thanks to Nuno for his distro love project.  There's a handy message indicator which I think is lovely and beats the pants out of what's available currently.  We finally have a half decent user setup tool, userconfig.  Translations are much improved (read: not broken compared to upstream).

Not everything went my preferred way, the world has yet to appreciate my genius in all matters.  I wanted Arora as the default web browser on the not unreasonable grounds that it can read slashdot, but others wanted to stay with Konqueror for its better KDE integration and won the day.  We also had an epic battle between Quassel and Konversation with Quassel winning narrowly, Konversation is still in main of course and has good mindshare outside KDE fans.  The message indicator is cool but Plasma folks want to do one with different technologies, somehow our messages didn't indicate enough with the person in Plasma who knew this until too late for that.  I didn't find time to work on system-config-printer-kde, one day, one day.

Outside KDE I'm glad to see Intel drivers working well enough to let me have compositing turned on for the first time, yay.  Intel drivers are so keen to keep you at your computer they won't let you log off, we're still working on that one.

Qt 4.6 is in the <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental">Kubuntu Experimental PPA</a> incase you hadn't spotted it.  In the main archive we have a new Qt-SDK package which will bring in all the tools you need to do development with the world's finest GUI library.

One last change, the Konqueror about: page, which we've patched for a while now to give it a search box now points to a Google custom search, use it if you want to help Kubuntu pay for itself (use the search box in the browser top right if you don't).

Three more weeks of beastie squishing and testing, join us in #kubuntu-devel if you want to help out.

<img src="http://people.canonical.com/~jriddell/karmic-countdown-banner/20.png" width="849" height="168" alt="20 days until karmic" />
<!--break-->
