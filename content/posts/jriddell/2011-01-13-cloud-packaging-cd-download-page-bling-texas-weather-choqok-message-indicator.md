---
title:   "Cloud Packaging, CD Download Page Bling, Texas Weather, Choqok Message Indicator"
date:    2011-01-13
authors:
  - jriddell
slug:    cloud-packaging-cd-download-page-bling-texas-weather-choqok-message-indicator
---
Upstream KDE used to give distros a week to package releases, including beta releases.  This meant we had plenty of time to package and test and find problems in releases before they go to users.  These days the fast pace of our six monthly release and a desire from developers not to send out releases a week out of date with the actual codebase means release candidates of KDE SC releases are only given to us distro packagers a day before they're announced.  How to build, package and test 20 source packages in only 24 hours?  

Well I could buy a stack of laptops, but contrary to occational belief Canonical don't buy me much in the way of hardware and my current laptop is mostly made up of duct tape to stop it falling apart.  However Canonical do have a lovely cloud policy where I can hire cloud computers (almost) all I want.  So I signed up for Amazon Web Services and started playing with EC2, computers for hire by the hour (at 0.08USD per hour).  A simple shell script is enough to fire up a cloud computer with an Ubuntu 10.10 image, update it to natty, download the source from KDE's ktown server and start to build it.  At the end of the build I do the manual bits of ensuring all build dependencies are satisfied, checking for new files, reviewing the ABI for breakage, ensuring it installs etc.  Unlike on my own laptop I can run several at the same time without detrement to bandwidth, disk usage or CPU usage.  Another simple shell script can run the upgrade test without much manual intervention.  The only slow bit is running the newly packaged KDE release which is done over remote X, maybe I should investigate NX.

It still takes a bit more than 24 hours to get it all packaged and tested enough for release but not too much more.  Come and join the ninjas in #kubuntu-devel to help out.
<hr />
Arrived in Texas for the Platform team rally to find the weather even more dreich than Scotland.  I'm sure it's not meant to snow in Texas.  Using the rooftop swimming pool each day anyway in defiance.
<hr />
Top Kubuntu artist sheytan was good enough to make our CD download pages a new look.  <a href="http://cdimage.ubuntu.com/kubuntu/daily-live/20110112/">ooh pretty</a>.
<img src="http://people.canonical.com/~jriddell/cdimage.png" width="400" height="250" />
<hr />
Choqok gained support for the Message Indicator applet we ship in Kubuntu.  It's a shame the kdeplasma-addons maintainer didn't accept the applet and still ships the dire incomingmsg applet.
<img src="http://people.canonical.com/~jriddell/choqok-message-indicator.png" width="192" height="112" />
<!--break-->
