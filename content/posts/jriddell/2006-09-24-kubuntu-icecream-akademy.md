---
title:   "Kubuntu Icecream at Akademy"
date:    2006-09-24
authors:
  - jriddell
slug:    kubuntu-icecream-akademy
---
As usual at Akademy we have an icecream cluster here, icecream is the distributed builder for the discerning compiler.  

<b>Update:</b> David Faure recommended upgrading to Subversion 1.4 for 10 times extra speed, so I've added that.  You can get subversion 1.4 and icecream from:

<pre>
deb http://kubuntu.org/~jriddell/akademy/ ./
</pre>

<img src="http://static.flickr.com/89/251145611_6d73da1cee.jpg?v=0" width="500" height="382" class="showonplanet" />

Ooh, lots of nodes, make -j 50 goodness.
<!--break-->
