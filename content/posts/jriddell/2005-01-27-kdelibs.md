---
title:   "kdelibs"
date:    2005-01-27
authors:
  - jriddell
slug:    kdelibs
---
Recently I took my first dive into kdelibs.  I wanted to make the tabs in Umbrello resize automatically like those in Konqueror and the correct way to do it when several applications want a feature is to put it in kdelibs.  I grabbed the code from Konqueror and made it fit into KTabWidget and posted to kde-core-devel.  Unfortunatly nobody replied and when I pressed I just got a "yeah, looks fine" which it wasn't.  There arn't many KDE developers with the knowledge to work on kdelibs and they're all very busy (and probably too trusting :). So I committed it and it worked fine in Umbrello which kept me happy but caused crashes and other strangeness in Konqueror.  Part of the problem may have been that even though I was very careful I still managed to break binary compatibility.  The rules for binary compatibility are complex and make no sense whatsoever unless (presumably) you know how linkers work.  I realised I was in too deep for my programming ability (best in university year isn't quite so good in the real world) and would have taken the code out had Binner not done a great job of fixing it all up as far as possible.  Phew.
