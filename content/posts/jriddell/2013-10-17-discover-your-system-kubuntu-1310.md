---
title:   "Discover your System with Kubuntu 13.10"
date:    2013-10-17
authors:
  - jriddell
slug:    discover-your-system-kubuntu-1310
---
<img src="http://www.kubuntu.org/files/kubuntu-1310.png" />

Packaged full of lovely goodness is the upgrade to 13.10, grab it now..

<a href="http://www.kubuntu.org/news/kubuntu-13.10">http://www.kubuntu.org/news/kubuntu-13.10</a>

Highlights include...

New Software from KDE - KDE Plasma and Applications 4.11
New app installer - Muon Discover
New accounts setup - User Manager
Easier to get everything during install - Wireless Setup in Installer
KDE Telepathy with Better Text Editing and Improved Notifications
New Network Manager applet
Easier to report what you're using with About System
documentation returns at http://docs.kubuntu.org
still using good old X.org and planning to follow the rest of the ecosystem with Wayland

Remember Commercial Support is available and you can help Kubuntu with buying Merchandise and giving Donations

Enjoy it all.
