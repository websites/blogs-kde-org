---
title:   "Kubuntu Commercial Support"
date:    2013-09-03
authors:
  - jriddell
slug:    kubuntu-commercial-support
---
<a href="http://www.kubuntu.org/news/commercial-support"><img src="http://people.ubuntu.com/~jr/kubuntu-commercial-banner-wee.png" width="600" height="147" /></a>
As reported on Slashdot <a href="http://news.slashdot.org/story/13/09/02/2224231/kubuntu-announces-commercial-support">Commercial Support is available again for Kubuntu</a>.  If you or your organisation needs help with your computer systems running Kubuntu but you don't want to worry about the best IRC channel to ask in you can pay some money and phone up the office in England to get some help.  They will use phone, e-mail, Google hangout, Skype, VNC and any other method you like to help diagnose the issue and resolve it for you.

Many thanks to <a href="http://www.emerge-open.org">Emerge Open</a>, a not-for-profit company who's mission is to provide sustainable funding for Open Source Communities.  So the profits will be going straight back to Kubuntu.  They even operate in a transparent way, you can find their <a href="
http://www.emerge-open.biz/#!being-open/cy29">list of director's pay</a> and contracts which are published under a CC licence.  My sort of company.
