---
title:   "KDE Hal Device Manager"
date:    2006-01-07
authors:
  - jriddell
slug:    kde-hal-device-manager
---
To get my Python skills up to scratch I ported hal-device-manager to KDE making <a href="http://kubuntu.org/~jriddell/kde-hal-device-manager/">KDE HAL Device Manager</a>.  

Python is so much nicer to code in than C++ in almost all respects.  The only part I'm not convinced about is variables that aren't static typed, static typing adds a lot to the readability of the code.  Unfortunately the pyuic compiler for Qt Designer doesn't like working with KDE widgets so no KListView stripes or searchbar for now.  

It would be interesting to learn Ruby and compare that to Python since everyone who's touched Ruby seems to praise it forever, alas I don't have time currently.

The python-dcop bindings need GTK's mainloop to listen for signals so the KDE HAL Device Manager can only ask HAL for information but can't listen for updates.

The GTK version comes in at a bit over 800 lines of code while my Qt version comes in at just over 600 lines of code, a 1/4 saving.  Much of the diff in that is GTK storing the information in a model first then displaying that in a widget so I wonder if Qt's Interview would mean the line count going up in a Qt 4 port (line count having little relevance to the quality of the code, since larger line count can mean a more readable program).

<img src="http://kubuntu.org/~jriddell/kde-hal-device-manager/hal.png" width="545" height="483" />

<!--break-->