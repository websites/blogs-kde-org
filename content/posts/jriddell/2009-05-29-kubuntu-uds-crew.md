---
title:   "The Kubuntu UDS Crew"
date:    2009-05-29
authors:
  - jriddell
slug:    kubuntu-uds-crew
---
The Kubuntu spods here at the Ubuntu Developer Summit here in Barcelona went out with a few of the local KDE types.  What  a good looking bunch.

<a href="http://people.ubuntu.com/~jriddell/2009-05-29-kubuntu-groupphoto-uds-barcelona.jpg"><img src="http://people.ubuntu.com/~jriddell/kubuntu-groupphoto-uds-barcelona-wee.jpg" width="500" height="283" /></a>
Jonathan Riddell, Aleix Pol, Scott Kitterman, Alex Fiestas, Ken Wimer, Yuriy Kozlov, Roderick Greening, Anthony Mercatante, Albert Astals Cid, Ryan Kavanagh, Mackenzie Morgan, Daniel Chen, [bottom row] Sebastian Krugler, Michael Casadevall, Roman Shtylman

Tonight UDS has finished its sessions and goes en masse to the karaoke.  This could get dangerous.
<!--break-->
