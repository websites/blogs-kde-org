---
title:   "Ubuntu Online Summit KDE Frameworks Talk"
date:    2014-06-10
authors:
  - jriddell
slug:    ubuntu-online-summit-kde-frameworks-talk
---
<a href="http://summit.ubuntu.com/uos-1406/2014-06-10/">Ubuntu Online Summit</a> starts today with talks and sessions on all matter of stuff related to Ubuntu.  It opens with the highlight of the summit, <a href="http://summit.ubuntu.com/uos-1406/meeting/22262/kde-frameworks-libraries-for-all-qt-users/">KDE Frameworks - Libraries for all Qt users</a> a talk about the 50-odd framework libraries KDE is releasing and how they will be useful to all Qt programmers.  The excitable David Edmundson introduces the current status and what works well.
<img src="http://people.ubuntu.com/~jr/ubuntu-online-summit.png" width="296" height="52" />
