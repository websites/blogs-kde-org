---
title:   "Umbrello Tabs and KDM Themes"
date:    2004-11-24
authors:
  - jriddell
slug:    umbrello-tabs-and-kdm-themes
---
<p>Last night I made Umbrello use tabs for it's diagrams.  KTabWidget does the job but it misses the nice feauture that Konqueror and Akregator have which is shrinking tabs to fit available width.  It would be nice if this feature was in KTabWidget directly.</p>

<img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/fa4beafa0061b5b2a1a930f3aa682fcb-735.png" />

<p>In other news KDM has got support for GDM themes.  Uga and Ossi have done a great job on this which is one of the oldest open beasties and has over 1000 votes.  Still plenty to fix, not all the feature exist yet but it looks lovely.  <a href="http://www.kde-look.org/content/show.php?content=18160">First post!</a></p>

<img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/b6a5a4161974a1947d0da3a85d5afbc6-736.png" />
<!--break-->
