---
title:   "Re-blog: Pau Garcia i Quiles, Mark’s divisive leadership"
date:    2013-03-08
authors:
  - jriddell
slug:    re-blog-pau-garcia-i-quiles-mark’s-divisive-leadership
---
<pre>A <a href="http://www.elpauer.org/?p=1309">repost of elite KDE contributor Pau Garcia i Quiles' blog</a> for Planet Ubuntu</pre>
<p>Mark Shuttleworth recently critized Jonathan Riddell for proposing Xubuntu and others join the Kubuntu community. I thought I could make a few amendments to Mark’s writing:</p>
<p style="padding-left: 30px;"><strike>Jonathan</strike> <em>Mark</em> <a href="http://www.markshuttleworth.com/archives/1232">says that <strike>Canonical&nbsp;</strike><em>Kubuntu</em> is not taking care</a>&nbsp;of the Ubuntu community.</p>
<p style="padding-left: 30px;">Consider for a minute, <strike>Jonathan</strike> <em>Mark</em>, the difference between our actions.</p>
<p style="padding-left: 30px;"><strike>Canonical</strike> <em>Kubuntu</em>, as one stakeholder in the Ubuntu community, is spending a large amount of energy to evaluate how its actions might impact on all the other stakeholders, and offering to do chunks of work in support of those other stakeholder needs.</p>
<p style="padding-left: 30px;">You, as one stakeholder in the Ubuntu community, are inviting people to contribute less to the broader project <em>[all the X and Wayland -based desktops]</em>, and more to one stakeholder <em>[Unity and Mir]</em>.</p>
<p style="padding-left: 30px;">Hmm. Just because you may not get what you want is no basis for divisive leadership.</p>
<p style="padding-left: 30px;">Yes, you should figure out what’s important to <strike>Kubuntu</strike> <em>Ubuntu Unity and Mir</em>, and yes, you should motivate folks to help you achieve those goals. But it’s simply&nbsp;wrong to suggest that <strike>Canonical</strike> <em>Kubuntu</em> isn’t hugely accommodating to the needs of others, or that it’s not possible to contribute or participate in the parts of Ubuntu which <strike>Canonical</strike> <em>Kubuntu</em> has a particularly strong interest in. Witness the fantastic work being done on both the system and the apps to bring <strike>Ubuntu</strike> <em>Plasma</em> to the phone and tablet. That may not be your cup of tea, but it’s tremendously motivating and exciting and energetic.</p>
<p>See Mark? I only needed to do a little search and replace on your words and suddenly, meaning is completely reversed!</p>
<p>Canonical started looking only after its own a couple of years ago and totally dumped the community. Many people have noticed this and written about this in the past two years.</p>
<p>How dare you say Jonathan or anyone from Kubuntu is proposing contributing less to the broader community?&nbsp;<span style="line-height: 1.714285714; font-size: 1rem;">The broader community uses X and/or Wayland.</span></p>
<p>Canonical recently came with Mir, a replacement for X and Wayland, out of thin air. Incompatible with X and Wayland.</p>
<p>No mention of it at all to anyone from X or Wayland.</p>
<p>No mention of it at FOSDEM <strong>one month ago</strong>, even though I, as the organizer of the Cross Desktop DevRoom, had been stalking your guy for months because we wanted diversity (and we got it: Gnome, KDE, Razor, XFCE, Enlightenment, etc, we even invided OpenBox, FVWM, CDE and others!). I even wrote a mail to you personally warning you Unity was going to lose its opportunity to be on the stand at FOSDEM. You never answered, of course.</p>
<p>Don’t you think Mir, a whole new replacement for X and Wayland, which has been in development for 8 months, deserved a mention at the largest open source event in Europe?</p>
<p>Come on, man.</p>
<p>It is perfectly fine to say “yes, Canonical is not so interested in the community. It’s our way or the highway”.</p>
<p>But do not pretend it’s anything else or someone else is a bad guy.</p>
<p>In fact, is there any bad guy in this story at all!? I think there is not, it’s just people with different visions and chosen paths to achieve them.</p>
<p>Maybe Mir and Unity are great ideas, much better than X and Wayland. But that’s not what we are talking about. We are talking about community, and Canonical has been steadily destroying it for a long time already. If you cannot or do not want to see that, you’ve got a huge problem going on.</p>