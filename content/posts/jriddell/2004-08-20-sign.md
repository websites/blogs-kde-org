---
title:   "A sign"
date:    2004-08-20
authors:
  - jriddell
slug:    sign
---
I was at a train station in a town called Heidelburg at about 4:00 this morning on my way to akademy, having not slept for 20 hours at this point and I'm trying to work out the German train system where one machine charged me &euro;14 and another wanted to charge me &euro;49 when a man wanders up to me and says that "I look poor" and would I like some money.  I assured him that I wasn't in any real need for money just now and he wanders off.  Not long later when I'm on (the wrong) train when he taps on the window and gives me 20 euro, mutters something about god and wanders off.

Is this a sign of a good week ahead?  Or maybe that there's some particularly generous people in Germany.  Dinnae ken, but it made my day.  

30 hours without sleep and doing good.

