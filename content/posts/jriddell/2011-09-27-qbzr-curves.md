---
title:   "qbzr with curves"
date:    2011-09-27
authors:
  - jriddell
slug:    qbzr-curves
---
Nice little visual change to qbzr, curves on the diff view..

Before:
<img src="http://people.canonical.com/~jriddell/qbzr-not-curved.png" width="645" height="410" />

After:
<img src="http://people.canonical.com/~jriddell/qbzr-curved.png" width="645" height="410" />

Thanks to <a href="https://code.launchpad.net/~hid-iwata/qbzr/curved-diff-handle/+merge/74833">Iwata Hidetaka</a>.

Being bored of the IRC poll on blogs.kde.org I made a new <a href="http://blogs.kde.org/node/4484">poll for revision control systems</a>.  I'm glad to see that after one vote Bazaar is at 100%.
<!--break-->
