---
title:   "10:10:10 10/10/10 It's Kubuntu 10.10"
date:    2010-10-10
authors:
  - jriddell
slug:    101010-101010-its-kubuntu-1010
---
<a href="http://www.kubuntu.org/news/10.10-release"><img src="http://www.kubuntu.org/files/10.10-release-announce/kubuntu-10.10-banner.png" width="500" height="250" /></a>

The date and time are set, <a href="http://www.kubuntu.org/news/10.10-release">Kubuntu 10.10</a> is out.

Highlights include all the latest KDE Software, an application focused software installer, combined Desktop/Netbook images, new installer layout (MP3 ready during the install!), a new web browser, a shiny new font, a nifty global menu for Netbook, Kubuntu Mobile featuring Plasma Mobile (technology preview), ooh lots more!

<a href="http://www.kubuntu.org/getkubuntu/download">Download</a> or <a href="https://help.ubuntu.com/community/MaverickUpgrades/Kubuntu">upgrade</a> now

Lots of people to thank in this release, Scott, Jonathan, Harald, Rohan, Michal, Felix, Yofel, Alessando, Clay, Rodrigo and probably many others I've forgotten.  Notable upstreams include Daniel who did an awesome job on KPackageKit turning it into a Software Centre/App Store style application and Martin who helped us with lots of questions about compositing.  And of course all the people in and around Canonical who help too, Mark, Michael, Emmet, Grue, Colin, Evan.
<!--break-->
