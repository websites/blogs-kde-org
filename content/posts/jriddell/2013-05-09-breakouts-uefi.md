---
title:   "The Breakouts: UEFI"
date:    2013-05-09
authors:
  - jriddell
slug:    breakouts-uefi
---
Me and Rohan and Harald spent the day playing with the cheapo Sony Vaio laptop I got with Windows 8 and UEFI firmware.

This laptop has many partitions:
sda1 is fat32 and contains a Boot and a Microsoft EFI boot loaders
sda2 is ntfs and contains Windows Recovery bits
sda3 is efi/fat32 and contains a Boot, a Microsoft, a kubuntu and a ubuntu EFI boot loader
sda4 is a Microsoft reserved parition and won't mount, spooky
sda5 is an ntfs partition and has Windows 8 on it
sda6 is ext4 and I install Ubuntu and Kubuntu to it
sda7 is a linux-swap partition

I've no idea why there are two efi partitions
I've no idea why there's both an ntfs windows recovery partition and a microsoft reserved partition.

We installed Kubuntu but it didn't set up Grub and we couldn't do much useful at the Grub command line.

So we installed Ubuntu Unity 13.04 and it successfully set up Grub so we could boot from Grub into Ubuntu.  Grub also listed Windows 8 but it gives an error on trying to boot "error: can't find command drivemap  error: invalid EFI file path".  Grub also listed windows recovery and this does boot and lets us boot into Windows 8 but that then stops Grub being loaded so there's no way to reboot into Ubuntu.  <a href="http://people.ubuntu.com/~jr/syslog-ubuntu-good-uefi-install">see log</a>.

Then we tried to install Kubuntu again and it give an error during install about the grub setup package not configuring right <a href="https://bugs.launchpad.net/ubuntu/+source/ubiquity/+bug/1178294">see bug for log</a>.

If you go to ubuntu.com to download it points Windows 8 users to <a href="https://help.ubuntu.com/community/UEFI">this scary UEFI wiki page</a> with scary headings like <b>"Installing Ubuntu Quickly and Easily via Trial and Error"</b>.

Conclusion: UEFI is a giant MS conspiracy to make installing Linux more faffy than it already is.  Kubuntu is slightly more broken then Ubuntu but not much.  Only silver lining is that Windows 8 is rubbish and when we tried it there genuinely was a notification saying <b>"Warning: your children might not be protected"</b>.  Think of the children and don't use Windows 8!

Oh well, here's some pretty pictures to keep you amused.

<a href="http://www.flickr.com/photos/jriddell/8721960698/" title="DSCF7600 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7386/8721960698_6a139dd76c_n.jpg" width="320" height="240" alt="DSCF7600"></a>
Les Frogs sont hypercool

<a href="http://www.flickr.com/photos/jriddell/8720833415/" title="DSCF7593 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7373/8720833415_1472eb891e_n.jpg" width="320" height="240" alt="DSCF7593"></a>
The view of Catalunia is great for contemplation

<a href="http://www.flickr.com/photos/jriddell/8720834451/" title="DSCF7594 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7317/8720834451_03da0bdcc7_n.jpg" width="320" height="240" alt="DSCF7594"></a>
Murdering a water mellon

<a href="http://www.flickr.com/photos/jriddell/8721949864/" title="DSCF7589 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7364/8721949864_52e4d3fc16_n.jpg" width="320" height="240" alt="DSCF7589"></a>
VHanda ignores Doctor Who.