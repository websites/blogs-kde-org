---
title:   "Laptop"
date:    2005-02-21
authors:
  - jriddell
slug:    laptop
---
With IBM selling their Thinkpad division I have to wonder what the best type of laptop to get once my current Thinkpad goes the way of its predecessors (stolen, destroyed by aeroplane cargo men etc).  Fortunately Betty bought me this laptop as a birthday present at the weekend for 50p down the market, I'm fairly confident it will last for years.  It even beeps which means its a real laptop.

<img src="http://jriddell.org/photos/wee/2004-02-21-new-laptop-wee.jpg" class="showonplanet" width="300" height="400" />
<!--break-->
