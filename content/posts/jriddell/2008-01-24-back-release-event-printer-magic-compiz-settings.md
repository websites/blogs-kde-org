---
title:   "Back from Release Event, Printer Magic, Compiz Settings"
date:    2008-01-24
authors:
  - jriddell
slug:    back-release-event-printer-magic-compiz-settings
---
The release event went swimmingly with a whole bunch of useful discussions and talks.  Since then I've been in Canonical Tower looking down upon London.  

With feature freeze coming closer for Hardy, exciting things are in development.  I've been working on porting system-config-printer to Qt, so we get magic printer setup when you plug in your printer.  Yesterday came the first alphas of Adept 3, a rewrite for KDE 4.  Today nosrednaekim popped up with some sources for a compiz setup tool for those who want maximum bling.  And iRon is looking at bullet proof X for KDM.

<a href="http://jriddell.org/photos/2008-01-jonathan-above-london.jpg"><img src="http://jriddell.org/photos/wee/2008-01-jonathan-above-london-wee.jpg" width="300" height="188" /></a>
Somewhere above the politicians and ferris wheel

Printer magic setup, also available in <a href="http://kubuntu.org/~jriddell/system-config-printer.ogg">video format</a> following experiments with krecordmydesktop
<a href="http://kubuntu.org/~jriddell/system-config-printer.png"><img src="http://kubuntu.org/~jriddell/system-config-printer-wee.png" width="400" height="180" /></a>

Easy Compiz setup
<a href="http://kubuntu.org/~jriddell/desktop-effects-kde.png"><img src="http://kubuntu.org/~jriddell/desktop-effects-kde-wee.png" width="300" height="231" /></a>
<!--break-->

