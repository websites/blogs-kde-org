---
title:   "Plasma Desktop Scripting IRC Session"
date:    2010-04-28
authors:
  - jriddell
slug:    plasma-desktop-scripting-irc-session
---
Aaron Seigo did a session yesterday with distro packagers on Plasma Desktop Scripting.  Here are <a href="http://people.canonical.com/~jriddell/plasma-desktop-scripting-irc-session.text.html">the logs in HTML</a> and <a href="http://people.canonical.com/~jriddell/plasma-desktop-scripting-irc-session.text">plain text</a>.

<hr />

<img src="http://people.canonical.com/~jriddell/10.10-countdown/kubuntu_1.png" width="180" height="150" />
Something exciting happening tomorrow
<!--break-->
