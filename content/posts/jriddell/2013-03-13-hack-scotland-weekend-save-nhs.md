---
title:   "Hack Scotland weekend to Save the NHS"
date:    2013-03-13
authors:
  - jriddell
slug:    hack-scotland-weekend-save-nhs
---
NHS Hack Scotland is a weekend event to bring together geeks and developers with medics and manager from Scotland's lovely health service.  Like the equivalent <a href="http://nhshackday.com/">NHS Hack Days</a> in the equivalent (but endangered) health service in England is it planned to end up with open source prototype programmes to solve interesting problems.  

Unfortunately NHS Lothian has a policy of not installing open source software.  Or as the <a href="http://www.whatdotheyknow.com/request/151327/response/367532/attach/html/3/open%20source%20software%20march%202013.doc.html">response to my request put it</a>  "we do not currently install
open source software and as a result we don't have such a policy." Hopefully this will fix the lack of policy in the right direction.

Myself and Paul Adams will be judges to hand out prizes to the best programmes.  We might well look more favourably on anything using KDE but I couldn't possibly comment.(jest!)

<img src="http://www.nhshackscotland.org.uk/files/1613/6249/9192/Scotland_hack_page2.jpg" width="587" height="856" />
Which judge is me and which is Paul in the above cartoon?
