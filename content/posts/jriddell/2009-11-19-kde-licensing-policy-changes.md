---
title:   "KDE Licensing Policy Changes"
date:    2009-11-19
authors:
  - jriddell
slug:    kde-licensing-policy-changes
---
Today I updated the <a href="http://techbase.kde.org/Policies/Licensing_Policy">KDE Licensing Policy</a> with a couple of changes following requests from folks.  Most notably Creative Commons is now allowed.  This is only for standalone media files (such as an image for a splash screen) and not for anything which might want to be mixed with GPL material such as icons.  "Attribution-Share Alike 3.0 Unported" is the version allowed.  The other change is requiring BSD licencing for CMake modules, which brings the policy into line with existing practice.
<!--break-->
