---
title:   "Registration for Akademy 2007 Now Open"
date:    2007-04-12
authors:
  - jriddell
slug:    registration-akademy-2007-now-open
---
<a href="http://www.kde.org.uk/akademy/">Registration for Akademy 2007</a> is now open.  This year it's being run by KDE GB.  You need to register by the end of the month if you want accommodation with the KDE booking.  Please pay promptly for the accommodation, it'll save us all a lot of hassle, payment is through paypal.  Akademy 2007 is going to be great!  Do come along.

<img src="http://akademy2007.kde.org/images/montage.jpg" width="549" height="97" />
