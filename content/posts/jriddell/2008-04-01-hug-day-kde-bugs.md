---
title:   "Hug Day for KDE Bugs"
date:    2008-04-01
authors:
  - jriddell
slug:    hug-day-kde-bugs
---
Today's <a href="https://lists.ubuntu.com/archives/ubuntu-motu/2008-April/003512.html">Kubuntu Hug Day</a> looks at bugs in Launchpad with comments regarding the KDE bug tracker.  Join #kubuntu-devel and #ubuntu-bugs to help out.
