---
title:   "UKUUG 2006"
date:    2006-07-05
authors:
  - jriddell
slug:    ukuug-2006
---
Last week I went to UK Unix Users Group's Linux 2006 conference to talk about KDE and Kubuntu and watch a bunch of other talks.

First talk was from Tethys who tried to convince us that sed was good for more than s/foo/bar, seems not worth the hassle to me.  Torsten Spindler showed off his video masking privacy feature which detects when someone has walked into a CCTV shot and pixilates that are so you can't see who it is.  A loud person from Novell talked about their desktop work by starting his talk saying he didn't mind what desktop people used and then went on to talk about Gnome, he didn't even have KDE highlighted in the list of projects Novell hires key people in, the other Novell attendee didn't pretend and just said that they had picked Gnome as their desktop to promote.  That there are people in Novell who deliberately ignore the excellent work their company puts into KDE (like sponsoring <a href="http://dot.kde.org/1151271635/">KDE Four Core</a>) is a shame and they'll loose out because of it.

Fabien gave an update on FreeNX but despite NoMachine releasing NX 2.0 there doesn't seem to be much chance of there being something that's usable to make distribution packages with any time soon.  We had a demo of Xara the newly GPL'ed vector graphics renderer, it looks very smart as far as I can tell without using it and if the rendering library it uses becomes GPL'ed too as promised I'll try and get it into Edgy..

Aaron Crane gave a talk about how The Register works, their CMS exports to HTML with Server Side Includes as the fastest way to serve pages and be able to updates themes for a day at a time.  Similarly Neil McGovern gave a talk about the setup at fotopic.net and they fun they have with load balancing.

The man from AMD pimped their support for Xen and how it's much better than Intel's and Jono Bacon flew back from Guadec to talk about pimping Free Software in general.  

Most impressive was Leslie Fletcher's move work on an All Party Parliamentary Open Source Group, the debate afterwards was mostly about terminology of Open Source or Free Software. Although I agree that open sauce is inferior and just as confusing it's not the most important point here, I hope UKUUG will continue to support the work on creating an All Party Parliamentary Open Source Group and it may even get me to pay my UKUUG fees.

The OpenStreetMap talk was also very interesting, they've done some impressive work getting free mapping working which will be an impressive resource here in the UK where we have the best mapping anywhere but under very restrictive usage terms thanks to the Ordinance Survey.  

Wookey talked about the ABI transition needed in Debian for Arm, I was pleased to hear it will probably be done by creating a new architecture rather than renaming every library in Debian (and Ubuntu).

The feedback from my talk about KDE 4 and Kubuntu seemed good, I'm hardly the best of public speakers but I think there was a lot there that people didn't know about.  <a href="http://websvn.kde.org/*checkout*/trunk/promo/presentations/2006_07_UKUUG/2006-06-jonathan-riddell-kde-kubuntu.odp?rev=557222">Slides</a> for KPresenter.

And finally my good friend Paul talked about random Ubuntu things with great enthusiasm.  If only I could get him to switch to KDE then all my laptop support problems would be sorted.

So three solid days of talks there.  It was good to see Kenny and David and others who I havn't seen for a while.

John Tapsel turned up and we got to see his fancy holographic laser.

<a href="http://www.flickr.com/photos/jriddell/182338859/"><img src="http://static.flickr.com/59/182338859_05516f9a11_m.jpg" class="showonplanet" /></a>

Ooh, Konqi in 3D!

<a href="http://www.flickr.com/photos/jriddell/182338899/"><img src="http://static.flickr.com/55/182338899_62befc5e33_m.jpg" class="showonplanet" /></a>

Oy, you at the back, stop using IRC when I'm giving a talk!

<a href="http://jriddell.org/photos/2006-07-01-ukuug-jonathan-talk2.jpg"><img src="http://www.duffus.org/photos/d/18633-2/img_1545.JPEG" class="showonplanet" /></a>
<!--break-->