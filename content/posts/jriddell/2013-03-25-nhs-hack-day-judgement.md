---
title:   "NHS Hack Day Judgement"
date:    2013-03-25
authors:
  - jriddell
slug:    nhs-hack-day-judgement
---
Myself and Paul and Nicky and Carry judged the entries.  They were all winners.  They all showed what could be done when you put people with ideas together with people who can hack.  They all showed the power of open, collaborative and friendly development.  

The winner was: Pocket TherAPPist, Michael Richardson learnt how to code Android in two days and mastered Eclipse to make a way of recording a patient's anxiety levels for later review.

Second was <a href="http://www.thatscottishengineer.co.uk/quickhelp/">NHS Quick Help</a> a PR campaign and simple webpage to point people towards the best place to go when you have a problem, could be A&E but could also be your local Pharmacy, Minor Injuries Unit or NHS 24.

Third was: South East Scotland Anesthesia information, they didn't add themselves to the project list so I don't know more :(

Fourth was GP Provision Planner, which used open source, open data and open street map to show where GP surgeries are over subscribed or under subscribed for future planning needs.

Code can be found on <a href="https://github.com/nhshackscotland/">github/nhshackscotland</a> and on the <a href="https://docs.google.com/document/d/11ksPot84WISMF23m-yVn5rm9tsnT0KeBfdfmRZavuq8/edit">Project List</a>.  Hopefully the NHS staff can take this back and convince their departments and managers to go forth with Freedom.

<a href="http://www.flickr.com/photos/jriddell/8587436120/" title="525063 10151714044274505 1313883357 n by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8508/8587436120_7561e4fd75.jpg" width="500" height="333" alt="525063 10151714044274505 1313883357 n"></a>
Judging Mode

<a href="http://www.flickr.com/photos/jriddell/8586906887/" title="DSCF7412 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8238/8586906887_b3e122ab26.jpg" width="500" height="375" alt="DSCF7412"></a>
The Judges Contemplate

<a href="http://www.flickr.com/photos/jriddell/8588002644/" title="DSCF7407 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8244/8588002644_c1ee135459.jpg" width="500" height="375" alt="DSCF7407"></a>
Coding on NHS Quick Help
<!--break-->