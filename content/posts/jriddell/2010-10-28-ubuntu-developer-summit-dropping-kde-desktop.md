---
title:   "Ubuntu Developer Summit: Dropping KDE Desktop"
date:    2010-10-28
authors:
  - jriddell
slug:    ubuntu-developer-summit-dropping-kde-desktop
---
The Ubuntu Developer Summit is in full swing here in Florida.  There have been a load of important decisions taken.  For example today I dropped KDE from our desktop.  I know this may be controvertial with some parts of the community but we can have unity in our new desktop.. <a href="http://websvn.kde.org/trunk/KDE/kdebase/workspace/kdm/kfrontend/sessions/kde.desktop.cmake?r1=1190695&r2=1190694&pathrev=1190695">Plasma</a>.  Of course we're Kubuntu so we did it upstream.

Martin and the X.org packagers had a face off about X drivers.  Kubuntu Mobile has plans to make it more useful next round.  We found problems that need fixed like KDE's system localisation support.  We reviewed all the patches Kubuntu and Debian has for Qt and found a load that can be dropped or moved upstream.  Canonical decided it loves Qt and Qt asked what Canonical wants (accessibility was mentioned a lot).

There's still plenty of sessions to go if you want to take part remotely, <a href="http://summit.ubuntu.com/uds-n/2010-10-28/">see the schedule</a> and the <a href="http://uds.ubuntu.com/participate/remote/">remote participation</a>.

Florida is hot but unfortunately this is considered a bug and every car and building has a lot of energy spent on it turning it into a fridge.  This is ironic since the size of the fridges here are not far off the size of the buildings.  

I haven't seen Mickey yet (Harald and co spent half an hour queueing to meet the oversized mouse, bless) but we did go to Downtown Disney and ate tea under a giant Magic Mushroom.

<img src="http://people.canonical.com/~jriddell/DSCF5145.JPG" width="400" height="300" />
What a good looking bunch
Thiago, Mackenzie, Martin, Valorie, Harald, Jussi, Aurelien, Scott, David, Rodrigo, Jonathan, Rohan

<img src="http://people.canonical.com/~jriddell/DSCF5149.JPG" width="400" height="300" />
In heated discussion between sessions

<img src="http://people.canonical.com/~jriddell/DSCF5147.JPG" width="400" height="300" />
T-REX scaring Harald and Mackenzie

<img src="http://people.canonical.com/~jriddell/DSCF5148.JPG" width="400" height="300" />
Konqi-ness monster fanboy

<img src="http://people.canonical.com/~jriddell/harald.jpg" width="400" height="533" />
This hat has shiny lights, bless

[Note: fear not, the headline is ironic. I changed the branding in the X session file to match the new KDE branding where KDE is the community and Plasma is the desktop.]
<!--break-->
