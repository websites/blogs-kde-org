---
title:   "UK Linux Expo ready"
date:    2004-04-20
authors:
  - jriddell
slug:    uk-linux-expo-ready
---
The good news, Rich, is that the KDE stand at the UK Linux Expo is now ready, cos I just set it up :)

Thanks to SuSE for supplying a machine.  Various other SuSE bits were also supplied, does anyone have ideas for what to do with 4 copies of SuSE 8.2, a box full of SuSE Office-Pro CDs for SuSE 8.0 and 20 SuSE mouse mats?  Hopefully I can get a SuSE 9.1 CD so I can install software as needed, like the required en-gb translations.  Incidently the SuSE 9.1 widget theme is an interesting and quite pleasing mix between Plastik and Kermarik.

We have competition, for the first time that I've seen at an event in .uk the GNOMEs have a stall.  They havn't got any posters but they do have geekchicks, clever.  I'm quite tempted to set the background image on the demo machine to a photo I have of my girlfriend looking sweet in a KDE t-shirt.

As it happens the KDE stall, GNOME stall and to an extent the Debian stall have all happened because of the quiet (well, silent) Quaker geek mafia which is slowly (and silently) taking over Free Software.  It's funny when people think Quakers don't do technology although we all seem to have an avertion to mobile phones.

It would be nice if there were KDE supporters in London to lend monitors and odd bits that are always needed and drive them around places.  Quaker Paul has just walked in the door with a burst tyre, apparantly CRT monitors and Brompton bicycles don't get on.  Please contact <a href="http://www.kde.me.uk/index.php?page=kde-gb">KDE GB</a> if you can help in future.

Thanks also to Drochaid who printed 300 leaflets and half a dozen laminated A4 printouts of icons from KDE applications, they look really nice.

Very fashionable polo shirts on sale for &pound;18 and other quality merchandise available.  See you there, I'll be the one with the kilt and Irn-Bru.  
