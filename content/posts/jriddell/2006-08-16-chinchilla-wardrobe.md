---
title:   "Chinchilla Wardrobe"
date:    2006-08-16
authors:
  - jriddell
slug:    chinchilla-wardrobe
---
In the tradition of KDE Developers <a href="http://vizzzion.org/?blogentry=617">Sebas</a> and <a href="http://www.chrishowells.co.uk/">Chris</a> me and Krissy have converted this antique wardrobe into a Chinchilla cage.  The chinchilla's like it very much.

<p><img src="http://jriddell.org/photos/2006-08-16-chinchilla-wardrobe.jpg" width="300" height="400" class="showonplanet" /><br />
Eagle eyed readers may spot the two lines near the top on either side where we sawed the top off to get it up the stairs.</p>

<p><img src="http://jriddell.org/photos/2006-08-16-chinchillas.jpg" width="300" height="400" class="showonplanet" /><br />
<i>Gee, what do you want to do tonight?</i></p>
<!--break-->
