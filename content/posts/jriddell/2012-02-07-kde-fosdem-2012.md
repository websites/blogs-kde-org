---
title:   "KDE at FOSDEM 2012"
date:    2012-02-07
authors:
  - jriddell
slug:    kde-fosdem-2012
---
At the end of a long day here are some <a href="http://www.flickr.com/photos/jriddell/sets/72157629220786173">photos from KDE at FOSDEM 2012</a>.  Pradeepto says "3.24 AM here, am in office, those pictures made my day/night/whatever itis now".

<a href="http://www.flickr.com/photos/jriddell/6837751493/" title="DSCF6528 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7005/6837751493_4a2f507803.jpg" width="500" height="375" alt="DSCF6528"></a>
KDE Love as Claudia sells t-shirts

<a href="http://www.flickr.com/photos/jriddell/6837750385/" title="DSCF6525 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7148/6837750385_9d88cbf3e6.jpg" width="500" height="375" alt="DSCF6525"></a>
Paul demos KDE Software on every form factor: mobile, tablet, desktop, Windows, cloud and server.

<a href="http://www.flickr.com/photos/jriddell/6837758223/" title="DSCF6537 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7157/6837758223_1257bf8267.jpg" width="500" height="375" alt="DSCF6537"></a>
Corridor chat with Frank

<a href="http://www.flickr.com/photos/jriddell/6837759619/" title="DSCF6539 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7006/6837759619_e83e00f767.jpg" width="500" height="375" alt="DSCF6539"></a>
Cross-Desktop room group photo (missing lots of people who were at other talks)

<a href="http://www.flickr.com/photos/jriddell/6837761567/" title="DSCF6545 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7157/6837761567_9fba83b342.jpg" width="500" height="375" alt="DSCF6545"></a>
KDE dinner - had to turn away quite a lot of people who were too late to get a seat.  I may be concussed but I'm still able to herd KDE cats better than anyone else did.

<a href="http://www.flickr.com/photos/jriddell/6837772125/" title="DSCF6561 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7143/6837772125_b44a2e252d.jpg" width="500" height="375" alt="DSCF6561"></a>
A business man pose from Paul

<a href="http://www.flickr.com/photos/jriddell/6837756405/" title="DSCF6533 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7033/6837756405_c1042e18a3.jpg" width="500" height="375" alt="DSCF6533"></a>
Lydia Launches the <a href="http://open-advice.org/">Open Advice book</a> on which <a href="http://open-advice.org/author.html#Jonathan">I am a contributing author</a>

FOSDEM reminded me why I love KDE, great people and friends working on great technology.
<!--break-->
