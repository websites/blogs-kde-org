---
title:   "Free Software's Money Problems Solved"
date:    2005-02-07
authors:
  - jriddell
slug:    free-softwares-money-problems-solved
---
The news of a GPled Qt for Windows today let in a round about way to a discussion on IRC about funding Free Software development.  Would people pay for easily installable software? Maybe apt could include a payment system, after all that's what Lindows is trying to do.  Mobiles seem to manage something similar with ringtones somebody pointed out.  And then we realised KDE could do the same thing.  KNotify+KNewStuff+Paypal.  Genius.   1 euro for a new login sound.  2 euro for <a href="http://www.mms-sms.ch/lyrics/schnappi-das-kleine-krokodil-schnappi.asp">Schnappi</a> (the semi-official song of KDE 3.4 beta 1) to be your KMail new mail notify.  The plan can't fail.
<!--break-->
