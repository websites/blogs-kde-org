---
title:   "Fashion News: Kubuntu Knitware"
date:    2009-12-09
authors:
  - jriddell
slug:    fashion-news-kubuntu-knitware
---
Winter is here, the snow is falling, the frost is crisp each morning and it get progressively harder to break the ice for the day's canoe.

Fortunately Kubuntu has just the thing to keep you snug in these cold months. From the davmor2 knitware shop comes the Kubuntu jumper range.  Available in a selection of colours including beige with blue, beige with ligher blue and beige with cyan, it has been carefully crafted by designer extraordinaire Sue Morley over many months of hard work.  Order one now, it's the perfect fashion item to impress that special someone in your life for Christmas (possible 6 months waiting time from ordering).  

<img src="http://people.canonical.com/~jriddell/kubuntu-knitware.jpg" width="600" height="450" />

Hugs to Sue for being the best knitter I know.
<!--break-->
