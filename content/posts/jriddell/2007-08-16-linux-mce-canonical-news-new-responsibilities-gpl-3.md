---
title:   "linux mce, Canonical news, new responsibilities, gpl 3"
date:    2007-08-16
authors:
  - jriddell
slug:    linux-mce-canonical-news-new-responsibilities-gpl-3
---
<a href="http://linuxmce.org/">LinuxMCE</a> launched their new version 0704 today.  It's an addon CD to Kubuntu 7.04 which can set your your computer to serve media around the house or play it with their beautiful bling interface.  It can even run your home.  Watch the <a href="http://wiki.linuxmce.org/index.php/Video">video walkthrough</a> for what this baby can do (or just <a href="http://wiki.linuxmce.org/index.php/Features">read the features list</a>).

<hr />

<a href="https://shop.canonical.com/">Canonical Shop</a> is launched.  If shipit isn't sending you enough CDs, you can <a href="https://shop.canonical.com/product_info.php?products_id=79&osCsid=7cb10dde97ff6c776697b06b6bf3652f">now buy them</a>.  You can even buy support.

Canonical also announced the previously top secret <a href="http://canonical.com/landscape">Landscape</a> which lets you manage your computer (or 50,000 computers) from your web browser.  It ties in with Canonical's support offerings.

There's also a job going for an <a href="http://www.ubuntu.com/employment">External Project Developer Relations</a>, which means you get to be Jono's slave and talk nicely to our upstreams.

<hr />

Recently I became an <a href="https://launchpad.net/~ubuntu-archive">archive admin</a>.  It's full of exciting tasks like running syncs and backports.  The most interesting part is reviewing New queue which is about keeping the archive sane, the biggest worry being licencing.  Surprisingly few packages are a simple case of "it's all GPL, let it through".  So please remember to include a copy of the full GPL (or whatever licence you're using), add a copyright and licence declaration to the header of every file and include LGPL if any files use it (all KDE 3 programmes include some in admin/) and FDL if you have any documentation using that.  And don't do something daft like use GPL 3 but depend on the GPL 2 only Qt.

Talking of GPL 3, I did a <a href="http://techbase.kde.org/Policies/Licensing_Policy/Draft">draft update</a> of KDE's licencing policy and posted it to <a href="http://lists.kde.org/?l=kde-licensing&m=118713730807837&w=2">kde-licensing</a>.  So far very little support or otherwise.  Since this will be a major problem for distros in the coming months, I find that a little disappointing, hopefully it means that nobody objects.

The other responsibility I have taken on is release manager while Pitti goes to get wed, Tribe 5 is out next week so it'll be my test run.  Scary.

<!--break-->