---
title:   "Announcing Get Hot New Wallpapers"
date:    2005-03-10
authors:
  - jriddell
slug:    announcing-get-hot-new-wallpapers
---
Launching today for those of you lucky enough to have KDE 3.4 installed is... Get Hot New Wallpapers!

Using the excellent <a href="http://www.kstuff.org">Get Hot New Stuff</a> platform and its KDE library KNewStuff I added Get New Wallpapers button to the desktop configuration module.  It looks like KDE Look man Frank has now added the feed from kde-look.org so you can get the current 10 latest, greatest and most downloaded wallpapers at the touch of a button.

There's some improvements I ought to make for the next release, using aseigo's Hot New Button for example and making it update to the downloaded wallpaper automatically.  Also the feed mixes newest, highest rated and most downloaded so you get all three wallpaper selections together in the same list.  

And of course there's plenty more content on kde-look to mine for: icon themes, splash screens, colour schemes etc.  Widget styles would be harder since they are compiled.

Test it out and then think up some more places we could use Get Hot New Stuff.

<img src="http://muse.19inch.net/~jr/hot-new-wallpapers-wee.png" class="showonplanet" />
<!--break-->