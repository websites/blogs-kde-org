---
title:   "dapper, tango, UBZ"
date:    2005-10-28
authors:
  - jriddell
slug:    dapper-tango-ubz
---
<a href="http://linux.blogweb.de/archives/127-Dapper-Drake-Duck-or-Dragon.html">Stephan Hermann asks</a> why Mark likes dragons so much yet doesn't use KDE.  Seems Stephan wasn't on the IRC channel to spot this

<i>[Sabdfl] Riddell: so, I'm thinking of converting my desktop to kubuntu for dapper</i>

Which means the pressure is on to make dapper the most slick Kubuntu ever.

<br>

Over on planetkde there's been some Tango love from Tom and others.  Tango promotes itself as a cross desktop project but in the >6 months of its existance they told all of 1 KDE developer before going public, that's quite cheeky.  They are making a new icon theme which will be the next Gnome default icon theme.  They hope that KDE will adopt this theme too and we'll end up with a definitive Free desktop icon theme.  That's not going to happen, and not just because the icons look like Gnome icons without the border and with crystal shine, it's because KDE quite likes having an identity of its own.  

The other half of Tango is standards for artwork.  They want people to be able to download a theme and it'll work on all desktops. KDE already has a theme format which the tango people were interested in when I showed it to them.  Standardising on something like that is a good while off yet but Tango wants to start with an icon name spec, this is an excellent idea which is why it's been in discussion at freedesktop for ages.  (I've been meaning to look over it for a while, hopefully I'll get round to that soon.)  There's no real reason for a new project to be necessary to get the icon name spec agreed on and in use but if that's what it takes that's all good.

<br>

People have started arriving in Montreal for the Ubuntu Below Zero conference.  Whenever I go to France or Belgium I try to use some of the French that I spent 8 years learning but after a couple of sentences the francophone feels sorry for me and moves to English.  I was hoping that here they would be a bit more strict about insisting on French but so far they all seem to spot me as an Anglophone immediately.  

<!--break-->
