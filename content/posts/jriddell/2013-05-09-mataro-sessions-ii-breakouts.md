---
title:   "Mataro Sessions II: the breakouts"
date:    2013-05-09
authors:
  - jriddell
slug:    mataro-sessions-ii-breakouts
---
Notes from breakout on Homerun:

<ul>
<li>Needs to move to Extragear
<li>no upstream designer in Plasma so hard to change fundementals like app menu
<li>KWin would be against it as default, uses full screen blur, no visual consistency between it and dashboard
<li>Does not show app generic names
<li>netrunner says full screen is distracting for a menu
<li>no filters
<li>no recent apps
</ul>

Notes from breakout on High DPI sceens:

<ul>
<li><a href="https://community.kde.org/KDE/High-dpi_issues">see the wiki page</a>
<li>faking it with large font not really accurate, many more problems, see <a href="http://starsky.19inch.net/~jr/tmp/screenshot.png">screenshot</a>
<li>needs someone with a high dpi monitor to test and fix
</ul>

<a href="http://www.flickr.com/photos/jriddell/8721951840/" title="DSCF7590 by Jonathan Riddell, on Flickr"><img src="https://farm8.staticflickr.com/7286/8721951840_192b7f7d21.jpg" width="375" height="500" alt="DSCF7590"></a>
Post-it Kanban

<a href="http://www.flickr.com/photos/jriddell/8721947846/" title="DSCF7587 by Jonathan Riddell, on Flickr"><img src="https://farm8.staticflickr.com/7402/8721947846_43b910515b_n.jpg" width="320" height="240" alt="DSCF7587"></a>
The talks

<a href="http://www.flickr.com/photos/jriddell/8720836375/" title="DSCF7597 by Jonathan Riddell, on Flickr"><img src="https://farm8.staticflickr.com/7324/8720836375_ecc115e407_n.jpg" width="320" height="240" alt="DSCF7597"></a>
Acrobatics

<a href="http://www.flickr.com/photos/jriddell/8721958946/" title="DSCF7599 by Jonathan Riddell, on Flickr"><img src="https://farm8.staticflickr.com/7385/8721958946_d9be8a4c55_n.jpg" width="320" height="240" alt="DSCF7599"></a>
KDE People talking about KPeople
