---
title:   "about:konqueror"
date:    2005-02-01
authors:
  - jriddell
slug:    aboutkonqueror
---
Shiney new about:konqueror in CVS today.  KControl also updated.  Expect the docs to be updated soon too.  Is there a right-to-left script user out there who can tell me if this works properly on right-to-left?

<a href="http://muse.19inch.net/~jr/konqueror-about-wee.png"><img src="http://muse.19inch.net/~jr/konqueror-about-wee.png" class="showinplanet" /></a>
<!--break-->
