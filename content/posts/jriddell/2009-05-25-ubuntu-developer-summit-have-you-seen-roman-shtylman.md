---
title:   "Ubuntu Developer Summit, have you seen a Roman Shtylman?"
date:    2009-05-25
authors:
  - jriddell
slug:    ubuntu-developer-summit-have-you-seen-roman-shtylman
---
UDS is happening in Barcelona.  The hotel is super posh, which means the service is rubbish.  I was supposed to arrive on Saturday but they gave my room to some politician instead.  On Sunday my room wasn't ready until the evening.  This morning the pool tempted me and Sebas to go for a swim until an irate janitor started shouting at us in Spanish indicating that it wasn't open, I wonder why they bother having a pool.

But none of this matters because the Kubuntu croud at UDS is as lovely as ever.  Tonio was tired but managed to talk lots anyway.  Scott debated the merits of the server.  Roderick is busy packaging kblogger.  Ryan got let out of the country.  Yuriy's name I still seem to be unable to spell correctly.  Sebas got us kicked out of the pool, the rebel.

However we havn't found elite Ubiquity hacker Roman Shtylman, if he's around do poke him my way.
<!--break-->
