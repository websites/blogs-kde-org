---
title:   "Kubuntu on Twitter and Facebook"
date:    2014-06-13
authors:
  - jriddell
slug:    kubuntu-twitter-and-facebook
---
Slightly late to the game, Kubuntu now has a Twitter and Facebook account to join the <a href="https://plus.google.com/107577785796696065138/posts">Google+ account</a>.  New headlines will go there and we've a fancy account from the nice people at <a href="http://sodash.com/">SoDash</a> that makes it easy to interact.  Give us a Like or a Tweet.

<a href="https://twitter.com/kubuntu"><img src="http://upload.wikimedia.org/wikipedia/en/thumb/9/9f/Twitter_bird_logo_2012.svg/200px-Twitter_bird_logo_2012.svg.png" width="200" height="162" /> https://twitter.com/kubuntu</a>

<a href="https://www.facebook.com/kubuntu.org"><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Facebook.svg/220px-Facebook.svg.png" width="220" height="83" /> https://www.facebook.com/kubuntu.org</a>
