---
title:   "Plasma Tutorial at Ubuntu Developer Week"
date:    2009-08-31
authors:
  - jriddell
slug:    plasma-tutorial-ubuntu-developer-week
---
<a hre="https://wiki.kubuntu.org/UbuntuDeveloperWeek">Ubuntu Developer Week</a> is under way with a whole week of IRC sessions on a range of development topics.  Me and Aurelien are running a Write-Your-First-Plasmoid session at 20:00UTC today.  See you in #ubuntu-classroom
