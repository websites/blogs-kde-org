---
title:   "KDE's 4.6 Release Day"
date:    2011-01-26
authors:
  - jriddell
slug:    kdes-46-release-day
---
Today is <a href="http://www.kde.org/announcements/4.6/">KDE's 4.6 release day</a>.  Shiny new versions of KDE Platform, KDE Applications, Plasma Workspaces plus Digikam and KDevelop are available for installing.  See <a href="http://www.kubuntu.org/news/kde-sc-4.6">kubuntu announcement</a> for details.

ColinG says he's bringing "amber coloured drinks" for this evening's <a href="http://community.kde.org/Promo/ReleaseParties/4.6#Scotland">release supper</a>.

<img src="http://people.canonical.com/~jriddell/4.6-elegantly-yours.png" width="500" height="181" />
<!--break-->
