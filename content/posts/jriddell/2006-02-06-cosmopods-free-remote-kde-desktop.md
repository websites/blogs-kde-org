---
title:   "Cosmopod's Free Remote KDE Desktop"
date:    2006-02-06
authors:
  - jriddell
slug:    cosmopods-free-remote-kde-desktop
---
A while ago I did an <a href="http://dot.kde.org/1130593003/">article on Cosmopod</a> on KDE Dot News.  <a href="http://www.cosmopod.com">Cosmopod</a> do remote KDE desktops via NX supported by an advert down the side.  Before publishing I asked the Cosmopod man if he was sure he would be ready for the influx of users this would bring.  He said he was but 10,000 registrations later and the poor server came to a grinding halt.  Well they're now back up and running, the <a href="http://www.cosmopod.com/aboutus.php">About Us</a> page has some interesting comments on the future of computing.
<div style="border: thin solid grey; padding: 1ex; width: 300px">
<img src="http://static.kdenews.org/jr/cosmopod-wee.png" width="300" height="234" />
</div>
<!--break-->
