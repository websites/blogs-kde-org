---
title:   "Akademy 2012 in Pictures"
date:    2012-07-01
authors:
  - jriddell
slug:    akademy-2012-pictures
---
A life reaffirming gathering of KDE contributors is happening this week in sunny Tallinn in Estonia. Here's some pictures for those stuck at home

<img src="http://starsky.19inch.net/~jr/akademy-2012.png" width="400" height="178" />

<a href="http://www.flickr.com/photos/jriddell/7478523760/" title="DSCF6979 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7248/7478523760_054850b497_n.jpg" width="320" height="240" alt="DSCF6979"></a>
Pre-Akademy Hacker beer

<a href="http://www.flickr.com/photos/jriddell/7478529994/" title="DSCF7002 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8020/7478529994_56927a81d9_n.jpg" width="320" height="240" alt="DSCF7002"></a>
Some KDE developers arrived by lunar module

<a href="http://www.flickr.com/photos/jriddell/7478527972/" title="DSCF7015 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7264/7478527972_70583b45b8_n.jpg" width="320" height="240" alt="DSCF7015"></a>
Qt keynote opens Sunday

<a href="http://www.flickr.com/photos/jriddell/7478527354/" title="DSCF7017 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7250/7478527354_cbe73dca2c_n.jpg" width="320" height="240" alt="DSCF7017"></a>
Lining up for the group photo

<a href="http://www.flickr.com/photos/jriddell/7478527198/" title="DSCF6996 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8166/7478527198_062cdbf420_n.jpg" width="320" height="240" alt="DSCF6996"></a>
Hackerlabs are pretty quiet while talks are going on but some folks can't resist to use them

<a href="http://www.flickr.com/photos/jriddell/7478527594/" title="DSCF7001 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8020/7478527594_690d405a3a.jpg" width="500" height="375" alt="DSCF7001"></a>
Your friendly Kubuntu team at Akademy:
Rohan, Jonathan, Aurelien, Alex, Myriam, Tazz, Valorie, Clemens, Jussi
<!--break-->
