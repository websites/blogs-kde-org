---
title:   "Bazaar Explorer Talk in 2 hours"
date:    2011-09-05
authors:
  - jriddell
slug:    bazaar-explorer-talk-2-hours
---
I'm giving a talk on Bazaar Explorer as part of <a href="https://wiki.kubuntu.org/UbuntuAppDeveloperWeek">Ubuntu App Developer Weeks</a>.  Starts at 17:00UTC in #ubuntu-classroom on freenode IRC.
