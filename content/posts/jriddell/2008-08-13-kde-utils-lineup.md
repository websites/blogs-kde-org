---
title:   "KDE Utils Lineup"
date:    2008-08-13
authors:
  - jriddell
slug:    kde-utils-lineup
---
One of the release process improvements that happened with KDE 4 is the move to have module maintainers.  This has ment some of the lesser loved modules now have a greater sense of community which helps a lot for those apps which are otherwise developed all alone.  Here is the <a href="http://utils.kde.org/">kdeutils</a> members who were at Akademy.

<img src="http://www.kubuntu.org/~jriddell/akademy/kdeutils-lineup.jpg" width="400" height="267" />

Jonathan Riddell: <a href="http://utils.kde.org/projects/printer-applet/">Printer Applet</a> author.  Although secretly I just copied it from Red Hat.

Michael Leupold: <a href="http://utils.kde.org/projects/kwalletmanager/">KWallet</a> maintainer.  He knows all your passwords.

Friedrich Kossebau: <a href="http://utils.kde.org/projects/okteta/">Okteta</a> maintainer.  He uses Okteta to code Okteta.
                                                                                                                                                                                                                         
Daniel Laidig: <a href="http://utils.kde.org/projects/kcharselect/">KCharSelect</a>.  He has memorised unicode.

Michael Zanetti: Klirc maintainer.  He can control your computer without you knowing.
<!--break-->
