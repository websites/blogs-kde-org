---
title:   "13.04 to go Ahead"
date:    2013-03-06
authors:
  - jriddell
slug:    1304-go-ahead
---
Of all the nutty things Canonical has done in the last week wanting to drop 13.04 four months into development and two months before release is one of the more anti-social to the community who have been working on it.  Fortunately at the "UDS" session today I poked enough and we seem to have consensus that it'll go ahead on the schedule we agreed at UDS last October.  Feature freeze on Thursday my friends, two days to go.
