---
title:   "Akademy in Belgium"
date:    2008-08-11
authors:
  - jriddell
slug:    akademy-belgium
---
We have had a great couple of days talks here at Akademy in Belgium.  The energy in the KDE community is brilliantly high as we move from the long KDE 4 development period into having the world beating desktop.  

Favourite photos:

<img src="http://www.kubuntu.org/~jriddell/akademy-audience-wee.jpg" width="500" height="301" />
Wide angle lens

<img src="http://www.kubuntu.org/~jriddell/IMG_3771.JPG" width="500" height="333" />
One of us!

<img src="http://www.kubuntu.org/~jriddell/IMG_3888.JPG" width="500" height="333" />
Free waffles, it must be Belgium

<img src="http://www.kubuntu.org/~jriddell/IMG_3957.JPG" width="500" height="333" />
Hard work this KDE 

<img src="http://www.kubuntu.org/~jriddell/IMG_3974.JPG" width="500" height="333" />
Bart the dictator

<img src="http://www.kubuntu.org/~jriddell/seli-blue-hair.jpg" width="500" height="333" />
This is why I have <a href="http://blauzahl.livejournal.com/3675.html">a testimony against betting</a>.

<img src="http://www.kubuntu.org/~jriddell/round-robin-table-tennis.jpg" width="500" height="333" />
Round Robin Table Tennis

<img src="http://www.kubuntu.org/~jriddell/akademy-buddies.jpg" width="500" height="333" />
Bestest Buddies

<img src="http://www.kubuntu.org/~jriddell/akademy-drinks.jpg" width="500" height="333" />
Why Nokia, with this free Belgian beer you are really spoiling us no?

Best of all is the <a href="http://static.kdenews.org/jr/akademy-2008-group-photo.html">group photo</a> featuring Strigi enabled screensaver Plasmoid.

Photos from Johann Lapeyre and Bart Coppens
<!--break-->
