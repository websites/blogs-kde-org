---
title:   "Save the musiK!"
date:    2005-10-14
authors:
  - uga
slug:    save-musik
---
In case you haven't noticed, there's a nice cute little menu in amaroK's playlist which mentions the horrible word "delete". Yes sir, delete, as in "remove completely from your harddisk". The option is side by side with "remove from playlist" option, so loosing your files by mistake is so easy. (nobody reads warnings nowadays, and everyone knows that).

So, unless you want to end up like me, deleting tons of files by mistake (only saved by an http:// url in the playlist), I suggest you test the following patch. Sorry, I'm not welcome to apply the patch nor anything similar to svn, by the maintainers ;)

Save yer musiK!

<code>
Index: src/playlist.cpp
===================================================================
--- src/playlist.cpp    (revision 470659)
+++ src/playlist.cpp    (working copy)
@@ -2590,27 +2590,27 @@
     const int count  = urls.count();
     QString text;
     if (count == 1)  // remember: there are languages that use singular also for 0 or 2
-        text = i18n("<p>You have selected the file <i>'%1'</i> to be <b>irreversibly</b> deleted.")
+        text = i18n("<p>You have selected the file <i>'%1'</i> to be trashed.")
                     .arg(static_cast<PlaylistItem*>( currentItem() )->url().fileName() );
     else
-        text = i18n( "<p>You have selected one file to be <b>irreversibly</b> deleted.",
-                     "<p>You have selected %n files to be <b>irreversibly</b> deleted.", count );
+        text = i18n( "<p>You have selected one file to be trashed.",
+                     "<p>You have selected %n files to be trashed.", count );

     int button = KMessageBox::warningContinueCancel( this,
                                                      text,
                                                      QString::null,
-                                                     KStdGuiItem::del() );
+                                                     i18n("&Move to Trash") );

     if ( button == KMessageBox::Continue )
     {
         // TODO We need to check which files have been deleted successfully
-        KIO::DeleteJob* job = KIO::del( urls );
+        KIO::CopyJob* job = KIO::trash( urls );
         connect( job, SIGNAL(result( KIO::Job* )), SLOT(removeSelectedItems()) );

         job->setAutoErrorHandlingEnabled( false );

         amaroK::StatusBar::instance()->newProgressOperation( job )
-                .setDescription( i18n("Deleting files") );
+                .setDescription( i18n("&Trash") );

         // we must handle delete errors somehow
         CollectionDB::instance()->removeSongs( urls );
@@ -3145,8 +3145,8 @@

     popup.insertItem( SmallIconSet( "edittrash" ), i18n( "&Remove From Playlist" ), this, SLOT( removeSelectedItems() ), Key_Delete, REMOVE );
     popup.insertItem( SmallIconSet( "editdelete" ), itemCount == 1
-            ? i18n("&Delete File")
-            : i18n("&Delete Selected Files"), this, SLOT( deleteSelectedFiles() ), SHIFT+Key_Delete, DELETE );
+            ? i18n("&Move to Trash")
+            : i18n("Move &Selected Files to Trash"), this, SLOT( deleteSelectedFiles() ), SHIFT+Key_Delete, DELETE );
     popup.insertSeparator();

     popup.insertItem( SmallIconSet( "info" )
</code>