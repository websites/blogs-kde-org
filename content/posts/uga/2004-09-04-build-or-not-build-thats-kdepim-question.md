---
title:   "To build, or not to build, that's a kdepim question"
date:    2004-09-04
authors:
  - uga
slug:    build-or-not-build-thats-kdepim-question
---
I thought this would be a good moment to blog. So you ask .. why? Well, I began rebuilding kde from scratch, and I reached to kdepim. Yes, you guessed. I spent the last 2 hours fixing the build, and still haven't fixed it :) First it was unsermake problem, now kconfigs don't build, dirs don't get build in the right order... So I got no e-mail, no konvi, addressbooks ... to sum it up, I'm un-PIMized, disconnected from the rest of the world

Of course, while fixing this I delayed building the rest of packages, and that includes kdevelop, which limits my ability to code on keepit. Kate is nice, but it won't help as much as kdevelop. So I got nothing better to do than blogging until I fix this mess

Those guys under #kontact don't care about my comments,... so what will I do? Shall I commit my changes and say them "screw you"? Sometimes one feels like nobody cares about the golden rule "you should at least make sure it builds before you commit"
