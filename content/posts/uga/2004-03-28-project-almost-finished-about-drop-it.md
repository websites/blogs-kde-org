---
title:   "Project almost finished, About to drop it"
date:    2004-03-28
authors:
  - uga
slug:    project-almost-finished-about-drop-it
---
It's not easy to explain why one begins doing opensource code. For once,  you find some good software, and can't resist the temptation to look at its code and improve it. On the other hand, it's a good means to learn coding, specially working on team projects. But one of the most important factors is probably that someone is using your code, and thinks its useful.

That's how I began on this little themes project. Nobody wanted to get involved, and it wasn't a difficult project on its own. I could deal with it. More importantly, every user seemed to want it. So why not? I began coding in my little spare time. The project has been slow, mainly because of my lack of time for it. But yes! the code is there, almost finished in cvs now.

So why? Why am thinking of dropping it? Maybe because I'm missing that important factor I mentioned above... nobody really cares. There are a few dependancy and library issues to be solved, and the code won't get into a KDE module until those are solved. But nobody cares about it! I can spend my time doing thousands of lines of code, and I'll never know if I'll be allowed to get this code into KDE.

So basically right now I'm not in the mood to continue with this code. If somebody wants to pick up the project, go on. The code is basically "done", and I think the comments help understanding it. I myself will continue in krecipes. I have enough work there.
