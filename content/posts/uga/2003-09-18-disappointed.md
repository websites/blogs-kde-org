---
title:   "Disappointed"
date:    2003-09-18
authors:
  - uga
slug:    disappointed
---
So yes, another step back in the Krecipes usability. SQLite is nice, easy to install, easy to code... but in two (or 3) words: "it's crap". Slower than a cow trying to climb up a ladder.

It's convenient when the lists are small and no strange queries are done, but give it a bit of search work, and it's useless. MySQL could do a query over 3000 recipes in 0.12s and took SQLite 1 minute to complete the same thing.... I can't say that's useful.

Somebody will say that having 3000 recipes is not usual... Well it is by other recipe tool users. Those 3000 recipes were imported just from a single file.

I'm now thinking of using embedded MySQL, but hey, distros like Mdk forget about this tiny library that should be included by default. Why???

If I can't solve this stuff... well, krecipes has its days counted. Well, at least for moms that do not know about passwords and usernames.