---
title:   "kde, compiz and alt+f2: lets make it possible"
date:    2006-08-29
authors:
  - uga
slug:    kde-compiz-and-altf2-lets-make-it-possible
---
Yeah, I fell into the trap too. It's a toy, just a toy. But I can't ignore eyecandy  =)

There's some annoying 'g' dependancy on it for now, but I couldn't resist. Most of you will also have experimented with it, and possibly found that your dear <i>minicli</i> is no more accessible through Alt+F2 shortcut. Of course, this is unacceptable for me, given I use it at least 99 times a day. And possibly annoying for you too, right? 

After digging myself for quite some time, and desperate not to find anything, not even a single cheap trick from the SuSE guys promoting all this <i>(maybe it's just google gone bad?)</i> , I thought... okay,  you've got DCOP there, ... it can't be that difficult... and indeed it's was soooo simple at the end... Now I wonder why such a nice tool is being replaced ;)

Anyway, straight to the point. Here are the instructions:

Open GRegedit. Sorry, errrm... gconf-editor, I mean. And go into apps->compiz->general->allscreens->options
Find entry "command0", and type in.... "<b>dcop kdesktop KDesktopIface popupExecuteCommand</b>"
on the "run_command0_key", enter...  "&lt;Alt&gt;F2"

There will be another entry with the same Alt+F2 shortcut, and that's "run_key" entry. I haven't figured out how to setup the run application, so I just ditched this one, so that there was no shortcut conflicts. Set it to "&lt;Alt&gt;foo" "Disabled", or whatever you please there.

Et voilá. KDE (3) is great again, <strike>will KDE (4) be even greater?</strike> and KDE (4) will be even greater!

Just my 2 euro cents.
