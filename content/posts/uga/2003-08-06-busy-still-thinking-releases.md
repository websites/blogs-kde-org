---
title:   "Busy, but still thinking of releases"
date:    2003-08-06
authors:
  - uga
slug:    busy-still-thinking-releases
---
So yes, I'm busy, but still I managed to get some time for the Krecipes development. Things seem to go fine, and I expect a release in 2 weeks may be (cross fingers).

I'm now addressing the biggest issue: MySQL. Krecipes users don't know how to set it up, and things seem to vary between distros and obviously there are also source installations. So this thing may bring me more problems than solutions, but I'll be adding another part to the wizard to set up MySQL automagically.

This thing won't last for long though. SQLite is out there waiting for me to use it, so 0.4 will come out with a new DB backend support. Bugix has created a Qt based API for it, so the port shouldn't take more than a couple of days (oooookay, .... lets say a week at most).

Anyway, I'm happy since most things that I wanted are already becoming reality in the app. One can already define recipe authors, and classify recipes in categories with the cvs version,  and I'll be simplifying the process of creating new ingredients soon.  What else does one need?

Oh, sorry, I need to go now, my lasagne is in the oven ;-)