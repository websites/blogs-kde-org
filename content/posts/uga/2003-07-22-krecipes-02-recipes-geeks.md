---
title:   "Krecipes 0.2: Recipes for geeks"
date:    2003-07-22
authors:
  - uga
slug:    krecipes-02-recipes-geeks
---
I need more testers for this little beast, guys. Or is it that there're no bugs left? :-) Just to let you know, this version includes two delicious recipes from and for geeks. One by Adriaan de Groot, and another one by me. Even developers cook sometimes...

Anyone is encouraged to submit recipes for the next version. I think 10 sample recipes would be enough. And please, make them short... (you know... i18n)