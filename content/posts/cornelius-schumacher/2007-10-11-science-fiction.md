---
title:   "Science Fiction"
date:    2007-10-11
authors:
  - cornelius schumacher
slug:    science-fiction
---
As <a href="http://aseigo.blogspot.com/2007/10/bleh.html">Aaron is reading Spook Country</a> I also wanted to chime in with some Gibsoness. I finished <a href="http://www.amazon.com/Spook-Country-William-Gibson/dp/0399154302/ref=pd_bbs_sr_1/103-2557430-6258222?ie=UTF8&s=books&qid=1192059206&sr=8-1">Spook Country</a>, the latest novel of <a href="http://www.williamgibsonbooks.com/">William Gibson</a>, a couple of weeks ago. It's a very stylish work of art with lots of amazingly sharp ideas. I really enjoyed reading it. His concept of locative art is fascinating (and I want a magnetically elevated bed as well ;-)).

I guess Gibson really is my favorite author. I even own a signed copy of <a href="http://www.amazon.com/Pattern-Recognition-William-Gibson/dp/B000MGAHY6/ref=pd_bbs_2/103-2557430-6258222?ie=UTF8&s=books&qid=1192059206&sr=8-2">Pattern Recognition</a> which I bought in a book store in Provo, Utah, a couple of years ago. My all time favorite still is <a href="http://www.amazon.com/Mona-Lisa-Overdrive-William-Gibson/dp/0553281747/ref=pd_sim_b_5_img/103-2557430-6258222">Mona Lisa Overdrive</a>, though. I come back reading it again and again.

Despite my passion for Gibson I'm actually not a very big science fiction fan. So many books of this genre just feel too artifical. One of the exceptions is <a href="http://www.amazon.com/Rainbows-End-Vernor-Vinge/dp/0812536363/ref=pd_bbs_sr_1/103-2557430-6258222?ie=UTF8&s=books&qid=1192059905&sr=1-1">Rainbows End</a> by Vernor Vinge which I'm reading right now. It's a view into the future. It's disturbing and fascinating at the same time to think that it might actually come true. Come back in 20 years and I will report if this happened or not.
<!--break-->