---
title:   "How to access a REST web service?"
date:    2008-08-25
authors:
  - cornelius schumacher
slug:    how-access-rest-web-service
---
When thinking about how to implement a client for the <a href="http://www.open-collaboration-services.org/">Open Collaboration Services</a> API which Frank presented at this year's <a href="http://akademy2008.kde.org">Akademy</a> <a href="http://akademy2008.kde.org/conference/slides/socialdesktop.pdf">keynote</a> I came across the question of how to generally access a REST web service on a client.

As REST provides a simple way to access services by using the standard HTTP mechanism for which plenty of frameworks exist, there are a lot of ad-hoc implementation for a specific service, but is there a generic solution which encapsulates all the small annoyances you run into when doing it by hand? I would particularly be interested in a solution in the context of C++ and KDE. Does anyone have a suggestion or any pointers, preferably to existing code?

One generic solution is <a href="http://api.rubyonrails.org/files/vendor/rails/activeresource/README.html">ActiveResource</a> which is part of <a href="http://www.rubyonrails.org/">Ruby on Rails</a>. This is nice because it encapsulates not only the HTTP transfer, but also the mapping of XML documents to native model objects. Ruby as dynamic language makes this very easy and makes it possible to provide an elegant API by creating object methods on the fly based on the content of the exchanged data. In C++ this is much harder. ActiveResource also has the drawback that it by default makes some assumptions about how the REST API is structured, so it works best with standard web services implemented in Rails, but for services which don't follow the Rails conventions it becomes a bit harder.

Of the custom implementations I also wrote one as part of FATE, the <a href="http://www.cornelius-schumacher.de/talks/SuseFeatureManagement_Froscon.pdf">SUSE feature tracking system</a>. It's pretty fancy with full read and write access, offline mode and an advanced caching scheme for optimal performance even over slower connections. But it's still custom and adapting it to other service APIs would be quite some work, especially with the model classes.

The <a href="http://www.lst.de/~cs/kode/index.html">Kode projects</a> includes an approach to accessing SOAP based web services in a generic way. It contains kwsdl_compiler which generates code from a WSDL description to access the corresponding web service. As WSDL 2.0 supports REST style web services, it might be an option to extend it to also cover native KDE interfaces for REST web services. But somehow I'm hesitant to even think about this as WSDL really is a beast.

As pretty much every web site that takes a bit of proud in being relevant implements a REST API to access its data and functionality, be it <a href="http://code.google.com/apis/gdata/overview.html">Google</a>, <a href="http://developer.amazonwebservices.com/connect/entry.jspa?externalID=1661&categoryID=19">Amazon</a>, <a href="http://developer.ebay.com/products/shopping/">eBay</a>, <a href="http://wiki.developers.facebook.com/index.php/API">Facebook</a> or <a href="http://apiwiki.twitter.com/REST+API+Documentation">Twitter</a>, there are numerous implementation which use some low-level HTTP way of accessing these APIs. This is fine, but looking at some of these implementations shows that this still needs quite some code. I really would like to have something where the developer doesn't have to think about how to access the service, HTTP and object translations at all.

So what's the best way to access a REST based web service from KDE? Ideas and suggestions are <a href="mailto:schumacher@kde.org">very welcome</a>.
<!--break-->