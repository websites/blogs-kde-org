---
title:   "Joel is wrong"
date:    2004-09-17
authors:
  - cornelius schumacher
slug:    joel-wrong
---
Many software developers stumble over the <a href="http://www.joelonsoftware.com">"Joel On Software"</a> columns at some time and like them. Sure, Joel was a Microsoft employee and he develops proprietary software, but still his columns are inspiring and fun to read. Sometimes he is completely wrong, though.

Yesterday I read Joel's column about <a href="http://www.joelonsoftware.com/articles/BuildingCommunitieswithSo.html">"Building Communities with Software"</a> where he says, when talking about Usenet:

<i>Quoting, with the ">" symbol, is a disease that makes it impossible to read any single thread without boring yourself to death by re-reading the whole history of a chain of argument which you just read in the original, seconds ago, again and again and again. Shlemiel the Painter reading.</i>

How can someone be so wrong?

Quoting is the essential tool to make emails into a communication rather than just unrelated talking to a wall. Sure, good quoting isn't easy, just as most other aspects of communication aren't easy, but without that newsgroups, mailing list or internet forums are worthless for people actually intending to share and discuss thoughts, arguments or whatever instead of only spreading words.

Emails are most often not read in the context of a thread but as they arrive. When you read hundreds of mails a day can you remember the related mails of one specific mail? Certainly not. So having the context of a reply as quote saves the time to look up the mail which is replied to. This can really be a very significant time saver when you get many mails as it's the case for most of us who are heavily involved in internet communities.

Another reason is that most replies don't reply to a mail as a whole but to certain parts of the mail. You could do this by writing some text explaining the reference, but isn't that kind of ridiculous when this can be done automatically by the mail client?

Finally quoting easily allows to perform "line-by-line nitpicking" which is a good thing when you want to get something right, especially when discussing development questions. You could use other tools for that like an issue-tracking system, but for simple things which can be done informally email is such a great medium that it would be overkill to use anything else. I really like these threads where somebody posts for example a list of software problems and somebody replies (of course with proper quoting) with "Fixed, fixed, needs thought, somebody else knows better, what exactly do you mean?" and so on. This is efficient and wouldn't be possible without quoting.

The drawback of quoting is that you have to do it right. You have to <a href="http://www.netmeister.org/news/learn2quote.html">learn quoting</a>. Many people get it wrong and quote too much text or don't reply where the quote is they are referring to. The basic rule is "Quote less". If you follow that, think of the people reading your replies and use some common sense quotes shouldn't create any problems and if you then also use an email client which handles quotes in a decent way like formatting quotes correctly or coloring quoted text then quoting is the most efficient way to create actual communication by email.

Equally important to learning to quote is learning to write quotable text. Mangling different thoughts into one big block of text will make it hard to refer to only parts of it. Too few line breaks make it hard to pick only the text which is replied to as a quote. In the end well-strucured text is not only easier to quote but also easier to read, so you will get a double benefit from getting this right.

Oh, and after writing all that I actually had a look at the discussion forum at Joel's site. I was impressed by the quality of the posts and when I saw that a whole thread is put on a single web page I also finally understood Joel's aversion against quoting. In the context of his discussion forum it really isn't helpful. But in the context of Usenet and Email it is essential.

This makes me wonder if a single page thread view in a mail client would be a useful feature. It could fold the quotes, so that no text is duplicated. Showing non-linear threads might be difficult in such a view, though.

Ok, enough ranting, back to coding...
