---
title:   "Surprise Features"
date:    2007-09-10
authors:
  - cornelius schumacher
slug:    surprise-features
---
One of the nice aspects of being a software developer is that sometimes users come up with using your software in creative ways you never have thought of. They discover <b>surprise features</b>. I particularly like these because they show that you have great users and they often also are a sign that you took some right design decisions.

Some time ago a user discovered such a surprise feature in the combination of KNotes and KDesktop. He implemented an <a href="http://ezinearticles.com/?Time-Management---Some-Philosophical-and-Practical-Considerations&id=102414">Eisenhower Matrix</a>, which is a matrix consisting of four quadrants ordering task by importance and urgency. He did this in a truly creative way by using a special desktop background representing the matrix and putting in the tasks in the form of KNotes notes. Amazing!

[image:2986 size=original hspace=100]

For putting a task into the matrix you have to decide if it has high or low importance and if it has high or low urgency. This way you get four possible combinations which are reflected by the four quadrants. Now the usual way to address these tasks is as follows: The tasks which are unimportant and not urgent are discarded. That's the waste. The tasks which are not important but urgent are delegated to somebody else who considers them to be more important. The tasks which are urgent and important you handle yourself immediately and the tasks which are important but not urgent you plan for doing yourself.

Especially when it comes to planning, the surprise feature implementation reaches its limits. Here it would be nice to have some integration with KOrganizer to keep track of deadlines and support time planning. This could for example be a special view in KOrganizer managing tasks in the four quadrants, or maybe KNotes could be adapted to be able to associate or represent KOrganizer tasks in notes. This certainly would be a fun project. If somebody feels inspired to work it, don't wait, the <a href="http://www.ohloh.net/projects/6552?p=KOrganizer+%28KDE%29">code base</a> is ready to get some creative treatment.

By the way, the name of the matrix goes back to a quote of the former president of the United States <a href="http://en.wikipedia.org/wiki/Dwight_D._Eisenhower">Dwight D. Eisenhower</a>:

<code>
"Most things which are urgent are not important, and most things which are important are not urgent."
</code>

<!--break-->
