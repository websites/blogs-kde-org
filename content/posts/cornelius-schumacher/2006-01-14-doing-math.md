---
title:   "Doing the math"
date:    2006-01-14
authors:
  - cornelius schumacher
slug:    doing-math
---
I'm here at the OpenSync meeting in Amsterdam and we were talking about when KDE 4 might be released. We discussed how long it took for previous Qt releases to be adapted by KDE, but didn't really get the dates together. So I decided to dig into the press archives, collect the numbers and do the math. Here are the results:

<ul>
<li><a href="http://lists.trolltech.com/qt-interest/1996-09/thread00080-0.html">Qt 1.0: September 24 1996</a></li>
<li><a href="http://www.trolltech.com/newsroom/announcements/00000064.html">Qt 2.0: June 25 1999</a></li>
<li><a href="http://www.trolltech.com/newsroom/announcements/00000075.html">Qt 3.0: October 15 2001</a></li>
<li><a href="http://www.trolltech.com/newsroom/announcements/00000209.html">Qt 4.0: June 28 2005</a></li>
</ul>
<ul>
<li><a href="http://www.kde.org/announcements/announce-1.0.php">KDE 1.0: 12 July 1998</a></li>
<li><a href="http://www.kde.org/announcements/announce-2.0.php">KDE 2.0: 23 October 2000</a></li>
<li><a href="http://www.kde.org/announcements/announce-3.0.php">KDE 3.0: 3 April 2002</a></li>
<li><a href="http://images.google.com/images?q=crystal%20ball">KDE 4.0: ???</a></li>
</ul>

That means it took us 1 year and 10 months to adapt Qt 1, 1 year and 4 months to adapt Qt 2, and 6 months to adapt Qt 3.

Extrapolating this sequence gets us negative numbers which would mean to theoretically release KDE 4 before Qt 4. This obviously can't happen.

So let's take into consideration how big the change between the different Qt versions was. Qt 1/2 took 2 years and 9 months. Qt 2/3 took 2 years 4 months, Qt 3/4 took 3 years and 8 months. Taking the average of the relation of KDE porting time to Qt release time (a factor of 0.48 for KDE 2 and 0.21 for KDE 3 giving a factor of 0.35 for KDE 4) we get an estimated release date for KDE 4. It's 15 months after Qt 4, in September 2006. Now that sounds at least possible...
