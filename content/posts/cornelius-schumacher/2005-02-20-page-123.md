---
title:   "Page 123"
date:    2005-02-20
authors:
  - cornelius schumacher
slug:    page-123
---
<p>Found on <a href="http://www.inkstain.net/fleck/archives/001577.html">Planet GNOME</a>:</p>

<ol>
<li>Grab the nearest book.</li>
<li>Open the book to page 123.</li>
<li>Find the fifth sentence.</li>
<li>Post the text of the sentence in your journal along with these instructions.</li>
<li>Don't search around and look for the "coolest" book you can find. Do what’s actually next to you.</li>
</ol>

<p>The result: <b>"To a novelist, there is no such thing as a 'good' sentence."</b>. Can you guess, which book this was?</p>

