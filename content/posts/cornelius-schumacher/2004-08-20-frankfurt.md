---
title:   "Frankfurt"
date:    2004-08-20
authors:
  - cornelius schumacher
slug:    frankfurt
---
A couple of weeks ago I was waiting at the Frankfurt airport for a connection flight to N&uuml;rnberg. I had three hours to wait and so I was sitting at the gate hacking on <a href="http://www.kde-apps.org/content/show.php?content=14501">Plutimikation</a>. At some time a guy waiting at a table nearby for another flight came to me and asked if I would be a KDE developer. I was surprised. It turned out that he was a greek translator and recognized me because of the KDE logo on my bag. It's amazing that the KDE community now seems to be big enough so that you can meet KDE people by accident at some random place in the world. The project world domination seems to proceed well ;-)

That's it for now. I'm now heading for Ludwigsburg. I'm really looking forward to an exciting ten days on the biggest KDE meeting ever. See you there.
