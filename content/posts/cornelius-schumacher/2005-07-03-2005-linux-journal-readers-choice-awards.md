---
title:   "2005 Linux Journal Readers' Choice Awards"
date:    2005-07-03
authors:
  - cornelius schumacher
slug:    2005-linux-journal-readers-choice-awards
---
The final round of voting for the <a href="http://www.linuxjournal.com/article/8272">2005 Linux Journal Readers' Choice Awards</a> has begun. <a href="http://www.kde.org">KDE</a> is nominated in no less than <a href="ftp://ftp.ssc.com/pub/lj/Web/RC/8272.txt">eight categories</a>. What I especially like is that individual KDE application are nominated together with the top open source projects. <a href="http://www.kdevelop.org">KDevelop</a> is competing with GCC and Eclipse in the development tool category. <a href="http://kate.kde.org">Kate</a> runs with Vim and Emacs for the best text editor. <a href="http://kontact.kde.org">Kontact</a> is nominated with LaTex and OpenOffice.org for the office award. That shows again that KDE is not only a nicely integrated desktop, but also provides a fantastic environment to create world-class software projects. The final round closes 28 July 2005. <a href="ftp://ftp.ssc.com/pub/lj/Web/RC/8272.txt">Vote now!</a>
