---
title:   "Progress in Osnabrück"
date:    2006-01-07
authors:
  - cornelius schumacher
slug:    progress-osnabrück
---
We are making progress here in Osnabr&uuml;ck at the KDE PIM meeting. We had lots of interesting discussions yesterday, throwing around crazy ideas and collecting all kind of requirements for the new PIM Storage Service. During this process we also came up with a mission statement and a name for the project:

<b>Akonadi - The PIM Storage Service</b>

<i>We intend to design an extensible cross-desktop storage service for PIM data and meta data providing concurrent read, write, and query access. It will provide unique desktop wide object identification and retrieval.</i>
<!--break-->
Akonadi is the name of an oracle goddess of justice in Ghana, by the way.

You can find some documentation of what's happening here at the <a href="http://pim.kde.org/playground/osnabrueck4/">Osnabr&uuml;ck 4 section on the pim.kde.org playground.
