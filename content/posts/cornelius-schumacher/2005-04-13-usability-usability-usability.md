---
title:   "Usability, Usability, Usability"
date:    2005-04-13
authors:
  - cornelius schumacher
slug:    usability-usability-usability
---
Usability is one of my favorite buzzwords. It sounds great, people <a href="http://lists.kde.org/?l=kde-usability&r=1&w=2">get emotional</a> about it and it even has some <a href="http://en.wikipedia.org/wiki/Usability">real meaning</a>. In addition to that it's a very interesting area to work on in KDE. With initiatives as <a href="http://www.openusability.org">OpenUsability</a> or <a href="http://dot.kde.org/1112736422/">APPEAL</a> it gets more and more focus and we have structures in place which enable us to actually succeed in making KDE the most usable desktop one can imagine.

The technical base of KDE is excellent. Our framework allows for amazingly productive development. The question now is: How do we deliver this technical excellence of KDE to the user? How do we make sure that our applications not only have lots of cool features and great technical implementations, but the user is also able to easily use all of this to reach his actual goals?

One of the answers lies in the framework itself which enforces some consistency through technical means, e.g. by common dialog base classes or standard schemes for menus. This is a start, but it's by far not enough. Much more important are the efforts which provide guidance about how the user interface should actually look like and behave, like the <a href="http://developer.kde.org/documentation/standards/kde/style/basics/index.html">KDE User Interface Guidelines</a> or the upcoming <a href="http://wiki.kdenews.org/tiki-index.php?page=KDE+HIG">Human Interface Guidelines</a> for KDE 4.

The most promising effort I see is the cooperation with the <a href="http://www.openusability.org">OpenUsability</a> project. It provides a platform for getting usability professionals and open source developers together. This is a great opportunity for both parties. We get qualified input about the usability of our applications and the usability people get the chance to see direct results of their work, much more direct than with any proprietary software. For example in KDE PIM we already had some quite <a href="http://www.openusability.org/reports/?group_id=55">good results</a> with this collaboration.

But working on usability provides several special challenges. One of them is that, unlike with programming, everybody seems to feels as an expert (The "I watched my grandmother using KDE and she wasn't able to configure the pointer threshold of the mouse. That proves that the usability of KDE is horrible." type of argument). It's hard to cope with that. You need a thick skin and good arguments. Fortunately the KDE usability efforts provide the latter.

Another challenge is that almost all users are resistive to change. If we change the interface somewhere it's certain that some people will complain, even if it's an obvious improvement. That's the problem of overcoming old habits, but it also means that user interface changes have to be well-justified and really need to provide an improvement. On example about resistance to change can be found in <a href="http://alweb.dk/node/66">Anders' comments on the KMail recipients picker</a>. As Ingo already has <a href="http://alweb.dk/node/66#26">pointed out</a> most of the issues aren't real problems. But Anders has a point about the missing availability of categories in the recipients picker, so I <a href="http://lists.kde.org/?l=kde-cvs&m=111330604121433&w=2">fixed that</a>.

All in all, I'm quite sure we are on the right track to take the usability of KDE to the next level. That feels good :-)
