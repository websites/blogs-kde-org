---
title:   "Happy Birthday KDE"
date:    2006-10-14
authors:
  - cornelius schumacher
slug:    happy-birthday-kde
---
It's party time. Today we celebrate the tenth birthday of <a href="http://www.kde.org">KDE</a>. It all started with the <a href="http://www.kde.org/announcements/announcement.php">famous post by Matthias Ettrich</a> on October 14th 1996 calling for programmers to create a piece of free software he called KDE. The mission: "The idea is to create a GUI for an ENDUSER. Somebody who wants to browse the web with Linux, write some letters and play some nice games." Ten years later we have grown an amazing community of hundreds of developers and millions of users which made big parts of Matthias' original vision become reality.

[image:2444 size=original hspace=80]

I was looking for a way to illustrate the progress we made and what happened in this ten years of KDE. But that's not too easy. Getting software from ten years ago to run on a current computer seems to be nearly impossible. So I digged up the oldest screenshot of a program I worked on that I could find, a KOrganizer version from 1998, this was just before KDE 1.0. To give some contrast I also made a screenshot of the current version of Kontact. You can see the difference and if you take into account what happened behind the screens in the framework and the community it's absolutely amazing what free software can achieve in a time span of ten years.

[image:2445 size=original hspace=80]

So today is a happy day. I can't be at the <a href="http://events.kde.org/10years/">10 years KDE</a> event, but I'm sure that there is some heavy partying going on. All I want to say is: Happy Birthday, KDE. I'm proud to be part of this community.
<!--break-->