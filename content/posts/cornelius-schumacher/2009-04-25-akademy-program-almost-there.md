---
title:   "Akademy program, almost there"
date:    2009-04-25
authors:
  - cornelius schumacher
slug:    akademy-program-almost-there
---
The Akademy program is almost done. Speaker notification deadline was yesterday, but we are still busy sorting out some last details and haven't sent notifications yet. Please bear with us and have a bit more patience. We have a lot of great proposals, more than we can fit into the schedule. So it's not easy to decide what we can take, and the co-hosting with GUADEC adds another dimension of complexity to this task. But we are on a good track, and we will have a fantastic program. Stay tuned...