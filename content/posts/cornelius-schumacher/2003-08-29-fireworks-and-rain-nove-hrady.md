---
title:   "Fireworks and rain at Nove Hrady"
date:    2003-08-29
authors:
  - cornelius schumacher
slug:    fireworks-and-rain-nove-hrady
---
This evening there was a great firework directly in front of the castle in Nove Hrady where we are sitting and hacking. That's a stylish ending for a great week at a stylish location. After having sunshine all the days before it begin raining tonight. Seems like it's time to go home. Many people already have left and I'm going back to Germany with the KDE tour bus tomorrow morning.
<br><br>
We accomplished a lot of things here. The KDE e.V. membership meeting finally managed to pass the new bylaws, we had a great conference with lots of interesting talks (The highlight certainly was Kalles talk about KDE history with lots of funny and some embarassing pictures) and we had lots of time for hacking, discussing and having fun.
<br><br>
One very nice thing is that there were lots of kdepim developers here. The applications and also the developers are moving closer together. This gives kdepim an increasingly strong identity. Even Don and Marc got well along. There is not much reason left for conflicts. That's not surprising. Beer diplomacy works.
<br><br>
Kontact is progressing. I integrated KitchenSync and fixed the about dialog with help from Simon. KOrganizer now provides its configuration as KCModules which can be included in the common Kontact configuration dialog. Matthias was ferociously hacking on KConfigureDialog which will automatically integrate all the config modules for Kontact including plugin configuration. It will also serve as a convenient way for other applications to provide a configuratin dialog. David worked on the toolbar problems and Tobias did a lot of work on libkabc and KAddressBook. Reinhold reorganized the KOrganizer printing support and the KMail guys got some improvements and bug fixes into KMail. There still are some issues, but most of them are solvable before the 3.2 release.
<br><br>
The biggest problem currently is that the kroupware branch still isn't merged into HEAD. I hope that Bo will take this over and integrate the missing code, so that Kontact will become a fully usable client to Martins Kolab server. As a fallback we still have the option to make a separate kdepim release after 3.2 when the merge is finished.
<br><br>
I also spent a lot of time on KitchenSync. Lots of cleanups, integration into Kontact, a new backup/restore part, bugfixes and now I'm trying to get the Qtopia Konnector to work again. But I'm still not sure if KitchenSync will be ready for 3.2 release. Let's see what the next few weeks bring.
<br><br>
Josef hacked on KNewStuff and I talked with Frank about providing some of the cool stuff on www.kde-look.org via the "Get Hot New Stuff" feature. This also seems to work out well.
<br><br>
Now I'm tired. Thanks to all the people organizing this event and all the KDE contributors who were here and made this meeting such a great success. See you next year :-)