---
title:   "KDE 4 Progress"
date:    2007-10-21
authors:
  - cornelius schumacher
slug:    kde-4-progress
---
It has been a <a href="http://blogs.kde.org/node/3053">fun</a> <a href="http://blogs.kde.org/node/3035">week</a>. Sitting together with Andre, Daniel, Dirk, Jared, Klaas, Stephan and Will in the openSUSE office and hacking on KDE 4. There was one point in time when seven of ten KDE commits where coming from this office. I'm pretty much convinced now that we are on track with KDE 4. There certainly still is some way ahead, but we are getting there. This will be an exciting release. Of course I'm writing this blog entry on a KDE 4 desktop.

[image:3055 width="640" height="480" hspace=100]

I made a lot of progress on KOrganizer. It's pretty usable now. I fixed stuff like the creation of all-day events, the event indicators, coloring of events, got rid of some old cruft and Qt3 remains, and implemented header labels for the great multi-timezone view in the agenda view.

[image:3056 size="original" hspace=100]

Casper did some work on the Oxygen style and fixed some bugs. This is nicely coming along. I really like it. It's beautifully elegant and if you tweak the color scheme a bit, it also doesn't suffer the lack of contrast it apparently has by default. Tobias worked on KAddressbook and fixed the bug which prevented creation of new address books. Lots of other work happened as well and I also had a couple of good chats on IRC. As most of my KDE time is spent on the <a href="http://ev.kde.org">e.V.</a> these days, it feels particularly good to be in development mode for a change.
<!--break-->
