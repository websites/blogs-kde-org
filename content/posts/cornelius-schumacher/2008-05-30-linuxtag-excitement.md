---
title:   "LinuxTag Excitement"
date:    2008-05-30
authors:
  - cornelius schumacher
slug:    linuxtag-excitement
---
LinuxTag is a blast. I'm here for the third day, have met a lot of fantastic people, listened to great talks, and had a lot of fun. On Wednesday there was Aaron's KDE 4 keynote, where he also showed the tremendously exciting <a href="http://blogs.kde.org/node/3475">Marble with OpenStreetMap integration</a>. Yesterday Till talked about Kontact, which now runs on all platforms including Windows. Liquidat has <a href="http://liquidat.wordpress.com/2008/05/30/kde-at-linuxtag-2008-day-2-taking-over-the-world/">screenshots</a>, or check it out live at the BSI booth. Another fascinating project I saw is the <a href="http://www.obico.de">Open Bicycle Computer</a>, a bike computer built from scratch as open project.

Today at 13:30 there is Nat's keynote <a href="http://www.linuxtag.org/2008/de/conf/events/vp-freitag/vortragsdetails.html?talkid=133">The future of Linux is software appliances</a>. Go to room London to see it or check out the <a href="http://streaming.linux-magazin.de/programm_linuxtag08.htm">live stream</a>. It will be worth it :-)

Today there also is the KDE track and tomorrow will be the openSUSE track with more great talks.

Linuxtag excitement!
<!--break-->