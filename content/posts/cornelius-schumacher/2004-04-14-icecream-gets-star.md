---
title:   "Icecream gets a star"
date:    2004-04-14
authors:
  - cornelius schumacher
slug:    icecream-gets-star
---
While compiling KDE once again I found some time to work a little bit on icecream and revived Frerichs star view. This is a cool thingy, especially when there are many hosts, where the 'gantt' view reaches its limits. Have a look by yourself and enjoy the hypnotic effect of looking at <a href="http://blogs.kde.org/node/view/418">colored bubbles</a>. You will perceive compilation as faster than ever before ;-)

Icecream is becoming a really great tool. It's one of my favorites right now. It even makes sense with only two or three hosts. So if your girlfriend/wife/mother/whoever has a reasonably fast computer, give it some useful work to do by installing an icecream daemon.

By the way: Scott, you should really add a scrollbar to your summary view. My screen explodes when I do a <tt>make -j20</tt>.
