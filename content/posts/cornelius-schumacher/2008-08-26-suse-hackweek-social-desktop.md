---
title:   "SUSE Hackweek: Social Desktop"
date:    2008-08-26
authors:
  - cornelius schumacher
slug:    suse-hackweek-social-desktop
---
This week is hackweek at SUSE and people are <a href="http://zonker.opensuse.org/2008/08/26/hack-week-marches-on/">frantically hacking</a> on <a href="http://idea.opensuse.org">all kind of stuff</a>. Fun.

My project is the <a href="http://idea.opensuse.org/content/ideas/social-desktop">Social Desktop</a>, which is the buzzwordy title for an implementation of the Open Collaboration Services API (see <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">specification</a> on <a href="http://freedesktop.org">freedesktop.org</a>). <a href="http://blog.karlitschek.de/">Frank Karlitschek</a> has joined the fun and is at the SUSE offices for hackweek, so server and client implementations go hand in hand. The idea is to bring the community to the desktop and take benefit of the fact that free software projects are not only about software but also about community. This can provide a lot of extra value for our users, especially as the desktop is the place where all the social web data from different sites comes together and the user is in full control of what happens to the data and how it is combined. For some more background have a look at <a href="http://conference2008.kde.org/conference/slides/socialdesktop.pdf">Frank's Akademy keynote</a>.

As a first result I have now implemented a client which accesses <a href="http://opendesktop.org">opendesktop.org</a> through the Open Collaboraton Services API and makes its users available on the desktop. Next steps are searching for people and enabling communication.

<img src="https://blogs.kde.org/files/images/social_desktop3.png"/>

For more info and progress updates have a look at <a href="http://hackweek.blogspot.com">my hackweek blog</a> or the <a href="http://twitter.com/cschum">occasional tweet</a>.
<!--break-->