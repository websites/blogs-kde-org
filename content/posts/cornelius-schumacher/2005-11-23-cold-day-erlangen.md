---
title:   "a cold day in erlangen"
date:    2005-11-23
authors:
  - cornelius schumacher
slug:    cold-day-erlangen
---
It's getting winter here. I drank my second "Glühwein" today and the day after tomorrow the "Christkindlesmarkt" at Nürnberg will open, one of the most famous Christmas markets on the world. Bicycling is only mildly fun because it gets your fingers, toes and ears frozen, but all in all I like this time of the year. Dark evenings at home provide the opportunity to do some of the things you always wanted to do. In my case that's finishing some tasks I accepted as board member of the KDE e.V., continuing on establishing OpenSync as the one unified syncing solution for the Linux desktop and preparing to get back into KDE PIM coding. This all is promising to be some fun :-)
