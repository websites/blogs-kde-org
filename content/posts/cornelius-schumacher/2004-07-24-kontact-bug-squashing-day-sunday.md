---
title:   "Kontact Bug Squashing Day on Sunday"
date:    2004-07-24
authors:
  - cornelius schumacher
slug:    kontact-bug-squashing-day-sunday
---
We will have our second Kontact Bug Squashing Day on Sunday 25th July. As CVS is feature and message frozen for the 3.3 release we are all in bug-fixing mode and want to intensify our efforts to get a Kontact 1.0 which is as stable as possible. If you want to become part of this venture you can join us on IRC and help with reviewing bug reports, creating patches, testing fixes or generally cheering up the developers.

<a href="http://lists.kde.org/?l=kde-pim&m=109043752405127&w=2">Details</a> can be found on the <a href="https://mail.kde.org/mailman/listinfo/kde-pim">kde-pim mailing list</a>.

I'm looking forward to see you all on Sunday and hope that we can get hold of some of the nasty little beasts which populate some of the dark corners of Kontact. Let's squash some bugs together!

