---
title:   "We need to embrace freedesktop.org"
date:    2004-01-29
authors:
  - cornelius schumacher
slug:    we-need-embrace-freedesktoporg
---
What's up with freedesktop.org? Daniel says KDE people don't care about it, Ian says we have to abandon it. I say let's embrace it and make it what it's meant to be, a common building block for all desktops, KDE, GNOME and whatever else.

There is some cool technology on freedesktop.org like the X server and Cairo, some interesting stuff like D-Bus, but where are the cool things KDE could provide? Where is KConfig XT, where are DCOP bindings for D-Bus, where are the specifications for the various desktop file flavors we use? Let's put them on freedesktop.org. It really can't hurt if some more people outside of KDE get some exposure to KDE technology. Many things we have aren't tied that much to our specific desktop, but could very well turned into something useable as a common base for multiple desktops.

Ian stated that freedesktop.org standards are polluted by implementations. Well, that's how free software works. Those who write the code decide. We aren't able to sit down as bureaucrats, write formal standards as thick and as interesting as phonebooks and contemplate about philosophical details of extended Backus-Naur forms. We are able to write code that works and gets the job done. This code which actually exists defines the standards and we should extract that and put it on freedesktop.org.

Think big. KDE isn't about developing one specific cool desktop environment, it's about providing the base for desktop computing at all and about executing our vision how this desktop should be.
