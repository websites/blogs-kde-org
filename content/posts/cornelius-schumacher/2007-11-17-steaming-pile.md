---
title:   "Steaming pile of ..."
date:    2007-11-17
authors:
  - cornelius schumacher
slug:    steaming-pile
---
From time to time I get overwhelmed by my passion for computer games and I buy a game which promises to be fun. So it happened a few weeks ago when I saw a special offer of Valve's "The Orange Box" in the local electronics store. This box contains all kind of Half-Life 2 stuff including the new Episode Two and the very promising looking game "Portal". But boy was I wrong. This was one of the worst buys I ever made.

The box comes with two DVDs, but to be able to play you need to connect to "Steam", an online restrictions management server, which then downloads tons of data to "update" and "activate" your game. It took almost an hour before I was even able to start the game. Needless to say that of course Steam gets autostarted on each login and annoys you with advertisements. What a "steaming" pile of crap.

After this unpleasant experience I thought I would be ready to play the game, but no, I was caught in the nightmare of graphics card drivers for Windows. Trying to be helpful Steam told me that I would have to upgrade the graphics card drivers to the latest version, so I did that. But then when starting the game I only got a cryptic error message. Fortunately problems like that are so common, that it was relatively easy to find an entry in a forum which offered an solution: Downgrading the graphics card drivers to a specific version. So I went again to the ATI site, downloaded some more Megabytes and finally it worked, but another hour was gone. If you think that graphics drivers on Linux are a problem go back to Windows and you will see that it can be worse.

The game itself is brilliant, by the way. What a pity that Valve ruined it with their crappy restriction management concept. But it also had a good side for me: I was reminded again how precious the freedoms of free software are. It feels so good to know that we are doing the right thing by writing great free software instead of inventing strange concepts to annoy users through restricting what they can do with the software they bought.
<!--break-->