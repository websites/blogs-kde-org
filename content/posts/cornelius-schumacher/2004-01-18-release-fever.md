---
title:   "Release Fever"
date:    2004-01-18
authors:
  - cornelius schumacher
slug:    release-fever
---
We are approaching the 3.2 release. That's exciting. I just collected what's new in KOrganizer for 3.2 and was impressed. I didn't remember that it was so much cool stuff we implemented in the year it took since KDE 3.1.

Reinhold for example did a tremendous job in fixing bugs, Tim did the long overdue rewrite of the agenda view rendering, Jan-Pascal initiated the resource framework which now brings us multiple calendars and more. Many others also contributed great code.

Are there still critical bugs left? That question is somewhat disturbing, but I guess that's part of the release fun ;-). I'm compiling the freshly created 3.2 branch right now, but I don't really expect new problems. The HEAD branch worked already very well the last weeks.

Last year with the 3.1 release I had one of these horrifying last-minute  bugs. It was triggered by changing the version number to 3.1. This made some compatibility code for fixing broken calendar files from older versions run amok. Fortunately we were able to find and fix the bug before the actual release. It would have rendered KOrganizer almost useless.

Now for something completely different: I read that Zack shaved his dreads. That's frightening...