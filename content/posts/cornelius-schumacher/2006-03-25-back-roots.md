---
title:   "Back to the roots"
date:    2006-03-25
authors:
  - cornelius schumacher
slug:    back-roots
---
Today apparently was board-gets-back-to-coding day. Since I was elected into the <a href="http://ev.kde.org/corporate/board.php">board of the KDE e.V.</a> last summer in Malaga most of my KDE time is eaten by non-coding jobs. But today I had one of my productive days and got a lot of done on <a href="http://websvn.kde.org/trunk/KDE/kdepim/kode/kxml_compiler/">kxml_compiler</a> and <a href="http://websvn.kde.org/trunk/KDE/kdepim/kode/kxforms/">kxforms</a>. It was a nice surprise when I noticed on the <a href="http://lists.kde.org/?l=kde-commits&r=1&w=2">kde-commits</a> mailing list, that Eva also was heavily committing code working on <a href="http://websvn.kde.org/trunk/kdenox/konq-embed/">Konqueror/Embedded</a>. Five minutes before midnight Aaron joined the fun with a <a href="http://plasma.kde.org/">Kicker</a> patch. Huzzah! We are back to the roots. The board is still coding.

Working on kxforms was fun. My goal is to automatically generate user interfaces for editing XML data from XML schemas. Today I implemented big parts of the important step to create an intermediate abstract interface definition (this definition is what I call "kxforms"), from the schema. The code for actually creating a GUI from the kxforms definition already works. I presented this at last aKademy in my <a href="http://conference2005.kde.org/slides/meta-programming--corneluis-schumacver.pdf">Metaprogramming talk</a>. While working on the schema parsing code I also added XML Schema support to kxml_compiler, so that it's not only possible to generate the C++ classes handling XML from RelaxNG schemas, but now also from W3C XML Schemas. As always when I do some more intensive coding after a while away from KDE coding I was amazed how productive it was to work with the KDE framework. This stuff seriously rocks.

After a day of satisfying coding I watched an old Laurel and Hardy movie with my daughters: "Way Out West". I remember watching this as a kid and it was incredibly great too see it again. But it also was strange to see how far away this kind of movie is from what kids are used today. My daughters kept asking questions like "Why isn't that colored?" or "Why don't they talk?". They still enjoyed it very much.
