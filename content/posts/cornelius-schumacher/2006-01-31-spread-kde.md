---
title:   "Spread KDE"
date:    2006-01-31
authors:
  - cornelius schumacher
slug:    spread-kde
---
The first big thing today was the release of <a href="http://www.kde.org/announcements/announce-3.5.1.php">KDE 3.5.1</a>, made possible by the hundreds of dedicated contributors which make up the wonderful KDE community. Especially the <a href="http://i18n.kde.org">translators</a> did some great work, so KDE 3.5.1 is available in the incredible number of 63 languages.

The second big thing today was the announcement of <a href="http://www.spreadkde.org">SpreadKDE</a>, the new promotion site of KDE. This is brought to you by the restless guys from the <a href="http://dot.kde.org/1131467649/">KDE Marketing Working Group</a>. Hats off for this nice new site.

But to put this into the bigger perspective of spreading the Free Desktop in general, I'm happy to see that a common free desktop marketing effort is being created, including people from both, GNOME and KDE. There is a mailing list for a start and certainly there is more to come. It makes so much sense to work together on reaching our common goals. More of this, please.
<!--break-->