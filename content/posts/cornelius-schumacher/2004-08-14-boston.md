---
title:   "Boston"
date:    2004-08-14
authors:
  - cornelius schumacher
slug:    boston
---
When going to Boston for the USENIX conference I started to read <a href="http://www.harpercollins.com/catalog/book_xml.asp?isbn=0380977427">Quicksilver</a> by <a href="http://www.nealstephenson.com/">Neal Stephenson</a> in the plane from Frankfurt to Boston. I didn't know much about about the setting of the book, so it came as a surprise that the book starts with the execution of a woman at the <a href="http://blogs.kde.org/node/view/555">Boston Common</a>. Ten hours later I was standing at the place where <a href="http://www.metaweb.com/wiki/wiki.phtml?title=Stephenson:Neal:Quicksilver:Enoch_Root">Enoch the Red</a> watches Jack Ketch doing his job.

Neal Stephenson has become one of my favorite authors in the last years. I own all of his book, including "Interface" and "The Cobweb". The only one I haven't read yet is "The Confusion", but that's only a matter of time. The book of Stephenson I like most is "Diamond Age", because it's one of these science fiction novels with such a non-scientific atmosphere. In this respect it's similar to <a href="http://www.randomhouse.com/catalog/display.pperl?055329461X">The Difference Engine</a> by William Gibson and Bruce Sterling. Well, and I have always loved books about books.
