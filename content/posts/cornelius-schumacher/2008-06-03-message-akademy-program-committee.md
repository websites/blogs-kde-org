---
title:   "Message from the Akademy program committee"
date:    2008-06-03
authors:
  - cornelius schumacher
slug:    message-akademy-program-committee
---
Today I'm writing in my role as a member of the program committee for the Akademy 2008 Contributor's Conference. We have received a lot of great proposals for presentations and are still in the process of selecting and assembling a program. So speaker notification will be delayed by something like a week.

Regardless of the question, if you will present at the Akademy conference or not, I highly recommend to join Akademy. Register at the Akademy web site, book your travel, and go to Belgium in August. I can promise that we will have an exciting conference program and I'm pretty sure that Akademy as a whole will live up to the high standards of community gathering we are used to from previous Akademies.
<!--break-->
