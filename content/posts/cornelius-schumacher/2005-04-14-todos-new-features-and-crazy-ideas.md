---
title:   "Todos, New Features and Crazy Ideas"
date:    2005-04-14
authors:
  - cornelius schumacher
slug:    todos-new-features-and-crazy-ideas
---
A while ago I blogged my <a href="http://blogs.kde.org/node/view/921">thoughts about future KDE releases</a>. I promised to post the list of some of the things I have in mind for KDE 3.5 and 4.0. So here it comes. It's a long list and if I do it all myself it will keep me busy for years. So if you want to help or pick something up, please <a href="mailto:schumacher@kde.org">contact me</a>, you are very welcome.
<br>
<br>

<b>Mail</b>

<ul>

<li>
<b>Compact message list</b>: If KMail would provide a compact mode for the message list, e.g. by using two lines per mail, the mode showing the preview pane next to the message list would be much more useful. This way we could finally use all vertical space available for showing mails. As the width of mails is limited but the length is not, it is the much more natural way to show a mail.
</li>

<li>
<b>Full thread message view</b>: KMail could use a full thread message view that doesn't show emails of a thread one-by-one, but as a combined view, similar to the <a href="http://www.kde.org.uk/apps/akregator/akregator3.png">combined view</a> of <a href="http://akregator.sf.net">akregator</a> or <a href="http://gmail.google.com">Gmail</a>'s <a href="http://gmail.google.com/gmail/help/screen2.html">conversation view</a>.
</li>

<li>
<b>Recipients Editor</b>: There is some fine-tuning to do and a small part of the suggestions from the <a href="http://www.openusability.org/reports/view.php?group_id=55&repid=44">usability studies</a> done by the <a href="http://www.openusability.org">OpenUsability</a> people isn't completely addressed yet.
</li>

<li>
<b>Recent Addresses</b>: KMail already does a good job in remembering recently used mail addresses. On the other hand there is so much yet unused information in mails which isn't used, for example signatures and email addresses from incoming mail. If we can extract at least a part of the data which is present in all the emails a user processes all day, we could save a lot of manually input and management of contact information. Technicaly, I think, the class handling the recent addresses in KMail should be converted to a normal addressbook KResource. This would make the contacts collected by KMail appear in all KDE applications making use of the central KDE address book and it might even save some code.
</li>

</ul>


<b>Calendar</b>

<ul>

<li>
<b>KOrganizer Theming</b>: In the real world calendars are made from paper and carry lots of pretty pictures, witty sayings, history data or similar things. Compared to that calendar programs are extremly plain. One of the intents of the <a href="http://webcvs.kde.org/kdepim/korganizer/interfaces/calendar/">CalendarDecoration plugin interface</a> for KOrganizer was to make it possible to add theming to the calendar views, so that you can add all the fun things which make paper calendars more than just a bunch of papers with dates on it. Combined with the <a href="http://www.kstuff.org/index.php?p=ghns">KNewStuff</a> technology this could bring much more color to our desktops and make KOrganizer intersting to people which currently wouldn't think of using a calendar program.
</li>

<li>
<b>Blogging Resource</b>: Reinhold has written a <a href="http://webcvs.kde.org/kdepim/kresources/blogging/">blogging resoure</a> for KOrganizer. I have to find out if it works and start to use it.
</li>

<li>
<b>Feature Plan Resource</b>: There is the resource for importing features from the <a href="http://developer.kde.org/development-versions/kde-3.5-features.html">KDE Feature Plan</a> as tasks in KOrganizer. I wrote it as demonstration for the kxml_compiler technology I presented at Ludwigsburg. Back then it was a read-only resource, in the mean-time I have added write support. The only problem with that is that writing doesn't necessarily preserve the formatting of the XML file. I made an attempt to solve this problem, by adding a custom XML parser to kxml_compiler, but didn't finish this work yet. Maybe I should drop this idea and concentrate on making the resource useful. It currently lacks filtering capabilities (e.g. showing only features assigned to a certain responsible) and the write support has to be finalized.
</li>

</ul>


<b>Kontact</b>

<ul>

<li>
<b>Windows port of Kontact</b>: Expanding the potential user base of Kontact by a factor of 50 is, well, tempting. Especially given the fact that most of the porting work already is done. We have Qt on Windows, with Qt 4 even as GPL version, there is a port of the KDE libraries, there already are native version of KOrganizer and KAddressbook running on Windows. We only need to take the last step and face the challenge of being on the same level as projects like Mozilla or OpenOffice.
</li>

<li>
<b>Offline mode for Kontact</b>: There are still missing some aspects of a real offline mode for Kontact. We have the <a href="http://webcvs.kde.org/kdepim/networkstatus/">network status daemon</a> written by Will and all the data is already cached locally, but KMail and the KResources of KOrganizer and KAddressbook still don't fully make use of it. This is not too much work, but includes some tedious logic (when to do automatic reload of data from a server, how to handle errors in all the different cases, how to avoid multiple error messages for the same error, etc.) 
</li>

</ul>


<b>Collaboration</b>

<ul>

<li>
<b>GroupDAV</b>: We have basic <a href="http://www.groupdav.org/">GroupDAV</a> support in Kontact, but the <a href="http://webcvs.kde.org/kdepim/kresources/groupdav/">implementation</a> as well as the <a href="">standard</a> still have some issues. GroupDAV is a really nice and simple way to manage server-based calendar and contact data and it gets support from several projects: <a href="http://kontact.kde.org">KDE Kontact</a>, <a href="http://www.gnome.org/projects/evolution/">Evolution</a> and <a href="http://www.mozilla.org/projects/calendar/sunbird.html">Sunbird</a> on the client side and <a href="http://www.opengroupware.org/">OpenGroupware.org</a> and <a href="http://www.citadel.org/">Citadel</a> on the server side. So to make this project fly is most probably a highly rewarding task.
</li>

<li>
<b>Auto-Setup Server Access</b>: When you configure Kontact for accessing a groupware server you usually have to put in quite a lot of information. The wizards we have help with that, but still the questions arises why do I have to configure anything at all. The server already knows very well, what kind of protocols it supports and which paremeters have to be used aftr all. So the basic idea of auto-setup is to retrieve the configuration information for the client from the server. This could be done as XML file specifiying things like ports of services like SMTP, POP, IMAP, GroupDAV, whcih of these services are available, if they use encryption or what kind of it they use. The final goal would be to set up access to any server by just providing the server name, an account and a password.
</li>

<li>
<b>Locking for remote KResources</b>: With the remote KResources of KAddressbook and KOrganizer it's easily possible to maintain addressbooks and calendars on FTP servers, servers accessible per SSH or by any other kioslave supported protocol. This works fine as long the same files aren't simultaneously accessed by multiple clients. Introduction of a locking scheme would significantly reduce the possible problems. It's probably not possible to find a solution which works with all kioslaves in all cases, but a solution which works for most of them in most of cases I would consider good enough. Did I mention that this isn't much work to implement?
</li>

<li>
<b>KDE Addressbook Library</b>: With KDE 4 libkabc needs some serious work. We definitely need to improve the handling of resources providing big addressbooks. We also need to find a way how to merge addressees from different resources in an intelligent way, so that the entries from the company LDAP server, from the kde-accounts file, from the manually maintained local record and from the recently used addresses in KMail aren't shown as four distinct entries with the same name, but as one unqiue object combining all the available information.
</li>

<li>
<b>KitchenSync</b>: Now that we have <a href="http://pim.kde.org/development/meetings/osnabrueck3/kitchensync.php">fancy design documents</a> for the GUI, we only need to finish the implementation ;-).
</li>

</ul>


<b>XML</b>

<ul>

<li>
<b>kxml_compiler</b>: I have <a href="http://conference2004.kde.org/slides/cornelius.schumacher-metaprogramming.pdf">presented</a> the concept and first implementation at the <a href="http://conference2004.kde.org/">aKademy at Ludwigsburg</a>. The basic idea is to automtically generate C++ code for handling XML data from the schema for the XML. I have <a href="http://webcvs.kde.org/kdepim/kresources/featureplan/">something working</a> for the <a href="http://developer.kde.org/development-versions/kde-3.5-features.html">KDE feature plan</a>, but there is still a long way to go until it works with a wider range of XML. Next step is to support the schema for the <a href="http://webcvs.kde.org/kdeplayground-pim/libkholidays_ng/">next generation of the KDE holiday library</a>.
<br>
Another idea is to not only create C++ code from the schema, but also automatically create a GUI for viewing and editing XML data using the schema. This approach could potentially be a far better solution to the problem of editing XML files than any generic XML editor can ever provide.  
</li>

<li>
<b>SOAP support for KDE</b>: Accessing web services from KDE applications is not as easy as it could be. For the <a href="http://www.novell.com/products/groupwise/">GroupWise</a> resource for <a href="http://kontact.kde.org">Kontact</a> we use <a href="http://www.cs.fsu.edu/~engelen/soap.html">gSoap</a>, which is an amazing tool creating C++ classes and stubs for accessing SOAP services from WSDL descriptions. It works well, but could be better integrated with KDE, e.g. by using QString instead of std::string or by making use of kioslaves. So either we add this to gSoap or write our own version (which would be hard and time-consuming, but an interesting challenge).
<br>
We also don't have much support for writing apps providing web services. This could be solved by changing dcopidl to optionally create SOAP bindings or by writing a DCOP/SOAP bridge. It would be cool if KDE would expand in these direction with such simple measures. With Qt 4 providing the non-GUI parts in separate libraries it might become more interesting to write server applications using the KDE framework.
</li>

</ul>


<b>Configuration</b>

<ul>

<li>
<b>KConfigWizard</b>: The wizards for setting up access to groupware servers in Kontact are already quite useful. But they don't make full use of the underlying concepts yet. The wizards are based on a generic mechanism to propagate configuration data to the configuration files of the applications. This is done in an abstract way, so it is well-defined which values are propagated to which files. The current implementation already is able to show an overview of the imminent changes. This concept needs to be extended. By recording the changes done by a wizard it would be possible to collect the changes in transactions. This would allow to show a history of changes done by wizards, to revert transactions and to indicate in the configuration dialogs of the individual applications which options are affected by wizard settings. My gut feeling is that there is a tremendous potential in the concepts the <a href="http://webcvs.kde.org/kdepim/libkdepim/kconfigpropagator.h">KConfigPropagator</a> and the <a href="http://webcvs.kde.org/kdepim/libkdepim/kconfigwizard.h">KConfigWizard</a> classes are based on. It would be really interesting to explore this potential a bit more thoroughly. Yes, and by the way, the wizards should be made able to run in parallel to the application they configure.
</li>

<li>
<b>Automatic generation of configuration dialogs</b>: With KConfig XT we have a technology which makes it extremely simple to handle configuration data in an application. The developer basically just has to specify which configuration options he wants and then can make use of it in the code. All the actual loading and saving of data, handling of config keys etc. is automatically done in the background. That's nice, but it's still necessary to manually create the configuration dialogs for editing the data.
<br>
So the next step would be to automatically create the dialogs from the data specified through the KConfig XT XML files. All the information is there and I'm sure that at least 90% of the configuration dialogs we have in our applications could be generated without writing a single line of C++ code. There is a simple and very limited <a href="http://webcvs.kde.org/kdepim/libkdepim/cfgc/example.cpp?rev=1.3&view=auto">prototype</a> in kdepim.
<br>
The big benefit of automatically creating the dialogs would be that it becomes extremly cheap to add, remove of change configuration options, so that we can concentrate on optimizing the configurability of our applications without the fear of having to throw away effort spent on creating the dialogs. This way we can focus on improving usability instead of writing boring code.
</li>

</ul>

<b>Desktop</b>

<ul>

<li>
<b>KHelpCenter</b>: KHelpcenter uses a simple <a href="http://webcvs.kde.org/kdebase/khelpcenter/README.metadata?rev=1.3&view=markup">meta data mechansim</a> to get access to and integrate all the documentation the system provides. It already has quite some history and part of the plan always has been to make this a standard for broader use, so that it's easy to put application manuals and additional documentation in the central desktop help system. We had a <a href="http://lists.freedesktop.org/archives/xdg/2003-December/thread.html#2924">discussion</a> on the freedesktop.org mailing list a while ago and even agreed on a concept. It still has to be executed, though.
<br>
In addition to the meta data stuff, there are also some old <a href="http://webcvs.kde.org/kdebase/khelpcenter/DESIGN?rev=1.18&view=auto">KHelpcenter ideas</a> which might be fun to implement.
</li>

<li>
<b>Extending KDatePicker</b>: The Kicker clock applet and its associated calendar popup is ubiquitous on KDE desktops. Compared to the KOrganizer date picker it doesn't show much information, though. We need to extend the widget behind the calendar popup to make it possible to show holidays, appointments or other date-based data. It would be trivial to do this in kdepim based on the KOrganizer code, but doing it in a more generic way, so that all KDE application using the date picker benefit, is much more preferable. It should also not be that difficult. Something like a plugin interface for KDatePicker for highlighting dates and associating some extra info should do the trick.
</li>

<li>
<b>KBugBuster</b>: During the old mail-based bug tracking system days, KBugBuster was a killer application because it provided a convenient user interface and offline capabilities for the KDE bug tracking system. Since we switched to Bugzilla and in the times of internet access and flatrates being much more widely available it isn't used that much anymore. But KBugBuster has support for a wide range of different Bugzilla versions and is able to at least read the bug tracking systems of most relevant open source projects and with a reasonable amount of work it could be improved to be a native client with the same level of functionality as the web interface. In some areas, e.g. offline support, local searches and responsiveness of the user interface it could even go quite a bit beyond the capabilities of the web interface. By the way, this project really is fun because you can achieve results with little effort you never would have dreamed to be able to implement with the web interface.
</li>

</ul>
