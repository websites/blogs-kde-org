---
title:   "Recruiting on Error"
date:    2007-11-26
authors:
  - cornelius schumacher
slug:    recruiting-error
---
I saw a nice form of recruiting software developers on web.de today. On the page which appears when an error on the server occurs they have a box saying "This wouldn't happened to us with you? Show it to us, apply for a job as software developer". That sounds like the commercial version of "Send a patch".

[image:3112 width="320" height="252" hspace=100]

But the photo on the page disturbs me. I have never seen a group of software developers where everybody wears a white shirt.
