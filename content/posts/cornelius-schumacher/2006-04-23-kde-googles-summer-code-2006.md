---
title:   "KDE in Google's Summer of Code 2006"
date:    2006-04-23
authors:
  - cornelius schumacher
slug:    kde-googles-summer-code-2006
---
KDE is again participating in <a href="http://code.google.com/soc/">Google's Summer of Code</a>. We did this last year and got <a href="http://developer.kde.org/summerofcode/soc2005.html">24 exciting projects</a> running. They had all kinds of results, from widely successfull over interesting concept to mild failure. I mentored three projects and it certainly was a great and enjoyable experience, so I will be a mentor again this year.

There already is a <a href="http://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas">list of ideas for KDE projects in Google's Summer of Code 2006</a>. It includes lots of interesting stuff. There are general ideas like implementing new concepts for rendering windows on the desktop, web services development tools or innovative ways of providing online help for applications. There are whole projects which provide ideas, like <a href="http://www.koffice.org">KOffice</a> or <a href="http://kdevelop.org/mediawiki/index.php/Google_SOC_2006_Project_Proposals">KDevelop</a> (my favorite feature from their list is the <a href="http://kdevelop.org/mediawiki/index.php/Google_SOC_2006_Project_Proposals#Unified_code.2Fheader_editing">Unified code/header editing for C++</a>, that's an interesting concept and a challenge which is just right for a Summer of Code). There also are lots of possible projects around <a href="http://pim.kde.org/akonadi">Akonadi</a>, the upcoming PIM Storage Service which will provide the base for handling mails, calendars, contacts and more in <a href="http://kontact.kde.org">Kontact</a> of KDE 4. As the Akonadi is based on a cross-platform and cross-desktop architecture it will be able to reach beyond the borders of KDE. This certainly is an inspiring area to work on.

So if you are a student: Don't miss this opportunity. Get involved, <a href="http://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas">pick a project</a> or come up with an exciting new idea, <a href="http://code.google.com/soc/studentfaq.html">apply for the program</a> and earn some money while having a great time in the Summer of Code. As Chris DiBona put it: Flip bits not burgers.
<!--break-->