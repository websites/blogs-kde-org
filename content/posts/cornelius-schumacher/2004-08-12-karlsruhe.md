---
title:   "Karlsruhe"
date:    2004-08-12
authors:
  - cornelius schumacher
slug:    karlsruhe
---
The last months have been a busy time. Some special things like the <a href="http://dot.kde.org/1091054636/">USENIX conference</a>, the <a href="http://dot.kde.org/1090614115/">KDE Free Qt Foundation agreement</a>, the final phase of the <a href="http://dot.kde.org/1092139173/">KDE 3.3</a> release cycle or the preparations for the upcoming KDE conference <a href="http://conference2004.kde.org">aKademy</a> took a considerable amount of my time in addition to the usual having a job and a family. But it was fun and still is.

One of the most fun things was the Linuxtag at <a href="http://www.karlsruhe.de">Karlsruhe</a>. It was great as always to meet a lot of other KDE developers but the special highlight was the dinner at the KSC Clubhaus under the label "KDinner" (we can't do it without a K, can we?) together with the Debian and GNOME people. This event was special to me because it reminded me of a <a href="http://www.dfb.de/bliga/bundes/archiv/1992/schemen/288.html">great football game</a> I watched at the <a href="http://www.karlsruhe.de/Sport/Wildpark/index.htm">stadium</a> just next to the Clubhaus more than ten years ago. The <a href="http://www.karlsruhersc.info/">KSC</a> defeated the famous <a href="http://www.fcbayern.de/en/index.php">FC Bayern</a> 4:2. Great atmosphere, great game and a great succes for the KSC. Unfortunately the club nowadays isn't as successfull as 1993, but some of the players are still <a href="http://fifaworldcup.yahoo.com/02/en/t/t/pl/78091/">somewhat relevant</a>.

