---
title:   "My first KDE patch"
date:    2005-05-21
authors:
  - cornelius schumacher
slug:    my-first-kde-patch
---
Today is the sixth anniversary of <a href="http://rechner.lst.de/~cs/kde/history/mail_firstpatch.html">my first KDE patch</a>. I was a fix for the development version of KOrganizer which made it not crash on startup. This was during the time of porting to KDE 2. It was exciting and fun to make the applications work with the new libraries, quite similar to what's now <a href="http://blogs.kde.org/node/view/1068">going on with KDE 4</a>. Energizing :-)
