---
title:   "Marble Live CD"
date:    2009-05-09
authors:
  - cornelius schumacher
slug:    marble-live-cd
---
<a href="http://edu.kde.org/marble/">Marble</a> is one of my favorite applications. I especially like it in combination with <a href="http://openstreetmap.org/">OpenStreetmap</a>. Free software and free maps, a brilliant combination. But I also love the historical map or the moon view.

<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://2.bp.blogspot.com/_I0jbESd5Btw/SgWraB4SSNI/AAAAAAAAADs/CRmVnf6nnXQ/s1600-h/studio-marble-testdrive.png"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer; width: 320px; height: 217px;" src="http://2.bp.blogspot.com/_I0jbESd5Btw/SgWraB4SSNI/AAAAAAAAADs/CRmVnf6nnXQ/s320/studio-marble-testdrive.png" alt="" id="BLOGGER_PHOTO_ID_5333857797359683794" border="0" /></a>

Marble also is great as a demo application. It's easy to grasp and makes an attractive showcase. To make demoing Marble a bit easier I thought it would be nice to have a Marble live CD, and as I happen to have a <a href="http://susestudio.com/">great application</a> for doing this at hand, I created some Marble live CDs. You can download them from my <a href="http://cornelius-schumacher.de/marbleinabox.html">Marble live CD page</a>.

I'm interested in feedback. So if you have questions, comments or want to help, <a href="mailto:schumacher@kde.org">contact me</a>.

<a href="http://susestudio.com/"><img style="text-align: center;" title="Built with SUSE Studio" src="http://susestudio.com/images/built-with-web.png" alt="Built with SUSE Studio" border="0" height="30" width="120" /></a>
<!--break-->