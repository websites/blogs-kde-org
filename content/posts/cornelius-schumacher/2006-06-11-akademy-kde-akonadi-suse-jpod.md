---
title:   "aKademy, KDE, Akonadi, SUSE, JPod"
date:    2006-06-11
authors:
  - cornelius schumacher
slug:    akademy-kde-akonadi-suse-jpod
---
I wrecked my wrist nine days ago. My doctor says I will be able to use it again in a week. Hope he is right. I would cross my fingers, if it wouldn't hurt so much. Anyway, there are some exciting things I wanted to write about, so I'm blogging single-handedly now.

<b>aKademy 2006:</b> The <a href="http://conference2006.kde.org/conference/call.php">call for participation</a> has been announced and the wonderful new <a href="http://conference2006.kde.org">web site for aKademy 2006</a> is now online. The <a href="http://www.oxygen-icons.org/?cat=2">Oxygen team</a> did a terrific job with the design of the web site and the local Dublin team is busy on fleshing out the details to make sure that we will have a great event at the end of September in Dublin. If you have something interesting to tell to the KDE community (and I know many of you, who are reading this now, have) don't hesitate to submit a propsal for a presentation at aKademy. It will be an exciting week. Don't miss it.

<b>12.000 new KDE Users:</b> The financial authorities of Niedersachsen, the second biggest German federal state are <a href="http://news.zdnet.co.uk/0,39020330,39274196,00.htm">moving their desktops to Linux</a>. They are using KDE on SUSE. That's a great win for the free desktop.

<b>Akonadi:</b> Till, Tobias, Volker and Ingo met at Aachen for a long weekend of <a href="http://blogs.kde.org/node/2062">ferocious hacking</a> on <a href="http://pim.kde.org/akonadi">Akonadi</a>, the storage and query service for PIM data for KDE 4. They produced an amazing amount of code, so that we now have a first version of a working backend and KMail even can already <a href="http://www.matha.rwth-aachen.de/~ingo/2006-05-25--27-akonadi-hacking-in-aachen/img_2435.jpg">talk to it</a>. The architecture we created at the <a href="http://pim.kde.org/development/meetings/osnabrueck4/overview.php">Osnabrueck 4</a> meeting seems to work out. I'm looking forward to have some time to join the fun and work on Akonadi myself. I already have some code on my harddisk which I hope to make ready for inclusion within the next weeks.

<b>SUSE Linux 10.1:</b> I have installed the <a href="http://www.novell.com/products/suselinux/">latest SUSE version</a> on my laptop now and I'm quite happy with it. All the hardware works flawlessly now. This wasn't the case before. The wireless (including all LEDs), suspend, 3D acceleration, everything worked out of the box. With KNetworkManager and all what is behind that networking is more convenient than ever before. Logging into KDE is blazingly fast. There are some problems with the update functionality. But it's possible to <a href="http://blogs.kde.org/node/2092">work around</a> them and fixes are in the pipeline.

<b>JPod:</b> Last week I read the new book by Douglas Coupland: <a href="http://www.jpod.info/">JPod</a>. Fascinating. It's an absurd entertaining set of crossing stories about six game developers whose names start with a J, packed in a novel and spiced up by things like an appearance of an unappealing variant of the author himself, twenty pages with the first hundred thousand digits of pi, full page versions of the Chinese words for boredom or pornography and densly packed hilarious ideas how to twist the story in the strangest ways. An enjoyable read.

That's it for now. A dense week. I'm looking forward to the next one.
<!--break-->