---
title:   "Gwenview cropping"
date:    2007-07-26
authors:
  - aurélien gâteau
slug:    gwenview-cropping
---
(Creating an entry on my old blog until PlanetKDE is updated)

I implemented crop support in Gwenview. Here is <a href="http://agateau.wordpress.com/2007/07/26/crop-this/">the blog entry</a> about it.