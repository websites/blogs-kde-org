---
title:   "WengoPhone 2.1.0 is out!"
date:    2007-05-18
authors:
  - aurélien gâteau
slug:    wengophone-210-out
---
(warning, lame marketing attempt following)

I am happy to report that Wengo (the company I work for) has finally released <a href="http://www.openwengo.org">version 2.1.0 of the WengoPhone</a>, a GPL licensed, Qt4 based, cross-platform softphone featuring SIP based VoIP, multi protocol IM support (thanks to <a href="http://developer.pidgin.im/wiki/WhatIsLibpurple"><strike>libgaim</strike> libpurple</a>), SMS sending, PSTN calls and much more.

Want to know more? here is the <a href="http://www.openwengo.org/index.php/openwengo/public/homePage/news?payload[newsId]=0">release announcement</a>.

Convinced? <a href="http://www.openwengo.org/index.php/openwengo/public/homePage/openwengo/public/projectsNgDownload">grab your copy here</a>!

<!--break-->