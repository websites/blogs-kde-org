---
title:   "Moved my blog"
date:    2007-07-15
authors:
  - aurélien gâteau
slug:    moved-my-blog
---
I moved my blog from kdedevelopers.org to wordpress.com. The new address is http://agateau.wordpress.com.

This new blog will stay KDE focused, but will also include more general geek stuff and real-life news. I just posted my first KDE related blog entry. It's about the <a href="http://agateau.wordpress.com/2007/07/15/you-have-unsaved-changes/">new way to save images in Gwenview</a>. I am eagerly waiting for Planet KDE readers to check and comment :-)