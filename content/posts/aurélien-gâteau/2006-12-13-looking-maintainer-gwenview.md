---
title:   "Looking for a maintainer for Gwenview"
date:    2006-12-13
authors:
  - aurélien gâteau
slug:    looking-maintainer-gwenview
---
Having been working on Gwenview for six years, I finally came up with the decision that it was time to move on. 

It has been a tough decision, but I realize I don't have enough free time to manage such a project by myself anymore. If you want to know  more, have a look at the <a href="http://sourceforge.net/mailarchive/message.php?msg_id=37647108">message I posted to the mailing list</a>.

Of course, I do not want Gwenview to die, that's why I'm looking for a maintainer. If you are interested, please contact me.
<!--break-->