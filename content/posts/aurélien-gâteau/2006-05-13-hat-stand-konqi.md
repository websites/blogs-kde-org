---
title:   "Hat stand Konqi"
date:    2006-05-13
authors:
  - aurélien gâteau
slug:    hat-stand-konqi
---
It seems Konqi got a bit fed up of being the KDE mascot these days. I spotted him this morning on his new workplace: he got hired by my wife to hold the hat of Clara, my daughter.

Here is the proof:
[image:2011]
<!--break-->