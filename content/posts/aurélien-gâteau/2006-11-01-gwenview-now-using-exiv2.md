---
title:   "Gwenview is now using Exiv2"
date:    2006-11-01
authors:
  - aurélien gâteau
slug:    gwenview-now-using-exiv2
---
While trying to fix a crash in Gwenview when saving a JPEG image (<a href="http://bugs.kde.org/show_bug.cgi?id=136112">Bug 136112</a>) I found out the crash was caused by a file I borrowed from <a href="http://libexif.sf.net">libexif</a>. Not wanting to debug this code, I decided to switch Gwenview to use <a href="http://exiv2.org">Exiv2</a> instead.

I'm quite happy with the change: Exiv2 is C++, so it's quite easier to use than libexif and it brings in more features like handling the JPEG comment for me. Let's hope nothing got broken in the process. If you use the SVN version of Gwenview, please give it a try and report any regression.

PS: This is not a rant against libexif. The libexif devs explained the file I used was just example code and recommended against using it but I stupidly ignored the advice. This file (jpeg-data.c) is not even in libexif source anymore (I guess the libexif devs got fed up telling stupid devs like me not to use it :-) ).
<!--break-->