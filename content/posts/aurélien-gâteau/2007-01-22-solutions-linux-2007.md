---
title:   "Solutions Linux 2007"
date:    2007-01-22
authors:
  - aurélien gâteau
slug:    solutions-linux-2007
---
<a href="http://www.solutionslinux.fr/en/index.php">Solutions Linux</a>, a french exhibition happens next week in Paris.

As usual, the <a href="http://www.kde-france.org">KDE France team</a> will demonstrate the finest solutions in KDE technologies. To help us in our task, we need you: tell us what is a must see in the latest 3.5 version as well as in the upcoming 4.0 release. With your contribution, we should be able to ensure maximum jaw drop...

This year, <a href="http://wengo.com">Wengo</a> -- the company I work for -- is sponsorising the KDE booth. We are also organizing a KDE hackfest in Wengo office (Paris, La Défense) on the 29th of January. We have an icecc network and a professional coffee machine, so if you are interested, contact me at aurelien dot gateau at wengo dot com!
<!--break-->