---
title:   "Gwenview 1.4.1"
date:    2006-11-28
authors:
  - aurélien gâteau
slug:    gwenview-141
---
Last sunday I released version 1.4.1 of Gwenview. This version contains quite a few bug fixes. Most important for me is that <a href="http://bugs.kde.org/show_bug.cgi?id=117173">the rotating bug</a> is now fixed. This one has been bothering me for a while.

Let me explain: JPEG images can contain information about their orientation, so that when you load them they are displayed with the right angle. Until 1.4.1, Gwenview used to load and display the image *before* rotating it, causing some unpleasant flickering.

This is no longer the case, Gwenview now rotates the image while it's loading it. This has the interesting side effect that your image is progressively displayed from left to right or right to left if it has been rotated. You will mostly notice it when viewing an image from a remote location.

Interested? <a href="http://gwenview.sf.net/download">grab your copy now</a>!
<!--break-->