---
title:   "Looking for a Linux friendly laptop"
date:    2007-02-10
authors:
  - aurélien gâteau
slug:    looking-linux-friendly-laptop
---
I finally decided I needed a laptop. My dream machine would meet the following criteria:

<ul>
<li>Linux friendly. I would like to "vote with my wallet" and buy a machine for which there exist open source drivers for all components.</li>
<li>I will be compiling a lot with it, so it should be quite fast at this. This means a fast processor, a decent hard disk and not turning into a bakeoven everytime a binary is linking.</li>
<li>Not the size of an aircraft-carrier. But not too small too.</li>
</ul>

If you know the model of my dream machine, please tell me!
<!--break-->