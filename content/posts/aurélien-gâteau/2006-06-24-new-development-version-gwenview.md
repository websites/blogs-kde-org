---
title:   "New development version of Gwenview"
date:    2006-06-24
authors:
  - aurélien gâteau
slug:    new-development-version-gwenview
---
A few days ago I was looking at Gwenview NEWS file. That's when I noticed how much time has passed since I last released a new version. Seems like becoming a father has a bad effect on my "free time productivity" (still I enjoy it, don't worry!).

Anyway, I decided to get a new development version out, version 1.3.91. This version contains quite a lot of changes: new UI, support for video and SVG, filter bar, enhanced KParts... <a href="http://gwenview.sf.net">have a look by yourself</a>!
<!--break-->