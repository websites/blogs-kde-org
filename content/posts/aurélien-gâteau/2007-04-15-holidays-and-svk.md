---
title:   "Holidays and SVK"
date:    2007-04-15
authors:
  - aurélien gâteau
slug:    holidays-and-svk
---
As you may know, <a href="http://blogs.kde.org/node/2747">I bought myself a laptop</a>, which I felt compelled to bring with me during my latest vacation to unload my digital camera and for the occasional late night hack.

The trouble with hacking on a laptop on holidays is that you may not have network access, meaning you can't commit your work to Subversion...

There are several solutions to this, one is to use a decentralized version control system with support for Subversion (git comes to mind, there are probably others). Instead, I decided to use <a href="http://svk.bestpractical.com">SVK</a>. Quoting the home page: «SVK is a decentralized version control system built with the robust Subversion filesystem. It supports repository mirroring, disconnected operation, history-sensitive merging, and integrates with other version control systems, as well as popular visual merge tools.»

It works by mirroring a Subversion repository, then allowing you to create local branches on it and commit to these branches. When you want to publish your changes you merge your branch with the local mirror. The extra neat thing is that it has a nice merge system which, unlike Subversion, will remember the latest merged revision for you.

So far I have been very happy with it. Here is a very light tutorial:

<!--break-->

First you need to initialize SVK (do this only the first time you use SVK on a machine)
<pre>svk depotmap --init</pre>

Then you can mirror the project you want to work on
<pre>
svk mirror svn/url/of/project //project/trunk

svk sync //project/trunk
</pre>

<code>svk sync //project/trunk</code> will replicate <i>all</i> project revisions. If you don't need that much history use this instead:
<pre>
svk sync -s start_revision //project/trunk
</pre>

If you don't care about history at all, you can use this:
<pre>
svk sync -s HEAD //project/trunk
</pre>

Note: to be able to use <code>svk sync -s HEAD //project/trunk</code> on KDE playground, I had to apply <a href="http://rt.openfoundry.org/Foundry/Project/Tracker/Display.html?Queue=82&id=17278">this patch</a>. This is a bit frightening, but it works-on-my-machine(tm). This is with SVK version 1.08, I don't know if latest versions solve the problem.

You can go offline at this point.

Now you need to create a local branch (you can create others if you want)
<pre>
svk copy //project/trunk //project/local
</pre>

Get a copy of your branch:
<pre>
svk checkout //project/local project
</pre>

Note: Unlike SVN, SVK working copies can't be moved or renamed, so make sure you checkout your copy correctly.

Hack, very similar to svn:
<pre>
svk st
svk log
svk diff
svk ci
...
</pre>

Time to get the latest changes from upstream. Of course you need to be online at this point.

<pre>
svk sync //project/trunk
</pre>

Now we can apply upstream changes to our local branch:

<pre>
# -C == --dry-run
svk smerge -C //project/trunk //project/local

# -l == auto generate commit message
svk smerge -l //project/trunk //project/local
</pre>

Now is the time to publish our work, let's commit our changes upstream. This is actually almost the same as applying upstream changes to our local branch, we just swap the source and destinations.

Note: anything committed to //project/trunk is also committed to the Subversion server.
<pre>
svk smerge -C //project/local //project/trunk
svk smerge -l //project/local //project/trunk
</pre>

The above command will commit all pending changes in <b>one</b> Subversion commit. If you want to create one Subversion commit per SVK commit, use this instead:
<pre>
# -I == non interactive
# --verbatim == use the same log message for Subversion commit as for SVK commit
svk smerge -I -l --verbatim //graphics/local //graphics/trunk
</pre>

If you want more details, please check this very nice <a href="http://www.bieberlabs.com/wordpress/archives/2004/11/30/using-svk">SVK tutorial</a>.
