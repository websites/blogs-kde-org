---
title:   "Clara, 8th wonder of the world"
date:    2006-04-02
authors:
  - aurélien gâteau
slug:    clara-8th-wonder-world
---
I'm sorry to disappoint all other parents of the world, but I'm now the proud father of Clara, the cutest baby to have ever appeared on earth :-)

In case you do not believe me, here is a proof:

[image:1897]

Of course, you can expect some (even longer) delay in the way I handle mail. Sorry for the inconvenience...
<!--break-->