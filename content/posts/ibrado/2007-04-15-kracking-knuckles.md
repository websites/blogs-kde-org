---
title:   "Kracking knuckles"
date:    2007-04-15
authors:
  - ibrado
slug:    kracking-knuckles
---
It's been a while (3 years!) since my last post regarding "changing the stars" of Open Source developers. I faded out from the KDE developer scene to concentrate on the corporate job I was offered then (though not because of that post :-P). Now that my duties at work have stabilized, I've "followed my feet" back to Open Source and KDE.

To get my gears grinding again, I decided to start small -- literally. In the past month, I've written and released a couple of tray icon applets:

1. For eyecandy lovers, <a href="http://kompanion.kdex.org/">Vladstudio Kompanion</a> [<a href="http://www.kde-apps.org/content/show.php?content=55610">kde-apps</a>, <a href="http://freshmeat.net/projects/vskompanion/">freshmeat</a>]. It's a port of the Companion utility at <a href="http://www.vladstudio.com/">vladstudio.com</a> which allows you to monitor and install beautiful wallpapers from that site.

2. For inveterate scripters, <a href="http://konch.kdex.org/">Konch</a> [<a href="http://www.kde-apps.org/content/show.php?content=56313">kde-apps</a>, <a href="http://freshmeat.net/projects/konch/">freshmeat</a>]. It's a system tray proxy that allows your scripts to change its appearance, create menus, etc. via standard I/O, DCOP, or command line parameters.

Please give them a try. And do be kind; I'm a bit rusty. :-D




