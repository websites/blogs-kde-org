---
title:   "Rant of the day (22/4)"
date:    2004-04-23
authors:
  - zogje
slug:    rant-day-224
---
I'm a fairly happy puppy with little to rant about at the moment but as I explained earlier, I have a problem coming up with good titles, so I stick with this one.

Mostly working on <a href="http://extragear.kde.org/apps/kiosktool.php">kiosktool</a> this week. I have done a first release last week and haven't received any bugreports so far which probably means that it is so broken that people don't even bother reporting about it. I hope to have a better looking version out this weekend. If you want to have bugs fixed you better hurry reporting them.

Have been discussing the shared mimetype spec with Jonathan Blandford tonight, hope to get some code for that in KDE next week. I'm afraid it will require a lot of icon renaming.

The New project has completed phase 2 in the meantime. <a href="http://blogs.kde.org/node/view/445">Action photo available</a>

Nice to hear about the <a href="http://ktown.kde.org/~howells/award.JPG">award</a> that the <a href="http://dot.kde.org/1082665263/">KDE guys in London</a> got. After working on KDE for quite some years I sometimes tend to forgot how far we have come, I find these awards always a good moment to look back and appreciate the progress that we have made. We still have 6592 open bugs on bugs.kde.org to go of course :-)
