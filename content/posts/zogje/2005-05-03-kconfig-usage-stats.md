---
title:   "KConfig usage stats"
date:    2005-05-03
authors:
  - zogje
slug:    kconfig-usage-stats
---
When thinking about a future configuration system it's nice to have an idea about current usage patterns. So I did some measurements to see how many different configuration keys KDE applications look up when starting up. <!--break--> Some results:

konqueror 743
kcalc     235
kedit     220
kword     554
kasteroids 180

Starting KDE itself resulted in 4529 different lookups in 60 different config files. A lot of that seems to be related to global settings, e.g. when kedit starts it looks up keybindings in e.g. keditrc and then kdeglobals, when kcalc starts up it looks for the same settings first in kcalcrc and then in kdeglobals. As a result the same setting leads to two different lookups. If I ignore the file in which the lookup is done, I find 1711 lookups for different keys.

Note that the above numbers do not take system defaults or profile settings into account.

Do these numbers matter? A little bit... it shows that it is probably a bad idea to do any communication/processing on a per key basis. Currently KConfig does processing on a per file basis. What if we did processing on a per group basis? Starting KDE accesses 1510 different file/group combinations. Things look better if we ignore the file in which the lookup is done, the number then drops to 368 different groups.

To benefit from that reduction we would need to change the code in the various kde libs to explicitly address kdeglobals instead of the default application config object. There is no benefit in changing that with the current KConfig implementation because KConfig performance is mostly determined by the number of different config files we read (disk seeks!), not by the number of groups or keys that we lookup.
