---
title:   "Apache: Wake up and smell the roses"
date:    2006-06-06
authors:
  - zogje
slug:    apache-wake-and-smell-roses
---
The Apache foundation <a href="http://issues.apache.org/bugzilla/show_bug.cgi?id=38301">steadfastly</a> <a href="http://issues.apache.org/bugzilla/show_bug.cgi?id=37185">refuses</a> to include support for the OpenDocument filetypes to its distribution despite the mimetypes being <a href="http://www.iana.org/assignments/media-types/application/">registered</a> with IANA. Appearantly the ASF doesn't <a href="http://mail-archives.apache.org/mod_mbox/www-legal-discuss/200507.mbox/%3C20050713151848.GA84909%40mail26b.sbc-webhosting.com%3E">agree</a> with the OASIS IPR policy seemingly unaware that the OASIS OpenDocument TC has <a href="http://www.oasis-open.org/archives/members/200602/msg00017.html">switched</a> to a most liberal IPR policy (<a href="http://www.oasis-open.org/who/intellectualproperty.php#10.2.3">Royalty Free with restrictions on possible licensing conditions</a>) earlier this year.

It's not that the Apache foundation is developing a better open standard for office documents (ASCII maybe?) and so far they do not seemed to be bothered the least shipping mimetypes for proprietary formats such as <a href="http://svn.apache.org/viewvc/httpd/httpd/trunk/docs/conf/mime.types?view=markup">QuickTime (video/quicktime), MP3 (audio/mpeg) or *gasp* Microsoft Word (application/msword)</a>. I'm sure it makes sense in their world, it doesn't in mine.
<!--break-->