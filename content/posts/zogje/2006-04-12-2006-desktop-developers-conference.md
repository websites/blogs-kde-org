---
title:   "2006 Desktop Developers Conference"
date:    2006-04-12
authors:
  - zogje
slug:    2006-desktop-developers-conference
---
Want to go to a woop-ass desktop developers conference and don't have the patience to wait till september? On July 17-18 the <a href="http://www.desktopcon.org/2006/">2006 Desktop Developers Conference</a> (DDC) will be held in Ottawa located in visa-friendly Canada. OSDL DTL has partnered with the DDC organisation team and will be providing financial assistance to cover travel costs of a number of selected speakers. THAT CAN BE YOU! You still have till the end of the month to send in your <a href="http://www.desktopcon.org/2006/cfp.php">speaking proposal</a> to DDC. Let the world know about the groundbreaking developments that you are doing.

In particular if you are working on the integration of wireless, laptop specific features or other hardware related integration DDC would like to hear from you. Steps that KDE is taking in KDE4 to make it easier for independent software vendors (ISVs) to integrate their applications with KDE is also an excellent topic. A number of other suggested topics can be found on <a href="http://www.desktopcon.org/2006/event_info.php">http://www.desktopcon.org/2006/event_info.php</a> Is your favourite topic not listed? Don't worry, just send in your proposal!
<!--break-->