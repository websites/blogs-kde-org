---
title:   "Krazy Moves to Gitorious"
date:    2010-10-31
authors:
  - awinterz
slug:    krazy-moves-gitorious
---
I just finished moving the Krazy repository from KDE SVN trunk/quality to <a href="http://gitorious.org/krazy/krazy">Gitorious</a>.

Seems to work ok.
For the record, here's how I did it:
<code>
% git svn clone svn+ssh://winterz@svn.kde.org/home/kde/trunk/quality/krazy2
% cd krazy2
% git remote add origin git@gitorious.org:krazy/krazy.git
% git push origin master
</code>

Note that the "git svn clone" took a few hours since it searches through all 1.2million revisions in the KDE SVN looking for krazy2 commits.

"git log" shows a very long list of changes we made over the years so I'm content that we have some history.


