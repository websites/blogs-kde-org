---
title:   "KDEPIM 4.4.6 Available"
date:    2010-09-10
authors:
  - awinterz
slug:    kdepim-446-available
---
Tarballs for kdepim-4.4.6 and kdepim-runtime-4.4.6 are now available from a mirror near you.

<a href="ftp://ftp.kde.org/pub/kde/stable/kdepim-4.4.6/src">ftp://ftp.kde.org/pub/kde/stable/kdepim-4.4.6/src</a>


There is no associated kdepim-l10n-4.4.6 tarball since I don't know how to make one -- but there are also no new messages strings so the existing 4.4.5 translations should work.

There is also no ChangeLog since that also is something I'm not sure how to do.

If there are volunteers to make the l10n or Changelog, feel free to contact me about it.