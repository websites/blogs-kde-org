---
title:   "KDEPIM 4.4.8 Tagged and Released"
date:    2010-12-04
authors:
  - awinterz
slug:    kdepim-448-tagged-and-released
---
I just finished tagging and releasing KDEPIM 4.4.8.  Should be showing up in the usual place on ftp.kde.org soonish.

Here's the Changelog:
<code>
2010-12-02 20:26  djarvie

        * [r1202940] Make disabled icon more distinguishable for colour
          blind users

2010-11-30 22:41  tmcguire

        * [r1202450] Backport r1202449 by krake from trunk to the 4.4
          branch:
          
          Use hashes for storing pending items instead of lists to avoid
          double entries.
          Having the same item twice in the added/modified list will cause
          the resource's transaction to fail.

2010-11-24 12:25  mlaurent

        * [r1200243] Fix Bug 257594 - Crash on closing kmail
          BUG: 257594

2010-11-21 17:39  djarvie

        * [r1199363] Disable Defer button when no deferral is possible

2010-11-21 17:38  djarvie

        * [r1199362] Disable Defer button when no deferral is possible

2010-11-19 20:23  djarvie

        * [r1198809] Update changelog

2010-11-19 20:15  djarvie

        * [r1198805] Fix conversion of non-recurring alarms with simple
          repetitions, from version < 1.9.10.

2010-11-19 19:45  djarvie

        * [r1198795] Fix reminder time shown when editing a non-recurring
          alarm's deferred reminder.

2010-11-17 22:49  djarvie

        * [r1198233] Improve fix for working-time-only alarms not
          triggering if KAlarm is started up outside
          working hours, after the last trigger time during working hours
          was missed.
          (See rev 1197422, 1197414.)
2010-11-17 22:14  djarvie

        * [r1198227] Fix crash in properties.constFind()

2010-11-15 19:50  djarvie

        * [r1197426] Add missing change

2010-11-15 19:06  djarvie

        * [r1197422] Better code for revision 1197414 fix (including revert
          of kaevent.{h,cpp} changes)

2010-11-15 17:54  djarvie

        * [r1197414] Fix working-time-only alarms not triggering if KAlarm
          is started up outside
          working hours, after the last trigger time during working hours
          was missed.

2010-11-05 19:24  winterz

        * [r1193375] merge SVN commit 1193359 by winterz from e35:
          
          in slotMsgActivated(), make sure to set the currentMessage for
          the MessageActions
          properly before attempting to edit it.
          should solve the crash in kolab/issue4653

2010-11-05 13:20  mlaurent

        * [r1193302] Fix Bug 256129 - kmail does not remember Display
          Tooltips setting in message list
          There is two config in kmail and in messagelist (I don't like it
          but I can't change it)
          => fix sync

2010-11-05 01:02  djarvie

        * [r1193207] Don't quit if no window is visible when 'show in
          system tray' is deselected.

2010-11-04 19:18  djarvie

        * [r1193134] Bug 255673: start minimised at login when configured
          not to show in tray

2010-11-04 09:31  mlaurent

        * [r1192928] Fix Bug 256022 - Delete Filter -> Wrong Display
          BUG: 256022
2010-11-03 18:42  mlaurent

        * [r1192678] Fix Bug 255969 - KMail crashed on first run
          BUG:255969

2010-11-01 00:43  djarvie

        * [r1191727] Bug 255673: Fix KAlarm showing in system tray at login
          when configured not to show in tray.

2010-10-31 17:49  djarvie

        * [r1191613] Update for kdepim 4.4.7

2010-10-26 17:26  mlaurent

        * [r1190032] Allow to copy/paste html text from an email

2010-10-25 15:42  mlaurent

        * [r1189698] Fix Bug 230276 - build fails with -g - undefined
          symbols
          Add patch (can't test but it compiles fine)
          BUG 230276

2010-10-25 13:57  mlaurent

        * [r1189655] fix Bug 186104 - I can't choose "Load external
          Reference"
          BUG: 186104

</code>


