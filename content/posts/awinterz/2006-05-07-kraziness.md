---
title:   "Kraziness"
date:    2006-05-07
authors:
  - awinterz
slug:    kraziness
---
And now a message from our sponsor...
<p>
A special offer for KDE developers!  Now you can run the Krazy Code Checker locally on your code!  <br>Here's how:
<p>
<code>
% svn TOP_OF_YOUR_KDE_SVN_TREE
% svn update -N trunk/playground/devtools
% cd trunk/playground/devtools
% svn up krazy
% cd krazy
% ./install                  # installs into /usr/local.  edit $TOP to change the destination dir
</code>
<p>
If all goes well, <i>krazy</i> should be installed, as well as the <i>krazy</i> man page.
<p>
Example Krazy runs:
<code>
% krazy *.cpp *.h
% find . -name "*.cpp" -o -name "*.h" | xargs krazy
</code>

<p>
Note that I'll continue to add checks, so you might want to 'svn update' regularly.

<p>
And don't forget to read the <i>krazy</i> man page for more info, including how to write your own checker plugins.
