---
title:   "Help for KDE CMake Modules"
date:    2008-06-19
authors:
  - awinterz
slug:    help-kde-cmake-modules
---
We now have documentation for our <a href="http://api.kde.org/cmake/modules.html">custom KDE CMake Modules</a>, brought to you by the <a href="http://www.englishbreakfastnetwork.org">EBN</a>.  I have a little script that runs every night to re-generate this page, just in case we changes things as time goes by.

But, if you're like me, you'd rather have quick access to a man page. You can create one with the following command:
<code>
% cmake -DCMAKE_MODULE_PATH=/path/to/kdesvn/trunk/KDE/kdelibs/cmake/modules --help-custom-modules /path/to/kdeinstall/share/man/man1/kdecmake.1
</code>
making sure that /path/to/kdeinstall/share/man is in your $MANPATH, of course.