---
title:   "Man Pages for API Dox"
date:    2007-09-27
authors:
  - awinterz
slug:    man-pages-api-dox
---
I like man pages.

So today I wrote a little script called kdedoxyman.sh that uses <a href="http://doxygen.org">doxygen</a> to generate API Dox in man page format
from our KDE source code.  

To use:
<code>
% cd trunk/KDE/kdelibs
% ../kdesdk/scripts/kdedoxyman.sh -d /path/to/kde/share
[...Wait...]
</code>
<br>

Replacing "/path/to/kde" in these instructions with the real path to your KDE4 installation.

Repeat for other KDE modules: kdepimlibs, etc.

Now add <tt>/path/to/kde/share/man</tt> to your MANPATH environment variable (or add "MANPATH /path/to/kde/share/man" to your <tt>/etc/man.config</tt> file).

At this point you should be about to run 'man KAboutData', for example, from the comfort of your konsole command line.

Of course, <a href="http://api.kde.org">api.kde.org</a> is useful too :)