---
title:   "Stable and Non-Stable APIDOX Urls"
date:    2011-12-18
authors:
  - awinterz
slug:    stable-and-non-stable-apidox-urls
---
FYI:

I just added some symbolic links on the <a href="http://api.kde.org.">api.kde.org</a> machine to make accessing the 'stable' and 'unstable' dox a little easier.

So now stable points to 4.7 and unstable points to 4.8.

For example:
<a href="http://api.kde.org/stable/kdepimlibs-apidocs">http://api.kde.org/stable/kdepimlibs-apidocs</a> is the url for the stable kdepimlibs 4.7
<a href="http://api.kde.org/unstable/kdepimlibs-apidocs">http://api.kde.org/unstable/kdepimlibs-apidocs</a> is url for the upcoming kdepimlibs 4.8

Might want to keep this in mind when putting links into your documentation. for example.

Now I need to remember to move those symlinks when a new release comes along