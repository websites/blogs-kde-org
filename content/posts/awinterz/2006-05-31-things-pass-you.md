---
title:   "Things that pass you by..."
date:    2006-05-31
authors:
  - awinterz
slug:    things-pass-you
---
I eagerly await the delivery of my new baby... err... my new desktop system (the system is a "he" and we've already named him jiffy).  According to the FedEx tracker, jiffy will be delivered tomorrow.  I'm having the <a href="http://en.wikipedia.org/wiki/Heebie_Jeebies">heebee-jeebies</a> waiting to do KDE-development again. Sure the computer is good for email and web surfing and keeping your checkbook, but the real fun is using the computer for software development.  I'm sure we all agree with that.

<p>
Anyhow, back to the real reason for this blog.

<p>
With a new computer one needs an up-to-date O/S.  So, I decided to upgrade all the WinterNet household computers to Fedora Core 5.  Today I decided to burn the Fedora Core 5 installation CDs.

<p>
The burning works fine with <a href="http://k3b.org">k3b</a> until installation CD #3.  For this CD I keep getting an error at the 98% completion?  What could be wrong?  Search the archives and the forum.  Nothing.

<p>
Then I see... hmm. CD #3 is 687MB large.  Now, the last time I looked (or cared, for that matter), CDs could only hold 650MB.  Why would Fedora make an iso image larger than that?  Huh.  Could it be that CDs can hold more than 650MB these days?  So I travel down to my local Wal-Mart and .. wouldyaknow.... all the CD-RWs on the shelves can hold up to 700MB.  When did that happen???

<p>
I wonder what else is passing me by?  People still listen to vinyl records, don't they?


