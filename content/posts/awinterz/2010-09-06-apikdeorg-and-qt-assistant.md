---
title:   "api.kde.org and Qt Assistant"
date:    2010-09-06
authors:
  - awinterz
slug:    apikdeorg-and-qt-assistant
---
<a href="http://api.kde.org">The KDE API Reference (api.kde.org)</a> web site is now providing  ".qch" files suitable for loading into Nokia's Qt Assistant.

Look for the "[qt]" links.

Currently, auto-regeneration of these files happens on a nightly basis for kdelibs and kdepimlibs trunk, and for kdelibs and kdepimlibs 4.5.  More can be added, upon request.

To use load these files into assistant:
* download them to a safe place
* in assistant, [Add] them from the Edit->Preferences->Documentation tab

I'll leave it to the experts to tell us how best to load these files into KDevelop or QtCreator.

<b>Warning</b>: these files are not small.