---
title:   "kDebug areas Be-Gone (almost)"
date:    2007-12-29
authors:
  - awinterz
slug:    kdebug-areas-be-gone-almost
---
Thanks to Marc Mutz's <a href="http://lists.kde.org/?l=kde-core-devel&m=119875146731279&w=2">idea</a>, one can now set their application debug area by telling the C++ compiler about it and then never having to remember that debug area again.

"Set it and forget it"

Simply add the following line into your application's top-level CMakeLists.txt
<pre>
  <code>add_definitions(-DKDE_DEFAULT_DEBUG_AREA=XXXX) </code>
</pre>
(replacing XXXX with your debug area).

Then replace <code>kDebug(XXXX)</code> with <code>kDebug()</code> in your code and you never have to worry about remembering (or correctly typing) your debug area again.
