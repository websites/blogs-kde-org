---
title:   "Apple reduced the diff between JavaScriptCore's and KHTML's kjs"
date:    2005-07-14
authors:
  - martijn klingens
slug:    apple-reduced-diff-between-javascriptcores-and-khtmls-kjs
---
A couple of weeks ago I was curious about the differences between Apple's kjs and our own and decided to download Webkit and do a diff. It turned out that there is still a substantial gap between both trees that will be very hard to fix.<br>

A lot of this requires someone intimately familiar with the codebase. However, I also saw some very low hanging fruit in the form of typo fixes and other trivialities that can be extracted from the diff without requiring a lot of knowledge about KJS' internals. Yet, despite being easily mergable, those changes clobber the overall diff, making it harder to spot the actual code changes.<br>

Also, extracting this stuff is fairly boring. It made an excellent fit for a dull Friday evening where I didn't feel like doing anything serious whatsoever. And so I went and extracted a first set of patches and <a href="http://bugzilla.opendarwin.org/show_bug.cgi?id=3945">submitted</a> them to Apple. Quicker than I expected the changes got merged today, <b>reducing the diff between both trees by over 1000 lines!</b><br>

The bad thing is, there is "only" 16k lines of diff left, and that's only counting KJS and not KHTML!<!--break-->

The top 5 files with most lines changed forms over two thirds of this:<br>
<pre>859     internal.cpp
932     date_object.cpp
1167    ustring.cpp
1196    function.cpp
2300    nodes.cpp
-----   ---------------- +
6454</pre><br>

Those files are basically unmergable by anyone but the KJS core developers at our or Apple's side. At the other end of the list are the files with the fewest lines changed:<b>
<pre>11      simple_number.h
12      date_object.h
12      number_object.h
13      array_object.h</pre><br>

Too bad those are header files and thus require merging in their implementations too.<br>

There's two places where potential merges will be the most likely, the .cpp files with little changes and the remaining non-code changes like comments and other trivialities. I expect the latter to have the potential to shave another 1000 lines off of the diff. The rest will be <b>very</b> hard work, and I doubt it'll be me who is able to pull it off, not even considering the amount of time it'd take to do.<br>

The bottomline is that Apple is happy to incorporate sensible patches and they are fairly quick at it too. This makes it worthwhile to invest time to reduce diffs, especially now, because after KDE will move to Qt 4 the diff will only grow bigger again. At the same time I can only hope that the diff can get below, say, 10,000 lines before aKademy, or at least before the move to Qt 4.<br>

Time will tell I guess. At least things are better than they used to be with Apple being open for patches now...