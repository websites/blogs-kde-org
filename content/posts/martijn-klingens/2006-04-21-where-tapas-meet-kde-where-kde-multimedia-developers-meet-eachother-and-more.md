---
title:   "Where tapas meet KDE, where KDE Multimedia developers meet eachother and more"
date:    2006-04-21
authors:
  - martijn klingens
slug:    where-tapas-meet-kde-where-kde-multimedia-developers-meet-eachother-and-more
---
This week has been interesting, to say the least. Monday was an official holiday in the Netherlands (called "2nd Easter Day") which I used to extend the break from work and KDE that I took last weekend.

Charged with fresh energy came Tuesday, the third Tuesday of the month, which marks the monthly dinners/meetings of the <a href="http://www.hollandopen.nl/">Holland Open Source Platform (HOSP)</a> (Dutch version only). KDE NL attends most of these -- often very interesting -- meetings. Last month it was <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/167-IANAL,-but-I-can-blog-about-one.html">Adriaan </a> and me, this time <a href="http://elektron.et.tudelft.nl/~cmlot/">Claire</a> and me went, to Utrecht this time. This month's <a href="http://www.hollandopen.nl/agendaevent.jsp?nr=1382">presentation</a> was about Business Models and making open source commercially interesting.

The <i>real</i> selling point however was the dinner, a tapas buffet. I just can't say "no" to that, and they had delicious food. Tuna salad that made me relive fond memories of Sevilla, garlic mushrooms that reminded me of Barcelona, and great marinated olives that you can find all over Spain basically. Too bad I took too much of these, so I couldn't try many of the other dishes, like the Paella. I know, luxury problem! ;)

There were lots of familiar faces around, which was extra interesting given that many of them approached us about our sponsoring letter for the upcoming <a href="http://wiki.kde.org/tiki-index.php?page=multimedia-meeting">KDE MultiMedia Meeting</a>, modelled after last year's extremely productive <a href="http://dot.kde.org/1117213451/">KDE PIM Meeting</a>.

We heard a <i>lot</i> of interesting stuff during the evening. One of the bigger items was the announcement of a <a href="http://www.hollandopen.nl/article.jsp?nr=1515">Dutch alternative</a> for Google's <a href="http://code.google.com/soc/">Summer of Code</a>. Other scoops are not official yet, but hopefully we can disclose more about them in a couple of weeks, if they indeed turn out useful for KDE.

The evening also showed once again that KDE really needs to collect presentations, since we got requests to give several talks, but so far we're severely lacking the manpower to prepare all of these, hampering our visibility. At least for me it's much more frustrating to have great opportunities and having to let go due to lack of time or other resources than it is to not get an opportunity in the first place.

On the way back home Claire and me discussed some ideas to leverage <a href="http://www.spreadkde.org">SpreadKDE</a> to collect presentation material in a central location and streamline the process. This would depend on an upgrade from Drupal 4.6 to 4.7 and the resource framework that Tom has been working on. So he couldn't have done better than hand us <a href="http://tom.acrewoods.net/node/423">exactly this</a> the next day, even before we raised the issue on the marketing list! Thanks Tom! It will take a while until our backlog of work has dropped enough to start this, but it's pretty high up the priority list, so hopefully it won't take too long.

And to top off a pretty good week, today we got confirmation from enough sponsors to take the decision to go for the above-mentioned MultiMedia Meeting. During the HOSP meeting that was still uncertain.

Oh, and the KDE business cards are probably finally underway, I expect them delivered within a couple of days. The ordering process alone has probably cost me a year of my live because the web site wasn't really finished yet and we couldn't wait any longer or Wade would not have his cards in time for <a href="http://wadejolson.blogspot.com/2006/03/i-love-it-when-good-plan-comes.html">his talk</a> on the Desktop Linux Summit. While fixing the remaining issues we'll probably do another small, controlled, batch before opening up the process for all KDE e.V. members, but the end is finally near!

In non-KDE news, <a href="http://imdb.com/title/tt0438097/">Ice Age: The Meltdown</a> is a great movie! Claire, me and a bunch of other friends went there last weekend and it was more than worth visiting. Back at Claire's place we played Settlers of Catan, which was new to me, and which was a nice twist from Carcassonne, which I usually play instead. The friend of mine who introduced me to Carcassonne once said that luck is less important there than it is in Catan, and I think that's true, but it was still lots of fun :)

Whew... I probably should have split this blog post in smaller parts. Anyone still with me until here? ;)

<!--break-->