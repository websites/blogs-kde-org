---
title:   "I should blog more often"
date:    2006-03-24
authors:
  - martijn klingens
slug:    i-should-blog-more-often
---
I really should blog more often. My last blog has been over four months ago. And it's not like nothing happened since then.

KDE promotion and marketing hasn't been so alive in years. After aKdemy Sebastian, Wade and me started the first Marketing Working Group and together with the help of several other people (notably, Tom, who has done a tremendous amount of work there) we launched <a href="http://www.spreadkde.org">SpreadKDE</a> back last November.

Since then Sebastian, Wade and Aaron have done a nice job of keeping people updated on our achievements and attracting new people. And me? I've done the boring behind-the-scenes stuff, like keeping track of things, maintaining the task list and all the other things that will never give me rock-star status.

On the upside, it's rewarding work to do since I can help turning chaos into order, and it gives me the gratitude of my fellow KDE marketing friends, which of course makes it all worth it.

The developments for <a href="http://www.spreadkde.org">SpreadKDE</a> look promising, we have work underway on a new look, events tracking, hardcore research, screenshots and our booth gear, amongst several other things. Exciting to say the least, especially when comparing this to a year ago!

My only personal peeve is the current task management, which is little more than a wiki page maintained mostly by me. Real task management is on the way, so I really have no right to complain (besides, I don't have time to work on it, giving me even less rights to complain). It's just that the task list is slowly getting outdated because I can't keep up with all the good stuff we are discussing, and that frustrates me.

So, while all of you are looking forward to fancy stuff like nice screenshots and a new look I'm looking forward to something as boring as a task manager :)

And I'll try to blog more often.
<!--break-->