---
title:   "Post-after-dinner dip"
date:    2006-05-26
authors:
  - martijn klingens
slug:    post-after-dinner-dip
---
As you've no doubt read on <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/212-KDE-Four-Multimedia-kickoff.html">Adriaan's blog</a>, the <a href="http://www.kde.nl/agenda/2006-05-k3m/index-en.php">KDE Four Multimedia Meeting</a> kicked off today. Previously known as <i>K3M</i>, we decided to extend the name to reflect the other KDE Four developer meetings that will be held later this year and give them a more common naming.

While Adriaan mentioned an <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/215-After-dinner-dip.html">after-dinner dip</a> in his last blog, currently there's activity everywhere. The group is split into several brainstorming sessions and people hacking away deeply concentrated.

The atmosphere is as inspiring as last year, even while the weather is infinitely worse. Hopefully we will at least <b>see</b> the sun tomorrow, but the weather forecast is about as bad as it can be.

If you haven't done so yet, be sure to check out <a href="http://vizzzion.org/?id=gallery&gcat=KDE4MultimediaMeeting">Sebas' pictures</a> of the afternoon session. More pictures will hopefully follow later.
<!--break-->