---
title:   "Building on new pillars: Activities and KPeople in Plasma 5.3"
date:    2015-04-25
authors:
  - eike hein
slug:    building-new-pillars-activities-and-kpeople-plasma-53
---
With the release of Plasma 5.3 only <a href="https://techbase.kde.org/Schedules/Plasma_5">days away</a>, it's time to start talking about some of the new features users of Plasma Desktop will get their hands on in the new version.

<h1><b>Activities</b></h1>

Many Plasma Desktop users are already familiar with Activities as a toolbox to divide and organize their personal computing environment into shards: You can set up and name activities, distribute application windows across them, associate relevant files with them, switch between, start and stop them to your heart's content.

A previously less explored side to the Activities story is what the technology can offer while working inside any one of your activities - thus even if you don't bother with setting up more than one and just stick to the default activity created for you the first time you log into Plasma.

This changes with Plasma 5.3! New work to make it easier for applications to enter useful data into the activity's knowledge store, as well as run smart queries against it, enable some new and improved functionality in both the Application Menu and Task Manager widgets in this release.

In Application Menu, all of the <i>Recent</i> categories (one of which is new) are now powered by Activities. One immediate advantage of this is that their contents now change with the activity you're in, but as promised it also provides several other inherent benefits:

<center><img src="http://www.eikehein.com/kde/blog/plasma53_pillars/plasma53_kicker01.png" alt="The three Recent categories in Application Menu 5.3" /><br /><small><i>The three Recent categories in Application Menu 5.3</i></small><br /><br /></center>

<ul>
<li><b>Recent Applications</b> used to be only aware of applications launched through Application Menu itself. With launch info now retrieved from the Activities knowledge store and all other launchers (for example KRunner, but also just opening a document from Dolphin is a launch) free to enter data there, this submenu can now gather much more recent system activity in one place. Furthermore, new data sources can be added as time goes on without having to touch Application Menu itself.<br />&nbsp;</li>
<li><p><b>Recent Documents</b> benefits in a similar way. Over the older KRecentDocument framework used before, the Activities backend offers access to a longer, richer backlog of document use data. User control over the data is also enhanced - it's now possible to forget about individual documents from the context menu, instead of being forced into a complete wipe.<br />&nbsp;</p>
<p>But that's not all on the documents front! Activities enable both Application Menu and Task Manager to easily query for recent documents <i>by the application they were opened in</i>, leading to lists of recent docs in the context menus of various types of application launcher buttons:</p>

<center><img src="http://www.eikehein.com/kde/blog/plasma53_pillars/plasma53_kicker02.png" alt="Per-application recent documents in Application Menu and Task Manager 5.3" /><br /><small><i>Per-application recent documents in Application Menu and Task Manager 5.3</i></small></center><br />&nbsp;</li>
<li><b>Recent Contacts</b> is a brand new addition to Application Menu 5.3 showing the people you've been in touch with recently, along with their realtime status information. The first part of this is handled through Activities; dealing in people data, however, is facilitated by a recent addition to KDE <a href="http://en.wikipedia.org/wiki/KDE_Frameworks_5">Frameworks 5</a>: KPeople.<br />&nbsp;</li>
</ul>

<h1><b>KPeople</b></h1>

<center><img src="http://www.eikehein.com/kde/blog/plasma53_pillars/plasma53_kicker03.png" alt="Application Menu's Recent Contacts category in Plasma 5.3" /><br /><small><i>Recent Contacts in Application Menu 5.3: Powered by Activities and KPeople</i></small><br /><br /></center>

The KPeople framework allows applications (including the shell) to retrieve lists of people and information about them (status, contact metadata), as well as perform actions on them - e.g. initiate a chat, as used by Application Menu when you click on an entry in the Recent Contacts category.

The data backing this interface is provided by backend plugins. The first implementer of a fully-featured KPeople backend is of course our very own <a href="https://userbase.kde.org/Telepathy">Telepathy</a> instant messenging suite. In the future, other KDE communication apps will integrate with KPeople, making conversations had e.g. in KDE's IRC client Konversation pop up in Application Menu as well.

<h1><b>Closing thoughts</b></h1>

Both the enhanced Activities backend and KPeople are very fresh. While we work hard to line up everything behind them (implement additional data sources and optimize those implementations, for example), they are still exposed somewhat carefully in Plasma Desktop 5.3. The Recent Contacts category in Application Menu, for example, is disabled in this release by default, because a Telepathy release entering conversation data into the Activities knowledge store is still pending. It can be enabled from the widget's config dialog.

In future releases you can expect us to make more of these pillars as our confidence grows, with additional UI functionality built on them and more prominent placements of it in various areas of the shell. Lots of work ahead!