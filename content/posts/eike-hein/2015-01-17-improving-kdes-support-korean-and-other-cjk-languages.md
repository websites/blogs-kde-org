---
title:   "Improving KDE's support for Korean (and other CJK languages)"
date:    2015-01-17
authors:
  - eike hein
slug:    improving-kdes-support-korean-and-other-cjk-languages
---
<center><img src="https://www.eikehein.com/kde/blog/cjk/hunminjeongeum.jpg" alt="Hunminjeongeum" /><br /><small><i>The <a href="http://en.wikipedia.org/wiki/Hunminjeongeum">Hunminjeongeum</a> (or 훈민정음). This 1446 document first introduced the <a href="http://en.wikipedia.org/wiki/Hangul">modern Korean writing system</a> to the Korean people and is now listed among the <a href="http://www.unesco.org/new/en/communication-and-information/flagship-project-activities/memory-of-the-world/register/full-list-of-registered-heritage/registered-heritage-page-8/the-hunmin-chongum-manuscript/#c187086">UNESCO Memory of the World</a>. (Photo: <a href="https://www.flickr.com/photos/koreanet/14324130250/">Jeon Han</a>, CC BY-SA 2.0)</i></small><br /><br /></center>

In addition to my usual work on things like Plasma, I've been hacking away on bugs that pose barriers to the use of the Korean language and writing system in KDE/Qt systems lately (I took up studying Korean as a new hobby). As a bonus, many fixes also tend to help out users of other CJK (Chinese, Japanese, Korean) languages, or even generally of languages other than English.

Localization defects come in many shapes and forms and tend to be fun puzzles to solve, with lots of vertical cross-cutting through the stack. Here's a few examples from my plate in the past half year:

<b>Kate: <a href="https://bugs.kde.org/show_bug.cgi?id=339467"><i>Input method / IME / ibus support doesn't seem to work well in KF5 version</i></a> and the resulting Qt: <a href="https://bugreports.qt-project.org/browse/QTBUG-41640"><i>ibus plugin badly maps text attributes to QTextCharFormat</i></b>

On Linux systems using <a href="https://code.google.com/p/ibus/">IBus</a> for complex writing system input (which is most of our desktops), Qt 5 would generate <a href="http://doc-snapshot.qt-project.org/qt5-5.3/qinputmethodevent.html">QInputMethodEvents</a> with badly-formed formatting directives, causing pre-edit text (such as incomplete <a href="http://en.wikipedia.org/wiki/Hangul">Hangul</a> blocks) to turn invisible in some applications. This fixes text entry in Korean and any other language doing complex in-line composition in KWrite/Kate, KDevelop and other applications built on KDE's KTextEditor framework (and likely others).

<b>Amarok: <a href="https://bugs.kde.org/show_bug.cgi?id=333946"><i>Podcast save location containing Korean characters are garbled</i></b>

A fun mess where a chain of <code><a href="http://doc.qt.io/qt-5/qstring.html#arg">QString::arg()</a></code> calls against a template string would go awry by replacing a URL with percent-encoded Korean into the template, causing subsequent calls to trip up on unexpected % placeholders. Your Korean podcasts will now work nicely again in Amarok.

<b>KCodecs and kdelibs: <a href="https://bugs.kde.org/show_bug.cgi?id=243513"><i>KCharsets does not support CP949 encoding</i></a>

<a href="http://en.wikipedia.org/wiki/Code_page_949">Code page 949</a> is a superset of the <a href="http://en.wikipedia.org/wiki/Extended_Unix_Code#EUC-KR">EUC-KR</a> character encoding. Introduced by Microsoft, it supports additional Korean characters not supported by EUC-KR and remains in use on some Korean websites and <a href="http://www.hanirc.org/">IRC networks</a> (unfortunately - please switch to UTF-8!). <a href="http://doc.qt.io/qt-5/qtextcodec.html">QTextCodec</a> gained support for CP949 in the Qt 4.x series, but our <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/kcodecs/html/classKCharsets.html">KCharsets</a> was never sync'ed up to those enhancements, hiding the codec from selection UIs throughout KDE's products. The effective impact of this is reduced somewhat by Qt 5's support for <a href="http://site.icu-project.org/">ICU</a>, which is smart enough to handle CP949 transparently in EUC-KR mode, but the situation was still confusing for users nonetheless (and left broken when running non-ICU builds of Qt).

This last one is interesting because patches to address this were actually supplied by the community in 2010 already, but they sat around unloved until recently despite not being very complicated - developers are often reluctant to engage in tickets like this because they feel out of their depth, or simply struggle with the setup necessary to reproduce a problem. I worry this may cause a bad feedback cycle of bugs not being reported by users who don't have the time or energy to educate developers about the problem space.

If you're a user of Korean (or other CJK languages) and KDE, please do report them. I'll be keeping an eye out. If you're a developer and struggling with Korean or CJK support in your application, you should consider getting in touch, too.