---
title:   "Fear not, OMG! Ubuntu! You will bounce again!"
date:    2017-03-10
authors:
  - eike hein
slug:    fear-not-omg-ubuntu-you-will-bounce-again
---
<b>Serving the quadruped audience</b>

Intrepid journalist Joey Sneddon over at <a href="http://www.omgubuntu.co.uk/">OMG! Ubuntu!</a> recently <a href="http://www.omgubuntu.co.uk/2017/03/kde-bouncing-ball-plasma-widget">pointed out to us</a> that Plasma 5 is currently not doing so well when it comes to serving an important user demographic - bored cats!

Indeed, Plasma 5.0 cost them (and us) the Bouncy Ball widget. And the reasoning mentioned in the article (<i>[...] when trying to develop a professional experience toys and gimicks aren’t a good thing to be shipping by default [...]</i>) is actually pretty solid I think. Hmm.

<b>Have we lost our bounce forever? No!</b>

But! These days we have the sexy <a href="https://store.kde.org/">KDE Store</a> going on, which is a great place to put toys and gimmicks (along with <a href="https://blogs.kde.org/2017/01/30/simple-menu-launcher-kde-store">neat menus</a>).

So it's back! Behold the demo:

<center><img src="http://i.imgur.com/saTL7Xk.gif" alt="Bouncy Ball v2.0 on Plasma 5" /></a><br /><small><i>Bouncy Ball v2.0 on Plasma 5</i></small><br /><br /></center>

You can grab it now for your Plasma 5 via <i>Add Widgets...</i> in your desktop menu  and then <i>Get new widgets</i> in the Widget Explorer, or check out the <a href="https://store.kde.org/p/1172489/">Bouncy Ball store page</a>.

Now for some additional fine print, though: I wrote this at ludicrous speed over a Friday night, and it's not well-tested. It behaves a little quirky sometimes (the goal was to match the original closely, but I didn't have a running KDE 4 to refer to). And despite the v2.0 moniker, it's still missing some of the features of the old Ball, including auto-bounce and that satisfying <i>Boing!</i> sound on collisions. I went with v2.0 in honor of the heritage - I'll polish it and add back the missing features a little later!

<b>Update</b> Bouncy Ball v2.1 is now on the store with sound support, auto-bounce, much better mouse response, a configurable simulation tick and a few bugfixes!