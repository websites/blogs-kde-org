---
title:   "New Plasma Task Manager backend: Faster, better, Wayland"
date:    2016-05-31
authors:
  - eike hein
slug:    new-plasma-task-manager-backend-faster-better-wayland
---
During the last several months, I've been rewriting the backend for Plasma's Task Manager, the strip of launchers and windows embedded into the panel at the bottom of the screen.

<center><img src="https://www.eikehein.com/kde/blog/libtmng/tm.png" alt="Plasma Task Manager widget" /><br /><small><i>My current</i> Task</small><br /><br /></center>

<b>What's in a Backend</b>

The backend's responsibility is to gather (or store) pinned launchers, startup notifications and window information, and combine all that raw data into the abstraction known as <i>tasks</i>, forming a rough lifecycle: Launchers transition into startup notifications and then window buttons, though none of these states is technically required or guaranteed, resulting in the need to be able to match up each type of thing with any other based on what is known about them. Along the way the data is also filtered (e.g. by activity or virtual desktop), sorted (say, alphabetically, or at the user's behest, when tasks get dragged around) and optionally grouped.

The code currently performing these duties has been with us a long time—it goes back to, incredibly, KDE versions released some 16 years ago. Rewriting it finally became necessary as part of our ongoing efforts to add support for the Wayland windowing system to Plasma, as the old code was heavily tied to X11 concepts and window metadata. However it also had some fundamental limitations and problems that badly needed addressing, and was written to engineering standards we've since surpassed in the community (e.g. it wasn't unit-testable, while the new backend is written to be).

<b>The Goods</b>

As such the new backend certainly enables the Task Manager to support Wayland, but also immediately translates to various improvements for users on X11, as well as the developer audience:

<ul><li><b>Improved performance.</b> On virtual desktop or activity switches, or certain settings changes, the old backend would trigger a full reset of the Task Manager UI, requiring costly reinstanciation and relayout of all the buttons. The new backend is smart enough to only make the changes necessary each time, saving a lot of cycles.<br /><br /><br /></li>

<li><b>Improved behavior</b>, particularly when grouping tasks. The old backend could act unreliably and unpredictably at times, failing to properly reform groups after virtual desktop switches, or losing sort order for tasks in a group when the group was broken apart. A much cleaner internal implementation of grouping should do away with these sort of annoyances. Further, a key task of the backend is to determine that two different types of things (say, a launcher and a window) belong to the same application, based on their metadata—these types of equivalence checks were sometimes done in subtly different ways across different parts of the old backend, such as sorting and grouping. The new backend consistently uses the same data and business logic in all cases, making behavior more predictable. Additionally, changes made in the system Menu Editor (e.g. selecting a different icon for an application) now appear immediately in launcher and window buttons, instead of taking their time or even requiring a restart.<br /><br /><br /></li>

<li><b>Improved memory usage.</b> The old backend would keep data for windows common to virtual desktops or activities redundantly, as well as store some icon data not actually used anymore by modern codepaths. The new backend keeps less data, and keeps it only once.<br /><br /><br /></li>

<li><b>Cleaner and more maintainable code.</b> After 16 years of development and maintenance, the old backend had turned into a big hairy ball. Along with a lack of unit tests, this kept us from fixing some of the above issues for fear of breaking more in the process. The new codebase benefits from being written to the list of requirements gathered over all those years. It knows much better how to separate its concerns, and implements features such as sorting and grouping agnostic to the underlying windowing system. The new code being much easier to work with will also enable us to translate user feedback into Task Manager changes much more readily again.<br /><br /><br /></li>

<li><b>Improved Wayland support.</b> While more work remains to be done to get task management to feature parity with X11, this is now largely a matter of supplying the new backend with the data it needs, feeding into upper layers ready to consume it and fully shared between both supported windowing systems. The much improved Wayland support will also enable the task management backend to be shared between Plasma Desktop and Plasma Mobile for the first time.</li></ul>

Now nearing completion, this new backend will undergo heavy testing and polish over the next several weeks. We hope to ship it with the upcoming Plasma 5.7 release.