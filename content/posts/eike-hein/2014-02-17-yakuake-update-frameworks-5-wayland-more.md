---
title:   "A Yakuake update: Frameworks 5, Wayland, More"
date:    2014-02-17
authors:
  - eike hein
slug:    yakuake-update-frameworks-5-wayland-more
---
Things have been rather quiet in Yakuake land for a while. 2014 is going to shake things up, though, so it's time for a brief look at what's been going on and where things are headed next.

<b><br />Frameworks 5</b>

Not long ago I pushed an initial port of Yakuake to <a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">Frameworks 5</a>, the next generation of KDE's platform libraries, to Yakuake's git repository. The new code lives on a branch called <tt>frameworks</tt> for now.

Yakuake on Frameworks 5 is functional and free of the use of any deprecated APIs. The port is mostly complete, too, missing just a few more touches to the first run dialog and skin handling. Here's a screenshot:

<center><img src="http://www.eikehein.com/kde/blog/yakuakekf5/yakuake.png" alt="Yakuake on Frameworks 5" /><br /><small><i>Ah yup: Looks no different.</i></small><br /><br /></center>

<b><br />Wayland</b>

One of the broader initatives the community is engaged in this year is enabling our workspaces and applications to use <a href="http://en.wikipedia.org/wiki/Wayland_%28display_server_protocol%29">Wayland</a>. Yakuake may just have a small role to play in that.

Historically, Yakuake's relationship with window managers has been plenty interesting. It's managed to crash several of the popular ones at one point or another; it's an unusual application with unusual behavior that exercises window manager code differently from more typical apps. More recently, it was perhaps the first non-workspace process to communicate and collaborate tightly with the compositor, asking <a href="http://en.wikipedia.org/wiki/KWin">KWin</a> to perform the slide-in/out animations on its behalf if available.

The latter is a model for things to come. In Wayland, application windows know intentionally little about the environment they exist in, and instead have to petition and rely on the window manager for things like positioning with no recourse. Yakuake on X11 does this legwork by itself; on Wayland, the comm protocol to the window manager will have to be rich enough to allow for equivalent results. 

Having Yakuake ported and ready will allow for it to serve as a useful testcase there.

<b><br />General feature plans</b>

Yakuake's theming system has been showing its age for a while, so I'm looking to implement a replacement for it. The new system will be based on QML, taking some inspiration from KWin's Aurorae decoration engine. The result should allow themes to support anti-aliased edges and shadows, along with much more flexible control over the placement of Yakuake's UI elements. Existing themes will continue to be supported however (by way of new-format theme that knows how to load and display the old ones -- the config UI will list both types transparently in the same list, though).

The other major feature that's been a long time coming is proper support for session restore. This has mostly been held back by missing functionality in the API of the Konsole component that provides Yakuake with terminal emulation. Unfortunately that situation remains unchanged for now, but I'm still hoping to eventually get around to some Konsole hacking to satisfy Yakuake's needs there.

<b><br />Schedule thoughts</b>

Frameworks 5 uptake in distributions has been very promising so far, with several distros (I'm aware of Fedora and Kubuntu, but there are no doubt others) packaging the preview releases soon after they were announced. It's still pre-release software, though, and APIs might still change until the stable release this summer. Until it's out, the repo's master branch will therefore continue to contain the KDE 4 codebase, and there will be another maintenance release cut from it sometime soon.

Development of the new theming system will be targeted at Qt 5 and Frameworks 5, however, due to significant API changes in the new Qt Quick and QML generation. As with the next KDE 4-based release there's currently no firm date for this - Aaron makes a <a href="http://aseigo.blogspot.de/2014/01/frameworks-5-timing-is-everything.html">good case</a> for not rushing things - except to say it will be some time this year.