---
title:   "Plasma 5.8: Per-screen Pagers"
date:    2016-09-11
authors:
  - eike hein
slug:    plasma-58-screen-pagers
---
The other day I wrote about the <a href="https://blogs.kde.org/2016/09/06/plasma-58-more-efficient-pager-and-activity-pager-widgets">Pager improvements awaiting in Plasma 5.8</a>. In the comments user <b>btin</b> re-raised the <a href="https://bugs.kde.org/show_bug.cgi?id=356693">issue</a> of limiting the Pager's display to the screen it's currently on, instead of being all-exclusive.

At the time I wasn't sure we could still sneak this in before feature freeze, but thanks to the screen-awareness of the new backend (which, to recap, is shared with the Task Manager and already needs to determine what screen a given window resides on), it turned out to be easy enough to do!

The default remains the screen-spanning behavior for now, but in the Pager's "Display" settings you can now tick a new "Only the current screen" checkbox. If enabled, Pagers in panels on different screens will now nicely be limited to showing their respective screen's windows on each desktop.