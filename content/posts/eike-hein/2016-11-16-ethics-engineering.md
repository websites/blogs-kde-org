---
title:   "Ethics in engineering"
date:    2016-11-16
authors:
  - eike hein
slug:    ethics-engineering
---
Powerful: <a href="https://medium.freecodecamp.com/the-code-im-still-ashamed-of-e4c021dff55e#.76pl74cy2"><i>The Code I'm Still Ashamed Of</i></a>

Things like this are a big reason why I work in open source.