---
title:   "Join the first KDE e.V. board dinner of 2019"
date:    2019-01-02
authors:
  - eike hein
slug:    join-first-kde-ev-board-dinner-2019
categories:
  - KDE e.V.
---
Twice a year (on that note, happy new one!), the KDE e.V. board of directors comes together for an in-person meeting, taking care of business. It's become a tradition that on one of the two meeting days, the board hosts a dinner event open to KDE users, contributors and other interested parties.

The board's first meeting of 2019 will be in <b>Seoul, South Korea</b>, and the dinner will be held on <b>Saturday, January 19th</b> in central Seoul.

If you're interested in attending (us five board members aside, we've already had the pleasure of confirming the attendance of many notable local community members - it won't be boring!) and talking all things KDE and FOSS over good food with us, please <a href="mailto:hein@kde.org">drop me a mail</a>. Modulo available space, I'll get back to you with details on the time and location as soon as we've finalized both.