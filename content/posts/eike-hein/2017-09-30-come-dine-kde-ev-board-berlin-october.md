---
title:   "Come dine with the KDE e.V. board in Berlin in October!"
date:    2017-09-30
authors:
  - eike hein
slug:    come-dine-kde-ev-board-berlin-october
---
As has become tradition in recent years, the KDE e.V. board will have an open dinner alongside its in-person meeting in Berlin, Germany on October 14th, at 7 PM.

We know there will be a lot of cool people in town next month, thanks to a KDE Edu development sprint, <a href="http://qtworldsummit.com/">Qt World Summit</a>, the <a href="https://wiki.gnome.org/Hackfests/Foundation2017">GNOME Foundation hackfest</a> and probably other events, and you're all invited to drop by and have a chat with us and amongst yourselves - and enjoy good food.

We're still picking out a location currently, so if you're interested in attending, please <a href="mailto:hein@kde.org">drop me a mail</a> to pre-register and I will (space permitting) confirm and send details soon.