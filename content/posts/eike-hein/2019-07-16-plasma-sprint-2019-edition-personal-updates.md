---
title:   "Plasma sprint, 2019 edition; personal updates"
date:    2019-07-16
authors:
  - eike hein
slug:    plasma-sprint-2019-edition-personal-updates
---
In June, I had a great time at a series of KDE events held in the offices of <a href="https://slimbook.es/">Slimbook</a>, makers of fantastic Neon-powered laptops, at the outskirts of Valencia, Spain. Following on from a two-day KDE e.V. board of directors meeting, the main event was the 2019 edition of the Plasma development sprint. The location proved to be quite ideal for everything. Slimbook graciously provided us with two lovely adjacent meeting rooms for Plasma and the co-located KDE <i>Usability & Productivity</i> sprint, allowing the groups to mix and seperate as our topics demanded - a well-conceived spatial analog for the tight relationship and overlap between the two.

<center><img width="500" src="https://i.imgur.com/aEBYrCr.jpg" alt="Alejandro López attaching a silver KDE sticker to my new laptop" /></a><br /><small><i>The Plasma team walked the gorgeous Jardí del Túria almost every day during their sprint week to stay healthy and happy devs.</i></small><br /><br /></center>

As always during a Plasma sprint, we used this opportunity to lock down a number of important development decisions. Release schedules, coordinating the next push on Plasma/Wayland and a new stab at improving the desktop configuration experience stand out to me, but as the <a href="https://dot.kde.org/2019/07/04/plasma-usability-productivity-sprint-valencia-spain">Dot post</a> does a fine job providing the general rundown, I'll focus on decisions made for the Task Manager widgets I maintain.

On one of the sprint mornings, I lead a little group session to discuss some of the outstanding high-level problems with the two widgets (the regular <i>Task Manager</i> and the <i>Icons-only Task Manager</i>), driven by frequent user reports:

<ul>
<li>Poor experience performing window management on groups of windows</li>
<li>Unnecessary duplication in the UI displaying window group contents</li>
<li>Unintuitive behavior differences between the two widgets</li>
</ul>

To address these, we came up with a list of action items to iteratively improve the situation. Individually they're quite minor, but there are many of them, and they will add up to smooth out the user experience considerably. In particular, we'll combine the currently two UIs showing window group contents (the tooltip and the popup dialog) into just one, and we'll make a new code path to cycle through windows in a group in most recently used order on left click the new default. The <a href="https://notes.kde.org/p/usability-productivity-sprint-2019">sprint notes</a> have more details.

Decision-making aside, a personal highlight for me was a live demo of Marco Martin's new desktop widget management implementation. Not only does it look like a joy to use, it also improves the software architecture of Plasma's home screen management in a way that will help Plasma Mobile and other use cases equally. Check out <a href="https://notmart.org/blog/2019/07/a-week-in-valencia/">his blog post</a> for more.

<center><img width="500" src="https://i.imgur.com/ruAybrU.jpg" alt="Alejandro López attaching a silver KDE sticker to my new laptop" /></a><br /><small><i>I got a new laptop. Slimbook founder Alejandro López made it a proper computer by attaching a particularly swanky metal KDE sticker during the preceding KDE e.V. board sprint.</i></small><br /><br /></center>

In KDE e.V. news, briefly we stole one of the sprint rooms for a convenient gathering of most of our <a href="https://ev.kde.org/workinggroups/fwg.php"><i>Financial Working Group</i></a>, reviewing the implementation of the annual budget plan of the organization. We also had a chance to work with the <i>Usability</i> <a href="https://phabricator.kde.org/T6831">goal crew</a> (have you heard about <a href="http://blog.lydiapintscher.de/2019/06/09/evolving-kde-lets-set-some-new-goals-for-kde/">KDE goals</a> yet?) on a plan for the use of their remaining budget -- it's going to be exciting. 

As a closing note, it was fantastic to see many new faces at this year's sprint. It's hard to believe for how many attendees it was their first KDE sprint <i>ever</i>, as it couldn't have been more comfortable to have them on board. It's great to see our team grow.

See you next sprint. :)

<hr />

In more personal news, after just over seven years at the company I'm leaving Blue Systems GmbH at the end of July. It's been a truly fantastic time working every day with some of the finest human beings and hackers. The team there will go on to do great things for KDE and personal computing as a whole, and I'm glad we will keep contributing together to Plasma and other projects we share interests and individual responsibilities in.

As a result, the next ~10 weeks will see me very busy moving continents from Seoul back to my original home town of Berlin, where I'll be starting on a new adventure in October. More on that later (it's quite exciting), but my work on the KDE e.V. board of directors or general presence in the KDE community won't be affected.

That said -- between the physical and career moves, board work and personal preparations for Akademy, I'll probably need to be somewhat less involved and harder to reach in the various project trenches during this quarter. Sorry for that, and do poke hard if you need me to pick up something I've missed.

And of course:
<a href="https://akademy.kde.org/2019">
  <img src="https://cdn.kde.org/akademy/2019/imgoing/Akademy2019BannerDuomo-wee.png" width="420" height="103" alt="I'm going to Akademy 2019" />
</a>