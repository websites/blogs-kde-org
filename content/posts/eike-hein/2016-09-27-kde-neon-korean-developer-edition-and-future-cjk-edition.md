---
title:   "KDE neon Korean Developer Edition (... and future CJK Edition?)"
date:    2016-09-27
authors:
  - eike hein
slug:    kde-neon-korean-developer-edition-and-future-cjk-edition
---
While not being advertised on the <a href="https://neon.kde.org">KDE neon</a> main page just yet (and it won't be for a while), we've recently begun doing regular builds of a special <i>Korean Edition</i> of neon's Developer Edition tracking the stable branch of KDE's code repositories. The Korean Edition pre-selects the Korean language and locale at boot, packs all the Korean translations we have and comes with a Korean input method pre-setup.

<center><img src="https://www.eikehein.com/kde/blog/neonkoedition/hangeulmetaltype.jpg" alt="Hangeul metal type from the Joseon era" /><br /><small><i><a href="https://en.wikipedia.org/wiki/Joseon">Joseon</a>-era <a href="https://en.wikipedia.org/wiki/Hangul">Hangeul</a> metal type</i></small><br /><br /></center>

<b>Why a Korean Edition?</b>

Among many other locations around the planet, the local community in Korea is <a href="https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary#Seoul">planning</a> to put on a KDE 20th Anniversary birthday party in Seoul on October 14th. The KDE neon Korean Developer Edition was directly created on request for this event, to be made available to attendees.

That said - this is actually something we've been wanting to do for a while, and it's not just about Korean.

None of the bits that make up the new image are new per-se; KDE has supported Korean for a long time, both with foundational <a href="https://techbase.kde.org/Localization/Concepts/Transcript#Korean_Postpositions">localization engineering</a> and regular <a href="https://blogs.kde.org/2015/01/17/improving-kdes-support-korean-and-other-cjk-languages">maintenance activity</a>. And as of the Plasma 5.6 release, our Input Method Panel is finally bundled with the core desktop code and gets automatically added to the panel on first logon in a locale that typically requires an input method.

Yet it's pretty hard to keep all of this working well, as it requires tight integration and testing across an entire stack, with some parts of the whole living upstream or downstream of KDE.org. For example: After we attempted to make the Plasma panel smarter by making it auto-add the Input Method Panel depending on locale, we couldn't actually be sure it was working as desired by our users, as it takes time for distros to get around to tuning their dependency profiles and for feedback from their users to loop back up to us. It's a very long cycle, with too many opportunities to lose focus or domain knowledge to turnover along the way.

This is where KDE neon comes in: As a fully-integrated product, we can now prove out and demo the intended distro experience there. We can make sure thing stay in working order, even before additional work hits our other distro partners.

Right now, we're kicking things off with Korean Edition, but based on time, interest and testers (please get in touch!), we'd like to build it out into a full CJK Edition, with translations and input support for our Chinese and Japanese users pre-installed as well (as another precursor to this, the decision to switch to Noto Sans CJK as Plasma's default typeface last year was very much made with the global audience in mind as well).

<b>Ok, but where do I get it?</a>

<a href="http://files.kde.org/neon/images/neon-ko-devedition-gitstable/">Here!</a> Do keep in mind it's alpha. ☺