---
title:   "Engineering Plasma: Extensions and stability — Present and future"
date:    2018-08-02
authors:
  - eike hein
slug:    engineering-plasma-extensions-and-stability-—-present-and-future
categories:
  - Plasma
---
This week, we have received a number of inquiries into how Plasma extensions, particularly those found on the <a href="https://store.kde.org/">KDE Store</a>, relate to the stability and safety of a Plasma system. With an engineering focus, this blog hopes to provide answers.

<b>Crash Resilience By Process Isolation</b>

<center><img width="500" src="https://i.imgur.com/1k0s5IMr.png" alt="Present-day Plasma process architecture diagram" /></a><br /><small><i>Present-day process architecture: Compositor and shell UI are isolated from each other</i></small><br /><br /></center>

In Plasma, the shell UI and the compositor are isolated into seperate processes (<code>plasmashell</code> and <code>kwin</code>, respectively). The two exchange information regulated by IPC mechanisms such as the windowing system (Wayland or X11) and DBus, using standard protocols where suitable and custom-created ones where needed.

In a Wayland session, it's kwin that plays host to your application clients and puts them on screen (on X11, both KWin and app are clients to the X Server, providing a further isolation - KWin can crash and restart without impacting apps). Shell UI extension live in the seperate plasmashell process.

In the event that a shell extension crashes the shell, this allows it to be restarted without impacting KWin and therefore without impacting your applications. They continue to run undeterred while the shell takes steps to right itself.

Beyond crash resilience, putting limits on untrusted code run in the compositor process also has a security dimension. Particularly on Wayland, where one of the design goals is not to allow untrusted code to introspect the windowing system globally.

Meanwhile, to make KWin ready to taking over for the X Server in a Wayland session, we significantly upgraded our engineering story - requiring and dramatically raising unit test coverage for KWin and related library code, for one.

<b>Process Isolation: Next Steps</b>

The architecture discussed above shields applications and the compositor from shell UI extensions, but it doesn't shield the shell. We want to fix that next.

<center><img width="500" src="https://i.imgur.com/OgFZWjT.png" alt="Future R&D Plasma process architecture diagram" /></a><br /><small><i>Next: Isolate shell UI and extensions, too — individual or batch processes for extensions</i></small><br /><br /></center>

The always-stunning David Edmundson has been spearheading several engineering efforts to make the shell itself have a multi-process architecture. Some of this has already quietly been shipping in the <a href="https://www.kde.org/announcements/plasma-5.12.0.php">Plasma 5.12</a> LTS release: <code>KRunner</code> plugins, which provide search results in our menus/launchers, can now opt into running out-of-process. We've used this to isolate some of the crash chart-topping plugins, preventing them from taking down the shell when performing a search query.

Likewise, for shell UI extensions, he has been working on top off the library stack we initially built for the Wayland transition to allow them to be run out of process and then get composited into the shell UI. In addition to making the shell far more resilient to extension crashes, this would also create additional security domains for untrusted extension code - we can build on this to sandbox and drop privileges for them.

All of this is broadly similar to application architecture improvements pioneered by multi-process web browsers in past years, albeit built squarely to leverage the shared free desktop stack (particularly Wayland and DBus). Unlike what took place in Firefox' project <a href="https://wiki.mozilla.org/Electrolysis">Electrolysis</a>, however, we don't see a need to break extension compatibility in our case. For Plasma's extension API scope, we've always taken a conservative demand-based approach instead of allowing extensions free access to all data and code by default. This is paying dividends now, allowing us new shell architecture options while preserving the known, stable API contract it has with its extensions.

David is set to show off the R&D proof of concept we have working now in <a href="https://conf.kde.org/en/Akademy2018/public/events/66">his talk</a> at this year's <a href="https://akademy.kde.org/">Akademy</a> conference, which I can't wait to attend. Be sure to also keep your eyes peeled on his <a href="https://blog.davidedmundson.co.uk/">blog</a>, where he will dive deeper into the details of this project after the conference excitement.

<b>In Summary</b>

Plasma today is architected to prevent shell UI extensions from being able to crash your session and interfere with your apps. We are working to expand on this and prevent extensions from interfering with the shell.

<b>In Context</b>

One of the broad goals of Plasma 5 has been raising <b>quality</b>. This is a user-driven process. Converting feedback we got during the first generation of Plasma into software, we worked hard to bring Plasma users tangible improvements to speed, resource usage and UI polish (this is by no means over, either). We also already implemented a LTS release process to improve our support story.

Dove-tailing with the <a href="https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond">KDE community's Goals</a>, climbing higher on the ladder of safety and security is one of our immediate next ones. Increasing process isolation in Plasma and achieving state of the art crash resilience and sandboxing, a likely first on the desktop, is a concrete engineering expression of this aim.

Sounds interesting? If you want to be part of a team and a community that keeps pushing the desktop (and <a href="https://www.plasma-mobile.org">not just the desktop</a>) forward in the days to come, <a href="https://community.kde.org/Get_Involved">join us</a> in one of our <a a href="https://userbase.kde.org/IRC_Channels">channels</a>.

Of course:

<center><img src="https://akademy.kde.org/sites/akademy.kde.org/files/2018/going_to_akademy_banner.jpg" alt="I'm going to Akademy promo picture" /></a></center>