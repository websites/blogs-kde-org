---
title:   "People who rock"
date:    2008-04-21
authors:
  - sad eagle
slug:    people-who-rock
---
Jakub Stachowski, who just committed a <a href="http://lists.kde.org/?l=kde-commits&m=120878655602469&w=2">significant performance improvement to KConfig parsing</a>, which, with previous improvements of his, made it about 4.5 times faster than the initial KDE4 version, and slightly faster than the KDE 3.5.9 one.
