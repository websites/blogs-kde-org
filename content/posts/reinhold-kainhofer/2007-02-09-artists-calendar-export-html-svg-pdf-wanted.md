---
title:   "Artists for calendar export to HTML, SVG, PDF wanted!"
date:    2007-02-09
authors:
  - reinhold kainhofer
slug:    artists-calendar-export-html-svg-pdf-wanted
---
As I wrote in my last blog, KOrganizer now has the ability to export the calendar to all different kinds of formats using technology called XSLT transformations. The only thing that I'm missing (because I'm entirely bad at those things) are good designs that I can implement. So I'm looking for nice and visually appealing designs of how calendar exports might look. Possible export formats are e.g.
<ul>
  <li> SVG graphics
  <li> HTML (to-do list, calendar, journal, showing just one event)
  <li> PDF output
</ul>
Just be creative! In principle, one can create anything from the calendar data, from A0 wall poster calendars to A8 pocket sheets, from SVG graphics showing the calendar with a nice holiday picture in the background to professional-looking calendar books.
Of course, with SVG and HTML it would be much easier for me to implement if your design is already in SVG or HTML.

BTW, if anyone wants to try it out, the KOrganizer plugin is currently in the kdepim-3.5.5+ branch for new features to 3.5 (branches/work/kdepim-3.5.5+/korganizer/plugins/xslt/ in SVN). To work with the 3.5 branch, just copy that directory over and it should work. You'll have to enable the plugins in korganizer's config dialog before you can see them in the "File -> Export" menu, though. There are some proof-of-concept style sheets available already (for HTML, SVG, CSV, etc.), but they are mostly dummy styles and do not really produce any useful output (and if they do, the output is plain ugly, so I won't give an example here).
<!--break-->