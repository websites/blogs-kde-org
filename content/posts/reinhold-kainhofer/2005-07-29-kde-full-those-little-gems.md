---
title:   "KDE is full of those little gems"
date:    2005-07-29
authors:
  - reinhold kainhofer
slug:    kde-full-those-little-gems
---
KDE is simply amazing. There you are, the current developer and maintainer of your application, and you think you know your application inside out and you are aware of all the small features it provides to the user...
And then it happens: You work with your application and think "Oh, right, that's one feature I will have to add", and the next moment you realize that your application already has it. You just haven't noticed so far.

This just happened to me. As the maintainer of KOrganizer I thought I would know that application really well. Now I keep large to-do lists of all my tasks. I sort them hierarchically and use categories to color them differently in the view. So today I created a sub-to-do, and was prepared to change the category of that item, but automagically it had already taken the category of the parent...

It might look unimportant, but those little issues are what makes working with KDE such a charm. So many thinks already work right the way you would expect them to be. And you find new things every day, even after you have been using KDE for several years, or have been working on the application yourself.