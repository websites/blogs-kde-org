---
title:   "Waiting for the end of the feature freeze!"
date:    2004-07-28
authors:
  - reinhold kainhofer
slug:    waiting-end-feature-freeze
---
Boy, I can't wait till the feature freeze is finally over. There are so many useful things that korganizer needs, and I even took the time to implement some of them properly. Only problem is the feature freeze, which doesn't allow me to commit these new features :-( 

But when the freeze is over, the next day korganizer will support archiving of (finished) todo items (patch attached to wish 34079), deleting only future events of a recurring sequence (wish 50712), as well as dissociating an event or all future events from a recurring sequence (wish 54959). The patch for the last two features is already finished on my harddisk, but bugs.kde.org seems to have problems, so i can't attach it to these bug reports.

Today I also took on kaddressbook and fixed some issues with the embedded editor of kaddressbook, which couldn't be used at all since the text edit lines would loose focus after each keystroke. It seems I'm the only person on this planet who really uses this killer feature of kaddressbook. The rest of the world can't image what they are missing ;-)