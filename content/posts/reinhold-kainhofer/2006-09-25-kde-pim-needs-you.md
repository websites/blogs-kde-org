---
title:   "KDE-PIM needs YOU!"
date:    2006-09-25
authors:
  - reinhold kainhofer
slug:    kde-pim-needs-you
---
Once upon a time, there were all those nice, separate applications that were somehow meant for various PIM tasks like mail, calendar, addressbook, etc. To make the world an even better place (and to share resources), they decided to unite, join forces and create this wonderful application, called Kontact. Just like one large family, they worked together and due to the large number of active developers the project flourished.

But then, reality set in: Most of the developers got a real job, and suddenly they painfully found their time for KDE dramatically cut or even completely reduced to no time at all. 

Unfortunately, that's our current situation in KDE-PIM. There is hardly anyone left (Till is working on his thesis, Ingo recently started a new job, Cornelius is in the e.V. board, Ade has the EBN and is on the e.V. board, I have a job plus some other commitments, etc.), only Volker and Tobias working on Akonadi, and of course, Allen Winter fixing bugs all over the place.

So, to put it short: <b>We really need some help!</b> As much help as we can get, in fact. 

If you are at aKademy, there's the easy route to get involved: Just join our BoF on Tuesday, 17:00! Otherwise, just join us on #kontact on IRC or subscribe and send a mail to kde-pim@kde.org.<!--break-->