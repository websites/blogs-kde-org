---
title:   "KOrganizer just got XSLT support"
date:    2007-02-07
authors:
  - reinhold kainhofer
slug:    korganizer-just-got-xslt-support
---
XSLT is a W3 specification that allows general transformations from XML into practically any other format (mainly XML, but you can also create any text).
In the kdepim 3.5.5+ feature branch I just added a plugin to korganizer, that exports the calendar into XML and then applies an XSLT transformation to it to generate all different kinds of output... For example, one can write an XSLT style sheet for some fancy HTML export, or for CSV export, or to XSL-FO to generate nice PDFs. One might even generate SVGs from the calendar.

And the best of all: All these export styles are not hard-coded in C++, but they are pure xml files that can be edited by anyone (okay, not really anyone, you need to know XSLT). Due to KDE's nice standard dirs approach, there are some system-wide style sheets, but each user can also add her/his own style sheets in one's home directory.

Unfortunatly, I'm not much of a graphics or web designer, so the output of the transforms is currently really ugly (and not completely implemented in XSLT, either). But at least the technology is there, now only the style sheets have to be implemented and polished.<!--break-->