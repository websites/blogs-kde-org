---
title:   "My Work on Documentation (June/July)"
date:    2021-07-30
authors:
  - schwarzer
slug:    my-work-documentation-junejuly
categories:
 - Documentation
---
After two month in documentation I can tell you this: documentation in general is quite alive and kicking. :) From the outside you might see outdated content here and there, but there are quite a few people working on improving that. Of course, as most things, it is a never-ending effort and every helping hand is appreciated. If you are interested in helping, please talk to us on <a href="https://mail.kde.org/mailman/listinfo/kde-doc-english">our mailing list</a>. One of the more time-consuming tasks is currently porting documentation from <a href="https://techbase.kde.org/Welcome_to_KDE_TechBase">TechBase</a> to the new <a href="https://develop.kde.org/">Developer Portal</a>. It's basically copy&paste with some adjustments, so volunteers welcome :)

For me the time flew by blazingly fast. In the beginning ... there was Akademy, the first conference for me. It meant getting up at 7 to go to work, joining Akademy when I came back home and staying up till midnight or longer for the last talks or events to finish. Processing and carving out the outcome of the three documentation BoF sessions is still on my todo list.

In the time before and since Akademy, I have been busy reading through several years of documentation improvement planning backlog and fixing lots of smaller issues in existing docs in the wikis and on the new Developer Portal. This also included cleaning up some outdated content (EBN has been decommissioned) and proofreading other people's documentation-related merge requests when asked to do so.

Unfortunately, <a href="https://invent.kde.org/websites/volunteers-akademy-kde-org/-/merge_requests/1">my first contribution to one of the website's Git repositories</a> is still unmerged at the time of writing. I blame me for that not keeping track of my own merge requests. But it also shows one of the things we need to put more effort into: closing merge requests (one way or another). Also, check your own merge requests once in a while. You can do that by using the following URL and putting you account name at the end:
https://invent.kde.org/dashboard/merge_requests?scope=all&state=opened&author_username=<b>your_user_name</b>

For some projects (e.g. KApiDox there had been several merge requests open for a year or longer. One was a data loss issue when generating api documentation from the parent folder (..). <a href="https://invent.kde.org/frameworks/kapidox/-/merge_requests/17">Adriaan was so kind to fix that</a> even if only because he wanted me to stop nagging. ;) These open merge requests and some open issues are now merged with only one merge request remaining which will probably be replaced by a new one from last week: <a href="https://invent.kde.org/frameworks/kapidox/-/merge_requests/20">preparing KApiDox to be run in a docker container</a>. That one I am working on with a friend with stronger Python and Docker foo.

Another issue with KApiDox was that it could not be run locally anymore due to restrictions on fetching KDE accounts information from SVN. <a href="https://invent.kde.org/frameworks/kapidox/-/merge_requests/19">I changed that accounts parsing behaviour</a> to be an optional command line switch <a href="https://invent.kde.org/websites/quality-kde-org/-/merge_requests/2">which is run on api.kde.org</a> but not locally. You are still free to fetch the KDE accounts file manually and point KApiDox to it.

But while tooling was my main scope during that first time, it's not what I actually wanted to do. It just jumped in my way. :) So I also collected opinions, topics and ideas for the actual documentation. These days I am starting to write porting notes meant mainly for third-party developers who will start porting their KF5 applications to KF6 soon. More on that one later.

To conclude these first two months, I can say, wherever you look, you will find issues to fix, things to improve, content to update ... And many of these things are not hard to do. So if you ever wondered how you could start contributing to KDE, documentation is great place to start. :)