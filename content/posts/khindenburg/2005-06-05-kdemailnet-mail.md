---
title:   "kdemail.net mail"
date:    2005-06-05
authors:
  - khindenburg
slug:    kdemailnet-mail
---
So a while ago, I stopped getting copies of my mails to kde-devel and kde-core-devel.  Also I no longer get emails from bugs.kde.org when I edit a bug.  I have the option check on both bugs and the ml to send copies of my mails.  

Rather annoying as it took me a while to figure out why I didn't see my emails to the the -devel ml.  Not getting the emails from bugs is worse as it makes sorting out the bugs for Konsole harder....

I also see "We have received some recent bounces from your address. Your current bounce score is 2.0 out of a maximum of 5.0." for the mailing-lists.  I wonder if kdemail.net is having problems...

I had to change my address for the svn-commits in order to get those emails... perhaps I'll have to drop my kdemail.net email.  :-(


<!--break-->