---
title:   "Konsole's vttest results"
date:    2012-01-06
authors:
  - khindenburg
slug:    konsoles-vttest-results
---
I finally got around to running vttest [1] in Konsole (4.8/master) [2].

The most noticeable issue is the the console resizing commands don't work [3].
<!--break-->
A list of other issues:
<ul>
<li>49 - The line bold doesn't look correct (OSX); the line underscored has no underline
<li>54 - Set 0 (SI/SO) bottom line differs (OSX/Linux have different graphics).
<li>56 - An extra "This is another line" normal size is displayed
<li>58 - An extra "This is another line" normal size is displayed
<li>60 - An extra "This is another line" normal size is displayed
<li>62 - An extra "This is another line" normal size is displayed
<li>63 - The outer box frame doesn't connect all the way around
<li>64 - The outer box frame doesn't connect all the way around
<li>76 - Control keys didn't work: @ ^ _ (OSX)
<li>87 - Graphics character set - bottom line differs
<li>93 - The "by one." line has "Push <RETURN>" being overwritten
<li>99 - The "by one." line has "Push <RETURN>" being overwritten
</ul>

None of these are terrible although the resizing issue really needs to get fixed!

[1] http://invisible-island.net/vttest
[2] http://www.kermitproject.org/vttest.html
[3] https://bugs.kde.org/show_bug.cgi?id=238073