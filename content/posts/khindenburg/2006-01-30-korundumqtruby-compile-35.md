---
title:   "korundum/qtruby compile 3.5?"
date:    2006-01-30
authors:
  - khindenburg
slug:    korundumqtruby-compile-35
---
The 3.5 branch of kdebinding doesn't compile and hasn't for a while I believe.  It seems like the development of korundum/qtruby has moved to http://rubyforge.org/projects/korundum/.

I wonder why?  

I copied some of the files from the latest release of korundum to kdebindings and it seems to compile (further) then it did.

smoke/qt just got done... smoke/kde is compiling...

<code>
M  korundum/rubylib/rbkconfig_compiler/tests/Makefile.am
M      kalyptus/ChangeLog
M      kalyptus/kalyptusCxxToKimono.pm
M      kalyptus/kalyptus
M      kalyptus/kalyptusCxxToJNI.pm
M      smoke/qt/header_list
M      smoke/qt/Makefile.am
M      smoke/qt/generate.pl.in
M      smoke/qt/qtguess.pl.in
</code>