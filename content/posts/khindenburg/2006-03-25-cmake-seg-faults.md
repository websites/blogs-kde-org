---
title:   "cmake && seg faults"
date:    2006-03-25
authors:
  - khindenburg
slug:    cmake-seg-faults
---
Well, kdelibs4_snapshot compiles and installs with cmake.  Almost, anyway... I had to remove kstyles/keramik since the genembed seg faults...

... and after installing kdelibs4_ss...

% /opt/KDEDIRS/trunk/bin/kde-config --path
Segmentation fault

Urgh, I would rather have something NOT compile than have it seg fault...

Oh well, cmake is still in infancy... I'll be glad when all the auto* crap is deleted from KDE!
<!--break-->