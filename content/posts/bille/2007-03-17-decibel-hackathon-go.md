---
title:   "Decibel Hackathon is go!"
date:    2007-03-17
authors:
  - bille
slug:    decibel-hackathon-go
---
About an hour ago the Decibel hackathon in Darmstadt started.  Hosted by Basyskom, this weekend brings together domain experts in instant messaging, VoIP, PIM data and contact management to create the real time communication infrastructure for KDE 4.  At the moment we are all bringing each other up to speed on the state of each others' technologies.  We're all very excited, and it's not just the coffee - stay tuned for updates.
<!--break-->