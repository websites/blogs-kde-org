---
title:   "Network Management in KDE 4.1"
date:    2008-05-14
authors:
  - bille
slug:    network-management-kde-41
---
<p>Today I took the plunge and merged the Solid network management infrastructure into KDE SVN trunk, where it will soon be released as part of KDE 4.1.  Here's a summary of what it includes.  Since what follows is Long, Save Planet[KDE|OpenSUSE] and read more for details.</p>
<!--break-->

Network Management in KDE 4.1 consists of the following components:
<dl>
<dt>An abstraction of network status and networking system configuration methods</dt><dd><p> (kdebase/workspace/libs/solid/control).  With this apps can find out which network devices are present in the system (similar to HAL, but with more run-time information present), what their status is, are they connected, their IP details, hardware specifications.  You can also control the system as a whole - go offline or into flight mode.</p>
<p>This is modelled closely on the NetworkManager apis but is abstracted from them so that different versions of NetworkManager can be used by the same tools on various distros, or other network instrastructure can be used on other platforms.  The API is a lot more flexible and better structured than the previous version found in KDE 4.0, and <a href="http://ervin.ipsquad.net/">Kevin Ottens</a> has been sharing his API design wisdom during the process.</p>
<p>Network configuration, for example: setting up wireless network connections, is deliberately excluded from the abstracted Solid API.  It's possible to activate or deactivate a connection using Solid, but connections are treated as opaque identifiers only.  The reason for this is that realistically, configuration is best done by one app on the desktop, so developing a whole chunk of API for it to use is not worth it.  The configuration app can set up connections, then expose their identifiers to Solid apps to use.</p></dd>
<dt>NetworkManager backends</dt><dd><p> (kdebase/workspace/solid/networkmanager-*).  I've been working on a NetworkManager 0.7 backend, accessed indirectly by the above API, as this is the default in the upcoming openSUSE 11.0.  A couple of hours after I merged, <a href="https://blogs.kde.org/blog/2661">Pino Toscano</a>, all-round champion developer, jumped in and started porting the NetworkManager 0.6 backend from KDE 4.0 to work with the new infrastructure.</p>
</dd>
<dt>Frontend apps</dt><dd><p><a href="http://chris.theblauvelts.com/blog/">Chris Blauvelt</a> has been patiently (and occasionally kicking my arse) waiting for me to merge but hasn't been idle.  He's developing a plasmoid to work as a control applet, replacing the KNetworkManager applet from KDE 3.  We had a workshop on this at the KDE 4 release event, where we consulted Celeste to design an interface that meets the two goals (or maybe the Scylla and Charybdis) of UI design, power and intuitiveness.</p>
<p>The plasmoid will be backed up with a System Settings module where you can create and edit network connections.  Since I haven't started this yet, it won't be in KDE 4.1 main modules but I hope to have it available and useful somewhere else when 4.1 comes out.  I've been hacking on KNetworkManager 3 together with Helmut Schaa (a colleague at Novell) to get it ready for openSUSE 11.0 recently and gathering some ideas how to structure the <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kdeui/html/classKCModule.html">KCM</a>.</p>
</dd>
<dt>A network status service</dt><dd><p>  Since Solid::Control isn't tightly bound to NetworkManager, we don't know that there is always a daemon available for apps to query for network status so they can go to offline mode.  A <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kdecore/html/classKDEDModule.html">KDED module</a> provides this information reliably to KDE apps and as a lightweight plugin to KDED, doesn't add much overhead.</p></dd>
<dt>UI and application infrastructure</dt><dd> (kdelibs/kio/kio and kdelibs/solid) <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kio/html/classKStatusBarOfflineIndicator.html">KStatusBarOfflineIndicator</a> makes it stupidly easy to have a smart widget in your app's status bar to indicate when it's in offline mode, and a bunch of other API in Solid::Networking in kdelibs enables more powerful ways to find out if an app should behave differently when the internet goes away.  Apps can just use Solid::Networking's easy namespaced functions and never know about NetworkManager, DBus or any of that gunk.  This was patched into KDE 3 in openSUSE, and also used by Pardus, but couldn't be added as it was feature frozen.  Now it's there for everyone to use in KDE 4.  Go for it, app developers!</p></dd>
</li>
</dl>

<p>So, that's my brain dumped for now.  If you want to know more look for me on #solid (wstephenson).  Apologies to Danny for not sending him this for the Commit Digest, and to kde-core-devel for not using it for its intended purpose.</p>


