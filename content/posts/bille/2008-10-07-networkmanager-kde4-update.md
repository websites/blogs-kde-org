---
title:   "NetworkManager-kde4 update"
date:    2008-10-07
authors:
  - bille
slug:    networkmanager-kde4-update
---
Back in May <a href="http://blogs.kde.org/node/3462">I blogged about the network management architecture</a> in KDE 4.  It's October now and I can't believe we still don't have a native NetworkManager frontend.  But there's nothing like a <a href="http://en.opensuse.org/Roadmap/11.1">imminent openSUSE release<a> to focus my energies, and now we're a big step closer to having one.  

To remind you what makes up the NetworkManager frontend, we have three components: a Plasmoid that sits in the system tray and is the main control for activating and deactivating network interfaces; a dbus service that provides the configuration needed for them; and a System Settings module that provides the user interface for adding these settings.

These three parts are now present and (slightly) functional.  As of this morning it can connect to wired and to unencrypted wireless networks.  A fair bit of plumbing work is needed to finish it and enable encrypted/authenticated wireless, UMTS/GSM/CDMA cellular connections, DSL modems and VPNs, but the skeleton of the building is complete.

Today I had a useful meeting with the Plasma team to find out how to make the applet useful and pretty.  We had a lot of ideas, but as a picture says a thousand words and a mockup is half-way to completion, I'll close on some pictures.

PS: my father has an adage about never showing an unfinished sketch to fools or art teachers.  I trust none of you fall into those categories ;).

<p>The Present of NetworkManager-kde4
<a href="https://blogs.kde.org/files/images/NetworkManager-kde4_applet.png">
<img src="https://blogs.kde.org/files/images/NetworkManager-kde4_applet.thumbnail.png" alt="The Present of NetworkManager-kde4"/></a>
</p>
<p>System Settings module
<a href="https://blogs.kde.org/files/images/NetworkManager-kde4_kcmodule.png"><img src="https://blogs.kde.org/files/images/NetworkManager-kde4_kcmodule.thumbnail.png" alt="System Settings module"/></a>
</p>
<p>Wireless Connection editor
<a href="https://blogs.kde.org/files/images/NetworkManager-kde4_editor.png"><img src="https://blogs.kde.org/files/images/NetworkManager-kde4_editor.thumbnail.png" alt="Wireless Connection editor"/></a>
</p>
<p><em>Mockup</em> of where this is headed.  Image by Half-Left of #plasma, thank you!
<a href="https://blogs.kde.org/files/images/NetworkManager_plasma_mock1.preview.png"><img src="https://blogs.kde.org/files/images/NetworkManager_plasma_mock1.thumbnail.png" alt="Where this is headed"/></a></p>