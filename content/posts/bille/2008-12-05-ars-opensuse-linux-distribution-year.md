---
title:   "Ars: openSUSE a 'Linux Distribution of the Year'"
date:    2008-12-05
authors:
  - bille
slug:    ars-opensuse-linux-distribution-year
---
I'm a bit slow off the mark, but I was busy fixing a few last bugs in <a href="http://software.opensuse.org/developer">openSUSE 11.1</a> and <a href="http://blogs.kde.org/node/3783">entertaining krake</a>.  Today I noticed that Ars Technica have chosen <a href="http://arstechnica.com/articles/culture/ars-awards-2008.ars/7">openSUSE as one of the distributions of the year</a>, with a special mention for the quality of our KDE environment. Thanks to my colleagues and the determined #opensuse-kde team, and here's hoping we can make KDE on openSUSE in 2009 even better!

Now I had better go hack on NetworkManager-kde4 some more...
<!-- break -->