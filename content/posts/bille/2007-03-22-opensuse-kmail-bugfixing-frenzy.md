---
title:   "openSUSE KMail Bugfixing Frenzy"
date:    2007-03-22
authors:
  - bille
slug:    opensuse-kmail-bugfixing-frenzy
---
The geeko is hungry, so for the past few days <a href="https://blogs.kde.org/blog/124">Coolo</a> and I have been feeding it the carcasses of many of the more serious bugs in KMail.  Our focus has been on online IMAP, as that has had the most egregious bugs in our opinion, but we're also after low-hanging fruit anywhere else in KMail.  We're basically doing it because We Care A Lot, but we also want some tangible improvements in KDE 3.5 in <a href="http://en.opensuse.org/Development_Version">openSUSE 10.3</a>, besides all the KDE 4 work we're doing at the moment. The fixes are all going into 3.5 branch, of course, and are being ported to the enterprise branch and 3.5.5+, so rest-of-world benefits too. 

So if you are using 3.5 branch svn, please hammer on KMail, especially online IMAP, and report any crashes you see.  I'll try and post a list of the bugs we've scalped so far later, but for now I need to take a break and clear my head.<!--break-->