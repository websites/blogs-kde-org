---
title:   "KDE NetworkManagement Sprint Day Three and Wrapup"
date:    2009-06-08
authors:
  - bille
slug:    kde-networkmanagement-sprint-day-three-and-wrapup
---
On Sunday the work continued at a furious pace.  Dario carried on moving the connection list generating code out of the applet and into the KDED module.  This makes the applet much simpler and easier for Plasma specialists to improve.  We considered using a Plasma DataEngine or Service, but decided not to for now because it adds another layer of indirection.  For NetworkManager at least, if the settings service process leaves the system bus (due to a deliberate or accidental exit) you fall offline.  The settings service and Plasma are both complex programs, so combining them increases the chances that a bug in one can crash the other.  So we put it in a different process, forcing one layer of indirection already. 

Meanwhile Frederik Gladhorn and I were refactoring the storage layer for Connection settings so that it is independent of NetworkManager.  One of the good things about NetworkManager's settings is that they are so comprehensive the classes I developed to configure them cover all of wicd's settings too.  Frederik namespaced the general classes while I moved the DBUS code that is specific to NetworkManager 0.7 out of the libs/ directory.  Since it is generated automatically from some .kcfg files by a modified kconfig_compiler and then extra stuff is patched into those files, this was quite a lot of work. 

Our students from the University of Bergen, Anders, Peder and Sveinung, were busy working on the mobile broadband improvements for their degree group project.  This includes a set of DBUS bindings for the ModemManager auxiliary interface of NetworkManager, which were used to successfully send an SMS and will support useful functions like retrieving cellular signal strength, a set of Qt widgets around libmbca, taking the pain out of configuring cellular data connections, and a test harness.  We hope they will continue with KDE development after they graduate.

The status of Network Management as of Sunday 7 June then is that it doesn't even compile.  I'm working on remedying that as soon as possible.  If you do want to use Network Management from SVN, take a safe revision like r978079 until you hear otherwise.

We'd like to thank the Trolls for being great hosts and the KDE eV for sponsoring this sprint.
<!--break-->
