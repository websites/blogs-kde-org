---
title:   "openSUSE KDE 4 Build Guide Updated"
date:    2008-06-27
authors:
  - bille
slug:    opensuse-kde-4-build-guide-updated
---
I just updated the <a href="http://en.opensuse.org/KDE/Developing/Guide">Build Guide for KDE on openSUSE</a> for 11.0, some fixes for recent package splits, and some better formatting.  Unless you are or want to become a developer, our <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.0/">Build Service 4.1 Beta packages</a> should be new enough for you - they are updated weekly.