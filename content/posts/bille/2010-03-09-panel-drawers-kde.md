---
title:   "Panel Drawers in KDE"
date:    2010-03-09
authors:
  - bille
slug:    panel-drawers-kde
---
1) Create a folder somewhere eg ~/Desktop/My Favourite Apps
2) Drag and drop the folder onto a panel
3) Choose "Folder View" from the popup that appears
4) Drag apps from the menu, documents from Dolphin and other stuff onto the new panel icon
5) Click it and enjoy your new menu!