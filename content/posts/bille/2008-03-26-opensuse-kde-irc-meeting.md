---
title:   "openSUSE KDE IRC meeting"
date:    2008-03-26
authors:
  - bille
slug:    opensuse-kde-irc-meeting
---
Some people already think we do a <a href="http://jaysonrowe.wordpress.com/2008/03/25/i-found-my-perfect-kde4-setup/">damn fine job packaging KDE at openSUSE</a>.  But we're just a few guys and we'd do it even better with your help.  Tonight at 1900UTC we're having our latest openSUSE-KDE IRC meeting in <a href="irc://irc.opensuse.org/opensuse-kde">#opensuse-kde on FreeNode</a> and we'd love to see you there.  This is addressed to anyone who uses KDE on openSUSE and values the way KDE works there, whether you just booted a <a href=" http://home.kde.org/~binner/kde-four-live/">KDE 4 Live CD</a> or if you can remember KDE 1.1 on SuSE 6.4 and have your name on half of kdelibs.  In return we value your attention, so we can tell you what's coming up, your feedback, so we do it right, and your time - if you can help us plan features or organise squashing our bugs or tell us about the things we overlook because we are used to them, KDE gets better. 

Together we make an even bigger difference to the Free Software desktop, and every single contribution you make creates hundreds or thousands of individual moments of satisfaction as others use KDE.
<!--break-->