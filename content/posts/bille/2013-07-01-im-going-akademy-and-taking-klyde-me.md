---
title:   "I'm going to Akademy (and taking Klyde with me)"
date:    2013-07-01
authors:
  - bille
slug:    im-going-akademy-and-taking-klyde-me
---
Everything's booked: the weekend after next I'll be in Bilbao at KDE's tenth <a href="http://akademy2013.kde.org/">Akademy</a> meeting.  Catch <a href="https://conf.kde.org/en/Akademy2013/public/events/14">my talk about the latest happenings in Klyde, the lightweight presentation of KDE</a> at 14:30 on Saturday in the New Ideas track.