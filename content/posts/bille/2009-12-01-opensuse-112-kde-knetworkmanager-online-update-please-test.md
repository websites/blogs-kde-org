---
title:   "openSUSE 11.2 KDE KNetworkManager online update: please test!"
date:    2009-12-01
authors:
  - bille
slug:    opensuse-112-kde-knetworkmanager-online-update-please-test
---
If you've been paying attention at the back there, you'll know that openSUSE started using a new community-driven online update administration process for 11.2.   As well as Novell employees, community people are taking care of the workflow of examining and approving online updates to buggy packages.  Now I have a favour to ask of you - the online updates that are ready to go out need testing to make sure they don't inflict gross mischief on users' systems. 

KNetworkManager has an online update sitting in this queue awaiting testing.  As anyone who has read http://bugzilla.novell.com/show_bug.cgi?id=553908 knows, I forgot to initialise 2 bools added to KNetworkManager just before 11.2 shipped, causing the settings that control whether DNS and routing settings from DHCP to be applied to be set randomly, so the update team wants someone more reliable's opinion on whether my fix for that is any good.

If you want to test it out and take part, add the 11.2 update test repo (or manually browse this URL and install all the NetworkManager-kde4 packages yourself) - http://download.opensuse.org/update/11.2-test .  NB that due to a process bug the release number is the same so you will have to --force the install.  If it works for you, please comment on bnc#553908 to that effect.
<!--break-->
