---
title:   "KDE GSoC Idea"
date:    2009-03-31
authors:
  - bille
slug:    kde-gsoc-idea
---
A promising student was talking with me about working on Network Management in GSoC 2009, but decided to concentrate on his studies this summer.  Out of the discussion I've created this idea proposal. In case anyone is interested in making mobile broadband connections really easy to do in Network Management, see <a href="http://techbase.kde.org/Projects/Summer_of_Code/2009/Ideas#Network_Management">the KDE Google Summer of Code 2009 ideas page</a>.