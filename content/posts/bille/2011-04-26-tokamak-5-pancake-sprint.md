---
title:   "Tokamak 5: The Pancake Sprint"
date:    2011-04-26
authors:
  - bille
slug:    tokamak-5-pancake-sprint
---
Flat things are good.  I'm at Tokamak 5 in Nijmegen, the KDE sprint where we plough a deep furrow into the future of the Free Desktop and sow KDE seeds that will grow into exciting, novel interfaces and make the stuff we already have even faster and more reliable.  

So what about the flat things I mentioned?  We've just guzzled our way through a stack of pancakes of geological proportions, produced for us by pancake-flipper and KDE allrounder <i>par excellence</i> <a href="http://euroquis.nl/bobulate/">Adriaan de Groot</a>.  Other good flat things are tablets (I won't call them 'tablet computers' in case I sound old fashioned), which are in evidence here in a variety of makes and models.  We're working on several things that will make KDE on tablets as easy and fun to consume as Adriaan's pancakes.  

I'm here for a few days to make KConfigXT, KDE's proven automatic configuration persistence layer, work with user interfaces programmed in Qt Quick, and to support the <a href="http://community.kde.org/Plasma/Active">Plasma Active</a> work going on in the <a href="https://build.opensuse.org/project/monitor?project=KDE%3AActive">openSUSE Build Service</a> with my <a href="http://en.opensuse.org/openSUSE:Boosters">geeko skills</a>.
<!--break-->
