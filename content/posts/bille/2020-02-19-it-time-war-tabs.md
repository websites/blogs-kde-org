---
title:   "It is time for a war on tabs"
date:    2020-02-19
authors:
  - bille
slug:    it-time-war-tabs
---
<img src="https://blogs.kde.org/sites/blogs.kde.org/files/war_on_tabs.png" alt="5 different tab bar UIs, 4 of which are KDE"/>

We (as a UI shell project) see the limits of our territory as the window, when there is the assumption nowadays that MDI tabbed interfaces are where most significant user activity takes place.  Yet interacting with different views/documents within those windows is not standardised, so the user has to remember which app they are using, then select the appropriate actions to:

<ul>
<li>recognise the current tab</li>
<li>switch tabs</li>
<li>move tabs in the current window or between windows</li>
<li>notice which tab is being switched to (different switcher UIs, not shown)</li>
<li>open/close tabs</li>
<li>be warned when closing a window with multiple tabs</li>
<li>use keyboard shortcuts to switch tabs</li>
<li>persist tabs between logins</li>
<li>share sets of tabs between devices</li>
</ul>
All this causes additional cognitive load/dissonance when using your computing device. 

I'm not saying Plasma needs to become a tabbed window manager, but we can do better, and it is definitely time to declare war on the mess above.