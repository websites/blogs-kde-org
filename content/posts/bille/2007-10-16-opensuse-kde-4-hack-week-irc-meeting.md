---
title:   "openSUSE KDE 4 Hack week IRC meeting"
date:    2007-10-16
authors:
  - bille
slug:    opensuse-kde-4-hack-week-irc-meeting
---
As <a href="http://blogs.kde.org/node/3035">Cornelius</a> blogged, this week the KDE people at SUSE are spending our hard-won innovation time on polishing KDE 4.  The vast majority of our new development time is now allotted to KDE 4, so to make sure that our efforts go in the right direction and to coordinate them with the work the rest of the community, we'd like to announce an inaugural openSUSE KDE IRC meeting happening tomorrow, Wednesday 17 October, in #opensuse-kde on FreeNode, at 1800 CEST.  That's 1700 BST, or 1200 EST or the same time of day as the openSUSE project meeting if you exist in the shadowy planes of hacker-time.  The agenda so far is to discuss how to make KDE openSUSE 10.3++ the best ever, how you can contribute via the Build Service, and how to use KDE 4 already.

Now, back to cleaning up Kopete, Kontact and KHelpCenter - my birthday present to myself :).<!--break-->