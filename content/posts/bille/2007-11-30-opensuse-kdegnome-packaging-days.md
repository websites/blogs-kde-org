---
title:   "openSUSE KDE/GNOME Packaging Days"
date:    2007-11-30
authors:
  - bille
slug:    opensuse-kdegnome-packaging-days
---
Today and tomorrow are the first openSUSE KDE/GNOME Packaging Days.  In all timezones.  A truly global event.  One of the goals of openSUSE is to get SUSE packages in the care of non-Novell employees, so <a href="http://en.opensuse.org/User:Dirkmueller">Dirk Mueller</a> and <a href="http://en.opensuse.org/User:Maw">Michael Wolf</a> have been organising a couple of days where fearless peeps can get on board the <a href="http://build.opensuse.org">openSUSE Build Service</a> with a little help from the pros.  

Why use the openSUSE buildservice?  Well, we're bigger, and older, and better than the rest.  From one set of sources you can build, package and host for 7 SUSE Linux versions, 3 Fedoras, 3 *buntus, 2 Mandrivas and Debian Etch.  If you've got a new piece of software which hasn't yet hit the mainstream, how could you get more exposure quicker?  And we have best people.  As the maintainer of <a href="http://en.opensuse.org/Packaging/RpmLint">RpmLint</a> at SUSE, and the mad genius behind getting an entire distribution, KDE3 and a fair bit of KDE4 on <a href=" http://download.opensuse.org/distribution/10.3/iso/cd/openSUSE-10.3-GM-KDE-i386.iso">one</a> <a href="http://download.opensuse.org/distribution/10.3/iso/cd/openSUSE-10.3-GM-KDE-x86_64.iso">CD</a>, Dirk knows more than a bit about a clean package.  And as one of the leading GNOME packagers, Michael is a nonpareil at unravelling a ball of dependencies.  And we have a <a href="http://blogs.kde.org/node/3114">lot of CPU cycles</a> we want to give to you.  

Even if you are just building some <a href="http://www.kde-look.org/index.php?xcontentmode=14">cool widget style from kde-look</a> for your own use, learn how to package it and host it in your home project, and all the rebuilding hassle when a new version of your distro appears just goes away.

Come to <a href="irc://irc.eu.freenode.net/#opensuse-buildservice">#opensuse-buildservice on FreeNode</a> and get your favourite software all bundled up and ready to be served from a Build Service mirror near your users.
<!--break-->