---
title:   "KDE NetworkManagement Sprint Day Two"
date:    2009-06-06
authors:
  - bille
slug:    kde-networkmanagement-sprint-day-two
---
I felt like the grumpy grandpa of the NM sprint when the others hammered on my door at 9.30am after I'd rolled over for just another 10 minutes two hours earlier.  The grey cells do still work once you hit your thirties but they need more care and feeding if I want to be able to speak intelligibly the next morning - not going to rock bars until 3am!

We've continued planning this morning.  The big goals for this meeting was to 'get Network Management finished' and 'make it usable on non-NetworkManager systems' but our discussions last night showed us that the current complexity of the applet prevents both goals - it takes me several days of getting up to speed with the code before I dare to try to code it and it's deterring Frederik from making significant changes.  So we identified all the pieces and started juggling them last night over pizza until they landed in a way that makes sense.

The big picture is that most of the complexity will move from the Plasma applet into the KDED module.  This module will abstract different network management systems by being replaceable.  The module provides a simple list of the things to show in the applet's popup.  Configuration UI and stored settings are to be shared - we think that the current (NM-derived) settings schema is comprehensive enough.

There's a temptation to write an über-system that models everything and allows any number of applet implementations but we're resisting that as it would never be finished.  I'm a little bit disappointed that we won't be adding a lot of polish and nice to have features but a sprint is the ideal time to swing a large hammer at hard architectural issues that otherwise would stunt Network Management's growth.
<!--break-->
