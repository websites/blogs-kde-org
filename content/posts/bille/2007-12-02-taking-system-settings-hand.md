---
title:   "Taking System Settings in hand"
date:    2007-12-02
authors:
  - bille
slug:    taking-system-settings-hand
---
[image:3123 align=left hspace=20 node=3123]One of the big things about KDE 4 at an app level was moving from KControl to System Settings.  <u>The</u> major complaint about KDE (from non-KDE users) is that it is too configurable, where 'too' generally means they can't find the thing they want to configure.  System Settings is the product of usability-led design, and kcontrol was dropped some months ago, but it seems very little has happened since it was ported to KDE 4.  So rather than just give myself an ulcer about it, I've decided to take System Settings in hand and make it good.  I started by fixing a <a href="http://websvn.kde.org/?view=rev&revision=740484">couple</a> of <a href="http://websvn.kde.org/?view=rev&revision=740425">little</a> <a href="http://websvn.kde.org/?view=rev&revision=740466">bugs</a> but as the size of the task became apparent I decided to organise System Settings' development and maintenance first.  So spent today doing this.  I've started a <a href="http://techbase.kde.org/Projects/SystemSettings">project on TechBase</a> to:<!--break-->
<ul>
<li>pool all the design documentation there is on SystemSettings</li>
<li>track the state of the main app</li>
<li>track the states of all the modules</li>
<li>decide what to do about the missing modules</li>
</ul>
This last point is important.  I count 63 distinct modules in KControl in KDE 3.5.8 on openSUSE 10.3 but there are only 30 present in some form in KDE 4's System Settings.  I saw somewhere that System Settings was intended to be the 'basic' config and that advanced users should user KControl, but that is no longer possible in KDE 4.  So we need to find some homes for those other modules.  It has already been decided that some of them should only appear in the relevant apps (all the web browsing config stays in Konqueror), but a lot of them are only visible using kcmshell4 --list.  

I also went through the (few) bug reports vs KDE4 System Settings, closed a bunch that were obviously fixed, and set the right versions on the rest.  Please use bugs.kde.org for KDE 4 bugs!

As well as sorting out the shell, though, another challenge we face is getting the actual config modules sorted out and well ported to KDE 4.  I realised this when creating components for system settings for each KCModule.  The list of default owners for bugs on these components reads like a Who's Who of KDE Past - a lot of big names there but most of whom have left active development.  This is the biggest part of the job and it's more than one person can handle.  But fortunately KCModule work is a relatively easy way to start KDE 4 coding.  The scope is well defined and the architecture is relatively simple.  So, if you would like to find a way into KDE development, consider helping making System Settings as cool as it promises to be.