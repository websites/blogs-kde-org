---
title:   "Get your Hot New Emoticons"
date:    2005-03-10
authors:
  - bille
slug:    get-your-hot-new-emoticons
---
Everyone else is doing it so why can't we?  Today Kopete jumped on the Hot New Stuff bandwagon by letting users fetch and install new emoticon themes from the Configure dialog.  

<img class="showonplanet" src="http://www.stevello.free-online.co.uk/kopete/kopete_emot_khns.png"/>

<img class="showonplanet" src="http://www.stevello.free-online.co.uk/kopete/kopete_emot_buttons.png"/>

Implementing this was incredibly easy - only 8 lines of code - and since KMail, Kopete and Konversation share the same emoticon themes, the other apps will be able to use the new emoticons too.

I talked to Frank Karlitschek of kde-look.org about it, and we will soon be able to fetch themes from there.

Next up, rinse and repeat for chatwindow styles.