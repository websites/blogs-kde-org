---
title:   "calling all aspiring Instant messaging developers"
date:    2007-06-24
authors:
  - bille
slug:    calling-all-aspiring-instant-messaging-developers
---
I'd like to inform you all of my tutorial on instant messaging development for <a href="http://kopete.kde.org/">Kopete</a> (the KDE instant messenger), at 10.00am (huh?) on <a href="http://akademy.kde.org/codingmarathon/tutorialday.php">aKademy 2007's tutorial day</a>.  In an act of breathtaking opportunism I noticed that the no-one else has promoted their talk yet, so with a massive First Post!, be cognizant that both chat protocol and utility plugin development for the KDE 4 Kopete API will be presented to you, with lots of detail on the tricks and tips needed to make a useful extension to KDE's number one IM application.  
<!--break-->
Although the tutorial only has an hour on the schedule, I'm going to find us a quiet spot where we can have a code clinic on Kopete development for the rest of Wednesday, before we all get midge-bitten and sunburnt on Thursday.

So if you've ever thought about implementing some weird IM protocol (hey, I did Groupwise!), or if you have an idea for a plugin that no IM client was cool enough for, or you just want to make Kopete better, but the build system or the libkopete API frustrated you, come to aKademy and we'll make it real.

NB If you can't come, never mind, the tutorial will be posted pronto on the website, and we'll be on IRC.