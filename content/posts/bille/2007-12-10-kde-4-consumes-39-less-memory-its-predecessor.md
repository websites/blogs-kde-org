---
title:   "KDE 4 'consumes 39% less memory than its predecessor'"
date:    2007-12-10
authors:
  - bille
slug:    kde-4-consumes-39-less-memory-its-predecessor
---
Not mine, but one <a href="http://www.jarzebski.pl/read/kde-3-5-vs-kde-4-0.so">Korneliusz Jarzebski (in Polish)</a> has done <a href="http://www.pro-linux.de/news/2007/12082.html">the numbers (pro-linux coverage in German)</a> and produced a chart showing exactly how the RAM consumption of comparable KDE4 and KDE3 sessions measure up.  The result is a mindblowing <i>39% smaller memory footprint</i> in KDE 4.  This just goes to show, that it's worth making large-scale changes to your desktop environment to get the fruit hanging on the higher branches.   Wait for the mini- and micro-optimisations to start happening in KDE 4.x, too.<!--break-->