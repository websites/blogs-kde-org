---
title:   "16:38 MV CA"
date:    2008-01-18
authors:
  - bille
slug:    1638-mv-ca
---
I'll keep this short because my mental batteries are running a bit low.  Yesterday Dirk, Cornelius and myself from SUSE travelled to Mountain View in California for the KDE 4 launch event at Google.  So far we had BoFs on marketing, distributions (always fun), and now it's Plasma's turn.  Apart from that we had a nice lunch - even if the food didn't quite satisfy all the hungry hacker appetites present.  I've had fun meeting all the north american community members and hackers for the first time, spoken briefly on the phone to LugRadio with Aaron, and patched KUser to fix a bug the Slackware guys were experiencing.  There's a nice vibe here, much like at aKademy, and to me the turnout proves that with good planning, it would be possible to hold an aKademy here in the future.
<!--break-->