---
title:   "openSUSE Conference KDE Team Party"
date:    2010-10-15
authors:
  - bille
slug:    opensuse-conference-kde-team-party
---
Next week is openSUSE Conference week!  I'm using both my <a href="http://lizards.opensuse.org/author/wstephenson/">openSUSE</a> and KDE blogs to remind everyone that we're having <a href="http://en.opensuse.org/openSUSE:Conference_Kde_Social">a pre-conference meetup</a> at 6pm for the KDE team before the real conference begins at Barfüßer in the Nuernberg old town.  Remember a morning of keynotes is only fun if you have a thumping hangover from microbrewed beer (and if you're a keynote speaker, from local schapps too)!  If you are attending the conference or if you are just a friend of KDE in the area, please join in.  

<a href="http://www.flickr.com/photos/wstephenson/271479660/" title="KDE Hysteria! by will.stephenson, on Flickr"><img align="center" src="http://farm1.static.flickr.com/106/271479660_0e405d089f.jpg" width="300" height="225" alt="pa130024.jpg" /></a>

If you <a href="http://en.opensuse.org/openSUSE:Conference_Kde_Social">add your name to the wiki</a> I'll have an idea how big a table we need, I've provisionally got space for 20.


Will
<!--break-->
