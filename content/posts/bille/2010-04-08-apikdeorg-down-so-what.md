---
title:   "api.kde.org down! so what?"
date:    2010-04-08
authors:
  - bille
slug:    apikdeorg-down-so-what
---
KDE Developers may have noticed that the developer documentation server at api.kde.org is down.  This is due to a hardware failure which will be recovered next week.  That need not put the brakes on your work though, since if you have the source code on your system you can build the API docu locally yourself, as HTML, as man pages, or as Qt Assistant help files to view in Qt Assistant or Qt Creator.

Read all about it on techbase: <a href="http://techbase.kde.org/Development/Tools/apidox">http://techbase.kde.org/Development/Tools/apidox</a>