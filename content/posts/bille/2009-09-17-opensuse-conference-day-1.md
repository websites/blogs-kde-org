---
title:   "openSUSE Conference, Day 1"
date:    2009-09-17
authors:
  - bille
slug:    opensuse-conference-day-1
---
I'm just back in from the first day the openSUSE conference.  The day started badly when I woke up in a cold sweat dreaming that OpenOffice ate my presentation (again), but it was still there when I resumed my laptop and so I biked the 5km into the Berufsförderungswerk Nuernberg, the technical college where the conference is a guest.  A good number of people were in for Lenz Grimmer's keynote on virtual development teamwork, which was a relief, then I sat in for a bit of the openSUSE Weekly News talk by Sascha Manns.  Running a news magazine is an important and demanding part of a project's internal and external communications and I'm grateful that Sascha and team put in the effort, and hope they get more contributors.  Then I earwigged at the back of the GNOME team meeting, while Andy Wafaa demoed me the SUSE Goblin image.  It's impressively polished and will give the Plasma netbook interface a tough act to follow. 

Over lunch I mingled with openSUSE users and developers old and new, and a good number of KDE people including Frank, Alexandra, Danimo and Frederik who had made the journey to Nuernberg.  We had a cross-desktop meeting afterwards to figure out how to improve the general openSUSE desktop experience.   Following the recent decision to set a desktop choice in openSUSE, I feared there might be some tension, but it was all very amicable and constructive.  I had to leave early to give my talk, 'The Future of openSUSE KDE', moved forward from the weekend.  OpenOffice behaved, nothing crashed, and I gave my manifesto for producing the best desktop distribution with exemplary cooperation with upstream projects.  And interspersed it with previews from openSUSE 11.2, so nobody fell asleep.  KNetworkManager4 drew the most questions, and even managed to detect Danimo's phone on hotplug, something I'd never tested.

Finally we had a BoF session led by Cornelius to plan a KDE Showcase image for use at shows and by reviewers, which has a lot of potential, if also to be a lot of work.  

Tonight we're unwinding at Joe's Bar, a refactoring of the Novell/SUSE office as a den of iniquity, hosted by our Community Manager, Joe 'Zonker' Brockmeier.  Don't speak too loudly to me in the morning...
<!--break-->
