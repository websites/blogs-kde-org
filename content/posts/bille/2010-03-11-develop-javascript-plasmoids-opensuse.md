---
title:   "Develop Javascript Plasmoids on openSUSE"
date:    2010-03-11
authors:
  - bille
slug:    develop-javascript-plasmoids-opensuse
---
Aaron, Sandro, moofang, Shantanu and Diego have been hacking up a Plasma storm lately on the Javascript bindings for Plasma and the <a href="http://liveblue.wordpress.com/2010/03/02/plasmate-and-ghns/">Plasmate builder tool</a>.  Since good code is running code, and running code is a lot easier when somebody else builds it and packages it, I've updated the <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Playground/openSUSE_11.2_KDE4_Factory_Desktop/">Plasmate packages</a> in KDE:KDE4:Playground to <a href="http://aseigo.blogspot.com/2010/03/plasmate-01-alpha2.html">0.1alpha2</a> and have updated the javascript bindings in our KDE SC 4.4.1 packages to include <a href="http://aseigo.blogspot.com/2010/03/javascript-javascript-jam-updates.html">Aaron's latest errata</a> - no need to update yourselves.

So it's even easier to take part in the Plasma <a href="http://plasma.kde.org/cms/1243">Javascript Jam Session</a> competition now.

And while you're at it, how about completing the loop by using our <a href="http://blogs.kde.org/node/4177">kde-obs-generator</a> to package your plasmoids and make them available on kde-look.org, so others can start to download and improve them directly in Plasmate?  Free Software virtuous circle FTW!
<!--break-->
