---
title:   "KDE at FOSDEM 2009 photos"
date:    2009-02-12
authors:
  - bille
slug:    kde-fosdem-2009-photos
---
I took a few pictures of KDE people and the area around the hotel most were staying in.  Click for the rest:

<a href="http://flickr.com/photos/wstephenson/sets/72157613686748938/"><img src="http://farm4.static.flickr.com/3441/3273065536_5ac1d71a9b_m.jpg"/></a>
<a href="http://flickr.com/photos/wstephenson/sets/72157613686748938/"><img src="http://farm4.static.flickr.com/3381/3272248055_00464e5b22_m.jpg"/></a>
