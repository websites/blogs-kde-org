---
title:   "News of the world"
date:    2008-03-15
authors:
  - pinotree
slug:    news-world
---
Taking the idea of this entry from this album of Queen, I am going to show you some of the new stuff you will see in <a href="http://okular.kde.org">Okular</a> for KDE 4.1.
The development is pretty active, also on other parts that bring benefits to Okular.

<!--break-->

Albert, Brad and me are active part of the <a href="http://poppler.freedesktop.org">Poppler</a> team, and we concentrated our efforts for the new release, Poppler 0.8, going to be released within a month, hopefully.
Our work so far brought the following improvements:
<ul type="disc">
<li>Improvements in the way we get the information about the position of the characters in a page of a PDF; this means characters in rotated pages will be highlighted correctly in Okular</li>
<li>Poppler-Qt4 can now read check boxes and radio buttons in forms, and Okular can show them, yay!</li>
<li>Saving support: Poppler can now save the changes (not real editing though). Poppler-Qt4, the Qt4 frontend that Okular use, can do that as well, but only for form fields.<br />
For Okular this turns into a new "Save As" menu item that allows you save a document with the changes to its form fields. (Note: it is not perfect yet, so really make to do not overwrite your original document, and to not rely on the edited document only.)</li>
<li>Layer supports: Poppler now supports the layers in PDF documents, and the Qt4 frontend exposes an <a href="http://doc.trolltech.com/4.1/qabstractitemmodel.html">item model</a> for reading and manipulating the layers (enable and disable them). The only limitation is that there is no easy way to know which pages are affected by toggling a layer, and that is the only problem that keeps me from adding the layer support in Okular (a basically working patch without immediate page refreshing lies on my HD).</li>
</ul>

Pretty cool, isn't it? ;)

On the Okular-specific side, we already have many interesting improvements.
[image:3327 align=right hspace=10]
The most recent one (dated... today) is the addition of the possibility to search also backward, not only forward. This can be quite useful, especially when searching through documents with lots of pages; and was actually much requested too.

I think it can be enough for the moment, stay tuned for more! :)