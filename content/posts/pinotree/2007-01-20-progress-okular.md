---
title:   "Progress in okular"
date:    2007-01-20
authors:
  - pinotree
slug:    progress-okular
---
So, in line with the articles written by <a href="http://tsdgeos.blogspot.com">TSDgeos</a>, I'll continue talking about the work done in okular. A reason more to do that is that people ask about the work we are doing in okular, and about the new stuff we've done, so why don't let people know? ;-)

This time is dedicated two small user interface improvements, located in the View menu and in the Table of contents side pane.

<!--break-->

[image:2645 align=right]
The first one is the improved Orientation section of the View menu. Instead of the previous entries that did not sound quite user-friendly ("Default" / "Rotated 90 Degrees" / "Rotated 180 Degrees" / "Rotated 270 Degrees"), our great usability master Florian Graessle suggested me a much simpler and straightforward configuration. After all, it's what you usually need, ie rotate step by step on a side. We decided to add also a quck way to restore the original document orientation.

<br clear="left">

[image:2644 align=right]
As you can see in the screenshot, the Table of contents had some improvements w.r.t. KPDF.
First of all, a search line edit was added, handy tool when looking of a topic in large TOCs.
Then you can see a simple indicator (the black arrow) of the position of the shown page in the TOC. Its behaviour needs to be finetuned better, but mostly works. About the icon: at the moment it seems a working solution, even because we can't change the color as visual indicator (the focused item in the tree view has a different background color, so there could be a conflict with colors). Of course, any better solution will be glad (remember it has to pass the usability-master test :) )
The last improvement you can see is the possibility to label the pages of a document, and so to dislay their labels (if available) instead of the pages in the TOC. "Bad" side (temporary): it depends on a (small) patch for Poppler-Qt4, that sits in my hd waiting for TSDgeos to review ;-)

