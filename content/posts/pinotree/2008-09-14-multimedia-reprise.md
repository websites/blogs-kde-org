---
title:   "Multimedia reprise"
date:    2008-09-14
authors:
  - pinotree
slug:    multimedia-reprise
---
Yet another blog entry that comes after some time, so usual the apologize for the silence (in the rare case you missed me, of couse).

As also written in the <a href="http://commit-digest.org/issues/2008-08-31/">KDE Commit Digest</a> of two weeks ago, I started working again on the multimedia support in <a href="http://okular.kde.org">Okular</a>.

<!--break-->

A first part of the multimedia was the support for sounds in PDF documents, added before KDE 4.0. This means Okular can play sounds as requested. Currently, this can only happen as a reply of a page change action in presentation (for example, you open a page, and a sound plays).
The PDF standard has also sound annotations, that is an annotation represented by an icon, with a sound to be activated on activation. Poppler (the PDF library Okular uses) has a basic support for them in the last development version (0.9.0, released few days ago). And so has Okular, in the development version: small pity is the fact that Iñigo (the other guy working on annotations in Poppler, lately) and me we were not able to find documents with this kind of annotations, so they were not fully tested (and integrated in viewers). So, if you have such kind of PDF document, and can/want share it, please contact me :)

Another kind of multimedia are videos. This was already present in the core of Poppler, but not esposed so far by the frontents (Qt3, Qt4, and GLib). In Poppler 0.9.0 I added a basic support in the Qt4 frontend for them, and so in Okular's development version (what you will see in KDE 4.2, basically). The result is (more or less) the following:

<center>
<a href="http://blogs.kde.org/node/3677"><img src="https://blogs.kde.org/files/images/okular-video-3.preview.png" hspace="10" vspace="10" width="673" height="685"></a>
</center>

The above is a bit "weird", because it is the testbed image backend hacked to provide a movie (<a href="http://kde.org/stuff/clipart/konqi-magical-rope-video-720x576-divx.avi">Konqi and the Magical Rope of Curiosity</a>, as you could recognize) and ease a bit the development. But this is quite close to what you will see in the reality.
Ah, almost forgetting to say that you will be able to watch movies not only when reading the document normally, but also in presentation mode (probably one of the best use for such multimedia feature in PDF documents).

One last thing about this feature: this was requested with a <a href="https://bugs.kde.org/show_bug.cgi?id=136574">wish</a> on the KDE bug tracking system. Some time ago, I looked at its voter list, and I found something curious. So, turn on your best sight, <a href="http://blogs.kde.org/node/3678">look at the bugzilla screenshot</a> and see whether you can catch it ;)
