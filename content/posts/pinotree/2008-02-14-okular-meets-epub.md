---
title:   "Okular meets epub"
date:    2008-02-14
authors:
  - pinotree
slug:    okular-meets-epub
---
One of the objective we (as Okular[1] team) had for KDE 4 was bringing a public API for developers to expand the Okular formats easily, with no need to add new stuff into a single application. <a href="http://api.kde.org/4.0-api/kdegraphics-apidocs/okular/html/index.html">The result</a> (obviously not perfect) is what we hope to be a good base for people to have their document formats shown in a single application, without caring about writing code for the user interface. As usual, just <a href="https://mail.kde.org/mailman/listinfo/okular-devel">let us know</a> if there is any need or problem ;)

<!--break-->

Some months ago, <a href="http://nakee.wordpress.com">Ely Levy</a> came on IRC interested in writing a backend for reading the epub documents.
His efforts were concentrated first to the creation of a libray and a set of tools for reading epub documents. He is hosting them on SourceForge: <a href="http://sourceforge.net/projects/ebook-tools">http://sourceforge.net/projects/ebook-tools</a>.
So, he started integrating his library with Okular, and finally <a href="http://nakee.wordpress.com/2008/02/10/okular-epub-plugin/">few days ago</a> and <a href="http://nakee.wordpress.com/2008/02/13/okular-epub-plugin-part-2/">yesterday</a> he was able to show us his work, that finally landed in our Subversion (<a href="http://websvn.kde.org/trunk/playground/graphics/okular/epub/">playground/graphics/okular/epub</a>).

<img src="http://nakee.files.wordpress.com/2008/02/snapshot2.png">

Very cool, isn't it? :)

[1] Yes, we standardized the naming to "Okular".