---
title:   "A boring day"
date:    2005-02-23
authors:
  - barefooted
slug:    boring-day
---
It's a boring day at work.
Nothing special to do, just waiting for the time passing by to go home in the evening.
I can't wait getting home, so I can continue working on the feedback wizard I came up with a few weeks ago.

That reminds me:
Is anyone interested in a feedback wizard for his/her applications?
It's a nice way to get your users opinion on your work.

I admit, it would be better and quite cool if it would collect
statistical data about applications usage, but for the time being it is a nice tool anyway.
Feel free to give it a try http://kde-apps.org/content/show.php?content=20360

I'm working on a 0.2-release with some improvements and a nice new default banner ;-)
Please let me know if you can use it or if you have ideas how to improve it :-)