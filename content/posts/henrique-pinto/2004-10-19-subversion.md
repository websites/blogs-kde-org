---
title:   "Subversion?"
date:    2004-10-19
authors:
  - henrique pinto
slug:    subversion
---
I used to be the editor of KDE Traffic. Unfortunately, I'm having almost no free time, and working on it requires a lot of time. Besides it I also work on Ark and KEduca (that taking much of my time), on content for a Brazilian KDE site, and I stay in school for 12 hours a day.

I loved to write it, and I still would like to continue doing that (though I don't think I'll be able soon), and that's a reason why I still keep reading quite a lot of KDE mailing lists. While writing a complete KDE Traffic issue takes a lot of time, writing a blog entry doesn't. So I thought I would start a blog on what's happening on KDE mailing lists. Hope you don't find it boring...

One of the things being discussed right now is a possible move to Subversion from CVS. Some weeks ago it was proposed to convert kdenonbeta as a test (and while at it, rename it to something more meaninful such as kdeplayground or kindergarten). KDE Extra Gear's new maintainer Helio Castro proposed to convert the extragear modules instead (or in addition to). Currently, he's waiting for answers from all extragear app maintainers (but so far nobody objected).

One interesting statistic that appeared due to that discussion was the size of the full KDE CVS repository. Currently, it takes 12GB. It seems most of that is due to kde-i18n, and according to Stephan Kulow trowing away i18n's history might cut the size of the repository to 6GB. It seems converting the repository to SVN will increase that size, though it's still not certain how much.
