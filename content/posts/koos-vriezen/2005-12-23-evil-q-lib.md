---
title:   "That evil Q lib"
date:    2005-12-23
authors:
  - koos vriezen
slug:    evil-q-lib
---
What should one do if someone spreads FUD about a part of open source. Lets try the "why?" question.
So one bashes qt on #maemo, channel for this GTK based platform used by the N770. Names are mangled if not me.
<pre>
&lt;kalos&gt; Anyone know if the Skype Debian package will work on Maemo?
&lt;nomas&gt; kalos: unlikely.
&lt;koel&gt; isn't that using that evil Q lib?
&lt;nomas&gt; koel: yeah that probably as well.
&lt;koos&gt; koel: why is Q lib evil?
&lt;nomas&gt; does qt still need its proprietary preprocessor?
&lt;koos&gt; nomas: AFAIK qt preprocessor moc is dual licenced, one of them GPL
&lt;nomas&gt; koos: by "proprietary" I mean "no one else uses it" in this case... :)
&lt;koos&gt; nomas: proprietary is a bad choosen word then :)
&lt;thoughtfax> koos: "unique" is better
&lt;koos&gt; nomas: wrt to OSS
&lt;koos&gt; nomas: of course moc is for qt based sources only (I'm sure that a newly designed lib would use libsig++ now)
* koel|770/#maemo is happy with his qt-free zaurus
&lt;nomas&gt; koos: yeah, I am just not a very big fan of nonstandard language extensions.
&lt;nomas&gt; (maybe related to the fact that I work at the programming languages department of our university  :)
</pre>
And the guy doesn't want to answer, but someone else does. Wow, I'm impressed.
This FUD-ware annoys me a lot. But I see it too on dot.kde.org about eg. glib. Please stop this nonsense, accept different tastes, different choices. Yes there are good reasons why there isn't a libqt-mt on N770, same for wxwindows etc. But there is no need to disrespect fellow OS-developers. Long live our freedom!