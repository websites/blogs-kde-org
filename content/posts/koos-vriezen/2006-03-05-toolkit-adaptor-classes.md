---
title:   "Toolkit adaptor classes"
date:    2006-03-05
authors:
  - koos vriezen
slug:    toolkit-adaptor-classes
---
Mentioned in <a href="http://blogs.kde.org/node/1691">an older blog</a>, I ported KMPlayer to the Nokia770.
Given that the GUI of a full KDE desktop and from this device are so different, I doesn't made sense to make fake QWidget classes for this port. However for utility classes like QString and KURL it did make sense. This weekend I ported the RealPix support to the N770 and benefiting greatly from these wrapper classes. The diff between these source files are no more than some include statements and lines where QString -> String.

The design is like this. In my code I use calls to QString like 'lower()' and 'upper()'. So the wrapper looks like:
<code>
 template <class T> class StringTmpl {
    T string;
    ....
    StringTmpl <T> lower () const;
    StringTmpl <T> upper () const;
    ....
 };
 template <class T> inline StringTmpl <T> StringTmpl<T>::lower () const {
    return string.lower ();
 }
 template <class T> inline StringTmpl <T> StringTmpl<T>::upper () const {
    return string.upper ();
 }
</code>
Now for this to work with Qt, I should declare
 <code>typedef StringTmpl<QString> String;</code>
and I have my QString back again. But of course the idea is to wrap this up for GLib/GDK/GTK, so I wrote a GLibString class that implements all those calls and write
 <code>typedef StringTmpl<GLibString> String;</code>
instead. I have wrappers for QChar/QByteArray/QString/QFile/QTextStream/KURL/KMimeType/KIO::Job/QRect/QColor/QPainter and added QPixmap/QImage just now all using that same scheme. Only for CharTmpl, which maps to GLib's gunichar, I specialized the CharTmpl<gunichar> methods, avoiding having to write a GLibChar class.

One of the nice things about the chosen solution is that all of the above is optimized away by the compiler. And if the implementation classes has lots of inlines as well, it should be as fast as using the g-functions directly (ignoring the "W" symbols if compiling to a shared object, as they can be stripped away)