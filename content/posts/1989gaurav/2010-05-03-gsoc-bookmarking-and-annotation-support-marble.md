---
title:   "GSOC : Bookmarking and Annotation Support to Marble"
date:    2010-05-03
authors:
  - 1989gaurav
slug:    gsoc-bookmarking-and-annotation-support-marble
---
<p>Namaste(Hello) everyone specially marble developers, Writing my first blog because I got accepted in GSOC 2010 for marble and want to keep you updated about my progress. First of all thanks to KDE Community to give me such an opportunity.</p>

<b>About Me</b></br>
<p>My full name is Gaurav Gupta and everyone call me GG and thats why you can find me on #kde-edu and #marble with the same nick. I am final year undergraduate student of computer science and engineering from IT BHU India. I am quite interested and using open source from last four years and starting my contribution with KDE marble. I am interested in swimming, volley and playing strategy games like AOE or DOTA.</p>

<b>GSOC project</b>
<p>I worked on terrain visualization software in vizexperts so I found marble quite interesting for me that's why I proposed my own idea for GSOC. I proposed 3 basic functionalities of bookmarking, file attachments and drawing tool bar.<p>

<ol><li><b>Bookmark Facility</b></li>
<p>Main aim of this task would be to add bookmarking facility in marble. We can add, delete or load a particular view of marble.

<ul>        <li>Once the user wishes to add Bookmark various attributes like Centre Longitude, Centre Latitude and Zoom Level  are stored in a kml file.</li>

        <li>Once the user wishes to load Bookmark, parser parses the kml file and extracts values of various attributes by calling various tag handlers corresponding to a bookmark. These values are used to set current state of MarbleWidget and bookmarked is loaded.</li>
        <li>For this purpose I am planning to have a Bookmark tag having structure like :
         <code><bookmark>
              <name></name>
              <description></description>
              <lookAt>
                  <longitude></longitude>
                  <latitude></latitude>
                  <altitude></altitude>
              <lookAt>
          </bookmark></code></li>
</ul>
</p>
<li><b>Drawing Tool Bar</b></li>
I will introduce a drawing tool bar in marble having four functionalities which are :
<ul>
<li>Add Placemark</li>
<p>
A Placemark associated with a Point has an icon associated with it which marks a Point on the map in 3d viewer. At present Placemarks are saved in static kml file which are loaded at the starting time and not editable. Aim of this task is to provide functionality of adding placemark with a particular point on map. Which will be done as follows :
<ul>
<li>On addition of new placemark, insert newly created placemark in kml file of placemarks which follows predefined schema, with various of child tags and their attribute values.</li>
<li>On change re-parse the kml file and draw completely in widget.</li>
</ul>

</p>
<li>Draw Polygon and Annotate</li>
<p>
A user can directly draw a polygon over the marble widget containing various point of interest with the help of mouse, and this polygon is primarily used to add annotation with the particular selected area. Whenever someone clicks in the polygon then all added annotations will be shown with some additional options which are remove note, edit note, attach and view attached files ( explained later ). I will proceed  as follows :
<ol>
<li>MarbleWidgetInputHandler will be modified to handle the click event if draw polygon option is chosen. On each click a new point is created and added into polygon, drawn by QPainter and on completion of polygon the vector of the polygon is stored in kml file with added annotation.</li>
<li>When someone clicks on a polygon then MarbleWidgetInputHandler will call parser to extract information about that vector  and corresponding annotations will be shown with some editing options.</li>
<li>To store information about polygon in kml file I will use LinearRing tag which is previously implemented with GeoDataLinearRing object, I have to just add some more functionalities.</li>
</ol>

</p>
<li>Add Path</li>It will be same as polygon, the difference would be that it may not have a closed boundary and in terms of implementation here I will use LineString tag to store in kml files.
<li>Add Image overlay</li>In this task I have to add an image on the marble as a surface. Main step is wrapping the image onto the surface on which “Torsten rahn” is worked already and my aim wold be just to integrate it with marble GUI.
</ul>
<li><b>Attach Documents</b>
Finally this task aims at attachment of a file ( which may be family photos, videos, global presentation etc. ) with marble. Isn't it cool that you can visualize your data geographically. When anyone adds a new placemark or new polygon then there will always be an option of attaching file with it. This will be quite useful in case of memorizing things on the basis of their geographic position.  At the time of attachment link of file is inserted with corresponding information of note and geographic boundary, and when one is interested in viewing them then extract link for kml file and show them.</li></ol>

<b>Values I will add to Marble<b>
<ol>
<li>This project will put one more checkmark in the comparison done between various virtual globe at http://en.wikipedia.org/wiki/Virtual_globe in “Drawing Tools” field.</li>
<li>After this project marble can store data and various information geographically which are stored in relational databases.</li>
<li>After above described functionality marble will be of great use for defence people to keep track of all past activities and information at a specific location and give them geographical perspective of storage.</li>
<li>A bug https://bugs.kde.org/show_bug.cgi?id=177059 regarding geographically storage was opened, which is not yet implemented but my idea will serve it better.</li>
</ol>

That's all about my GSOC project but I won't stop here because I have some more advanced features in my mind and will try to implement them once all above functionalities are implemented. So wish me luck for my project completion before time so we can have some more cool features in marble.

Hope you will find this blog interesting and in end I wish best of luck to all GSOC accepted student for their project completion and lets give it a marblish enhancement. 