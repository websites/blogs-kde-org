---
title:   "Qt3 D-BUS bindings preview release"
date:    2005-12-16
authors:
  - krake
slug:    qt3-d-bus-bindings-preview-release
---
Just reached a releaseable state in my efforts to backport the Qt4 D-DBUS bindings to Qt3.
Basic usage works as expected, but I don't know yet if the marshalling code is sufficient for real world use as Qt4's QVariant is a lot better than one from Qt3, e.g. allowing user defined types.

<a href="http://www.sbox.tugraz.at/home/v/voyager/dbus-qt3-20051216.tar.bz2">This tar archive</a> contains the a qt3/ sub directory meant to go into a dbus CVS checkout.

If you replace the old qt3 directory there with the contents of the archive, you can enable the Qt3 bindings build by passing --enable-qt3 to configure (after you have run make -f Makefile.cvs)

Kudos to Harald Fernengel for writing the Qt4 bindings!
<!--break-->