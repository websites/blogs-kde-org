---
title:   "PIM(p) your desktop!"
date:    2008-01-30
authors:
  - krake
slug:    pimp-your-desktop
---
Great quote from <a href="http://daniel.molkentin.de/blog/archives/105-KDE-4.0-Chaos-Radio-Express.html">Danimo's Chaos radio express interview</a> when the topic <a href="http://pim.kde.org/akonadi/">Akonadi</a> is discussed.

Since I am one of the developers invited to this year's KDE PIM developer meeting in Osnabrück, I am currently trying to improve my knowledge of Akonadi.

Being the maintainer of a rather small PIM application, kabcclient, which only uses one of the specialized APIs, the addressbook API <a href="http://api.kde.org/4.x-api/kdepimlibs-apidocs/kabc/html/index.html">KABC</a>, I thought that a good way would be to start there.

KABC and its sibling KCal are based on the KResources framework, which means they get their data from plugable backends, so called resources.
Since they are part of the kdepimlibs module and have been released as part of KDE 4.0, they will still need to work once we have transitioned to Akonadi in KDE 4.1, or in other words they both need a way to get data from Akonadi instead of their usual sources like direct file access.

So I <a href="http://websvn.kde.org/trunk/playground/pim/akonadi-kres/">started to work</a> on a KABC::Resource which operates on an Akonadi collection and a KCal::ResourceCalendar which does the same.

The one for addressbook data already works quite well, the other one still crashes a lot, you have been warned :)

While we are on the topic of compatability: Aaron <a href="http://aseigo.blogspot.com/2008/01/lca-live-blogging.html">writes</a> from a conference in Australia that Akonadi is also being recommended to our peers in the GNOME project.
This makes a lot of sense because Akonadi is basically the next generation implementation of the principles they are currently using with E-D-S, the <a href="http://www.go-evolution.org/EDS_Architecture">Evolution Data Server</a>, quite similar to how D-Bus is a next generation implementation of concepts we previously used through DCOP.

Such a change is obviously quite some work, so it might make sense to do it as a series of smaller steps.
One such step could be to let their E-D-S application side APIs untouched but make them talk to Akonadi instead, a bit like how the <a href="http://labs.o-hand.com/embedded-eds/">embedded version</a> transparently uses D-Bus instead of CORBA.

Most of the access patterns can be mapped quite easily, I have a compatible D-Bus service side implementation based on our traditional APIs (e.g. KABC) sitting in my local SVN repository (which I might commit to playground once I have finished its transition to Akonadi).
<!--break-->
