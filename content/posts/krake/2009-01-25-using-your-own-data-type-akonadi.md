---
title:   "Using your own data type with Akonadi"
date:    2009-01-25
authors:
  - krake
slug:    using-your-own-data-type-akonadi
---
After I received a lot of positive feedback about my 
<a href="http://techbase.kde.org/Development/Tutorials/Akonadi/Resources">Akonadi Resource Tutorial</a>, or probably more accurate outright praises :), I knew I had to write another one.

This time I chose a topic that is probably less relevant for the majority of developers, however it should be helpful for people who either work on PIM applications with data types not yet covered by our current code or who want to use Akonadi for non-PIM data.

Akonadi, or more precisely its central component the Akonadi server/cache, can deal with any kind of data, basically treating it as array's of bytes.

At the client side, i.e. in applications, resource and agents, an item's MIME type is being used to decide how to process the data, e.g. how to convert it into a programming language's data structure.

This conversions are delegated to so-called "serializer plugins" and the tutorial is about writing such a converter.
As the example used as the custom data type to create support for I chose a simple chat log/history, loosely based on Kopete's XML format for storing conversation histories.

<a href="http://techbase.kde.org/Development/Tutorials/Akonadi/SerializerPlugin">Enjoy!</a>
<!--break-->
