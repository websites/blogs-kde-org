---
title:   "Release Party, Graz, Austria"
date:    2010-08-03
authors:
  - krake
slug:    release-party-graz-austria
---
<a href="http://apachelog.wordpress.com/">Harald</a> has asked me to organize a KDE release party here in Graz.

However, I am too lazy (wait! busy! sorry for my English, busy is the word I was looking for) to do that there won't be a dedicated event to celebrate KDE's newest achievement.

Fortunately the release date, August 4th, is a Wednesday or more precisely the first Wednesday of the month.
And on such rare occasions (happens only twelve times a year) Free Software users from Graz and areas close by gather at a nice Italian restaurant to have Pizza, Beer (some even have Cappuchino and Tiramisù) and talk about stuff that comes to their minds.

For the German native speakers among you, we call this "LUGG Stammtisch" :)
(as in Linux User Group Graz, Stammtisch should be self-explanatory)

So if you think you can stand yet another pizza or fancy having some beer with awesome people, including at least one well know KDE developer ;), join us at <a href="http://www.cosanostra.at/">Cosa Nostra</a>, <a href="http://stadtplan.icomedias.com/stadtplan/index.php?coords=-67593,214865&dpi=9000&show=-67593,214865,Hans-Sachs-Gasse,10&suche_nach=strassen&413550510">Hans-Sachs-Gasse 10</a>.

Beginning at 20:00, until we are thrown out (gah! until we are politely asked to leave) :D

See you there!
<!--break-->