---
title:   "Houston, you have a visitor"
date:    2008-04-07
authors:
  - krake
slug:    houston-you-have-visitor
---
Well, not yet.

I am currently at Graz airport, about to begin a journey to Austin, Texas, where I will be attending the Linux Collaboration Summit.
Special thanks go to the <a href="http://www.linux-foundation.org/">Linux Foundation</a> for covering my travelling costs and <a href="http://ev.kde.org/">KDE e.V.</a> for the hotel, specifically <a href="http://www.monroe.nu/index.php">Ian Monroe</a> who took the burden of doing the hotel reservation.

Ah, right. Houston.
The summit takes place from Tuesday to Thursday, however I am not returning until Sunday. Interestingly any return flight earlier doubles the fair, so I basically have to stay two days longer :)
My plan is to a day trip to Houston and visit the Johnson Space Center there, though I have still to figure out how to get from Austin to Houston and back again.

Anyway, boarding will begin soon (hopefully).
<!--break-->
