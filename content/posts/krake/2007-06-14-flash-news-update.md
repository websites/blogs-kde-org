---
title:   "Flash news, update"
date:    2007-06-14
authors:
  - krake
slug:    flash-news-update
---
About two days ago <a href="http://blogs.kde.org/node/2832">I wrote about changes</a> in the way browser plugins are ging to be handled. At that time, based on the information I gathered from two threads on kfm-devel (linked to from the other blog entry), I assumed that the new Adobe Flash player plugin would just use XEmbed for the visual part but still be an old style in-process plugin with all its difficulties.

However, as we can read on Mike Melanson's blog today, the Linux version of the <a href="http://blogs.adobe.com/penguin.swf/2007/06/fullscreen_beta.html">Flash plugin is now a native GTK application</a>.

He doesn't give any details on how they communicate between the remainging plugin stub and the player application. Best guess is a Unix socket based custom protocol, though their current push to use non-ancient technologies would leave it as a possibility that their are actually using common technology like D-Bus.
<!--break-->
