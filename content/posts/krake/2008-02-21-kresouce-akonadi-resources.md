---
title:   "KResouce Akonadi resources"
date:    2008-02-21
authors:
  - krake
slug:    kresouce-akonadi-resources
---
No, the title is not recycled from my <a href="http://blogs.kde.org/node/3270">previous blog entry</a> but it is very closely related.

Last time I wrote about Akonadi based KResource plugins, this time I am writing about KResource based Akonadi resources.

Since both entries are rather targeted at other developers, I promise to write a third one explaining the things from a user's point of view :)

But now back to the topic.
In order not to lose all the work put into the KResource implementations for various backends like popular groupware servers, it is basically a necessity to provide a bridge using these implementations to get data from their respective backends into Akonadi.

Akonadi uses a special variation of its clients, called Akonadi resources, to provide loading and saving on some external storage, e.g. local files, remote files, groupware servers.

The two new resources, called <a href="http://websvn.kde.org/trunk/KDE/kdepim/akonadi/resources/kabc/">KABCResource</a> and <a href="http://websvn.kde.org/trunk/KDE/kdepim/akonadi/resources/kcal/">KCalResource</a> use <a href="http://api.kde.org/4.x-api/kdepimlibs-apidocs/kabc/html/classKABC_1_1AddressBook.html">KABC::AddressBook</a> and <a href="http://api.kde.org/4.x-api/kdepimlibs-apidocs/kcal/html/classKCal_1_1CalendarResources.html">KCal::CalendarResources</a> respectively to access data provided by an appropriate KResource implementation.

Again, similar to my other compatability implementations, I haven't tested it very thoroughly yet, but loading from a local vcard or calendar file, saving at resource shutdown and reacting to changes in the file's data should already work.

If you'd like to help testing, both resource can be added as usual through akonadiconsole, where they are called "Akonadi KABC compatability resource" and "Akonadi KCal compatability resouce".

As their configuration step they'll open the usual KResource selection dialog which allows to select their KResource backend, i.e. the actual data access implementation.

Any feedback appreciated!
<!--break-->
