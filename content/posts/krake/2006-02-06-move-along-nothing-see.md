---
title:   "Move along, nothing to see..."
date:    2006-02-06
authors:
  - krake
slug:    move-along-nothing-see
---
...here. Yet.

"Yet" because I don't have anything to commit as all involved code is littered with commented codeblocks of failed attemps.

I am talking about a QObject adapter for the Qt3 D-BUS bindings, i.e. a wrapper class that makes slots of a given QObject instance callable through D-BUS.
Generating D-BUS introspection data from the slot signature is next.

Finding out how to call slots wasn't that difficult, any moc output file gives you plenty of hints.
The hard part is to get the details of all this "QU*" classes (leftovers from Universal Objects?) and to set the actual parameter data.

While I understand that the headers in qt-includes/private/ are not documented because they are private, why the heck do I have to hack a giant switch() for the QVariant types when there is this nice, public, QVariant::rawAccess method?

*sigh*

At least I have finally reached the point where the current binding's marshalling code isn't sufficient anymore.

So I guess I will be adding "writing a variant datatype class" to my C++ knowlegde portfolio soon.
Unless someone else contributes one (hint, hint) ;)
<!--break-->
