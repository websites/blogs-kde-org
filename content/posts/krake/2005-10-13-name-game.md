---
title:   "The name game"
date:    2005-10-13
authors:
  - krake
slug:    name-game
---
Since it is currently en vogue to ponder about the use of the KDE name, I have no other joice to join the fun ;)

While the focus has been on the topic of applications, it also applies to developers:
when can a developer call himself a KDE developer?

Is it enough to work on an application that uses KDE technology?
Do you have to be at least the maintainer of it?
Does it have to be hosted in KDE's SVN repository?
Does it have to be in one of the modules that get officially release with each KDE release?
Do you need to have made contributions to kdelibs?

Now this is something to think about! :)

For the record:
I consider an application as a KDE application when it uses KDE technology properly and a KDE developer a person that contributes to such an application.
No matter where it is hosted.

Since it is currently also en vogue to suggest alternative naming schemes: how about calling application maintained and released by the KDE team KDE.org applications?
<!--break-->