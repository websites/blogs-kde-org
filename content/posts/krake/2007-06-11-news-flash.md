---
title:   "News Flash"
date:    2007-06-11
authors:
  - krake
slug:    news-flash
---
Or rather Flash news?

One of Adobe's Flash player developers, Tinic Uro, today used his blog to tell us users about upcoming features.

Among the <a href="http://www.kaourantin.net/2007/06/flash-player-update-3-beta-1.html">list of changes</a> we can read about a change regarding the Linux Flash plugin:
<i>The Linux plugin now uses the XEmbed protocol. This is work in progress. The downside is that konqueror and Opera do not support this right now, so the Flash plugin will not work until these vendors update their plugin support.</i>

As a KDE user you might now think "doesn't Konqueror already support XEmbed?"

Short answer: it does. It been using XEmbed for years, actually in an even more progessive way, i.e. detaching the plugin from the browser process by running it in an external plugin viewer and just embedding its window.

For a collection of reasons on why this is a good idea, check out <a href="http://zrusin.blogspot.com/2007/05/browser-plugins.html">Zack's blog about this</a>

So, how can Konqueror both support and not support this kind of technology?

Well, Tinic Uro is talking about a certain use case of XEmbed, a new variant of the Netscape plugin API for X11.
You can find a thread on KDE's kfm-devel list about this <a href="http://lists.kde.org/?t=117560482600004&r=1&w=2">here</a>, initiated by another Adobe Flash player developer, Mike Melanson, who <a href="http://blogs.adobe.com/penguin.swf/">is working on the Linux player</a>

Plugins using this new API would still run inside the browser process, thus getting into trouble if they make assumptions about the browser, e.g. assuming that it is Firefox or, more low level, assuming that the browser <a href="http://lists.kde.org/?t=117813541400003&r=1&w=2">uses GTK+ as its toolkit</a>

Since neither Gecko, KHTML or WebKit are tied to a particular toolkit, though Gecko and KHTML only have one widely used backend implementation, this is quite a futile assumption.

However, there is light on the horizon regarding this problem, since even the  Mozilla/Firefox team <a href="http://zrusin.blogspot.com/2007/05/browser-plugins.html#1095983335336638311">is considering</a> (comment by "Robert") using out-of-process plugins. Especially interesting for Firefox users who visit websites with Java applets, since an out-of-process Java Applet does not freeze the browser during JVM startup.

Back to the Flash topic. Another upcoming feature, though not Linux related yet, is this one:
<i>Full-screen mode with hardware scaling. Probably the biggest new feature in the Flash Player Update 3. This leverages DirectX on Windows and OpenGL on OSX.</i>

Not yet Linux related because I am pretty sure they will add this to the Linux player as well, once they have the code paths tested through their Mac OSX version.
On a related note, the GNU Flash player <a href="http://www.gnu.org/software/gnash/">GNASH</a> is already using OpenGL for rendering so there shouldn't be any other technical problems for cross-platform feature parity in this case.
<!--break-->
