---
title:   "Why Free Java matters"
date:    2005-09-21
authors:
  - krake
slug:    why-free-java-matters
---
<a href="http://www.yepthatsme.com/2005/09/21/why-make-a-free-runtime-for-java/">Barry Hawkins</a> blogs about his reasons why he thinks working on free Java implementations is a good thing.

Being a long time JavaLobby subscriber I have read several pieces of pro and contra and enjoyed several flamewars in the articles' trackbacks or forums :)

One item I care about but that really seldom comes up in this discussions is installing Java runtime environments (JRE).

You can find postings of Sun employees how neat their new Windows installer is and how it can found out which components of the JRE you have already installed and how it uses this information wisely to minimize the downloads.

However there is no such thing for Linux.

All you will get is some kind of self extracting blob that may or may not ruin your system.
According to Sun's download page the same situation applies to Solaris, so I grant them lack of knowledge instead of believing they are making it harder than necessary.

Of course, the moment you suggest Sun could arrive in the 21st century and find out about proper package management, you'll have whiners all over you, telling you that poor Sun can't obviously produce packages for all those bazillion packagemanagers (two anyone?).

Well, how about not prohibiting repackaging then?

As the obviously have one single version of the required files for all Linux distributions, why not allow to rearrange those files, without modifications of course, into an installable form?

Or, how about allowing distribution at all?

As it stands, a Java application developer can either rely on an JRE installed or put some kind of installation instructions into their package. Automatic dependency resolution? Sorry.

If you reach that point in a discussion, you'll get the gem: "You are allowed to distribute a JRE along your Java application"

Wonderful!
Must be a new mantra. Like "Write once, run anywhere" has been replaced by "Write once, deliver everything yourself".

Therefor I have high hopes on the progress of free Java implementations. Not just because free as in speech is good, not just because of avoiding lock-in, because of getting a platform that can actually be delivered to the users in a way they expect software to be delivered.
<!--break-->