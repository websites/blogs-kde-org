---
title:   "Qt-KDE integration"
date:    2003-08-20
authors:
  - krake
slug:    qt-kde-integration
---
Letting Qt applications user aRts as their sound backend.

Short introduction: currently most of my KDE related time is used to help newbie Qt and KDE developers. When one of the user of www.mrunix.de, a German Unix/Linux developer board, had a problem with Qt sound (didn't have have NAS installed), I decided to try implementing an aRts backend for Qt.

Well, here it is:
 http://www.sbox.tugraz.at/home/v/voyager/qsound_arts.tar.bz2
(the implementation is actually only one file, the rest of the archive is the modified qsound example from Qt)