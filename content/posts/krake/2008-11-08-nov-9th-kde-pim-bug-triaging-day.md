---
title:   "Nov. 9th - KDE PIM Bug Triaging Day"
date:    2008-11-08
authors:
  - krake
slug:    nov-9th-kde-pim-bug-triaging-day
---
The KDE BugSquash team is holding another testing and bug triaging day tomorrow, Sunday 9th of November, to help maintain and improve the quality of KDE PIM applications.

Especially versatile applications like KMail and KOrganizer can potentially be tested numerous times by its developers without finding any issues because it requires a certain workflow or data set to trigger them.
Therefore help by as many volunteers as possible is hugely improving the situation, because every person will have different goals, preferences on how to do things (e.g. mouse v.s. keyboard), data sources, amount of data, etc.

If you are interested and have some time tomorrow you'd like to donate to KDE, see the BugSquad's <a href="http://techbase.kde.org/Contribute/Bugsquad">information page</a>.
<!--break-->
