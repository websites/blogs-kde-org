---
title:   "Install-time bugs"
date:    2006-11-09
authors:
  - krake
slug:    install-time-bugs
---
Bugs are bad, I think we all agree on this.
One of the most evil kind of bugs are those that appear only during installation or installation related procedures.

I wrote in an earlier blog that I have been helping the Debian-KDE packagers fix a <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=392807">bug</a>.
Unfortunately, due to a side effect (I hate side effects), a newly installed KDE would no longer find the KControl modules!
The bugs does not appear when upgrading the packages since the old, wrong, directory is still there. So it took someone with a fresh installation to even discover the problem :(

Obviously the programs which are most likely to be bitten by this are installers. For example, a user <a href="http://lists.debian.org/debian-kde/2006/11/msg00020.html">posted</a> to the debian-kde mailinglist that the Debian installer, or more likely its tasksel stage, have a bug when installing the desktop task, i.e. the KDE desktop is not installed correctly.
Since installers, especially new ones, are mostly used when a new distribution release is about to happen, so their developers are really the poor ones.
Lets hope the DDs can figure it out before the Etch release, would be a pitty to have the Debian desktop weaken while it is having heavy competition from derivatives.

What is the usual procedure to test installers, or how could regular users help testing them? Virtualisation? Spare machine?
<!--break-->
