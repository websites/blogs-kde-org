---
title:   "There and back again"
date:    2008-12-15
authors:
  - krake
slug:    there-and-back-again
---
Last week I have been in Nürnberg again, this time almost a whole week.

On Tuesday evening I met Will and half a dozen other SUSE people in a really nice pub called Irish Castle.
Ah, Guiness, Fish&Chips and more Guiness :)
Though, as an afterthought, I should probably have gone with Wings&Chips like Will did, they just looked even more delicious.

Uh, and as a warning for anyone else spending time with those crazy SUSE folks: they have strange running gags going on, like pooling money for someone to get a new car which that someone doesn't see any need for :)

On Friday I was fortunate enough to get out of work early afternoon and so I decided to visit the famous Christmas market.
On the way there I came across a small park, ground and trees nicely covered with snow and swarms of huge blackbirds everywhere. Like chocolate chips on icecream :)
Even more impressive an official nature reserve called Pegnitz Valley right in the middle of the city!

The visit to the Christmas market, which seems to spread all over the inner city, went as expected:
- squeezing through crowds of people in different states of drunkness: check
- eating unhealthy food: check
- with lots of fat and alcohol: check
- and while we are at alcohol: <a href="http://www.nuernberger-feuerzangenbowle.de/">check</a>

Afterwards I went to a nice Cafe/Bar/Restaurant called <a href="http://www.cafe-bar-celona.de/regio.php?cat=92">Cafe&Bar Celona</a> and really nice Pasta, Hefeweizen and Hot Chocolate with Baileys.

Speaking about "Weißbier": you know you are in Bavaria when the answer to the question "Do you have dark one as well" is not a blank stare or "no, sorry", but rather a friendly "of course!" :)

Anyway, bonus points for anyone correctly guessing where I am currently heading to. <b>again</b>
<!--break-->
