---
title:   "Akonadi Resources for Google Contacts and Calendar"
date:    2009-08-16
authors:
  - krake
slug:    akonadi-resources-google-contacts-and-calendar
---
There has been some confusion about Google data capabilities around the KDE 4.3 release.

The <a href="http://techbase.kde.org/Schedules/KDE4/4.3_Feature_Plan#kdepim">4.3 Feature Plan</a> has an entry for that and it is marked as "Completed".

What it meant is that the Google Data resoures by Adenilson Cavalcanti would be available at the time of the 4.3 release, however it got, understandably, interpreted as being part of KDE PIM in 4.3.

Lesson learned: don't put features which are not part of the main modules on the feature plan.

Anyway.
The Google Data resources are currently developed and maintained as part of KDE's extragear effort, basically a "third party" module hosted on KDE's infrastructure.
Applications in there follow their own development and release cycles, though I think it is possible to put tags on certain versions to indicate which version will work best with the newest KDE release.

In case of the Google Data resources, the original plan of releasing simultaniously with KDE 4.3 got scrapped in favor of an urgent fix in one of the two resources and, IIRC, in the Google Data access library used by both.

Since neither the maintainer nor any of us from the KDE PIM had previous experience with extragear style releases, we seem to have failed doing a proper release announcement or doing it at the proper channels.

The source archive for the resources can be found <a href="http://libgcal.googlecode.com/files/akonadi-googledata-1.0.tar.bz2">here</a>, depending on the libgcal version 0.9.2 which can be found <a href="http://libgcal.googlecode.com/files/libgcal-0.9.2.tar.gz">here</a>.

We might decide to move their code into the main KDE PIM module (kdepim-runtime actually) at a later version. For now the extragear style has the advantage of allowing a fast paced development with released in between KDE releases.
<!--break-->
