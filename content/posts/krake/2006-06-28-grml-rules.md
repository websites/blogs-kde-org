---
title:   "grml rules"
date:    2006-06-28
authors:
  - krake
slug:    grml-rules
---
If the title doesn't tell you anything, you'll be a lot wiser after reading this :)

So I bought this <a href="http://blogs.kde.org/node/2091">new Vaio laptop</a>.After checking that the really nice Vaio recovery tool does indeed leave any main operating system intact when restoring the pre-installed entertainment system, I was about to install the usual Debian/SID onto it.

However, I decided to install <a href="/www.grml.org">grml</a> instead.
The LiveCD already showed that it would support most of the hardware through autodetection, so after a few minutes of waiting for grml2hd and installing the <a href=http://"www.kde.org/">important things</a> through Debian's unstable repositories, I had a working machine.

Or so I thought.

The sky2 driver necessary for the Marvell Yukon ethernet device stops working after some seconds of high traffic, I have to reduce the bandwidth using tc (thanks go to Kurt Pfeifle for mentioning this terrific tool).

Some issues came from grml or rather my choice to use it as a desktop system, for example it installs dbus by default, but does not start it through the runlevel mechanism.
However, grml has an nvidia package suitable for their kernel, so I had hardware accelerated 3D out of the box. Warning: playing Wingcommander Privateer Remake kills your time faster than you can kill Pirates or Kilrathis :)

Yesterday I installed grml's 2.6.17 kernel packages, including an updated nvidia package and, after reboot, found that <a href="http://www.michael-prokop.at/">Mika</a> has added ipw3945 support to their kernel, as well as packaged the additional userspace daemon and firmware.

Therefore I declare Mika my personal laptop experience super hero!
<!--break-->