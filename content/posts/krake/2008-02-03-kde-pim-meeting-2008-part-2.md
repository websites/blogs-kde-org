---
title:   "KDE PIM Meeting 2008 - Part 2"
date:    2008-02-03
authors:
  - krake
slug:    kde-pim-meeting-2008-part-2
---
Meanwhile I have switched the Cafe to one inside the security area and from Cappuchino to Schneider's Weiße :)

One import think about KDE developer meetings is that they are not only about getting work done, but also about getting to know each other better.

Additionally to the usual "mapping faces to IRC nicks" happenings which one can also experience at our big events like Akademy, this is also about meeting people who build successful businesses upon our work, in this case the people from Intevation.

Aside from hosting the event at their office and sponsoring our dinner on Friday (including many beers ;)) they are totally cool guys and talking with them is both enjoyable and enlightening. In a world where vendors of proprietary products dominate the view customers have on software, it is an exceptional achievement to be a successful competitor based on Free Software.
<!--break-->
