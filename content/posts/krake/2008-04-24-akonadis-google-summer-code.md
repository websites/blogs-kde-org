---
title:   "Akonadi's Google Summer of Code"
date:    2008-04-24
authors:
  - krake
slug:    akonadis-google-summer-code
---
Other mentors have already blogged about their GSoC projects, so I am going to do the same for Akonadi.

Basically Akonadi got three slots from KDE and one from <a href="http://www.openchange.org">OpenChange</a>.

Lets start with the one from OpenChange: Brad Hards will be mentoring student Alan Alvarez, who's goal is to implement an Akonadi resource based on the OpenChange <a href="http://wiki.openchange.org/index.php/Category:MAPI_Library">MAPI library</a>.
If he succeeds it allow Akonadi and thus all Akonadi-enabled clients, to access mails, contacts and calendar items stored on a Microsoft Exchange server, using the same access mechanism as Micrsoft Outlook, i.e. not requiring that the Exchange administrator enables any additional transport such as <a href="http://en.wikipedia.org/wiki/Outlook_Web_Access">OWA</a>

The first of the Akonadi GSoC projects hosted by KDE is a RSS framework for Akonadi, where student Dmitry Ivanov is mentored by <a href="http://akregator.kde.org/">Akregator</a> maintainer Frank Osterfeld. 
The goal of this project is to have a central service for aggregating news feeds, so applications like Akregator or KNewsTicker can access the same data without requiring them to implement article transfer and storage multiple times.

The second project is by student Igor Trindade Oliveira and he will be mentored by me.
Igor's plan is to implement a test framework for Akonadi, so that resource developers like Alan or Dmitry, <a href="http://www.mailody.net/">application</a> developers like Tom Albers and the Akonadi team members can more easily run tests on their creations.
Currently running reproducible test scenarios requires to manually setup a clean test environment, manually start the required set of processes, manually manipulating files or remote storage, manually... (I guess you get the picture).

One of the probably most interesting parts from the point of view of a larger audience will be the possibilty to script Akonadi data processing through a Kross bridge, which might also be useful for developers and power users of Akonadi PIM applications.

The third Akonadi related project is officially about modernizing <a href="http://cvs.codeyard.net/kpilot/">KPilot</a> where student Bertjan Broeksema will be working under guidance of mentor Jason Kasper. I am blogging about this because syncing of PIM data will using Akonadi on the sync's desktop side.

Like all other KDE subprojects we got a lot more good proposals than we had GSoC slots for, for example three very good applications for implementing an Akonadi resource which uses Google's data API to transfer data from/to Google's web-based PIM apps.
<!--break-->
