---
title:   "Osnabrück PIM Meeting 2010"
date:    2010-01-12
authors:
  - krake
slug:    osnabrück-pim-meeting-2010
---
It's that the year again when KDE PIM developers attend the annual meeting in Osnabrück, traditionally hosted by Intevation, one of the companies which continously excels in acquiring funding for KDE related development.

Tom already <a href="http://www.omat.nl/2010/01/10/kde-pim-meeting-8-day-1-2-3/">blogged</a> about it and there will be a Dot article as usual.

Unlike other developer meetings such as the Akonadi sprints, the Osnabrück meeting doesn't involve a lot of hacking, for some of use no hacking at all.
It has more a strategic character, i.e. where we evaluate where we stand and what we want to achieve over the next year(s).

There are a lot of awesome developments in KDE PIM space about to unfold in the coming year, proving the value of Free Software for tasks which involve personal data, secure communication, privacy, etc. and that this does not necessarily come at the expense of stability, professionality, or even "bling".

Speaking of bling: PIM is usually not something anyone would associate with bling, especially since the underlying technologies and standards are sometimes hard to understand, old, broken but necessary for compatibility, etc.

However, through the introduction of Akonadi, we now remove these obstacles from the application level, opening it for developers who are more interested in doing new and cool alternative user interfaces.

We will keep working on the traditional applications because there are a lot of user who prefer those, but this shouldn't stop e.g. developers with a Plasma background to do a fully Plasma based PIM experience.
If this sounds like something you want to try, we certainly can and will help you with low level parts such as data engines or services.
<!--break-->
