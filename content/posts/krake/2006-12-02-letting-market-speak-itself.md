---
title:   "Letting the market speak for itself"
date:    2006-12-02
authors:
  - krake
slug:    letting-market-speak-itself
---
I'm sure every reader of this blog has at least once encountered the myth that Linux users, or more generally users of free software platforms, would not consider spending money on software.

Usually this myth is used as a convenient excuse either not to port some application to Linux or not to develop a multiplatform product in the first case.

Since we can only speculate how this myth has been created, I'll leave this to our philosphers :)

On Friday I discovered a new (to me) way to counter it: a company pledging to create a Linux version of a game if they get enough pre-orders.

The company I am talking about is <a href="http://www.rune-soft.com/index.html">Runesoft</a> (sorry, their website unfortunately <b>requires</b> JavaScript) and the game in question is <a href="http://www.ankh-game.com/">ANKH</a>, an adventure game.

The number of required <a href="http://www.ixsoft.co.uk/cgi-bin/web_store.cgi?ref=Products/de/RSANKH01DV.html">pre-orders</a> (German-only site for now) is remarkably low: 200 (in words two hundred). According to the publisher's feedback there are already over 100 pre-orders registered (one from me too), so I'll wager they'll get to the needed number quite soon :)

I'd wish more companies would do something like this before ignoring the market. I am sure market studies are all nice and shiny, but asking the consumers directly on a case-by-case basis looks far more accurate to me.
<!--break-->
