---
title:   "KApplication dependency sucks"
date:    2004-08-08
authors:
  - krake
slug:    kapplication-dependency-sucks
---
Caution, rant ahead!

I started working on an extension library for Qt applications which should enable them to use service or features offered by desktop APIs without creating link time dependencies on the respective desktop libs. http://www.sbox.tugraz.at/home/v/voyager/qds/index.html

My special interest was to have access to KRun and the KIO functionality under KDE. Latter for pure coolness, first for things like "launching the default browser".

All went fine at first, I managed to hack some framework stuff that also compiled and ran on Windows.

But when attempting to use KRun from a Qt application I failed miserably.
KRun requires that a KApplication instance exists!

I had a quick look at the relevant sources to see if this could be worked around by forking some code but then decided to tackly KIO first.
KIO is even worse. One would  have to reimplement almost all of the client side code of KIO.

So I ended up with a hack: the KDE plugin has a property telling the application that it needs its own QApplication subclass running and can create one on request.
This effectivly restricts the usefulness of QDS on KDE to applications which don't require own QApplication subclasses.
Granted this should be the majority of apps, never the less it makes the library handling artificially complicated.


