---
title:   "Other integration fronts: GNU Classpath"
date:    2005-08-31
authors:
  - krake
slug:    other-integration-fronts-gnu-classpath
---
Over on <a href="http://planet.classpath.org/">Planet Classpath</a> the people blog about the ongoing
<a href="http://www.advogato.org/person/robilad/diary.html?start=73">efforts</a> and <a href="http://www.advogato.org/person/jserv/diary.html?start=2">success</a> with their <a href="http://www.kaffe.org/screenshots_qte.shtml">Qt based AWT peers</a>.

Nice to see that the high quality of Qt's code on all its platforms get more widespread acknowledgement outside the KDE area.
<!--break-->