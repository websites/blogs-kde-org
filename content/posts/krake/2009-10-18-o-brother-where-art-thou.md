---
title:   "O Brother, Where Art Thou?"
date:    2009-10-18
authors:
  - krake
slug:    o-brother-where-art-thou
---
Not at the Akonadi sprint. Shame!
(You can find Tom's blog about it <a href="http://www.omat.nl/2009/10/17/akonadi-meeting-day-1-discussions-api-review/">here</a> and <a href="http://www.omat.nl/2009/10/17/akonadi-meeting-day-2-hard-work/">here</a>)

Missing out on the API discussions would have been already bad enough, missing the presentation of my GSoC student and not getting to meet Brad Hards sucks.

Anyway. Despite not having fun^Wto do a lot of work in Berlin, I still have something Akonadi related to blog about.

The next couple of months I'll be working with a group of students from <a href="http://en.wikipedia.org/wiki/Paul_Sabatier_University">Paul Sabatier University, Toulouse, France</a> under a program initiated several years ago by The Other Kevin (also knowns ervin and, to some lesser extent, as Kevin Ottens :)).

One of the nice things about Akonadi is its separation of concern. Developers can afford to concentrate on one specific area of interest such as providing awesome user interfaces or providing access to systems not designed to be used by desktop software or only one of a specific vendor.

Our French collegues are working on one of the latter, the Akonadi Forum Resource.

The concept behind this project is that we currently have a communication devide, a gap between two major groups in our community (users and developers) due to different preferences regarding communication channels.

So our idea is to build a bridge between two channels commonly used by each group: web base forum and (plain) text based newsgroups.
Users like the <a href="http://forum.kde.org/index.php">forum</a> because it looks good, is convenient and accessible from any computer with an Internet connection.
Developers prefer using specialized software with features such as scoring (hide/show messages based on extensive rules) and loathe to wait for "unnecessary stuff" (e.g. fancy buttons, graphical emoticons, advances stylesheets) to be downloaded and processed.

The Akonadi Forum Resource is an attempt to develop a solution for this differences, by letting the developers (or anyone else with similar preferences) participate in the forum through newsgroup capable desktop software.

It will map the forum structure as newsgroup folders, forum topics as newsgroup threads and forum comments as newsgroup postings.
Depending on available time we are also considering access to forum personal messages and member and/or buddy lists.

That's all for now, stay tuned.
<!--break-->
