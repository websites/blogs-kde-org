---
title: Power, input & people at Akademy 2024
authors:
  - jpetso
date: 2024-09-18
SPDX-License-Identifier: CC-BY-SA-4.0
---

Contrary to popular belief, [Akademy 2024](https://akademy.kde.org/2024/) was not my first Akademy.
KDE seems to keep tagged faces from Akademy group photos around, so I stumbled over some vintage
pictures of me in 2006 (Glasgow) and 2007 (Dublin). At the time, I was an utter greenhorn with
big dreams, vague plans, and a fair bit of social anxiety.

![Dublin is where Aaron Seigo taught me how to eat with chopsticks. I continue to draw from that knowledge to this day.](/images/2024-09-18.jpetso-akademy-2006-2007.png)

And then I disappeared for 15 years, but now it's time for a new shot. This time around, I'm a
little less green (rather starting to grey a little) and had a surprising amount of stuff to
discuss with various KDE collaborators. Boy, is there no end of interesting people and
discussion topics to be had at Akademy.


## "Oh, you're the PowerDevil guy"

[You're not wrong](https://blogs.kde.org/2024/04/23/powerdevil-in-plasma-6.0-and-beyond/), I've
been [contributing to that](https://blogs.kde.org/2024/09/04/brightness-controls-for-all-your-displays/)
for the past year. As such, one highlight for me was to meet KDE's hardware integration contractor
Natalie Clarius in person and sync up on all things power-related.

![Akademy's no-photo badge makes its wearer disappear from selfies. AI magic, perhaps?](/images/2024-09-18.akademy-2024-nclarius-jpetso.jpg)

Natalie presented a [short talk](https://conf.kde.org/event/6/contributions/212/) and hosted a
BoF session ("Birds of a Feather", a.k.a. workshop) about power management topics. We had a good
crowd of developers in attendance, clearing up the direction of several outstanding items.

Power management in Plasma desktops is in decent shape overall. One of the bigger remaining topics
is (re)storing battery charge limits across reboots, for laptops whose firmware doesn't remember
those settings. There is a way forward that involves making use of the cross-desktop UPower service
and its new charge limit extensions. This will give us the restoring feature for free,  but we
have to add some extra functionality to make sure that charge threshold customization remains
possible for Plasma users after switching over.

We also looked at ways to put systems back to sleep that weren't supposed to wake up yet.
Unintended wake-ups can happen e.g. when the laptop in your backpack catches a key press from the
screen when it's squeezed against the keyboard. Or when one of those (conceptually neat)
"Modern Standby" implementations on recent laptops are buggy. This will need a little more
investigation, but we've got some ideas.

I talked to Bhushan Shah about power saving optimizations in Plasma Mobile. He is investigating
a Linux kernel feature designed for mobile devices that saves power more aggressively, but needs
support from KDE's power management infrastructure to make sure the phone will indeed wake up
when it's meant to. If this can be integrated with KDE's power management service, we could improve
battery runtimes for mobile devices and perhaps even for some laptops.

The friendly people from Slimbook dropped by to show off their range of Linux laptops, and unveiled
[their new Slimbook VI with KDE neon](https://slimbook.com/en/shop/product/kde-slimbook-vi-amd-ryzen-7-8845hs-1467?category=58)
right there at the conference. Compared to some of their older laptops, build quality is improved
leaps and bounds. Natalie and I grilled their BIOS engineer on topics such as power profiles,
power consumption, and how to get each of their function keys show the corresponding OSD popup.

![KDE Slimbook VI shortly after the big reveal](/images/2024-09-18.akademy-2024-slimbook.jpg)


## "I'm excited that your input goal was chosen"

Every two years, the KDE community picks three "Goals" to rally around until the next vote happens.
This time, contributors were asked to form teams of "goal champions" so that the work of educating
and rallying people does not fall on the shoulders of a single poor soul per goal.

So now we have eight poor souls who pledge to advance a total of
[three focus areas over the next two years](https://blogs.kde.org/2024/09/07/kde-goals-a-new-cycle-begins/).
Supported by KDE e.V.'s new Goals Coordinator, Farid. There's a common thread around attracting
developers, with Nicolas Fella and Nate Graham pushing for a
["Streamlined Application Development Experience"](https://phabricator.kde.org/T17396) and
the KDE Promo team with a systematic recruitment initiative titled
["KDE Needs You"](https://phabricator.kde.org/T17439). And then there's this other thing,
with a strict end user focus, briefly introduced on stage by guess who?

![Yup. Hi! I'm the input guy now.](/images/2024-09-18.akademy-2024-goal-announcement.jpg)

Turns out a lot of people in KDE are passionate about support for input devices, virtual keyboards
and input methods. Gernot Schiller (a.k.a. duha) realized this and assembled a team consisting of
himself, Joshua Goins (a.k.a. [redstrate](https://redstrate.com/)) as well as Yours Truly to
apply as champions. The goal proposed that
["We care about your Input"](https://phabricator.kde.org/T17433) and the community's response
is Yes, Yes We Do.

As soon as the new goals were announced, Akademy 2024 turned into an Input Goal Akademy for me.
In addition to presenting the new goal on stage briefly, we also gathered in a BoF session
to discuss the current state, future plans and enthusiastic volunteering assignments related to
all things input. I also sat down with a number of input experts to learn more about everything.
There is still much more I need to learn.

It's a sprawling topic with numerous tasks that we want to get done, ranging from multi-month
projects to fixing lots of low-hanging fruit. This calls for a series of dedicated blog posts,
so I'll go into more detail later.

Join us at [#kde-input:kde.org on Matrix](https://matrix.to/#/#kde-input:kde.org)
or watch this space (and [Planet KDE](https://planet.kde.org/) in general) for further posts
on what's going on with input handling in KDE.


## Look at the brightness side

KWin hacker extraordinaire Xaver Hugl (a.k.a. zamundaaa) demoed some
[of his color magic](https://conf.kde.org/event/6/contributions/221/) on a
standard SDR laptop display. Future KWin can play bright HDR videos in front of regular SDR
desktop content. Accurate color transformations for both parts without special HDR hardware,
that's pretty impressive. I thought that HDR needs dedicated hardware support, turns out I'm wrong,
although better contrast and more brightness can still improve the experience.

I also got to talk to Xaver about touchpad gestures, learned about stalled attempts to support
DDC/CI in the Linux kernel directly, and pestered him for a review to improve Plasma's D-Bus API
for the new per-monitor brightness features. Also the
[XDC conference in Montreal](https://indico.freedesktop.org/event/6/page/29-attending-xdc-2024),
is happening in less than a month, featuring more of Xaver as well as loads of low-level graphics
topics. Perhaps even input-related stuff. It's only a train ride from Toronto, maybe I'll drop by.
Maybe not. Here's a medieval German town selfie.

![Towering over the rooftops of Rothenburg ob der Tauber with Xaver, Jonathan Riddell, and two suspect KWin developers in the back](/images/2024-09-18.akademy-2024-rothenburg-jpetso.jpg)

Thanks to the entire KWin gang for letting me crash their late-night hacking session and only
throwing the last of us out at 2 AM after my D-Bus change got merged. Just in time for the
Plasma 6.2 beta. I was dead tired on Thursday, totally worth it though.


## Atomic KDE for users & developers

Plasma undoubtedly has some challenges ahead in order to bring all of its power and flexibility
to an image-based, atomic OS with sandboxed apps (i.e. Flatpak/Snap).
[David Edmundson's talk](https://conf.kde.org/event/6/contributions/205/) emphasized that
traditional plugins are not compatible with this new reality. We'll need to look into other ways
of extending apps.

![David Edmundson wildly speculating about the future](/images/2024-09-18.akademy-2024-davidedmundson-sandboxing-facts.jpg)

The good news is that lots of work is indeed happening to prepare KDE for this future.
Baloo making use of thumbnailer binaries in addition to thumbnailer plugins. KRunner allowing D-Bus
plugins in addition to shared libraries.
[Arjen Hiemstra's work-in-progress Union style](https://conf.kde.org/event/6/contributions/206/) being
customizable through configuration rather than code. Heck, we even learned about a project called
[KDE Neon Core](https://conf.kde.org/event/6/contributions/220/) trying to make a Snap out of each
and every piece of KDE software.

Going forward, it seems that there will be a more distinct line between Plasma as a desktop platform
and KDE apps, communicating with each other through standardized extension points.

All of this infrastructure will come in handy if
[Harald Sitter's experimental atomic OS](https://conf.kde.org/event/6/contributions/202/),
KDE Linux (working title), is to become a success. Personally, I've long been hoping for a
KDE-based system that I can recommend to my less technical family members. KDE Linux could
eventually be that. Yes, Fedora Kinoite is also great.

![KDE Linux: Useful to users, hardware partners, and... developers?](/images/2024-09-18.akademy-2024-sitter-kde-linux-goals.jpg)

What took me by surprise about Harald's presentation was that it could be great even as a
development platform for contributing to the Plasma desktop itself.

As a desktop developer, I simply can't run my Plasma development build in a container.
Many functions interact with actual hardware so it needs to run right on the metal. On my current
Arch system, I use a secondary user account with Plasma installed into that user's home directory.
That way the system packages aren't getting modified - one does not want to mess with system packages.

But KDE Linux images contain the same system-wide build that I would make for myself.
I can build an exact replacement with standard KDE tooling, perhaps a slight bit newer,
and temporarily use it as system-wide replacement using systemd-sysext. I can revert whenever.
KDE Linux includes all the development header files too, making it possible to build and replace
just a single system component without building all the rest of KDE.

Different editions make it suitable for users anywhere between tested/stable (for family members)
and bleeding edge (for myself as Plasma developer). Heck, perhaps we'll even be able to switch
back and forth between different editions with little effort.

Needless to say, I'm really excited about the potential of KDE Linux. Even without considering
how much work it can save for distro maintainers that won't have to combine outdated Ubuntu LTS
packages with the latest KDE desktop.


## Conclusion

There's so much else I've barely even touched on, like
[NLnet funding opportunities](https://conf.kde.org/event/6/contributions/209/), quizzing Neal Gompa
about KDE for Enterprise, [Rust](https://conf.kde.org/event/6/contributions/203/) and
[Python](https://conf.kde.org/event/6/contributions/208/) binding efforts, Nicolas Fella being
literally everywhere, Qt Contributor Summit, finding myself in a hostel room together with fellow
KDE devs Carl & Kåre. But this blog post is already long enough.
Read some of the [other KDE blogs](https://planet.kde.org/) for more Akademy reports.

![German bus stops have the nicest sunsets. Also rainbows!](/images/2024-09-18.akademy-2024-jpetso-goodbye.jpg)

Getting home took all day and jet lag isn't fun, but I've reasonably recovered to give another shot
at bringing KDE software to the masses. You can too!
[Get involved](https://community.kde.org/Get_Involved), [donate to KDE](https://kde.org/donate/),
or simply enjoy the ride and discuss this post
[on KDE Discuss](https://discuss.kde.org/t/power-input-people-at-akademy-2024/21496).

Or don't. It's all good :)
