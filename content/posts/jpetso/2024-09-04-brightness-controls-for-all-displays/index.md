---
title: Brightness controls for all your displays
authors:
  - jpetso
date: 2024-09-04
SPDX-License-Identifier: CC-BY-SA-4.0
---

Whoops, it's already been months [since I last blogged](/2024/04/23/powerdevil-in-plasma-6.0-and-beyond/). I've been actively involved with Plasma and especially its power management service PowerDevil for over a year now. I'm still learning about how everything fits together.

Turns out though that a little bit of involvement imbues you with just enough knowledge and confidence to review other people's changes as well, so that they can get merged into the next release without sitting in limbo forever. Your favorite weekly blogger for example, [Nate Graham](https://pointieststick.com/), is a force of nature when it comes to responding to proposed changes and finding a way to get them accepted in one form or another. But it doesn't have to take many years of KDE development experience to provide helpful feedback.

Otfen we simply need another pair of eyes trying to understand the inner workings of a proposed feature or fix. If two people think hard about an issue and agree on a solution, chances are good that things are indeed changing for the better. Three or more, even better. I do take pride in my own code, but just as much in pushing excellent improvements like these past the finish line:

* Fabian Arndt's [support for Lenovo laptops' battery conservation mode](https://invent.kde.org/plasma/powerdevil/-/merge_requests/248) (Plasma 6.1)
* Natalie Clarius's feature to [block apps from inhibiting sleep and screen locking](https://pointieststick.com/2024/08/30/this-week-in-plasma-inhibiting-inhibitions-and-more/) (6.2)
* Christoph Wolk's [improvements to usability for the Power Management settings page](https://invent.kde.org/plasma/powerdevil/-/merge_requests/402) (6.2, one among many of the sort)
* Nate's changes to [show the current power profile in addition to the battery charge](https://invent.kde.org/plasma/powerdevil/-/merge_requests/399) (6.2)
* Xaver Hugl's push to allow [software brightness adjustments not just for HDR displays](https://zamundaaa.github.io/wayland/2024/05/11/more-hdr-and-color.html) (6.1), but indeed for any display without hardware brightness controls (6.2)

In turn, responsible developers will review your own changes so we can merge them with confidence. Xaver, Natalie and Nate invested time into getting my big feature merged for Plasma 6.2, which [you've already read about](https://pointieststick.com/2024/08/23/this-week-in-kde-per-monitor-brightness-control-and-update-then-shut-down/):

## Per-display Brightness Controls

![Nate's screenshot is nice and hi-res, I'll just reuse it here](https://pointieststick.com/wp-content/uploads/2024/08/image-18.png)

This was really just a follow-up project to the [display support developments mentioned in the last blog post](/2024/04/23/powerdevil-in-plasma-6.0-and-beyond/). I felt it had to be done, and it lined up nicely with what KWin's been up to recently.

So how hard could it be to add another slider to your applet? Turns out there are indeed a few challenges.

In KDE, we like to include new features early on and tweak them over time. As opposed to, say, the GNOME community, which tends to discuss them for a loooong time in an attempt to merge the perfect solution on the first try. Both approaches have advantages and drawbacks. Our main drawback is harder to change imperfect code, because it's easy to break functionality for users that already rely on them.

Every piece of code has a set of assumptions embedded into it. When those assumptions don't make sense for the next, improved, hopefully perfect solution (definitely perfect this time around!) then we have to find ways to change our thinking. The code is updated to reflect a more useful set of assumptions, ideally without breaking anyone's apps and desktop. This process is called "refactoring" in software development.

But let's be specific: What assumptions am I actually talking about?

#### There is one brightness slider for your only display

This one's obvious. You can use more than just one display at a time. However, our previous code only used to let you read one brightness value, and set one brightness value. For which screen? Well... how about the code just picks something arbitrarily. If you have a laptop with an internal screen, we use that one. If you have no internal screen, but your external monitor supports DDC/CI for brightness controls, we use that one instead.

What's that, you have multiple external monitors that all support DDC/CI? We'll set the same value for all of them! Even if the first one counts from 0 to 100 and the second one from 0 to 10.000! Surely that will work.

No it won't. We only got lucky that most monitors count from 0 to 100.

The solution here is to require all software to treat each display differently. We'll start watching for monitors being connected and disconnected. We tell all the related software about it. Instead of a single set-brightness and a single get-brightness operation, we have one of these per display. When the lower layers require this extra information, software higher up in the stack (for example, a brightness applet) is forced to make better choices about the user experience in each particular case. For example, presenting multiple brightness sliders in the UI.

#### A popup indicator shows the new brightness when it changes

So this raises new questions. With only one display, we can indicate any brightness change by showing you the new brightness on a percentage bar:

![OSD indicator with percentage bar, but no display name](/images/2024-09-04.brightness-osd-no-text.png)

Now you press the "Increase Brightness" key on your keyboard, and multiple monitors are connected. This OSD popup shows up on... your active screen? But did the brightness only change for your active screen, or for all of them? Which monitor is this one popup representing?

Ideally, we'd show a different popup on each screen, with the name of the respective monitor:

![OSD indicator with percentage bar plus display name. Early screenshot, the final version uses the same small icon as the original popup](/images/2024-09-04.brightness-osd-with-text.png)

That's a good idea! But Plasma's OSD component doesn't have a notion of different popups being shown at the same time on different monitors. It may even take further changes to ask KWin, Plasma's compositor component, about that. What we did for Plasma 6.2 was to provide Plasma's OSD component with all the information it needs to do this eventually. But we haven't implemented our favorite UI yet, instead we hit the 6.2 deadline and pack multiple percentages into a single popup:

![OSD indicator with display names and percentages both as text](/images/2024-09-04.brightness-osd-multi-text.png)

That's good enough for now, not the prettiest but always clear. If you only use or adjust one screen, you'll get the original fancy percentage bar you know and love.

#### The applet can do its own brightness adjustment calculations

You can increase or decrease brightness by scrolling on the icon of the "Brightness and Color" applet with your mouse wheel or touchpad. Sounds easy to implement: read the brightness for each display, add or subtract a certain percentage, set the brightness again for the same display.

Nope, not that easy.

For starters, we handle brightness key presses in the background service. You'd expect the "Increase Brightness" key to behave the same as scrolling up with your mouse wheel, right? So let's not implement the same thing in two different places. The applet has to say goodbye to its own calculations, and instead we add an interface to background service that the applet can use.

Then again, the background service never had to deal with high-resolution touchpad scrolling. It's so high-resolution that each individual scroll event might be smaller than the number of brightness steps on your screen. The applet contained code to add up all of these tiny changes so that many scroll events taken together will at least make your screen change by one step.

Now the service provides this functionality instead, but it adds up the tiny changes for each screen separately. Not only that, it allows you to keep scrolling even if one of your displays has already hit maximum brightness. When you scroll back afterwards, both displays don't just count down from 100% equally, but the original brightness difference between both screens is preserved. Scroll up and down to your heart's content without messing up your preferred setup.

#### Dimming will turn down the brightness, then restore the original value later

Simple! Yes? No. As you may guess, we now need to store the original brightness for each display separately so we can restore it later.

But that's not enough: What if you unplug your external screen while it's dimmed? And then you move your mouse pointer again, so the dimming goes away. Your monitor, however, was not there for getting its brightness restored to the original value. Next time you plug it in, it starts out with the dimmed lower brightness as a new baseline, Plasma will gladly dim even further next time.

Full disclosure, this was already an issue in past releases of Plasma and is still an issue. Supporting multiple monitors just makes it more visible. [More work is needed](https://invent.kde.org/plasma/powerdevil/-/issues/38) to make this scenario bullet-proof as well. We'll have to see if a small and safe enough fix can still be made for Plasma 6.2, or if we'll have to wait until later to address this more comprehensively.

Anyway, these kind of assumptions are what eat up a good amount of development time, as opposed to just adding new functionality. Hopefully users will find the new brightness controls worthwhile.

## So let's get to the good news

[Your donations](https://kde.org/donate/) allowed KDE e.V. to approve a travel cost subsidy in order to meet other KDE contributors in person and scheme the next steps toward world domination. You know what's coming, I'm going to:

[![Akademy 2024: 7th to 12th September 2024 in Würzburg, Germany](https://akademy.kde.org/media/2024/banners/akademy2024-banner-600x110.png)](https://akademy.kde.org/2024/)

Akademy is starting in just about two days from now! Thank you all for allowing events like this to happen, I'll try to make it count. And while not everyone can get to Germany in person, keep in mind that it's a hybrid conference and especially the [weekend talks](https://akademy.kde.org/2024/program/) are always worth watching online. You can still [sign up](https://akademy.kde.org/2024/) and join the live chat, or take a last-minute weekend trip to Würzburg if you're in the area, or just watch the videos shortly afterwards (I assume they'll go up on [the PeerTube Akademy channel](https://tube.kockatoo.org/c/akademy/videos)).

I'm particularly curious about the outcome of the KDE Goals vote for the upcoming two years, given that I [co-drafted a goal proposal](https://phabricator.kde.org/T17433) this time around. Whether or not it got elected, I haven't forgotten about my promise of working on mouse gesture support on Plasma/Wayland. Somewhat late due to the aforementioned display work taking longer. Other interesting things are starting to happen as well on my end. I'll have to be mindful not to stretch myself too thinly.

Thanks everyone for being kind. I'll be keeping an eye out for [your bug reports](https://bugs.kde.org/) when the Plasma 6.2 Beta gets released to adventurous testers in just over a week from today.

Discuss this post [on KDE Discuss](https://discuss.kde.org/t/brightness-controls-for-all-your-displays/20900).
