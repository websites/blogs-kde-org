---
title:   "Call for students in Summer of Code"
date:    2006-05-03
authors:
  - thiago
slug:    call-students-summer-code
---
I guess you all already knew about it, but just in case you didn't:

KDE is participating again in the Google Summer of Code. So, submit all of those ideas that you had but never had had the courage to start working on!

Please remember: the deadline is May 8th. So you have to start working now if you want to participate.

Our ideas list page: <a href="http://wiki.kde.org/tiki-index.php?page=KDE+Google+SoC+2006+ideas">http://wiki.kde.org/tiki-index.php?page=KDE+Google+SoC+2006+ideas</a>

The subscription page: <a href="http://code.google.com/soc/student_step1.html">http://code.google.com/soc/student_step1.html</a>

Happy hacking everyone!
<!--break-->