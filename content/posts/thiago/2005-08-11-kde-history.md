---
title:   "KDE in History"
date:    2005-08-11
authors:
  - thiago
slug:    kde-history
---
I've found this gem while searching for some old contributors to KDE:<br>
Enjoy www.kde.org in 1998: <a href="http://sunsite.bilkent.edu.tr/pub/linux/www.kde.org/">http://sunsite.bilkent.edu.tr/pub/linux/www.kde.org/</a>.

See specially <a href="http://sunsite.bilkent.edu.tr/pub/linux/www.kde.org/announce-1.0.html">this announcement</a>.
<!--break-->