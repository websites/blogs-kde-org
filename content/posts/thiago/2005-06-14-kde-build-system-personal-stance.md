---
title:   "KDE build system - personal stance"
date:    2005-06-14
authors:
  - thiago
slug:    kde-build-system-personal-stance
---
As you may have noticed, Chris Lee started a <a href="http://c133.org/blog/tech/KDE/build_system_notes.html">blog</a> and a <a href="http://lists.kde.org/?t=111863688400002&r=1&w=2&n=38">thread on kde-core-devel</a> about KDE's next build system. I was there on IRC when the discussion spontaneously started --- as usual, it must have been someone ranting about automake/autoconf/libtool.

I'd like to give my input in the process. I'll reply to the thread soon with my own ideas as well.
<!--break-->
We've been using automake, autoconf and libtool since KDE first started, as must be every traditional OSS project out there. The tools provide a lot of flexibility and support for a number of platforms. This is not to complain about them: I want to congratulate the developers in all three tools for the work they've done to us all.

However, I think those tools are past their usefulness now. KDE is a large and quite complex project, written in C++ and supported only in few configurations. We don't support old, pre-ANSI compilers; we don't support old Linux a.out binaries; support for some of the Unix is also quite hard to come through. Given all that, automake, autoconf and libtool become bloated: we don't need code in them to support those systems. What's more, the fact that libtool (for instance) is written in plain /bin/sh script makes it hard for us to maintain our local changes and to make the necessary modifications.

We're facing more and more problems due to those tools. Some developers have said they spend most of their time fixing the buildsystem, instead of actually working on porting code. This tells a lot about the state of our buildsystem: we've got hack upon hack to make automake behave as we want it to (read: <tt>admin/am_edit</tt>).

We've even got a replacement for automake! <a href="https://blogs.kde.org/blog/124">Coolo's</a> <a href="http://websvn.kde.org/trunk/kdenonbeta/unsermake">unsermake</a> is a fine tool, written in Python. Originally, it was developed to replace automake and create Makefile.ins, without the need for the am_edit hack, to support .moc, .uic files, kdeinit objects, etc. Now, it also replaces make itself, adding nice features that make doesn't have.

I've been running unsermake since I've first heard about it, I don't regret it and I even recommend it for non-KDE projects. Output is concise, and even sports a progress counter, which integrates nicely with <a href="http://grammarian.homelinux.net/~mpyne/weblog/">mpyne's</a> kdesvn-build (now part of kdesdk).

Tomorrow I'll try to put together my thoughts on what the ideal buildsystem would be. I know we'll never get the ideal, but I'd like to share my thoughts and hear some feedback.

<small>Yeah, I know I owe you all an account of FISL</small>