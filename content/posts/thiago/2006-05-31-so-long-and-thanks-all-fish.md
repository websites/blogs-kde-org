---
title:   "So long and thanks for all the fish..."
date:    2006-05-31
authors:
  - thiago
slug:    so-long-and-thanks-all-fish
---
With <a href="http://websvn.kde.org/trunk/KDE/kdelibs/?rev=546830&view=rev">commit 546830</a>, KDE says good-bye to one of its longest friends: DCOP. The technology has served us well for 6 years, to the point that has become one of our most proeminent features. Many KDE applications are given an edge over their competitors by supporting advanced functionality through DCOP: you can tell a Konqueror page to evaluate a JavaScript code snippet (think <tt>document.write</tt>...), tell a Kate window to raise itself, Kontact to check email or Kopete to send an automated message, etc.

We now say hello to DCOP's younger brother: <a href="http://www.freedesktop.org/wiki/Software/dbus">D-BUS</a>. I <a href="http://websvn.kde.org/trunk/KDE/kdelibs/?rev=546826&view=rev">merged</a> this morning all the changes I had in a separate branch back into trunk. This completes one phase of the work and starts a new one!

D-BUS brings us better interoperability with many other programs. While DCOP was pretty much restricted to KDE applications (yes, I know there were C bindings, but not many people used it...), D-BUS already comes with bindings for several other major frameworks: glib, Java, Python, Perl, Mono, etc. D-BUS has been designed from the ground up to be an interoperable IPC system and also to <a href="http://dbus.freedesktop.org/doc/dbus-faq.html#dcop">replace DCOP</a> when the time came. And so it did.

D-BUS also allows us to better talk to our own system: projects like <a href="http://freedesktop.org/wiki/Software/hal">HAL</a> and <a href="http://www.freedesktop.org/wiki/Software/Avahi">Avahi</a> are already being used by many Linux distributions to let normal applications get access to some privileged resources. In time, I also hope the <a href="http://portland.freedesktop.org">Portland Project</a> to come around and use D-BUS for its IPC needs, thus freeing us from using a special library with its own protocol to do what D-BUS already does.

You may have noticed a pattern in the links above: all are "freedesktop.org". So, yes, KDE is collaborating with the free desktop initiative. But D-BUS isn't restricted to freedesktop.org projects! In case you didn't know, the Nokia 770s use D-BUS extensively for its own internal IPC. There's also the <a href="http://tapioca-voip.sourceforge.net/wiki/index.php/Tapioca">Tapioca</a> Project using D-BUS. Those are just a few: there are many <a href="http://www.freedesktop.org/wiki/Software_2fDbusProjects">more</a> (and that list is far from complete).

And, of course, <a href="http://www.kde.org">KDE</a> now. (We probably bring more applications into D-BUS in one go than there currently are...)

Many thanks to Simon Hausmann, Harald Fernengel, Kévin Ottens, Benjamin Meyer and Roberto Raggi for helping me with the KDE Libs port to D-BUS. And many thanks to <a href="http://www.trolltech.com">Trolltech</a> for letting me develop and maintain the <a href="http://websvn.kde.org/trunk/kdesupport/qt-dbus">QtDBus bindings</a>.

<!--break-->