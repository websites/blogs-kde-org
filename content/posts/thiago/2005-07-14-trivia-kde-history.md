---
title:   "Trivia on KDE History"
date:    2005-07-14
authors:
  - thiago
slug:    trivia-kde-history
---
On Jan 4th, 1999, the <a href="http://websvn.kde.org/trunk/KDE/kde-common/accounts">kde-common/accounts</a> file was created by <a href="https://blogs.kde.org/blog/124">Stephan Kulow</a>, but the idea is attributed to David Faure (no blog).

After <a href="http://lists.kde.org/?l=kde-commits&m=91548463225279&w=2">threatening removal of accounts</a> for people who didn't report in, the kde-common/accounts file was created with 180 names.

If you look at that file's history, you can find this gem:

<pre>
------------------------------------------------------------------------
r14836 | coolo | 1999-01-04 20:41:46 -0200 (Seg, 04 Jan 1999) | 4 lines

more people - Did you know that one guy in America called his daughter
"EinenUniversalschluesselindieComputerderBuerokratiewerfen"? (Had
to think about it while adding mhk ;-)

------------------------------------------------------------------------
</pre>
(<a href="http://lists.kde.org/?l=kde-commits&m=91548995328032&w=2">http://lists.kde.org/?l=kde-commits&m=91548995328032&w=2</a>)

Today there are 1138 accounts in KDE.
<!--break-->