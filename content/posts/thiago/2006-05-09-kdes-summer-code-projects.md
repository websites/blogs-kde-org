---
title:   "KDE's Summer of Code Projects"
date:    2006-05-09
authors:
  - thiago
slug:    kdes-summer-code-projects
---
With less than 2 hours to go to submit applications, students must be busy smoothing the final edges of their texts before submitting to Google. And as it turns out, we're getting some very interesting ideas for Summer of Code this year. I hope the quality of the proposals is indicative of the quality of the code we'll see in the next three months.

But I already do have one complaint:

Why did you all wait so long to submit?!

Last week, I was really worried when we had a handful of applications only. I was worried we had not done our job getting the information out before the deadlines. Compared to last year, we received a lot less applications. In fact, by the end of the second day, we had more mentors available than applications to mentor.

But now, in retrospect, I think I can come up with at least a few good reasons why that happened:

<ol>
<li>Those are students! They have classes!</li>
<li>We had more than one week last year and I only joined the mentors well into the second week</li>
<li>We also had a lot more spam and completely clueless users last year (people who just copied & pasted our short ideas)</li>
</ol>

I'd also like to thank all the mentors and all the students participating. And I'd like to say that the KDE community welcomes all new contributors, so, even if you don't get selected, please consider trying out your idea anyways.
<!--break-->