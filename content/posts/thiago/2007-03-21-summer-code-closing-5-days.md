---
title:   "Summer of Code closing in 5 days"
date:    2007-03-21
authors:
  - thiago
slug:    summer-code-closing-5-days
---
Yesterday I <a href="http://blogs.kde.org/node/2735">blogged</a> there were 4 days left. Today there are 5 days left: Google extended the deadline to March 26th.

So you now have the weekend to finish your proposal. But I <b>still</b> would like to see submissions earlier than that.
<!--break-->