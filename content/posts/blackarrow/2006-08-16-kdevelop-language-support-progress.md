---
title:   "KDevelop language support progress"
date:    2006-08-16
authors:
  - blackarrow
slug:    kdevelop-language-support-progress
---
For those of you who might not be aware, KDevelop v4 is starting to take shape, at least behind the scenes.  For some time now we have had Roberto's new c++ parser in the code base but not doing very much.  Then Matt and Adam worked hard on improving project management support (including native support for CMake) and the background parser / code model, so now we have a functional code model (even if not in the final state).

More recently, Jakob's Java and C# parsers (along the lines of the c++ parser) have been added to the codebase.  They are just beginning to get codemodel integration.

While this was happening, I took over Roberto's work on creating definition-use chains.  These are trees which represent contexts or scopes in which definitions are made, and then links from each definition to all of its uses.  This turned out to be relatively easy once I had spoken in person to Roberto about it at kde four core in Norway (for some reason I didn't really understand what they were about when we talked online).

Yesterday, I managed to apply syntax highlighting to the katepart editor, based off the definition-use chain.  This means that kdevelop will have separate highlights for variables defined in different ways, eg. local / function / class member / global, and also unrecognised variable usage.  Here's a screenshot:

<img src="http://members.optusnet.com.au/~hamishrodda/kdev4-duchain.jpg"/>

(yes, there are bugs visible there)  This work is still quite raw and several major crashes need fixing, but this work has much potential.  I'll save the details for my next blog entry...<!--break-->