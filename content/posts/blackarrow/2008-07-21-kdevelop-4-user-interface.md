---
title:   "KDevelop 4: User Interface"
date:    2008-07-21
authors:
  - blackarrow
slug:    kdevelop-4-user-interface
---
Another leap forward in KDevelop version 4 is the interface.  We've tried to make it both highly flexible (to fit most people's requirements) and reliable (kdev3 suffered from a difficult to maintain ui library, prior to a simplified rewrite), and the current state is pretty good (certainly better than kdev3).  Features include:

<li><b>"Ideal" mode</b> - one central view, surrounded by collapsable tool views.  The tool views can be shown/hidden, switched between, maximized (one toolview takes up the whole screen temporarily), and made to auto-hide (when the editor gains focus), all via keyboard shortcuts as well as mouse clicks.  Each tool view has its own action tool bar, if it has any actions to expose.  I also recently fixed the last known focus bugs, which makes the toolviews now particularly nice to work with.</li>

<li><b>Arbitrarily splittable central view</b> - you can split vertically or horizontally as many times as you like. (Still some bugs on closing the last document in a split view to be worked on).  I've found this useful although I didn't initially think I would use it.  We still need keyboard shortcuts for switching between split areas (afaik).</li>

<li><b>Perspectives</b> - switch between different configurations for your tool views, editor windows etc. easily and quickly.  A separate perspective for different programming tasks (eg. debugging) may turn out to be particularly handy.  Details of how it's supposed to work are still being sorted out, however.</li>

<li><b>Mulitple mainwindow support</b> - although this has been the intention, it has never worked properly and needs more developer time to get it right.</li>

<br>Features likely to be implemented at some stage include split toolview areas, and drag + drop of tool views.<!--break-->