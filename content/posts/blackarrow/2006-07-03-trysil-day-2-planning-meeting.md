---
title:   "Trysil Day 2 - Planning Meeting"
date:    2006-07-03
authors:
  - blackarrow
slug:    trysil-day-2-planning-meeting
---
This evening in Trysil we had a long meeting to discuss goals for KDE 4.0, both generally and specifically.

Topics covered did not contain many surprises, most issues were already covered in the planning documents already existing in the wiki and svn.  Prioritising our time is of course important, and in rough descending order they were thought to be:
- dbus support
- compositing manager + svg support
- api cleanup
- binary compatibility policy, hiding public classes which shouldn't be public
- akonadi
- xmlgui redesign
- plasma
- hig
- kdom/khtml2
- accessibility

then many, many others, even some Zack-specific items :)

Also, it was recognised that qt 4.2 feedback is very important, so that vanilla 4.2 is ready to support kde4 once it ships.

Anyway, it's after midnight, so time for sleep...