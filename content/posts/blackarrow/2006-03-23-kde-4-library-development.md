---
title:   "KDE 4 library development"
date:    2006-03-23
authors:
  - blackarrow
slug:    kde-4-library-development
---
Well, it's been ages since I've blogged - generally I feel that code speaks louder than blogs :)  However, I think it's time to do some catching up.

Currently the rest-of-kde porting is proceeding after the massive KAction changes I recently committed to kdelibs.  Now that the snapshot carries the changes too, it's starting to get heaps of testing, which is so far standing up pretty well (only a few bugs reported to date, the significant bug already fixed).

My next major library-level change will likely be the removal of all unnecessary KAccel classes + friends.  As well as being a good cleanup and quite multi-platform friendly, I've just figured out today that it is simple for me to add the ability to configure any KAction with a global shortcut.  Soon you'll be able to configure any action you like for quick access even when your app isn't focused.  Neat :)

Well, gotta go but I've got much more to blog about, so stay tuned...<!--break-->