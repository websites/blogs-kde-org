---
title:   "On holidays again!"
date:    2006-11-12
authors:
  - blackarrow
slug:    holidays-again
---
I've made it to my second lot of annual leave this year, so I've bought a nice shiny new digital SLR (the Sony a100) and am going to Japan for two weeks for a holiday.

If there are any KDE devs or users who'd like to meet up in Japan (and preferably can speak english, as my japanese is non-existant) drop me a line... rodda a_t kde .org.