---
title:   "More KTextEditor"
date:    2004-03-03
authors:
  - blackarrow
slug:    more-ktexteditor
---
Being on holidays certainly gives you time to reflect on what new features you'd like in your text editor :)  I've come up with something which I hope will help make using a text editor a better experience by providing more assistance to the user... input filtering.<br>
<br>
It came to me when I was thinking of cleaner ways to do code templates.  At the moment, I think you have to use a keyboard shortcut to insert code templates?  Anyway, this will make it easier for the 3rd party app developer to e.g. respond to "for " and insert a template.  It will also make immutable text a possibility, something requested in a comment to my last blog.  In fact it will make it possible to enforce valid structure on a document, though I personally think that would be too restrictive.  Immutable text could be appropriately highlighted, and the cursor could move around it in a logical manner, so that the for ( ; ; ) { } template could be navigated more easily.<br>
<br>
Other ideas on the go are improved text navigation eg. easier use of code folding (previews of folded code? auto-expand modes?) and the ability to add a custom widget to the top and/or bottom right-hand corners.<br>
<br>
I've also decided to improve the idea I had for syntax highlighting by giving each text range a "state" variable, and associate a specific highlighting style to a state, then the developer just has to change the state of a range to make it look different.  I'm also taking requests for whatever text decorations you'd like to see available in the next kate - I'm thinking of borrowing some ideas from css, just let me know which ones you think would be useful.<br>
<br>
Furthermore, I'm planning to extend on code completion by adding syntax highlighting to the dropdown, expanding the info presented, allowing several different filtering and sorting rules (eg. by inheritance; by access (public/protected etc); alphanumerical; and hiding / disabling completions which don't fit the current context), and integrating (somehow - ideas?) summary documentation for the highlighted method.<br>
<br>
My flight calls, gotta go... someone should sponsor KDE developers going on long-haul international flights so they can hack from business class!  Economy is way too cramped...