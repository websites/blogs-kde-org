---
title: My First Akademy Adventure
authors:
  - traceyc
date: 2024-09-18
categories:
  - Akademy
---

This year was my first Akademy, and I was thrilled to be able to attend in 
person. Even better, some veterans said it was the best Akademy so far. It was 
great to see some people I had met in April and to meet new people. I arrived 
on Friday, 6th Sept and left the following Friday. I very much enjoyed staying 
in the lovely town of Würzburg and doing a day tour of Rothenberg. Now that 
I've caught up on sleep (the jet lag, it is real), it's time to write about it.

As described in [the Akademy 2024 
report](https://akademy.kde.org/news/2024-09_akademy-2024-report/), the focus 
this year was resetting priorities, refocusing goals and reorganizing projects. 
Since I had recently become a more active contributor to KDE, I was keen to 
learn about the direction things will take over the next year. It was also 
exciting to see the new design direction in Union and Plasma Next!

## A Personal Note

Speaking of goals, a personal one I've striven toward in my career is to 
contribute to something that improves the world, even if indirectly. It's not 
something I've always been able to do. It feels good to be able to work with a 
project that is open source, and is working to make the computing world more 
sustainable.

I'd also like to recognize all the wonderful, welcoming folks that make Akademy 
such a great conference. I've been to a few other tech conferences and events, 
with varying atmospheres and attitudes. I can say that people at Akademy are so 
helpful, and so nice - it made being among a lot of new faces in a foreign 
country a truly great experience.

## The Conference

The keynote had powerful information about the impacts of improper tech 
disposal and what we can do to improve the situation. This highlighted the 
importance of the KDE Eco project, which aims to help to reduce e-waste and 
make our projects more sustainable. Their new [Opt Green 
initiative](https://eco.kde.org/blog/2024-05-29_introducing-ns4nh/) is going to 
take concrete steps toward this.

Some of the talks I attended:

- KDE Goals - one talk about the last 2 years of goals and another revealing the 
new goals.
- "Adapt or Die" - how containerized packaging affects KDE projects.
- Union and styling in KDE's future.
- ~~Banana OS~~ [KDE Linux](https://invent.kde.org/sitter/kde-linux) - why it's 
being developed and some technical planning.
- Getting Them Early: Teaching Pupils About The Environmental Benefits Of FOSS
  This strategy has been powerful for other projects (like Microsoft Windows, 
Google Chromebooks, Java), so I'm glad to see it for KDE.
  - Why and how to use KDE frameworks in non-KDE apps
  - KDE Apps Initiative
  - Cutting Gordian's "End-User Focus" vs. "Privacy" Knot - collecting useful 
user data while respecting privacy and security.
  - Plasma Next - Visual Design Evolution for Plasma
  - The Road to KDE Neon Core

## BoF Sessions

Some of the BoF sessions I attended:

- Decentralizing KUserFeedback
- Streamlined Application Development Experience
- Organizing the Plasma team, Plasma goals
- Plasma "Next" initiative
- Union: The future of styling in KDE
- KWallet modern replacement
- Video tutorial for BoF best practice (Kieryn roped me into this one)
- Security Team BoF Notes

Thanks to everyone who made this year's Akademy such a wonderful experience. If 
anyone out there is thinking of attending next year, and can make it, I really 
recommend it. I'm hoping to be at Akademy 2025!
