---
title: Konsole Layout Automation (part 1)
authors:
  - traceyc
date: 2024-06-11
categories:
  - Konsole
---

Do you find  yourself opening up the same tabs in your terminal and running the same commands every day? Have you wanted to make this easier, say, by clicking a desktop icon or running a short command?

Recently, I saw a question along these lines in the [KDE support room on Matrix](https://go.kde.org/matrix/#/#kde:kde.org). Someone wanted to have [Konsole](https://konsole.kde.org/) automatically open some tabs and run specific commands when it's started. Since I've done this kind of thing, for personal and professional environments, I thought I'd share. You too can avoid doing the same manual terminal setup day after day, and just get to what you're working on.

Here in Part 1, I'll outline how to open tabs with different profiles and run commands in them. Next week, in Part 2, I'll share how to set up layouts (a.k.a. split panes) and run commands in those. You can combine these techniques to make your setup truly flexible. Using aliases, functions and scripts, you can bring up a complex layout with one command or button click.

## Inspiration

I have a motto: If I have to do the exact same thing more than twice (on a computer), I should probably script / automate it. It's an up front time investment that saves me a lot of manual tedium later.

My inspiration for scripting terminal layouts came from iTerm2's customization abilities. After a few years of using it on a work-provided MacBook Pro, I was spoiled. It's truly nice how easy it is to create profiles and save splits in the GUI. You can then tweak their configurations using any text editor. When I searched for how to do this with Linux terminals, I found quite a few people were also looking for an "iTerm2 equivalent for Linux".

## Goal: Launch a Konsole window with tabs which run commands

> 💡 **Note**
> For more detailed instructions on using Konsole, please see the output of `konsole --help` and take a look at the [Konsole Handbook](https://docs.kde.org/stable5/en/konsole/konsole/index.html)

We'll start with that request from Matrix: having Konsole create tabs and run commands in them when it starts. Here are the steps I used to accomplish this. Feel free to use this as inspiration and adapt for your own needs.

**Use case:** Quickly set up a working environment that I use daily to save time every day.  There will be a couple of tabs connected to different remote hosts, one that changes to a notes directory, and one running a system monitor.

Here's an overview of the steps:

- Create a Konsole profile which will run a command and optionally use another color theme
- Create a tab layout file to launch Konsole with which will open a tab with that profile
- Create a profile with a different command for an additional tab, and then identify commands for a couple of the other tabs
- Update the tab layout file to launch Konsole with all the desired tabs

Now let's get into the details.

## Create the first Konsole profile

Making custom profiles for different hosts are where my adventures in customizing Konsole began.

**Use case:** I would like to automatically connect to my hosted VPS in a tab with a specific color theme.

**Reasoning:** Running commands using a theme is quicker and easier than typing by hand (especially as they get more complex). With a custom color scheme, I can see at a glance which tab has my server session vs. any other system. This helps me avoid typing commands for my server into another system and vice-versa.

> 💡 **Note**
> Profiles are stored as plain text files in `/.local/share/konsole/`.
> Profile names are cAsE senSiTive.
> Since profile files are plain text, you can create, edit, and delete them with scripts. That is beyond the scope of this post.

The first step is to set up a custom profile with a custom color theme, a custom tab title format, and a command.

*I'm using "[eternal terminal](https://eternalterminal.dev/)"  (`et`) for its ability to reconnect, rather than `ssh` to connect to the VPS. I also have an entry for the host in `.ssh/config`*

In Settings -> Manage Profiles - New I created a profile named VPS. The following are the options I changed. Others were left as-is ("hostname" is a stand-in for the actual VPS hostname):

    Name: VPS
	Command: et VPS
	Tab title format: %n hostname
	Tab Color: I chose a color
	Appearance - Color scheme & font: I chose a different color scheme

Now that I have a custom profile, I can use it with a new tab in Konsole. When I go to  File -> New Tab and click "VPS", the tab will open with the new profile, and the command will run to connect to the server.

You can use Konsole's [command line options](https://docs.kde.org/stable5/en/konsole/konsole/command-line-options.html) in the following way to have it launch with a tab using a profile.

```bash
konsole --new-tab --profile VPS
```

This command can be used with an alias, function, script, Windows batch file, desktop shortcut or whatever you prefer.

There's another way to launch Konsole which is better for opening multiple tabs. It uses a "tabs configuration file".

### Create a tabs configuration file

> 💡 **Note**
> I have found this to be unreliable if any tab uses `ssh` or `et`
> There's an  [open bug report for this](https://bugs.kde.org/show_bug.cgi?id=488396). Until this is resolved, you may have better luck with a shell script that opens multiple tabs, I'll add information below.

Now I'll create a tabs configuration file with the VPS profile in it.

Each line in this file describes a tab for Konsole to open. The format is detailed in the [command line options documentation](https://docs.kde.org/stable5/en/konsole/konsole/command-line-options.html). I keep these in `~/konsole_layouts/`, keep yours in whatever directory works for you. I'll save this file as "vps-tabs".

Each line must have at least a profile or a command field. It can have both, and there are [other options as well](https://docs.kde.org/stable5/en/konsole/konsole/command-line-options.html).  
*Do not specify a command on a line for a tab if you also use a profile which itself has a command. This may lead to unexpected behavior.*

If you only need to run a command in a tab, you can just use the command field. You combine a command with a profile (that has no command) if you want to customize the tab further.

This is what will go in the file for now for my first tab:

```
title: VPS ;; profile: VPS
```

The command below will start Konsole using this file. (Change file path to point to the file on your system). Opening Konsole in this way can also be done with an alias, script etc.

```bash
konsole --hold --tabs-from-file ~/konsole_layouts/vps-tabs
```

Notice that I'm using `--hold` which tells Konsole to keep tabs open even after their command has exited. This will help debugging any problems that might arise (unless the window crashes entirely or fails to load). Once things are set up and running smoothly, you don't need to use `--hold`, if you prefer.

## Create additional profiles

**Use case:** At the start of the day, I want to automatically open tabs which connect to both servers I need to work with. I want an additional tab open to my notes directory and another one running a system monitor.

Time to add an additional profile to Konsole:

- Server2 - this will use `ssh` to connect to a second server and use a profile for a different color theme

I won't create profiles for the system monitor tab or the Notes tab. I'll run those commands directly from the tab configuration file.

## Update the tabs file with additional tab definitions

At this point, I'll add lines to the tabs configuration file I created.

- A tab for the Server2 profile.
- A tab which changes to a notes directory, and uses the Sand profile with colors I prefer for them.
- A tab for the system monitor which runs the `btm` command and uses another profile with yet another color scheme.

The file now looks like this:

```
title: VPS ;; profile: VPS
title: SysAdminNotes ;; workdir: ~/Nextcloud/Notes/SysAdmin ;; profile: Sand
title: Server2 ;; profile: Server2
title: NavyBlue ;; command: btm ;; profile: NavyBlue
```

And now, when I run `konsole --hold --tabs-from-file ~/konsole_layouts/vps-tabs`, Konsole launches with all the tabs I need.

## Bonus: Run commands after connecting to a remote host

It's possible with both `ssh` and `et` to run commands on a remote host after connection. For instance, I could update the command in my VPS profile like so:

    Name: VPS
	Command: et VPS -c 'sudo apt update && apt list --upgradable; exec /bin/zsh'
	Tab title format: %n hostname
	Tab Color: I chose a color
	Appearance - Color scheme & font: I chose a different color scheme

The command not only connects to my VPS, it also displays a list of any available updates, and stays connected to the host. Note the `exec /bin/zsh` at the end. This will keep an interactive terminal open after other commands are run.

With `ssh`, to run a command on a remote host while staying connected, the command is slightly different:

```bash
ssh -t VPS 'sudo apt update && apt list --upgradable; zsh'
```

### But what if I need to connect to a jumphost before the remote host and I still want to run a command after I connect?

This can be done with either SSH or Eternal Terminal. For SSH, the process is relatively easy, and plenty of sites online provide instructions. Eternal Terminal, on the other hand, is a bit more tricky.

It took some research and experimentation to get this working with Eternal Terminal, so here it is ("jumphost" is a placeholder for the actual hostname of the jumphost).

```bash
Command=et jumphost -c "ssh -t remotehost 'cd /some/directory && . ../some_script && cd ../another-directory; exec /bin/zsh'"
```

## Alternative to using a tabs file: a shell script or function

If you have difficulties with using a tabs configuration file, you can use a shell script (or function) that takes advantage of the command line options Konsole has. For instance, using the `-e` flag we can have tabs run commands:

```bash
#!/usr/bin/env bash
konsole --new-tab -e echo This is tab 1 &
konsole --new-tab -e echo This is tab 2 &
konsole --new-tab -e echo This is tab 3 &
konsole --new-tab -e echo This is tab 4 &
```

You can combine commands with profiles as well:

```bash
#!/usr/bin/env bash
konsole --new-tab --profile VPS &
konsole --new-tab --profile Sand --workdir: ~/Nextcloud/Notes/SysAdmin &
konsole --new-tab --profile Server2 &
konsole --new-tab --profile NavyBlue -e btm &
```

Explore the Konsole docs and experiment!

## Known issues

* Launching Konsole with `--tabs-from-file` will open an extra tab in addition to the ones from the configuration file. This has [an open bug report](https://bugs.kde.org/show_bug.cgi?id=487896).
* Sometimes, with `--tabs-from-file`, the new Konsole window immediately disappears. This *may* be related to ssh/et being in a command. I filed [a bug report for that](https://bugs.kde.org/show_bug.cgi?id=488396).
* If using a tabs configuration file, or a script: only use Eternal Terminal with one tab. Trying to use it with more than one tab will result in `et` core dumping in one of them.
