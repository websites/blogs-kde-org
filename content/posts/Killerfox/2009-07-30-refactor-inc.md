---
title:   "Refactor Inc."
date:    2009-07-30
authors:
  - Killerfox
slug:    refactor-inc
---
Here is another update on the progress of the KDevelop refactoring support SoC.

Last week was pretty hectic for me. Sadly I didn't have as much time as I would have liked to work on the project, and even less to blog about it. However with this update I will list things that have happened these past two weeks. Among things that happened:
<!--break-->
<li>Unit tests finally work with:</li>
<li>This was needed instead of the current DUChain testing method because it now supports mapping of chains between different code samples, because most refactoring tools would need to make changes in more than one file</li>
<li>Extracted the change widget created by Aleix for use of cmake automatic changes. Extracted it for a more generalized use of refactoring tools for user review</li>
<li>Extended change widget by adding support for multiple documents, and switching between change view (via KomparePart), and edit view(via KatePart) to allow the user to tweak the changes before they are saved.</li>
<li>Patching Kompare to allow comparison of a file, and a custom provided string (In progress)</li>
<li>Added the capacity of generating changes in layers. To apply a few changes, have the parser/DUChain builder run, and then perform more changes on that result.</li>

That last point is specially important because that allows for higher lever code generation tools to call low lever tools, apply those changes to a temporary location, and then manipulate that chain part of the code, Example:

When writing Private implementation I need to create a new class to be private,so I invoke the class generator, feed it the parameters I need, have it generate the class into a temporary representation, and then I have a duchain built for that class so I can manipulate it a lot easier.

<b>What Next?</b>

So for next steps, goal is to have the KomparePart work correctly in the widget before beta5. And also finish the generator for Making class implementation private.

See you the next update!