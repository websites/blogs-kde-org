---
title:   "...and Refactoring for All"
date:    2009-07-07
authors:
  - Killerfox
slug:    and-refactoring-all
---
Hello everybody, I should introduce myself first: I am Ramón Zarazúa, I am a GSOC student working on C++ refacoring support for KDevelop. I am very pleased to be contributing to KDE and the community, and want to make us the best we can be!

<b>State of things:</b>
To be honest things are not going good :( For lots of different reasons progress has been stagnant on the project. There have been changes in the plan, and now focus will switch to do refactoring on a more textual level (instead of the initially planned purely conceptual level). I am currently working on the refactor framework, and a refactoring concept to "make a class implementation private" or as it is known in QT and KDE: the 'd' pointer.
<!--break-->
<b>What to do now?</b>
Well I have met up seriously with my mentor, and we have discussed in what direction things should go now. Some more code cleanup is required, unit tests need to be set up, and fix a few bugs to be in track again. Focus will switch to implement refactor concepts only until we have a decent enough framework, so concept implementation can become easier

<b>Track plan:</b>
Merge branch into trunk
Set up unit-testing environment
Hunt down selection context bug
commit first iteration of MakeImplementationPrivate
Implement temporary file changes and reparse of DuChain/AST

This last task seems quite complex, so more collaboration with the kdevelop team will be needed, however it is essential for any kind of decent refactoring concept

More details as they arrive!