---
title:   "Krita's results of the past summer"
date:    2006-08-29
authors:
  - bart coppens
slug:    kritas-results-past-summer
---
Krita participated through KDE with the 2006 Summer of Code by Google. I think the result turned out to be quite impressive and usable. Basically, there were 2 big objectives: create a Bézier tool, and create an 'intelligent scissors' tool, through a common, to-be-created framework. The student that did the programming for this, was Emanuele Tamponi. He completed the project nicely on time, so that the new features can be admired in the upcoming 1.6 release of Krita.
First, there was the Bézier tool. This now (finally) allows us to draw an arbitrary and potentially nicely flowing (or is it derivable?) curves on the screen, edit them, and then decide to stroke (draw them with the current brush and color), or convert it to a selection. The basic interface looks like this.
<img src="http://www.bartcoppens.be/blog/bezier.png">
Which gives you the ability to move points, control points, add points, etc. In my opinion it's very usable and gives a pretty result (but then, I'm not an artist). There's quite a chance that this tool will get superseded in Krita 2 with the flake bézier tool, which is quite a shame. The far future might look bleak, but at least now we have a working implementation, that will probably get used for the year(s?) to come, until KDE4 and Krita 2 actually get to the users.
The other big thing that got done, is the scissors tool. Or, MOS, as we call it now (stands for Magnetic Outline Selection). The tool does a pretty neat thing: you move your mouse around an object (preferably one with well-defined contours), and the tool will try to follow the outline, drawing a curve around it. When you're finished, you can go and modify it a bit, or just convert the outline to a selection. It's a bit like the regular selection tool, but with the very cool effect that you can shake your hands with the mouse, and the tool will actually try to follow the mind, not the hand ;-)
<img src="http://www.bartcoppens.be/blog/mos1.png">
In the picture, I moved my mouse around the rose, and the tool automatically added intermediate points. Those points are everything it needs; the lines in between are automatically calculated from the image itself. Pretty cool and useful, I'd say.
If you'd like to have a look at the features, you can either wait for the 1.6 version of Krita and KOffice (scheduled for October 10th), the first beta (September 8th), or just check out the svn version from the 1.6 branch (but as with all code, no guarantees it will work).<!--break-->