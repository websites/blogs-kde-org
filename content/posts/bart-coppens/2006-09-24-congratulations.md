---
title:   "Congratulations"
date:    2006-09-24
authors:
  - bart coppens
slug:    congratulations
---
Just a small entry to congratulate Boudewijn (and all the other Krita developers, of course!) with his winning (on his birthday) of an award at aKademy with Krita: "Best Application: Krita, Boudewijn Rempt"! Yay! Too bad he was not at aKademy to receive his price first-hand, but I'm guessing the applause was big enough to be heard in the Netherlands ;) Congratulations!<!--break-->