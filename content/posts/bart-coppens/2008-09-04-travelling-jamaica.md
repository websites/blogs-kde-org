---
title:   "On travelling (to Jamaica)"
date:    2008-09-04
authors:
  - bart coppens
slug:    travelling-jamaica
---
As many of you will know, I'm not really keen on travelling. This has much to do with the dislike of leaving my zone of comfort, my fear of flying, etc. However, when I <emph>do</emph> have to travel, I try to inform myself of issues that might arise from travelling to the specific country I'm supposed to go to. For instance, like a lot of Europeans, I do not look forward to the idea of travelling to the US because of the issues with immigration (like privacy). Issues like these (luckily) still generate some press coverage so that most people know vaguely what to expect. Similarly, it's probably pretty well-known that you better don't start waving around Swastika's when you're visiting Germany.

However, people intending to travel to the <a href="http://troy-at-kde.livejournal.com/15657.html">first KDE Americas event</a> (which will apparently be in Jamaica next January) might not be aware that Jamaica doesn't sound to be a nice place to go to if you're LGBT (Lesbian/Bi/Gay/Transgender). See for instance <a href="http://en.wikipedia.org/w/index.php?title=LGBT_rights_in_Jamaica&oldid=233635133">the Wikipedia article on Jamaican LGBT rights</a>, <a href="http://hrw.org/reports/2004/jamaica1104/">Human Rights Watch</a> or this <a href="http://www.time.com/time/world/article/0,8599,1182991,00.html">Time article</a>.

Now since I won't go anyway (like I said, I don't like travelling) this doesn't influence me into not going there. Yet I think it's only fair to bring it to the attention of people intending to go, so that they don't have unpleasant surprises when they find out about it after confirming to go...
<!--break-->