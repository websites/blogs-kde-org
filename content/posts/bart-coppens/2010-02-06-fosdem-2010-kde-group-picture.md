---
title:   "FOSDEM 2010 KDE Group Picture"
date:    2010-02-06
authors:
  - bart coppens
slug:    fosdem-2010-kde-group-picture
---
As always, we had the KDE Group Picture taken at FOSDEM. It went pretty smooth this year, apart from not immediately hearing when the autotimer of the camera clicked :)
<a href="http://www.bartcoppens.be/fosdem2010.JPG"><img src="http://www.bartcoppens.be/fosdem2010_small.JPG"></a>

[Edit: Hmmm why does this not show up in the feeds?]
<!--break-->