---
title:   "KDE4, Krita 2 and fun with SVN"
date:    2006-10-05
authors:
  - bart coppens
slug:    kde4-krita-2-and-fun-svn
---
So I was finally forced to switch to KDE4 (again, but this time for real, apparently ;) ) to start porting my Krita 1.6 stuff to Krita 2. Meaning I now have some new and not completely unrelated things to talk about.

<i>KDE4</i>
I didn't actually install kdebase, so this is entirely based on the Krita stuff I did with the kdelibs of kde4. The best part of it is still the lots faster relinking: it almost makes me cry to see all of Krita's plugins being relinked so fast! So fast! It is magnificent! Also very nice: the radiobuttons in the QMenus. Much clearer than what we had before (but I'd not use it in the recent files list).
The thing that annoyed me most was the wrong (well, wrong relative to what my hand is used from KDE3) button order for the yesNoCancel messagebox. My hand coordination would get completely confused with a different order for my Krita 1.6 and Krita 2 installs, and that would annoy me. So I made <a href="http://www.bartcoppens.be/buttonorder.diff">this patch</a> to get it out of my mind (I'm not committing since I'm not sure it's not done on purpose). Another thing that slightly annoyed me is the uneven look of the default Qt style: some things look very smooth and rounded, while other stuff looks very rectangular and even outdated (the tab things). But that's a minor concern, I'm sure there are better styles in kdebase somewhere.

<i>Krita 2</i>
Not much to say, yet. It starts, but somehow it gives a very slow impression in general (after I close the dockers that actually go <i>crazy</i>, I'm being told to blame Qt or KDE4 for it, so I do just that). I'm not sure what causes it, really, I'm guessing the ancient Krita code triggering a slow code path in Qt or X (like my Konqueror 3 does). The general impression I got when starting it, is a bit like your favorite starship or space station, just after it's been beaten by some overpowered opponents: it's general functions (floating around in space) still work, but it doesn't look very pretty anymore.

But that doesn't mean the devvers aren't buzzing with new ideas! Now we're all thinking (and doing) cool and long-needed rewrites, refactorings and other stuff we really shouldn't do in anything that resembles a stable branch. Cyrille is rewriting the color library to use a much more templated design internally, toying around with OpenRaster and playing with dynamic brushes. In the meantime, Boudewijn is completely rewriting the KisView stuff (the previous code was really ancient, contains most of the antique stuff in Krita), I'm thinking about some of the cool things pippin talked about, etc! There was somebody on the IRC channel who wanted to see if he could use Krita to play with image filters on his GPU, that'd be very cool as well.
What I'm also saying is: if you're thinking of adding some revolutionary feature or big redesign to Krita, maybe it'd be a good time to try it out now. If it breaks stuff now, it's not as bad as when it'd be broken a week before we want to do a release freeze.

<i>Fun with SVN (and XML)</i>
Now that I'm looking at porting my 1.6 code, I thought it would it a good idea if I had a big list of all the changes I did during the 1.6 period (yes, I was too lazy to actually keep a list of changes along the way). So I was already starting to prepare myself to do some Evil Shellscripting combined with Weird Regexps, to trick the svn log output into becoming a list of my changes, with a list of changed files.
But then came elvstone (Aron Stansvik) to help me out of my misery: svn also has XML output, and you can do cool stuff with that using XSLT. (That almost makes me wish that I had done the exercises I could have done with xslt for a course last year. Almost, but not quite :P) So the trick is first to dump your log with <pre>svn log --xml -v > alogfile.xml</pre> to a file for further use. The -v option also dumps the changed files! I find that just so cool! (Before I knew that, I was writing a script that svn diff'ed each of my revisions, grepped on Index:, and then didn't care about the rest ;))
Then, the cool part:
<pre>cat alogfile.xml | xsltproc --stringparam author coppens author-log-filter-files.xsl - > filtered.log</pre>
using <a href="http://www.bartcoppens.be/author-log-filter-files.xsl">this xsl file</a>. This filters all the commit messages listed in the svn log on my svn name, and then pleasantly displays revision number, date, log message and the list of files changed (including the 'A', 'M' and 'D' modifiers). I can't get over the coolness of this, really, because if this didn't exist, I'd have wasted the entire afternoon swearing at egrep and friends for not doing what I insist it must.

<i>Random Addendum</i>
<a href="http://en.wikipedia.org/wiki/Babylon_5">Babylon 5</a> rocks! (/mornfall-induced message.) And it's actually true, it really rocks. It's a very cool series, watch it if you haven't already :P<!--break-->