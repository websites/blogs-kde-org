---
title:   "FOSDEM KDE Talks: Hurry Up!"
date:    2008-01-19
authors:
  - bart coppens
slug:    fosdem-kde-talks-hurry
---
I want to bring to your attention the fact that the deadline for scheduling FOSDEM's devrooms is coming up soon. There are as of yet only very few talks proposed, I'd like to see some more ideas! We'll be sharing the room with the GNOME people on Sunday, where we'll have some talks that will be related to issues that are interesting to both of our audiences. That also means that if we don't have enough KDE talks, they'll easily fill in the gap for us ;)
So, hurry up and add a talk proposal on <a href="http://wiki.kde.org/tiki-index.php?page=FOSDEM2008">KDE's FOSDEM Wiki Page</a> (or mail me, or query me on IRC). More information about KDE's FOSDEM presence on the <a href="http://dot.kde.org/1198427916/">dot article</a>.<!--break-->