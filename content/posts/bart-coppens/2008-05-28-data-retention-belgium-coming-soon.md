---
title:   "Data Retention in Belgium: coming soon?"
date:    2008-05-28
authors:
  - bart coppens
slug:    data-retention-belgium-coming-soon
---
Today, the BIPT (Belgian Institute for Postal Services and Telecommunication) released a <a href="http://www.bipt.be/nl/408/ShowDoc/2808/Raadplegingen/Raadpleging_door_de_Raad_van_het_BIPT_op_verzoek_v.aspx">draft of the implementation of the EU directive regarding data retention in Belgian Law</a> at the request of the minster for Enterprising and Simplification (dunno how else to translate 'minister voor Ondernemen en Vereenvoudigen') (downloadable in Dutch and French). It seems the idea is to introduce some form of public scrutiny (instead of just making the law and be done with it), so that people can comment on it. I'm of course completely pessimistic about real chances of influencing it for the better, but one never knows.

Even though that text seems to have some very minor redeeming parts on the surface, when I read it, I wasn't happy. Seems like they want ISPs and other services providers (email, webmail, VoIP, etc) to keep a lot of information, for the maximum extent permissible by the directive (that is, 2 years). Of course, no modern law would be complete without having a clause in it that makes it permissible to extend that term indefinitely in case of 'extraordinary circumstances'. Sigh.

I can't make too much comments on it since I'm just a computer scientist, not a lawyer. It is rather hard for me to grasp the intricate details of some formulations (usually because I interpret terms differently). The fact that part of it changes and references the gigantic (at least for me) telecommunications law, isn't helping much either.

In any case, Belgians that have an interest in law and data retention might find the text an interesting (if not happy) read.
<!--break-->