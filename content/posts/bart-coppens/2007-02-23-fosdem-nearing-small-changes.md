---
title:   "FOSDEM Nearing - Small changes"
date:    2007-02-23
authors:
  - bart coppens
slug:    fosdem-nearing-small-changes
---
<a href="http://www.fosdem.org/2007/">FOSDEM 2007</a> is approaching very rapidly: this weekend a lot of KDE and other FOSS people will gather in Brussels to meet and talk. There are two small changes to the KDE Devroom schedule that might be interesting. The first one is that we had to change the speaker of one talk: unfortunately Sebastian Trueg is ill, so now the Nepomuk-Semantic KDE talk will be done by Jos van den Oever (of Strigi fame) and Stéphane Laurière (from Nepomuk, EDOS). Should be a very interesting talk! The second change is the addition of a new talk: '<a href="http://www.fosdem.org/2007/schedule/events/kde_ev">KDE e.V. - The organization behind the project</a>' by Sebastian Kügler (of KDE e.V. Board and MWG fame).
Hope to see you Saturday or Sunday! (<a href="http://wiki.kde.org/tiki-index.php?page=fosdem2007">Here</a> you can find a very incomplete list of KDE related people who will be attending, you can add yourself if you're coming. <a href="http://dot.kde.org/1172149758/">Dot article</a> for some more info.)<!--break-->