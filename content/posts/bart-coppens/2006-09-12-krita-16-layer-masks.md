---
title:   "Krita 1.6: Layer Masks"
date:    2006-09-12
authors:
  - bart coppens
slug:    krita-16-layer-masks
---
This is a small blog about layer masks, so that Sander can hopefully use it for the documentation (documentation, yay!).
Basically, a layer mask is a mask that you place on your paint layer. This will literally mask areas of the layer, so that the content underneath shows through. You can paint on it with greyscale colors: the more black the color, the less the layer under it will shine through, the more white, the less the layer under it will be shown. So complete white will let nothing through, complete black will let everything through. Basically, it's a bit like selecting a piece of your image, and then cutting it, so that the selected bits go away. So what is the use for a mask here? The big advantage is that it is non-destructive: if you decide that you masked out the wrong part of your layer, you can easily remove the mask and start anew, something a lot harder (not to say near impossible, especially in between sessions) with regular selection-cutting.

So, how to create a mask? There are 2 ways:
<ul>
<li> Start from scratch. Layer -> Mask -> Create Mask. The mask starts with everything being retained, that is, a complete white mask. Basically you won't see any change as long as you don't paint on it.
<li> Start from the current selection. Layer -> Mask -> Mask From Selection. The selectedness will be converted to whiteness. This means that fully selected area will be visible, fully unselected areas will be invisible, and the rest will be partially visible, depending on how much the area was selected.
</ul>

Editing the mask: First, make sure you are editing the mask, not the layer, by making sure Layer -> Mask -> Edit Mask is checked. (This is checked by default.) Then you can paint on the layer just like before, only now you are painting on the mask, instead of on the layer itself. To stop painting on the mask, you can uncheck the Edit Mask checkbox. There's also the option to show the mask, through checking Layer -> Mask -> Show Mask. (This is not checked by default). This option will render the entire layer as a visual representation of the mask in greyscale, instead of the actual layer. This can be handy to see where your mask is, but it might be not as handy when you want to edit it, since you can't look at the actual layer.

Other actions: you can also remove the mask if you are not satisfied with it, and want to start over again, or just want to remove it, with Layer -> Mask -> Remove Mask. You can also 'apply' the mask, meaning that the mask will be made permanently. This means that the mask is removed, but that its effect of transparency will be committed to the layer.

Good, that concludes the actual documentation part of the blog entry. You might have noticed that the UI aspect of this sucks. And I concur. This has none of the usual gimmicks you get with masks in the GIMP, like it appearing in the layer box. This has a reason: apparently it was a bit difficult to integrate it in the current layerbox, but I was told this kind of thing would be much easier in the KOffice 2 layerbox. So I didn't really bother to invest much effort into hacks that wouldn't be needed in the next version anyway. This all means that KOffice2/Krita2 might be something to look forward to, if I can fix it there :)
Another, related idea (still needs the KO2 changes) would be to see (in the layerbox) the masks as 'sublayers'. Then you could easily disable rendering, enable editing, have a nice preview of the mask, etc, all in the handy reach of the layerbox. And it might be cool if you could actually move masks to a different layer with drag&drop! :D
Another small thing you might have noticed: I didn't actually provide any screenshots. I originally planned to. But then the bugs came crawling towards me, and I needed to fix them all. Only then, I discovered that I really, truly suck at making any form of art with masks, so I won't inflict it on you ;)<!--break-->