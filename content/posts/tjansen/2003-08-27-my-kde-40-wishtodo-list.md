---
title:   "My KDE 4.0 Wish/Todo list..."
date:    2003-08-27
authors:
  - tjansen
slug:    my-kde-40-wishtodo-list
---
Ok, with the official presentation of Qt 4 the pandora's box called KDE 4.0 is open, and people start discussing possible changes. So here is my personal contribution of wishes (that I would be willing to work on, of course, at least if I am not the only one):

<ul>
<li>Clean namespace hierarchy. I'd really would love to have a finer-grained seperation of kdelibs into namespaces in the way I did it with <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/gst/">the GStreamer libs</a>, <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kdeutil/kde/ws/">my new WebServices/SOAP lib</a> and the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/knot/kdeapi/">new SLP API</a>. Basically that means putting user-interface stuff into a namespace KDE::UI, utility classes like KSharedPtr into KDE::Util, KIO stuff into KDE::KIO (or KDE::IO) and so on. It could also mean to get rid of the "K" prefix in the classes. For backward compatibily there could be, of course, typedefs with the original names.
<ul>
<li>The current way does not scale. When you have only one kind of service (.desktop files), using a name like KService is ok. But I have three services on my disk: the old .desktop file KService, the basic entity of SLP is called KDE:: SLP:: Service and in the still unfinished WSDL implementation I have a KDE::WS::WSDL:: Service. As KDE gets more libraries you need namespaces to avoid confusion. And all classes should be namespace'd because if one is not, it is not possible to import the other namespaces without conflicts.
<li>All APIs associated with KDE should be in a 'KDE' namespace. Otherwise it is too easy to get a conflict with other people's APIs. For example my GStreamer wrappers are not the only C++ wrappers for GStreamer, there are also GStreamer bindings for Gnome. If you (for any strange reason) want to have both in your app, this would not work if both implementations use a 'GST' namespace.
<li>The headers should use the same hierarchy, so KDE:: SLP:: Service should be implemented in a header 'kde/slp/service.h'. Encoding this into the header name without directories is unreadable, especially with lower case file names (who can decipher 'kdewswsdlservice.h'?)
<li>It becomes easier to find documentation of a class when they are in a strict hierarchy. Right now, when I see an unknown class starting with the K, i need to search through all modules to find it
<li>Hopefully in the future, with the small Qt core library, people will start using Qt/KDE classes for writing servers, like the Knot does. This requires  better seperation between modules. Right now even kdecore requires X11 (see KApplication and KShortcut&friends).
</ul>

<li>I would like to see inter-distribution binary compatibility for KDE apps. This means that KDE recommends a set of compiler, library versions, compilation flags etc that has to be used for building KDE. Then you could have compile-once-run-everywhere KDE binaries and make the life for 3rd party developers much easier, together with <a href="http://autopackage.org/">Autopackage</a> or a similar tool that should be finished when KDE4 is out and could be embraced by KDE

<li>I would like to have BeOS-style binary compatibility for virtual methods. They use(d) a simple trick: each class got a number of unused, dummy virtual methods. When the API is extended and needs another virtual function, they just replaced one of them with the new virtual method. The current way, using virtual_hook, is a fine last resort (when you run out of dummy virtual methods) but ruins the API documentation and is harder to use. The dummy virtuals could be done using a simple macro that conains a number (either as argument or in its name). Just add the macro to your class, and when you add a virtual, decrement the number.
</ul>