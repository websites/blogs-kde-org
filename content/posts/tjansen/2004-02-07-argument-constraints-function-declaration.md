---
title:   "Argument Constraints in the Function Declaration"
date:    2004-02-07
authors:
  - tjansen
slug:    argument-constraints-function-declaration
---
The most annoying thing when writing public APIs is that it's a lot of redundant typing. A good API implementation should check all arguments and return a useful error message if an argument is invalid. Good API documentation should document which arguments values are valid and which return values are possible. Both tasks are work-intensive and annoying for the developer, which is probably one of the reasons why there so many bad APIs. 
Here is a Java snippet with Sun-style argument handling:
<pre>
/**
 * Sets the discount as percentage.
 * @param percentage the discount as percentage
 * @throws IllegalArgumentException if percentage &lt; 0 or percentage > 100
 */
void setDiscountPercentage(float percentage) {
	if ((percentage &lt; 0.0) || (percentage > 100.0))
		throw new InvalidArgumentException("percentage must be >=0 and &lt;=100");
	mPercentage = percentage;
}


/**
 * Gets the discount as percentage.
 * @return the discount as percentage. Guaranteed to be positive and not
 *         higher than 100.0
 */
float getDiscountPercentage() {
	assert (mPercentage >= 0.0) && (mPercentage &lt;= 100.0);
	return mPercentage;
}
</pre>

So is it possible to remove this redundancy and specify the constraints only once? I think so.
The obvious solution is to add a list of assertions to each argument. This is an attempt at a very compact syntax:
<pre>
void setDiscountPercentage(float percentage(>=0.0,&lt;=100.0)) {
	mPercentage = percentage;
}

float(>=0.0,&lt;=100.0) getDiscountPercentage() {
	return mPercentage;
}
</pre>
Each argument is followed by a list of comma-separated constraints that can be checked before executing the function. To make the syntax more crisp it is allowed to start the expressions directly with a comparison operator. They will then be executed with the function argument as first operand. It's also possible to use normal boolean expressions, for instance to check the length of a string:
<pre>
void setId(String id(!=null, id.length() > 0, id.length() &lt;= 10)) {
	//...
}
</pre>
The syntax looks pretty cryptic though, because the declaration becomes very long and the nesting of parentheses is confusing. An alternative would be to declare the assertions after the normal signature:
<pre>
void setDiscountPercentage(float percentage) 
	with percentage: >=0.0, &lt;=100.0 {
	mPercentage = percentage;
}

(float percentage) getDiscountPercentage() 
	with percentage: >=0.0, &lt;=100.0 {
	return mPercentage;
}

void setId(String id) 
	with id: !=null, id.length() > 0, id.length() &lt;= 10 {
	// ...
}
</pre>
It's a little bit more verbose, and for return value constraints this syntax requires <a href="http://blogs.kde.org/node/view/321">named return values</a>,
but I think it is much more readable than the first attempt. Here an example with more arguments and a return value tuple:
<pre>
(int a, int b, int c) create3PositiveNumbers(int x)
        with a: >0
	with b: >0
	with c: >0
	with x: >0 {
	return (x, x, x);
}
</pre>
I think it's quite nice. A few random things:
<ul>
<li>The constraints need to be shown in the API, so they must use only public symbols.
Otherwise the user would not be able to understand them. The compiler should enforce this.
<li>If a reference argument is null, any operation on it will fail. But quite often
null is allowed as an argument, which would make the constraints quite ugly. Imagine
a setId() variant that allow nulls:
<pre>
void setIdAllowsNull(String id) 
	with id: (id==null) || (id.length() > 0), (id==null) || (id.length() &lt;= 10) {
	// ...
}
</pre>
An easy solution is the following rule: if an argument is a reference value and it is null, it always passes all tests, unless the first constraint is "!=null". With this rule the constraints can be simplified to
<pre>
void setIdAllowsNull(String id) 
	with id: id.length() > 0, id.length() &lt;= 10 {
	// ...
}
</pre>
<li>Overridden virtual functions always inherit the constraints and can not add any new constraints (or even change the original ones)
<li>Just like regular assert statements it should be possible to disable the argument checks if performance is  critical
</ul>