---
title:   "Moving Forward"
date:    2004-05-10
authors:
  - tjansen
slug:    moving-forward
---
As the things that I am working on are not KDE-related anymore (and Blogger.com now allows comments :), I have moved my english blog to <a href="http://www.tjansen.de/blogen">tjansen.de</a>.