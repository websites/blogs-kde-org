---
title:   "Playing with the Switch and Foreach Statements"
date:    2004-02-22
authors:
  - tjansen
slug:    playing-switch-and-foreach-statements
---
I've been thinking about C's control statements (if, while, switch, for etc) for a little while, and I think there's some room for improvements in the later two...<br><!--break--><br>
<b>switch</b><br>
The <a href="http://www.artima.com/intv/choices.html">C# guys</a> have made two interesting modifications to C's switch statement: fall-through
is only allowed when there's no statement after the case (thus if a statement
follows a case, there must be a <i>break</i> or similar jump statement).
And it added the 'goto case' statement that can be used for
fall-through effects. Here's a C# snippet:
<pre>
	void test(int a) {
		switch (a) {
		case 0:
			Console.WriteLine("blabla");
			break;
		case 1:
			Console.Write("More ");
			goto case 0;
		case 2:
		case 3:
			Console.WriteLine("lalala");
			break;
		}
	}
</pre>
I think forbidding implicit fall-through is a good idea, it avoids
a common source of error. But if you forbid it, why is it still necessary
to write the redundant <i>break</i> statements for each case? Especially long
switch statements look much better without them:
<pre>
	void test(int a) {
		switch (a) {
		case 0:
			Console.WriteLine("blabla");
		case 1:
			Console.WriteLine("More");
			goto case 0;
		case 2:
		case 3:
			Console.WriteLine("lalala");
		}
	}
</pre>
Something that I am missing all the time is a way to describe value
ranges in a switch statement. The
<a href="http://blogs.kde.org/node/view/339">constraint syntax from my
last entry</a> offers a neat solution for it:
<pre>
	void test(int a) {
		switch (a) {
		case <0:
			System.out.println("a is negative");
		case 0:
			System.out.println("a is 0");
		case >0, &lt;=100:
			System.out.println("a is between 1 and 100");
		case &lt;200:
		case >500, &lt;600:
			System.out.println("a is >100 and &lt;200, or >500 and &lt;600");
		default:
			System.out.println("a has some other value");
		}
	}
</pre>
Allowing comma-separated lists of operator plus constant makes switch much more
powerful for many purposes.

<br><br>
<b>foreach</b><br>

Over the last years most C-based languages got a foreach statement to
iterate over collections and arrays, but every language got its own
syntax. This is what it looks like in C#:
<pre>
	int sum = 0;
	foreach (int i in someArray)
		sum += i;
</pre>
... and ECMAScript (JavaScript):
<pre>
	var sum = 0;
	for (var i in someArray)
		sum += i;
</pre>
... and Java 1.5:
<pre>
	int sum = 0;
	for (int i: someArray)
		sum += i;
</pre>
I definitely prefer Java's syntax as 'for' is shorter than 'foreach'
(without reducing readability) and I don't like making <i>in</i>
a keyword. <br>
Unfortunately neither Java nor C# exploit their <i>foreach</i> as a nicer
alternative for regular <i>for</i> loops. The common C pattern
<pre>
	for (int i = 0; i &lt; 100; i++)
		System.out.println("Bla");
</pre>
can be expressed with a <i>foreach</i> loop and a special object that
provides a sequence of integers, similar to
<a href="http://www.python.org/doc/current/tut/node6.html#SECTION006300000000000000000">Python's range() function</a>:
<pre>
	for (int i: new Range(100))
		System.out.println("Bla");
</pre>
It's looking nicer without the <i>new</i> keyword, as shown <a href="http://blogs.kde.org/node/view/279">here</a>:
<pre>
	for (int i: Range(100))
		System.out.println("Bla");
</pre>
Using other constructors it would be possible to specify at start value, to
go backwards or use bigger steps:
<pre>
	for (int i: Range(50, 10, -1))
		System.out.println("i=" + i);
</pre>
As <i>for</i> loops are pretty common, it may make sense to have a special
operator for simple ranges, like
<a href="http://www.rubycentral.com/book/tut_containers.html">Ruby does</a> and
<a href="http://cal018000.student.utwente.nl/wakka/wakka.php?wakka=Foreach">Project Alpha proposes</a>:
<pre>
	for (int i: 0...99)
		System.out.println("i=" + i);
</pre>
The expression 'a...b' is short for '(a&#60;=b)?Range(a, b+1):Range(b, a-1, -1)'.
Ranges can also be used for other purposes, for instance as function
arguments. With Range and an overloaded indexer iterating through the
first ten members of a list would be as easy as:
<pre>
	void print10Numbers(List<Integer> numberList) {
		for (int i: numberList&#91;0...9])
			System.out.println("i=" + i);
	}
</pre>
Unlike regular methods for slicing collections such as Java's <a href="http://java.sun.com/j2se/1.5.0/docs/api/java/util/List.html#subList(int, int)">List.subList()</a>
this syntax gives more flexibility. You could get the first ten numbers in reverse order (numberList&#91;9...0]), skip every second number (numberList&#91;Range(0, numberList.size(), 2)]) and so on.
