---
title:   "The freedesktop.org discussion in three simple points"
date:    2004-01-29
authors:
  - tjansen
slug:    freedesktoporg-discussion-three-simple-points
---
I think the whole discussion can be simplified with three points that hopefully everyone can agree to:<br>
<ol><li>In the next 12-24 months the only way to get a somewhat competitive desktop is to pile up code from all sources, including kde and gnome, and try to integrate them somehow
<li>If you think about the architecture of a desktop in 5-10 years, this mixture of pure C code, Gnome/Glib C code, KDE C++ code, Python, Bash scripts, maybe Mono C# code etc, all with different API conventions and wrapped by various wrapping mechanisms, all that sounds like a horrible nightmare that no one really wants
<li>Whether you like freedesktop.org or not depends on whether you want to have a usable desktop soon (thus today's desktop has a high priority for you and the future a low priority), or whether your goal is to have the best desktop at some point in the future (future: high priority, today: low priority)
</ol>
<br>
Make your choice.<!--break-->



