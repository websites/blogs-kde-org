---
title:   "Nerd scoring (sorry)"
date:    2005-01-17
authors:
  - brad hards
slug:    nerd-scoring-sorry
---
<a href="http://www.wxplotter.com/ft_nq.php?im"> 
<img src="http://www.wxplotter.com/images/ft/nq.php?val=2723" alt="I am nerdier than 74% of all people. Are you nerdier? Click here to find out!">

And in even worse news, I'm finding it difficult to do real work and any KDE hacking. I've done a bit of QCA work, but there is still a long way to go, and wasted half of Sunday trying to figure out a weird problem in KMail, but that is about all.