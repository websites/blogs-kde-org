---
title:   "SoC ideas"
date:    2008-02-29
authors:
  - brad hards
slug:    soc-ideas
---
Thiago has set up the <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas">KDE Summer of Code 2008 ideas page</a>. Thanks!

<!--break-->

I've put some ideas on there (far more than I could possibly mentor), and I'd like everyone else to do so too. The reason is that ideas are fairly cheap, and we need to get students inspired. From previous years, some of the best ideas came from students (not from the mentors' list). In addition, just about every idea on the "list" had a submission (which was sometimes just the name of the idea).

If you have ideas, put them down. I'd like to see around a hundred ideas.

However please note the "fine print" at the bottom of the wiki page. I'll restate it here:
* Before making any modifications, please log in to Techbase. This will help us track who is contributing to the ideas. 
* When making modifications to existing ideas, please consider whether you're changing it more fundamentally or just superficially. If your changes are substantial, you probably have an entirely new idea. Similarly, if your idea is modified and you feel it no longer reflects your original thought, please split the idea in two, restoring yours.

An idea needs to be more than a heading - if a potential student only needs the heading, then it probably wasn't really worth adding. The wiki page says:
* if the application is not widely known, a description of what it does and where its code lives 
* a brief explanation 
* the expected results 
* pre-requisites for working on your project 
* if applicable, links to more information or discussions 
* mailing list or IRC channel for your application/library/module 
* your name and email address for contact (if you're willing to be a mentor)

If you want to be a KDE SoC student in 2008, then you can do a few things to help yourself. Things to think about:
* SoC is a job, and you are basically going to have to write a job application. Start the process early. Last minute work always shows.
* Experience will help - you will help your application and your progress if you have a working KDE4 environment.
* At least basic C++ and Qt skills are going to be required for most KDE programming projects - make sure you've at least completed the Qt tutorial.
* Good jobs often come from knowing people, who tell you about things that you might not otherwise be able to find out. Getting known inside the KDE community (e.g. at least subscribe to the mailing list specific to your chosen project) is almost certainly going to help, as long as it isn't for something really bad (like poor behaviour).

Good luck!
