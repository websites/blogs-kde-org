---
title:   "KRDC, QCA, etc"
date:    2007-08-08
authors:
  - brad hards
slug:    krdc-qca-etc
---
I've been meaning to blog for a while, and finally managed to get there.

I'd like to start off by pointing out the awesome work done by Urs Wolfer on Krdc (or KRDC, if you prefer) as his Google Summer of Code project. I sometimes wonder if it might not deserve a new name given how much new code has gone into it. If you didn't catch it, I definitely recommend reading <a href="http://commit-digest.org/issues/2007-08-05/"> last weeks Commit Digest</a> for a description of the changes and some nice screen shots of the new user interface.

I'm very proud to have been slightly associated with this project, nominally being the mentor. I say "slightly" and "nominally", because pretty much all I've done is sit back and admire the progress.

In my own stuff, I've made some progress on the akonadi resource for Microsoft Exchange servers, using <a href="http://openchange.org">openchange's</a> libmapi. Not ready for use at this stage, even if I did have a solution for the GPLv3 issue in Samba 4.

What is getting closer though is the "test1" release of QCA. I noted <a href="http://bertjan.broeksemaatjes.nl/node/13">Bertjan's work</a> introducing QCA into KPilot, and some problems that he had. We've certainly been working on the documentation, but perhaps still have some way to go on this. In the mean time, I've put the Doxygen API docs up at <a href="http://www.frogmouth.net/qca/apidocs/html/index.html">http://www.frogmouth.net/qca/apidocs/html/index.html</a>
They are also available at <a href="http://api.kde.org">http://api.kde.org</a> thanks to the nice people at the <a href="http://www.englishbreakfastnetwork.org">EBN</a>.
If you've got comments on the use of QCA, we'd love to know more about what problems you had and what you did and did not like.


