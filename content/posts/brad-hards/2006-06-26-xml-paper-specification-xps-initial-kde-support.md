---
title:   "XML Paper Specification (XPS) - initial KDE support"
date:    2006-06-26
authors:
  - brad hards
slug:    xml-paper-specification-xps-initial-kde-support
---
Microsoft have been working on a PDF competitor, call XPS (for [w:XML Paper Specification|XML Paper Specification]). Basically, it gives you a way to represent a printable document (as in a traditional document made up of a number of pages) in XML. 

It is a bit too complex for my taste, but appears to be generally well thought out, and there appear to be genuine attempts to reuse instead of re-inventing (e.g. meta-data mostly comes from Dublin Core, image formats have to comply with the PNG spec, JPEG spec, TIFF specs as appropriate, embedded fonts are basically just standard true-type formats, and it is all wrapped up in a standard Zip file).

I decided to take a short break from messing with cryptography and do a bit of more "fun" coding - I wrote a thumbnailer (kde:ThumbCreator) for the XPS format. I took the opportunity to try a few things I'd been meaning to look at for a while, and decided to use KDevelop (I'm normally an xemacs + command line user). It also gave me a chance to learn a little more about Qt's XML classes (in my case, QDom*).
<!--break-->

Here is a first cut, rendering some of the example files that Microsoft provide (with weird click-through licensing):
[image:2140 size=preview]
(yep, click on the image for a bigger version)

It isn't quite right yet, so no check-in. Plus I did it with KDE3/Qt3, just because it compiles and runs :-) The next stage might be to do some meta-data work, at which point it might be time to start on a proper viewer (probably though okular or kviewshell). It might also be possible to do a fairly simple import filter for KOffice....

Overall, I'm very happy with KDevelop - it seems much improved since the last time I tried it (perhaps 12-18 months ago). The Fedora Core 5 virtual machine I do my development on isn't set up as well as I'd like (e.g. the documentation links in KDevelop aren't working), but the workflow is very nice. Congratulations to all involved!

The other thing that I've noticed is that kde:KZip doesn't appear to have any Zip64 support. The KArchive classes could use a bit of love during KDE4 - if anyone is working on them already, or is planning to, please let me know so we could perhaps coordinate effort.

I really enjoyed working on this little project. If you are feeling a bit burned out with some boring porting work, perhaps a couple of days doing the fun stuff might be the right solution for you too?