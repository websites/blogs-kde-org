---
title:   "KDE Associative Desktop"
date:    2004-02-08
authors:
  - brad hards
slug:    kde-associative-desktop
---
I recently read a Mac magazine (borrowed from the local library - I wouldn't have bought it :)) that had an interesting idea about extending iCal into an associative tool. 

That got me thinking about how we can make a better user experience along similar lines. As an example, if I'm writing a letter in KWord, it might be useful to find other letters to the same client. Or emails from that client, or records of meetings with that client.  

But it doesn't need to be client based. If I'm looking at a letter that I wrote some time ago, and I remember that some of the content was the result of a spreadsheet (but I have no idea what the name of the file was), it would be nice to have KDE figure out which files it might have been. If a document has a photograph, it might be very useful to get a list of other photographs that were taken at about the same time. I'm sure there are other useful associations, but I see a key role for the address book and calendaring apps, since they are the tie-in parts that have meta-data associations. 

This isn't a feature for occasional users. This is a feature that makes regular users more productive.

KDE, the associative desktop.

There are really three problems that I see in providing this level of associative power:
1. You need good quality meta-data from over a fairly long period before it becomes really valuable, and that is hard unless there is some way of getting the data recorded automatically. 
2. You need powerful useful interfaces to query that meta-data.
3. You need to present the data in an intuitive and flexible way.

Each of these issues can be resolved. We do have good meta-data for some applications, especially some of the PIM applications. We certainly have the pattern for a query interface through the mighty DCOP. Presenting the query results is a harder problem, and it would
be nice to have some help from a psychologist who can explain how people naturally associate information, which should show us what the useful meta-data associations might be, and feed that into the UI design.

In the longer term, we need to figure out what additional meta-data is going to be required. Most applications have some support for meta-data, but we may need to find ways to make sure that users put the data in, or that we can at least guess what the right values
should be. This might take the form of more wizards or templated KOffice documents, for example.
