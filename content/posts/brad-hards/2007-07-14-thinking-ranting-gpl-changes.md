---
title:   "Thinking / ranting on the GPL changes"
date:    2007-07-14
authors:
  - brad hards
slug:    thinking-ranting-gpl-changes
---
I have been working on providing Microsoft Exchange capability for <a href="http://pim.kde.org/akonadi">Akonadi</a> on-and-off for a while. It is coming together - I can suck messages off the server into akonadiconsole. More on that later (or as part of the commit-digest, perhaps).

There is an emerging problem though. The akonadi provider relies on the <a href="http://www.openchange.org">OpenChange</a> libraries, which in turn rely on <a href="http://www.samba.org">Samba</a> 4. That was all fine until the GPL version 3 release. The samba team <a href="http://news.samba.org/announcements/samba_gplv3/">has decided to adopt GPLv3</a>.

The problem is that GPLv2 and GPLv3 <a href="http://www.fsf.org/licensing/licenses/gpl-faq.html#v2v3Compatibility"> are not compatible</a>. My code is fine - it is "GPLv2 or later". The incompatibility is that Qt is released under GPLv2 (not "or later") - see the top of LICENSE.GPL in your Qt4.

Based on my understanding, that means that KDE4 can't use anything from Samba4 (or Samba3.2+) and Qt4 in the same application.

This is not purely an issue with Samba - GnuPG is has also <a href="http://lists.gnupg.org/pipermail/gnupg-announce/2007q3/000255.html"> switched to GPLv3</a>, and I'm guessing GPGME will follow. I'm sure there are other libraries that are switching to GPLv3, and yet others that are GPLv2 only.

There are technical solutions to some of these problems - for example, the openchange resource (which is a separate application) could avoid using anything from Qt/KDE; we could link the smbclient kioslave to the Samba 3.0.25b (forever); and we could find other ways to use GnuPG without GPGME. All of those require precious programmer time, and restrict our options for the future.

I'm not sure I care enough about exchange support to develop so much from scratch. Maybe it would be easier to just delete the code - it will still be in SVN if anyone wants to try.

Its a pity that the FSF couldn't find a way to make GPLv2 and GPLv3 compatible.

I hope the GPLv3 really is good enough to justify this impact.