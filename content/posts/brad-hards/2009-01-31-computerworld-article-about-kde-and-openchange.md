---
title:   "Computerworld article about KDE and OpenChange"
date:    2009-01-31
authors:
  - brad hards
slug:    computerworld-article-about-kde-and-openchange
---
Rodney Gedda of Computerworld Australia has written an article about my talk at linux.conf.au 2009.

You can see it at <a href="http://www.computerworld.com.au/article/274883/openchange_kde_bring_exchange_compatibility_linux?fp=16&fpid=1">http://www.computerworld.com.au/article/274883/openchange_kde_bring_exchange_compatibility_linux?fp=16&fpid=1</a>
and
<a href="http://www.computerworld.com.au/article/274883/openchange_kde_bring_exchange_compatibility_linux?pp=2&fp=16&fpid=1">http://www.computerworld.com.au/article/274883/openchange_kde_bring_exchange_compatibility_linux?pp=2&fp=16&fpid=1</a>

(sorry for the ads, but presumably that is what pays for the site).

I'm not sure it is an entirely accurate reflection of what I intended to say (e.g. “I would not recommend Akonadi in production use,” isn't what I was trying to say - I was talking about the Akonadi OpenChhange resource not being production quality yet, rather than all of Akonadi). I offered to review the article before publication, but no such luck.


