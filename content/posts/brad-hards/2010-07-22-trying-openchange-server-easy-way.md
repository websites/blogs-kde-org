---
title:   "Trying OpenChange server, easy way"
date:    2010-07-22
authors:
  - brad hards
slug:    trying-openchange-server-easy-way
---
OpenChange is an important project, but it does require quite a lot of work to get it all to build. We're working on the process, but in the mean time, we've (ok, Julien Kerihuel with nothing from me except encouragement) has built a Virtual Box image that provides OpenChange all built, configured, set up and ready to try.

See <a href="http://tracker.openchange.org/projects/openchange/wiki/OpenChange_Appliance">http://tracker.openchange.org/projects/openchange/wiki/OpenChange_Appliance</a> for the download (ftp or rsync) location and setup procedures.

Have fun, and let us know how it goes!