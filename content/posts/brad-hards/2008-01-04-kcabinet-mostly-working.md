---
title:   "KCabinet - mostly working"
date:    2008-01-04
authors:
  - brad hards
slug:    kcabinet-mostly-working
---
I finally got the KCabinet class (in <a href="http://websvn.kde.org/trunk/playground/libs/kcabinet/">playground/libs/kcabinet</a>) working.

To make any sense out of this, you need to know that the <a href="http://msdn2.microsoft.com/en-us/library/bb417343.aspx">Microsoft Cabinet format</a> is based on blocks of data (the CFDATA block) that end up being <= 32768 bytes. There are four ways that the data is packaged - uncompressed, MS-ZIP format, Quantum and LZX. I'm mainly interested in MS-ZIP, which is basically the <a href="http://en.wikipedia.org/wiki/Deflate">deflate algorithm</a> with a little header (similar to how gzip works). With MS-ZIP, you get around 2K of compressed data in each block, which expands to 32768 bytes in each block - except the last one of course.

So what I was doing was using <a hef="http://www.zlib.net/">zlib</a> to inflate() each block. The problem is that when a single file extends over more than one block, zlib wouldn't actually decode anything beyond the first block - it just returns <tt>Z_STREAM_END</tt>, which makes sense (since each block is complete), but didn't ever write beyond the first of uncompressed output (i.e. 32768 bytes of good output, followed by whatever I initialised the QByteArray to.

I tried lots of things. I switched to using zlib directly (instead of through KFilterDev), I tried various options to the zlib functions. I pulled what is left of my hair out. I considered importing the <a href="http://www.cabextract.org.uk/libmspack/">libmspack library</a>, or a modified copy (e.g. from <a href="http://www.samba.org">Samba</a> or <a href="http://www.clamav.net/">ClamAV</a>).

In the end, I asked the zlib authors. <a href="http://alumnus.caltech.edu/~madler/">Mark Adler</a> (possibly not an up-to-date site) came back overnight with the right answer (despite never having tried it): the previous 32K block as a dictionary.  You can use inflateSetDictionary() to set the dictionary
for the next block before decompressing.

Magic!

Essentially the sequence that works for me is:
<pre>
inflateInit2( streamState, -MAX_WBITS );
for each block:
        parse the header (including the CK prefix on the data);
        read the compressed data, and add that the streamState;
        inflate( streamState, Z_SYNC_FLUSH );
        copy the decompressed data to the output buffer;
        inflateReset( streamState );
        inflateSetDictionary( streamState, decompressedData, decompressedDataSize );
inflateEnd( streamState );
</pre>

I never would have got that without the assist.

We still might need mspack (if we ever need LZX or Quantum), but zlib is already a dependency for KDE, and it works for me :-)

Next step is to follow Aaron's advice, and refactor the code to make sure it is easy to understand and robust. I am beyond the experimentation stage now, and I've got the unit tests to make sure it doesn't go bad.
