---
title:   "Qt Cryptographic Architecture - approaching final API"
date:    2007-08-22
authors:
  - brad hards
slug:    qt-cryptographic-architecture-approaching-final-api
---
Justin Karneges recently released the "test1" version of QCA. <a href="http://lists.affinix.com/pipermail/delta-affinix.com/2007-August/001005.html">Read the announcement here</a>. There will be a "test2" release soon, and the final release is scheduled for 4 September 2007.

If you have an interest in crypto, or Qt APIs, or (ideally) both, note that we are providing Binary Compatibility and source compatibility from QCA 2.0.0 release for the duration of Qt4. If the API doesn't do what you need, now is the time to tell us. After 2.0.0 we will do additions, but nothing that will break BC.

The plugins aren't finished, but they are getting closer. In particular, the pkcs11 smartcard provider is looking pretty good. <a href="http://delta.affinix.com/2007/08/21/qca-and-smartcards/">Justin recently blogged about smartcard support</a>, showing an application that does CMS signing using smartcards with QCA.

You can read the API documentation at <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/qca/html/">api.kde.org</a>, and we also provide build documentation with QCA tarballs. The tarball API docs (or the output if you run doxygen(1) in the kdesupport/qca directory yourself) also include links to the example code.  

Remember: now is the time to let us know of any last minute changes!