---
title:   "KCabinet"
date:    2007-12-17
authors:
  - brad hards
slug:    kcabinet
---
I've just checked in the start of a new class - KCabinet. Its in playground/libs/kcabinet for the morbidly curious. I'm not sure if it is really going to be right for kdelibs - need to get it a bit more mature before needing to make that decision.

The format is reasonably well documented, at <a href="http://msdn2.microsoft.com/en-us/library/bb417343.aspx">http://msdn2.microsoft.com/en-us/library/bb417343.aspx</a>, and there are other tools that can read it.

This is the first time I've really set out to do test-driven development of a class. It is interesting in the "confidence" that it brings to have even a few tests that exercise the code you've written. It is also amazing how it changes your thinking about design and implementation (rather than about testing). In this case, I'm subclassing KArchive, so there isn't so much design, but the effect is still there at a lower level.

Of course, right now I'm stuck. I'm trying to parse out the date and time (into a q:QDateTime), and it all works. Except that one field (the minutes) are wrong, and I can't see why. Argh!

Other than that though, TDD really does add quite a lot of enjoyment to development.