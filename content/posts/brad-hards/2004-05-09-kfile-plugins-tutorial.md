---
title:   "KFile plugins tutorial"
date:    2004-05-09
authors:
  - brad hards
slug:    kfile-plugins-tutorial
---
After a bit of a delay, I've finally finished off the KFile plugin tutorial. Please review and patch!
<!--break-->

The tutorial is up on <a href="http://developer.kde.org">http://developer.kde.org</a>, as  <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/t1.html"> http://developer.kde.org/documentation/tutorials/kfile-plugin/t1.html</a>. I've had part of it up for a while, but not linked or built from the SGML source into HTML.

I see this documentation as mainly of use to people who don't have a lot of time to work on KDE, but want to do something to help. The KFile plugin interface is pretty easy to  understand, and you don't need to understand most of the KDE or Qt classes to get started, although obviously you can end up doing things the hard way if you don't know of the magic of Qt+KDE. 

Those people can then spread their goals (and use of more of their time :) ) working on other parts of KDE that interest them.  I hope that, in my small way, I've reduced the barrier to entry for at least a few people. In the longer term, I'd like to see more tutorials on various plugins (for example, kimgio is a candidate), that both make it easy for people to get involved, and allow us to support a very wide range of file formats and applications.