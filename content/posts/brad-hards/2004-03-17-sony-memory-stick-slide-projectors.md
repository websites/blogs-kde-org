---
title:   "Sony memory stick slide projectors."
date:    2004-03-17
authors:
  - brad hards
slug:    sony-memory-stick-slide-projectors
---
A friend of mine got access to a Sony projector. One of the things
that was interesting is that it has a memory stick slot on it, and it
can project directly from the memory stick. Turns out that you have to
process the presentation to make it work though, and Sony provides
such software. For Microsoft Windows + Microsoft Office. We'll have to
see about that!
<!--break-->
I've never used the Sony software, but a friend was kind enough to
provide some sample files. Konqi and the JPEG kfile-plugin showed that
it turns the files into 1024x768 baseline encoded JPEGs. KHexEdit
showed that there is a master file for each presentation that includes
the number of slides, the filename of the title slides (which are added)
and the filename of each of the JPEG encoded slides.

A quick trip into KDevelop (which really needs a template for "gimme a
plain old command line application, with KDE support") produced a
junkcode viewer for the file format, despite not being able to get
KRun to work. 

A subsequent visit to KDevelop produced a partial writer, and I'm
currently procrastinating rather than finishing it off. Maybe over the
weekend, since I'm going to the linux.conf.au 2005 organisers meeting
tomorrow night.

The long term goal is to integrate this functionality into KPresenter,
as "Export to Sony Memory Stick...". Shouldn't be hard - modify the
HTML export wizard, save to JPEG instead of PNG, and write out the SPP
master file.

There are a few subtleties, including handling the case where there
are already a couple of presentations on the memory stick, and you
want to add another one. Also the master file is fixed length at 16384
bytes, and I'd like to know what happens when you have 249 slides,
since the 249'th filename won't fit into the 16384 byte file.

I do have a couple of issues that I'd appreciate some help on:
1. Is there a reasonably portable way to find out where the memory
stick is mounted? If so, I'd like to use that as the default location
to save to. Parsing /etc/fstab isn't going to make it....
2. Is there a KDE/Qt equivalent to mtools? I'd prefer to be able to save
to the memory stick even if it isn't mounted.
3. Is the cleanest way to create an arbitrary binary file to use a
qt:QByteArray and then just stream the whole thing out? Or to write bits
of it at a time?