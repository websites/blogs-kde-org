---
title:   "More XML paper specification stuff"
date:    2006-09-09
authors:
  - brad hards
slug:    more-xml-paper-specification-stuff
---
Despite being very tired from my day job, I managed to get it together enough to put together a kfile-plugin (metadata support) for the XML Paper Specification format. It pulls out whatever data is available (sometimes not much, but thumbnails are reasonably common). It is committed into trunk (in kdegraphics/kfile-plugins/xps)

The work required to get to this stage is mainly XML references to XML files containing more XML references. Hopefully I can reuse it to work up more sophisticated support, including document viewing.

There are a couple of known problems, including not having Zip64 support in KZip, and a nasty bug associated with interleaved Zip files. I'd appreciate a clue for this one:
<code>
    KZipFileEntry* relFile = static_cast<const KZipFileEntry *>(xpsArchive->directory()->entry("_rels/.rels"));

    if ( !relFile ) {
        return false;
    }

    if ( relFile->name().isEmpty() ) {
        return false;
    }

    QIODevice* relDevice = relFile->device();
</code>

It crashes on the last line shown above. Any suggestions?