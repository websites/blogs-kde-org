---
title:   "More on plugging into GCC"
date:    2009-05-24
authors:
  - brad hards
slug:    more-plugging-gcc
---
Based on a comment from Taras Glek on my feeble attempts to get a GCC plugin going, I did some work on Dehydra / Treehydra. Its an interesting approach, and one that benefits from the GCC plugin API.
<!--break-->
Basically, you can write the checks that you want to do in Javascript (using the Spidermonkey engine from Mozilla). That is more powerful than the declarative approach that mygcc uses, although I'm still struggling a bit with it. Hopefully I can get the rest of the porting to GCC 4.5 done, and start writing checks that are actually useful.

It isn't the easiest thing to get into - the build steps involve a separate checkout from Mozilla of Spidermonkey (using Mercurial), some minor patches to GCC (which has to be built from svn), and then building dehydra and treehydra. Hopefully we can make that package-friendly by the time GCC moves to its release stage.