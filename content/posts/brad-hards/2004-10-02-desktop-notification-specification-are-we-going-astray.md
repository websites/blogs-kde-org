---
title:   "Desktop notification specification - are we going astray?"
date:    2004-10-02
authors:
  - brad hards
slug:    desktop-notification-specification-are-we-going-astray
---
I've been drifting around the various planets, and found a <a href="http://www.chipx86.com/blog/archives/000064.html">blog by Christian Hammond</a> in the  <a href="http://planet.freedesktop.org/">http://planet.freedesktop.org/</a> aggregation, where he talks about the <a href="http://galago.sourceforge.net/specs/notification/">Desktop Notification Specification</a> he is working on.

I think that the approach of the Desktop notification spec is wrong. We should be specifying the DBUS message in detail, and the intended usage. When it gets to the level of < b > tags and icons, we are going in the wrong direction.

A popup is just one option, and not especially good for some cases. As an example, if a USB device gets plugged in or unplugged, then we need an agreed DBUS message. What the desktop does can vary - it might load a special control app, it might show a popup, it might do a text-to-speech notification, or it might do some combination of those and something else.