---
title:   "KDE buildbot preview now available"
date:    2005-10-15
authors:
  - brad hards
slug:    kde-buildbot-preview-now-available
---
As Adriaan de Groot alluded to in one of his recent blogs, I've been working on a automatic builder for KDE. With much thanks to ade for hosting this, and for assistance in getting it set up, I can now tell you that the builder is up and running on http://www.englishbreakfastnetwork.org:8010 [Yep, kdelibs is broken right now]

At the moment, the buildmaster monitors kde-commits (through the magic of commitfilter.kde.org) and triggers an incremental build and a full build of QCA and kdelibs from trunk/ each time someone does a commit that might affect the build. The builds are done on a couple of my machines (an AMD64 and ia32 box, each running FC4). I have a couple of other buildslaves that I hope to run soon (one with the intel C compiler, and one on a Mac, but I have some more work to do to get those going properly).

The build does a checkout, followed by a configure, compile and unit test cycle. You can see the results, and get a full log as well. Results appear on the web page, and can also be mailed (for every build, every failure, or every failure that previously passed) and I hope will also be able to be delivered to an IRC channel. You can currently ask the bot (currently on #kde4-devel as buildbot-bradh) for the last build, or to force a build, and a few other things. RSS or Atom feeds are obviously desirable as well, but that is a little way ahead.

At this stage I'm looking for suggestions on how people would like this to work so we get the most value for it. Is there enough demand for a mailing list for build reporting? What should else should we check? Do we need multiple buildmasters to cover off different branches?

The build system is inherently distributed, and I'd appreciate access to machines with different configurations (hoping to get access to a PPC64 box soon). There are some security risks though - the buildmaster can ask the buildslaves to run arbitrary shell commands. I'm currently looking at virtualisation as a potential solution to this, even though the buildslaves only run as a normal user.

