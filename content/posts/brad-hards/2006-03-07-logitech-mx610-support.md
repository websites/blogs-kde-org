---
title:   "Logitech MX610 support?"
date:    2006-03-07
authors:
  - brad hards
slug:    logitech-mx610-support
---
I've been doing a bit of work on a Linux HID project, for a Point Of Sale scanner. I took a little bit of time out to look at the Logitech MX610 mouse (e.g. http://www.logitech.com./index.cfm/products/details/AU/EN,CRID=2135,CONTENTID=10917) again. Its looking promising.

The mouse is a bit strange - it has two logical "interfaces" for the USB receiver/transmitter. They are on the same plug, but they are treated as separate devices by the Linux USB subsystem. The first USB interface is the normal mouse device - it has the X/Y motion, the normal buttons and the scrollwheel. The five buttons down the left edge (Vol +, Vol -, Mute, Email and IM) are on the second interface, which probably has the additional features like battery status as well as the indicator LEDs for Email and IM.

I can now turn the Email lamp on solid, or flash on/off, or pulse (slow on/off/on/off transitions), or off. If you want to play with the test code, see:
http://www.frogmouth.net/mx610hack-0.2.tar.gz
(This version can only do the Email lamp, but extending it to do the IM lamp shouldn't be too hard once you have the right command sequences)

At this stage I'm thinking that the lamp control should be part of some notification daemon - perhaps KDE specific, perhaps more general. The special button handling should probably be part of khotkeys or its replacement. If anyone has more ideas on KDE integration, I'm happy to take suggestions!