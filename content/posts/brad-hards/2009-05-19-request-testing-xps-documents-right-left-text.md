---
title:   "Request for testing - XPS documents with Right-to-Left text"
date:    2009-05-19
authors:
  - brad hards
slug:    request-testing-xps-documents-right-left-text
---
Its been a while since I did anything productive in KDE land, so thought I'd try to do something in a morning. Its hard freeze time, so that should be bug fixing. <a href="https://bugs.kde.org/show_bug.cgi?id=185532">Bug 185532</a> was something I'd been thinking over for a while.

That bug basically deals with XPS documents that didn't render correctly with RTL text. It looked like a fairly standard Right-to-Left text problem, and I assumed there was just something I hadn't handled because none of the documents I look at are Right-to-Left. I am, after all, ignorant of both Hebrew and Arabic.
 
The fix turned out a bit more complicated, and I think also fixes up some other XPS rendering issues. If you have a collection of XPS documents, especially anything with Arabic or Hebrew text, and are OK with building KDE and applying patches, I'd appreciate any feedback. You can find the patch on <a href="http://reviewboard.kde.org/r/715/">KDE's ReviewBoard</a>


Guidance:
1. If your document works differently to an unpatched Okular, please add a comment at <a href="https://bugs.kde.org/show_bug.cgi?id=185532">Bug 185532</a>
2. If your document doesn't work well, and is about equally bad in both patched and unpatched Okular, raise a new bug.

Thanks!

Brad