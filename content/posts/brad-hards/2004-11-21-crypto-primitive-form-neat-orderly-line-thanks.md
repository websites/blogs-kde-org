---
title:   "Crypto primitive - form a neat orderly line, thanks"
date:    2004-11-21
authors:
  - brad hards
slug:    crypto-primitive-form-neat-orderly-line-thanks
---
It has been a while since I blogged - a bit too much real-world work, plus a concerted effort on the Qt Cryptographic Architecture (QCA) are to blame.

Right now, I have QCA2 running, I have two plugin providers working - one based on OpenSSL, and the other based on GNU libgcrypt, and I have 595 passing unit tests. I'm particularly proud of the unit tests, because they give me a lot of confidence in the plugins. In terms of functionality, we have hashing (MD2, MD4, MD5, SHA0, SHA1, SHA256, SHA384, SHA512 and RIPEMD160), keyed hashes (HMAC-SHA1, HMAC-MD5 and HMAC-RIPEMD160) and some ciphers (AES128, AES192 and AES256 in ECB, CBC and CFB modes). Supporting that we have a basic random number provider, a secure byte array, and some arbitrary precision integer classes. It isn't really ready for prime time, but its emerging nicely.

Future work will include SSL/TLS, X509 certificate handling, Simple Authentication and Security Layer (SASL), RSA, password-based key derivation ( RFC2898 ) and some additional ciphers (DES, 3DES and Blowfish).

If your KDE app needs anything more (in terms of crypto) than those things, please let me know what they are. In particular, if SMIME or PGP support would make sense for you, please describe the API you'd like.

And of course, more help is always appreciated - check out kdesupport/qca ...