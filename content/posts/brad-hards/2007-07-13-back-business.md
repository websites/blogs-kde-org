---
title:   "Back in business"
date:    2007-07-13
authors:
  - brad hards
slug:    back-business
---
A (another) big thanks to the KDE sysadmin team for getting kdedevelopers.org back in business.