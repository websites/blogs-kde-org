---
title:   "KDE + OpenChange + others in GSoC"
date:    2009-03-20
authors:
  - brad hards
slug:    kde-openchange-others-gsoc
---
I was happy to see both <a href="http://socghop.appspot.com/org/show/google/gsoc2009/kde">KDE</a> and <a href="http://socghop.appspot.com/org/show/google/gsoc2009/openchange">OpenChange</a> (and some <a href="http://socghop.appspot.com/program/accepted_orgs/google/gsoc2009?limit_1=100&offset_1=0">lesser projects</a> :-)) were accepted for this year's Summer of Code project.

OpenChange is progressing quite well - Julien now has a server running that supports Outlook logging in, and returning the folder list. If that is the sort of thing you'd like to be involved in, check out the <a href="http://wiki.openchange.org/index.php/SoC/Ideas">list of ideas</a>. 

I'm sure you can find the list of KDE ideas, but I'd like to point out my favourite idea - the Okular presentation recording. 

A few thoughts about applying:
- A detailed, well prepared application is critical.
- You need to make contact early (pick a specific mailing list for KDE).
- Your own ideas are frequently a better approach than one from the list, as long as it is sane and novel (at least it will stand out on KDE). If you need a mentor for a specific idea, make sure you have one lined up.
- Your chances of getting picked are generally better on a smaller project (like OpenChange) than on a really big project (like KDE). KDE only gets slots for about 10-20% of its applicants. The risk is a smaller project might get a couple of great applications and only has a very small number of slots. Even a good application could come in at n+1, where n is the number of slots. 
- There is no magic recipe, but treating Summer of Code as a job is probably the best concept. So this is really a job application.

Good luck, and remember to get started early. Applications open 19:00 UTC on 23 March 2009!

