---
title:   "foo->update()"
date:    2006-07-05
authors:
  - brad hards
slug:    foo-update
---
I'm in Melbourne (Victoria, Australia) this week for work.

I'd hoped to catch up with Hamish Rodda, but he's in Norway. Sounds like the flights (36 hours?) might have been a bit harsh, but there is certainly a lot of interesting stuff coming out of Trysil.

I've been playing with the <a href="http://doc.trolltech.com/4.2/graphicsview.html">Qt 4.2 Graphics View Framework</a>. It really is very nice. I'm having a few conceptual problems (like understanding exactly how the coordinate system works; getting segmented/spanned ellipses to work; and understanding how foreground/background on the view works, as opposed to the scene) but they might be a combination of residual bugs or documentation problems. I really need to get into the code - one of the fantastic things about Qt is that when you really want to know, it is all there.

I had intended to try to get kdelibs building on my new laptop, but it looks like I'm not going to be able to until the qt-copy issues (that is, some of the dbus tools won't build) are resolved.

Anyway, back to the takeaway Lamb Masala, and some more coding....

