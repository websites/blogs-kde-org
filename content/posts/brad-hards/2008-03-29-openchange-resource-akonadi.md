---
title:   "Openchange resource for akonadi"
date:    2008-03-29
authors:
  - brad hards
slug:    openchange-resource-akonadi
---
Spent some time today getting the <a href="http://www.openchange.org">OpenChange</a> resource for Akonadi up and running again.  I haven't really done anything to improve it from where it was a few months ago (OK, September 2007, when I was on holidays), but it lives again.

Those few months have involved a lot of changes on both the OpenChange side and on the Akonadi side of this resource. However we can still read mail (kind-of) and show contacts (kind-of).

Screenshots, using the akonadiconsole tool:
[Image:3358], [Image:3359]
(click to see full size).

As you can see, we have a long way to go - the data is coming off the server, but we are faking some of the elements of the mail header (instead of parsing it and making a proper RFC822 multipart message) the image in the contact is br0ken, etc. It is a start to a proper resource though.