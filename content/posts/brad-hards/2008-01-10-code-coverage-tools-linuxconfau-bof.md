---
title:   "Code coverage tools - linux.conf.au BoF"
date:    2008-01-10
authors:
  - brad hards
slug:    code-coverage-tools-linuxconfau-bof
---
Recent development has left me feeling a bit unhappy with part of my toolset - <a href="http://en.wikipedia.org/wiki/Code_coverage">code coverage</a>. I'm not especially interested in metrics (except as a guide to where to start) - rather in the detail.

gcov does part of the problem, but it really only works at the level of "lines executed", and doesn't do much for shared libraries.

There are a few people interested in a BoF session about this, to occur at <a href="http://linux.conf.au">linux.conf.au 2008</a>. If you are interested, add your name and any comments to the linux.conf.au <a href="http://linux.conf.au/wiki/index.php?title=BOF_sessions#Test_Coverage">BoF Wiki page</a>

Look forward to seeing all you smart people there.