---
title:   "KDE4 porting - getting better"
date:    2006-07-20
authors:
  - brad hards
slug:    kde4-porting-getting-better
---
After Laurent suggested it might be dead, I decided to try to resurrect the lost soul known as KDE Remote Desktop Connection (KRDC, a.k.a krdc for the command line junkies), which provides Remote Desktop Protocol and Virtual Network Computer (VNC) support for KDE. I use it a bit to administer a Windows application server we have in the office, and really didn't want to be without it for KDE4.

The porting is going reasonably well - I'll blog more about KRDC progress (including screen shots of it running :-)) a bit later. 

However I did want to point out that KDE4 development has recently got a lot easier. After spending weeks trying, the recent versions of kdelibs and kdebase not only compile - they also result in mostly-running applications. I've even managed to get Konqueror running. 

That isn't to say that there aren't still a lot of bugs (since a few things broke in the port), and there are still a few warnings about deprecated code, and I'm sure that there will still be a lot of changes to the API in kdelibs. However it definitely looks like things are stable enough to get back into development, especially if you have a reasonably fast spare machine (or can use a virtual machine - I'm having good success with vmware server on my laptop).

There are still a lot of applications (mainly outside of kdebase and kdepim) that haven't been ported yet - you can get a list when you run cmake in the top level module directory. If you are planning to start porting, then I found that a copy of the KDE3.5 API documentation, a copy of the KDE CVS API documentation (both available from http://developer.kde.org) Qt4.2 documentation (from http://doc.trolltech.com) and frequent reference to the KDE4PORTING.html file in the top directory of the kdelibs module were very useful.

The great thing about porting is that you get a broad overview of the various API changes in a very short time. And it is really rewarding to see an application running again :-)

Good luck and have fun. 