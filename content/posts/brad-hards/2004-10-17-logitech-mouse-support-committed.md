---
title:   "Logitech mouse support - committed"
date:    2004-10-17
authors:
  - brad hards
slug:    logitech-mouse-support-committed
---
I've been meaning to add special Logitech mouse support to the mouse KCM for a long time, and I've finally gotten it done. If you have Wheel Mouse Optical, MouseMan Traveler, MouseMan Dual Optical, MX310 Optical Mouse, MX510 Optical Mouse, MX300 Optical Mouse, MX500 Optical Mouse, iFeel Mouse, Mouse Receiver, Dual Receiver, Cordless Freedom Optical, Cordless Elite Duo, MX700 Optical Mouse, Cordless Optical Trackman, Cordless MX Duo Receiver, MX1000 Laser Mouse or Receiver for Cordless Presenter, you should give it a try. It can give you the battery status and RF channel on cordless devices, and resolution switching on other devices.

It would also be possible to provide some support for "Smart Scroll" aka "Cruise Control" - you can enable and disable the special scroll buttons, but some mice (eg MX700 and maybe the Cordless Optical Trackman) can't tell you whether it is currently enabled or not, so I don't have a good UI design. If you know Mouseware from Logitech, and there is soemthing that you see there that could help under KDE, provide the design!

If you are a Linux packager, there are a couple of files that need to go into /etc/hotplug/usb. Get in contact with me if you have a problem with this.

The code should be portable to a BSD system and MacOS, since the USB stuff is implemented through <a href="http://libusb.sf.net">libusb</a> which is portable - apparently even to windows. However the permissions may need to be adjusted - I have no idea how to do that outside Linux, sorry. Of course, you need to have libusb installed...

