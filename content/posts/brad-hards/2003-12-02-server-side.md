---
title:   "Server side"
date:    2003-12-02
authors:
  - brad hards
slug:    server-side
---
I've been thinking about what KDE is. Or what it means to be a "desktop environment" at all. KDE has many faces (and we're not just talking about kde:KJanusWidget :). The windowing manager (and associated KDE Panel) is the public face, but the infrastructure in
kdelibs that makes the key applications (KMail, Konqueror, konsole, kdevelop, and lots more) not just possible, but consistent and reliable. The toolset in kdevelop, and the developer community are key parts of KDE too.
<!--break-->
But what is a client server architecture without servers? Maybe we should have a K Server Environment which provides excellent support for the K Desktop Environment. I'm thinking of something that is based around the Kolab project, and extends it out using some of the other
key server projects (eg Samba). The development environment would need to include a no-X version of Qt and some of the kdelibs, plus perhaps some of the networking code. This would be a good time to split kdelibs into a couple of parts. I guess the model I have in mind is
something like the E-Smith (now Mitel SME server, recently orphaned) distro, which provided a uniform approach; although implemented with more of the LDAP concepts of Kolab, and without the half-firewall content (since the firewall should be dedicated IMHO - the K Server would be hardened to live in the DMZ if required).

The general idea would be that you have a workgroup server supporting a set of machines with KDE. The underlying operating system would be inherently irrelevant - this server could be on Linux, BSD, Solaris, whatever; although I'm drawn to Linux or BSD as for the auto-install idea. The installation would be mostly automatic - you put the CD or DVD in, you confirm you really do want to trash the current contents of the machine, and it installs. At the end, a short wizard walks you through the essentials of post-installation configuration. There are a range of meta-distributions that can already support this, although some tweaking may still be required.

The advantage here is that we can tune the server side to optimally support whatever the desktop would ideally use. Sure, other servers would work, perhaps even as well, but the K Server Environment (KSE? KDE-Server? KServ?) would be a consistent, tested platform.