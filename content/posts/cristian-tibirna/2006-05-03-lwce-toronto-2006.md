---
title:   "At LWCE Toronto 2006"
date:    2006-05-03
authors:
  - cristian tibirna
slug:    lwce-toronto-2006
---
With a 7-days delay, here is what I still remember ;-) of my activity at <a href="http://www.lwnwexpo.plumcom.ca/">LWCE Toronto 2006</a>.

I went there pushed by George Staikos (who couldn't go this year) and sponsored by the organizers (<a href="http://www.plumcom.ca">Plum Communications</a> of Canada).

The initial deal almost was for me to give a conference and to help 
keeping up a KDE booth. The final deal was me giving a 3h tutorial: <a href="http://ktown.kde.org/~ctibirna/2006-lwet/lwet2006-tibirna-kde-desktop-tutorial-FULL.pdf">Why KDE for the desktop</a> and a 1h conference:
<a href="http://ktown.kde.org/~ctibirna/2006-lwet/lwet2006-tibirna-desktop-hold.pdf">Take a Hold of the Rapidly Maturing Linux Desktop</a>. Both these were on April 25th. 

I also had to drop the idea of the booth, because it wasn't possible for me (work constraints) to stay on 26th of April too.

The tutorial and conference were interesting. I didn't have full rooms (I think the conference as a whole was a bit less active than usual). I found the experience interesting. 

After the tutorial, came to me two engineers from Xerox, who showed to me the new printing driver dialog that they write for Linux, which their colleague, David Salgado, already mentioned to me at the <a href="http://blogs.kde.org/node/1936">Atlanta printing summit</a> but didn't have the means to demo it to me.

Also, the president of a local software development company (Advidi of Nepean, Ontario), came to me after the tutorial and told me I helped him a lot to make a better idea and inform his choice for a Linux development framework. I strongly hope that I helped him choose KDE ;-).

In Toronto, I met <a href="https://blogs.kde.org/blog/70">Waldo Bastian</a> twice in the same month, after knowing him for years and not meeting him at all ;-) We had very interesting chats about KDE, of course. Thanks again for that excellent evening, Waldo!

OK, now that head is above sink level again, let's prepare for <a href="http://www.cllap.qc.ca/2006/modules/smartsection/item.php?itemid=29">CLLAP 2006</a> (Linux in Government - local conference in Qu&eacute;bec).

<!--break-->