---
title:   "My leaving the bug reports discussion"
date:    2005-09-15
authors:
  - cristian tibirna
slug:    my-leaving-bug-reports-discussion
---
Wow! I'm glad I'm not in Calgary tonight, or <a href="http://aseigo.blogspot.com/2005/09/more-on-trust-based-bug-systems.html">Aaron might</a> have come down to my place and beat me up ;-) ) Anyways I will refrain from wielding back at him my (past century) sarcasm and irony weaponry.

I will just <a href="http://blogs.kde.org/node/1443">restate</a> that <i>"I must agree that some parts of this idea are useful and needed in an eventual process of improving the automatisation of b.k.o."</i> and I acknowledge that I need to overstate the implied: <i>in <b>some</b> situations, a trust-based automatic triage might not be the first choice</i>. I still think my intervention is useful, as it puts this one (of many) aspect of the argument in view.

Anyways, I'm glad there is strong healthy defending of ideas in this play. We can use a supplement of such attitude in our community (although things aren't bad on that front). (Still, I think that the intensity of Aaron's reaction proves that in his inner forum he knows I too am right ;-) )

Over and out.
<!--break-->