---
title:   "Here, there, everywhere"
date:    2006-03-31
authors:
  - cristian tibirna
slug:    here-there-everywhere
---
I will be to the <a href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing">Printing Summit</a> in Atlanta in a few days.

Then I will be at the <a href="http://www.lwnwexpo.plumcom.ca/">LinuxWorld Conference</a> in Toronto in a few weeks.

The work required before (and for) these is much more than I thought and planned initially. But it's worth it.
<!--break-->