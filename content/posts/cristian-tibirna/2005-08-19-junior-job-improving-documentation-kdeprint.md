---
title:   "Junior job for improving documentation of KDEPrint"
date:    2005-08-19
authors:
  - cristian tibirna
slug:    junior-job-improving-documentation-kdeprint
---
<a href="http://lists.kde.org/?l=kde-print&m=112448485820419&w=2">This message</a> says it all. This is a really easy opportunity to start contributing to your favorite open source project. 
