---
title:   "Summary display in KMail"
date:    2005-08-20
authors:
  - cristian tibirna
slug:    summary-display-kmail
---
Since a few days, I feel the lack of a summary mode in KMail.
<!--break-->

I have many tens of folders and subfolders, on three accounts (local, work, uni) and I have KMail filters putting things al overy this stuffy arborescence.

Instead of the "Welcome to KMail" screen that I get when clicking on "Local Folders" or the empty view that I get when I click on an IMAP server's arborescence root, a list of folders that contain new messages and how many, or even the headers of the new messages, would be nice to have.

I know that Kontact has something like this but I didn't manage to get that working and Kontact is a bit overkill for my needs.

I didn't look in the b.k.o's wishes for KMail.

Well, KDE4 is still quite a bit away in the future. The nice KMail developers will perhaps get to this one too.