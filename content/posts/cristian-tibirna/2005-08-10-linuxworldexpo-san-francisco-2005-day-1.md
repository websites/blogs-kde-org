---
title:   "LinuxWorldExpo San Francisco 2005 - day 1"
date:    2005-08-10
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-day-1
---
Here is the story of my day 1 at LWE-SF.
<!--break-->

The morning:
Finally, the important part of my trip here is starting. Immediately after the breakfast, we meet all (Till Kamppeter, Kurt Pfeifle, Uli Wehner, Patrick Powell, David Suffield, George Liu, and me) at the booth for the latest preparations.

Raph Levien makes his entrance with a large Epson inkjet printer with which he will demo photo printing for day 1.

We arrange things around. George installs a little projector. Patrick makes a few more tests with booklet printing on the Ricoh printer and things start to work. It's quite something to see a PostScript programmer at work ;-).

The other booths around us have started finally to get a bit of activity. Only the KDE booth and the linuxprinting.org booth (and perhaps the Xorg one) were prepared from yesterday.

Charles Samuels is already there. The KDE booth has two computers, a few leaflets printed on the printers from the linuxprinting.org booth and, later in the day, SimplyMepis bootable CDs and a few books. The KDE booth is unfortunately less populated with both developers/presenters and general stuff than the Xorg, the Debian, the Gentoo or the Gnome booths that are just nearby.

The Xorg booth has really big TFT and plasma displays. They are demo-ing some multimedia platform (didn't look closer yet, but it has to be X-MAS) so the music is loud all the time. Debians and Gnomes have t-shirts for sale.

Still, many people stop by the KDE booth and declare that they use and like KDE, or ask questions. There is the occasional teen that comes up with the usual banter: "I don't use KDE because it's bloated and overconfigurable and it takes 500M or RAM". But a few nicely directed questions are enough to manage them.

We also have lots of visitors at the linuxprinting.org booth. I didn't have anybody all day complaining (or even mentioning :-) kdeprint, so I gather all things are OK on this front. Just kidding ;-)

I have a few short discussions with George and Till about adding support for "string", "enum" and "password" CUPS commands in kprinter. George demonstrates to me on the large Ricoh printer the lockprint feature that would make use of these types of commands.

Just before midday, I go downstairs to do a round of the commercial exposition floor. The biggest booths are those of IBM, AMD and Novell. All is very loud, very flashy, so commercial. Most of the displays are showing KDE, apart from the RedHat booth (hmm... I didn't really see displays there...) and from the Novell booth. In the Novell booth, probably a bit more than half of the screens showed Gnome. I asked one of the presenters there and he said that's what <i>he</i>'s used with.

One strange thing going on there was some sort of caps contest. Novell was giving away OpenSUSE caps, light green with the gecko on them. RedHat was giving away red caps. And people were encouraged to wear them (there was some promised giveaway). So, all the floor was full of people wearing green and red caps. Green was winning for whatever reason.

The afternoon:
Enough with visiting the commercial part (when I also got to see Tux photographed with Batman and Wonderwoman, I decided it's time to go).

Going back upstairs, I replace Charles for about an hour, time to get quite a few questions and comments from visitors. There was one in particular that came by and said: "well, no comments, no questions, I use KDE and all works so well I don't have anything to ask". Thanks for the compliment ;-)

Late in the afternoon I had dinner with David, Till, Raph and Uli, a good occasion for nice technical, political and social discussions. This dinner was very kindly sponsored by <a href="http://hp.com">HP</a>.

After coming back from dinner, Chris Schlaeger came to visit us. We had a nice discussion about KDE, about his work on TaskJuggler and my starting work on kdeprint, about his work at Novell and some other general things. It is nice to meet him again after all this time.

The evening:
Being still very tired after the sleep depravation of the last week, I went to bed at around 21h00 and finally recuperated.

Now it's day 2 and in 7 minutes I meet with the others for breakfast. The story of this day, tonight.