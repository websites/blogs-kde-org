---
title:   "10 YEARS"
date:    2006-10-15
authors:
  - cristian tibirna
slug:    10-years
---
Ten years ago tomorrow, I was sitting in my office at the university, at 1AM, and, tired of many hours of difficult coding, I decided to take a break and read some of my building up mailing list subscriptions.

Among many messages of varying importance (not important enough to remember them to this day), I found <a href="http://www.kde.org/announcements/announcement.php">this gem</a> signed by one Mathias Ettrich, on one of the lyx lists. I'm not an emotional person. But I still strongly remember the joy and excitement that annoucement produced to me. I guess I answered to Matthias immediately. I only started to actually code for KDE a few months later. But then, the coding kept going, with various speeds, until today. 10 years. It's definitely the longest focus of my attention on anything related to my hobbies or work.

My friendliest embrace to you all that made KDE what it is today. Thank you! You are great!
<!--break-->