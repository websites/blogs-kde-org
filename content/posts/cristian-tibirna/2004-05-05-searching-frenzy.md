---
title:   "searching frenzy"
date:    2004-05-05
authors:
  - cristian tibirna
slug:    searching-frenzy
---
You <b>know</b> that a framework is great when adding a new feature (like, say, searching in a listview) takes under 5 minutes.

I got annoyed of never being able to easily find offending keybindings in the kkeydialog's listview so I decided to use Scott Wheeler's ListViewSearchLine widget to fix the problem. It just took me 5 minutes (including the update and compilation of a fresh kdeui from the head cvs) and I got a perfectly useful new functionality. All is <b>so</b> intuitive...

Of course, there is a flip side (although a well sweetened one). Developing with the KDE framework became (in the time I was less active) a "full time"-like job. This, because one needs a huge amount of experience and hands-on knowledge in order to keep his code compliant with current coding/styling/documentation practices. E.g., once the searchline widget added, I wanted to simply add a little "Clear Search" button similar to konq's and a label. Not the end of the world, but I would have needed long minutes to search for the appropriate way of inserting the correct icon (yes, you have to know that you need an icon-set) without the kind help of Scott on IRC.

To sum it up, gotta lllove KDE development. But gotta be faithful too....

P.S. Yes, I will commit my code as soon as I get approval from the maintainer.