---
title:   "smart"
date:    2006-06-09
authors:
  - cristian tibirna
slug:    smart
---
Today I started to use <a href="http://susewiki.org/index.php?title=Smart">smart</a> on all the suse-10.1 systems I use and, in the joy, I installed it on my suse-9.3 systems too. Life is better now.