---
title:   "Tsync"
date:    2005-09-02
authors:
  - cristian tibirna
slug:    tsync
---
An interesting thing to have: <a href="http://www.newswise.com/articles/view/514134/">James Anderson's G-SOC sponsored</a> <a href="http://tsyncd.sourceforge.net/">TSync</a>.

I didn't look at the code yet, but the principles are clear enough and are definitely attractive when related to synchronizing collections of (KDE) settings, a long time wish/request from our users.

It occured to me a couple of times, only in the last week, to have to ponder between an CVS/SVN and an RSync based synchro solution for my work. In simple cases, where versioning and changeset tracking weren't needed, I prefered RSync. But being allowed to not care of cron scripts or manual sync-ing, this is a good thing to have.
<!--break-->