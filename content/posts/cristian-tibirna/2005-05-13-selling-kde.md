---
title:   "\"Selling\" KDE"
date:    2005-05-13
authors:
  - cristian tibirna
slug:    selling-kde
---
Among my other numerous and intricate tasks at work, I also play the temporary role of informatics djin in our research group. I herd machines, mold users (yeah :-|  ), tend for software (both sickly commercial licenced stuff and marvelous free gems), test compilers etc. I thus come regularly in contact with absolute new unix users (in the occasion of creating their accounts and guiding them around), that come from 3-4 years of windows history and are cruelly plunged, headfront, into a boiling sea of C++ & g++ & python & Finite Elements & Mathematics & Unix & KDE & XEmacs. People that see a command line for the first time in their life. I have to handhold them during the first days and explain to them why machines don't crash (thus no need to hit the pesky reboot button) and so on.

Once in a while, I am blessed by the gods to get one truly brilliant individual, which takes such a coarse occurance as a cool "learn something new" opportunity and which is inhaling enthusiastically at it;-) Such a refreshing event occured a few days ago. The boy came to my office and started asking. He wanted to understand. He was thirsty for unix history, C++ advocacy, KDE "raison d'être", X technology etc.

Thus I unleashed myself. I showed to him many of the tricks I do in XEmacs to speed my coding, I showed off my KDE usage idiosyncrasies (like Alt+F2 gg:,  a gem that Windows users are quick to long for, once they find it out). I was high, as if I had drank two liters of strong green tea in 10 minutes. And then, the thundering little thing happened. I was in the middle of a comparison of KDE with everything else when the boy wispered: <i>"bon vendeur"</i>. That's French for "You know how to sell it". I stopped there, for 5 seconds, reflecting. I went on then, but this little nag stayed with me ever since.

We can't do better than "selling" KDE person by person. Yeah, it's nice to do good PR, it's important to have flashy web sites and thorough documentation. But these things become useful mainly <b>after</b> conversion. I realized, during that flash, that we actually are to do evangelisation, in the most apostolic sense of the word, if we care for our work become more and more useful to the humanity. Yeah, great words, but that's our most secret aspiration, of each of us, when writing code late at night. The hope that somebody will get and use and benefit from our work.

Hey, my friends, once in a while, take a 30 minutes pause from coding and go sit with your colleague or your friend, the odd one, who showed interest in you KDE hobby. Don't try to convert her on the spot. But simply try to find out what she'd like to find in using KDE and then show to her how to get what she searches. Evangelise. It's useful. And energy-restoring.

