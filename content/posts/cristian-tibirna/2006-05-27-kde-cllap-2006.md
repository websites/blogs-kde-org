---
title:   "KDE at CLLAP 2006"
date:    2006-05-27
authors:
  - cristian tibirna
slug:    kde-cllap-2006
---
I'm "back" from <a href="http://www.cllap.qc.ca">CLLAP 2006</a> (quotes around the word <i>back</i> because it was a 15 minutes drive to home) (ed. 2006-05-26: I started to write this on 2006-05-24...)

Although the conference was international and addressed to governments, the expo that accompanied it (and in which we had a KDE booth) was very small.

I had the valuable help of Frédéric Sheedy, a coordinator of the French translation team, who lives in my city and, most impressing for me, is very young. He is also very reliable.

The <a href="http://blogs.kde.org/node/2044">"booth"</a> consisted of a small table and a vertical panel, which we had to share with another .org . I managed to get help from <a href="http://cyberlogic.ca">Cyberlogic</a>, a Montréal computer company, for the media at the booth: two computers and a beamer.

The two days of the exposition were occasion for interesting chats with the fellows from the other .org exponents. We've got a few visitors too. The participants at the conference weren't numerous to pass through the booths, even though the coffee break area was just a few meters away from the expo. We had a high level systems administrator from the Québec provincial auto insurance authority wanting to learn what KDE, as a desktop suite technology, has to offer in advantage over the traditional offerings.

Peter Silva, a former <i>Konsole</i> developer, who works for a weather prediction company in Canada, came to see me. I had a very nice discussion with <a href="http://www.figuiere.net/hub/">Hubert Figuière</a>, of AbiWord and gphoto fame, now associated with Xandros. I had a quite embarrassing moment at the beginning of the encounter, when I didn't recognize Hubert (never having seen his photo ;-). Sorry, Hubert, hope you forgot me ;-)

Many of the Kubuntu Breezy CDs that Jonathan Riddell sent to me some time ago found happy owners. (k)Ubuntu seems to be much more popular (even in Canada...) than I thought.

I didn't have the means to actually see some of the <a href="http://www.cllap.qc.ca/2006/modules/smartsection/item.php?itemid=11">conferences presented</a> though I would have liked to. I understood that the presentations of Jean-Marie Lapeyre and of Prof. Daniel Pascot were met with high interest.

All in all, I think this event could have been better, but having nice organizers who asked for our participation and getting to meet members of government and actually witnessing interest for FOSS at this high level, was a good experience for me.