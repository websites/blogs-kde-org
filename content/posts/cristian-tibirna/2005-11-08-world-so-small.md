---
title:   "This world is so small"
date:    2005-11-08
authors:
  - cristian tibirna
slug:    world-so-small
---
I found on Hans Nowak's "Effectos Speciales" <a href="http://zephyrfalcon.org/weblog2/arch_e10_00840.html#e847">a mention of the map of SoC mentors and students</a> This is the first time I see it.

OK, no deep philosophical reflections on this, but let me put out a few observations:

<ul>
<li> KDE does quite well (and its contributors were quite well distributed around the world)</li>
<li> Distribution of people involved of course respects the distribution of population density on the planet but...</li>
<li> the huge voids in Africa and Asia are <i>desolating</i> and sadening.</li>
<li> both countries I care most for (in the measure countries still mean something in today's world), Canada and Romania, are doing quite bad.</li>
</ul>

Now, the level of free computing and general economy in Romania had allowed me to predict this result. But Canada?... Well, I'm not so surprised. Unfortunately Canada is quite behind when it comes to FOSS. Still, I couldn't really understand why. Canadians are passionate and socially involved people. What keeps us (as a group) away from FOSS? Question open.