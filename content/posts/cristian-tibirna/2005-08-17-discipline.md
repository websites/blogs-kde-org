---
title:   "Discipline"
date:    2005-08-17
authors:
  - cristian tibirna
slug:    discipline
---
I started  rather late in my life to work for a gain, at age 23. This was so given the political organization of the place I was then in and thanks to the invaluable care of my parents, who wanted us (me and my sister) to get solid education and be nondisturbed by external difficulties of any kind in the process.

Thus I carried some sort of idealism from my teen age way into my adult life. This makes that, in the last 15 years, there were only rare days in which a good amount of my time was <i>not</i> spent on productive activities having no immediate (or even projected) material output. I still call them productive because I dare think these activities usually made me a better being.

Since 1996, I was continually involved in KDE in varying measures, as such a productive (non moneyable) activity. My most active participation was between 1998 and 2001. I am definitely addicted to KDE. And I of course love to be so. Still, I think I have a problem. And this is related to lack of time through lack of discipline.

I usually work 8 to 10 hours a day on a job almost completely unrelated to KDE. I have a wonderful and understading wife and a marvelous son. I make it the most pleasant of duties to reserve to them at least 3 hours of my usual day. Starting with 1990, I used to sleep 5-6 hours a night, with frequent white nights. My health started to falter so in the last year or so I had to recede to 7 hours a night, the biggest concession I am likely to make. I need about 2 hours a day for eating, caring for my hygiene and other physical status and so on. This leaves me with 2 to 4 hours for "other stuff", including KDE.

Thus, I think it's  easily understood why I can't usually get more than one hour freed for KDE any usual day (perhaps a bit more on saturdays). This is after all not so bad. Still, I realized that, with time passing, I "evolved" to a stage where most of this hour is spent in reading. Around 500 emails per day need tending (well, 450 to 470 of them are simply thrown away...). There are about 200 RSS items (and this only in about 10 feeds) of which I carefully read half. The occasional informative/useful web page, documentation site, manual and so on, the well known piling "stuff".

I try better time organization and harsher discipline (and I was raised by a father member of the military for most of his active life). Yet, I still can count on one hand's fingers the times in a month when I directly do KDE production (usefully answering email, writing code, mending bug reports).

So, I know the problem: too much "stuff" reading. But I don't have a solution, since <i>not</i> reading stuff would mean falling off the bleeding edge. I still look for the miracle solution.

(NOTE: today's title is courtesy of <a href="http://bobbymcferrin.com">Bobby McFerrin</a> and is inspired by his "Discipline" song, the seventh on the "Medicine Music" CD. I often listen together with my son to this magnificent play in which Bobby is accompanied by his father, Robert McFerrin, a famous baritone).
<!--break-->