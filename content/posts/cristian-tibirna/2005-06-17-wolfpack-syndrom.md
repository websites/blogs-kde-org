---
title:   "Wolfpack syndrom"
date:    2005-06-17
authors:
  - cristian tibirna
slug:    wolfpack-syndrom
---
I call what <a href="http://blogs.kde.org/node/view/856">njaard</a> observed the <i>wolfpack syndrom</i>. The human being is a competitional animal (as any product of natural evolution) but with a twist. We <i>premeditate</i> the way we compete, we're not doing it instinctively.

The wolfpack syndrom is what you get when one slightly smarter (or at least more expressive -- language-wise -- ) individual emits judgements of value. He might have vested (and/or hidden) interests in the point of his judgement, or he might simply not know better. This isn't so troubling. <b>But</b> then the "pack" forms. These are groups of individuals, (which, by the very act of packing, are losing their indivituality), that simply follow the opinion leader without questioning, and sometimes being more rabid than the leader itself in defending the packing-generating opinion. Yes, you can see now the similarity with the behavior of a pack of wolfs stalking a pray.

One can explain lots of things with this "concept": the incipient phases of monopolies, wars, religions, the rise of Linux; think of it! You will find examples everywhere.

And now you got the solution too! Find a recognized/respected opinion leader that is ready, by conviction or by interest, to pitch KDE, and the wolfpack will form.
<!--break-->