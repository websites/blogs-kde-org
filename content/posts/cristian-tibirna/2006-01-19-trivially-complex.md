---
title:   "Trivially complex."
date:    2006-01-19
authors:
  - cristian tibirna
slug:    trivially-complex
---
I found a sound clip. OGG. I wanted to listen to it. Just listen. 
- So I clicked on it. 
- KDE offered to open it with Kaffeine. I accepted
- Kaffeine started and promptly gave me an error dialog but no sound.
- Went back and chose another player  from the context menu: amarok.
- amarok started, asked me 5 (five) almost identical question dialogs (sequentially!) but no sound.
- Back to the context menu and chose xmms.
- xmms started, gave an error dialog about sound card not being available but no sound.
- Back to the context menu, RealPlayer 10 was offered but I already lost interest.
- Copied the url of the sound bit, went to the konsole, wrote ogg123 and pasted the url then hit enter. Bam! Instant Sound.

Trivially complex :-(
<!--break-->