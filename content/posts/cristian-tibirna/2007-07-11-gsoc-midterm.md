---
title:   "gsoc midterm "
date:    2007-07-11
authors:
  - cristian tibirna
slug:    gsoc-midterm
---
Yes, time passes too fast. It's almost time for this year's Summer of Code midterm evaluation.

During the week-end I "met" my two students online for a more in depth checkpoint than what we usually have and I'm quite pleased! I looked forward to <a href="http://rgcgsoc.wordpress.com/">Rutger</a> and <a href="http://gsoc2007gavinbeatty.blogspot.com/">Gavin</a> to pass onto me some of their youthful energy again. KDEPrint needs it. 

Things progress interestingly. I can't wait for the near future moment where we'll get to commit their work to the kdeprint proper.