---
title:   "Books sent"
date:    2005-08-17
authors:
  - cristian tibirna
slug:    books-sent
---
I was <a href="http://blogs.kde.org/node/1290">writing</a> about my extra copies of the KDE 2.0 Development book. Three people asked for them. I shipped them today to Jure Repnic, Reuben Sutton and Lex Hider.
<!--break-->