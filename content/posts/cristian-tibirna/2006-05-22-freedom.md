---
title:   "freedom"
date:    2006-05-22
authors:
  - cristian tibirna
slug:    freedom
---
Aaron <a href="http://aseigo.blogspot.com/2006/05/free-as-in-beer-or-free-as-in-freedom.html">tries to wake back up</a> the old (and perpetual) discussion about freedom in software. I can't agree more with his perspective. It will make well over 8 years now that I continuously preach, to anybody patient to listen, the social importance of the essential notions of collaboration ingrained in free software.

Yet, I can't refrain from being somehow pessimist and disappointed about the reflections that I get from the surrounding communities (those that I connect "live" each of my normal days, those outside the KDE or Linux communities that I try to be part of at night, when time permits). I explained (or at least tried to) the essence of what Aaron says, to the best of my capacities, to audiences from one to tens, from students to government heads. Everybody knew that is correct to approve my enthusiasm and meet my invitation to participation and my explanations with openness. But I always saw in the glimmering of their eyes that I was for them a "rara avis", just good for exposition in a circus, unable to take hold of the realities of this world, an utopian dreaming wide awake. 

These glimmers, they hurt... But yeah, maybe we're outside of this time. Let's hope that we're not just away, but rather ahead.