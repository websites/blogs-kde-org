---
title:   "Speaking to government"
date:    2006-01-15
authors:
  - cristian tibirna
slug:    speaking-government
---
On January 10th 2006 I was offered the opportunity to speak to the directors of IT services from the Qu&eacute;bec gouvernment. Qu&eacute;bec is one of the provinces of Canada, with rather important economic and cultural power in North America.

Profesor <a href="http://www.fsa.ulaval.ca/html/danielpascot.html">Daniel Pascot</a>, director of the Department of Organizational IT Systems in the Administration Sciences Faculty here at Laval University, a fervent <a href="http://loli.fsa.ulaval.ca">supporter</a> of free software, invited me for a one hour conference on KDE, that I called <a href="http://ktown.kde.org/~ctibirna/2006/choix_kde.pdf">"KDE -- le choix judicieux"</a> (PDF, French, 417kB, tr: "KDE -- the proper choice"). This was part of a 2 and 1/2 days seminar on Free Software, especially conceived for this type of audience.

It was the first time I was addressing such a group. The possible consequence on adopting Linux and KDE on the local scene of public information systems is non-negligeable. Thus I took quite a bit of time to prepare and refine my speech. It was a rich experience.

I tried to conduct the conference more like a discussion with the assistance and I partly succeeded. As a consequence, I took two hours instead of one (once again, I appologize, Professor Pascot :-) ). During the presentation, it became clear quite rapidly that the first part -- containing case studies of applied Free Software in government and education setups -- should really have come second, <i>after</i> the detailed presentation of KDE itself.

Another interesting observation is that, for the people exterior to the Free Software movement -- the very people we would like to attract to using the results of our work --, it is rather tricky or, perhaps better said, ineffective, to keep describing KDE as being a whole of four parts: desktop, applications, development framework and community. We need suggestive nicknames for each of these aspects and clearly separate descriptions.

Questions that I expected came from the audience: 
<ul>
<li>"Doesn't it bother you to know that everybody can take inspiration from your code, or even the code itself, without retribution?"</li>
<li>"Will Free Software and open document formats bring stability to this confusing world full of incompatibilities?"</li>
<li>"Can I find a way to install KDE if I go to kde.org?"</li>
<li>"Why do you give away so much hard work, for free?"</li>
<li>"Is all this difficult to install?"</li>
<li>"How do you manage viruses and worms?"</li>
</ul>

But I also had questions that show that, even at high levels, technology is key:
<ul>
<li>"What programming languages does KDE use?"</li>
<li>"Why don't you use Java?"</li>
<li>"How would KDE fit in a high security environment, like a nuclear reactor installation?"</li>
And the most interesting:
<li>"Does KDE include an information indexing and search system for the data of the desktop user?"</li>
</ul>

As I said, it was a very interesting experience. People didn't quit the room screaming ;-). On the contrary, I had comments at the end from people curious to experiment with our software. I should have had live CDs with KDE distros...

I am trying now to figure out what would be the best continuation. Gaining some kind of direct contact to at least a few of these persons would be very beneficial for everybody. Anyways I'm very excited to see that finally things start to move in Qu&eacute;bec (and Canada) too with respect to Free Software. Let's hope that the word gets passed.
<!--break-->