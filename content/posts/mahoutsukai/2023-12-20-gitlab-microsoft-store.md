---
title:   "From GitLab to Microsoft Store"
date:    2023-12-20
authors:
  - mahoutsukai
slug:    gitlab-microsoft-store
categories:
  - CI/CD
  - Windows
---
This is an update on the ongoing migration of jobs from <a href="https://binary-factory.kde.org">Binary Factory</a> to <a href="https://invent.kde.org">KDE's GitLab</a>. Since the <a href="https://blogs.kde.org/2023/11/13/apks-now-built-invent">last blog</a> a lot has happened.

A first update of <a href="https://apps.kde.org/itinerary">Itinerary</a> was submitted to Google Play directly from our GitLab.

Ben Cooksley has added a service for publishing our websites. Most websites are now built and published on our GitLab with only 5 websites remaining on Binary Factory.

Julius Künzel has added a service for signing macOS apps and DMGs. This allows us to build signed installers for macOS on GitLab.

The service for signing and publishing Flatpaks has gone live. Nightly Flatpaks built on our GitLab are now available at <a href="https://cdn.kde.org/flatpak/">https://cdn.kde.org/flatpak/</a>. For easy installation builds created since yesterday include <a href="https://docs.flatpak.org/en/latest/repositories.html#flatpakref-files">.flatpakref</a> files and <a href="https://docs.flatpak.org/en/latest/hosting-a-repository.html#flatpakrepo-files">.flatpakrepo</a> files.

Last, but not least, similar to the full CI/CD pipeline for Android we now also have a full CI/CD pipeline for Windows. For Qt 5 builds this pipeline consists of the following GitLab jobs:

<ul>
<li>windows_qt515 - Builds the project with MSVC and runs the automatic tests.</li>
<li>craft_windows_qt515_x86_64 - Builds the project with MSVC and creates various installation packages including (if enabled for the project) a <code>*-sideload.appx</code> file and a <code>*.appxupload</code> file.</li>
<li>sign_appx_qt515 - Signs the <code>*-sideload.appx</code> file with KDE's signing certificate. The signed app package can be downloaded and installed without using the Microsoft store.</li>
<li>microsoftstore_qt515 - Submits the <code>*.appxupload</code> package to the Microsoft store for subsequent publication. This job doesn't run automatically.</li>
</ul>
Notes:

<ul>
<li>The craft_windows_qt515_x86_64 job also creates <code>.exe</code> installers. Those installers are not yet signed on GitLab, i.e. Windows should warn you when you try to install them. For the time being, you can download signed <code>.exe</code> installers from Binary Factory.</li>
<li>There are also jobs for building with MinGW, but MinGW builds cannot be used for creating app packages for the Microsoft Store. (It's still possible to publish apps with MinGW installers in the Microsoft Store, but that's a different story.)</li>
</ul>
The workflow for publishing an update of an app in the Microsoft Store as I envision it is as follows:

<ol>
<li>You download the signed sideload app package, install it on a Windows (virtual) machine (after uninstalling a previously installed version) and perform a quick test to ensure that the app isn't completely broken.</li>
<li>Then you trigger the microsoftstore_qt515 job to submit the app to the Microsoft Store. This creates a new draft submission in the Microsoft Partner Center. The app is not published automatically. To actually publish the submission you have to log into the Microsoft Partner Center and commit the submission.</li>
</ol>
<h2>Enabling the Windows CD Pipeline for Your Project</h2>
If you want to start building Windows app packages (APPX) for your project then add the <code>craft-windows-x86-64.yml</code> template for Qt 5 or the <code>craft-windows-x86-64-qt6.yml</code> template for Qt 6 to the <code>.gitlab-ci.yml</code> of your project. Additionally, you have to add a <code>.craft.ini</code> file with the following content to the root of your project to enable the creation of the Windows app packages.

<pre><code class="language-ini">[BlueprintSettings]
kde/applications/myapp.packageAppx = True
</code></pre>
<code>kde/applications/myapp</code> must match the path of your project's <a href="https://invent.kde.org/packaging/craft-blueprints-kde">Craft blueprint</a>.

When you have successfully built the first Windows app packages then add the <code>craft-windows-appx-qt5.yml</code> or the <code>craft-windows-appx-qt6.yml</code> template to your <code>.gitlab-ci.yml</code> to get the sign_appx_qt* job and the microsoftstore_qt* job.

To enable signing your project (more precisely, a branch of your project) needs to be cleared for using the signing service. This is done by adding your project to the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/signing#appxsigner">project settings of the appxsigner</a>. Similarly, to enable submission to the Microsoft Store your project needs to be cleared by adding it to the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/signing#microsoftstorepublisher">project settings of the microsoftstorepublisher</a>. If you have carefully curated metadata set in the store entry of you app that shouldn't be overwritten by data from your app's AppStream data then have a look at the <code>keep</code> setting for your project. I recommend to use <code>keep</code> sparingly if at all because at least for text content you will deprive people using the store of all the translations added by our great translation teams to your app's AppStream data.

Note that the first submission to the Microsoft Store has to be done <a href="https://develop.kde.org/docs/packaging/microsoftstore/">manually</a>.
