---
title: Accessibility and a bit of automation at the KDE Goals Sprint in Berlin
date: 2024-05-20
authors:
  - mahoutsukai
categories:
  - Accessibility
  - KDE Goals
SPDX-License-Identifier: CC-BY-SA-4.0
---

A few weeks ago (Time flies!) I attended the [KDE Goals Sprint](https://community.kde.org/Sprints/Goals/2024) in Berlin. I didn't have concrete plans, but I intended to look into accessibility. Quite some time ago I had improved the accessibility of Kleopatra and at Akademy 2023 in Thessaloniki I gave a [talk](https://conf.kde.org/event/5/contributions/141/) about it. Back then I had taken the easy route fixing everything directly in Kleopatra and working around several issues in Qt instead of fixing the issues in Qt itself so that all apps could profit. Time to do something about it.

## (In-)Accessible icon-only buttons

A common problem for accessibility is icon-only buttons. If a button doesn't have text then screen readers can only tell their user that there's a button. That's not very helpful. Sometimes the developers have at least assigned a tool tip to the button. This can be read out by the screen readers (Qt provides the tool tip as accessible description of the button.), but it's often too verbose. To make a button without text accessible the developer has to set the [accessible name property](https://doc.qt.io/qt-6/qwidget.html#accessibleName-prop) of the widget or, in case of a Qt Quick app, the name property of the [Accessible QML Type](https://doc.qt.io/qt-6/qml-qtquick-accessible.html). Unfortunately, that's often forgotten if the UI isn't designed with accessibility in mind.

At the sprint I discussed several ideas with other participants to help developers remember to set the accessible name:
* A helper class to instantiate in your app which inspects the app's windows and prints a report at the end with all inaccessible icon-only buttons it has found. A bit like Qt's [QAbstractItemModelTester](https://doc.qt.io/qt-6/qabstractitemmodeltester.html) or the different compiler sanitizers. I implemented a prototype of such a class, but didn't pursue this further. The downside of this approach is that the developer needs to open each window of the app to find all inaccessible buttons. If they are already aware of the problem then it's probably easier to search the code.
* Instead of using a helper class to inspect the widget tree from the inside one could inspect the accessibility tree of the app from the outside. This could be built into our Appium-driven UI test framework so that developers don't have to do anything special. Except that they need to write UI tests that open each and every window of their app. I think it's still worth to look into this.
* Last but not least, we pondered writing a [clazy](https://invent.kde.org/sdk/clazy) test. Thinking about the many different ways a text can be set on a button (e.g. with [KGuiItem::assign](https://api.kde.org/frameworks/kwidgetsaddons/html/classKGuiItem.html)) we doubted that it would be feasible to write such a test.

In the end the easiest approach could be education. If the developers are aware of the problem then there's a good chance that they remember to set an accessible name the next time they add an icon-only button to their app.

## Accessible date/time inputs

Volker, Carl, David, Harald and me discussed and explored some ideas to make the date and time inputs in Qt Quick apps like Itinerary accessible. In Kleopatra I resorted to allow the user to enter the date in a simple text input instead of trying to make the complex UI of KDE's [date picker](https://api.kde.org/frameworks/kwidgetsaddons/html/classKDatePicker.html) accessible. Read [Volker's blog](https://volkerkrause.eu/2024/04/27/kde-goal-sprint-april-2024.html) and [David's blog](https://blog.david-redondo.de/kde/2024/04/26/goals-sprint.html) to find out which solutions they found for Qt Quick apps.

## A small automation interlude

One advantage of sitting with other people in the same room is that you may overhear them talking about a mistake (e.g. a faulty commit) and you know exactly how to prevent this kind of mistake in the future. In this case the problem was a missing quote character in some YAML file. And the preventive measure was [adding a YAML linter CI job](https://invent.kde.org/sysadmin/ci-management/-/merge_requests/99). While I was at it I [removed some unnecessary code from the CI job](https://invent.kde.org/sysadmin/ci-utilities/-/merge_requests/294) and [added the job to a second repository](https://invent.kde.org/sysadmin/repo-metadata/-/merge_requests/377).

## List views with underlying multi-column model

In several widgets that show or use a simple list of items Qt allows using a model with multiple columns, e.g. [QListView](https://doc.qt.io/qt-6/qlistview.html#modelColumn-prop), [QComboBox](https://doc.qt.io/qt-6/qcombobox.html#modelColumn-prop), [QCompleter](https://doc.qt.io/qt-6/qcompleter.html#completionColumn-prop). In general this works well except that Qt has a long-standing bug: When navigating through the list screen readers read the entries of the underlying model column by column instead of reading only the entries in the selected model column ([QTBUG-33786](https://bugreports.qt.io/browse/QTBUG-33786)). In Kleopatra I worked around this bug with a proxy model which pretended that the model only had one column.

During the sprint I finally sat down and prepared a fix for Qt. For better readability I split my changes in five separate commits which resulted in five separate patches for Qt: [556857](https://codereview.qt-project.org/c/qt/qtbase/+/556857), [556858](https://codereview.qt-project.org/c/qt/qtbase/+/556858), [556859](https://codereview.qt-project.org/c/qt/qtbase/+/556859), [556860](https://codereview.qt-project.org/c/qt/qtbase/+/556860), [556861](https://codereview.qt-project.org/c/qt/qtbase/+/556861). Being used to multi-commit MRs in GitLab I wondered if I had done something wrong when I submitted my changes, but apparently that's Gerrit's way of handling patch reviews. The first two commits are code clean-ups, the third commit changes one aspect of the accessibility test for list views, the fourth commit is the actual fix, and the fifth commit adds a few more unit tests I find useful. The first three commits have been merged, but the actual fix is still waiting for a review.

After that I looked into the problem that QListView emitted an accessibility focus event when the current item changed even if the list view didn't have focus. I found out that this had been fixed recently by a fellow Qt contributor who ran into the [same problem](https://bugreports.qt.io/browse/QTBUG-118800) independently of me. This meant that I could [remove the workaround in Kleopatra](https://invent.kde.org/pim/kleopatra/-/commit/765b624e99670a420cf586673b9bd80c669005d5) for new enough Qt.

Thanks to MBition for hosting us and to Aleix for making sure we don't starve. And many thanks to those [donating to KDE](https://kde.org/donate) which makes these sprints possible.
