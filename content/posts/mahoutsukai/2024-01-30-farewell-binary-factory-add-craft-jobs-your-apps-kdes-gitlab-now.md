---
title:   "Farewell, Binary Factory! Add Craft Jobs for Your Apps to KDE's GitLab Now"
date:    2024-01-30
authors:
  - mahoutsukai
slug:    farewell-binary-factory-add-craft-jobs-your-apps-kdes-gitlab-now
categories:
  - CI/CD
  - Android
  - Windows
  - macOS
---
This is the final update on the migration of the Craft jobs from <a href="https://binary-factory.kde.org">Binary Factory</a> to <a href="https://invent.kde.org">KDE's GitLab</a>.
Since the <a href="https://blogs.kde.org/2023/12/20/gitlab-microsoft-store">last blog</a> the last missing pieces have been put in place.

We now build a KF6 runtime which is used for the <a href="https://userbase.kde.org/Tutorials/Flatpak#Nightly_KDE_Apps">nightly flatpaks</a> of many of the apps that will be part of <a href="https://kde.org/announcements/megarelease/6/rc1/">KDE's Megarelease 6</a>.

Moreover, additionally to signing the sideload APPX packages (see <a href="https://blogs.kde.org/2023/12/20/gitlab-microsoft-store">previous blog</a>) the Windows Craft jobs now also sign the NSIS (<code>.exe</code>) installers and all binaries included in the installers. This completes the port of the Windows Craft jobs from Binary Factory to KDE's GitLab.

Now is the time to add GitLab jobs to your project for builds previously run on Binary Factory. The 24.02 release branch has been cleared for using our signing and publishing services, so that you can prepare builds of AppImages, Flatpaks, Android packages, macOS installers, and Windows installers for the 24.02 release of your project, or any other release if you release independent of KDE Gear. To enable those builds add one or more of the following <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/gitlab-templates?ref_type=heads#our-gitlab-cicd-pipelines">GitLab templates</a> to your project's <code>.gitlab-ci.yml</code>.

<ul>
<li>craft-appimage.yml (Qt 5), craft-appimage-qt6.yml (Qt 6)</li>
<li>flatpak.yml</li>
<li>craft-android-apks.yml (Qt 5), craft-android-qt6-apks.yml (Qt 6)</li>
<li>craft-macos-arm64.yml (Qt 5), craft-macos-arm64-qt6.yml (Qt 6)</li>
<li>craft-macos-x86-64.yml (Qt 5), craft-macos-x86-64-qt6.yml (Qt 6)</li>
<li>craft-windows-x86-64.yml (Qt 5), craft-windows-x86-64-qt6.yml (Qt 6)</li>
<li>craft-windows-mingw64.yml (Qt 5), craft-windows-mingw64-qt6.yml (Qt 6)</li>
</ul>
All jobs except for the Flatpak job use Craft for building and packaging your app. You may have to add a <code>.craft.ini</code> file to your project's root folder for <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/gitlab-templates?ref_type=heads#details">overriding the defaults</a> of Craft and the Craft blueprints of your project or your project's dependencies.

<b>What's Next</b>

Next I'll work on making it possible to create and publish Android Application Bundles (AAB) additionally to APKs for your Android apps. Application Bundles contain the binaries for all supported architectures in a single package (instead of multiple different APKs for each architecture). This packaging format is required for new applications published on Google Play.