---
title:   "HIG about Simple vs. Advanced Settings"
date:    2016-02-08
authors:
  - htietze
slug:    hig-about-simple-vs-advanced-settings
---
Recently the question was asked in the KDE forums <a href=https://forum.kde.org/viewtopic.php?f=285&t=130260>how we handle advanced settings</a>. While there is neither a best practice nor a common approach in KDE software, we actually discussed a similar concept in respect to the Plasma control modules (KCM). 

The updated <a href=http://user-prompt.com/results-of-card-sorting-the-kde-system-settings>organization</a> of KCMs was implemented by the developers, the community decided about the basic <a href=http://user-prompt.com/kde-system-settings-about-the-general-navigation>layout</a>, and a couple of proposals were done [<a href=http://user-prompt.com/kde-control-module-usability-using-the-example-of-kcm-for-pointing-devices>1</a>, <a href=http://user-prompt.com/baloo-kcm>2</a>]. So why don't generalize this idea and write a guideline?

The following guideline proposal not only recapitulates what we considered for the KCM but also introduces some new ideas. There is first of all the import/export function. The use case for this function is a system backup where you may want to store application settings too. While installing software is a piece of cake you waste much time to get back your previous look and feel as well as the known behavior. The import/export function should affect all aspects of the application. In contrast, the <i>GHNS!</i> feature is applied locally to the selected "group", which could be a minor usability flaw. Also controversially discussed is the workflow how to add user-defined settings. The idea is to implement it similar to the current color KCM. When the user modifies properties of an existing preset, e.g. "Breeze", the changes are applied to the preset "Current". In order to keep the customization it has to be renamed, e.g. "My Breeze". The advantage - and probably big challenge for developers - is to provide not only one single factory setting. Simplicity does not mean to have no choice.

Before we now start to create examples we would like to get your agreement - or rejection. So what do you think? 
<hr>
<b> Purpose </b>

The settings dialog provides user-customizable options how an application or plasma (KCM) should behave. The dialog is intended for options that are not accessed frequently and are persitent. Following KDE's "Simple by default, powerful when needed" <a href=Projects/Usability/HIG/Presentation/DesignVisionPrinciples>design mantra</a>, settings are split into simple vs. advanced ones. Advanced settings are options that are not important to most users but essential for some, and can't removed therefore. Those options are hidden by default, but with an easy access in order to improve learnability. 

<b> Is this the right control </b>
<ul>
<li> Use this pattern for all settings that are relevant to change for users.
<li> Do not use the settings dialog for frequently accessed properties like, for instance, details vs. icon view. Use the toolbar or main menu (and optionally context menu) for these options.
<li> Do not use the settings dialog for rarely changed or developer options like the sql table name. Use extra configuration files or dialogs for those options.
</ul>
<b> General recommendations </b>
<ul>
<li> Simple by default: Define smart and polite defaults. Set the defaults in a way that most users don't have to alter them at all. 
<li> Powerful when needed: Provide enough options for the perfect customization according individual needs and preferences. But even though customizability is very important for KDE software, try to keep your settings dialog as small and simple as possible. Remember: every option requires more code and more testing! 
<li> Respect the privacy of the users: Always use opt-in, never an opt-out model for features that transmit potentially private data (e.g. usage statistics).
</ul>
<b> Layout </b>
<ul>
<li> Organize your settings in logical groups. (#1 in the example).
<li> Split options per group into standard and advanced. Make the standard easy to use for everyone. (#5)
<li> Offer several presets and let the user decide what type of configuration should be active. (#3)
<li> Consider to add access to third-party presets via Get Hot New Stuff! (GHNS), if available for this group. (#4)
<li> Show a live preview for visual features. Omit this section if it's not relevant.
<li> Provide functions to export/import all settings. (#7) If splitting the options into app-related (such as colors, fonts, etc.) and account-related (for instance personal settings, mail accounts...) make sense, let the user decide what to export. Import has to as straightforward as possible; let the user just confirm when data are being overwritten.
</ul>
<b> Behavior </b>
<ul>
<li> When the user changes the default switch to a special preset ("User" or "Current"). This preset cannot be applied unless it is renamed individually. Access to rename (and delete) is done per context menu. Indicate user defined presets by using italic font for the name.
<li> Sort your options and groups by importance.
<li> When a change is applied, the application should adopt it immediately without the need to restart it.
<li> Do not change the settings dialog depending on the context. You should always start with the same landing page regardless of the application context.
<li> Do not use a wizard to edit options. Only use a wizard to set options if actually a group of options all have to be edited at once, eg creating an account or a first run wizard.
</ul>
<b> Mockup </b>
<img  src=https://i.imgur.com/qU2C7Wu.png alt="Mockup">

1) Access groups via sidebar.
2) The preview has to be on the top of the content area. 
3) Offer a good number of presets to let the user choose one out of different factory settings. Anchor the presets so that users can have more space for the area below using the horizontal splitter. Cut long captions with ellipsis and show the full name in a tooltip.
(Remark 1: The mockup has very large splitters. The implementation should be visually less obtrusive.)
(Remark 2: The preset selection replaces the former "reset (to default)" function.)
4) Allow users to add more presets via Get Hot New Stuff (GHNS). Organize the setting in a way that GHNS access is per group and not global.
5) Provide access to the most relevant settings at the Standard section. Make sure that these options are easy to understand.
6) Indicate that Advanced options are available but keep this section collapsed by default.
7) Allow users to export the current settings to a file that can be easily imported on any other machine. 
8) Allow to Apply the current settings to the application without closing the dialog.
9) Provide access to functions for user-defined presets per context menu and standard shortcuts.
10) Scroll the whole area of options but neither the preview not the presets, if necessary.

<b>Examples</b>
* ^todo