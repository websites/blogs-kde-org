---
title:   "Heroes of the World!"
date:    2008-01-16
authors:
  - torsten rahn
slug:    heroes-world
---
<p>
Today when I entered the office I found a welcome present in my mail folder that made my day: <b>David Roberts</b> had sent me a patch to put our Marble Earth directly into the spotlight of our G2V Main-Sequence Star (also known as "sun" among non-astronomers). That's totally awesome because this feature will allow kde-edu users to visualize things like seasons. This feature always has been on my personal TODO but I haven't been brave enough yet to start work on it myself because it deals with the (non-trivial) texture mapping code. So I'm thrilled to see that someone else has started to work on this!</p>
<p>Currently the code David Roberts has sent me works quite well already but still needs some fixes and larger speed optimizations. But that doesn't keep me from posting screenshots already:</p> 
<br>
<a href="http://developer.kde.org/~tackat/marbleshots/marble_sunshading1.png"><img src="http://developer.kde.org/~tackat/marbleshots/marble_sunshading1_thumb.png" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marbleshots/marble_sunshading2.png"><img src="http://developer.kde.org/~tackat/marbleshots/marble_sunshading2_thumb.png" class="showonplanet" />
</a>

On another topic <b>Claudiu Covaci</b> has used the Marble Widget to create a small application for a student project at the university. It shows the orbits of some theoretical (GPS) satellite orbits around the earth. He does it by creating a <a href="http://de.wikipedia.org/wiki/Keyhole_Markup_Language">Google Earth KML</a> file from the <a href="http://en.wikipedia.org/wiki/Orbital_elements">orbital elements</a> and by feeding it to the Marble Widget. He has also sent me a patch that removed the last few issues with little additional code required. As a result I also post screenshots of the satellite-KML in Google Earth and Marble. As you can see Google Earth fails to display the possible satellite positions for the hemisphere that is targeted away from the observer:

<a href="http://developer.kde.org/~tackat/marbleshots/marble_satellites.png"><img src="http://developer.kde.org/~tackat/marbleshots/marble_satellites_thumb.png" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marbleshots/ge_satellites.png"><img src="http://developer.kde.org/~tackat/marbleshots/ge_satellites_thumb.png" class="showonplanet" />
</a>
<p>I'd like to thank all the people that have sent me patches and bug reports (like the one I received from Toti today) for Marble recently.</p>
<p>
In the meantime Inge and me are working on the next bug fix release Marble 0.5.1.
</p>
<img src="http://kde.org/img/kde40.png" class="showonplanet" />
<!--break-->

 