---
title:   "KDE 4.1 Beta1: OpenStreetMap Support in KDE via Marble"
date:    2008-05-21
authors:
  - torsten rahn
slug:    kde-41-beta1-openstreetmap-support-kde-marble
---
<p>The hero of the current Marble KDE 4.1 Beta1 release is <b>Jens-Michael Hoffmann</b>: He has successfully worked on getting <a href="http://www.openstreetmap.org">OpenStreetMap</a> integrated into <a href="http://edu.kde.org/marble">Marble</a> and KDE 4.1!
<p> 
This means that once you start our free software virtual globe and select "OpenStreetMap" as a theme then Marble will directly start to download OpenStreetMap tiles from the OpenStreetMap server:
<p>
<a href="http://developer.kde.org/~tackat/osm/marble3.jpg"><img src="http://developer.kde.org/~tackat/osm/marble3_small.jpg" class="showonplanet" /></a>
<p>
If you want to try it you can either wait for KDE 4.1 Beta1 packages to appear on the <a href="http://www.kde.org">KDE Website</a> next week. Or you start to compile current Marble SVN yourself. It's pretty easy: You only need just Qt 4.3 (or 4.4), or alternatively KDE >= 4.0 and Qt >= 4.3 including headers.
<p>
Then you can start to build Marble according to our <a href="http://edu.kde.org/marble/obtain.php">HOWTO</a>.
<p>
Once you have compiled Marble from current SVN you can start it either from the menu or from the commandline. You'll be greeted by our globe. If everything went well, you'll see that Marble now features a starry sky plugin (notice the constellation "Orion" right next to the earth in the following screenshot):
<p>
<a href="http://developer.kde.org/~tackat/osm/marble0.jpg"><img src="http://developer.kde.org/~tackat/osm/marble0_small.jpg" class="showonplanet" /></a>
<p>
If you have compiled the KDE version then you can adjust the quality settings for "Still image" and "During animations". We'd suggest that you use "High" for "Still image" and "Low" for "During animations" however you can adjust the values according to the performance of your hardware. 

<a href="http://developer.kde.org/~tackat/osm/marble5.jpg"><img src="http://developer.kde.org/~tackat/osm/marble5_small.jpg" class="showonplanet" /></a>
<p>
Now select the "Map View" tab on the left and choose "OpenStreetMap". The screen should look about like this:  
<p>
<a href="http://developer.kde.org/~tackat/osm/marble1.jpg"><img src="http://developer.kde.org/~tackat/osm/marble1_small.jpg" class="showonplanet" /></a>
<p>
Once you start to zoom in Marble will start fetching tiles from the OpenStreetMap server and will texturemap them onto the globe:
<p>
<a href="http://developer.kde.org/~tackat/osm/marble2.jpg"><img src="http://developer.kde.org/~tackat/osm/marble2_small.jpg" class="showonplanet" /></a>
<p>
Zoom in more and Marble will show streets and buildings. Please notice that we've also added a fitting OSM legend to Marble (on the left in the screenshot) so you are able to identify the items on the map correctly:
<p> 
<a href="http://developer.kde.org/~tackat/osm/marble4.jpg"><img src="http://developer.kde.org/~tackat/osm/marble4_small.jpg" class="showonplanet" /></a>
<p>
In the upcoming weeks Jens-Michael plans to refine OSM support by fixing bugs and letting tiles expire (so you'll always stay up-to-date with the current OSM mapping progress).
<p> On 12-13 July Jens-Michael and me plan to attend the OSM conference <a href="http://www.stateofthemap.org/">State of the Map</a> where I'll be present as a speaker.
<p>
If you are interested in learning more about Marble please have a look at our series "Marble's Secrets":
<ul>
<li><a href="http://blogs.kde.org/node/3269">Part I</a> <li><a href="http://blogs.kde.org/node/3272">Part II</a> <li><a href="http://blogs.kde.org/node/3275">Part III</a> 
</ul>
If you want to help us with creating free roadmaps please consider to join the OpenStreetMap project!
<p>
Oh and if you wonder about the blockiness due to the texture mapping: We are going to fix that for KDE 4.2. <i>Patrick Spendrin</i> is going to work on a Google Summer of Code 2008 project for Marble that will likely deliver life vector rendering of the OSM data in Marble. 
<!--break-->
