---
title:   "Marble on WinCE"
date:    2008-03-25
authors:
  - torsten rahn
slug:    marble-wince
---
<p>
I hope that everybody who celebrated Easter has enjoyed some pleasant days (well and of course everybody else, too ... ;-). Recently I've received a few e-mails from <a href="http://labs.trolltech.com/blogs/author/thomas-hartmann/">Thomas Hartmann</a> who works in Trolltech's Qt/WinCE team. Thomas has started to port Marble to Windows CE after I had told him about Marble during the KDE 4 satellite launch event in Berlin.
<p>
After having seen a few screenshots of Marble running on Qtopia this is the fifth plattform apart from Marble on Windows, Mac OS X and Linux/Solaris/Unix. The high portability of Marble is due to the fact that Marble doesn't have any dependencies apart from Qt 4 itself (and it doesn't use OpenGL). So you don't even need to have KDE 4 installed to run Marble. However for users the KDE frontend is very much recommended as it has a few extra features that the Qt4-only frontend doesn't offer: like e.g. GetHotNewStuff support. 
<p>
The fact that Qt offers the same API across plattforms (unlike other toolkits) has certainly helped a lot to make the Windows CE port possible. Of course Thomas had to strip down Marble due to the 32 MB memory limit, so the initial vector data isn't as detailed as for the desktop version (we hope that we'll find a GSoC 2008 student who will volunteer to implement <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#Marble">vector tiles</a> for Marble):
<br>
<a href="http://developer.kde.org/~tackat/marble-ce/screen02.png"><img src="http://developer.kde.org/~tackat/marble-ce/screen02_thumb.png" class="showonplanet" />
</a>
<p>
Thomas also created two youtube videos which show Marble on a Dell Axim X51v. The result is pretty impressive given that this PDA is using an XScale/ARM processor (which doesn't provide hardware support for floating point instructions):
<br>
<a href="http://www.youtube.com/watch?v=69KZBaRbkNo"><img src="http://developer.kde.org/~tackat/marble-ce/marble_wince1.png" class="showonplanet" />
</a>
<br>
<a href="http://www.youtube.com/watch?v=1nOo0oPA5fI"><img src="http://developer.kde.org/~tackat/marble-ce/marble_wince2.png" class="showonplanet" />
</a>
<p>
<!--break-->
