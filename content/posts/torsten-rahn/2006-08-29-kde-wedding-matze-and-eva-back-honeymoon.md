---
title:   "KDE Wedding: Matze and Eva back from honeymoon"
date:    2006-08-29
authors:
  - torsten rahn
slug:    kde-wedding-matze-and-eva-back-honeymoon
---
<p>It's certainly one of those rare cases many open source geeks are dreaming of: An open source developer finds his other half, who is also a passionate open source developer and they both decide to marry each other. Recently this dream came true for Eva and Matze, who have both been involved with the free software project KDE since many years already. For those who don't know them yet: Our queen of hearts Eva also is the president of the KDE e.V., the non-profit organisation that represents KDE developers in legal and financial matters. And both have a job that involves Qt programming.
<p>
The wedding took place on July 29th, 2006 in the beautiful St. Pankratius church in Mainz-Hechtsheim near Frankfurt. As our mascot Konqi unfortunately wasn't able to attend I was invited instead. So I had the chance to watch the wedding ceremony together with their family and their friends. Thanks to the organizers (Hi Christian!) and thanks to the weather gods the memorable day became even more memorable due to the stunning party afterwards.
<p>Matze and Eva have just returned from their honeymoon which they enjoyed on the the island Bali in Indonesia. There they did not only find some quiet moments without KDE but also an internet cafe which was using KDE on Linux of course.
<p>
The KDE team would like to offer Congratulations and Very Best Wishes on their Marriage.
<br>
<img src="http://devel-home.kde.org/~tackat/evamatze/justmarried.jpg" class="showonplanet" />

<!--break-->