---
title:   "KDE Booth at CeBIT: Volunteers needed!"
date:    2007-03-05
authors:
  - torsten rahn
slug:    kde-booth-cebit-volunteers-needed
---
<p> You have been using KDE since a few months and you love it so much that you'd like to tell other people about it? You're located in or near Germany and you have some sparetime to offer next week, between March 15-21 ? Then staffing the booth at CeBIT might be a great experience for you! We are still searching for volunteers who would like to help out a day or two to show people the wonderful world of KDE. Technical skills aren't really needed as long as you know how to use KDE! 
<p>Travel costs as well as accomodation shouldn't be a problem either as you can file a request for reimbursement to the KDE e.V. if you staff the KDE booth.
<p>If you'd like to join us, please enter your name and contact information into the <a href=http://wiki.kde.org/tiki-index.php?page=CeBIT2007>Wiki</a>. The <a href=http://wiki.kde.org/tiki-index.php?page=CeBIT2007>CeBIT KDE booth wiki page</a> is also the place where you'll find more information concerning the organization of this year's CeBIT KDE booth.
<br>
<p><img src="http://developer.kde.org/~tackat/marble/logo_linuxforum.jpg" class="showonplanet" />
<br>
<p><img src="http://developer.kde.org/~tackat/marble/marble_cebit.png" class="showonplanet" />
<br>
Above you can see a screenshot of <a href=http://blogs.kde.org/node/2412>Marble</a> showing the city of Hanover where CeBIT takes place.

<B>Update:</B> We aren't really in urgent need for people for the weekend anymore. This doesn't mean that we don't welcome you to help out. It just means that we are short on accomodation facilities.
The only day where we are currently short on volunteers is the last day of the fair: wednesday March 21st.
<!--break-->