---
title:   "Raise your voice for Marble!"
date:    2011-07-07
authors:
  - torsten rahn
slug:    raise-your-voice-marble
---
<a href="http://community.kde.org/Marble/VoiceOfMarble"><img src="http://community.kde.org/images.community/a/a2/Voice-of-marble.png" class="showonplanet" /></a>
Have you considered contributing to the <a href="http://edu.kde.org/marble">Marble (Virtual Globe)</a> yet? The <a href="http://community.kde.org/Marble/VoiceOfMarble">Voice of Marble</a> contest is about to end in 8 days, so you could make use of your weekend by contributing: 
<p><i>
Thousands of people <a href="http://talk.maemo.org/showthread.php?t=67316">use Marble on the Nokia N900</a> to find their way and explore the world. Become their voice!</i>
<p><i>
Record your voice speaking a handful of turn instructions like "bear left!" and participate in the Voice of Marble contest. <b>With a bit of luck, your voice will be chosen as the default speaker for voice guidance in Marble 1.2</b> (to be released in July 2011).</i>
<p><i>
We're looking for an <b>English speaker (male or female)</b> whose voice will be shipped with the Marble packages. 
<p><i>And we're also looking for alternative <b>speakers for all other languages</b> - at least one each, and that's a lot!</i>
<p><i>
Please participate in the contest and spread the word among your friends. The five best contributions will get a cool Marble T-shirt as a little present.</i>
<p>
Interested? Please head over to the <a href="http://community.kde.org/Marble/VoiceOfMarble">Voice of Marble wiki page</a> which contains all the details you need to participate.

<p>Interested in participating? Please follow these steps:
<ul>
<li> Choose one or more languages you want to record audio files in. Prepare translations of the commands for languages other than english. See <a href="/Marble/VoiceOfMarble/Translations" title="Marble/VoiceOfMarble/Translations">Translations</a> for details.</li>
<li> Record all 64 voice samples and store them as .ogg files. See <a href="/Marble/VoiceOfMarble/Recording" title="Marble/VoiceOfMarble/Recording">Recording</a> for details.
</li>
<li> Upload your voice in time for the upload deadline (cf. timeline above). See <a href="/Marble/VoiceOfMarble/Upload" title="Marble/VoiceOfMarble/Upload">Upload</a> for details.
</li>

<p><b>The deadline for submissions is Friday, July 15th 2011.</b>
<!--break-->
