---
title:   "Osnabrück 2010 or the Snow Wonderland"
date:    2010-01-10
authors:
  - tstaerk
slug:    osnabrück-2010-or-snow-wonderland
---
This weekend we, the KDE PIM developers, met again in Osnabrück to develop and discuss the future of kmail, korganizer, kjots, akonadi and other software for Personal Information Management.

<a href=http://community.kde.org/KDE_PIM/Meetings/Osnabrueck_8>There</a> you can find all meeting minutes, time tables and results, here I want to outline what was most important to me.

* wikis are commonly accepted as the best medium to share collaborative information. As there are many developers who are lazy in writing documentation (like me), having our <a href=http://userbase.kde.org/>userbase</a> as a wiki makes perfect sense. We reach more potential contributors and make it easier for everyone to improve the docutainment. Plus, users know best what users want to read in the KDE manuals, may they be online, offline or printed. In my opinion, this is why our <a href=http://wiki.kde.org>three wikis</a> will bring KDE into much more households.

* my personal infrastructure is getting better and better. Since the last meeting I have added UMTS and a touchscreen to my personal hardware equipment. When Patrick talks about SyncML, I just take a pencil and do the meeting minutes by handwriting on my touchscreen. I recommend <a href=http://xournal.sourceforge.net/>Xournal</a> to everyone who wants to do so. With UMTS I was in the internet all the time when I was in a metropolitan area. I pay 2 Euro 50 per day in the internet and I do not have a monthly rate nor a limitation in traffic with the solution I describe <a href=http://www.linuxintro.org/wiki/Umts>here</a>.

* the KDE PIM community has understood that it is not enough to develop excellent technology. You also have to talk about it, and not only within KDE, but to the public. Last year, we brought "fresh" students in to write software for the Nokia N810. This year, even better, we invited a journalist who will write a print-article for Linux User or Linux Magazine (sorry I forgot which one).

* the PIMsters are cool. On our way to the group photo, we started a snowball fight. This is, because it does not really fit our age, a good sign for people who are individualists and do care more for their fun than for what society expects from them. And never forget who won the battle ;)

* Paul is driving the visualization stuff that I like so much to perfection. Similar to my <a href=http://www.staerk.de/thorsten/index.php/Software/Wiki2mindmap>wikimindmap</a> he uses graphviz to visualize svn commits: Programmers A and B are nodes, if they contribute to the same file, they get an edge connecting them. Makes a wonderful mesh and in the center, there is krake. What Paul probably did not realize is that the german Krake means octopus in english :) Paul, if you get a chance, please comment on this article where we can find your pictures. Also, Bertjan still continues with his parsing ideas. In other words: parsing+visualization==(cool) results;

* thanks to Volker I am one step further in getting KDE to kompile on my cool Nokia N810 :)

Now all that's left is to say to <a href=http://www.intevation.net/>Intevation</a> "thank you for the fish" :)