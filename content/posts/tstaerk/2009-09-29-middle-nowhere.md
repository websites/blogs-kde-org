---
title:   "From the middle of nowhere"
date:    2009-09-29
authors:
  - tstaerk
slug:    middle-nowhere
---
I recently had a crash that was hard to fix. Well, it IS hard to fix because I am still on it. It is again one of these "from the middle of nowhere" bugs KDE is so good in producing.

I means those bugs that originate from a wrong-understood idea of automation. The programmer <i>does not have to do anything</i> because <i>the framework will do it for him</i>. Super. That leads to a problem if the framework does not do it correctly. The programmer will not see any way how to debug the problem at all and will have to become a framework-developer. By this, the advantages of <a href=http://en.wikipedia.org/wiki/Division_of_labour>division of labour</a> (invented about 6000 years ago) have been reverted.<br /><br />
The code I have to debug (not written by me) reads like this:
<pre>
    addModule( "ktimetracker_config_behavior" );
</pre>
BAAM! This crashes. It uses some .desktop file and tries to build a configuration dialog out of it. Now debugging this means going through all of the addModule/desktop file/factory hell which is simply not what I want. Have you ever tried to <a href=http://techbase.kde.org/Development/Tutorials/Using_KActions#Defining_your_own_help_menu>change the "help" menu entry in a KDE application</a>? It is virtually impossible. It just "comes out of the middle of nowhere". Same applies to the .desktop files and what actions they trigger. It is not in your code. Same applies to slots that are called when a signal (an invisible goto) is emitted. Same applies to exceptions that may interrupt your program and cause some side effects without you even noticing it. Let's not even talk about how often I have hunted a method through 3 parent- grandparent or great-grandparent-classes. It makes your life as a programmer worse, not better.

But the "out of the middle of nowhere" technique is not limited to programmers. They quickly arrive at the user - who is by definition incapable of solving a software development problem. BTW, when have you used ASSERT for the last time? It does exactly this - causing a crash and <i>hope</i> that the user-developer communication is good.

Alltogether: As long as user, tester and framework-developer are one person, "out of the middle of nowhere" techniques are fine, but at minimum 2 persons, you are going to get into trouble.