---
title:   "\"including all members\" only means \"including all KDE-inherited members\"?"
date:    2008-12-23
authors:
  - tstaerk
slug:    including-all-members-only-means-including-all-kde-inherited-members
---
Today I <a href=https://bugs.kde.org/show_bug.cgi?id=84510>fixed a bug that has been open for more than 4 years</a>. This feels good. However, there is a reason why it took so long: kdialog contains a member winId() as you can see <a href=http://websvn.kde.org/trunk/KDE/kdepim/ktimetracker/idletimedetector.cpp?r1=900568&r2=900567&pathrev=900568>here</a>, but this is not documented in <a href=http://api.kde.org/4.x-api/kdelibs-apidocs/kdeui/html/classKDialog-members.html>our api documentation</a>. KDE's api documentation has a list "This is the complete list of members for KDialog, including all inherited members." where winId() is missing although it is inherited from QT!
<br /><br />
I suspect the problem is that winId() is inherited from QT and not from KDE, however, this is something for you to be aware of: The API documentation might be incomplete.