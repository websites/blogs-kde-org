---
title:   "KDE 4 is not user ready"
date:    2009-02-06
authors:
  - tstaerk
slug:    kde-4-not-user-ready
---
It is often said that many open-source-software is not enterprise-ready. But in order to be enterprise-ready, software must first be user-ready. I want to give you a feeling what I mean.

I am in a small team where we provide a Linux Terminal Server (LTS) for a company. It is based on NX. Every employee in this company can use the service, however, we provide it free of charge and out of enthusiasm. That means, we are not paid for setting it up nor for giving phone-support. We sometimes have 70 concurrent users on the server, that may mean we reach 500 users on the whole. The server is running KDE 3.5 as desktop environment. Recently, we evaluated - no, let me keep this understandable - we sat together and discussed the possibility of upgrading to KDE 4.

Everyone including me was against the upgrade. This is especially ashaming for me as I am spending every weekend to develop KDE. So what were the reasons?

If you install a KDE 4 desktop by default, you do not have the possibility to add icons to your desktop by right-clicking onto the desktop. That would mean to us: Take 500 phone calls, explain users why it is no longer possible, explain 500 times why we do a change if it is a change to the worse... You got it, 500 times an ENOTAMUSED.

If you install a KDE 4 desktop by default, you do not have the possibility to move the clock in the panel. For me, the clock is ticking constantly on the left where I do not want it. Our users will be upset seeing another change to the worse. Yes, there is a work-around but it is so complicated that I do not want to tell it 500 times on the phone :(

If you install a KDE 4 desktop by default, you get a strange icon in the upper right corner. No one could explain to me what it is called, but everybody said it was something about Plasma. Users will click on it and eventually hit "Zoom out". Then, their screen is filled with strange gray squares. Just imagine you have to sit on a phone and answer 500 phone calls (for no money) from users who all tell you something about "squares" not knowing they should call it "activities".

OK, so you probably say I should point every user to <a href=http://userbase.kde.org/Tutorials/KDE3toKDE4>http://userbase.kde.org/Tutorials/KDE3toKDE4</a> where all the work-arounds are explained? Or maybe invest time and change the settings from default to whatever I like? Frankly, no. Why should I do this as long as there are other desktop environments that leave me from yelling colleagues phoning me? <a href="http://www.joelonsoftware.com/uibook/chapters/fog0000000059.html">Users do not want to decide on things. They want them to be right in the first place.</a>

If I compare KDE 4.2 with KDE 3.2 in that spirit, KDE 3.2 was more user-ready. The easy things were possible easily. The easy things that mere users care about, like placing an icon on the desktop. And there was no confusing Activity concept. How could this happen? I think the first disaster were the <a href=http://techbase.kde.org/Schedules/KDE4/Goals>goals for KDE 4</a>. When I read them, they almost only contained developer stuff: 
<ul>
<li>Base development on QT 4</li>
<li>Revised API</li>
<li>...and so on</li>
</ul>
Nothing like "make KDE faster". No word like "make it easier to use by...". Only: "Make it different to program".
So we come to Thorsten's law: <b>If you ignore the user uses your software to accomplish something, you will fail</b>.

"This is open-source", you will say, "file a bug and it will get fixed.". Bad luck, my dear. The reason why I write this is it does not get fixed. I filed <a href=https://bugs.kde.org/show_bug.cgi?id=183310>a bug report about the missing icon functionality</a> and <a href=https://bugs.kde.org/show_bug.cgi?id=182219>one about the missing movements in the panel</a>. Both were closed. Makes the impression we do not <b>want</b> to improve doesn't it?

Can we please make KDE more usable by default?

