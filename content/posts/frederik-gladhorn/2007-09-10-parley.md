---
title:   "Parley"
date:    2007-09-10
authors:
  - frederik gladhorn
slug:    parley
---
KVocTrain is dead.
Welcome <a href=http://en.wikipedia.org/wiki/Parley>Parley</a>!
You might already have noticed when synching kde-edu yesterday or today that the KVocTrain folder has gone and been replaced by Parley.
And not only the name has changed. I was able to close quite a few bugs while rewriting the better part of the old KVocTrain.
One bug that was very annoying was closed yesterday by simply enhancing the gui a little. It is a feature that Jeremy implemented in our rewrite of the kvtml (Parleys file format) lib. In the old KVocTrain one could have three languages (see screenshots).
Let's say German-English-French.

There are example where this is more usefull like English, Chinese Tranditional and Chinese Simplified, but since Chinese is Chinese to me, I have a hard time giving examples in that language...
Anyway, if I had German-English-French, I could only practice German-English or German-French but never English-French, because the first language was seen as "original". This set up was deeply incorporated in the sources, it took almost half a year of kicking at KVocTrain to get it out.
Now the code is much simpler and cleaner without special treatment for some original language.

The old KVocTrain with its query menu:
<p><img src="https://blogs.kde.org/files/images//kvoctrain-configure-practice_old.preview.png" alt="The old way of starting a query (test)"></p>

Parley - configure a test:
<p><img src="https://blogs.kde.org/files/images//kvoctrain-configure-practice.preview.png" alt="Parley test configuration"></p>
Pheeew.
I really think Parley has gained a lot over KVocTrain. And so yesterday the train had it's last ride for good.

Another funeral notice: The term query is gone from Parley.
It always got much attention but little understanding.
Native speakers felt like being treated like a data base, other people using the English version of KVocTrain were just confused.

So let's practice!
<!--break-->