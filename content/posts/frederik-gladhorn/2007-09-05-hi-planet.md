---
title:   "Hi Planet"
date:    2007-09-05
authors:
  - frederik gladhorn
slug:    hi-planet
---
Who am I? Good question, next question please.
But seriously, my name is Frederik Gladhorn and this is my first post on the planet, if everything goes well :)
I have started taking over KVocTrain some time ago.
KVT had some rough edges and still does.
"Bug 108568: kvoctrain interface is very confusing, can't figure out a thing"
Is by far my favorite. Closing it will almost make me sad...<!--break-->
Edu people probably know me by now from bugging them on IRC.
I have been working with Jeremy Whiting to get the keduvocdocument lib into shape, letting him do most of the work actually. The result is quite satisfactory, as KAnagram, KHangman, KWordQuiz and KVocTrain now have a XML file format that is actually readable for humans too.
After that was done, I used the chainsaw (as Pino) noted to go through the KVT code the last weeks or so. I'm quite satisfied with a much cleaner and easier to use interface.
Now KVocTrain really longs for a nice shiny new Oxygen icon. Usability suggestions are welcome as well :)
Another thing to quickstart people using the program is a wizard which also got redone last week.
The obligatory screenshot of the new language selection dialog:
<img src="https://blogs.kde.org/files/images//kvoctrain-language-edit.preview.png" alt="KVocTrain language dialog" title="KVocTrain language dialog"  class="image preview" width="640" height="391" />
This will probably only make people happy who know the old way of setting up languages in KVT... it makes me happy ;)
Frederik
