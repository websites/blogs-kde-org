---
title:   "Meeting KDE"
date:    2008-04-18
authors:
  - frederik gladhorn
slug:    meeting-kde
---
This morning Patrick complained that he had no picture of himself while he was at the cebit.
So here comes remedy. I stumbled across some other random KDE related pictures also...

Edu meeting last year:
While waiting for pizza...
[image:3404]
[image:3414]
... we made the Step team eat snails.

KDE at CeBit:
[image:3405]
[image:3408]

And as soon as the visitors started to leave...
[image:3406]

[image:3407]
At the interop-ability party of Novell-Microsoft (does the spelling hint at missing parts of specifications?)

KDevelop meeting last weekend (and last week for the KDevelop guys)
[image:3409]
[image:3410]
[image:3411]
[image:3412]

And now for you, dear reader... which world famous painting is Jeremy standing in front of?
[image:3413]
<!--break-->