---
title:   "2009 already"
date:    2009-01-15
authors:
  - frederik gladhorn
slug:    2009-already
---
Ok, it's sort of late... - I wish everyone a happy 2009 :)
And since I just found my camera:
<img src="http://ktown.kde.org/~gladhorn/blog/people/danimo_lydia.jpeg"/>
What a way to start 2009 - Danimo enjoys his swimming pool and Lydia her green dragon.

<b>Edit:</b>
This was before they found out their drinks were mixed up and they switched, so Lydia has Danimo's swimmingpool and Danimo the dragon... ;)