---
title:   "I'm going to....."
date:    2008-07-08
authors:
  - frederik gladhorn
slug:    im-going
---
I took a sneak peek at Kdevelop. I did so before, but after waiting a month or two and reading Andreas' <a href="http://apaku.wordpress.com/2008/07/06/welcome-bugreports-for-Kdevelop4/">announcement</a> I was curious to see the progress.
I've been impressed at how great the definition-use-chain (that David Nolden is working on) is becoming (it seems to know more about my code than I do ;)). Hovering over a variable highlights all occurrences of it, pretty helpful sometimes. It gives you auto-completion, shows all uses of a function and probably does a whole lot more fancy stuff that I haven't discovered yet.
[image:3547]
Another thing I that is extremely useful is the quick open (file/class/function) dialog which helps jumping around in the code. This is already available in Kdevelop 3 though I never noticed until it was demonstrated to me. Some easy-to-remember key combo like ctrl-alt-m will take you there, just give it a try.
The Kate part is very nice, though it's a bit too color full maybe.
I've been taught to choose cmake and simply point to the root folder of my project and let Kdevelop do its magic. Adding a build folder will allow compilation too.
With the new version it is possible to have more than one project open at the same time. That rocks.
There are lots of things that are not done yet. The class view for example opens the declaration instead of definition of a function by default. But I could already use it productively :)
I chose the smallest project I could think of - the soc branch containing only Parley and its lib. I decided to be brave and let the entire project be parsed in the background. That option is off by default and already took quite some time on the few files I have, so that might be a wise default. Usually Kdevelop parses files as they are opened and at reasonable speed too and then goes for the includes. Nice!
Keep up the good job Kdevelop guys, I'm really happy to see Kdevelop taking shape!
Useless proclaimers are fun and useless, so here it goes: Kdevelop is in alpha state, so unless you have strong nerves and a reflex to save every minute, you'll have to step in and get Kdevelop ready (or wait a little longer).
Testing the new Kdevelop I finally got around to fix a bug in the Parley summer of code branch. Since I introduced it, I guess it's only fair that I fixed it ;)

Of course the real reason for me to blog was this:
<img src="http://hemswell.lincoln.ac.uk/~padams/images/igta.png"/>
<!--break-->