---
title:   "No time, so I'll just dump some random notes here."
date:    2008-08-21
authors:
  - frederik gladhorn
slug:    no-time-so-ill-just-dump-some-random-notes-here
---
FrOSCon is coming up tomorrow-like... need to get ready, who stole all my time?

Still some notes from Akademy. It was my first (therefor best) Akademy! I can only say it was great meeting so many nice people. Others have blogged about that quite a bit already, so I will save me the effort ;)

Things that have not been said yet? Sébastien Renard gave a great overview about the French translation team and the use of python scripts to make their lives easier. I hope more teamwork across translator teams will happen. Too bad not many people attended.
I still have a few nice Akademy pictures, maybe I'll get around to put them online somewhere.

Hm, some of my time got eaten by this gadget thingy... Hey Nokia, you don't get my soul! Wait, it runs Linux and can be played with? Ok, we have a deal.

I don't have wireless around here, but a fast cable, so how about <a href="http://wiki.maemo.org/USB_networking">USB networking</a>?
If you want internet via usb with maemo-diablo most likely you have to install a package called libicd-network-dummy because otherwise all apps assume you are not connected and won't try to fetch data. After installing reboot and select DUMMY as network connection.

Then I started playing with the N810. Thanks to Qt being readily available it is quite possible to start compiling Qt apps for it. I'll leave getting kde to work to the others, probably <a href="https://blogs.kde.org/blog/2892">Marijn</a> is first.
After setting up scratchbox, getting <a href="http://code.google.com/p/arora/">Arora</a> to run is quite easy.
After building it with the arm target I could simply copy over the binary.
<br/>
<img src="https://blogs.kde.org/files/images/arora.preview.png"/>
The mozilla browser is not the only one on the N810 any more :) (though it works better since it's tuned for maemo of course).

Next thing to try was marble since it has a Qt-only version. I had to fix only one minor qreal issue since marble has been ported before for other devices. It runs sluggish, but what would you expect from a device with no fpu?
For marble I created a build dir below the source dir and ran cmake from there. I decided to use an external sd-card for marble.
cmake .. -DCMAKE_BUILD_TYPE=release -DQTONLY=ON -DCMAKE_INSTALL_PREFIX=/media/mmc1/marble/ -DMARBLE_DATA_PATH=/media/mmc1/marble/data/
It does not manage to run the tile creation, so make install fails. Second try, it pretends to succeed, without actually creating the tiles, but installs.
Assuming you have installed marble, there is no need to create the map tiles on the device, simply copy over the data dir from the normal installation.
Putting marble not into /usr forced me to symling it's lib, though there probably is a better way than this hack.
Create a symlink /usr/lib/libmarblewidget.so.6 -> /media/mmc1/marble/lib/libmarblewidget.so.6
<br/>
<img src="https://blogs.kde.org/files/images/marble_shot.preview.png"/>
<br/>
And even Open Street Map works with Marble on the N810:
<img src="https://blogs.kde.org/files/images/marble_shot2.preview.png"/>
<br/>
Have fun!