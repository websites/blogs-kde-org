---
title:   "Parley is sexy!"
date:    2007-09-19
authors:
  - frederik gladhorn
slug:    parley-sexy
---
Ok, it's been a while, but Parley did not lay sleeping. Actually it ate up my spare time pretty well... <a href="http://dot.kde.org/1189514559/1189615073/">Parley is sexy?</a>
Today I got an <a href="http://edu.kde.org/parley/">improvised page for Parley</a> up.

There is a great <a href="http://blue-gnu.biz/content/using_kvoctrain_build_your_foreign_language_vocabulary">story about KVocTrain</a> online.

And hearing that people actually already use Parley in a productive way to learn for example Latin makes me happy. It shows how stable and usable Parley has become. It celebrates the age of ten days today.

Annma was suprised by Parley being able to do fill in the missing words tests:
<a href="http://edu.kde.org/parley/screenshots/example_lord_of_the_rings.png">
<img src="http://edu.kde.org/parley/screenshots/screenshots/example_lord_of_the_rings_small.png">
</a>

The only thing that Parley is still badly missing is a fresh icon.
<!--break-->