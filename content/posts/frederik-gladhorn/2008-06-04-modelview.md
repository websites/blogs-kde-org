---
title:   "Model/View"
date:    2008-06-04
authors:
  - frederik gladhorn
slug:    modelview
---
<i>I always pass on good advice. It is the only thing to do with it. It is never of any use to oneself.</i>
Oscar Wilde

Quite some time ago, Pino gave me a great tip (hm, maybe advice is of use occasionally)...
Use <a href="http://labs.trolltech.com/page/Projects/Itemview/Modeltest">Modeltest</a> from <a href="http://labs.trolltech.com">labs.trolltech.com</a> to validate QAbstractItemModels.
It's as simple as including modeltest.cpp in your CMakeLists.txt, add
<code>#include "modeltest.h"</code>
and when constructing the model, hand over the model pointer to a ModelTest instance.
<code>new ModelTest(myQAbstractItemModel, parent);</code>
Then you'll have a hard time starting your app for a while, since ModelTest is somewhat picky (but that's what you wanted, right?).
Once you manage to get past all the ASSERTS checking for correct parenting and the like your model will have been a bit refined.
In the end I think it's quite a good idea to go through this hassle to have a valid model.
At least it helps getting rid of some obvious bugs with models.
So be sure not to forget beginInsertRows and friends :)
Have fun.
<!--break-->
