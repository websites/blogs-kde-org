---
title:   "Release Party in Stuttgart"
date:    2008-01-19
authors:
  - frederik gladhorn
slug:    release-party-stuttgart
---
At the "Hochschule der Medien" (applied university for media (?)), which is really close to where I live, a Linux day took place yesterday. I only got there when most of the show was over already, but in time to listen to the talks I was interested in, given by the Amarok promo people (Lydia and Sven, the official amarok-beer-manager(titles are important, right?)) and a KDE4 intro by Lydia and Ingo (local KDE enthusiast) which was fun, with a diverse (rather small) audience.
Afterwards we sat down in the S-Bar to have a few beers and talk some more, until we finally got the live stream working to watch Aaron talking.
So thanks to those who showed up, it was fun. Thanks for the nice organization also!
