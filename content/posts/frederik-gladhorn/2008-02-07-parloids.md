---
title:   "Parloids"
date:    2008-02-07
authors:
  - frederik gladhorn
slug:    parloids
---
Plasmoids have been creeping into different parts of KDE... The E-Team lately spotted two of them. So maybe check out KDE-Edu to get a real hot calculator made by apol. The gui is in need of some love, but it's already very powerful since it uses KAlgebra behind the scenes (yay, scientific calculations on your desktop).
[image:3267]
The other one I quickly hacked together, using artwork that leeo did for the icon originally.
Being reminded by aseigo to seperate engine and applet, I had a glimpse at how engines are created, decided it's easy enough, so the engine is there too now. It can easily be extended to give tons of data, so I'm open to crazy ideas, if you want it to spit out more data, just tell me. Font config works, but some layouting might do good. And feel free to come up with an improved design and especially layout. As I have little time lately and rather want to get Parley in trunk into a working state again, I won't update the plasmoid much. Junior jobbing anyone? Great place to start, I already have a couple of ideas... drop by in #kde-edu on freenode.
<!--break-->