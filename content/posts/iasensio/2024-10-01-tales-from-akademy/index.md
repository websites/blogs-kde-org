---
title: Tales from the Akademy
authors:
  - iasensio
date: 2024-10-01T23:15:00Z
discourse: Ismael
categories:
  - Akademy
---

This being my first post on the KDE sphere (or any other sphere),
it was supposed to be just a touch of contact with the world of blogging. But since time pass by in a blast, let's just summarize how I lived my third one in-person Akademy 2024.

![Konqi looking amazed at the Akademy program](konqi-awes-akademy-program.jpg)

## Würzbug. Back to Germany

This year's Akademy happily got me back to Germany, which has become like a second home and a place I like to visit at least once a year (yeah, I missed the Dürüms).

I had bought the D-Ticket, which allowed my to board any public transport immaginable (well, except for ICE trains, but I haven't heard much good about them either) for bare 49€. It brought me some memories back as a student in Dresden, enjoying the same perks with the _Semesterticket_, just on a regional scope. Thanks to [Itinerary](https://apps.kde.org/es/itinerary/) and its route planner I was able to make it to Würzburg even an hour earlier than anticipated (less 20min train delay which I've heard it's currently quite a good metric).

After having my hotel booking cancelled last minute due to needed repair works, I had booked an appartment because the hotel prices were a bit over the top. I was really lucky to find that just around the corner I had a bus stop to go to the venue, and also Andy Betts and [Richard Wagner](https://wuerzburgwiki.de/wiki/Wagnerhaus) as ilustrious neighbors. And one of the best rated Dönner places in the city. Very lucky indeed!

## The Talks

It's hard to make a better summary of the Talks days that our very own [Promo Team's report](https://akademy.kde.org/news/2024-09_akademy-2024-report/), which I agree with on many points.

What I particulary felt on these Akademy's talks was a high focus into the future. Some words were thematically present along most of the talks: story, product, and impact.

The story we as the KDE community want to tell is not just a bunch of code packages that live in an ethereal world to be grabbed by a few enthusiasts or distros, but a full useful product for the end users, an inviting environment for fellow developers, and a reliable asset for manufactures on their very concrete hardware.

There were many reveals and surprises to achieve this goal. Projects that had been incubating for some time, were now made public on this Akademy: the KDE OS Codename Banana by Harald Sitter, the Next Project and design system by Andy Betts and the Union theme engine by Arjen Hiemstra.

Some talks addressed the social and environmental impact of the technology we create. The one that specially got to me was the _small_ story Nicole Teal told at her lightning talk. How a group of kids gave many "older" PCs a new life installing KDE, while learning new skills and making community, felt really true and a spur to continue contributing to FOSS. It really matters.

From the technical talks, I enjoyed _"What is color, anyway"_ by Xaver Hugl, and unfortunately had to miss some other ones (Python and Rust integration with Qt). This is the hardest part, where you cannot just `.clone()` yourself and attend to two talks at the same time. Maybe I would have learnt to do that if I had attended the Rust presentation? _(yeah, sorry bad Rust dad joke)_

It was also on Sunday when Aniqa and Carl took me by surprise to ~~agains my will~~ happily answer to a small video interview. Just joking, it was fun. Just preemptively preparing myself for when the final video comes out and I can see what words I did babble :D.

## The BoFs

After a very intense weekend of talks and the social event and post-event on Sunday, I took the Monday's morning off to have some rest. In the evening, Andy and Manuel showed me a bit more about the design system they're using and the icon exporter Manuel has been developing to streamline the process between designers and the final product. Amazing stuff!

I also started a draft of this very blog post, which wasn't much successful as you can imagine by its final release date.

The big BoF day for me was mostly Tuesday, where I focussed on the Plasma and the VDG ones, though I missed those on KWin's roadmap and window tiling, due to competing schedules. During the Plasma BoF, we could experiment in real time, the step-by-step process of realeasing the Plasma 6.1.5 version, thanks to Jonathan, our Plasma release manager.

Finally, on Thursday I got to enjoy the brand new Sticker BoF. Besides me not having any stickers on my own to share, and being mostly minimalistic when it comes to decoration, I had a great time and ended up sticking my laptop up and about, including an very limited unit of the Sticker BoF's sticker. Thanks Kieryn for organizing it. Of course, Carl won the sticker's award 😄.

![My laptop with some stikers](laptop-with-stickers.jpg)

On a more personal level, I regret a bit not having participated more on some of the BOFs. Most of my KDE's contributions this summer have been improvements on very niche aspects: the Weather widget and the tool to preview keyboard layouts (_tastenbrett_), so I felt a bit "out of the loop" on the more general and pressing matters in Plasma.

## The Socials

Where the Akademy really shines is in putting together some hundreds of amazing people with some common interests, that in the end happen to make the best software products and computing ecosystem out there.

It is a real warp of space and time. On the Welcome Event I got to meet Eva Brucherseifer, one of the attendants and founders of the very first Akademys, and also recent joiners to the community I only knew via chat or MR interactions.

When the Biergarten that was booked for the Sunday Social Event did cancel due to a storm warning, I could immediately check two things:
- that the Weather Widget did correctly report the _Warnung vor starkem Gewitter_
  ![Weather warning report](weather-warning-storm.png)
- and that the local organizing team went the extra mile to make the Akademy a success, even against the elements. Beer, pizzas and good people was all required to have an enjoyable evening.

Finally I was really happy to meet again with friends from the previous Akademys and the Plasma Sprint in 2023, sharing opinions on widespread topics, suchs as immovable OSes, ingenuous ways to open a beer bottle, keyboard input methods, or the torture and punishment customs of German cities in medieval times.

Thanks to the organizing team, the speakers, the attendants, the patrons and the whole KDE Community which made possible yet another amazing Akademy!
