---
title: Akademy 2024 Experience
authors:
  - justinz
date: 2024-10-29
categories:
  - Akademy
discourse: justin
---

## Getting There!

Wow! What a trip! 20 hours across 2 flights, 2 hours on the train with travel buddies Nate Graham and Bhushan Shah, and several bus "adventures" with Nate Graham to our hotel. The hotel... Let's not go into too much detail, suffice to say it was an absolute mess.

This being my first Akademy in person it was a very anxious experience getting there, but with global roaming on my phone to keep communication flowing and a few travel buddies it was certainly made much better!

But once we were settled in and unpacked, it was off to the first event!

## The Welcome Event!

Wow, was it chaos once all the people showed up, but amazing to see so many KDE users and developers! A few locals even popped their head in, confused by the packed out venue. We thankfully managed to get a ride in  Adriaan de Groot's smooth E.V to the venue and found a few others after parking.

The place had a great vibe and the free drinks and snack courtesy of KDE went down a treat! I quickly connected to the free Wi-Fi, spun up some translations of the menu and grabbed the Mexican Fries with guacamole, tomatoes and onions. It was amazing with a few drinks to wash it all down!

On to the main event!

## Saturday Talks!

Having the talks start a little later was great as it meant we didn't have to get up early and get rushing out the door!

Adriaan also bought Stroopwafels with him! So delicious!

### Only Hackers Will Survive
The first talk (barring the brief opening by the Akademy team) was right into the thick of it, and it was about circular economies, electronic waste and how we, the hackers, have the right mindset to keep things working well beyond manufacturers expected lifetimes. Whether they be limited by lack of software updates or pushed out of the market by ever more demanding software performance requirements.

The scenes of landfills around the globe on display with people in Third World countries sorting through them to reclaim precious metals on display to really bring home the true impact. Here's hoping KDE's software and projects can help with this, especially with the [Blue Angel Certification](https://eco.kde.org/blog/2024-08-26-revised-blue-angel-criteria/). I am really hoping to see [Plasma Mobile](https://plasma-mobile.org/) continue to take charge in this area, where mobile devices are often discarded rapidly in favour of the newest shiniest thing.

### Goals Wrap Up!

Every two years, KDE chooses new [goals](https://community.kde.org/Goals) to focus on to make KDE's software better for all. At this year's Akademy it was time to review the goals outcomes for the last two years.

Carl Schwan went through his accessibility goal outcomes which included a discussion about how our hardware partners want to improve accessibility, 190 code changes that were accessibility related and a new [accessibility inspector application](https://invent.kde.org/accessibility/accessibility-inspector)! There was also notes around the Appium CI/CD test suite and Selenium driver test suite to help with ensure accessibility in our as well as [Project Spiel](https://project-spiel.org/) a new text to speech engine for the Free Desktop plus turning accessibility into a permanent goal for KDE.

Nate Graham wrapped up his goals around automation and systematisation. These were about being lazier in a good way, automating the boring trivial things we spend time doing all the time. Creating policies to reduce the debate on issues with opinions impacting what we do, instead of policy. Working as teams instead of alone and documenting how we do things instead of keeping tribal knowledge locked away in our brains.

I wasn't able to attend much of Cornelius Schumacher's KDE Eco goal so I didn't get any notes on that but I do love that KDE cares for the environment and this should be an ongoing goal to make sure our software (and hardware partners) are acting responsibly to ensure a brighter future for the next generations.

### Report of the Board

The KDE board assembled in person to give us a run-down of 2023 including all their favourite things that happened like paying members going from 53 to 719, GitHub sponsors up to 132 from 67, and 170 nonmember recurring donations up from 130 on Donorbox! On top of that, the best fundraising campaign ever in December 2023. A monumental task was also completed with the release of Plasma 6 alongside Frameworks and Gear at the same time! The KDE Eco grant was extended, they hired new staff as part of the [Make a Living](https://community.kde.org/Make_A_Living) goal, including someone to manage the goals. The new financial situation improved vastly due to a successful Donorbox rollout and fundraising campaign.

In looking to the future the board are encouraging more community members to get on board with the KDE Goals, with the improved finance situation they are hoping to leverage that to continue improving KDE's offerings. Conferences and in-person sprints are now viable post-apocalypse! Ask for funding/organisation from the board if you want to organise/attend one and represent KDE! They are also aiming to move beyond the Make a Living effort and leverage current staff for further work without losing sight of their goals and improve management of staff, contracts and keeping staff on. They're also hoping to expand our app store presence on Google Play, Windows Store and Flathub to get more apps on and investigate possible revenue sources from this.

### Adapt or Die: How new Linux packaging approaches affect wider KDE

David Edmundson gave an awesome talk about Linux packaging and where we are headed. Flatpaks! Oh, and I guess Snaps and Appimages and that new one from Deepin… But his talk was generally focussed on how KDE as a community will need to face the challenges of containerised applications since immutable distros are appearing all the time now. In this well researched discussion, he raised several case studies that he has identified as problem points for progress. This was one of my favourite talks, as I am a big proponent of Flatpak as the future of application distribution and plan on working with David to that end.

### Looking Back: What's Next
Wait what? What does that even mean? Nicolas Fella decided to share with us his run-down on how the porting from Qt5 to Qt6 went as a whole. From the timeline stretching way back to 2019 to the KDE Megarelease of 2024 including how each step was planned out and then managed into a reality. He also gave us a run-down of the good and the bad from his viewpoint. The good included lots of great new features and loads of pre-release testing. The bad included bugs (but you can never get rid of all of these), controversial decisions and broken distros as of release. He also noted a lack of documentation for third party users of KDE frameworks and some things that got left on the cutting room floor due to a lack of time to get them in. Those have been marked `TODO KF7` 😂

### An Operating System of Our Own
Harald Sitter gave us a run-down of his new Operating System KDE Linux (previously - and my favourite name - Project Banana). This is a new image based distro that uses BTRFS and images of the OS, with easy switching between them. This will be designed for anyone to use, from KDE developers to users and hardware vendors! It will bring apps from Flatpak (my favourite) and Snap to keep the OS and applications separate. I've been watching this for a little while before Akademy so I was happy to see it announced, and it has attracted many people onboard and has accelerated the development of it immensely! Come join us @ #kde-linux:kde.org on Matrix!

### A look on the Bright Side of Life
Harald gave us a quick lightning talk about remaining calm and enjoying what we do. Sometimes things are annoying or frustrating but sometimes we need to step back, look at all the awesome things we make and the amazing people we do that with. If in doubt, ask for help or a rubber duck and come back to fight another day.

## Sunday Talks!

Sadly I didn't take a lot of notes on Sunday but I attended a few talks.

* Openwashing - How do we handle (and enforce?) OSS policies in products? by Markus Feilner, Holger Dyroff, Richard Heigl, Leonhard Kugler and     Cornelius Schumacher. A discussion on companies using the title and reputation of open source without actually being open or contributing anything to the community.

* Group Photo - My first time in a KDE related photo!

* Contributing is more than just code by Kai Uwe Broulik was a really important talk for me as I can't code. It's just not how my brain works, but the topic of his talk resonated with me so much. KDE has lots of code, frameworks, apps, libraries and that requires a lot of code. But there is SO much more work to be done around that code, translations, quality assurance, bug triaging, documentation just to name a tiny few. If you're interested in contributing to KDE, there are [LOTS](https://community.kde.org/Get_Involved#Ways_to_Contribute) of things you can help with!

* Financial support for working on KDE
Jos van den Oever gave a great talk about how you as a KDE contributor can apply for funding. It was mostly focussed around [NLnet](https://nlnet.nl/funding.html) who had a representative there to encourage us to apply for funding and explain the application and approvals processes. They also stuck around for the next few days to help anyone who wanted to apply to submit their application!

### Daily driving Plasma Mobile and what's still lacking
Another talk that really hits home for me was by Bart Ribbers on Plasma Mobile. We've got amazing stories for our desktop offering, but the mobile space is a huge part of most people's daily lives. So many people don't own a computer any more, and they're living their digital life through a mobile phone. This talk gave insight into Bart's daily life with his Plasma Mobile enabled phone and where we need to narrow our focus to improve the experience.

## BoFs & Daytrip

Over the following days I attended many BoFs:
* Opt Green: Website Bloat and Green Web which we discussed how our website content affects the environment, from intensive JavaScript, oversized or large images increasing CPU usage and bandwidth to the core of the web itself and what Data Centres and traffic hubs use for their electricity, is it renewable or fuelled by dirty fossil fuels
* Opt Green / KDE Eco which was all about KDE Eco's certification of Okular, their involvement in defining the [Blue Angel](https://www.blauer-engel.de/en) specification, getting more KDE apps certified and how KDE was recognised as an expert in sustainability in the German parliament!
* KDE Goals - We care about your Input where discussions happened around how we improve input methods for those who use alternate languages and alphabets to game controllers and digital input tablets and devices.
* KDE's release schedules was all about starting discussions about when and how we release software, including how we can better cooperate with downstream distros who are kindly distributing our software

* Plasma Discover by Aleix gave us insight into the recent changes to performance by Harald and himself, and just a great general discussion around Plasma's current state and what is needed for the future of software stores.
KDE Goals - Streamlined Application Development Experience was a rather interesting chat about how we currently develop apps and many discussions about how we improve that process, introduce new blood to the community and make it seriously easy to start a new KDE application.

## Food!

This worried me in the lead up and on my way to Germany, as I had earlier this year decided to go Vegetarian. However, I was delightfully surprised and the quality and variety of food available, with the food organised by Akademy team being inclusive of my requirements and tasty! The food at University cafeteria was also amazing to my surprise and I learned that they had won awards!

I also thankfully found some students who sold me a crate of Cola for 10 Euros. I grabbed one and gave the rest to the Akademy help desk for them to give out to attendees for a 1 Euro donation ;)

## Outro

- What an amazing event, thank yous in no particular order go to Nate Graham, the KDE e.V for sponsoring my travel, the Akademy team and everyone I met at Akademy for making it an awesome experience especially for someone who hasn't been to a conference before, to all of the KDE community for making awesome software and the Akademy and KDE sponsors who make these events possible!
