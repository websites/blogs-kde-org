---
title:   "QRegExp + QStringLiteral = crash at exit"
date:    2015-11-05
authors:
  - sergio.martins
slug:    qregexp-qstringliteral-crash-exit
---
If you're seeing crashes lately, and they look like:
<code>
    (gdb) bt
    #0  0x00007ffff6790913 in QString::~QString() () from /usr/x86_64-pc-linux-gnu/lib/libQt5Core.so.5
    #1  0x00007ffff680fd89 in QHashData::free_helper(void (*)(QHashData::Node*)) () from /usr/x86_64-pc-linux-gnu/lib/libQt5Core.so.5
    #2  0x00007ffff6828e14 in (anonymous namespace)::Q_QGS_globalEngineCache::innerFunction()::Holder::~Holder() ()
       from /usr/x86_64-pc-linux-gnu/lib/libQt5Core.so.5
    #3  0x00007ffff620fed8 in __run_exit_handlers () from /usr/x86_64-pc-linux-gnu/lib/libc.so.6
    #4  0x00007ffff620ff25 in exit () from /usr/x86_64-pc-linux-gnu/lib/libc.so.6
    #5  0x00007ffff61fa617 in __libc_start_main () from /usr/x86_64-pc-linux-gnu/lib/libc.so.6
    #6  0x000000000040f5b9 in _start ()
</code>
and valgrind says something like:
    <i>Address 0x21d41498 is not stack'd, malloc'd or (recently) free'd</i>

it's most probably due to <i>QStringLiteral</i> usage, which in some rare cases can introduce crashes, and one of those cases is with <i>QRegExp</i>.

When you do:
<code> QRegExp(QStringLiteral("foo")); </code>

<i>QRegExp</i> will cache the string in a <i>Q_GLOBAL_STATIC</i> variable, and remember that because it's a <i>QStringLiteral</i>, it has it's data in <i>.rodata</i> of your KDE library (or wherever you defined it).

When you exit the application, your KDE library gets unloaded then QtCore get unloaded.
When QtCore is getting unloaded it runs DTORs of stuff with static lifetime, so <i>~QString()</i> gets called but <i>.rodata</i> from your KDE library was already unloaded -> crash.

If you want to know for sure which library the dissipated <i>.rodata</i> belonged to: run valgrind, and you'll get a message like:
<i>Address 0x21d41498 is not stack'd, malloc'd or (recently) free'd</i>

Then run valgrind with gdb support:
<code>$ valgrind --vgdb=yes --vgdb-error=0 <your_app></code>

then when your app is running, run "info target" in gdb and search for the respective address interval and you get the library you should fix.

Ofc, all this becomes a moot point if you just port your code to use QRegularExpression instead of QRegExp :)

