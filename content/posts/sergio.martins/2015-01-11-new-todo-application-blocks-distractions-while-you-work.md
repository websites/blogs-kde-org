---
title:   "New TODO application that blocks distractions while you work"
date:    2015-01-11
authors:
  - sergio.martins
slug:    new-todo-application-blocks-distractions-while-you-work
---
TL;DR: flow is a sticky TODO manager with support for the pomodoro technique and blocks distractions (cat pictures too) while you're focusing on a task: <a href="https://github.com/iamsergio/flow-pomodoro">git, </a><a href="https://aur.archlinux.org/packages/flow-pomodoro/">AUR, </a><a href="https://github.com/iamsergio/flow-pomodoro/releases/download/v0.9.5/flow-Windows-v0.9.5.zip">Windows, </a><a href="https://github.com/iamsergio/flow-pomodoro/releases/download/v0.9.5/flow-OSX-v0.9.5.zip">OSX</a>

"<b>A good task manager application is one that can beat a .txt file.</b>"

Even though I was (am?) KOrganizer's maintainer I could never get used to its TODO view. It simply didn't fit my workflow. The only time I used it was when I was programming it. For my important stuff, I used a text file instead, and was happy.

Meanwhile I've been experimenting with the <b>Pomodoro Technique</b> to help me focus at work and thought it would be fun to create an hybrid between todo manager and pomodoro app in QML.

So, what do we need to make a better organizer than notepad ?

Here's the 5 ingredients I chose to put in flow:
<ul>
<li>Sticky</li>
<li>Pomodoro</li>
<li>Block distractions</li>
<li>When, not what</li>
<li>Light</li>
</ul>

<b>1. STICKY</b>
Goal: It must be easier to add a task than it would to write it down on a piece of paper.
Flow's window is sticky (always above, always visible):
<br>
Contracted:
<img src="https://dl.dropboxusercontent.com/u/107699405/contracted.png" ></img>
<br>
Expanded:
<img src="https://dl.dropboxusercontent.com/u/107699405/flow3.gif" ></img>

Korg needs so many steps to add a TODO that people lose motivation.

<b>2. POMODORO</b>
Goal: Support the pomodoro technique. <a href="http://pomodorotechnique.com/">http://pomodorotechnique.com</a>

Each task has a play button, click it, you're now in a 25 minute focus block.
Work for 25 straight minutes *only* on that task. Then take a 5 minute break before you start another pomodoro.

<img src="https://dl.dropboxusercontent.com/u/107699405/contracted_play.png" ></img>

<b>"What kills productivity are the context switches, not <i>farmvilling</i>."</b>
It's OK to slack, space out or read junk e-mail, as long as you do it in batches.

The key is to group related tasks, do all your non-productive tasks at once, preferably in pomodoro breaks.
Don't go on reading e-mail every 5 minutes while you're working on something important.

When a co-worker asks you a favor, open flow and queue it. When the 25 minute focus block ends handle the tasks that you piled up meanwhile.

<b>3. BLOCK DISTRACTIONS</b>
Goal: While in a pomodoro, automatically block facebook and turn of KMail and IM distractions. When the 25m focus ends, re-enable distractions.

Flow supports blocking distractions when a pomodoro starts (via a plugin system).
Currently there are only plugins to block notifications for the apps I use (KMail and Pidgin), but there's also a "shell script plugin" where you can write shell scripts to do whatever you like.

<b>4. WHEN more important than WHAT</b>
How do you organize your TODOs in KOrganizer ? There are so many options: By Category, by resource, by todo/sub-todo hierarchies ?

I found out that for me the first level of grouping should be about the "when". In flow there are two areas,
the tasks for today, and the tasks for later and I don't need more granularity than later.

<b>5. SIMPLE and LIGHT</b>
Very low memory foot print, I'm also using it on my smartphone. Easy to install on windows and OSX due to minimal dependencies.

<b>Conclusion</b>
I think Korg is very good for long term planing, and I still use it for complex stuff  (like due date and recurrences stuff) . But it really sucks for micro-managing your day.

So there it is, please give flow-pomodoro a try. I would also like to hear about your current KOrganizer workflow and if you feel it makes you productive or not.

P.S: While writing this blogpost 3 KMail and 2 gchat notifications were blocked and one facebook attempt was killed.