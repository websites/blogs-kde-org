---
title: This Week in KDE Apps
subtitle: Back from Akademy
date: 2024-09-16
authors:
  - carlschwan
  - paulbrown
categories:
 - This Week in KDE Apps
---

Welcome to the first post in our "This Week in KDE Apps" series! You may have
noticed that Nate's ["This Week in KDE"](https://pointieststick.com/) blog
posts no longer cover updates about KDE applications. KDE has grown
significantly over the years, making it increasingly difficult for just one
person to keep track of all the changes that happen each week in Plasma, and
to cover the rest of KDE as well.

After discussing this at Akademy, we decided to create a parallel blog series
specifically focused on KDE applications, supported by a small team of editors.
This team is initially constituted by Tobias Fella, Joshua Goins and Carl
Schwan.

Our goal is to cover as much as possible of what's happening in the KDE world,
but we also encourage KDE app developers to collaborate with us to ensure we
don't miss anything important. This collaboration will take place on
[Invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests) and
on [Matrix #this-week-kde-apps:kde.org](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).

We plan to publish a new blog post every Sunday, bringing you a summary of the
previous week's developments.

This week we look at news regarding NeoChat, KDE's Matrix chat client;
Itinerary, the travel assistant that lets you plan all your trips; the Gwenview
image viewer; our sleek music player Elisa; KleverNotes, KDE's new note-taking
application; the KStars astronomy software; and Konsole, the classic KDE
terminal emulator loaded with features and utilities.

We also look at how Android support has been subtly improved, and  the effort
to clean up our software catalogue, retiring unmaintained programs and getting
rid of cruft.

Let's get started!

## NeoChat

Emojis in NeoChat are now all correctly detected by using ICU instead of a simple
regex. (Claire, NeoChat 24.08.2, [Link](https://invent.kde.org/network/neochat/-/merge_requests/1884))

On mobile, NeoChat doesn't open any room by default any more, offering instead
a list rooms and users. (Bart Ribbers, NeoChat 24.08.02,
[Link](https://invent.kde.org/network/neochat/-/merge_requests/1891))

Filtering the list of users is back! (Tobias Fella, NeoChat
24.08.02,
[Link](https://invent.kde.org/network/neochat/-/merge_requests/1886))

## Itinerary

The seat information on public transport is now displayed in a more
compact layout. (Carl Schwan, Itinerary 24.12.0,
[Link](https://invent.kde.org/pim/itinerary/-/merge_requests/324))

![](itinerary-seats.png)

## Gwenview

Rendering previews for RAW images is now much faster as long as KDcraw is
installed and available (Fabian Vogt, Gwenview 24.12.0,
[Link](https://invent.kde.org/graphics/gwenview/-/merge_requests/289))

## Elisa

We fixed playing tracks without metadata (Pedro Nishiyama, Elisa 24.08.2,
[Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/624))

## KleverNotes

The KleverNotes editor now comes with a powerful highlighter.
(Louis Schul, KleverNotes 1.1.0, [Link](https://invent.kde.org/office/klevernotes/-/merge_requests/113))

## KStars

The scheduler will now show a small window popup graphing the altitude of the
target for that night. (Hy Murveit, KStars 3.7.0,
[Link](https://invent.kde.org/education/kstars/-/merge_requests/1323))

## Konsole

You can set the cursor's color in Konsole using the OSC 12 escape
sequence (e.g., `printf '\e]12;red\a'`). (Matan Ziv-Av, Konsole 24.12.0,
[Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1029))

## Android Support

The status bars on Android apps now follow the colors of the Kirigami
applications (Volker Krause, Craft backports,
[Link](https://invent.kde.org/frameworks/kguiaddons/-/merge_requests/119))
![](kf6-android-window-inset-colors.jpg)

## Cleaning Up

We have archived multiple old applications with no dedicated maintainers and no
activity. This applies to Kuickshow, Kopete and Trojita, among others.
[Link](https://invent.kde.org/sysadmin/repo-metadata/-/issues/23)

## ...And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check
out [Nate's blog](https://pointieststick.com) and [KDE's
Planet](https://planet.kde.org), where you can find more news from other KDE
contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped achieve that status. As we grow, it’s going to be
equally important that your support become sustainable.

We need you for this to happen. You can help KDE by becoming an active
community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE; you are not a number or a cog
in a machine! You don’t have to be a programmer, either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general help KDE continue bringing Free
Software to the world.
