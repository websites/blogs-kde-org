---
title: 'This Week in KDE Apps'
subtitle: 'LSP Support in KDevelop, systemDGenie rewrite and big UI changes in Dolphin'
discourse: thisweekinkdeapps
date: "2025-03-09T19:55:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@carl@kde.social'
---

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/). This time we will cover the past two weeks as I was traveling last weekend.

Last week we released KDE Gear 24.12.3, which concludes the 24.12 series of KDE Gear. 25.04.0 is right around the corner, with only a few days left before the beta and feature freeze. Aside from the numerous bug fixes and polishing going on, we also had some pretty big changes in Krita regarding advanced text editing options, KDevelop with support for the LSP protocol, some big UI changes in Dolphin, and a complete rewrite of systemDGenie.

## General Changes

Balló György added improvements to many Kirigami projects for when they run with the software rendering backend. Projects that have been improved include [Kirigami](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1726) and [Kirigami Addons](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/350), but also many apps like [Tokodon](https://invent.kde.org/network/tokodon/-/merge_requests/720), [Kaidan](https://invent.kde.org/network/kaidan/-/merge_requests/1337), [Angelfish](https://invent.kde.org/network/angelfish/-/merge_requests/255) and more.

Volker wrote a small report about the recent improvements to KDE Apps on Android. You can find it [on his blog](https://www.volkerkrause.eu/2025/03/08/kde-android-news-march-2025.html).

We fixed an issue in KIO SFTP support where symlinks would be truncated (Kishore Gopalakrishnan, 25.04.0. [Link](https://invent.kde.org/network/kio-extras/-/merge_requests/403)), and another in KIO SMB support where shared resources from other computers on multiple LANs and virtual LANs were not displayed when using WSDD (Harald Sitter, 24.12.3. [Link](https://invent.kde.org/network/kio-extras/-/merge_requests/409)).

Another thing that got fixed was an issue where the report bug button would not open the report URL (Carl Schwan, KF 1.12.0. [Link](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/237)).

## Graphics and Multimedia Apps

{{< app-title appid="elisa" >}}

Balló György fixed restoring the hidden Elisa instance on file opening (Balló György, 25.04.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/669)), and Jack Hill fixed the spacebar play/pause action, as it was not being triggered when specific buttons had the focus (Jack Hill, 25.04.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/673)).

{{< app-title appid="gwenview" >}}

Pedro Hernández added an option to display hidden files (Pedro Hernandez, 25.04.0. [Link](https://invent.kde.org/graphics/gwenview/-/merge_requests/314)), and we changed how image size integers were displayed to make them clearer in all languages. Previously, we displayed `1,024x1,024`. Now it is `1024x1024`.

{{< app-title appid="kasts" >}}

Bart De Vries properly implemented single instance behavior (Bart De Vries, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/266)).

{{< app-title appid="okular" >}}

Okular now supports, in addition to S/MIME based signatures, PGP/GPG based signatures. PGP signatures have the advantages that it is a lot easier to get a PGP key than a S/MIME key. Note that this feature is not yet enabled by default and for the moment only works between Okular users (Sune Vuorela, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1124)).

## Creative Apps

{{< app-title appid="kdenlive" >}}

Darby Johnston added support for OpenTimelineIO export and import using the C++ library. This allows importing and exporting projects files to/from other video-editing applications that implement this open standard (Darby Johnston supported by [KDenlive fundraiser](https://kdenlive.org/en/fund/), 25.04.0. [Link](https://invent.kde.org/multimedia/kdenlive/-/merge_requests/561)).

{{< app-title appid="krita" >}}

Wolthera van Hövell implemented basic support for the [font-feature-settings CSS property](https://drafts.csswg.org/css-fonts-4/#font-feature-settings-prop) in Krita. This allows tweaking the rendering of text based on OpenType font features (Wolthera van Hövell, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2342)). Wolthera wrote an excellent blog post on this topic,  as well as covering the support of font variants mentioned two weeks ago. You can find the post [on her blog](https://wolthera.info/2025/03/controls-for-opentype/).

Maciej Jesionowski added a global pen tilt direction offset, which can be helpful to make the brushes feel the same for right- and left-handed users (Maciej Jesionowski, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2337)).

The process of porting Krita to Qt6 is making good progress: the macOS version now compiles (Freya Lupen. [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2334)), and the implementation of the tablet switching API for Windows is now using Qt APIs instead of a custom implementation (Dmitry Kazakov, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2341)).

In other news, Carl Schwan fixed the menubar visibility state being saved as non visible if the global menu option is turned on. This become an issue when turning off the global menu, as Krita's menubar wouldn't appear again (Carl Schwan, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2335)).

## Personal Information Management Apps

{{< app-title appid="korganizer" >}}

Allen Winter improved the agent selection dialog. Now the _Ok_ button is only enabled when an item is selected and the search text field has a placeholder (Allen Winter, 25.04.0. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/234)).

{{< app-title appid="merkuro.calendar" >}}

Shubham Shinde added support for displaying holidays in the week view and the month view. Note that it is possible to disable this feature (Shubham Shinde, 25.04.0. [Link 1](https://invent.kde.org/pim/merkuro/-/merge_requests/505), [link 2](https://invent.kde.org/pim/merkuro/-/merge_requests/506) and [link 3](https://invent.kde.org/pim/merkuro/-/merge_requests/508)).

![](merkuro-week.png)

![](merkuro-month.png)

{{< app-title appid="kleopatra" >}}

Tobias Fella fixed decrypting files with very long paths on Windows (Tobias Fella, 25.04.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/370)).

{{< app-title appid="akonadi" >}}

Milian Wolff optimized some code in Qt related to timezones to improve the performance of some serialization in Akonadi. (Milian Wolff, Qt 6.8. [Link](https://codereview.qt-project.org/c/qt/qtbase/+/626639/5))

{{< app-title appid="itinerary" >}}

Volker Krause unified the formatting of temperature ranges and dynamic depending on the home country. Similarly, imperial speed units are shown for countries that use them (Volker Krause, 25.04.0. [Link 1](https://invent.kde.org/pim/itinerary/-/merge_requests/387) and [link 2](https://invent.kde.org/pim/itinerary/-/commit/21f331f03ec49fd8f05483ccd316888e587327d3)).

Again Itinerary has increased the number of ticket types it supports and now handles [multi-page 12go PDF tickets](https://invent.kde.org/pim/kitinerary/-/commit/855ab75b99861c9f7f18c5d455d9da584d4c42c8) and [Ghotel reservation emails](https://invent.kde.org/pim/kitinerary/-/commit/f8a1522bb331e45faf48528eb31ef0b56ecf2f55).

## Social Apps

{{< app-title appid="neochat" >}}

Joshua Goins moved the "Explore rooms" button from the hamburger to the space drawer (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2171)), added a dialog explaining what to do next when tapping "Verify this device" (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2172)), and made joining remote rooms more reliable (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2173)). Joshua also fixed a bug where emoji autocompletion would destroy the current message draft (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2174)).

Meanwhile, James Graham improved the handling of switching link previews on and off (James Graham, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2167)).

## Developer Apps

{{< app-title appid="kate" >}}

Niels Thykier added built-in support for the `debputy` language server. This is used when writing Debian package (Niels Thykier, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1748)).

Meanwhile, Joshua Goins improved the titles of terminal tabs and assigning an icon to them (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1745)). Joshua also improved the UI of the compiler explorer integration. This includes polishing some strings, adding tooltips and fixing some padding issues (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1744)).

![](kate-compiler.png)

{{< app-title appid="kdevelop" >}}

KDevelop now support the Language Server Protocol (LSP) in addition to the native support for C++, PHP and Python. This reuses Kate's plugin, so, at the moment, it is only available when Kate is also installed (Igor Kushnir and Sven Brauch, 25.04.0. [Link](https://invent.kde.org/kdevelop/kdevelop/-/merge_requests/730)).

{{< app-title appid="konsole" >}}

Jonathan Marten fixed a crash when double clicking on a terminal scroll bar (Jonathan Marten, 25.04.0. [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1081)).

## System Apps

{{< app-title appid="ark" >}}

<!-- actually not merged
Added support for reading password-protected p7zip archives. This affects not only Ark but all applications using KArchive. (Azhar Momin, KDE Frameworks 6.13.0. [Link](https://invent.kde.org/frameworks/karchive/-/merge_requests/104))
-->

Natsumi Higa fixed the extraction of timestamps from 7-Zip archives, which now includes nanoseconds (Natsumi Higa, 25.04.0. [Link](https://invent.kde.org/utilities/ark/-/merge_requests/270)).

{{< app-title appid="dolphin" >}}

Dolphin is having its looks tweaked and has a new icon with an actual picture of a dolphin inside it! (Darshan Phaldesai, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/901)).

![](org.kde.dolphin.svg)

In the same vein, Akseli Lahtinen added a background to the navigation bar of Dolphin and Gwenview (Akseli Lahtinen, KF 6.12.0. [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1793)).

![](dolphin-navbvar.png)

And Nate Graham added a nicer split icon to the toolbar (Nate Graham, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/932)).

![](dolphin-split.png)

In other Dolphin news, Akseli Lahtinen fixed a crash when opening a new tab with search (Akseli Lahtinen, 24.12.3. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/923)).


{{< app-title appid="systemdgenie" >}}

systemDGenie was completely rewritten using QML. The new version also relies a lot less on blocking DBus calls (Carl Schwan, 1.0.0. [Link](https://invent.kde.org/system/systemdgenie/-/merge_requests/15)).

![](systemdgenie-system.png)
![](systemdgenie-user-details.png)
![](systemdgenie-timers.png)

{{< app-title appid="kwalletmanager5" >}}

Xuetian Weng sorted a security issue and passwords copied from the KWallet Manager are no longer visible in the clipboard history of Plasma (Xuetian Weng, 25.04.0. [Link](https://invent.kde.org/utilities/kwalletmanager/-/merge_requests/46)).

## Education Apps

{{< app-title appid="kiten" >}}

Balló György fixed the font size of the result view. The font size was stored as point size, but passed as pixel size, causing that the actual font size is smaller than it should be (Balló György, 25.04.0. [Link](https://invent.kde.org/education/kiten/-/merge_requests/66)). Balló also fixed the background color of some views when switching to a dark theme (Balló György, 25.04.0. [Link](https://invent.kde.org/education/kiten/-/merge_requests/65)).

{{< app-title appid="khangman" >}}

Max Brazhnikov Added support for non-latin alphabets (Max Brazhnikov, 25.04.0. [Link](https://invent.kde.org/education/khangman/-/merge_requests/41)).

{{< app-title appid="kstars" >}}

Hy Murveit added an altitude graph to the scheduler table (Hy Murveit, [Link](https://invent.kde.org/education/kstars/-/merge_requests/1441))

![](SchedulerGraph.png)

## Utilities

{{< app-title appid="alligator" >}}

Mark Penner made the text elide in the RSS entry list so that the buttons are always visible (Mark Penner, 25.04.0. [Link](https://invent.kde.org/network/alligator/-/merge_requests/132)), and Balló György set the default format to import and export feeds as OPML (Balló György, 25.04.0. [Link](https://invent.kde.org/network/alligator/-/merge_requests/136)).

{{< app-title appid="kdeconnect" >}}

José Rebelo added the possibility of filtering out notifications from the Android work profile (José Rebelo, [Link](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/528)).

{{< app-title appid="kdf" >}}

Kai Uwe Broulik added an option to explore in Filelight (Kai Uwe Broulik, 25.12.0. [Link](https://invent.kde.org/utilities/kdf/-/merge_requests/31)), and icons to the context menu entries (Kai Uwe Broulik, 25.12.0. [Link](https://invent.kde.org/utilities/kdf/-/merge_requests/30)).

![](kdf-contextmenu.png)

{{< app-title appid="kget" >}}

Balló György fixed the windows activation when the current window is in the system tray (Balló György, 25.04.0. [Link](https://invent.kde.org/network/kget/-/merge_requests/92)).

{{< app-title appid="krdc" >}}

Fabio Bas added a setting for desktop scale and device scale (Fabio Bas, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/170)), while Fabian Lesniakd disabled Kerberos support completely, since it turned out that having a broken support for it was worse than no support at all (Fabian Lesniak, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/163)).

{{< app-title appid="optiimage" >}}

Balló György fixed the name of the generated optimized images, and now the suffix is appended before the file extension (Balló György, [Link](https://invent.kde.org/graphics/optiimage/-/merge_requests/21)).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/03/08/this-week-in-plasma-a-very-fixy-week/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
