---
title: This Week in KDE Apps
subtitle: New KCron Settings UI, Krita 2.2.5 released, and more
discourse: thisweekinkdeapps
date: "2024-09-29T14:30:35Z"
authors:
 - nategraham
 - carlschwan
image: thumbnail.png
categories:
 - This Week in KDE Apps

itinerary:
- name: Itinerary search
  url: itinerary-places.png
- name: Itinerary Section delegate
  url: itinerary-section-delegate.png

kcron:
- name: KCron Global View
  url: kcron-global.png
- name: KCron Task Editor
  url: kcron.png
---

![](thumbnail.png)

Welcome to the third post in our “This Week in KDE Apps” series! If you missed
it,
we just announced this new series [two weeks ago](https://blogs.kde.org/categories/this-week-in-kde-apps/), and our goal is
to cover as much as possible of what's happening in the KDE world and complete
[Nate's This Week in Plasma](https://pointieststick.com/2024/09/27/this-week-in-plasma-converging-6-2/).

This week we had new releases of Amarok and Krita. There is also news
regarding KDE Connect, the link between all your devices; Kate, the KDE
advanced text editor; Itinerary, the travel assistant that lets you plan all
your trips;  Marble, KDE's map application; and more.

Let's get started!

## Amarok

Amarok 3.1.1 was released. 3.1.1 features a number of small improvements and
bug fixes, including miscellaneous fixes for toolbars and the return of tag
dialog auto-completions — functionality that initially got lost during the
Qt5/KF5 port. However, most of the work has again happened under the hood to
improve the codebase's Qt6/KF6 compatibility.

[See the full announcement for more information](https://blogs.kde.org/2024/09/29/amarok-3.1.1-released/)

## Itinerary

Itinerary now supports search for places (e.g. street names) in addition to
stops. (Code: Jonah Brüchert, Icon: Mathis Brüchert, 24.12.0.
[Link](https://invent.kde.org/pim/itinerary/-/merge_requests/326))

Itinerary now shows the date of the connection when searching for a public
transport connection. (Jonah Brüchert, 24.12.0.
[Link 1](https://invent.kde.org/pim/itinerary/-/merge_requests/332),
[link 2](https://invent.kde.org/libraries/kpublictransport/-/merge_requests/84))

{{< screenshots name="itinerary" >}}

## Digikam

A new face detection algorithm based on
[YuNet](https://link.springer.com/article/10.1007/s11633-023-1423-y) is now
available. (Michael Miller,
[Link](https://invent.kde.org/graphics/digikam/-/merge_requests/289))

## Kate

The debug plugin now works on Windows! (Waqar Ahmed, 24.12.0.
[Link](https://invent.kde.org/utilities/kate/-/merge_requests/1591))

The debug plugin is now much more usable. (Waqar Ahmed, 24.12.0. [Link
1](https://invent.kde.org/utilities/kate/-/merge_requests/1590), [link
2](https://invent.kde.org/utilities/kate/-/merge_requests/1599), [link
3](https://invent.kde.org/utilities/kate/-/merge_requests/1594))

Kate context menu will now show relevant external tools. (Waqar Ahmed, 24.12.0.
[Link](https://invent.kde.org/utilities/kate/-/merge_requests/1602))

## KCron

The System Settings page was ported to QML and given a fancy new UI! (Evgeny
Chesnokov, 24.12.0.
[Link](https://invent.kde.org/system/kcron/-/merge_requests/24))

{{< screenshots name="kcron" >}}

## KDE Connect

Fixed the Bluetooth support for KDE Connect. (Rob Emery, 24.12.0.
[Link 1](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/600),
[link 2](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/717))

## Keysmith

Keysmith now has an "About" page. (Plata Hill, 24.12.0.
[Link](https://invent.kde.org/utilities/keysmith/-/merge_requests/135))

## Kleopatra

Kleopatra now supports OpenPGP v5 keys. (Ingo Klöcker, 24.12.0.
[Link 1](https://invent.kde.org/pim/libkleo/-/merge_requests/142), [link
2](https://invent.kde.org/pim/libkleo/-/merge_requests/141))

## Krita

Krita 5.2.5 was released and is bringing over 50 bugfixes since 5.2.3 (5.2.4
was a Windows-specific hotfix). Major fixes have been done to audio playback,
transform mask calculation and more! [Read
more.](https://krita.org/en/posts/2024/krita-5-2-5-released/)

## LabPlot

LabPlot implements a new type of plot: [Process Behavior Chart
(X-Chart)](https://invent.kde.org/education/labplot/-/issues/629), (Alexander
Semke, [Link](https://invent.kde.org/education/labplot/-/merge_requests/571))

## Marble

Marble Maps, the QML version of Marble, has a new icon. (Mathis Brüchert,
24.12.0.
[Link](https://invent.kde.org/education/marble/-/commit/63e29e70e05c25890c1628b66403c94b8f6dc101))

![Marble Maps icon](https://apps.kde.org/app-icons/org.kde.marble.maps.svg)
{width="128" height="128"}

Fixed a major source of visual glitches in the QML version of Marble when
looking at the Earth globe. (Carl Schwan, 24.08.2.
[Link](https://invent.kde.org/education/marble/-/commit/a73d2882afd10cf89618fdda6b68c95644fcd532))

Marble Behaim — a special version of Marble to look at the oldest globe
representation of the Earth known to exist — now also works on desktop thanks
to Kirigami, and all the additional information and credits are now displayed
using a standard "About" page. (Carl Schwan, 24.12.0.
[Link](https://invent.kde.org/education/marble/-/commit/30a18c2766aa2c4ee01f7d7b2600056373ed6469))

![Marble Behaim new design and about page](behaim.png)

Marble's KRunner integration, Plasma Widget and the Wallpaper plugin are now
fully ported to Plasma 6. (Carl Schwan, 24.12.0.
[Link](https://invent.kde.org/education/marble/-/commit/5c3810c205bc127495ee13aa0986e9b4122cdee4))

![Marble Wallpaper and Plasma Widget](wallpaper.png)

## NeoChat

On modern versions of Android, NeoChat will now request the correct permission
to send system notifications. (James Graham, 24.12.0.
[Link](https://invent.kde.org/network/neochat/-/merge_requests/1919))

## Spectacle

Spectacle now respects your custom save file format as expected when using the
"Save As" functionality. (Noah Davis, 24.08.2.
[Link](https://bugs.kde.org/show_bug.cgi?id=493370))

## Others

Valentyn Bondarenko updated several screenshots of KDE apps:

- [Partion Manager](https://invent.kde.org/system/partitionmanager/-/merge_requests/48)
- [Calligra Plan & Konversation](https://invent.kde.org/websites/product-screenshots/-/merge_requests/72)
- [Kid3](https://invent.kde.org/websites/product-screenshots/-/merge_requests/71)
- [KMag](https://invent.kde.org/accessibility/kmag/-/merge_requests/14)
- [System Monitor](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/314)
- [KCharSelect & KColorChooser](https://invent.kde.org/websites/product-screenshots/-/merge_requests/68)
- [Kaffeine, KFind, KMag, KMouseTool, Konqueror, Marble & Yakuake](https://invent.kde.org/websites/product-screenshots/-/merge_requests/69)

Eamonn Rea made more Kirigami applications remember their size across launches:

- [KRecorder](https://invent.kde.org/utilities/krecorder/-/merge_requests/39)
- [KWeather](https://invent.kde.org/utilities/kweather/-/merge_requests/119)

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check
out [Nate's blog about Plasma](https://pointieststick.com) and [KDE's
Planet](https://planet.kde.org), where you can find more news from other KDE
contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped achieve that status. As we grow, it’s going to be
equally important that your support become sustainable.

We need you for this to happen. You can help KDE by becoming an active
community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE; you are not a number or a cog
in a machine! You don’t have to be a programmer, either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general help KDE continue bringing Free
Software to the world.
