---
title: This Week in KDE Apps
subtitle: Accessibility enhancements, new releases and performance boosts
discourse: thisweekinkdeapps
date: "2024-10-13T14:30:35Z"
authors:
 - carlschwan
image: thumbnail.png
categories:
 - This Week in KDE Apps
newsletter: 6cc7d914-24b5-4363-bd6a-cd70a3af79aa
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week we enhanced the accessibility of a bunch of our most popular apps; released new versions of KleverNotes, KPhotoAlbum; and improved the performance and usability of KDE Connect, Kate, Konqueror, and more.

Let's get started!

## Accessibility

We made the inline notifications that appear above the view in Dolphin, Gwenview, Okular, and many other applications fully accessible by keyboard and to screen readers. Applications will need to opt-in to actually have them announced
though. (Felix Ernst, KDE Frameworks 6.8, [Link](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/270), Dolphin 24.12.0, [Link](https://invent.kde.org/system/dolphin/-/merge_requests/836))

{{< app-title appid="dolphin" >}}

After renaming a file, the renamed file is immediately selected. (Jin Liu, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/839))

{{< app-title appid="merkuro.contact" >}}

The OpenPGP and S/MIME certificates of a contact are now displayed directly in Merkuro Contact. Clicking on them will open Kleopatra and show additional information. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/458))

![](merkuro-contact-crypto.png)

{{< app-title appid="kaddressbook" >}}

Fix a crash when editing a contact with a nonstandard phone type. (Jonathan Marten, 24.08.3. [Link](https://invent.kde.org/pim/akonadi-contacts/-/merge_requests/48))

{{< app-title appid="kate" >}}

We managed to reduce KDE's advanced text editor startup time by 250ms (Waqar Ahmed, 24.12.0. [Link 1](https://invent.kde.org/utilities/kate/-/merge_requests/1613), [link 2](https://invent.kde.org/utilities/kate/-/merge_requests/1611))

The Quick Open tool can now be used to search and browse the projects open in the current session (Akseli Lahtinen, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1615))

{{< app-title appid="kdeconnect" >}}

The list of devices you can connect to now shows the connected and remembered devices separately. (Albert Astals Cid, 24.12.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/724))

![](kdeconnect.png)

{{< app-title appid="kcron" >}}

Improve the clarity of the "Print Summary…" button. (Thomas Duckworth, 24.12.0. [Link](https://invent.kde.org/system/kcron/-/merge_requests/32))

The printed output is now translated. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/system/kcron/-/merge_requests/30))

{{< app-title appid="klevernotes" >}}

KleverNotes 1.1.0 is out! KDE's note-taking app has a faster Markdown parser, a better toolbar and a WYSIWYG-like editor. [Read the full announcement!](https://lemmy.kde.social/post/2107543).

![](klevernotes.webp)

{{< app-title appid="kmail2" >}}

Fix the dates in the message lists being stuck at 'Today' and 'Yesterday' even after the day has
changed. (Christoph Erhardt, 24.08.3. [Link](https://invent.kde.org/pim/messagelib/-/merge_requests/246))

{{< app-title appid="konqueror" >}}

Our venerable file explorer/web browser comes with improved auto-filling of login information. (Stefano Crocco, 24.12.0. [Link](https://invent.kde.org/network/konqueror/-/merge_requests/365))

{{< app-title appid="kphotoalbum" >}}

KPhotoAlbum 5.13.0 is out. This is a small update that fixes numerous bugs and reworks timespan calculation.
[Read the full announcement](https://www.kphotoalbum.org//2024/10/09/kphotoalbum-5.13.0-released/)

{{< app-title appid="kstars" >}}

KStars was ported to Qt6/KF6. (Jasem Mutlaq. [Link](https://invent.kde.org/education/kstars/-/merge_requests/1333))

{{< app-title appid="ktorrent" >}}

It is now possible to specify an https URL as webseeds in the "Create a Torrent" dialog. (Jack Hill, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=492399))

{{< app-title appid="neochat" >}}

KDE's homegrown Matrix instant messaging chat client comes with a redesigned general room settings dialog. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1906))

![](neochat-room-settings.png)

{{< app-title appid="tokodon" >}}

We have added an "Open Server in Browser" button in the profile editor. This lets you configure some settings not exposed via the API that Tokodon uses. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/550))

Tokodon now clarifies that a user's notes are private. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/546))

![](tokodon-profile.png)

Improve the names and descriptions of various profile options. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/542))

![](tokodon-profile-actions.png)

Tokodon now lets you manage your followers and following users. Which means, it's now possible to forcibly remove users from your followers list. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/544))

![](tokodon-manage-followers.png)

Added a new "Following" feed, to quickly page through your follows and their feed similar to the now discontinued [Cohost](https://cohost.org/rc/welcome) social network.

![](tokodon-following.png)

## Platforms

### Android

Volker Krause posted a summary of all the improvements made to the Android platform on Android in October. This includes the retirement of Qt 5 Android CI, better translations lookup, dark mode support and more. [Read the full blog post](https://www.volkerkrause.eu/2024/10/12/kde-android-news-oct-2024.html)

### SailfishOS

Thanks to Adam Pigg and rinigus, Qt6 and KF6 are now available on SailfishOS. This means Kirigami applications built with Qt6 can now be packaged on that platform. [Read the announcement](https://forum.sailfishos.org/t/qt6-available-for-developer-preview/20300/)

## ...And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](https://pointieststick.com) and be sure not to miss his [This Week in Plasma](https://pointieststick.com/2024/10/11/this-week-in-plasma-6-2-has-been-released/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete view of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
