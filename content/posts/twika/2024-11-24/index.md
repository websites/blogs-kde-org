---
title: 'This Week in KDE Apps: Bugfixing Week'
discourse: thisweekinkdeapps
date: "2024-11-25T11:20:35Z"
authors:
 - carlschwan
 - nategraham
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@kde@floss.social'
---
![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week, we are continuing to prepare for the KDE Gear 24.12.0 release, with a focus on bugfixing now that we've entered the feature freeze period.

Meanwhile, and as part of the [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/), you can "Adopt an App" in a symbolic effort to support your favorite KDE app. This week, we are particularly grateful to [mdPlusPlus](https://github.com/mdPlusPlus) and txemaq for supporting Dolphin; [mdPlusPlus](https://github.com/mdPlusPlus), Greg Helding and Archie Lamb for Okular; Henning Lammert and Thibault Molleman for Filelight; Nithanim, [Dominik Perfler](https://github.com/DomiStyle), and Thibault Molleman for Spectacle; Vladimir Solomatin, Akseli Lahtinen, Haakon Johannes Tjelta Meihack, and Nithanim for Kate; Henning Lammert and Marco Ryll for Kasts; GhulDev, Anders Lund Tobias Junghans, and William Wojciechowski for Konsole; Piwix for KWrite; [Gabriel Klavans](https://freeradical.zone/@dabe) for Tokodon; Matthew Lamont for Kontact; and Gabriel Karlsson for Itinerary.

Any monetary contribution, however small, will help us cover operational costs, salaries, travel expenses for contributors and in general just keep KDE bringing Free Software to the world. So consider [donating](https://kde.org/fundraisers/yearend2024/) today!

Getting back to all that's new in the KDE App scene, let's dig in!


## Global Changes

The KIO implementation for SFTP used by many KDE applications like Dolphin, Gwenview, and many others, now correctly closes the network connection when if a fatal error occurs, which means it is now possible to reconnect immediately without having to wait a few minutes. (Harald Sitter, 24.12.0. [Link](https://invent.kde.org/network/kio-extras/-/merge_requests/379))

Many apps received some small bug fixes to ensure they work correctly on Haiku OS. (Luc Schrijvers, [Link 1](https://invent.kde.org/network/kio-extras/-/merge_requests/386), [link 2](https://invent.kde.org/utilities/francis/-/merge_requests/22) and many more)

{{< app-title appid="dolphin" >}}

Dolphin now sorts more naturally when comparing filenames by excluding extensions. So now "a.txt" appears before "a 2.txt". (Eren Karakas, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/858))

{{< app-title appid="haruna" >}}

Left-clicking on a video now plays or pauses it by default. (Nate Graham, 25.04.0. [Link](https://invent.kde.org/multimedia/haruna/-/merge_requests/49))

{{< app-title appid="karp" >}}

Karp is a new PDF arranger and editor by Tomasz Bojczuk which has just finished the [incubation phase](https://develop.kde.org/docs/getting-started/add-project/#incubating-phase).

Tomasz was active this week and added an option to select the PDF version of the resulting PDF ([Link](https://invent.kde.org/graphics/karp/-/merge_requests/9)) and made it possible to move multiple pages at the same time ([Link](https://invent.kde.org/graphics/karp/-/merge_requests/4)).

{{< app-title appid="kate" >}}

The build plugin — which allows you to trigger a rebuild from Kate's interface — now supports multiple projects being open at the same time without having to constantly reload the list of targets every time you switch projects. (Waqar Ahmed, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1653))

The ctag indexing doesn't happen anymore on the root and home folder as it makes no sense and just wastes CPU cycles. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1654))

Fix getting a `PATH` when launching Kate outside of the console on macOS. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1621))

{{< app-title appid="kmymoney" >}}

It is no longer possible to apply category filters on the "Net Worth report" of KMyMoney as this was resulting in erroneous results. (Thomas Baumgart, 5.2, [Link](https://invent.kde.org/office/kmymoney/-/merge_requests/241))

{{< app-title appid="krdc" >}}

Fix loading the gateway server address in the settings. (Fabio Bas, 24.12.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/128))

Fix a crash when scrolling; the app was previously sending empty scroll events which were rejected by the remote server. (Fabio Bas, 24.12.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/126))

{{< app-title appid="konqueror" >}}

Stefano fixed various issues with the Plasma Activities integration inside Konqueror. We now, for example, wait for the Plasma Activity service to be ready, before restoring activities when starting Konqueror. (Stefano Crocco, 24.12.0. [Link](https://invent.kde.org/network/konqueror/-/merge_requests/377))

{{< app-title appid="konsole" >}}

We added the Campbell color scheme from Microsoft. (Mingcong Bai, 25.04.0. [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1046))

![Konsole with the Campbell color scheme](konsole-camptell.png)

We fixed a few font rendering issues in Konsole. (Matan Ziv-Av, 24.12.0. [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1044))

{{< app-title appid="okular" >}}

Changed the default value of the "scroll overlap" feature from 0% to 10%, which means that when you scroll down in a document using <kbd>Page Down</kbd> or <kbd>Space Bar</kbd>, the bottom 10% of the previous page will remain visible at the top of the view. This helps you retain your spatial awareness when quickly navigating. (David Cerenius, 25.04.0. [Link](https://bugs.kde.org/show_bug.cgi?id=476759))

{{< app-title appid="merkuro.calendar" >}}

Claudio fixed many issues with the day and month views. Now, clicking on a day in the month view will open the day view on the selected day and not just a random one, the current day will be correctly highlighted, some sizing issues are fixed, and the month view won't appear as disabled anymore in some situations. (Claudio Cambra, 24.12.0. [Link 1](https://invent.kde.org/pim/merkuro/-/merge_requests/486), [link 2](https://invent.kde.org/pim/merkuro/-/merge_requests/488) and [link 3](https://invent.kde.org/pim/merkuro/-/merge_requests/484))

When double-clicking on an empty space in the month view, the incidence editor will use the selected date as its start date. (Claudio Cambra, 24.12.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/483))

{{< app-title appid="neochat" >}}

We fixed the `sed`-edit feature in NeoChat, which allows you to type a `sed` expression like `s/foo/bar` to edit your previous message. (James Graham, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2009))

On mobile devices, NeoChat won't open the space homepage when trying to just switch the selected space. (James Graham, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2024))

Implemented [MSC4228: Search Redirection](https://github.com/matrix-org/matrix-spec-proposals/pull/4228/files) to harmlessly redirect searches for harmful and potentially illegal content.

{{< app-title appid="optiimage" >}}

It is now possible to remove an image from the list of images to optimize. (Soumyadeep Ghosh, [Link](https://invent.kde.org/graphics/optiimage/-/merge_requests/8))

Soumyadeep also fixed an issue where it was possible to add the same image multiple times (Soumyadeep Ghosh, [Link](https://invent.kde.org/graphics/optiimage/-/merge_requests/9))

{{< app-title appid="skanpage" >}}

Ported the export dialog in Skanpage to use `Kirigami.Dialog`, giving it a nicer and more consistent appearance. (Thomas Duckworth, 25.04.0. [Link](https://invent.kde.org/utilities/skanpage/-/merge_requests/78))

![](skanpage.png)

Ported Skanpage to use KIO, which allows saving scanned documents to remote folders. (Alexander Stippich, 25.04.0. [Link](https://invent.kde.org/utilities/skanpage/-/merge_requests/86))

{{< app-title appid="telly-skout" >}}

Fix the list of favorite TV channels being empty after opening a different page. (Plata Hill, 24.12.0. [Link](https://invent.kde.org/utilities/telly-skout/-/merge_requests/173))

{{< app-title appid="tokodon" >}}

Joshua added titles to the profile pages, so that it is not empty anymore. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/579/))

Quote post are now better detected. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/578))

It is now possible to start a new chat from the conversation page. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/575/))


## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/11/23/this-week-in-plasma-battery-charge-cycles-in-info-center/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.


## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
