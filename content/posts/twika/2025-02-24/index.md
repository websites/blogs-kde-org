---
title: 'This Week in KDE Apps'
subtitle: 'Smaller statusbar in Dolphin, CSS Font Variables in Krita, and SystemdGenie redesign'
discourse: thisweekinkdeapps
date: "2025-02-25T08:18:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/). This time again a bit delayed. If you are a non technical person and are looking at a way to contribute to KDE, you can help editing "This Week in KDE Apps" would be very much welcome. Just join our [Matrix chat](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).

This week we have some big changes in Krita, a redesign in SystemDGenie and a new, more compact statusbar for Dolphin.

{{< app-title appid="amarok" >}}

We dropped the Qt5 support and moved to Qt6 (Tuomas Nurmi, [Link](https://invent.kde.org/multimedia/amarok/-/merge_requests/142)).

{{< app-title appid="dolphin" >}}

Dolphin now uses a more compact statusbar by default (Akseli Lahtinen, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/903)).

![](dolphin.png)

When in selection mode, Dolphin now has a special keyboard navigation mode. You can read all about this feature in detail in the merge request description (Felix Ernst, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/871)).

{{< app-title appid="kasts" >}}

We fixed various usability issues and recent regressions (Bart De Vries, 25.04.0. [Link 1](https://invent.kde.org/multimedia/kasts/-/merge_requests/258), [link 2](https://invent.kde.org/multimedia/kasts/-/merge_requests/260), [link 3](https://invent.kde.org/multimedia/kasts/-/merge_requests/254), ...).

{{< app-title appid="kate" >}}

We improved support for DAP (the generic protocol for debuggers) (Waqar Ahmed, 25.04.0 [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1724)), and sped up KWrite's startup time by not loading a MIME database when just querying the icon for `text/plain` file. (Kai Uwe Broulik, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1725))

{{< app-title appid="kleopatra" >}}

We removed the _Decrypt/Verify all files in folder_ menu item in the Dolphin context menu as it was never implemented (Tobias Fella, 25.04.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/369)).

{{< app-title appid="konqueror" >}}

The _Save As_ dialog now remembers where a file was last downloaded and will open that directory. Note that the last location is only remembered for the duration of the Konqueror window (Stefano Crocco, 25.04.0. [Link](https://invent.kde.org/network/konqueror/-/merge_requests/387)).

{{< app-title appid="krita" >}}

We added a glyph palette to choose between alternates or variation of a given glyph, as well as a character map of a given font (Wolthera van Hövell, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2080)).

{{< video src-webm="krita.mp4" >}}

And implemented the edition of the [CSS Font Variants](https://drafts.csswg.org/css-fonts-3/#font-rend-props) in the text properties docker (Wolthera van Hövell, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2325)). 

![](krita-smooth.png)

Krita now compiles with Qt6 on Windows (Dmitry Kazakov, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2328)).

We added a new extension "Mutator". This new extension provides a docker which adds brush variations through action-invoked settings randomization (Emmet O'Neill, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2322)). We also added global pen tilt direction offset which is helpful to make brushes feel the same for right- and left-handed users (Maciej Jesionowski. [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2337)). Another brush related improvement is that their smoothness is now also affected by the speed (killy |0veufOrever, [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2192)).

{{< app-title appid="kup" >}}

We improved the link text in the KCM user interface (Robert Kratky - first contribution 🚀, 0.11.0. [Link](https://invent.kde.org/system/kup/-/merge_requests/39)).

{{< app-title appid="neochat" >}}

Long pressing has been disabled on non-touchscreen devices (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2154/)), and we improved the usability of the account menu by giving it a proper button (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2157)).

{{< app-title appid="okular" >}}

We have improved the error handling entering a bad passphrase for a digital signature (Sune Vuorela, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1131)) and made the overprint preview setting a combobox that gives you the option to choose between "Always", "never" and "Automatic", which is similar to Acrobat Reader. The "Automatic" value depends on the value of `HasVisibleOverprint` in the PDF metadata (Kevin Ottens, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1137)).

## SystemDGenie

SystemDGenie was ported to a more "frameless" interface and the statusbar was removed (Thomas Duckworth. [Link 1](https://invent.kde.org/system/systemdgenie/-/merge_requests/6) and [link 2](https://invent.kde.org/system/systemdgenie/-/merge_requests/14)).

![](systemdgenie.png)

SystemDGenie shows unloaded and inactive units by default (Thomas Duckworth. [Link](https://invent.kde.org/system/systemdgenie/-/merge_requests/11)) and the startup time was sped up by fetching the list of sessions and units asynchronously (Carl Schwan. [Link](https://invent.kde.org/system/systemdgenie/-/merge_requests/13)).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/25/this-week-in-plasma-fancy-time-zone-picker/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
