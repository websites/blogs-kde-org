---
title: 'This Week in KDE Apps: Gear 24.12.0 incoming'
discourse: thisweekinkdeapps
date: "2024-12-08T12:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
itinerary:
 - name: The timeline view
   url: itinerary-timeline.png
 - name: The query result
   url: itinerary-query-result.png
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week, we are adding the final touches to our applications for the KDE Gear 24.12.0 release coming next Thursday. We are also releasing KPhotoAlbum and KGeoTag, now based on Qt6; improving Itinerary's ticket extractor support coverage in central Europe; and continuing our work on Karp, KDE's new PDF editor.

Meanwhile, as part of the [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/), you can "Adopt an App" in a symbolic effort to support your favorite KDE app. This week, we are particularly grateful to
Stuart Turton for NeoChat;
Lukas, Stuart Turton and J. for Merkuro;
Andreas Pietzowski, Dia_FIX and Alex Gurenko for Ark;
Stuart Turton and [Cameron Bosch](https://fosstodon.org/@cameronbosch) for Tokodon;
Alex Gurenko and Steven Dunbar for Gwenview;
Alex Gurenko, Kasimir den Hertog and Pokipan for KWrite;
crysknife, Ian Kidd and [Felix Urbasik](https://ma.fellr.net/@fell) for KRDC;
Ian Nicholson for Alligator;
Cameron Radmore for ISO Image Writer;
Marcel Janik Kimpel and [@siriusfox@social.treehouse.systems](https://social.treehouse.systems/@siriusfox) for KDE Partition Manager;
Marton Daniel for Plasma System Monitor;
Alessio Adamo for AudioTube;
zukigay for Kasts;
Anael for Elisa;
Stuart Turton and [Clément Aubert](https://lipn.info/@clementaubert) for Konqueror;
Ulrich Palecek, [@ddjivan.bsky.social](https://bsky.app/profile/ddjivan.bsky.social) and Andreas Zautner for Discover;
Butters for KolourPaint;
KjetilS for krfb;
and finally fabacam, Michael Klingberg and [Gianmarco Gargiulo](https://mastodon.uno/@gianmarcogg03) for GCompris.

Getting back to all that's new in the KDE App scene, let's dig in!

{{< app-title appid="akonadi" >}}

When updating, adding, or removing a tag/category to a calendar event, the update is immediately visible without having to sync again with a remote server (Daniel Vrátil, 24.12.0 — [Link](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/190)).

{{< app-title appid="alligator" >}}

The "Refresh" action is now also visible on mobile (Mark Penner, 24.12.0 — [Link](https://invent.kde.org/network/alligator/-/merge_requests/122)).

{{< app-title appid="amarok" >}}

A beta release of the upcoming Amarok 3.2 music player is out for testing — [see the announcement email](https://mail.kde.org/pipermail/amarok-devel/2024-December/014769.html).

Amarok devs fixed Ampache version check. Ampache is self-hostable music streamer service server and the version check was broken since Ampache changed their version format, but it works again now (Ian Abbott, Amarok 3.2.0 — [Link](https://invent.kde.org/multimedia/amarok/-/merge_requests/130)).

You can also filter a collection by tracks that have tags missing or when tags are empty (Tuomas Nurmi, Amarok 3.2.0 — [Link](https://invent.kde.org/multimedia/amarok/-/merge_requests/131) — an 11 year old feature request!).

{{< app-title appid="arianna" >}}

Arianna now uses [foliate-js](https://github.com/johnfactotum/foliate-js) instead of epub.js to render EPUB files. foliate-js provides some advantages like no longer requiring to load the whole book into memory, and comes with a better layout engine (Ajay Chauhan, 25.04.0 — [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/48)).

![](arianna.png)

{{< app-title appid="audiotube" >}}

AudioTube now displays album information in the maximized player view (Kavinu Nethsara, 25.04.0 — [Link](https://invent.kde.org/multimedia/audiotube/-/merge_requests/147)).

{{< app-title appid="dolphin" >}}

Accessibility support in Dolphin was adapted to better work with Orca 47 (Felix Ernst, 25.04.0 — [Link](https://invent.kde.org/system/dolphin/-/merge_requests/850)), and, continuing with accessibility improvements, after activating a folder in the Dolphin sidebar, the view is now always focused (Felix Ernst, 25.04.0 — [Link](https://invent.kde.org/system/dolphin/-/merge_requests/868)). Likewise, when clicking on "Open Path" and "Open Path in New Tab" after searching for an item, the view will scroll to the selected item (Akseli Lahtinen, 25.04.0 — [Link](https://invent.kde.org/system/dolphin/-/merge_requests/857)).

The placeholder message when Samba isn't and can't be installed was improved (Ilya Katsnelson, 25.04.0. and partially backported to 24.12.0 — [Link](https://invent.kde.org/network/kdenetwork-filesharing/-/merge_requests/56)), and the Flatpak version now allows compressing files into an archive (Justin Zobel, 25.04.0 — [Link](https://invent.kde.org/system/dolphin/-/commit/18ced02b2ec74b92918fc4fce42b5bba5e6b8a20)).

{{< app-title appid="elisa" >}}

When removing the last track associated with an artist or a music genre, the artist or genre is now removed from the internal database (Jack Hill, 25.04.0 — [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/642)).

{{< app-title appid="gwenview" >}}

We fixed the incorrect numbering in full screen mode for the first image (Pedro Hernandez, 25.04.0 — [Link](https://bugs.kde.org/show_bug.cgi?id=406105)).

{{< app-title appid="itinerary" >}}

Volker wrote a recap for the past two months in Itinerary and can read it on [his blog](https://volkerkrause.eu/2024/11/30/kde-itinerary-october-november-2024.html). The post includes a report on work unrelated to Itinerary development, but nevertheless important, like the lobbying of [DELFI](https://www.delfi.de/), a cooperation network of all German federal states for public transport.

The "Vehicle Layout" page and the "Journey Details" page were slightly tweaked and use the new unified component to display the name of the train or bus (Carl Schwan, 25.04.0 — [Link 1](https://invent.kde.org/pim/itinerary/-/merge_requests/354) and [link 2](https://invent.kde.org/pim/itinerary/-/merge_requests/358)).

![](itinerary-vehicle-layout.png)

We also made significant progress on Itinerary's extractors this week, with many new extractors, including:

- The Colosseum Ticket in Rome (David Pilarcik, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/136))
- The Polish online ticket sale system [Droplabs](https://droplabs.pl/) (David Pilarcik, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/137))
- The train booking platform [Leo Express](https://www.leoexpress.com/en) (David Pilarcik, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/138))
- The German trade fair, congress, and event ticket sale system, [Dimedis Fairmate](https://www.fairmate.de/) (Kai Uwe Broulik, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/141))
- Google Maps links (Kai Uwe Broulik, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/142))
- The European Sleeper seat reservations in French (Luca Weiss, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/140))

{{< app-title appid="kaidan" >}}

Kaidan will now display a map preview by default when receiving a geo location (Melvin Keskin — [Link](https://invent.kde.org/network/kaidan/-/merge_requests/1190)).

{{< app-title appid="karp" >}}

Karp, KDE's new PDF editor, received visual improvements to its main interface (Carl Schwan — [Link 1](https://invent.kde.org/graphics/karp/-/merge_requests/13), [link 2](https://invent.kde.org/graphics/karp/-/merge_requests/12) and [link 3](https://invent.kde.org/graphics/karp/-/merge_requests/14)).

![](karp.png)

![](karp-files.png)

And Nicolas setup crash reporting for Karp (Nicolas Fella — [Link](https://invent.kde.org/graphics/karp/-/merge_requests/11)).

{{< app-title appid="kdeconnect" >}}

We moved the list of devices to the sidebar in an effort to bring the app to parity with the KCM (Darshan Phaldesai, 25.04.0 — [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/755)).

![](kdeconnect.png)

We also added icons to the plugin config list (Leia uwu, 25.04.0 — [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/758)).

![](kdeconnect-pluginlist.png)

For the bluetooth backend, we improved the speed of transferring data between devices (ivan tkachenko, 25.04.0 — [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/757/)).

{{< app-title appid="kgeotag" >}}

KGeoTag 1.7.0 is out! This release brings Qt6 support to the app. [Read the full announcement](https://kgeotag.kde.org/news/2024/kgeotag-1.7.0-released/).

{{< app-title appid="kphotoalbum" >}}

KPhotoAlbum 6.0.0 is out! This release also brings Qt6 support to the app. [Read the full announcement](https://www.kphotoalbum.org/2024/12/07/kphotoalbum-6.0.0-released/).

{{< app-title appid="konsole" >}}

It is now possible to resize Konsole's search bar (Eric D'Addario, 25.04.0 — [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1048)), and to search for an open tab by its name (Troy Hoover, 25.04.0 — [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1045)).

![](konsole-search.png)

{{< app-title appid="kongress" >}}

We now display the speaker's name (if available) in the talk info (Volker Krause, 25.04.0 — [Link](https://invent.kde.org/utilities/kongress/-/merge_requests/67)).

{{< app-title appid="kleopatra" >}}

We fixed a crash when the output directory for decrypting doesn't exist (Tobias Fella, 24.12.1 — [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/332)).

{{< app-title appid="krdc" >}}

We fixed the "Grab Keys" feature on Wayland when switching from and to full screen. Additionally the "Grab Keys" feature, now also correctly forwards every shortcut to the remote applications (Fabio Bas, 25.04.0 — [Link 1](https://invent.kde.org/network/krdc/-/merge_requests/136) and [link 2](https://invent.kde.org/network/krdc/-/merge_requests/134)).

We also fixed building KRDC on Haiku (Luc Schrijvers, 24.12.1 — [Link](https://invent.kde.org/network/krdc/-/merge_requests/137)).

{{< app-title appid="neochat" >}}

You can now sort rooms in the sidebar based on their most recent activity instead of by unread notifications (Soumyadeep Ghosh, 25.04.0 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2035)), and added a "Copy Link Address" context menu when clicking on a message (Kai Uwe Broulik, 25.04.0 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2040)).

![](neochat-copy-link.png)

We fixed the capitalization of the account dialog as well as many of NeoChat's settings pages (Joshua Goins, 25.04.0 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2022)), and removed device details from the device display name, as this could leak sensitive information (Tobias Fella, 24.12.0 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2032)).

{{< app-title appid="okular" >}}

When creating a new signature, Okular will automatically pick a font size depending on the available size instead of using a hardcoded size. This allows you to make signatures much smaller than before (Nicolas Fella, 25.04.0 — [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1053)). This work was sponsored by the Technische Universität Dresden.

{{< app-title appid="plasmatube" >}}

We improved the support for Piped, an alternative privacy-friendly YouTube frontend, in PlasmaTube, as we have improved the parsing of its media format information (Alexey Andreyev, 25.04.0 — [Link](https://invent.kde.org/multimedia/plasmatube/-/merge_requests/90)).

{{< app-title appid="tokodon" >}}

The Mastodon client used when posting on Mastodon is now displayed as a `Kirigami.Chip` element (Joshua Goins, 25.04.0 — [Link](https://invent.kde.org/network/tokodon/-/merge_requests/603)).

![](tokodon-post.png)

We also fixed the support for GoToSocial (snow flurry, 24.12.0 — [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/601) and [link 2](https://invent.kde.org/network/tokodon/-/merge_requests/602)), and added prelimary support for Iceshrimp (Joshua Goins, 24.12.0 — [Link](https://invent.kde.org/network/tokodon/-/merge_requests/598))).

{{< app-title appid="spectacle" >}}

Spectacle can now export to an animated WebP or a GIF (Noah Davis, 25.04.0 — [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/425)).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/12/07/this-week-in-plasma-oodles-of-features/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.


## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).

Thanks to Michael Mischurow and Tobias Fella for proofreading this post.
