---
title: 'This Week in KDE Apps'
subtitle: 'Optimization in Akonadi, configurable holiday region in Merkuro and progress on Krita Qt6 port'
discourse: thisweekinkdeapps
date: "2025-03-17T9:03:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
---

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

Last week we released the beta for KDE Gear 25.04 and focused on polishing the coming release.

## Creative Apps

{{< app-title appid="krita" >}}

The developer teams continued to improve the Qt6 port of Krita. Dmitry fixed the HDR support on Windows (Dmitry Kazakov, [link](https://invent.kde.org/graphics/krita/-/merge_requests/2348)). Freya fixed an OpenGL crash on macOS (Freya Lupen, [link](https://invent.kde.org/groups/graphics/-/merge_requests/?sort=updated_desc&state=merged&first_page_size=20)).

## Personal Information Management Apps

{{< app-title appid="akonadi" >}}

Carl Schwan reduced the memory usage of various Akonadi resources by around 75% each. The optimized resources, which take advantage of this new API, are the following: Birthday, VCard files and directories, Ical, Mbox, Open-Xchange, cardDAV and calDAV. There is already significant progress done in that direction also for the IMAP and POP3 resources. The technical background behind this is that these resources running as independent processes are now using non-visual QCoreApplication instead of the more powerful QApplication, which is more appropriate resource wise for background services. This is part of the [Don't depend on QtWidgets in lower parts of the stack](https://invent.kde.org/groups/pim/-/milestones/5#tab-issues) milestones. (Carl Schwan, 25.08.0. [Link 1](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/204), [link 2](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/205), [link 3](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/203), [link 4](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/206), [link 5](https://invent.kde.org/pim/akonadi/-/merge_requests/237), [link 6](https://invent.kde.org/pim/akonadi/-/merge_requests/239), [link 7](https://invent.kde.org/pim/akonadi/-/merge_requests/241), [link 8](https://invent.kde.org/pim/akonadi/-/merge_requests/243), [link 9](https://invent.kde.org/pim/akonadi/-/merge_requests/244), [link 10](https://invent.kde.org/pim/akonadi/-/merge_requests/245), [link 11](https://invent.kde.org/pim/akonadi/-/merge_requests/246), ...)

Daniel made a change to ensure that operations in Akonadi that operate on a large number of items are processed as multiple smaller batches which the SQL engine can then handle (Daniel Vratil, 25.08.0. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/221)).

{{< app-title appid="merkuro.calendar" >}}

Tobias ported Merkuro Calendar to the new QML declaration which slightly improves the performance but more importantly enables us to take advantage of the QML tooling (Tobias Fella, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/511)).

Carl made the region used to display holidays configurable. You can also select more than one region now (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/509))

{{< app-title appid="kleopatra" >}}

Tobias moved the notepad feature to a separate window, which means it's now possible to have multiple notepads open at the same time (Tobias Fella, 25.08.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/372)).

![](kleopatra-notepad.png)

Tobias also ensured the GPG password prompt (_pinentry_) in Kleopatra is properly parented to the correct parent window on Wayland (Tobias Fella, 25.04.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/373)). Other apps using GPG were also fixed.

{{< app-title appid="korganizer" >}}

Allen made a series of small improvements and bugfixes to Korganizer. He improved the configure view menu action description ([link](https://invent.kde.org/pim/korganizer/-/merge_requests/141)), added more information to the delete folder dialog ([link](https://invent.kde.org/pim/korganizer/-/merge_requests/136)), and added a search option to consider the current view filters. ([Link](https://invent.kde.org/pim/korganizer/-/merge_requests/137)).

## Social Apps

{{< app-title appid="neochat" >}}

James improved the thread support. Now it is possible to open a context menu for the individual thread messages (James Graham, 25.08.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2181)).

{{< app-title appid="kaidan" >}}

Melvin fixed downloading files (Melvin Keskin, [link 1](https://invent.kde.org/network/kaidan/-/merge_requests/1358) and [link 2](https://invent.kde.org/network/kaidan/-/merge_requests/1357)).

## Graphics and Multimedia Apps

{{< app-title appid="amarok" >}}

Tuomas fixed some database and encoding issues. (Tuomas Nurmi, [link](https://invent.kde.org/multimedia/amarok/-/merge_requests/134))

{{< app-title appid="digikam" >}}

The digiKam team released version 8.6.0. of the powerful photo classifying and editing tool. Among many other things, digiKam now comes with a smarter face management tool, an improved auto-tagging system that identifies elements in your images, fully automatic red-eye removal, and a new image quality feature that classifies images according to their aesthetic quality. The digiKam developers also fixed 140 bugs.

You can read more about this release [on digiKam's website](https://www.digikam.org/news/2025-03-15-8.6.0_release_announcement/).

![](digikam.png)

## System Apps

{{< app-title appid="kate" >}}

Javier Guerra added text search to the build output. (Javier Guerra, 25.08.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1747))

Leo Ruggeri made the reset history menu button only visible when relevant. (Leo Ruggeri, 25.08.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1754))

## Educational Apps

{{< app-title appid="gcompris" >}}

Bruno Anselme added a 6th level to the Guess 24 game. (Bruno Anselme, [Link](https://invent.kde.org/education/gcompris/-/merge_requests/221))

## Utilities

{{< app-title appid="ktrip" >}}

Volker improved the history of past searches in KTrip by reusing some code from Itinerary. The biggest improvements are that the list is now de-duplicated, and the model supports more features not yet exposed to the UI. (Volker Kruase, 25.08.0. [Link](https://invent.kde.org/utilities/ktrip/-/merge_requests/86))

### Other

Luigi removed Qt5 support in [Minuet](https://invent.kde.org/education/minuet/-/merge_requests/45) and [Step](https://invent.kde.org/education/step/-/merge_requests/37).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/25/this-week-in-plasma-fancy-time-zone-picker/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
