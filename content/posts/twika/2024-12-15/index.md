---
title: 'This Week in KDE Apps: Gear 24.12.0 is Out'
discourse: thisweekinkdeapps
date: "2024-12-15T12:50:35Z"
authors:
 - tobiasfella
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
itinerary:
 - name: The new My Data tab
   url: itinerary-mydata1.png
 - name: The new My Data tab
   url: itinerary-mydata2.png
powerplant:
 - name: Powerplant Overview
   url: powerplant-overview.png
 - name: Powerplant Plant Detail
   url: powerplant-plant.png
 - name: Powerplant Tasks
   url: powerplant-task.png
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week aside of releasing [KDE Gear 24.12.0](https://kde.org/announcements/gear/24.12.0/) and [Kaidan 0.10.0](https://www.kaidan.im/2024/12/09/kaidan-0.10.0/), we added an overview of all your data in Itinerary and polished many other apps. Some of us also meet in Berlin and organized a small KDE sprint where aside of eating some [Crêpes Bretonnes](https://kde.social/@carl/113651323876827291), we had discussion around Itinerary, Kirigami, Powerplant and more.

{{< app-title appid="itinerary" >}}

Itinerary has a new "My Data" page containing your program membership, health certificates, saved locations, travel statistics and let you export and import all the data from Itinerary. (Carl Schwan, 25.04.0 — [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/345))

{{< screenshots name="itinerary" >}}

{{< app-title appid="kalk" >}}

Fixed the "History" action not working (Joshua Goins, 25.04 — [Link](https://invent.kde.org/utilities/kalk/-/merge_requests/104))

{{< app-title appid="kaidan" >}}

Version 0.10.0 and 0.10.1 of Kaidan were released! See the [release announcement](https://www.kaidan.im/2024/12/09/kaidan-0.10.0/) for the full list of changes. 

{{< app-title appid="kongress" >}}

Show the speaker's name for each event (Volker Krause, 25.04 — [Link](https://invent.kde.org/utilities/kongress/-/merge_requests/67))

{{< app-title appid="kleopatra" >}}

Improved the dialog showing results of decrypt and verify operations (Tobias Fella, 25.04, [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/279))

Fixed a Qt6 regression that causes the dropdown menu for certificate selection to behave in unexpected ways (Tobias Fella, 25.04 — [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/338))

Improved the messages showing the result when decrypting and verifying the clipboard (Tobias Fella, 25.04 — [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/340))

{{< app-title appid="neochat" >}}

Fixed web shortcuts not working (Joshua Goins, 24.12.1 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2053))

Improved how colored text sent by some other clients shows up (Joshua Goins, 24.12.1 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2052))

Stop NeoChat from crashing when sending messages (Tobias Fella, 24.12.1 — [Link](https://invent.kde.org/network/neochat/-/merge_requests/2047))

{{< app-title appid="okular" >}}

Improved the look of banner messages (Carl Schwan, 25.04 — [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1081))

![](spectacle.png)

{{< app-title appid="powerplant" >}}

Mathis redesigned various part of Powerplant and added a tasks view. (Mathis Brucher)

{{< screenshots name="powerplant" >}}

## Other

More Kirigami applications are now remembering their size accross restart by using `KConfig.WindowStateSaver`. (Nate Graham, 25.04.0 — [Skanpage](https://invent.kde.org/utilities/skanpage/-/merge_requests/80) and [Elisa](https://invent.kde.org/multimedia/elisa/-/merge_requests/620))

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/12/14/this-week-in-plasma-better-fractional-scaling/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.


## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).