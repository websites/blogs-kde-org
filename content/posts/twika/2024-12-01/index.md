---
title: 'This Week in KDE Apps: OptiImage first release, Itinerary redesign and more'
discourse: thisweekinkdeapps
date: "2024-12-01T11:20:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
itinerary:
 - name: The timeline view
   url: itinerary-timeline.png
 - name: The query result
   url: itinerary-query-result.png
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week, we are continuing to polish our applications for the KDE Gear 24.12.0 release, but already starting the work for the 25.04 release happening next year. We also made the first release of OptiImage, an image size optimizer.

Meanwhile, as part of the [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/), you can "Adopt an App" in a symbolic effort to support your favorite KDE app. This week, we are particularly grateful to
Yogesh Girikumar, Luca Weiss and 1peter10 for supporting Itinerary;
Tobias Junghans and Curtis for Konsole;
Daniel Bagge and Xavier Guillot for Filelight;
F., Christian Terboven, Kevin Krammer and Sean M. for the Kontact suite;
Tanguy Fardet, dabe, lengau and Joshua Strobl for NeoChat;
Pablo Rauzy for KWrite;
PJ. for LabPlot;
Dominik Barth for Kasts;
Kevin Krammer for Ruqola;
Florent Tassy, [elbekai](https://norden.social/@elbekai) and retrokestrel for Gwenview;
MathiusD and Dadanaut for Elisa,
Andreas Kilgus and [@rph.space](https://bsky.app/profile/rph.space) for Konqueror;
[trainden@lemmy.blahaj.zone](https://lemmy.blahaj.zone/u/trainden) for KRDC;
Marco Rebhan and Travis McCoy for Ark; and
domportera for Krfb.

Getting back to all that's new in the KDE App scene, let's dig in!

{{< app-title appid="gcompris" >}}

GCompris 4.3 is out and contains bug fixes and graphics improvements on multiple activities.

[Read full announcement](https://gcompris.net/news/2024-11-29-en.html)

{{< app-title appid="itinerary" >}}

We redesigned the timeline of your trips and the query result pages when searching for a public transport connection to work better with a small screen while still showing all the relevant information. (Carl Schwan, 25.04.0. [Link 1](https://invent.kde.org/pim/itinerary/-/merge_requests/343) and [link 2](https://invent.kde.org/pim/itinerary/-/merge_requests/349))

{{< screenshots name="itinerary" >}}

Checking for updates and downloading map data now are scoped to a trip and will only query data from the internet related to the trip. (Volker Krause, 25.04.0. [Link 1](https://invent.kde.org/pim/itinerary/-/merge_requests/348) and [link 2](https://invent.kde.org/pim/itinerary/-/merge_requests/347))

The export buttons used in Itinerary are not blurry anymore. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/346))

Add an extractor for [GoOut](https://goout.net) tickets (an event platform in Poland, Czechia and Slovakia) as well as [luma](https://lu.ma/) and the pkpasses from Flixbus.de. (David Pilarcik, 24.12.0. [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/134), [link 2](https://invent.kde.org/pim/kitinerary/-/merge_requests/135) and [link 3](https://invent.kde.org/pim/kitinerary/-/merge_requests/133))

Optimize querying a location by sorting the list of countries only once instead of hundreds of times. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/351))

{{< app-title appid="optiimage" >}}

OptiImage 1.0.0 is out! This is the initial release of this image size optimizer and you can read all the details [on the announcement blog post](https://carlschwan.eu/2024/11/24/optiimage-1.0.0-is-out/).

![](https://carlschwan.eu/2024/11/24/optiimage-1.0.0-is-out/optiimage2.png)

{{< app-title appid="karp" >}}

Karp is now directly using the [QPDF](https://github.com/qpdf/qpdf) library instead of invoking a separate process, which improves the speed while making PDF operation more reliable. (Tomasz Bojczuk. [Link](https://invent.kde.org/graphics/karp/-/merge_requests/10))

{{< app-title appid="kate" >}}

Make it again possible to scroll, select text and click on links inside documentation tooltips in Kate. (Leia uwu, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1658)) Leia also improved the tooltip positioning logic so that it doesn't obscure the hovered word. (Leia uwu, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1660))

{{< app-title appid="kmail2" >}}

The mail folder selection dialog now remembers which folders were collapsed and expanded between invocations.

{{< app-title appid="ruqola" >}}

Ruqola 2.3.2 is out and includes many fixes for RocketChat 7.0!

[Read full announcement](/2024/11/26/ruqola-2.3.2/)

{{< app-title appid="spectacle" >}}

On Wayland, the "Window Under Cursor" mode is renamed to "Select Window" as you need to select the window. (Noah Davis, 25.04.0. [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/424))

{{< app-title appid="tokodon" >}}

Better icon for Android, which is also adaptable depending on your theme. (Alois Spitzbart, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/589))

![](tokodon-android-icon.jpg)

Streaming timeline events and notifications now work for servers using GoToSocial. (snow flurry, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/601))

Slightly improved the performance of the timeline, with particular focus on the media. (Joshua Goins, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/584))

In the status composer, user info is now shown - useful if you post from multiple accounts. Also, the look of the text box has been updated. (Joshua Goins, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/592))

![](tokodon-composer.png)

Added the ability to configure your notification policy. This allows you to reject or allow notifications e.g. for new accounts. (Joshua Goins, 25.03. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/590))

![](tokodon-notifications.png)

Improved the appearance of the search page on desktop. (Joshua Goins, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/593))

![](tokodon-search.png)

Added preliminary support for Iceshrimp.NET instances. (Joshua Goins, 24.12. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/598))

Added an error log in the UI to keep track of network errors. (Joshua Goins, 25.03. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/594))

![](tokodon-error-log.png)

## Kirigami Addons

Kirigami Addons 1.6.0. is out! You can read the full announcement [on my (Carl's) blog](https://carlschwan.eu/2024/11/24/kirigami-addons-1.6.0/). This week we also made the following changes:

Speedup loading Kirigami pages using `FormComboboxDelegate`, this is particularly noticable for the country combobox in Itinerary, but affects more applications. (Carl Schwan, Kirigami Addons 1.6.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/310))

Add new `RadioSelector` and `FormRadioSelectorDelegate` components to Kirigami Addons. On the screenshot below you can see how they're used in Itinerary. (Mathis Brüchert, Kirigami Addons 1.6.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/175))

![](radioselector.png)

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/11/30/this-week-in-plasma-disable-able-kwin-rules/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.


## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
