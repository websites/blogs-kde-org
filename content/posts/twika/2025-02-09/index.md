---
title: 'This Week in KDE Apps'
subtitle: Kasts polishing, progress on Krita Qt6 port and Kdenlive fundraising report
discourse: thisweekinkdeapps
date: "2025-02-10T11:25:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@carl@kde.social'
---

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/). This issue contains change from the last two weeks.

Much happened the past two weeks, we had a [successful KDE presence at FOSDEM](https://www.volkerkrause.eu/2025/02/08/kde-fosdem2025.html), we now have a location and date for this year's edition of [Linux App Summit](https://linuxappsummit.org/) (April 25-26, 2025 in Tirana, Albania) and also continued to improve our apps. Let's dive in!

## Releases

- [KDE Gear 24.12.2](https://kde.org/announcements/gear/24.12.2/) is out with some bugfixes.
- [Glaxnimate 0.6.0 beta](https://glaxnimate.org/news/releases/0.5.80/) is out. Glaxnimate is a 2d animation software and 0.6.0 is the first version of Glaxnimate as part of KDE. [Checkout Glaxnimate's website](https://glaxnimate.org/).
- [KStars 3.7.5](http://knro.blogspot.com/2025/02/kstars-v375-is-released.html) is out with mostly bugfixes and performance improvements.
- [GCompris 25.0](https://gcompris.net/news/2025-01-30-en.html) is out. This is a big release containing 5 new activities.
- [Krita 5.2.9](https://krita.org/en/posts/2025/krita-5-2-9-released/) is out. This is a bug fix release, containing all bugfixes of our bug hunt efforts back in November. Major bug-fixes include fixes to clone-layers, fixes to opacity handling, in particular for file formats like Exr, a number of crash fixes and much more!

{{< app-title appid="akonadi" >}}

We fixed an issue where loading tags was broken and would result in a constant 100% CPU usage. (Carl Schwan, 24.12.3. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/226))

{{< app-title appid="elisa" >}}

We now correctly re-open Elisa when it was minimized to the system tray. (Pedro Nishiyama, 24.12.2. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/666))

{{< app-title appid="dolphin" >}}

We made it possible to rename tabs in Dolphin. This action is available in each tab's context menu. This is useful for very long tab names or when it is difficult to identify a tab by a folder's name alone. (ambar chakravartty, 25.04.0. [Link](https://bugs.kde.org/show_bug.cgi?id=197009))

We also improved the keyboard based selection of items. Typing a letter on the keyboard usually selects the item in the view which starts with that letter. Diacritics are now ignored here, so you will for example be able to press the "U" key to select a file starting with an "Ü". (Thomas Moerschell, 24.12.3. [Link](https://bugs.kde.org/show_bug.cgi?id=482394))

We changed the three view buttons to a single menu button. (Akseli Lahtinen, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/894))

![](dolphin-menu.png)

We made the "Empty Trash" icon red in conformance to our HIG as it is a destructive operation. (Nate Graham, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/898))

We improved getting the information from supported version control systems (e.g. Git). It is now faster and happens earlier. (Méven Car, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/899))

{{< app-title appid="falkon" >}}

We added input methods hints to input fields. This is mostly helpful when using different input methods than a traditional keyboard (e.g. a virtual keyboard). (Juraj Oravec. [Link](https://invent.kde.org/network/falkon/-/merge_requests/122))

{{< app-title appid="itinerary" >}}

We continued to improve the coverage of Itinerary in Poland. This week we added support for the train operator [Polregio](https://polregio.pl/), fixed and refactored the extractor for Koleo and rewrote the extractor for PKP-app to support the ticket layouts. (Grzegorz Mu, 24.12.3. [Link 1](https://invent.kde.org/pim/kitinerary/-/merge_requests/161), [link 2](https://invent.kde.org/pim/kitinerary/-/merge_requests/160), and [link 3](https://invent.kde.org/pim/kitinerary/-/merge_requests/159))

We also added support for CitizenM hotel bookings. (Joshua Goins, 24.12.3. [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/162))

We also started working on an online version of the ticket extractor. A preview is available on [Carl's website](https://kitinerary.carlschwan.eu).

Volker also published a recap of the [past two months in Itinerary](https://www.volkerkrause.eu/2025/01/31/kde-itinerary-december-january-2025.html). This contains also some orthogonal topics like the free software routing service [Transitous](https://transitous.org/).

{{< app-title appid="kasts" >}}

We fixed the vertical alignment of the queue header. (Joshua Goin, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/228))

We are now using `Kirigami.UrlButton` for links and `Kirigami.SelectableLabel` for the text description in the podcast details page to improve visual and behavior consistency with other Kirigami applications. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/219))

We also improved the look of the search bar in the discovery page. It's now properly separated from the rest of the content. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/221))

We added the ability to force the app to mobile/desktop mode. (Bart De Vries, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/214))

![](kasts-mode.png)

We fixed the sort order of the podcasts episodes. (Bart De Vries, 24.12.3. [Link](https://bugs.kde.org/show_bug.cgi?id=499712))

Finally we made various improvements to our usage of QML in Kasts to use newer QML constructs. This should improve slighly the performance while reducing the technical debt. (Tobias Fella, 25.04.0. [Link 1](https://invent.kde.org/multimedia/kasts/-/merge_requests/233), [link 2](https://invent.kde.org/multimedia/kasts/-/merge_requests/238), [link 3](https://invent.kde.org/multimedia/kasts/-/merge_requests/237), [link 4](https://invent.kde.org/multimedia/kasts/-/merge_requests/237), [link 5](https://invent.kde.org/multimedia/kasts/-/merge_requests/235), and [link 6](https://invent.kde.org/multimedia/kasts/-/merge_requests/216))

{{< app-title appid="kate" >}}

We fixed some issues with the list of commits displayed in Kate. The highlight color is now correct and the margins consistent. (Leo Ruggeri, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1705))

We improved the diff widget of Kate. The toolbar icon sizes are now the same as other toolbars in Kate. (Leo Ruggeri, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1709))

{{< app-title appid="kdenlive" >}}

The Kdenlive team [published a report](https://kdenlive.org/en/2025/02/kdenlive-fundraising-final-report/) about the result of their last fundraising. It contains a huge amount of great improvements, so go read it! 

Added checkerboard option in clip monitor background (Julius Künzel, 25.04.0, [Link](https://invent.kde.org/multimedia/kdenlive/-/commit/8a0d13af8cb5ae275e0b4dc5f8472a72de4564f6))

![](kdenlive.png)

{{< app-title appid="konqueror" >}}

We fixed the handling of the cookie policy when no policy has been explicitly set. (Stefano Crocco, 24.12.3. [Link](https://invent.kde.org/network/konqueror/-/merge_requests/396))

{{< app-title appid="krita" >}}

The Krita team continued porting Krita to Qt6/KF6. The application now compiles and run with Qt6, but there are still some uni tests not working. [Link to Mastodon thread](https://fosstodon.org/@halla/113945958080701380)

![Krita with Qt6](krita-qt6.png)

Ramon published a video about "[Memileo Brushes](https://krita.org/en/posts/2025/new_video_impasto_brushes/)" on YouTube.

{{< app-title appid="krdc" >}}

We implemented the dynamic resolution mode from the remote desktop protocol (RDP). This means we now resize the remote desktop to fit the current KRDC window. This works for Windows >= 8.1. (Fabio Bas, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/160))

We added support for the `domain` field in the authentication process. (Fabio Fas, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/157))

We adapted the code to work with FreeRDP 3.11. (Fabio Bas, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/164))

{{< app-title appid="marknote" >}}

We fixed the list of "Sort Notes List" option not being set by default. (Joshua Goins. [Link](https://invent.kde.org/office/marknote/-/merge_requests/67))

We now properly capitalize the "undo" and "redo" actions. (Joshua Goins. [Link](https://invent.kde.org/office/marknote/-/merge_requests/66))

We removed internal copies of some Kirigami Addons components in Marknote. (Joshua Goins. [Link](https://invent.kde.org/office/marknote/-/merge_requests/68))

{{< app-title appid="okular" >}}

We added a way to filter the list of certificate to only show certificates for "Qualified Signatures" in the certificate selection. (Sune Vuorela, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1120))

{{< app-title appid="plasmatube" >}}

We improved the placeholder messages for empty views. (Joshua Goins, 25.04.0. [Link 1](https://invent.kde.org/multimedia/plasmatube/-/commit/fc648c77f49f333b99009f8d206a05d14d3093f2) and [link 2](https://invent.kde.org/multimedia/plasmatube/-/commit/9757789dd1e163e9ee1c8b463d6c831a5433c26d))

We fixed displaying thumbnails and avatars when using the Peertube backend. (Joshua Goins, 24.12.3. [Link](https://invent.kde.org/multimedia/plasmatube/-/commit/456b5bf5231445ed87577155f319d1b0390b038d), [link 2](https://invent.kde.org/multimedia/plasmatube/-/commit/2cfb7cd25ef27f84233a0e46fa4c1cf43374b73f), and [link 3](https://invent.kde.org/multimedia/plasmatube/-/commit/6b3bd58638dfd9b92286baae0409f588115783cd))

{{< app-title appid="qrca" >}}

Qrca can now scan a QR code directly from an image instead of just from the camera. (Onuralp Sezer, 25.04.0. [Link](https://invent.kde.org/utilities/qrca/-/merge_requests/104))

{{< app-title appid="tokodon" >}}

We are now using more fitting icons for the "Embed" and "Open in Browser" actions in Tokodon's context menu. We also removed the duplicated "Copy to Clipboard" action from that context menu. (Joshua Goins, 24.12.3. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/710) and [link 2](https://invent.kde.org/network/tokodon/-/merge_requests/711))

![](tokodon-contextmenu.png)

Following the improvements from two weeks ago, we did even even more accessibility/screen reader improvements to Tokodon. (Joshua Goins, 24.12.3. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/709))

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/25/this-week-in-plasma-fancy-time-zone-picker/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetarnky
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
