---
title: 'This Week in KDE Apps: #38c3'
discourse: thisweekinkdeapps
date: "2024-12-29T12:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
---

Unfortunately, there won't be any "This Week in KDE Apps" blog post this week as
I (Carl) and others are at the #38C3 (Chaos Communication Congress) in Hamburg.
But if you are also there, don't hesitate to come by and say hi.

![The KDE stand at 38c3](38c3.jpg)