---
title: 'This Week in KDE Apps: Adopt an App'
discourse: thisweekinkdeapps
date: "2024-11-10T8:20:35Z"
authors:
 - carlschwan
 - paulbrown
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@kde@floss.social'
---
![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week, we released [KDE Gear 24.08.3](https://kde.org/announcements/gear/24.08.3/) and we are preparing the 24.12.0 release with the beta [planned for next week](https://tsdgeos.blogspot.com/2024/11/kde-gear-2412-branches-created.html). The final release will happen on December 12th, but, meanwhile, and as part of the [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/), you can "Adopt an App" in a symbolic effort to support your favorite KDE app.

This week, we are particularly grateful to [@petejones@hcommons.social](https://hcommons.social/@petejones), [@DaisyLee@mastodon.social](https://mastodon.social/@DaisyLee) and Karcsesz for showing their support for [Tokodon](https://apps.kde.org/tokodon/); manchicken for [Merkuro](https://apps.kde.org/merkuro/) and [fat_malama](https://www.instagram.com/fat_malama/), Alexandru Traistaru and Neeko iko for [KDE Connect](https://apps.kde.org/kdeconnect/).

Any monetary contribution, however small, will help us cover operational costs, salaries, travel expenses for contributors and in general just keep KDE bringing Free Software to the world. So consider [donating](https://kde.org/fundraisers/yearend2024/) today!

Getting back to all that's new in the KDE App scene, let's dig in!

{{< app-title appid="alligator" >}}

The user can no longer open the feed details page multiple times (Soumyadeep Ghosh, 24.12.0, [link](https://bugs.kde.org/495724)).

{{< app-title appid="falkon" >}}

It is now possible to open a context menu with [Greasemonkey](https://en.wikipedia.org/wiki/Greasemonkey) (Juraj Oravec, 24.12.0, [link](https://invent.kde.org/network/falkon/-/merge_requests/89)). Greasemonkey lets you run little scripts that make on-the-fly changes to web page content. Juraj also removed the advertised FTP support in Falkon as the support for FTP was removed from Chromium. (Juraj Oravec, 24.12.0, [link](https://invent.kde.org/network/falkon/-/merge_requests/92))

{{< app-title appid="dolphin" >}}

We no longer ask password twice when entering the Dolphin's admin mode (kio-admin) (Felix Ernst, 24.12.0, [link](https://invent.kde.org/system/kio-admin/-/merge_requests/21)).

Felix also improved the keyboard navigation in the toolbar, now the elements are focused in the right order (Felix Ernst, 24.12.0. [link 1](https://invent.kde.org/system/dolphin/-/merge_requests/851) and [link 2](https://invent.kde.org/frameworks/kio/-/merge_requests/1756)).

{{< app-title appid="itinerary" >}}

Itinerary can now show you a map of the whole trip (Volker Krause, 24.12.0, [link](https://invent.kde.org/pim/itinerary/-/commit/10b4cecb4a50120093173f9ece4eb028761fee52)).

![](itinerary-map.png)

And display some statistics about your trip, for example the CO<sub>2</sub> emission, the distance travelled and the costs (if available) (Volker Krause, 24.12.0, [link](https://invent.kde.org/pim/itinerary/-/commit/19b6d2b6b3e7bb4edc9f97cac3e32059286061e2)).

![](itinerary.png)

Finally, the alignment of timeline elements in Itinerary is now much more consistent (Carl Schwan, 24.12.0, [link](https://invent.kde.org/pim/itinerary/-/merge_requests/333)).

{{< app-title appid="okular" >}}

When loading PDF files with Ink annotation containing an empty path, Okular won't crash. You shouldn't be able to create such annotations with Okular anyway, but some PDF files out there do contains such annotations (Albert Astals Cid, 24.12.0, [link](https://invent.kde.org/graphics/okular/-/merge_requests/1072)).

We also no longer hide the signing UI prematurely and now ensure it is visible until the signing process is actually finished (Nicolas Fella, 24.12.0, [link](https://invent.kde.org/graphics/okular/-/merge_requests/1056)).

Finally we fixed a small memory leak in Okular's latex support (Nil Admirari, 24.12.0, [link](https://invent.kde.org/graphics/okular/-/merge_requests/1068)).

{{< app-title appid="kaffeine" >}}

Kaffeine got ported to Qt6/KF6 (Tobias Klausmann, 24.12.0, [link](https://invent.kde.org/multimedia/kaffeine/-/merge_requests/11)).

{{< app-title appid="kalk" >}}

Kalk will now correctly handle pressing the <kbd>Esc</kbd> key and clear the input field like many other calculator applications do, instead of creating strange characters in the input area (Devin Lin, 24.12.0, [link](https://invent.kde.org/utilities/kalk/-/merge_requests/98)).

{{< app-title appid="kasts" >}}

Bart de Vries fixed password loading for synchronisation services on Windows (Bart De Vries, 24.12.0 [link](https://invent.kde.org/multimedia/kasts/-/merge_requests/203)).

{{< app-title appid="kate" >}}

The performance of displaying the build output has been improved (Waqar Ahmed, 24.12.0 [link](https://invent.kde.org/utilities/kate/-/merge_requests/1645/)).

{{< app-title appid="kdevelop" >}}

We addressed certain annoyances when working with the flatpak runtime. This included, for example, improving the handling of `.flatpak-manifest.json` files which we use in KDE for storing application's Flatpak manifest (Aleix Pol Gonzalez, 24.12.0 [link](https://invent.kde.org/kdevelop/kdevelop/-/merge_requests/673)).

{{< app-title appid="kmail2" >}}

KMail and other PIM applications can now be compiled on Windows (Ingo Klöcker and Laurent Montel [link 1](https://invent.kde.org/packaging/craft-blueprints-kde/-/merge_requests/1019) and [link 2](https://invent.kde.org/packaging/craft-blueprints-kde/-/commits/master/kde/pim?ref_type=heads)). Having KDE PIM applications work well on Windows is still in early stages of development. There is still a lot of work required to [make Kontact a good experience on that platform](https://invent.kde.org/groups/pim/-/milestones/6#tab-issues).

![](kmail-windows.png)

{{< app-title appid="kmix" >}}

KMix got ported to Qt6/KF6 (Jonathan Marten, 24.12.0 [link](https://invent.kde.org/multimedia/kmix/-/merge_requests/30)).

{{< app-title appid="krita" >}}

SVGs with clip masks now render faster (Dmitry Kazakov, [link](https://invent.kde.org/graphics/krita/-/merge_requests/2265)).

{{< app-title appid="konsole" >}}

Konsole now always creates a cgroup hierarchy when creating new process. This prevents entire applications getting killed in an Out-Of-Memory (OOM) scenarios when a tab consumes too much RAM (David Redondo, 24.12.0, [link](https://invent.kde.org/utilities/konsole/-/merge_requests/1042)). David also ensured the subprocess of Konsole are correctly mapped to Konsole's `.desktop` file ([link](https://invent.kde.org/utilities/konsole/-/merge_requests/1041)).

{{< app-title appid="kwave" >}}

Kwave now provides a better visual indication when playback is paused (Mark Penner, 24.12.0, [link](https://invent.kde.org/multimedia/kwave/-/merge_requests/40)).

{{< app-title appid="neochat" >}}

We improved the network proxy's config page look to make it more consistent with the other config pages (Joshua Goins, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1965)).

Joshua Goins and Olivier Beard improved the link preview. Now clicking anywhere on the link preview will take you to the linked webpage (Joshua Goins, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1971)). The separator shown to the left of the preview and quoted text also got stylish rounded corners (Olivier Beard, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1900)).

NeoChat now hides non-standard rooms from the room list as most of the time they are used exclusively for holding data (e.g. trip group information from Itinerary) and not meant to be interacted with (Joshua Goins, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1966)).

We also improved the way polls look (Carl Schwan, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1940)).

![](neochat-poll.png)

And made sending messages and inserting newline shortcuts configurable (Eren Karakas, 24.12.0, [link](https://invent.kde.org/network/neochat/-/merge_requests/1959)).

![](neochat-config.png)

{{< app-title appid="tokodon" >}}

Right clicking on a link on a post will now show a context menu allowing users to copy or share the URL directly (Arran Ubels, 24.12.0, [link](https://invent.kde.org/network/tokodon/-/merge_requests/564)).

## And all this too...

The layout of the _About_ dialog of applications using QtWidgets has been improved (Carl Schwan, KDE Frameworks 6.10.0, [link](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/242)).

{{< img-comparison-slider before="old-aboutdata.png" after="new-aboutdata.png" >}}

## ... And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/11/09/this-week-in-plasma-everything-you-wanted-and-more/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
