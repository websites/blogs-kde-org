---
title: 'This Week in KDE Apps'
subtitle: Qrca WiFi mode, Trust and Safety in Tokodon, and more
discourse: thisweekinkdeapps
date: "2025-01-27T10:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
---

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

Due to FOSDEM happening next weekend, there won't be any "This Week in KDE Apps" post next week. If you are in Brussels during the event, the KDE team will be in  [building AW](https://fosdem.org/2025/stands/), next to our friends from GNOME. Come say hi, we will have some stickers and demo devices!

## General Changes

The About page used in many Kirigami apps now uses a new `FormLinkDelegate` for entries that will open a link. (Carl Schwan, Kirigami Addons 1.8.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/343))

![](aboutpage.png)

{{< app-title appid="amarok" >}}

Support for Digital Audio Access Protocol (DAAP) was fixed. (Tuomas Nurmi, 3.2.2. [Link](https://invent.kde.org/multimedia/amarok/-/merge_requests/138))

{{< app-title appid="akonadi" >}}

Loading IMAP tags was optimized. (Carl Schwan, 24.12.2. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/224))

Some SQL queries were fixed so that they don't exceed the limits imposed by the SQL engines (e.g. when reindexing a big email folders). (David Faure, 24.12.2. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/223))

{{< app-title appid="elisa" >}}

Files will play automatically when opened from a different app (e.g. Dolphin). (Pedro Nishiyama, 25.04.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/658))

{{< app-title appid="itinerary" >}}

We improved the ticket extractor for PKP (Grzegorz Mu, 24.12.2, [Link](https://invent.kde.org/pim/kitinerary-/merge_requests/159))

We fixed public transport data access from Entur in Norway (24.12.2, also affects KTrip).

{{< app-title appid="kaidan" >}}

The onboarding workflow of Kaidan was completely overhauled. (Melvin Keskin. [Link](https://invent.kde.org/network/kaidan/-/merge_requests/1294))

The QR-code scanner and generator of Kaidan now uses Prison, KDE's standard QR-Code library (Jonah Brüchert and Melvin Keskin. [Link](https://invent.kde.org/network/kaidan/-/merge_requests/820))

{{< app-title appid="kalk" >}}

The history feature was fixed. (François Guerraz, 24.12.2. [Link](https://invent.kde.org/utilities/kalk/-/merge_requests/110))

{{< app-title appid="okular" >}}

We fixed Okular freezing when opening a PDF file with a lot of entries in a choice field. (Albert Astals Cid, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1117))

{{< app-title appid="qrca" >}}

Qrca gained a mode to only scan for Wifi QR-codes. Currently this can be triggered with the `--wifi` flag, but in the future this will be triggered directly from Plasma Network Management to scan for Wifi codes. Additionally when scaning the QR-code for an existing connection, instead of creating a new connection, Qrca will update the credentials of the existing connection. (Kai Uwe Broulik. [Link](https://invent.kde.org/utilities/qrca/-/merge_requests/95))

We removed the option to share a QR-code and replace it with a button to copy the QR-code. (Jonah Brüchert. [Link](https://invent.kde.org/utilities/qrca/-/merge_requests/103))

{{< app-title appid="tokodon" >}}

We added a menu item under the "Filters" timeline action to configure filters. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/708))

![](tokodon-menu-filter.png)

We improved the look of filtered posts significantly. (Joshua Goins, 25.04.0. [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/705))

![](tokodon-filter.png)

Tags and polls are hidden when the post has a content notice. (Joshua Goins, 25.04.0. [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/707) and [link 2](https://invent.kde.org/network/tokodon/-/merge_requests/702))

As part of more trust and safety improvements, we added a button to mute a conversation, so that you don't get any notifications for conversations you are not interested too. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/707))

We fixed voting in polls that was not working reliably. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/700)) and improved Tokodon when using a screen reader. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/695))

## Third Party Apps

### BlueJay

Evan Maddock released the [1.0.0](https://github.com/EbonJaeger/bluejay/releases/tag/v1.0.0) and [1.0.1](https://github.com/EbonJaeger/bluejay/releases/tag/v1.0.1) of BlueJay. BlueJay is a Bluetooth manager written in Qt with Kirigami.

![](bluejay.png)

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/25/this-week-in-plasma-fancy-time-zone-picker/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
