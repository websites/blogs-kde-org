---
title: 'This Week in KDE Apps: Mobile context menus'
discourse: thisweekinkdeapps
date: "2025-01-05T12:50:35Z"
authors:
 - carlschwan
 - Felix Ernst
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
contextmenu:
 - name: NeoChat's message context menu
   url: neochat1.png
 - name: NeoChat's room context menu
   url: neochat2.png
 - name: NeoChat's space context menu
   url: neochat3.png
 - name: NeoChat's account context menu
   url: neochat4.png
 - name: Tokodon's context menu
   url: tokodon-contextmenu.png
 - name: Merkuro's context menu
   url: merkuro-contextmenu.png
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/). This week we cover the two latest weeks as, due to the holidays, there wasn't a post last week.

## General changes

The convergent context menu from NeoChat was made more generic and upstreamed to Kirigami Addons. It is now used by Tokodon and Merkuro with work underway to integrate it into more apps. (Carl Schwan and Joshua Goins, Kirigami Addons 1.7.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/332))

{{< screenshots name="contextmenu" >}}

And, don't worry, on desktop platforms, the context menu stays the same.

Help is very much welcome to port more applications to this new component. Here are some merge requests for inspiration:

- NeoChat: https://invent.kde.org/network/neochat/-/merge_requests/2092
- Tokodon: https://invent.kde.org/network/tokodon/-/merge_requests/659 and https://invent.kde.org/network/tokodon/-/merge_requests/657
- Merkuro: https://invent.kde.org/pim/merkuro/-/merge_requests/495

{{< app-title appid="alligator" >}}

Alligator now has a nice F-Droid banner. (Alois Spitzbart. [Link](https://invent.kde.org/network/alligator/-/merge_requests/124))

![](alligator-fdroid.jpg)

{{< app-title appid="amarok" >}}

Amarok 3.2 is out! This release brings experimental Qt6 support while keeping Qt5 support for now. [Release announcement](https://blogs.kde.org/2024/12/29/amarok-3.2-punkadiddle-released/)

{{< app-title appid="arianna" >}}

Arianna allows users to use the wheel to turn pages. (Tomaz Canabrava, 25.04.0. [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/62)), and once again correctly remembers the current reading progress of books. (Ryan Zeigler, 25.04.0. [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/64))

{{< app-title appid="dolphin" >}}

When using languages which are typically written right-to-left, like Arabic or Hebrew, Dolphin's layout is mirrored to also go from right to left. Type `dolphin --reverse` in Konsole to try it out! It is an interesting experience. This week, Dolphin's details view mode finally received right-to-left support, which will show file icons on the right and folder names and details on the left. This change also includes some general improvements to resizing of columns in details view mode. (Felix Ernst, 25.04.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=449211), [Link 2](https://bugs.kde.org/show_bug.cgi?id=495942). Thanks to [the European Commission and NLnet for funding](https://nlnet.nl/project/DolphinAuth/) this work.)

Dolphin's selection mode now also changes keyboard controls to allow easier selecting. Previously there was no reason for keyboard-only users to ever use selection mode because the controls were identical. Now, when you are selecting specific files among a list you will no longer have to fear that accidentally letting go of the Control key while moving will clear your selection! Simply go into selection mode instead, move focus with arrow keys, and press Enter to toggle the selection. (Felix Ernst, 25.04.0. [Link](https://bugs.kde.org/show_bug.cgi?id=458091))

Finally, Dolphin received a quick accessibility report as part of the NLNet funding, and, while some issues were found, the report noted that "overall the app is very accessible"!

{{< app-title appid="falkon" >}}

The built-in ad blocker in Falkon can now also block websockets. (Juraj Oravec, 25.04.0. [Link](https://invent.kde.org/network/falkon/-/merge_requests/114))

{{< app-title appid="itinerary" >}}

We now displays platform information also for bus reservations. (Volker Krause, 25.04.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/374))

{{< app-title appid="kaidan" >}}

Laurent Montel and Melvin Keskin spent some times finishing the [Qt6 port](https://invent.kde.org/network/kaidan/-/merge_requests/1249), [enabling the generation of Windows builds](https://invent.kde.org/network/kaidan/-/merge_requests/1258), [bringing the codebase to KDE standards](https://invent.kde.org/network/kaidan/-/merge_requests/1248), and [more](https://invent.kde.org/network/kaidan/-/merge_requests?scope=all&state=merged).

{{< app-title appid="kate" >}}

You can now add paths to the PATH environment variable used by Kate, which is useful if you use LSP servers, formatters, or linters not present in your default PATH variable (Waqar Ahmed, 24.05.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1686))

We fixed opening URLs ending with `:x:y` cursor information from remote URLs. (Christoph Cullmann, 24.12.1. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1683)), and added a file template plugin which allows you to generate files from existing templates (Kåre Särs, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1676))

Finally, Joshua posted a blog post on how to use Hugo with Kate on [his blog](https://redstrate.com/blog/2024/12/using-hugo-with-kate/).

{{< app-title appid="kasts" >}}

Kasts' configuration dialog was ported to a newer component which works better on mobile devices. (Bart De Vries, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/209))

<!--
{{< app-title appid="konsole" >}}

Konsole now displays progress for some commands in the tab bar. This will work with systemd tools, kde-builder, and any other tools using the ConEmu-specific OSC (Operating System Command) to report their progress. (Kai Uwe, 25.04.0. [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1054)). You can read more details about this on [Kai Uwe's blog](https://blog.broulik.de/2024/12/holiday-hacking-2024/).

![](konsole-progress.png)
-->

{{< app-title appid="kdeconnect" >}}

It is now possible to manually enable and disable the network or the bluetooth backend for KDE Connect. (Rob Emery, 25.04.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/733))

![](kdeconnect-settings.png)

We also redesigned the welcome page of the Kirigami version of KDE Connect. (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/761))

![](kdeconnect-welcome.png)

On iOS, notifications for pairing a new device will be displayed in the app itself as a fallback if the app is not permitted to display notifications. (Ruixuan Tu. [Link](https://invent.kde.org/network/kdeconnect-ios/-/merge_requests/129))

![](kdeconnectios.png)

On Android, the application was made compatible with Android 15 (Mash Kyrielight. [Link](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/498)); more code was ported from Java to Kotlin (TPJ Schikhof. [Link 1](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/508), [link 2](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/490)); we reworked the custom devices lists to show the connection status and display toast messages when trying to add a device that already exists (TPJ Schikhof. [Link](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/453)); and we fixed the icon colors when using a dark theme (Mash Kyrielight. [Link](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/499)).

![](kdeconnectandroid.png)

{{< app-title appid="kdenlive" >}}

Jean-Baptiste Mardelle, from the Kdenlive team, posted an update about the much-requested feature of a modern background removal tool. Good news, there are testing binaries available. For more details, [consult his blog post](https://kdenlive.org/en/2024/12/kdenlive-new-year-preview/).

<video class="img-fluid" controls>
  <source src="bg-remove.mp4" type="video/mp4">
</video>

{{< app-title appid="kwave" >}}

KWave's file dialog now works in Flatpak and Snaps. (Mark Penner, 24.05.0. [Link](https://invent.kde.org/multimedia/kwave/-/merge_requests/32))

{{< app-title appid="okular" >}}

We fixed scrolling down/up at the last/first page in single page, non-continuous mode. (Kai Shen, 24.12.1. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1101))

{{< app-title appid="powerplant" >}}

Powerplant now shows the weather forecast in the home page. (Mathis Brüchert, 1.0.0. [Link](https://invent.kde.org/utilities/powerplant/-/merge_requests/47))

![](powerplant-weather.png)

{{< app-title appid="merkuro.mail" >}}

There is now a confirmation dialog when deleting an email. (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/commit/aeda45800258e3bd090ee967e1797d223ff8c673))

![](merkuro-mail-confirmation-dialog.png)

{{< app-title appid="neochat" >}}

Kai Uwe Broulik made avatars load asynchronously, which speeds up scrolling through the list of rooms and the timeline. (25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2084)). A similar change was also done to Tokodon.

{{< app-title appid="telly-skout" >}}

We fixed fetching some TV channels for the TV Spielfilm backend (Plata Hill, 24.12.1. [Link](https://invent.kde.org/utilities/telly-skout/-/merge_requests/196)) and it's now possible to see what is hapening on your favorite TV channels for more than the current day. (Plata Hill, 25.04.0. [Link](https://invent.kde.org/utilities/telly-skout/-/merge_requests/187))

Plata also made numerous behind the scene changes to Telly Skout.

## Third Party Apps

### Supersonik

Adam Pigg released the first tagged release of [Supersonik](https://github.com/piggz/supersonik), a Subsonic client written using Kirigami for SailfishOS and other mobile Linux operating systems.

### Organic Maps

Organic Maps, one of the best map applications for Android, got their [NlNet grant request accepted to work on a convergent UI for Linux based on Kirigami](https://nlnet.nl/project/OrganicMaps-ConvergentUI/).

## Packaging

Scarlett posted two updates about the state of Snaps in 24.12.0. You can read them on [her blog](https://www.scarlettgatelymoore.dev/).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/04/this-week-in-plasma-artistry-and-accessibility/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
