---
title: This Week in KDE Apps
subtitle: More Qt6 migrations, more mobile stuff, and more screenshots 
discourse: thisweekinkdeapps
date: "2024-10-20T13:30:35Z"
authors:
 - carlschwan
image: thumbnail.png
categories:
 - This Week in KDE Apps
newsletter: 6cc7d914-24b5-4363-bd6a-cd70a3af79aa
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week we migrated more apps to Qt6, made Dolphin more optimized for mobile and added many new features to Tokodon.

Let's get started!

{{< app-title appid="cantor" >}}

Cantor has been ported to Qt6. (Alexander Semke, Carl Schwan, Nikita Sirgienko and Stefan Gerlach, 24.12.0. [Link](https://invent.kde.org/education/cantor/-/merge_requests/63))

[Cantor's website](https://cantor.kde.org) was ported from Jekyll to Hugo for easier maintenance and a sleeker design. (Carl Schwan, Now! [Link](https://invent.kde.org/websites/cantor-kde-org/-/merge_requests/8))

{{< app-title appid="digikam" >}}

Percent values are now correctly translated. (Emir Sari. [Link](https://invent.kde.org/graphics/digikam/-/merge_requests/249))

{{< app-title appid="dolphin" >}}

From now on, Dolphin uses a phone-optimized alternative user interface when started on [Plasma Mobile](https://plasma-mobile.org/). After the addition of a [selection mode](https://tube.tchncs.de/w/doXJD33iiBBDoB2G1d3Yn9?subtitle=en) and [improvements](https://invent.kde.org/system/dolphin/-/commit/d7b33b76a18b14e9f286e4d8326b00910b9ea02a) [to](https://invent.kde.org/system/dolphin/-/commit/71ea4a88d890949500d05aa21839ad93ec6bdb1d) [touchscreen-compatibility](https://invent.kde.org/system/dolphin/-/commit/3123c086a77c28730866e848178c7e2d9a9006cb), Dolphin is surprisingly great on phones now! However, more work is still needed to more closely align the phone user interface with that of a phone app expectations. (Felix Ernst, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/826))

![](dolphin-mobile.png)

The right-click context menu for the Trash folder now contains actions to sort the Trash, change its view mode, and for cutting and copying items. (Eren Karakas, 24.12.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=493808), [Link 2](https://bugs.kde.org/show_bug.cgi?id=476955))

Quickly pressing the back or forward buttons on a mouse twice is no longer incorrectly interpreted as only wanting to go back or forward once. (Wolfgang Müller, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=485295))

{{< app-title appid="francis" >}}

Francis now lets you skip the current phase of work or break time. (Joëlle van Essen, 24.12.0. [Link](https://invent.kde.org/utilities/francis/-/merge_requests/18))

![](francis.png)

{{< app-title appid="gwenview" >}}

There is new a setting to disable starting over from the first image when moving forward beyond the last one. (Christian Svensson, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=458332))

{{< app-title appid="kdeconnect" >}}

You can now filter plugins in the plugin settings page. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/738))

![](kdeconnect-plugins.png)

We fixed getting default sounds for the "Find this Device" plugin in the Kirigami app. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/736))

{{< app-title appid="kleopatra" >}}

Improvements have been made to the debug dialog and it now allows you to run both pre-configured and custom debugging commands. (Tobias Fella, Gear 24.12.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/290))

![](kleopatra-debug.png)

{{< app-title appid="klevernotes" >}}

KleverNotes is now available on Flathub. (Louis Schul. [Link](https://flathub.org/apps/org.kde.klevernotes))

{{< app-title appid="kmail2" >}}

KMail's SMTP configuration dialog has been redesigned. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/kmailtransport/-/commit/3f991d9cb662a338e0776db8e5b247eeb49c04bc))

![](kmail-smtp.png)

{{< app-title appid="krdc" >}}

KRDC starts just by opening a `.rdp` file containing the RDP connection configuration. (Fabio Bas, 24.12.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/107))

{{< app-title appid="krita" >}}

There is now an option that lets you select the default color space for EXR files. (Dmitry Kazakov. [Link](https://invent.kde.org/graphics/krita/-/merge_requests/2256))

{{< app-title appid="ktorrent" >}}

Tooltips that had white on white text (and were thus unreadable) have been fixed. (Albert Astals Cid. 24.08.3. [Link](https://invent.kde.org/network/ktorrent/-/merge_requests/128))

You now have a more compact date format for the torrent list "Added" column. (George Florea Bănuș, 24.12.0. [Link](https://invent.kde.org/network/ktorrent/-/merge_requests/126))

{{< app-title appid="labplot" >}}

Labplot has been ported to Qt6 (Alexander Semke. [Link](https://invent.kde.org/education/labplot/-/merge_requests/574)).

{{< app-title appid="merkuro.calendar" >}}

Various small regressions in Merkuro have been fixed. (Claudio Cambra, 24.08.3. [Link 1](https://invent.kde.org/pim/merkuro/-/merge_requests/459), [link 2](https://invent.kde.org/pim/merkuro/-/merge_requests/460), [link 2](https://invent.kde.org/pim/merkuro/-/merge_requests/461), [link 3](https://invent.kde.org/pim/merkuro/-/merge_requests/462), [link 4](https://invent.kde.org/pim/merkuro/-/merge_requests/463), [link 5](https://invent.kde.org/pim/merkuro/-/merge_requests/464), [link 6](https://invent.kde.org/pim/merkuro/-/merge_requests/465))

{{< app-title appid="neochat" >}}

Navigation on mobile has been improved by loading the timeline only when requested. (James Graham, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1946))

The context menu on mobile has been fixed. (James Graham, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1947))

{{< app-title appid="spacebar" >}}

Sending SMS has been fixed. (Alistair Francis, Plasma 6.2.1. [Link](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/171))

A warning message is shown when Spacebar is not able to connect to its background service. (Devin Lin, Plasma 6.3.0. [Link](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/172))

The chat page now makes it easier to distinguish between "single contact conversations" and "groups" when creating a new chat. (Devin Lin, Plasma 6.3.0. [Link](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/169))

A fake ModemManager has been introduced to help developing Spacebar on laptops without a modem. (Devin Lin, Plasma 6.3.0. [Link](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/170))

{{< app-title appid="tokodon" >}}

"Content Warning" has been changed to "Content Notice" and the warning iconography has been removed. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/556))

Tokodon lets you remove and add users to your lists. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/557))

![](tokodon-users-management.png)

We added an "unread" notification count and you can now set your notifications to "read". (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/555))

A proper grid view for the media tab has been added in the profile page. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/540))

![](tokodon-gridview-notifications.png)

The wording of the private note field's label in the profile page has been improved. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/553))

Support for displaying authorship in preview cards has been added. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/559))

![](tokodon-authors.png)

A "News" and a "Users" sections have been added to the Explore page, and the "Tags" section has been renamed to "Hashtag". (Joshua Goins, 24.12.0. [Link 1](https://invent.kde.org/network/tokodon/-/commit/743f7745e552fa6857057b34c15f5fd47a62220b), [link 2](https://invent.kde.org/network/tokodon/-/commit/6c8de3552cef75f4897a3b36a3b0a862fb2ea15b), [link 3](https://invent.kde.org/network/tokodon/-/commit/01a1b65d881dc9cdef215e4a9632b0cd6f4c90a9))

![](tokodon-explore-users.png)

![](tokodon-explore-news.png)

Network settings have been removed from the login view as they are now available from the welcome page. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/95fe2fce2515f9fc75230fa069c3151794f1b4d6))

The "joined date" info has been added to the profile information. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/594adf2ba2d6a93b7595cd7a1b638d34600a3454))

A safety page has been added to the Tokodon settings to manage the list of muted and blocked users. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/0ae963762f93284f4591d0b27b7c5974c9b322e2))

The placeholders have been improved for when no posts are loaded. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/34f67749ca3572fe6f54a0b23b7e9d48ed20b8b0))

![](tokodon-placeholder.png)

Tokodon handles Mastodon 4.3.0's new (moderation warnings and [severance events](https://docs.joinmastodon.org/entities/RelationshipSeveranceEvent/)) notifications types. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/34f67749ca3572fe6f54a0b23b7e9d48ed20b8b0))

The media descriptions (also known as alt text) are now displayed in a popup when clicked. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/commit/5ee4a2199ed7c6764038a8d6db47bcc684d18ec7))

And many more improvements and cleanups. ([Link](https://invent.kde.org/network/tokodon/-/commits/master?ref_type=heads))

## Others

We updated the screenshots of many KDE games including Bomber, Granatier, Kapman, KAtomic, KBlocks and more. (Valentyn Bondarenko. [Link](https://invent.kde.org/websites/product-screenshots/-/merge_requests/76))

## ...And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](https://pointieststick.com) and be sure not to miss his [This Week in Plasma](https://pointieststick.com/2024/10/18/this-week-in-plasma-hardware-is-hard/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete view of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
