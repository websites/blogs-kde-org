---
title: 'This Week in KDE Apps: Python bindings'
discourse: thisweekinkdeapps
date: "2024-11-18T7:20:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@kde@floss.social'
---
![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week, we release the first beta of what will become KDE Gear 24.12.0. If your distro provides testing package, please help with testing. Meanwhile, and as part of the [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/), you can "Adopt an App" in a symbolic effort to support your favorite KDE app.

This week, we are particularly grateful to George Fakidis, [tmpod](https://tmpod.dev), Paxriel for showing their support for [Okular](https://apps.kde.org/okular/); Ian Lohmann, Anthony Perrett, Linus Seelinger and Nils Martens for [Dolphin](https://apps.kde.org/dolphin), Erik Bernoth for [Arianna](https://apps.kde.org/arianna/) and Daniel Lloyd-Miller and mdPlusPlus for [KDE Connect](https://apps.kde.org/kdeconnect/).

Any monetary contribution, however small, will help us cover operational costs, salaries, travel expenses for contributors and in general just keep KDE bringing Free Software to the world. So consider [donating](https://kde.org/fundraisers/yearend2024/) today!

Getting back to all that's new in the KDE App scene, let's dig in!

## Global Changes

[KWidgetsAddons](https://invent.kde.org/frameworks/kwidgetsaddons/), a collection of add-on widgets for QtWidgets, and [KUnitConversion](https://invent.kde.org/frameworks/kunitconversion/) now have Python bindings. (Manuel Alcaraz Zambrano, KDE Frameworks 6.9.0. [Link 1](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/258) and [link 2](https://invent.kde.org/frameworks/kunitconversion/-/merge_requests/48))

{{< alert title="Call to Action" >}}

KDE has over 70 frameworks, we need your help to add Python bindings to the relevant remaining frameworks. [See metatask.](https://invent.kde.org/teams/goals/streamlined-application-development-experience/-/issues/9)

{{</ alert >}}

The "About" page of Kirigami applications now provides helpful "Copy" button that lets you copy system information, which can be useful when filling a bug report. The same feature was also implemented for QtWidgets-based applications. (Carl Schwan, Kirigami Addons 1.6.0 and KDE Frameworks 6.9. [Link for Kirigami apps](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/302) and [link for QtWidget apps](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/255))

Additionally Joshua added icons to the "Getting involved", "Donate", and other actions for the Kirigami version. (Joshua Goins, Kirigami Addons 1.6.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/297))

![](aboutpage.png)

The "share" context menu of many applications can now copy the data to clipboard. (Aleix Pol Gonzalez, Frameworks 6.9.0. [Link](https://invent.kde.org/frameworks/purpose/-/merge_requests/115))

{{< app-title appid="alligator" >}}

Alligator now lets you bookmark your favorite posts. (Soumyadeep Ghosh, 25.04.0. [Link](https://invent.kde.org/network/alligator/-/merge_requests/111))

The selected feed will also now be highlighted correctly and the text of an article can now be selected and copied. (Soumyadeep Ghosh, 24.12.0. [Link](https://invent.kde.org/network/alligator/-/merge_requests/113) and [link 2](https://invent.kde.org/network/alligator/-/merge_requests/112))

{{< app-title appid="audiotube" >}}

Fix parsing certain playlists. (Eren Karakas, 24.12.0. [Link](https://invent.kde.org/multimedia/audiotube/-/merge_requests/145))

{{< app-title appid="kclock" >}}

Fix a crash of the Clock Daemon when waking up. (Devin Lin, 24.12.0. [Link](https://bugs.kde.org/496296))

{{< app-title appid="digikam" >}}

Digikam 8.5.0 is out! This releases improves the Face Management system, adds colored labels to identify important items, increases its list of supported languages to 61, and fixes over 160 bugs.

[Read the full announcement](https://www.digikam.org/news/2024-11-16-8.5.0_release_announcement/)

{{< app-title appid="dolphin" >}}

When Dolphin is started on a location which does not report a storage size (for example "remote", or "bluetooth") the status bar will no longer pointlessly show a empty storage size indicator for a split second before hiding it again. (Felix Ernst, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/854))

{{< app-title appid="gwenview" >}}

We fixed a bug where indexed-color or monochrome-palette images (e.g. from `pngquant`) would render with garbled colors or black and white noise when zoomed. (Tabby Kitten, 24.12.0. [Link](https://invent.kde.org/graphics/gwenview/-/merge_requests/293))

{{< app-title appid="itinerary" >}}

Itinerary's Matrix integration now uses encrypted Matrix rooms by default and Itinerary can now do session verification, which is going to be mandatory in the future. (Volker Krause, 25.04.0. [Link 1](https://invent.kde.org/pim/itinerary/-/commit/3f788118637df26377b85cace14740a705b864c7) and [link 2](https://invent.kde.org/pim/itinerary/-/commit/bc378c662e4d3205cfaab2a9064441d5a6578f5c)). Volker also fixed various small issues with the Matrix integration (too many to list them all) and backported these fixes for the 24.12.0 release.

{{< app-title appid="kate" >}}

It is now possible to disable the autocompletion popup which appears when you are just typing. (Waqar Ahmed, 25.04.0. [BUG 476620](https://bugs.kde.org/476620))

{{< app-title appid="kcalc" >}}

We redesigned the bit edit feature of KCalc. (Tomasz Bojczuk, 25.04.0. [Link](https://invent.kde.org/utilities/kcalc/-/merge_requests/161))

![](kcalc.png)

{{< app-title appid="kdenlive" >}}

Several Kdenlive effects got the capacity to animate their parameters with keyframes. (Bernd Jordan, Julius Künzel, and Massimo Stella, 25.04.0. [Link 1](https://invent.kde.org/multimedia/kdenlive/-/commit/74145f6a7f58d163df6711425e7b122baa9e61f3), [link 2](https://invent.kde.org/multimedia/kdenlive/-/commit/03a1ea6edb0e7784784ec36e2a873efaae1d6faf), [link 3](https://invent.kde.org/multimedia/kdenlive/-/commit/130b7f960e122b5f0bf9c6dc9d00889837243603) and [link 4](https://invent.kde.org/multimedia/kdenlive/-/commit/451a736f306edabd2523a1fe207367c5d9d18116))

{{< app-title appid="keysmith" >}}

Keysmith can now import OTPs from [andOTP](https://github.com/andOTP/andOTP)'s backup files. (Martin Reboredo, 25.04.0, [Link](https://invent.kde.org/utilities/keysmith/-/merge_requests/128))

{{< app-title appid="akonadi" >}}

Fixed a crash when migrating old iCal entries in Akonadi to be properly tagged. (Daniel Vrátil, 24.12.0. [Link](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/189))

Fix style of configuration dialogs for Akonadi agents on platforms other than Plasma. (Laurent Montel, 24.12.0. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/213))

Port away IMAP resource from KWallet and use QtKeychain instead. This ensures your email's credentials are correctly stored and retrieved on other platforms like Windows. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/167))

{{< app-title appid="kmail2" >}}

Reduce temporary memory allocation by 25% when starting KMail. If you are curious how, the merge requests are super interesting. (Volker Krause, 24.12.0. [Link 1](https://invent.kde.org/pim/akonadi-mime/-/merge_requests/18), [link 2](https://invent.kde.org/pim/akonadi/-/merge_requests/215), and [link 3](https://invent.kde.org/pim/messagelib/-/merge_requests/251))

{{< app-title appid="kodaskanna" >}}

Kodaskanna was ported to Qt6/KF6. (Friedrich W. H. Kossebau. [Link](https://invent.kde.org/graphics/kodaskanna/-/merge_requests/6))

{{< app-title appid="krdc" >}}

We added various options related to security of the RDP connection and the redirection of smartcards to the remote host. (Roman Katichev, 25.04.0. [Link](https://invent.kde.org/network/krdc/-/merge_requests/118))

![](krdc.png)

{{< app-title appid="kup" >}}

We rephrased all yes/no buttons in Kup's notifications to use more descriptive names. (Nate Graham, 25.04.0. [Link](https://invent.kde.org/system/kup/-/merge_requests/36))

{{< app-title appid="neochat" >}}

When receiving stickers with NeoChat, they will be displayed with a more appropriate size (256x256px). Same with custom emoticons, which are now displayed with the same height as the rest of the message. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2007))

![](neochat-emojis.png)

We don't show the filename underneath images anymore, and also make the download file dialog fill out the filename by default. (Joshua Goins, 24.12.0. [Link 1](https://invent.kde.org/network/neochat/-/merge_requests/1999) and [link 2](https://invent.kde.org/network/neochat/-/merge_requests/1981))

We redesigned the list of accounts in the welcome page. Now we show the display name and avatar of your accounts there, which makes it easier to recognize. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1986))

![](neochat-welcome.png)

We rearranged the room, file and message context menus. (Joshua Goins, 24.12.0. [Link 1](https://invent.kde.org/network/neochat/-/merge_requests/1996) and [link 2](https://invent.kde.org/network/neochat/-/merge_requests/1990))

{{< app-title appid="tokodon" >}}

Add "Open in Browser" action to profile pages. (Sean Baggaley, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/571))

Fix various issues on Android, most prominently ensure all icons required by Tokodon are packaged correctly. (Joshua Goins, 24.12.0)


## ... And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/11/16/this-week-in-plasma-discover-and-system-monitor-with-a-side-of-wine/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).

Thanks to Tobias Fella and Michael Mischurow for the proofreading.
