---
title: This Week in KDE Apps
subtitle: Chat Changes, Editor Enhancements and Audio Advancements 
discourse: thisweekinkdeapps
date: "2024-10-27T8:20:35Z"
authors:
 - carlschwan
 - paulbrown
image: thumbnail.png
categories:
 - This Week in KDE Apps
newsletter: 6cc7d914-24b5-4363-bd6a-cd70a3af79aa
fediverse: '@kde@floss.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week's changes and improvements cover a wide range of applications, from audio apps (including the classic Amarok, which is making a comeback) to Kate getting improvements to its integrated Git features.

In between, you have everything from new functionalities for note-taking utilities and media players, to upgrades in financial software and mobile apps.

Let's dig in!

{{< app-title appid="amarok" >}}

Tuomas Nurmi worked on making the codebase Qt6-compatible. (Tuomas Nurmi, [Link](https://invent.kde.org/multimedia/amarok/-/merge_requests/126))

{{< app-title appid="ark" >}}

Jin Liu disabled the "Compress to tar.gz/zip" service menu items in read-only directories. (Jin Liu, 24.12.0. [Link](https://invent.kde.org/utilities/ark/-/merge_requests/259))

{{< app-title appid="dolphin" >}}

You can now sort your videos by duration. (Somsubhra Bairi, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/844))

Eren Karakas added more standard actions (_Sort By_, _View Mode_, _Cut_ and _Copy_) to the context menu in the trash view. (Eren Karakas, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/840))

{{< app-title appid="elisa" >}}

Elisa now supports loading lyrics from `.lrc` files sitting alongside the song files. (Gary Wang, 24.12.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/629))

![](elisa.png)

Manuel Roth fixed the bug in which the metadata for webradio http streams was not getting displayed. (Manuel Roth, 24.12.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/628))

{{< app-title appid="haruna" >}}

You now have the option to open videos in full screen mode. (Rikesh Patel, [Link](https://invent.kde.org/multimedia/haruna/-/merge_requests/46))

{{< app-title appid="itinerary" >}}

Volker Krause was at the OSM Hack Weekend last week and worked on the support of MOTIS v2 API support in the [public transport client library](https://commits.kde.org/kpublictransport) used by [KDE Itinerary](https://apps.kde.org/itinerary). He also added a map view of an entire trip to Itinerary and the KPublicTransport demo application.

[Read more on his blog!](https://volkerkrause.eu/2024/10/26/osm-hack-weekend-october-2024.html)

{{< app-title appid="kate" >}}

In large repos, a `git status` update can be slow. The least we can do for the user is show that something is happening. Hence, now, if the git status is being refreshed you will see the refresh button become unclickable and start spinning. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1632))

In the project tree view, files will now show their status in git. The status is shown minimally, i.e. via a small circle displayed in front of the file name. If the file has been modified, the circle is red; if the file is staged, it's green. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1632))

![](kate.png)

We simplified the git panel by hiding the project combobox. The git panel will now show the status of the currently opened project. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1631))

![](kate-git.png)

We fixed the SQL plugin's SQL export being randomly ordered. (Waqar Ahmed, 24.08.3. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1627))

{{< app-title appid="kclock" >}}

KClock's timer now shows the remaining time instead of the elapsed time. (Zhangzhi Hu, 24.12.0. [Link](https://invent.kde.org/utilities/kclock/-/merge_requests/139))

{{< app-title appid="kmix" >}}

We fixed the _Audio Setup_ button, which didn't open the _System Settings Audio_ page correctly. (Sergio Basto, 24.12.0. [Link](https://invent.kde.org/multimedia/kmix/-/merge_requests/6))

{{< app-title appid="kmymoney" >}}

It's once again possible to download stock quotes from yahoo.com after they changed their output format. (Ralf Habacker, [Link](https://invent.kde.org/office/alkimia/-/merge_requests/49/))

Reports can now be exported as PDF and XML. (Ralf Habacker, KMyMoney 5.2.0. [Link 1](https://invent.kde.org/office/kmymoney/-/merge_requests/237), [link 2](https://invent.kde.org/office/kmymoney/-/merge_requests/236))

{{< app-title appid="koko" >}}

We improved the design of the properties panel. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/graphics/koko/-/merge_requests/143))

![](koko.png)

{{< app-title appid="kleopatra" >}}

The name of the "KWatchGnuPG" utility provided by Kleopatra has been updated to "GnuPG Log Viewer" (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/305)) and we gave it a new logo.

![logo of "GnuPG Log Viewer"](kwatchgnupg.png)

{{< app-title appid="klevernotes" >}}

KleverNotes' painting mode has been completely rewritten. It is now possible to add circles, rectangles, labels, and to choose the stroke size. The UI also uses a new floating toolbar. (Louis Schul, 1.2.0. [Link](https://invent.kde.org/office/klevernotes/-/merge_requests/129))

![](klevernotes.png)

We improved the animation when switching pages. (Luis Schul, 1.2.0. [Link](https://invent.kde.org/office/klevernotes/-/merge_requests/127))

The note preview in the appearance settings was simplified to only show the important parts. (Luis Schul, 1.2.0. [Link](https://invent.kde.org/office/klevernotes/-/merge_requests/125))

{{< app-title appid="kmail2" >}}

Fix a crash in the Exchange Web Services (EWS) backend. (Louis Moureaux, 24.08.3. [Link](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/186))

{{< app-title appid="krdc" >}}

We fixed sharing folders. (Fabio Bas, 24.08.3. [Link](https://invent.kde.org/network/krdc/-/merge_requests/114))

{{< app-title appid="merkuro.calendar" >}}

Claudio Cambra fixed adding and creating sub-todos (Claudio Cambra, 24.08.3. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/469) and [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/468))) and a bug that made clicking on the month view unreliable. (Claudio Cambra, 24.08.3, [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/466)).

We also added back the maps showing the location of individual events. This was disabled during the Qt6 migration and never enabled back afterwards. (Claudio Cambra, 24.08.3, [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/467))

{{< app-title appid="neochat" >}}

Support for [libQuotient 0.9](https://github.com/quotient-im/libQuotient/releases/tag/0.9.0) has been backported to NeoChat 24.08. This brings, among other things, cross-signing support and support for the Matrix 1.12 API, including most importantly content repo functionality switching to authenticated media. (James Graham, 24.08.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/1955))

{{< app-title appid="okular" >}}

Albert Astals fixed switching between pages in the single-page mode when using a mouse with a "high resolution" scroll wheel. (Albert Astals Cid, 24.12.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1064))

You can now use any image type as a signature background. (Sune Vuorela, 24.12.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1055))

We removed the last CHM support mention in Okular and on the website. CHM support was dropped when transitioning to the Qt6 version. (Albert Astals Cid, 24.12.0. [Link 1](https://invent.kde.org/graphics/okular/-/merge_requests/1062), [link 2](https://invent.kde.org/websites/okular-kde-org/-/merge_requests/29))

{{< app-title appid="zanshin" >}}

Fixed an issue where projects would be displayed twice when toggling on and off their data source. (David Faure, 24.08.3. [Link](https://invent.kde.org/pim/zanshin/-/merge_requests/49))

## And all this too...

Justin Zobel fixed various appstream files to use the new way of declaring the developer's name. (Justin Zobel, [KRuler](https://invent.kde.org/graphics/kruler/-/merge_requests/31), [Gwenview](https://invent.kde.org/graphics/gwenview/-/merge_requests/301), [KEuroCalc](https://invent.kde.org/utilities/keurocalc/-/merge_requests/6), ...)

We ported various projects to use declarative QML declaration for better [maintainance and performance](https://www.kdab.com/10-tips-to-make-your-qml-code-faster-and-more-maintainable/) (Carl Schwan, [Koko](https://invent.kde.org/graphics/koko/-/merge_requests/144), [Francis](https://invent.kde.org/utilities/francis/-/merge_requests/21), [Kalk](https://invent.kde.org/utilities/kalk/-/merge_requests/97)).

## ... And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](https://pointieststick.com) and be sure not to miss his [This Week in Plasma](https://pointieststick.com/2024/10/26/this-week-in-plasma-all-screens-all-the-time/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
