---
title: This Week in KDE Apps
date: 2024-09-22
discourse: thisweekinkdeapps
authors:
  - carlschwan
  - nategraham
categories:
 - This Week in KDE Apps
marble:
 - name: Marble Maps
   url: marble-maps.png
 - name: Additional information
   url: marble-maps-info.png
 - name: Layers Options in Marble Maps
   url: marble-maps-layers.png

itinerary:
 - name: Itinerary Trip List
   url: itinerary-trips-list.png
 - name: Itinerary Trip Details
   url: itinerary-single-trip.png
---

Welcome to the second post in our "This Week in KDE Apps" series! If you missed it
we just announced this new series [last
week](https://blogs.kde.org/categories/this-week-in-kde-apps/) and our goal is
to cover as much as possible of what's happening in the KDE world and complete
[Nate's This Week in Plasma](https://pointieststick.com/2024/09/20/this-week-in-plasma-polishing-like-mad/).

This week we had a new Ruqola, KDE's Rocket.chat client, release and a new
GCompris release. There is also news regarding NeoChat, KDE's Matrix chat client;
Itinerary, the travel assistant that lets you plan all your trips; the Dolphin
file browser; Marble, KDE's map application application; the Okular document
view and more.

Let's get started!

## Dolphin

Dolphin now ensures that the Trash always correctly shows all trashed files of all connected storage devices, even if they get dynamically connected or disconnected. (Akseli Lahtinen, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=493247))

Made the lists of recent files and locations update more reliably. (Méven Car, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=437382))

## Filelight

Resolved a bug that caused the graphs to sometimes be mis-rendered until hovered with the pointer. (Harald Sitter, 24.12.0. [Link](https://bugs.kde.org/show_bug.cgi?id=492326))

## Itinerary

Itinerary can now handle `geo://` URLs by opening the "Plan Trip" page with a
pre-selected arrival location. This is supported both on Android and Linux.
(David Redondo, 24.12.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/323))

Itinerary now defaults to showing the new two-level trips/timeline view. (Volker Krause, 24.12.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/328))

{{< screenshots name="itinerary" >}}

Trip groups can now be restored from backups. (Volker Krause, 24.12.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/327))

## KDE PIM

Fixed a crash when auto-discovery of or connection to Exchange Web Resources has failed (Daniel Vrátil, 24.08.2.
[Link](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/182))

## Okular

Implemented support for more types of items in comboboxes of PDF forms. (Pratham Gandhi, 24.12.0.
[Link](https://invent.kde.org/graphics/okular/-/merge_requests/1027))

Improved the speed and correctness of printing for the common case of not needing to rasterize the document and not needing to print annotations. (Oliver Sander, Albert Astals Cid, and Nicolas Fella sponsored by TU Dresden, 24.12.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1046))

Improved the UX of digitally signing a document (Nicolas Fella sponsored by TU Dresden, 24.12.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1005))

## Spectacle

Improved visual quality for screenshots taken at non-fractional scale factors. (Noah Davis, 24.08.2. [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/403))

## Marble

Marble was ported to Qt6. (Gilles Caulier & Carl Schwan, 24.12.0. [Link](https://invent.kde.org/education/marble/-/merge_requests/131))

The Kirigami UI — which was last updated in 2017 — was significantly rewritten and modernized. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/education/marble/-/merge_requests/135)).

{{< screenshots name="marble" >}}

## NeoChat

The NeoChat team meet at the [Matrix Conference in Berlin](https://2024.matrix.org/) which ended up being productive! Learn more at Carl's [mastodon post](https://kde.social/@carl/113171972627566660).

Fix two semi-common crashes reported to Sentry. (Tobias Fella & James Graham, 24.08.2. [Link 1](https://invent.kde.org/network/neochat/-/merge_requests/1898), [Link 2](https://invent.kde.org/network/neochat/-/merge_requests/1897), [Link 3](https://invent.kde.org/network/neochat/-/merge_requests/1899))

An F-Droid build is again available in [KDE's F-Droid repository](https://community.kde.org/Android/FDroid). (Tobias Fella & Volker Krause)

Fixed various visual papercuts for NeoChat on Android and Plasma Mobile. (Carl Schwan, 24.08.2 and 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1909), [Link](https://invent.kde.org/network/neochat/-/merge_requests/1910), [Link](https://invent.kde.org/network/neochat/-/merge_requests/1913), [Link](https://invent.kde.org/network/neochat/-/merge_requests/1905), ...)

## KDE Connect

The Ping and Find Remote Device plugins were ported to Kotlin. (TPJ Schikhof, [Link 1](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/478), [Link 2](https://invent.kde.org/network/kdeconnect-android/-/merge_requests/476))

## LabPlot

Add possibility to apply functions on curves directly on the plot. This makes it is possible to, for example, calculate the differences between curves, scale or shift curves, etc. (Martin Marmsoler. [Link](https://invent.kde.org/education/labplot/-/merge_requests/537))

![](labplot.png)

## Kate

Improved the visuals of Kate's inline code formatting tooltips. The content of these tooltips can now also be displayed in a special context tool view which can be enabled in the `Behavior` settings (Karthik Nishanth, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1557))

![](kate-text-hint.png)

## GCompris

GCompris 4.2 is out with some bug fixes and graphical improvements for multiple activities. More information is available in the [release announcement](https://www.gcompris.net/newsall-en.html#2024-09-20).

## Ruqola

Ruqola 2.3.0 — the KDE Rocket.chat client — is out with an administrator mode, a new welcome page, and better support for the custom markdown syntax of Rocket.chat. More information is available in the [release announcement](https://blogs.kde.org/2024/09/19/ruqola-2.3.0/).

## Other

Eamonn Rea made many Kirigami application remember their size across launches:
* [AudioTube](https://invent.kde.org/multimedia/audiotube/-/merge_requests/141)
* [Kontrast](https://invent.kde.org/accessibility/kontrast/-/merge_requests/45)
* [KRecorder](https://invent.kde.org/utilities/krecorder/-/merge_requests/39)
* [KWeather](https://invent.kde.org/utilities/kweather/-/merge_requests/119)

## ...And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check
out [Nate's blog about Plasma](https://pointieststick.com) and [KDE's
Planet](https://planet.kde.org), where you can find more news from other KDE
contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped achieve that status. As we grow, it’s going to be
equally important that your support become sustainable.

We need you for this to happen. You can help KDE by becoming an active
community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE; you are not a number or a cog
in a machine! You don’t have to be a programmer, either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general help KDE continue bringing Free
Software to the world.
