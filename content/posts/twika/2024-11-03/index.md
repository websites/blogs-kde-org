---
title: This Week in KDE Apps
subtitle: Dolphin's accessibility, Itinerary's travelling aids, and a bunch of new upcoming KDE apps 
discourse: thisweekinkdeapps
date: "2024-11-03T8:20:35Z"
authors:
 - carlschwan
 - paulbrown
image: thumbnail.png
categories:
 - This Week in KDE Apps
newsletter: 6cc7d914-24b5-4363-bd6a-cd70a3af79aa
fediverse: '@kde@floss.social'
gcompris:
  - url: gcompris1.png
    name: GCompris' main Sketch view
  - url: gcompris2.png
    name: Tools configuration
  - url: gcompris3.png
    name: Color chooser
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

In this issue we discover what developers have been doing to make Dolphin, KDE's most popular (but not only!) file explorer, more accessible. We also take a look at all the new services now integrated into Itinerary that will help you on your travels, the new features for Kate that programmers will enjoy, improvements to Kleopatra to help you manage your certificates and the encryption of your messages, and the flurry of new applications that will soon be available in KDE's software catalog.

This week, we also kicked off our [2024 end-of-year fundraiser](https://kde.org/fundraisers/yearend2024/) just in time for Halloween! Any monetary contribution, however small, will help us cover operational costs, salaries, travel expenses for contributors and in general just keep KDE bringing Free Software to the world. So consider [doing a donation](https://kde.org/fundraisers/yearend2024/) today!

Let's dig in!

{{< app-title appid="dolphin" >}}

We improved the keyboard navigation of Dolphin, as pressing <kbd>Ctrl</kbd>+<kbd>L</kbd> multiple times will switch back and forth between focusing and selecting the location bar path and focusing the view. Pressing <kbd>Escape</kbd> in the location bar, will now move the focus to the active view (Felix Ernst, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/845)).

Speaking of accessibility, the accessibility of the main view of Dolphin was completely overhauled to make it work with screen readers. This work was funded by NGI0 Entrust Fund, a fund established by NLnet with financial support from the European Commission's Next Generation Internet programme (Felix Ernst, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/837)).

Another change is that Dolphin will now store its view properties inside the extended file attributes instead of creating hidden `.directory` files when possible (Méven Car, 24.12.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/573)).

{{< app-title appid="digikam" >}}

digiKam is KDE's powerful photo management software for both professional and aficionado photographers.

Michael Miller fixed an issue where faces from the facial recognition feature were not deleted when a user untagged or deleted a face (Michael Miller. [Link](https://invent.kde.org/graphics/digikam/-/merge_requests/308)).

{{< app-title appid="elisa" >}}

Jack Hill fixed a few issues related to the lyrics feature. Clicking on the Lyrics button now takes you to the correct lyric and not the previous one, and the last line of the lyrics is not displayed completely (Jack Hill, 24.12.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/635)).

Jack also reworked the metadata dialogs to be more intuitive and correct some bugs (Jack Hill, 24.12.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/633)).

{{< app-title appid="gcompris" >}}

GCompris, the educational software suite, got a new activity: Sketch! This fun tool lets children express their creativity and draw beautiful artworks (Timothée Giet. [Link](https://invent.kde.org/education/gcompris/-/merge_requests/213)).

{{< screenshots name="gcompris" >}}

{{< app-title appid="itinerary" >}}

Itinerary now supports extracting reservations from planway.com, flight tickets from VietJet Air and train tickets from the Thai state railway. Additionally, dates from day-specific train tickets from NS (Nederlandse Spoorwegen) are now correctly parsed (Volker Krause, 24.08.3. [Link 1](https://invent.kde.org/pim/kitinerary/-/commit/7b6fc1297f5398d62ea03e9b95bf4fec019ec893), [link 2](https://invent.kde.org/pim/kitinerary/-/commit/a8501a190379c0a61404a0502a8fc326ad0c63a7), [link 3](https://invent.kde.org/pim/kitinerary/-/commit/e290f715f15222df4777e38e66f1339c823794b3)), and, while on an NS intercity train, you can also access the live journey information provided by the onboard WiFi (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/libraries/kpublictransport/-/merge_requests/86)).

{{< app-title appid="kate" >}}

Kate continues to become more and more developer friendly with the changes made by Christoph Cullmann where the order of the tabs is correctly restored when restoring a previous session (Christoph Cullmann, 24.08.3. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1618)), and the options of the LSP Symbols are more easily discoverable as they are not only available via a context menu, but also within a menu button at the top (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1633)).

![](kate-outline.png)

Benjamin Port fixed the Appium UI tests and reenabled them on the CI (Benjamin Port, 28.03.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1635)).

{{< app-title appid="kdenlive" >}}

Kdenlive is KDE's full-featured video editor, which now lets you resize multiple items on the timeline at the same time. (Jean-Baptiste Mardelle, 24.12.0 [Link](https://invent.kde.org/multimedia/kdenlive/-/merge_requests/550)).

{{< app-title appid="kleopatra" >}}

We redesigned Kleopatra's notepad and sign encrypt dialog. In the notepad, the text editor and the recipients view are also now side by side (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/289)).

Additionally, Tobias worked on the notepad's result messages and error dialogs to make them clearer. (Tobias Fella, 24.12.0 [Link 1](https://invent.kde.org/pim/kleopatra/-/merge_requests/293), [link 2](https://invent.kde.org/pim/kleopatra/-/merge_requests/307)).

![](kleopatra-notepad.png)

![](kleopatra-signencryptdialog.png)

Tobias also fixed a crash on non-kwin Wayland compositors (Tobias Fella, 24.08.3. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/310)), and Kleopatra has a [new website](https://apps.kde.org/kleopatra/) (Carl Schwan. [Link](https://invent.kde.org/websites/apps-kde-org/-/merge_requests/108)).

[![](kleo-website.png)](https://apps.kde.org/kleopatra/)

{{< app-title appid="kongress" >}}

Kongress is an app which helps you navigate conferences and events.

The newest version will display more information about events in the event list. This includes whether the event is in your bookmarked events and the locations within the event (e.g. the rooms) (cah fof pai, 24.12.0. [Link](https://invent.kde.org/utilities/kongress/-/merge_requests/66/)).

![](kongress.png)

Speaking of conferences, multiple KDE people will be at the [Chaos Communication Congress (38c3)](https://events.ccc.de/congress/2024/infos/index.html) in Hamburg this December! Come by and say hello!

{{< app-title appid="kstars" >}}

KStars is KDE's stargazing app that also helps you control your telescope for astrophotography.

We removed the "Simulate Eyepiece View" feature and stripped down `EyepieceField`. The reason is the offerings of the eyepiece view feature have already been superseded by two more powerful and easier-to-use features in KStars: the HiPS Overlay and the "Views" feature (Akarsh Simha, 3.7.4. [Link](https://invent.kde.org/education/kstars/-/merge_requests/1355)).

{{< app-title appid="kwave" >}}

Mark Penner wrote a [blog post](https://markpenner.space/blog/2024-10-28-kwave-update/) about his work on KWave.

![](kwave.png)

{{< app-title appid="labplot" >}}

LabPlot is KDE's complete suite of data analysis and visualisation tools.

The LabPlot developers added the `RAND_MAX` programming constants for GSL (GNU Scientific Library) support. (Martin Marmsoler [Link](https://invent.kde.org/education/labplot/-/merge_requests/586)), and rewrote the AsciiFilter to increase the parsing speed, like when parsing livedata from an mqtt feed (Martin Marmsoler. [Link](https://invent.kde.org/education/labplot/-/merge_requests/582)).

Kuntal Bar also fixed various issues with HiDPI screens (Kuntal Bar, [Link](https://invent.kde.org/education/labplot/-/merge_requests/584)).

{{< app-title appid="marknote" >}}

Marknote lets you create rich text notes and easily organise them into notebooks.

The icons in the app are now correctly displayed when running Marknote on other platforms, like Windows (Gary Wang, [Link](https://invent.kde.org/office/marknote/-/merge_requests/51)).

{{< app-title appid="ruqola" >}}

Ruqola, KDE's [Rocket Chat](https://www.rocket.chat/) client, received various fixes for its login, logout and network disconnection features (David Faure & Andras Mantia [Link 1](https://invent.kde.org/network/ruqola/-/merge_requests/172), [link 2](https://invent.kde.org/network/ruqola/-/merge_requests/171), [link 3](https://invent.kde.org/network/ruqola/-/merge_requests/166) and [link 4](https://invent.kde.org/network/ruqola/-/merge_requests/165)), and the unread message bar now uses buttons instead of more subtle links (Joshua Goins. [Link](https://invent.kde.org/network/ruqola/-/merge_requests/147)).

{{< app-title appid="spectacle" >}}

Spectacle is the utility for taking screenshots and screencasts of your desktop and apps. We fixed an issue where Spectacle would take a screenshot of itself (Noah Davis, 24.08.3. [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/414)).

## Other Stuff

The FormCard components used by most Kirigami application are now more compact on the desktop (Carl Schwan, Kirigami Addons 1.6.0 [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/292)).

{{< img-comparison-slider before="formcard-compact-old.png" after="formcard-compact.png" >}}

The About Page provided by the FormCard component now displays more information about the components used by the application (e.g. Qt, KDE Frameworks) (Carl Schwan, Kirigami Addons 1.6.0. [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/293)).

![](aboutpage.png)

## Playground

This section contains news about non released applications.

### Arkade

Arkade, a collection of games written in QML, was updated to Qt6 (Carl Schwan. [Link](https://invent.kde.org/games/arkade/-/commit/af3defdda17b6e5f357840520d1725f304af7a3e)).

![](arkade.png)

### Whale

Claudio Cambra ported Whale, a QML based file manager and explorer, to Qt6 (Claudio Cambra. [Link](https://invent.kde.org/carlschwan/whale/-/merge_requests/13)). Claudio also added a miller columns view to Whale (Claudio Cambra. [Link](https://invent.kde.org/carlschwan/whale/-/merge_requests/16)), and implemented navigation history (Claudio Cambra. [Link](https://invent.kde.org/carlschwan/whale/-/merge_requests/19)).


![](whale.png)


## And all this too...

Justin Zobel fixed various appstream files to use the new way of declaring the developer's name (Justin Zobel, [KRuler](https://invent.kde.org/graphics/kruler/-/merge_requests/31), [Gwenview](https://invent.kde.org/graphics/gwenview/-/merge_requests/301), [KEuroCalc](https://invent.kde.org/utilities/keurocalc/-/merge_requests/6), ...).

We ported various projects to use declarative QML declaration for better [maintainance and performance](https://www.kdab.com/10-tips-to-make-your-qml-code-faster-and-more-maintainable/) (Carl Schwan, [Koko](https://invent.kde.org/graphics/koko/-/merge_requests/144), [Francis](https://invent.kde.org/utilities/francis/-/merge_requests/21), [Kalk](https://invent.kde.org/utilities/kalk/-/merge_requests/97)).

## ... And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](https://blogs.kde.org/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](https://blogs.kde.org/2024/11/02/this-week-in-plasma-spoooooky-ooooooooom-notifications/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
