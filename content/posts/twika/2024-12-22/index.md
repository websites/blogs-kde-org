---
title: 'This Week in KDE Apps: Search in Merkuro Mail, Tokodon For Android, LabPlot new documentation and more'
discourse: thisweekinkdeapps
date: "2024-12-22T12:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@kde@floss.social'
okular:
 - name: Signature Properties
   url: okular-signature.png
 - name: Certificate Viewer
   url: okular-certificate.png
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

{{< app-title appid="audiotube" >}}

AudioTube now shows synchronized lyrics provided by [LRCLIB](https://lrclib.net/). This automatically falls back to normal lyrics if synced lyrics are not available. (Kavinu Nethsara, 25.04.0. [Link](https://invent.kde.org/multimedia/audiotube/-/merge_requests/148))

{{< app-title appid="dolphin" >}}

Quickly renaming multiple files by switching between them with the keyboard arrow keys now correctly starts a renaming of the next file even if a sorting change moved it. (Ilia Kats, 25.04.0. [Link](https://invent.kde.org/system/dolphin/-/merge_requests/863))

Fixed a couple of regressions in the 24.12.0 release. (Akseli Lahtinen, 24.12.1. [Link 1](https://bugs.kde.org/show_bug.cgi?id=497555), [link 2](https://bugs.kde.org/show_bug.cgi?id=495878), [link 3](https://bugs.kde.org/show_bug.cgi?id=497021))

{{< app-title appid="itinerary" >}}

Improved the touch targets of the buttons in the bottom drawer which appears on mobile. (Carl Schwan, 24.05.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/367))

![](itinerary-bottom-drawer.png)


{{< app-title appid="akonadi" >}}

Improve the stability of changing tags. Now deleting a tag will properly remove it from all items. (Daniel Vrátil, 24.12.1. [Link 1](https://invent.kde.org/pim/akonadi/-/merge_requests/220) and [link 2](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/191))

{{< app-title appid="kmail2" >}}

The tooltip of your folder in KMail will now show the absolute space quota in bytes. (Fabian Vogt, 25.04.0. [Link](https://invent.kde.org/pim/akonadi/-/merge_requests/219))

![](kmail-quota.png)

{{< app-title appid="kmymoney" >}}

An initial port of KMyMoney for Qt6 was merged. (Ralf Habacker. [Link](https://invent.kde.org/office/kmymoney/-/merge_requests/240))

{{< app-title appid="krita" >}}

Krita has a new plugin for fast sketching. You can find more about this on their [blog post](https://krita.org/en/posts/2024/fast_sketch_plugin/).

{{< app-title appid="ktorrent" >}}

Added the support for getting IPv6 peers from peer exchange. (Jack Hill, 25.04.0. [Link](https://invent.kde.org/network/libktorrent/-/merge_requests/69))

{{< app-title appid="labplot" >}}

We now show more plot types in the "Add new plot" context menu. (Alexander Senke. [Link](https://invent.kde.org/education/labplot/-/merge_requests/624))

LabPlot has [announced](https://labplot.org/2024/12/20/new-labplot-user-documentation/) a new [dedicated user manual page](https://docs.labplot.org/). 

{{< app-title appid="okular" >}}

We improved how we are displaying the signature and certificate details in the mobile version of Okular. (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1089))

{{< screenshots name="okular" >}}

When selecting a certificate to use when digitally signing a PDF with the GPG backend, the fingerprints are rendered more nicely. (Sune Vuorela, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1092))

It's now possible to choose a custom default zoom level in Okular. (Wladimir Leuschner, 25.04.0. [Link](https://invent.kde.org/graphics/okular/-/merge_requests/1079))

![](okular-zoom.png)

{{< app-title appid="merkuro.mail" >}}

Merkuro Mail now lets you search across your emails with a full text search. (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/493))

![](merkuro-search.png)

Additionally, the Merkuro Mail sidebar will now remember which folders were collapsed or expanded as well as the last selected folder across application restarts. (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/494))

{{< app-title appid="powerplant" >}}

We started the ["KDE Review" process for PowerPlant](https://invent.kde.org/utilities/powerplant/-/issues/5), so expect a release in the comming weeks.

We added support for Windows and Android. (Laurent Montel, 1.0.0. [Link 1](https://invent.kde.org/utilities/powerplant/-/merge_requests/41), [link 2](https://invent.kde.org/packaging/craft-blueprints-kde/-/merge_requests/1099) and [link 3](https://invent.kde.org/utilities/powerplant/-/merge_requests/42))

{{< app-title appid="ruqola" >}}

Ruqola 2.4.0 is out. You can now mute/unmute other users, cleanup the room history and more. Read the [full announcement](https://blogs.kde.org/2024/12/18/ruqola-2.4.0/).

{{< app-title appid="tokodon" >}}

This week, Joshua spent some time improving Tokodon for mobile and
in particular for Android. This includes performance optimization, adding
missing icons and some mobile specific user experience improvements. (Joshua Goins, 25.04.0. [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/637/), [link 2](https://invent.kde.org/network/tokodon/-/merge_requests/639) and [link 3](https://invent.kde.org/network/tokodon/-/merge_requests/638)). A few more improvements for Android, like proper push notifications via unified push, are in the work.

Joshua also improved the draft and scheduled post features, allowing now to discard scheduled posts and drafts and showing when a draft was created. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/631))

We also added a keyboard shortcut configuration page in Tokodon settings. (Joshua Goins and Carl Schwan, 25.04.0. [Link 1](https://invent.kde.org/network/tokodon/-/merge_requests/634) and [link 2](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/324))

![](tokodon-screenshots.png)

Finally, we created a new server information page with the server rules and made the existing announcements page a subpage of it. Speaking of announcements, we added support for the announcement's emoji reactions. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/633))

![](tokodon-rules.png)

![](tokodon-emojis.png)

{{< app-title appid="washipad" >}}

WashiPad was ported to Kirigami instead of using its own custom QtQuick components. (Carl Schwan. [Link](https://invent.kde.org/graphics/washipad/-/merge_requests/6))

![](washipad.png)

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2024/12/21/this-week-in-plasma-end-of-year-bug-fixing/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.


## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).