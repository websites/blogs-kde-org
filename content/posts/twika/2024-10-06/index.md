---
title: This Week in KDE Apps
subtitle: Marble gets an update, KDE Connect gets a speed boost, and Kate gets all fluttery
discourse: thisweekinkdeapps
date: "2024-10-06T8:30:35Z"
authors:
 - carlschwan
 - genericity
 - nategraham
 - paulbrown
image: thumbnail.png
categories:
 - This Week in KDE Apps
marble:
 - url: marble-route.png
   name: Marble Route Editor
 - url: marble-route-map.png
   name: Marble Routing displayed
kweather:
 - url: kweather-welcome.png
   name: Welcome Page
 - url: kweather-appeareance.png
   name: Appearance Setup
 - url: kweather-location.png
   name: Location Setup
dolphin:
 - url: kio-checksums.png
   name: Checksums tab
 - url: kio-permissions.png
   name: Permissions tab
---

![](thumbnail.png)

Welcome to a new issue of “This Week in KDE Apps”! In case you missed it, we announced this series [a few weeks ago](https://blogs.kde.org/categories/this-week-in-kde-apps/), and our goal is to cover as much as possible of what's happening in the world of KDE apps and supplement [Nate's This Week in Plasma](https://pointieststick.com/2024/10/04/this-week-in-plasma-6-2-is-nigh/) published yesterday.

This week we had new releases of Tellico and Krita. We are also covering news
regarding KDE Connect, the link between all your devices; Kate, the KDE
advanced text editor; Itinerary, the travel assistant that helps you plan all
your trips;  Marble, KDE's map application; and more.

Let's get started!

{{< app-title appid="dolphin" >}}

Dolphin now uses `ripgrep-all` or `ripgrep` for content search when Baloo indexing
is disabled. [Detailed information](https://blogs.kde.org/2024/10/02/use-ripgrep-all-/-ripgrep-to-improve-search-in-dolphin/)
(Jin Liu, 24.12.0. [Link](https://invent.kde.org/network/kio-extras/-/merge_requests/374))

The checksum and permissions tab in the property dialog used by Dolphin and other KIO-enabled applications is now more consitent with the other tabs. (Thomas Duckworth, Frameworks 6.8. [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1644))

{{< screenshots name="dolphin" >}}

{{< app-title appid="kaidan" >}}

Kaidan, KDE's XMPP instant messaging app, improves support for group chats. (Melvin Keskin, [Link](https://invent.kde.org/network/kaidan/-/merge_requests/1161))

{{< app-title appid="kate" >}}

Kate adds out of the box support for debugging Flutter projects. (Waqar Ahmed, 24.12.0. [Link 1](https://invent.kde.org/utilities/kate/-/merge_requests/1607), [link 2](https://invent.kde.org/utilities/kate/-/merge_requests/1606))

The option to 'Reopen latest closed documents' has been added to the tab context menu. (Waqar Ahmed, 24.12.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1600))

{{< app-title appid="kdenlive" >}}

KDE e.V. and Kdenlive have posted two job offers for contractors to work on Kdenlive. [Will this be your opportunity to contribute to KDE and get paid too?](https://ev.kde.org/2024/10/04/2024-10-05-kde-e-v-kdenlive-jobs/)

{{< app-title appid="kdeconnect" >}}

KDE Connect starts up much faster on macOS — startup time has gone from 3s to 100ms! (Albert Vaca Cintora, 24.12.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/725))

{{< app-title appid="kleopatra" >}}

Kleopatra makes its decryption errors easier to understand when content was encrypted with a certificate you don't have. (Tobias Fella, 24.12.0. [Link](https://invent.kde.org/pim/kleopatra/-/merge_requests/283))

{{< app-title appid="krita" >}}

Krita 5.2.6 is out and fixes a critical issue that popped up in last week's release. [More information](https://krita.org/en/posts/2024/krita-5-2-6-released/).

{{< app-title appid="krusader" >}}

Krusader has been migrated to Qt6 and KF6. (Alex Bikadorov, 3.0.0. [Link](https://invent.kde.org/utilities/krusader/-/merge_requests/137))

{{< app-title appid="kstars" >}}

KStars 3.7.3 is out with exciting features for astrophotography buffs. You're going to want to update if you're using multiple cameras with per-camera targeting and scheduling, leader-and-follower jobs, and focus synchronization. [Read more here!](https://knro.blogspot.com/2024/10/kstars-v373-is-released.html)

{{< app-title appid="kweather" >}}

KWeather removes the "Add current location" button, as it doesn't work anymore since the shutdown of Mozilla's location service. (Devin Lin, 24.08.2. [Link](https://invent.kde.org/utilities/kweather/-/merge_requests/122))

The setup wizard has been overhauled. (Devin Lin, 24.08.2. [Link](https://invent.kde.org/utilities/kweather/-/merge_requests/123))

{{< screenshots name="kweather" >}}

{{< app-title appid="itinerary" >}}

A new bi-monthly blog post about Itinerary and the infrastructure behind it is out: [August/September in KDE Itinerary](https://www.volkerkrause.eu/2024/10/03/kde-itinerary-august-september-2024.html)

Itinerary now extracts membership ids in German-language Eurostar (Thalys) tickets (Luca Weiss, 24.08.2. [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/131))

It can extract seat reservation data from SBB QR codes (Volker Krause, 24.08.2. [Link](https://invent.kde.org/pim/kitinerary/-/commit/7324f1a0473c982caa9342d18eab987c15254edf))

If you are arranging accommodations, Itinerary can handle German language variants of NH Hotels booking confirmations (Volker Krause, 24.08.2. [Link](https://invent.kde.org/pim/kitinerary/-/commit/8a8cbeb7b4f232f9fcf14985623a02faf7ca690b))

{{< app-title appid="labplot" >}}

The Color Maps Browser now has multiple view modes. Including one that shows
detailed information about the used colors in the color map and that also
allows to copy those values. (Alexander Semke, [Link](https://invent.kde.org/education/labplot/-/merge_requests/572))

Added a new visualization type: Run Chart, (Alexander Semke, [Link](https://invent.kde.org/education/labplot/-/issues/1007))

![](labplot-view-mode.png)

{{< app-title appid="neochat" >}}

NeoChat has a fix for a frequent and random crash on Android caused by receiving a notification. (James Graham, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1924))

The hover actions for the messages are now more reliable. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/1904))

{{< app-title appid="marble" >}}

Marble Behaim got a new logo, similar to the Marble Maps logo. (Mathis Brüchert, 24.12.0. [Link](https://invent.kde.org/education/marble/-/commit/7e96b14ea07b78d08a7b4efa3753ab64ed8da67d))

Marble Maps routing functionality was ported to Qt6 and redesigned. (Carl Schwan, 24.12.0. [Link](https://invent.kde.org/education/marble/-/commit/5419e17fde022c512902ed7840fe1ee43de8b64f))

{{< screenshots name="marble" >}}

{{< app-title appid="spectacle" >}}

Spectacle fixed a crash when saving while the system's timezone is misconfigured (Noah Davis, 24.08.2. [Link](https://invent.kde.org/graphics/spectacle/-/merge_requests/406))

{{< app-title appid="tellico" >}}

Tellico, the KDE app that helps you manage all your collecions, is out with version 4.0.1. This version includes fixes for Qt6. [More information](https://tellico-project.org/tellico-4-0-1-released/).

{{< app-title appid="tokodon" >}}

Tokodon fetches public servers and displays them in a list for registration. The list is fetched from [joinmastodon.org](https://joinmastodon.org) and more filtering options will be added later. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/531))

![List of servers](tokodon-pick-a-server.png)

Instead of wrapping all the tags for a post, they are now made scrollable. (Joshua Goins, 24.12.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/470))

## Apps on Windows

KDE Apps on windows now have better looking tooltips and menus without black corners.
(Carl Schwan, Breeze 6.2.1. [Link](https://invent.kde.org/plasma/breeze/-/merge_requests/493))

## Third Party Applications

To get your application mentioned here. Please contact us on [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).

{{< app-title appid="kraft" >}}

Kraft is a desktop app making it easy to create offers and invoices quickly and beautifully in small companies. Version 1.2.2 was just released and contains some small bug fixes. This is the last release before Kraft 2.0.
[More information](https://dragotin.codeberg.page/posts/kraft_122/kraft_122/).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check
out [Nate's blog about Plasma](https://pointieststick.com) and [KDE's
Planet](https://planet.kde.org), where you can find more news from other KDE
contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active
community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE; you are not a number or a cog
in a machine! You don’t have to be a programmer, either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general help KDE continue bringing Free
Software to the world.

To get your application mentioned here. Please let us know in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
