---
title: 'This Week in KDE Apps: Usability improvements, new features, and updated apps'
discourse: thisweekinkdeapps
date: "2025-01-12T12:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week we look at the usability improvements landing in Alligator, Dolphin, and Itinerary; new features for KMyMoney, Tokodon and NeoChat; and updated versions of Amarok and Skrooge.

{{< app-title appid="alligator" >}}

You can now mark one feed or all feeds as read (Mark Penner, 25.04.0. [Link](https://invent.kde.org/network/alligator/-/merge_requests/126)),
and save the settings when the application is suspended (Mark Penner, 24.12.3. [Link](https://invent.kde.org/network/alligator/-/merge_requests/125)).

{{< app-title appid="amarok" >}}

Amarok 3.2.1 is out with a more complete Qt6 support and some small UI bug fixes. You can find the full announcement on [Amarok's development Squad blog](/2025/01/11/amarok-3.2.1-released/).

Note that this version is still marked as experimental.

{{< app-title appid="arianna" >}}

Arianna will once again remember the current reading progress of your books with the new backend. (Ryan Zeigler. 25.04.0. [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/64))

It is now possible to add multiple books in your library at the same time. (Onuralp SEZER, 25.04.0. [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/66))

{{< app-title appid="dolphin" >}}

Dolphin now visually elides the middle portion of long file names rather than
the end. So rather than `Family Gathering 2….jpg` you might see
`Family Gath…ng 2018.jpg`. Depending on your naming schemes, this might be a
good or a bad change for you, so sorry in advance if it affects you negatively,
but on average it should be an improvement (Nate Graham, 25.04.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=497664)).

The right-click context menu in the _Trash_ had the "Restore" action right next
to the "Delete" action, which made it easy to accidentally click the opposite
of what you wanted leading to data loss. This week Nate moved the "Delete"
action to the very end of the menu so this no longer happens. Also the "Restore"
wording was changed to "Restore to Former Location" for clarity (Nate Graham, 25.04.0. [Link 1](https://bugs.kde.org/show_bug.cgi?id=498132)).

![](dolphin-trash.png)

Felix fixed a regression in Dolphin 24.12.0 on X11 which caused the keyboard focus to
move to the _Places_ or _Terminal_ panels when Dolphin is minimized and then
unminimized (Felix Ernst, 24.12.2. [Link](https://bugs.kde.org/show_bug.cgi?id=497803)).

{{< app-title appid="itinerary" >}}

The Itinerary team improved travel document extractors for Bilkom and PKP PDF
tickets (Grzegorz Mu, 24.12.2. [Link 1](https://invent.kde.org/pim/kitinerary/-/merge_requests/147)
and [link 2](https://invent.kde.org/pim/kitinerary/-/merge_requests/146)),
International Trenitalia ticket barcodes (Volker Krause, 25.04.0. [Link](https://invent.kde.org/pim/kitinerary/-/commit/eebf34caa1cb59c521f2c764fcfffae01cdb0ac0)),
and the Danish language support for Booking.com (Volker Krause, 24.12.3. [Link](https://invent.kde.org/pim/kitinerary/-/commit/bd7d3a728bdbf9842b5f5e2ebea83415693d5be0)).

The team also switched the querying public transport information feature from Deutsche Bahn
to a new API after the previous one was disabled. This unfortunately
results in the loss of some previously available trip information (Volker Krause,
24.12.2. [Link](https://invent.kde.org/libraries/kpublictransport/-/commit/154761ea3ef89c99f71bca252212e93a0af1cdc5)).

Note that this same issue affects KTrip and for the same reason.

{{< app-title appid="keysmith" >}}

It's now possible to import accounts via `otpauth://` URIs in QR codes (Jack Hill, 25.04.0. [Link](https://invent.kde.org/utilities/keysmith/-/merge_requests/148)).

![](keysmith.png)

{{< app-title appid="kdeconnect" >}}

The list of devices in the sidebar is now properly scrollable (Christoph Wolk, 25.04.0. [Link](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/771)).

{{< app-title appid="kdenlive" >}}

The scaling algorithm has been improved and now, when you zoom, individual pixels
without blur are clearly displayed (Jean-Baptiste Mardelle, 24.04.0. [Link](https://invent.kde.org/multimedia/kdenlive/-/commit/04ac1519630b6bfbebfaa966b04f4d157729b831)).

Note that this change is currently only available on Linux.

![](kdenlive-monitor-zoom.png)

{{< app-title appid="kmymoney" >}}

Thomas added a new feature that shows paid out dividends in investment reports and in the
calculation of returns (Thomas Baumgart. [Link](https://invent.kde.org/office/kmymoney/-/merge_requests/242)),
and Ralf added a column showing the annualized return in the investment
performance reports (Ralf Habacker [Link](https://invent.kde.org/office/kmymoney/-/merge_requests/251)).

{{< app-title appid="labplot" >}}

Israel made it possible for LabPlot to read the value generated by a formula from a cell instead of
the formula text iself when importing data from Excel files (Israel Galadima. [Link](https://invent.kde.org/education/labplot/-/merge_requests/648)).

{{< app-title appid="merkuro.mail" >}}

The email lists in Merkuro Mail now supports selecting multiple emails at once,
dragging and dropping and keyboard navigation (Carl Schwan, 25.04.0. [Link 1](https://invent.kde.org/pim/merkuro/-/merge_requests/496)
and [link 2](https://invent.kde.org/pim/merkuro/-/merge_requests/497)).

{{< video src-webm="merkuro.webm" >}}

It's also now possible to move or copy emails to another folder manually.

![](merkuro-move-dialog.png)

{{< app-title appid="neochat" >}}

Joshua implemented requests for user data erasure (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2079))
and Carl fixed the bug that stopped the context menu froma appearing in the
maximized image preview (Carl Schwan, 24.12.3. [Link](https://invent.kde.org/network/neochat/-/merge_requests/2103)).

{{< app-title appid="skrooge" >}}

The Skrooge Team announced the release of version 25.1.0 of its Personal
Finances Manager. This is the first version ported to Kf6/Qt6. You can find the full announcement [here](https://skrooge.org/news/2025-01-11-skrooge_25.1.0_released/).

![](skrooge.png)

{{< app-title appid="tokodon" >}}

Joshua improved the compatibility with GoToSocial servers even more (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/665))
and also made it possible to share an account handle via a QR code (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/668)).

![](tokodon-qr-code.png)

Meanwhile Carl ported the remaining menus from Tokodon to the new convergent alternative (Carl Schwan, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/659)).

## Packaging

We updated the Craft packages to use Qt 6.8.1 and KDE Frameworks 6.10.0.

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/11/this-week-in-plasma-final-plasma-6.3-features/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
