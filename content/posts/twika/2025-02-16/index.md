---
title: 'This Week in KDE Apps'
subtitle: 'Fahrenheit, new releases and bugfixes'
discourse: thisweekinkdeapps
date: "2025-02-17T12:18:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
fediverse: '@carl@kde.social'
---

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/). This time again a bit delayed due to some personal travel.

## Releases

* [Kaidan 0.11.0](https://kaidan.im/2025/02/14/kaidan-0.11.0/) is out. This new version of KDE's XMPP client brings Qt6 support as well as a few new features.
* [Tellico 4.1.1](https://tellico-project.org/tellico-4-1-1-released/) is out with a few minor fixes.
* [Amarok 3.2.2](https://blogs.kde.org/2025/02/15/amarok-3.2.2-released/) is out with some minor bugfixes, and improvements for building Amarok on non-UNIX systems and without X11 support.

{{< app-title appid="itinerary" >}}

Temperature displayed in Itinerary will now use Fahrenheit units when you set your home country to the USA. (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/pim/itinerary/-/merge_requests/383))

{{< app-title appid="kasts" >}}

Improved the volume button to use an adaptive icon depending on the volume level. (Bart De Vries, 25.04.0. [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/247))

{{< app-title appid="kate" >}}

Added a button to clear the debug output in the debug plugin. (Waqar Ahmed, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1721))

Added a button to switch between a normal diff (with only a few lines of context) and a full diff with all the context. (Leo Ruggeri, 25.04.0. [Link](https://invent.kde.org/utilities/kate/-/merge_requests/1719))

{{< app-title appid="korganizer" >}}

Fixed showing the details of a recurrent event. (Allen Winter, 25.04.0. [Link](https://invent.kde.org/pim/korganizer/-/merge_requests/133))

{{< app-title appid="konsole" >}}

Fixed some freezing issues when starting Konsole and any applications using Konsole KPart like Kate. (Waqar Ahmed, 24.12.3. [Link](https://invent.kde.org/utilities/konsole/-/merge_requests/1062))

{{< app-title appid="merkuro.calendar" >}}

Added an option to filter the tasks to only displays the ones due today. (Shubham Shinde, 25.04.0. [Link](https://invent.kde.org/pim/merkuro/-/merge_requests/501))

## SystemDGenie

Ported the editor to KTextEditor. (Thomas Duckworth. [Link](https://invent.kde.org/system/systemdgenie/-/merge_requests/10))

![](systemdgenie.png)

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](/2025/01/25/this-week-in-plasma-fancy-time-zone-picker/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetarnky
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
