---
title: 'This Week in KDE Apps: Usability, accessibility, and supercharging the Fediverse'
discourse: thisweekinkdeapps
date: "2025-01-19T12:50:35Z"
authors:
 - carlschwan
categories:
 - This Week in KDE Apps
newsletter: true
image: thumbnail.png
fediverse: '@carl@kde.social'
---

![](thumbnail.png)

Welcome to a new issue of "This Week in KDE Apps"! Every week we cover as much as possible of what's happening in the world of [KDE apps](https://apps.kde.org/).

This week we also published a new web page in our ["KDE For You"](https://kde.org/for) series, this time about ["KDE For Digital Sovereignty"](https://kde.org/for/digital-sovereignty/). These pages give you tons of recommendations about KDE and other FOSS apps you can use in different situations, be it for education, creativity, travel and more.

![](kde-for.png)

{{< app-title appid="arianna" >}}

It's now possible to change the app's color scheme independently of the system's color scheme (Onuralp SEZER, 25.04.0. [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/67)).

![](arianna.png)

{{< app-title appid="dolphin" >}}

When manually adding items to the _Places_ panel, the current location's custom icon is pre-populated in the icon field, and the item will now be created globally by default, so it appears in other apps' _Places_ panels as well (Nate Graham, Frameworks 6.11. [Link](https://invent.kde.org/frameworks/kio/-/merge_requests/1787) and [link 2](https://bugs.kde.org/show_bug.cgi?id=498579)).

{{< app-title appid="elisa" >}}

We added an entry at the top of the grid/list to open a track view for the current artist or genre. Tracks from artists opened from genre view will be filtered by genre (Pedro Nishiyama, 25.04.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/653)).

![](elisa.png)

We have solved the problem of creating infinitely nested views when browsing artist > album > artist (Pedro Nishiyama, 25.04.0. [Link](https://invent.kde.org/multimedia/elisa/-/merge_requests/656)).

{{< app-title appid="haruna" >}}

Haruna 1.3 is out with lots of code refactoring. Additionally, the default
actions for left and right mouse buttons have changed: left click is now
_Play/Pause_ and right click opens the context menu. These actions can be changed in
_Settings_ on the mouse page.

[Read the full announcement.](https://haruna.kde.org/blog/2025-01-16-haruna-1.3/)

![](haruna.png)

{{< app-title appid="itinerary" >}}

Volker restored public transport data access to Digitransit in Finland and to Rolph in Germany (Volker Krause, 24.12.2, also affects KTrip) and Joshua and Gregorz wrote and improved travel document extractors for American Airlines, Brightline and Southwest (Joshua Goins, 24.12.2, [Link 1](https://invent.kde.org/pim/kitinerary/-/merge_requests/150), [link 2](https://invent.kde.org/pim/kitinerary/-/merge_requests/150), and [link 3](https://invent.kde.org/pim/kitinerary/-/merge_requests/153)) and Koleo (Grzegorz Mu, 24.12.2, [Link](https://invent.kde.org/pim/kitinerary/-/merge_requests/154)).

{{< app-title appid="kmail2" >}}

Joshua fixed various issues with the markdown rendering in KMail, enabling markdown footnotes, highlighting and removing some dead code (Joshua Goins, 25.04.0. [Link 1](https://invent.kde.org/pim/kdepim-addons/-/merge_requests/59) and [link 2](https://invent.kde.org/pim/kdepim-addons/-/merge_requests/60)); and, to facilitate the use of KMail's security features, KMail will now query a key server when clicking on an unknown OpenPGP certificate (Tobias Fella, 25.04.0 [Link](https://invent.kde.org/pim/kmail/-/merge_requests/147)).

{{< app-title appid="kdenlive" >}}

The audio waveform of Kdenlive was completely rewritten. It is now around twice as fast to generate and is more accurate (Étienne André and funded by the [Kdenlive Fundraiser](https://kdenlive.org/en/fund/), 25.04.0 [Link](https://invent.kde.org/multimedia/kdenlive/-/merge_requests/562)).

Before:

<video class="img-fluid" controls muted="true" loop="true" autoplay>
   <source src="stretch-before.mp4" type="video/mp4">
</video>

After:

<video class="img-fluid" controls muted="true" loop="true" autoplay>
   <source src="stretch-after.mp4" type="video/mp4">
</video>

{{< app-title appid="kdevelop" >}}

We added and improved the debugger pretty printer for `QJSon*`, `QCbor*`, `QDateTime`, `QTimeZone` (David Faure, 25.04.0 [Link 1](https://invent.kde.org/kdevelop/kdevelop/-/merge_requests/688) and [link 2](https://invent.kde.org/kdevelop/kdevelop/-/merge_requests/706)).

{{< app-title appid="krita" >}}

The latest Krita Monthly Update is out. If you want to learn what's going on in Krita as well as see some amazing artwork made with Krita, [check it out](https://krita.org/en/posts/2025/monthly-update-22/).

![](https://krita.org/images/posts/2025/mu22_kurzschwardzenbuglen_nature_sanctuary-yaroslavus_artem.jpeg)

[Kurzschwardzenbuglen Nature Sanctuary by @Yaroslavus_Artem](https://krita-artists.org/t/109201)

{{< app-title appid="qrca" >}}

Qrca now forces the rendering of QR code content to be plain text (Kai Uwe Broulik. [Link](https://invent.kde.org/utilities/qrca/-/merge_requests/101)) and only shows the flashlight button on devices with a flashlight (e.g. not on your laptop) (Kai Uwe Broulik. [Link](https://invent.kde.org/utilities/qrca/-/merge_requests/97)).

{{< app-title appid="tokodon" >}}

Tokodon will now remind you to add an alt text to your images (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/686)).

![](tokodon-alt-text-reminder.png)

We also added an option for a confirmation dialog before boosting a post. This is particularly relevant for people managing multiple accounts to prevent them from boosting posts from the wrong account (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/661)).

In the department of trust and safety improvements, you can now filter some posts from your timeline (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/680)).

![](tokodon-add-filter.png)

And show a banner when an account has moved to another server (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/679)).

![](tokodon-banner.png)

You can now browse posts that are about a news link (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/676)) and see the post associated with an image in the media grid of a profile (Joshua Goins, 25.04.0. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/674)).

We also fixed a bug where, when failing to authenticate one of your accounts, Tokodon would be stuck indefinitely on the loading screen (Carl Schwan, 24.12.2. [Link](https://invent.kde.org/network/tokodon/-/merge_requests/678)).

{{< app-title appid="kwave" >}}

We improved the performance of the playback using QtMultimedia significantly (Thomas Eschenbacher, 25.04.0. [Link](https://invent.kde.org/multimedia/kwave/-/merge_requests/56)).

## …And Everything Else

This blog only covers the tip of the iceberg! If you’re hungry for more, check out [Nate's blog about Plasma](/categories/this-week-in-plasma/) and be sure not to miss his [This Week in Plasma](https://blogs.kde.org/2025/01/18/this-week-in-plasma-getting-plasma-6.3-in-great-shape/) series, where every Saturday he covers all the work being put into KDE's [Plasma desktop environment](https://kde.org/plasma-desktop/).

For a complete overview of what's going on, visit [KDE's Planet](https://planet.kde.org), where you can find all KDE news unfiltered directly from our contributors.

## Get Involved

The KDE organization has become important in the world, and your time and
contributions have helped us get there. As we grow, we're going to need
your support for KDE to become sustainable.

You can help KDE by becoming an active community member and [getting involved](https://community.kde.org/Get_Involved).
Each contributor makes a huge difference in KDE — you are not a number or a cog
in a machine! You don’t have to be a programmer either. There are many things
you can do: you can help hunt and confirm bugs, even maybe solve them;
contribute designs for wallpapers, web pages, icons and app interfaces;
translate messages and menu items into your own language; promote KDE in your
local community; and a ton more things.

You can also help us by [donating](https://kde.org/donate). Any monetary
contribution, however small, will help us cover operational costs, salaries,
travel expenses for contributors and in general just keep KDE bringing Free
Software to the world.

To get your application mentioned here, please ping us in [invent](https://invent.kde.org/websites/blogs-kde-org/-/merge_requests?label_name[]=This%20Week%20In%20KDE%20Apps) or in [Matrix](https://go.kde.org/matrix/#/#this-week-kde-apps:kde.org).
