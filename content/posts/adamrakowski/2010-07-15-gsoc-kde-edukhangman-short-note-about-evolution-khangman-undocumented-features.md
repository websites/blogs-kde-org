---
title:   "[GSoC] [KDE-Edu][KHangMan] Short note about evolution of KHangMan - undocumented features"
date:    2010-07-15
authors:
  - adamrakowski
slug:    gsoc-kde-edukhangman-short-note-about-evolution-khangman-undocumented-features
---
<p>The tasklist I submit to Google was very realistic, but don't know I why didn't suppose to meet any small bugs need to be solved. Almost all the Community Bonding Period I spent on setting developers environment. Newest Qt, Phonon, DCOP, ... , KDE-Edu. Then, once I compiled everything and I had hoped that everything is fine. Some hours of sleep and then great crash. KDE couldn't start. I was trying under Debian, Kubuntu and Mandriva. Same effect. Over three weeks of downloading, compiling, installing new distros etc... Endly, with help of Anne-Marie and #kde-devel Community I found out I could solve it. They provided me a solution to configure my environment easy and effortless. Huh, thanks! When coding period began I was ready to work. Beginnings are hard.</p>

<p>Until today following improvements were made:</p>
<ol>
<li>KHangMan themes are stored in XML.Before making this, all the themes were hard-coded, thus any modifications required source editing. Currently all the themes are stored in only in XML files, so each user can provide his own without recompilation. KHMThemeFactory (a container for themes) was completely refactored. Now it can handle with many themes located in multiple files. Using instance of this class even dirs can be searched for matching XML files. Below is screenshot from standardthemes.xml file, which handles default themes for KHangMan</li><p><img src="http://szn.republika.pl/xmltheme.jpg" /></p>
<li>Open recent file option have been added</li><p><img src="http://szn.republika.pl/recent.jpg" /></p>
<li>Spiral mode has been implemented. It's being tested. Without spiral approach next word to guess is taken randomly. If you activate this feature, program remembers words those were hardest to guess to you. Order of words to guess depends of their difficulty. Easiest one is get rarely, than hard one. This feature is still being tested, so haven't been commited yet.</li>
</ol>

<p>Things being developed:
Now I work on custom hangman images/animations. Anne Marie described me general idea of implementation. At this moment KHangMan supports only one KHangMan animation - it's a "K" hanging on a gallow. For children it can be cruel a bit. This feature maintain adding custom animations and change current one during play</p>

<p>Meanwhile I found some "undocumented features". They were fixed.
<ol>
<li>If word isn't guessed correctly, user gets a message with correct answer. In some languages translated message + long word could be wider, than message box (as in picture). After fix font size depends on screen resolution and text length. It always fits to message box. The belt you see on the right side was a bug. It has been fixed by Anne-Marie</li>
<p><img src="http://szn.republika.pl/strlen_before.png" /> <img src="http://szn.republika.pl/strlen_after.png" /></p>
<li>XML themes needs to be validated. Before sending patch, passing an empty string as a background svg filename was permitted. Currently it is not.</li>
<li>Anne Marie also made fixes to another bug I found - some values used to place KBalloon in correct position weren't used correctly. Now it's fixed.</li></ol></p>

 Hope this work will give children more fun and amusement! :)