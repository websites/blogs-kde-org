---
title:   "New blog for amaroK developers"
date:    2004-11-01
authors:
  - markey
slug:    new-blog-amarok-developers
---
<p>
We amaroK developers are now also blogging <a href="http://amarok.kde.org/component/option,com_mamblog/Itemid,48/">here</a>. The blog is basically both for amaroK/multimedia development and personal stuff, which we figure is too narrow in scope to post on kdevelopers.org.
</p>
<p>
RSS feed is not yet available, but we're working on it. Anyone know how to make a feed with mamblog?
</p>





