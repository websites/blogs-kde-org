---
title:   "call for icons"
date:    2004-04-12
authors:
  - berkus
slug:    call-icons
---
If there are any artists feeling they can make some icons, please please do!
Application of interest is <a href="http://berk.upnet.ru/projects/kde/akregator/">aKregator</a> (kdenonbeta/akregator)

I think one of the app icons may be a little green alligator =) But thats only a proposition, you may choose something entirely different.