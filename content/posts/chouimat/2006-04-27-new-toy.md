---
title:   "new toy"
date:    2006-04-27
authors:
  - chouimat
slug:    new-toy
---
This week I got a nice new toy to play with and
hack... it's a Linksys WRTSL54G, and yes it run
linux and it has an USB port so it can be used 
for storage like my Linksys NSLU2 ... the first 
thing I did was to build my own firmware (based
on OpenWRT) ... it's not in service on my network yet
because last time I had a WRT54G someone killed it so
this time I will to a lot of testing ...

And I'm also dreaming of a WMB54G, another device that run
linux and this one once hooked to my sound system will enable
me, I hope, to listen to my huge playlist with <b>REAL</b> speakers

And I wish you all a nice day and/or weekend 
:D
<!--break-->