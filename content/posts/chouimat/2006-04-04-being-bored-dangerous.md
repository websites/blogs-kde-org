---
title:   "Being bored is dangerous"
date:    2006-04-04
authors:
  - chouimat
slug:    being-bored-dangerous
---
Lately, I was even more bored than usual. so I decided to check what I had on old backup tapes
and I found a lot of applications I wrote for DOS in Clipper (the xbase compiler) and I thought that some
of them are worthy of being "modernized" I know some linux based xBase compilers, xharbour ( www.xharbour.org)
Clip (http://www.itk.ru/english/index.shtml) but they compile the clipper code to C and use a vm ... 
I thought it would be fun (I know I have a weird definition of fun) to hook one of them to GCC 4.1 ...

After a while I decide to write a new front-end from scratch for Xbase .... more to come :)

As you can see being bored is really dangerous :D
<!--break-->