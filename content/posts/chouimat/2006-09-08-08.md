---
title:   ":)"
date:    2006-09-08
authors:
  - chouimat
slug:    08
---
  There was a lawyer and he was just waking up from anesthesia after surgery, and his wife was sitting by his side. His eyes fluttered open and he said, “You're beautiful!