---
title:   "What's the point of all of this ..."
date:    2006-06-01
authors:
  - chouimat
slug:    whats-point-all
---
For the past few months (or should I say years) I'm trying to get rid of this 
feeling that I'm born in the wrong era or maybe aeon and in the current
society I'm more than worthless ... it's hard and since this morning
I'm asking myself why bother trying to improve myself and feel better
if nobody care about me anyway (ok it's not quite true it seems there is at least
one living being that care about me ... only because I feed him and clean his litter box ...
so basicly he's doing like everybody else he uses me ...) and something that really piss me off
is some people take me for granted for their development projects even when I'm not sure about the
viability of it ...  this is really helping me to feel better about myself 

So this morning I really contemplated the "Cliff solution {TM]" but since I'm a
failure I didn't did it ... hey I'm not that crazy I don't want to add this failure to the rest 
I also thought of going elsewhere but in the end I will only be running from myself and it's kind of hard to do 

so I will simply stay and wait that those feelings go away for a short period of time and came back to kick me with greater force next time ...
<!--break-->


