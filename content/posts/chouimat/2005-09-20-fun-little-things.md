---
title:   "fun with little things"
date:    2005-09-20
authors:
  - chouimat
slug:    fun-little-things
---
In the few weeks since I'm back from malaga, I got a new contract consisting of writing a control application on an emebedded device (a lpc2138 based one). And no I don't use Qt on this. The reason is I only have 32KB of ram and 512KB of flash on the board. so I must write all the graphic primitive, a nice change from those multigigabytes desktop systems ... This remind me of my old Color Computer 3 :'-( I miss this computer ...
<!--break-->