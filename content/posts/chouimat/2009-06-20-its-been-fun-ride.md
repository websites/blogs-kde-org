---
title:   "it's been a fun ride"
date:    2009-06-20
authors:
  - chouimat
slug:    its-been-fun-ride
---
yes it's been a fun ride. For the past 18 months, I had a nice job, but since yesterday not anymore. I think it's a nice occasion to look at all the stuff that eat all the space on my hard disks and see what can be reused and released as opensource. 

but for now time to relax and enjoy the simple pleasure of doing nothing, of having nothing to do and also not having to be at the office on time ... this is life ... except without the $$$ :)