---
title:   "a long, slow and extremely boring process"
date:    2007-09-05
authors:
  - chouimat
slug:    long-slow-and-extremely-boring-process
---
it took me nearly 2 months to go through 7 years of code.
and I came to the sad reality that only 5% of it is reusable and interesting 
even the embedded firewall product is not that interesting anymore since it's based
on an ancient technology (namely freebsd 4.x) so I decided to ditch it too, no more new version
and no support for it since I don't have the setup for doing it. What I will do is
to take the knowledge and some of the cool concept I got while designing it and reimplement them
using something more modern, here my goal is to not let die the best thing (even if sometime it was 
a pain in the ass) I managed to do in the past 7 years. 

my idea is to merge most of the stuff (concepts, ideas etc) from the various branches even those who were basicly
"let try this to see if it make senses". I love some of those crazy ideas :D
<!--break-->