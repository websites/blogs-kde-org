---
title:   "The \"Foie Gras\" syndrom or the force fed technology syndrom"
date:    2005-03-07
authors:
  - chouimat
slug:    foie-gras-syndrom-or-force-fed-technology-syndrom
---
The day that I feared the most is near. The day were my use of linux will be decided by marketing drones but this time they are not force feeding me proven technology but unproven ones and for what? Symply for the sake of interoperability between Gnome and KDE and also probably others Desktop. Well it's a noble goal but why using an unproven technology  instead of taking an existing one that works which might only need some fixes and adapt it to satisfy the need of everybody? 

But first what does interoperability mean? From The Free On-line Dictionary of Computing (27 SEP 03) :

  interoperability

          The ability of software and hardware on multiple machines from
          multiple vendors to communicate.


The definition is clear to be able to interoperate the linux desktop don't need to use all the same tecnology but have a bridge between their corresponding technology.
Now people are pushing use to use D-Bus instead of DCOP and even g-conf for configuration. We all know that DCOP, which is battlefield tested, need libICE to work, which is not maintened anymore. So the question is: What need to be done to addres
s this problem?
Here some possible way
1) leave it in the current state and pray that nothing break
2) reimplement libICE
3) reimplement DCOP using another mechanism like shared memory
4) ditch dcop and using something else

I personnaly prefers the third option because I don't need a networked ipc 99.999999% of the time and if we really need to interoperate with gnome let just use a bridge. And if people prefers to go with option 4 why use D-BUS? why not use DCE which is now Opensource and which will enable us to interoperate with more systems.

I must also add that marketing driven technology choice gave us Microsoft and Java ... and this is enough to make me ponders about the well founded of the use of D-BUS. No in reality I'm scared that we will repeat the same mistake that was made in the 90's by destroying what in my imho is a superior platform by using technologies that are not able to fix the problem of an inferior platform, just because someone with more marketing drones told us to do so for the sake of interoperability.

Some might wonder why I choose this title? It's simple I have the impression I'm a goose or duck that get force fed so they can produce better "foie gras"
 
<!--break-->