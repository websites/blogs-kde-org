---
title:   "Fresh new start soon"
date:    2007-11-11
authors:
  - chouimat
slug:    fresh-new-start-soon
---
Tomorrow morning, I will start my new job at axentra.
I must admit I'm a little nervous but also excited by this new job. After 3 years of being self-employed, and with all the shit I had last year and until this May , it's a definitly a welcomed change in my professional life. Sure I will miss the fact that I was able to wake up late in the morning, not having to dress to go to work and also the 30 seconds commute times, but for a more steady income it's a sacrifice I'm willing to make. 