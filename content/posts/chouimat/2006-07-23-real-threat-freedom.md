---
title:   "real threat to freedom??"
date:    2006-07-23
authors:
  - chouimat
slug:    real-threat-freedom
---
after reading the thread on kde-core-devel mailing about
a common coding style for kdelibs it seems we have to clan:
the pro and the anti ... well like every flamewars :D

the interesting part is definitly in the "against" clan,
some are against because they don't like the proposed style, the others are really
interesting and I'm one of them, they object on a purely political 
basis, the fact that using a mandatory coding style will add
another level of useless bureaucracy to kde ... for my part I think
that the discussion should have happened on an unrestricted mailing list
like kde-devel or kde-bahwedontcareaboutthis ... but who am I to say that
kde-core-devel was a bad choice ...

I understand the rationale behind this and yes having a consistent looking code base
is important, but having this without having a good discussion about the various part of the
guideline ... make me feel a little uneasy, it like having the guideline book on the
first day of a new job and after reading it you say that thing doesn't make any sense. 
And the answer of from your manager is "This is done on purpose, you're here to work not to
have fun" ...

To a certain point I find this a threat to my freedom of expression, software programming is not
a science (the techniques we use are) but the software itself is ART but some seems to lose the view of this ...

anyway I think we will all have to learn to live with this, like every other threat to our freedom we had lately, it's a post-9/11 world and we are getting used to ... well sort of :(

<!--break-->