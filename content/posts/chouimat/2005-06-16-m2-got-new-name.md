---
title:   "M2 got a new name"
date:    2005-06-16
authors:
  - chouimat
slug:    m2-got-new-name
---
M2 was simply the internal codename for the project. So I found this one: ARRRGGH and no it's not an acronym, but I bet some people will work very very hard to find one :) The name simply came from the sound made by a sysadmin who simply screwed up something. 

In my previous blog I said that the project was a generic framework for systems management and deployment, let me explain it a little. Let's say you have 25 computers with differents linux distribution and/or differents OS and/or differents processor arch and you need a central way to manage them and deploy your internal business critical applications. Since all those computers have different packages format, update mechanism ARRRGGH is independant of all of those, the server doesn't care about the distribution detail because the client work is made by plugins. Which mean for some computer the delivery mechanism could be apt-get, ghns, ssh or even ftp.

The current design is flexible enough that I'm currently using it to implement for a customer a way to delivers certificate for a vpn.
<!--break-->