---
title:   "Good morning, Mr. Kaper. We've been expecting you...."
date:    2004-09-20
authors:
  - daniel molkentin
slug:    good-morning-mr-kaper-weve-been-expecting-you
---
You sure have been wondering why we integrated the google search bar and why we "secretly" developed gecKo. The reason is simple: We are after you. Flee, but mind you: you can run, but you can't hide! *insert-evil-and-deep-laughter-here*
Read on to learn about all the evil details...
<br /><br />
Ok, and now for the serious part of this entry: Some facts about the evil "Google Plugin":
<ol>
<li>The plugin is shipped since at least KDE 3.2 already
<li>The plugin resides in kdeaddons (the very same module that also atlantikdesigner is in, how could you have missed it for so long?), so it's entirely up to you to install it or not.
<li>The problem of it not being deselectable on the GUI is actually a problem with all Konqueror plugins. If you don't like it, send a patch.
<li>If you don't like it to have google as default, just replace it with your own preferred searchprovider in the source code or make it configurable, it's really straightforward!). After all, it's called the search plugins, as it can also search inside the page, so why not extend it?
<li>If you don't like it at all, "make uninstall" is your friend, but as a developer, you sure know that.
<li>Oh and about your paranoia: I guess they have yellow pages where you live, so find yourself a good psychologist and <b>stop bothering us</b>. Thank you.
</ol>