---
title:   "amaroK podcasting also big in .de"
date:    2005-09-25
authors:
  - daniel molkentin
slug:    amarok-podcasting-also-big-de
---
amarok podcasting rocks, but it's <a href="http://blogs.kde.org/node/1482">not only big in .nl</a>, Fab! Deutschlandfunk (Cologne) and Deutschlandradio Kultur (Berlin) are two german nationwide broadcasters that deliver lots of informational content. Both recently introduced podcasting for some of their shows. On their <a href="http://www.dradio.de/aktuell/398578/">page about podcasts</a>, they introduce podcast reception with iTunes AND amarok. Go for it!
<!--break-->