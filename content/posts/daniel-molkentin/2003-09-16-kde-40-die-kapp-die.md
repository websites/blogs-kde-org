---
title:   "KDE 4.0: Die kapp, die!?"
date:    2003-09-16
authors:
  - daniel molkentin
slug:    kde-40-die-kapp-die
---
Recently, a discussion about KApplication::random() popped up on kde-cvs. People were asking to move this in a Math tool class. This reminded me of something the KDE-Qt integration group in n7y discussed as a "would-be-nice-to-have" thing. But let me first describe my POV:
<br><br>
IMHO KApplication was way to long the place to store everything but the kitchensink. I'd love to get rid of KApplication in KDE 4 if that is possible somehow (or strip it down to a minimal layer that provides no new methods. The motivations:
<ul>
<li>qApp does not provide all functionality we need but
<li>kapp is not defined (and hence crashes) when classes are used in designer or other qt apps.
<li>It's really hard to use KDE Widgets outside of a KDE application (because they internally rely on the kapp pointer. I tried to guard those calls to make the widgets usable with designer, but it's a really PITA).
<li>KApplication is quite bloated.
</ul>
It would be nice if we could have the KDE libs a a nice add-on lib to Qt rather than something that "overrides" it. Saying that I know that we might want a certain control over our classes, but I see no reason why KApplication should provide added functionality (overriding for behavioral adjustments might be necessary, but it would at least make the kapp pointer obsolete)
<br><br>
Ettrich said that the custom event handling stuff could be removed. the KConfig/KStandardDirs/KLocale object and friends could be retrieved via KGlobal, which should then probably be a singleton. All the small convinience functions should probably move out to tool classes (KDE::Tools). That doesn't mean they have to be less convient! I see no reason why invokeMailer and co. should be in KApplication, except for the tempting short "kapp" macro. So IMHO:
<br><br>
<ul>
<li>kapp must die
<li>KApplication could remain as a small wrapper around QApplication that overrides some virtual methods, but it's really questionable if we really need it.
</ul>
To all those who are worried about backward compatibility:  the KDE_COMPAT macro around deprecated methods is your friend there. I guess in the end the problem will be... manpower.