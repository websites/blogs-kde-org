---
title:   "Why Skype is more than a hype"
date:    2004-10-06
authors:
  - daniel molkentin
slug:    why-skype-more-hype
---
Yesterday, I did some calls with Skype. Amongst others I talked to Fabrice Mous and afterwards we even did a conference call with Stephan Binner. Later on I called my father. This is even more remarkable since my provider (which I luckily will get rid of at the end of this month) blocks UDP ("It's stateless, we can't control it, therefore it's dangerous."). This usually means the end to any VoIP solution which require UDP for low-latency. Still Skype simply works -- remarkable well.

Skype uses a peer-to-peer approach which helps in quite some situations, but I was surprised about the low latency, which I would have expected to be significant with 2P2 phones.

Many people think of Skype of "yet-another-hype" and it may well be one. 
Others pity that Skype is not free software (e.g. the current Beta it can only make use of OSS instead of ALSA with Linux). But the software and it's license are not the siginificant part. It's the idea that makes Skype shine.

The concept behind Skype is the proof that VoIP for John Doe is possible -- in non-ideal NATed and firewalled networks. The broad support of operating system boundaries (Windows, OS X, Linux, which is quite remarkable for commercial software) also helps a lot. 

It's a proof that VoIP can simply work rather than getting in your way. I spend nights with other solutions without success, but Skype's approach is different.

I hope to see a Free Software implementation using a similar approach soon. Linux always had good free (as in speech) P2P protocols and software. Having them for VoIP is overdue.