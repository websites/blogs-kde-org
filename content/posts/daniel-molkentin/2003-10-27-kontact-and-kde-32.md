---
title:   "Kontact and KDE 3.2"
date:    2003-10-27
authors:
  - daniel molkentin
slug:    kontact-and-kde-32
---
Last night I did a lot of bug fixes and janitor work on bugs.kde.org and others were also working hard to get some stuff done before the string freeze that hits us as of today. Still we (#kde-pim) realized that we are not quite there, which is why we will most probably release Kontact as version 0.8 with the following features disabled:

- Kolab Support (can be enabled by a hidden switch for the braves)
- Exchange 2000 Support (not completely implemented, but patches are there)
- Button Bar (had quite some issues)

On the shiny side of things we will have a separate KDE PIM release 2-3 Months after the KDE 3.2 release and hopefully another meeting of the KDE PIM team to get those (and maybe other) features implemented.The other shiny thing is that Kontact itself is down to 4 real bug reports, of which I can't reproduce two. #57204 is evil. I will solve this in the next days by committing an application simiar to kfmclient which will do the right thing (tm). #65587 will be solved by using libkcal that got locking and modification notification.

Also Cornelius was able to implement drag an drop for the icon sidebar, e.g. one can drop a mail onto the todo list, which will create a new todo item reminding of deadlines wrt mail. I hope we can enhance this by implementing #60496 (Associating contacts with mail traffic).

I'd like to thank the whole KDE PIM team for getting Kontact so far and especially Bo Thorsen who was working hard to merge kroupware_branch this weekend, sacrificing his holidays. And while might be bit sad to see KDE 3.2 go without official groupware support in Kontact, I think this release gives us the possiblity to react on broad user request coming in after KDE 3.2 and to deliver an even better Kontact. So: Bear with us, stay tuned :)

PS: I will go to LinuxWorldExpo in Frankfurt on Tuesday and show off what we've got so far.