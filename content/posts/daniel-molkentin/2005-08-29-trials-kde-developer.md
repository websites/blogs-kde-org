---
title:   "Trials of the KDE Developer"
date:    2005-08-29
authors:
  - daniel molkentin
slug:    trials-kde-developer
---
The life of the traveling KDE developer is not an easy one. We put our lives on hold while we travel far from home to meet with other ardent souls to bring you Free Software par excellance. It's a hard life indeed, as you can see in the picture below. We are sitting in a square in the old town of Malaga with food, beer and the backdrop of a cathedral. Somehow we manage. ;)
<a href="http://developer.kde.org/~danimo/meetings/akademy05/hackersinparadise.jpg"><img src="http://developer.kde.org/~danimo/meetings/akademy05/hackersinparadise_small.jpg" border="0" class="showonplanet" /></a>

PS: Ivor said that it might be worth to add that we are eight people from eight different
countries, and our waitress is actually polish, owns a Dell Inspirion, and knows about Linux :)
<!--break-->