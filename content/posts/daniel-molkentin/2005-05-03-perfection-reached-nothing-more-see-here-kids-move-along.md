---
title:   "Perfection Reached. Nothing More to See Here Kids. Move Along!"
date:    2005-05-03
authors:
  - daniel molkentin
slug:    perfection-reached-nothing-more-see-here-kids-move-along
---
Our beloved news source <a href="http://www.slashdot.org">Slashdot</a> is reporting that Microsoft is about to help Ford to build <a href="http://hardware.slashdot.org/article.pl?sid=05/05/03/1735254&tid=109&tid=126&tid=137">cars that don't crash</a>. The <a href="http://www.livescience.com/technology/ap_050502_gates_cars.html">linked article</a> on  livescience reveals: "Eventually, Gates said, there could be a car that wouldn't let itself crash." Now, the obvious go to joke would be: "Right, this is the consequent development after they shipped the operating system that never crashes. Oh wait...". I would put it differently: "Thank god I don't own a Ford anymore..."
<!--break-->