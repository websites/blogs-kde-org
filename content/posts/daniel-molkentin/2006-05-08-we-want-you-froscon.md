---
title:   "We want YOU for FrOSCon!"
date:    2006-05-08
authors:
  - daniel molkentin
slug:    we-want-you-froscon
---
For the first time, <a href="http://www.froscon.org">FrOSCon</a> will take place this year in St. Augustin near Bonn, the former capital of Germany. Due to its location, it is also in reasonably short distance for most KDE contributors from the BeNeLux countries.

The conference is gonna be very similar to FOSDEM -- many open source projects have applied for an own room, as has KDE. Since the event will take at the Fachhochschule Bonn-Rhein-Sieg, where I happen to study, I will organize the devroom.

Thus: If you would like to attend, please add yourself to the <a href="http://wiki.kde.org/tiki-index.php?page=FrOSCon%202006">FrOSCon 06 Wiki page</a>. We are also still looking for KDE presentations and tutorials, KDE 3 related ones just as well as talks on new technology.

For those of you who like a cheap sleeping opportunity, there is have a gym available 500m from the conference site. For those who prefer a nice bed, we'll find you a nice location with reasonable prices. If you have any questions, just contact me on irc or via mail.

See you at FrOSCon!
<!--break-->