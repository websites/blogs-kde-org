---
title:   "KDE for Windows: Installation made easy!"
date:    2005-10-08
authors:
  - daniel molkentin
slug:    kde-windows-installation-made-easy
---
Ok, finally back at home. *Pheeew*.

Over the last days I played with the <a href="http://nsis.sf.net">Nullsoft scriptable installer system</a> (NSIS) which allows for creating comfortable installation routines based on a description file. It's pretty scriptable and gives nice results. Together with Ralf Habakers recent efforts to port KDE to Win32 with MinGW, the GCC for Windows port, it looks like installing KDE 4 applications with Scons/bksys could be both pretty flexible for us and simple for the user (unlike developing on windows for developers without MSVC, but I will comment on that in a later article).

It seems like we could create a fairly generic script that would fit most applications that we can semi-automatically create RPMs for today. At least for SUSE, that's pretty painless.

All a user would generally need is installing the KDE base package (compare to the ".NET framework" that many apps need on windows today). This would only be required once for any KDE-based application and the installer could even fetch the package automatically.

Even though packaging windows applications with NSIS looks odd to someone only trained to create tarballs and RPMs, it is a pretty flexible piece of software.
<!--break-->