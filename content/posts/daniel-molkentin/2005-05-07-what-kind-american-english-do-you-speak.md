---
title:   "What Kind of American English Do You Speak?"
date:    2005-05-07
authors:
  - daniel molkentin
slug:    what-kind-american-english-do-you-speak
---
I <a href="http://www.blogthings.com/amenglishdialecttest/">took the test</a> and those are my results:

<table style="color: black;" width=400 align=center border=1 bordercolor=black cellspacing=0 cellpadding=2>
<tr><td align="center" bgcolor="#A8FFB3">
<h3>Your Linguistic Profile:</h3>
</td></tr><tr><td bgcolor="#D9FFD8">
50% General American English</td></tr><tr><td bgcolor="#A8FFB3">
25% Yankee</td></tr><tr><td bgcolor="#D9FFD8">
20% Dixie</td></tr><tr><td bgcolor="#A8FFB3">
0% Midwestern</td></tr><tr><td bgcolor="#D9FFD8">
0% Upper Midwestern</td></tr></table>

<div align="center">
<a href="http://www.blogthings.com/amenglishdialecttest/">What Kind of American English Do You Speak?</a>
</div>
I'm really curious how our british folks can master this, or if they simply feel insulted ;) (Hey folks, I seem to speak 5% non-american english, must be <a href="http://en.wikipedia.org/wiki/Denglisch">Denglisch</a>... )
<!--break-->