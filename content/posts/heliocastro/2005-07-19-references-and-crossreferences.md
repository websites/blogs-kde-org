---
title:   "References and crossreferences..."
date:    2005-07-19
authors:
  - heliocastro
slug:    references-and-crossreferences
---
For everyone's blaming me not blogging for some time, i wouldn't feel good to write anything before recently changes. Too much work, not good days, even hard to get a good mood to work on KDE, but...
Well, last happy weekend i saw a lot of movies, from    
<a href="http://imdb.com/title/tt0338013/">Eternal Sunshine of the Spotless Mind</a>, with an great Jim Carrey and Kate Winslet to <a href="http://imdb.com/title/tt0305206/?fr=c2l0ZT1kZnxteD0xMDAwfHR0PTF8ZmI9dXxwbj0wfHE9YW1lcmljYW58cnM9Y3xodG1sPTF8bW9yZT10dA__;fc=10;ft=1187;fm=1">American Splendor</a>, another amazing picture retracting some part of United States culture, and have the presence of Crumb ( anyone who read cartoons MUST know him ) as a crucial part of the history, to the terrible but brilliant <a href="http://imdb.com/title/tt0363163/?fr=c2l0ZT1kZnxteD0yMHxsbT01MDB8dHQ9MXxmYj11fHBuPTB8cT1oaXRsZXJ8aHRtbD0xfG5tPTE_;fc=1;ft=99;fm=1">Der Untergang ( Downfall )</a>, an crude and too alive picture from the last days of Hitler on Berlim saw by eyes of persons not biased by second war cruelty, making his point of view unique
Three complete different pictures, and for me, which most came to my attention was soundtracks. all perfect in their context. Of course i'm gone to find some of the tracks.
One in special, from American Splendor, was an old latin tecnopop hit called Silent Morning ( Noel ). This is a well fitted song for today, when 80's is around everywhere, and i would really like to listen more of same style ( not genre now ).
So i'm gone for the first obvious channels, google and amazon. I got some results from google, like having time references from the 80's, some top hit songs, but even in the 20 first hits i couldn't find the info i needed. Charts from those times are very hard to find and sometimes inacurate. So, i gone to Amazon to see what buyers bought togheter with Noel CD. I got some interesting references, like Stevie B's and Johnny O's, which record lead me to TKA and other's. So ok. Now i got some more references. But all this references are from available cd's on amazon.
I would like to try singles that probably never would be released on cd and maybe ( i said maybe ) some "genious" from music industry put him available on download.
So, next try ? Audioscrobbler. Terrible idea. Too many references from people who listened Noel, including crap brazilian funk music ( Funk Pancadão on local language ). Not works for who want some basic results. So i decided not follow the users path ( even on google all the results lead me to make cross references on user indications ).
Now it's time to find by genre. So i gone to insane and funny <a href="http://www.di.fm/edmguide/edmguide.html">Ishkur's Guide to Electronic Music</a> and let them point me the genre of this kind of song. Was nice since i could remember and listen for the good old hip-hop like Grandmaster Flash and Sugarhill Gang. Backing to the genre, i thought was some kind of Miami Bass, but no. Neither Electro Funk ( hail George Clinton ). Electro is getting near and links moved me to SynthPop ( Human League anyone ? ). So after inumerous references, guess what style most fits on my needs ? Freestyle.
Can you imagine how easy is to find something based on "freestyle" genre ?. 
So, getting the point. We have a lot of information available around internet, lot of ways to search, and even users cross references, but NONE way to find this in an easy method whithout leading you to make a personal data mining.
I got something like 20 songs after all this searchs, and takes me a time that a not obstinated computer geek would take.
There's still too much things to research and develop. That's a fact
<!--break-->