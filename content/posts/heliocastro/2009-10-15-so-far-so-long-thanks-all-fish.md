---
title:   "So far, so long, thanks for all the fish..."
date:    2009-10-15
authors:
  - heliocastro
slug:    so-far-so-long-thanks-all-fish
---
So some already heard the news, but just today i felt that is time to push my own message.
After 10 and a half years, i'm leaving Conectiva and of course Mandriva. Among all this years i shared all the up and downs, saw literally hundreds of colleagues leave company in all sort of situations, saw company evolution from a linux box distro to a major world linux developer, survived position changes, office changes, even building changes.
As everyone that lived all the past and current histories knows that are all to stay for life.
I arrived at Conectiva to do one of amazing hard work, which is convert an migrate the first ATM machines in the world from DOS to Linux in a stupid very short time ( bank is well know as Banrisul and you can see many pictures allover the web ). Guess what, still works, and you can see application running as today, with all post revisions. And i joined later the corporate development team at Conectiva.
But was my post assignment that created my real meaning in FLOSS, when i joined the distribution team ( the distro !! ). I will not deny that much of what i learn until now came from the guys there, some of then today found in companies that already are or starting to grow and learn FLOSS.
And was there that i get my real love for code, and in special some desktop interface called KDE.
Same thing that bring me to know the world, and many friends.
From there to now, i've been doing so many things is FLOSS that i really loose track. But this is a not ended history :-)
At Conectiva, would be unfare to say thanks for a specific person, since was not a workplace, was a family, as disfunctional with good and bad days family. But survivors.
And to Mandriva french friends, i have special thanks for Anne ( extended to her husband Erwan ) that welcome us in a moment that lot of people at .br are having doubts if merge would work and mostly to received me at their home as close friends, even with a surprise invitation to their wedding. And to Blino too, since he was a party buddy and good support at .fr side. ( yes,PLF people, i will not forget you, so no hunting me please :-). And i can say that KDE will be in very good hands at Mandriva starting November...

But now, moving forward, i'll be starting a new big life project that is called Collabora. I have no much to talk about, i'm basically on the initial lego blocks to build, and will see how my time will be there. 
About the work ? Is enough to say that i will work with the things that i love to do. 

So, for all Mandriva guys in Brazil, in France, and the PLF crazy people, thanks for everything. Be sure that friendship is kept forever...
<!--break-->
