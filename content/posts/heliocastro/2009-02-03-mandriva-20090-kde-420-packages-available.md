---
title:   "Mandriva 2009.0 KDE 4.2.0 packages available"
date:    2009-02-03
authors:
  - heliocastro
slug:    mandriva-20090-kde-420-packages-available
---
Thanks to a good earphone playing lots and lots of loud and good trance music, i'm providing Mandriva 2009.0 KDE 4.2.0 packages.
For now only i586 packages are available, x86_64 packages will be provided after initial feedback.
A standard urpmi repository is available.
<a href="ftp://ftp.kde.org/pub/kde/stable/4.2.0/Mandriva/README">README</a> for some detailed information.
