---
title:   "The evil of all translation ( or how i18n packaging can be hell on the earth )"
date:    2004-03-25
authors:
  - heliocastro
slug:    evil-all-translation-or-how-i18n-packaging-can-be-hell-earth
---
Well
Following the tuff days at Conectiva ( company restructuring is always tuff ), we are finishing our next Conectiva release.
Many people know how insane and massive split package policy we have, and adding new packaging features like automatic i18n subpackages generation and auto detecting i18n install by language ( all thanks to LUA language embedded on apt ) looks like the things are become harder to manage, no ? Really no. We reach a good technology stage and a good efficiency with our small team and we do this things easily.
BUT ( always have a BUT ), there is such beasts as KDE i18n Packages...
I and my trainee suffer a lot every time we need create de KDE i18n packages. Why ? 
We package whole KDE, Koffice, Quanta, Kdevelop, etc.. As a distribution, we always choose a branch stable bugfix release ( of course ), so let's use branch i18n packages, right ? Terribly wrong...
Let me enumerate such points:

1 - Koffice have a separate i18n using their branch. Oh, how nice.. But, they didn't compile, recreate using make -f Makefile.cvs not work. Solution: Use head i18n, but ( again a but ) in the head theres no separate package to koffice, so what i need do ? Create a script to download specific koffice docs from HEAD i18n to create a koffice-i18n packages to koffice code from branch. Nice one, huh? There's more...

2 - kdeextragear apps. I don't know if this is a holy sacred space, but this space have many killer apps for KDE, like k3b, so it's kind of obvious that distros will like to package such applicatiosn. But ( oh, there's but again ) as an undefined policy on extragear packages, we don't have a branch tag on this packages, wich implies ( tcharam ) not have i18n branch packages and worst, they aren't available on i18n opfficial branch... So what i need do ? Create a script to download specific kdeextragear docs from HEAD i18n to create kdeextragear-i18n packages. 

So the hell is over after scripts and split, yada, yada, yada ? Far from this..

After created all automatic splitted specs things would be very well tamed to our RPM specs, but ( again ) look, package build failed.. And wonder why.. 

1 - There are many official translations really broken ( talkin on buildsystem ). If you install full package, or just a language, maybe you didn't see this, but things like directories with full Makefile.am and no files creates empty packages. This is an annoyance when we treat specs with scripts based on official i18n templates and look like translators not following all the instructions very well :-/
Before start to attack me, the fault isn't of translators, they do a very hard and boring job and is suppose that they didn't need know or handle buildsystem. But seens like we haven't a janitor for i18n buildsystem, which is huge.

2 - There's no policy about which is the minimal acceptable tranlation for applications. I'm not talking about the whole lang, which is ok not be fully translated, but for inside .pot files fully translated. I mean, if you start to translate the whole kword pot files ( as example ), make all available. 
Now i'm speaking on behalf translators itself.. Is insane take a pot file half translated and try to finish using your judge, if your lang team don't have translate policy, and believe me, even on team with policy there are a lot of divergences effectively noted on final tranlated potfiles.

So, if any of you are curious why i'm so upset with packaging i18n take a look on our 4000+ thousand packages for each kde-i18n release we have. :-/