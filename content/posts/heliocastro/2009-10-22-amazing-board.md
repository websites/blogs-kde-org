---
title:   "Amazing Board"
date:    2009-10-22
authors:
  - heliocastro
slug:    amazing-board
---
<img src="https://blogs.kde.org/files/images/board_0.jpg">Amazing Blog</img>
This is the amazing idea that came with Amanda from the new local KDE-MG group.
Simple, beatifull and the most interesting way to get conference people to enjoy your booth, put his ideas out of the mind and getting everyone close.
At this picture, you see Tomaz ( no tap dance included ), and the board, saying ODEIO ( I Hate ) on left and AMO ( I Love ) and everyone can express their feeling about our beloved project where we can improve it or not, writing a note, whatever you want to write.
Our small booth is really more interesting:-)
Right now, i'm in the talk table to introduce Anne-Marie in her's bugsquad presentation !
Later will talk more about our second Latinoware experience, getting better and better...
Ahh, i forgot to say, there's a nice tipical "Minas Gerais" prize waiting for one of the guys writing the note !
<!--break-->
