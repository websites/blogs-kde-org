---
title:   "The strange KDE vacations ever..."
date:    2005-05-03
authors:
  - heliocastro
slug:    strange-kde-vacations-ever
---
Well... This year i intended to take vacations at mom's house, in a beatifull place called Florianópolis..
I scheduled my vacations to april, 3 and i was thinking that everything would go smooth until the week before arrived !!
So, i was invited for 3 KDE, Desktop and Qt talks in this month. Ok, i thought, it's just 3.
But ( always have a but ), i didn't imagine what's coming next..
Here's the itinerary:
<li><b> Start-> Curitiba-PR ( ground 0 )</b> - First day of vacation -  total km: 0 km
<li><b> 8,9 April -> Belo Horizonte-MG ( 844 km from Curitiba )</b> http://psl-mg.softwarelivre.org/tiki-index.php?page=EMSL2005 - total km: 844 km.<br>
This is the first event. I did a KDE presentation on opening and a KDE and Desktop trends on a 2 hours tutorial.
Back to Curitiba. total km: 1688
<li><b> 11 April -> Florianópolis-SC ( 300 km from Curitiba ) </b>Mom's house. Everything was in a vacation mood ( florianópolis is an island with 44 beaches and some paradisiac places ) until a personal friend asked me to go in an event at a computer technical school. So, there goes Helio doing the same talk of Minas TWICE !! on same day, for two different groups. Preety cool as i saw lot of interested new guys on open source.
total km: 1144
<li><b> 18,19 April -> Belém-PA ( 2878 km from Florianopolis )</b> http://www.linuxpaidegua.com.br/forum/<br>
These was a huge event o the other side of country, a completly and amazing diferent culture. Belém scity is the famous place of Pará Nuts and is on the border of Amazon forest. I did a KDE tutorial and a Desktop talk for 400 persons.. The whole place and city worth every mile ( and is a lot ) of travel.
Can you imagine a italian like icecream place with some like 50 flavors, and the only knowed was Vanilla and Chocolat ? The made hiquality icecream with all native fruits from amazon forest.
Time to back to mom's house. <br>total km: 6900
<li><b>Curitiba 23/4 </b>I intended to take last week on my house in Curitiba, so i back and was invited by a external GIS project manager to go to INPE ( Space Research Intitute in Brazil ) at São José dos Campos on 28.
Total km: 7200
<li><b>São José dos Campos ( 450 km from Curitiba )</b> The interesting part about been in a place surrounded of Msc and Phd guys is about see how amazing a task can be solved for great minds. After some meetings, discovered that they use and love Qt there, i received som complains about <b>kdevelop</b> which i detected and can be easily solved. They tried use kdevelop on their projects:
http://terralib.dpi.inpe.br/ and http://www.dpi.inpe.br/terraview/ and because using a buggy kdevelop version, switched to eclipse. I will try address the problem for him later..
Back to Curitiba to take the flight to last event...
Total Km: 8100
<li><b>Belo Horizonte-MG 30/04 01/05 ( 844 from Curitiba)</b> - The brazilian LinuxChix event. This event is special, since they are a relaxed and specifically technical event and we have a lot of smart girls around. I did a brief 1 hour presentation of Qt4, and i was amazing that in certain moment i really looked like a salesperson in a matter of speak :-) ... The vibe was good, the usual saturday party was huge ( as the hangover. ) Pics: http://www.linuxchix.org.br/evento/2005/fotos/
Total km: 9800 !!!!!!!!
<br><br>
So, almost 10000 km just inside Brazil talking about KDE. Was phisically cansative of course, but this vacations made exactly what i need, take some time having fun with KDE, even not coding, but spreading the word. And almost no stress :-)
Hope next one be some like this
<!--break-->


