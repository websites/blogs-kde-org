---
title:   "Arena de Programação do FISL 8 / FISL 8 Programming Arena Contest"
date:    2007-04-10
authors:
  - heliocastro
slug:    arena-de-programação-do-fisl-8-fisl-8-programming-arena-contest
---
( For english speakers, translation will follow soon portuguese entry )

Bom, acredito que a maioria dos interessados nessa arena de programação do FISL devem já ter recebido o recente comunicado sobre premiações e desafios que pretendem colocar.
Eu fui pessoalmente convidado, com outros colegas brasileiros do projeto KDE para ajudarmos na mesma, sendo que somente eu poderei estar presente esse ano. 
Para tanto, tive que fazer algumas considerações sobre o planejamento original da arena como nos foi requisitado. Foi aventado a possibilidade de colocar no contest uma certa quantidade de bugs do KDE ( também Gnome e Debian ) como os ítens de resolução. Poderia ser até uma boa idéia, porém estamos falando de um torneio de tempo curto e seria muito difícil que os participantes, salvo excessões, tivessem conhecimento básico de tecnologia KDE e Gnome, e ainda mais sobre específica distribuição como Debian ( assumindo que todos os participantes usem no seu dia a dia a distribuição ). 
Foi de comum acordo entre o grupo que não seria viável simplesmente escolher bugs do KDE e separá-los por dificuldade, ainda mais se decidiríamos por kde 3 o kde 4. Isso é irreal no espaço de tempo curto envolvido.
Eu me ofereci prontamente para ajudar no último dia se necessário, porém meu planejamento pessoal envolve, com muito gosto, ser o "tech news man" para o br-linux, o qual o Augusto já tinha me convidado a tempos, portanto, meu tempo está reduzido em relação as atividades extras e as pessoais.
Mas de novo, me coloquei a disposição de poder ajudar, mesmo quem sabe ajudando a avaliar problemas reais e factíveis da arena, para que realmente possamos encontrar bons programadores, porque quem vive nesse ramo aqui, sabe como isso é escasso e raro...

( English version )
Well, i believe most of brazilian computer society turned on next big computer big event, FISL 8, already received a note about the programmer contest which will happens in the form of Google Code Jam style. I was personally invited, with other KDE brazilian fellows to help this event task. Unfortunately, i'm the only one who will be present. But i need to do some considerations about the original idea of this contest as our help was requested, They intend to pick KDE, Gnome and Debian ( ?? ) bugs from their bug trackers, and put for the contestants to solve, and divide this in dificulty levels from easy to hard. 
Was a common agreement that this is quite not viable since even to solve a minor kde bug ( kde 3 ? kde 4 ? ), this involves some basic knowledge of KDE infrastructuer ( as well Gnome, and maybe Debian but i still not understand Debian related to programmer bugs. ), I s rate difficult teach basic KDE programming in one day, in terms of use KDevelop and do everything elese to get a hello fancy world dialog. Imagine solve bugs in a small amount of time in contest. Not mentioning that we're talking about programmers that need to know C++, C, at least. Don't have the choice in terms of KDE or Gnome.
I offer my help for last day to give some help if necessary, but i have some personal task, which is be the tech news guy for br-linux, major open source/free software/tech news from Brazil, which i was "recruited" by their editor, Augusto Campos a few weeks ago, and i accepted gladly, even this making my personal time a little bit shorter during the event.
I hope the idea of the contest inside the event have good results, and after all, we can get some goos programmers mind to help us on our open source projects everywhere..