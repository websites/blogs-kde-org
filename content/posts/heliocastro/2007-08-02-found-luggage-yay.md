---
title:   "Found Luggage :-)  Yay !!"
date:    2007-08-02
authors:
  - heliocastro
slug:    found-luggage-yay
---
Ok, 22 days after, the bag is back !
All stuff inside, no damage at all, just the bag itself with some scratches.  
Interesting thing in that clearly luggage was open and all things changed position, even things that was inside a necessaire, like a deodorant, was drop on bag, and necessaire was still closed :-)
I will not complain about the scratches on the bag, i had enough stress about whole thing last weeks, i'm happy enough to have all things back with me.
Case closed !!