---
title:   "LinuxChix Brazil Meeting next weekend"
date:    2006-09-03
authors:
  - heliocastro
slug:    linuxchix-brazil-meeting-next-weekend
---
Breaking my long time without blogging and doing talks, i couldn't forget to say that we have soon the new Linuxchix event, and now with a new important target added on the roll, the teacher's side.
Basically one part of event will be dedicated to give headings for school and university teachers to how to teach and bring new students to the open source world.
Quoting the official press release:
"LinuxChix Brazil Meeting - Webcast, teachers and sponsors
Next September 8th and 9th, the 4th Linuxchix Brazil National Meeting will take place in Florianópolis, Brazil. The Meeting will be held in the Barddal Foundation. There is no need to pre-register, and registration will be on site. The talks will be presented in Portuguese.
The subjects this year cover OpenLDAP, Linux in 64 bits processors, Python, Academic Management Systems, Modern Graphics Interfaces, Gentoo, High Availability, FreeBSD, Asterisk, Udev, Kernel for beginners... it will be 22 talks, 4 mini courses and a Teacher's Afternoon."
Read entire press release here: [http://www.linuxchix.org.br/?q=node/72|LinuxChix Brazil 2006]
<!--break-->